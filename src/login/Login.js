import { Route, Routes} from 'react-router-dom';

import { LoginMain } from './join/LoginMain';

import { JoinRecruiters } from './join/RecruiterJoin';
import { JoinEmployers } from './join/EmployerJoin';

export const Login =() => {
  return(
    <>
      <Routes>
        <Route exact path="/" element={<LoginMain/>}/>

        {/* 회원가입 페이지 라우팅 */}
        <Route path='/join-recruiter' element={<JoinRecruiters/>}/>
        <Route path='/join-employer' element={<JoinEmployers/>}/>
      </Routes>      
    </>
  );
}

