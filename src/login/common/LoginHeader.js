import LocaleSelect from 'employers/components/common/LocaleSelect';
import { FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';
import styles from '../css/login.module.css';
import wecruitLogo from '../images/logo.png';

export const LoginHeader =() => {
  return(
    <header className={styles.loginHeader}>
    <div className={styles.headerWrap}>
      <Link to='/'>
        <img src={wecruitLogo} alt="위크루트 로고" />
      </Link>

      <div className='localeWrap'>
        <LocaleSelect/>
        <span>
          <FormattedMessage
            id="wecruit"
            defaultMessage="위크루트"
          /> 031-548-2310
        </span>
      </div>
    </div>
  </header>
  );
}