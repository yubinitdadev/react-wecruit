import { FormattedMessage } from 'react-intl';
import styles from '../css/login.module.css';

export const LoginFooter =() => {
  return (
    <footer className={styles.loginFooter}>
      <div className={styles.footerWrap}>
        <span>
          <FormattedMessage
            id="footer.nav1"
            defaultMessage="서비스 소개"
          />
        </span>
        <span>
          <FormattedMessage
            id="footer.nav2"
            defaultMessage="이용요금"
          />
        </span>
        <span>
          <FormattedMessage
            id="footer.nav3"
            defaultMessage="이용자주 묻는 질문요금"
          />
        </span>
        <span>
          <FormattedMessage
            id="footer.nav4"
            defaultMessage="블로그"
          />
        </span>
        <span>
          <FormattedMessage
            id="footer.nav5"
            defaultMessage="회사 소개"
          />
        </span>
      </div>
      <p>Copyright ⓒ WECRUIT. All Right Reserved.</p>
    </footer>
  );
}