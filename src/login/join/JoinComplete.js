import { LoginHeader } from "login/common/LoginHeader"
import { FormattedMessage } from "react-intl";
import { Link } from "react-router-dom";
import styles from '../css/login.module.css';

export const JoinComplete =() => {
  return(
    <>
      <LoginHeader />

      <div className={styles.joinPage}>
        <h1>
          <FormattedMessage
            id="description.join.complete"
            values = {{
              breakLine: <br/>
            }}
            defaultMessage="가입신청이 성공적으로{breakLine} 접수되었습니다."
          />
        </h1>

        <Link to="/" className={styles.blueButton}>
          <FormattedMessage
            id="back.to.home"
            defaultMessage="처음으로"
          />
        </Link>

        <div className={styles.alertBox}>
          <span>
            <span className="material-icons">error</span>
            <FormattedMessage
              id="description.join.ask"
              defaultMessage="시스템 보안을 위해 가입 승인 후 시스템을 사용하실 수 있습니다. 빠른 조치 사항이나 문의사항이 있는 경우 고객센터를 통해 확인해주세요."
            />
          </span>
          <span>
            <FormattedMessage
              id="description.channel.talk"
              defaultMessage="기타 문의 사항은 우측 하단 채널톡을 이용해 문의 해주세요."
            />
          </span>
        </div>
      </div>
    </>
  )
}