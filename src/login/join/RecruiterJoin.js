import {Link, useNavigate} from 'react-router-dom';
import { ToastContainer, toast } from "react-toastify";
import { LoginFooter } from '../common/LoginFooter';
import { LoginHeader } from '../common/LoginHeader';

import styles from '../css/login.module.css';

import employerJoin from '../images/tab_join1.jpg'
import recruitJoin from '../images/tab_join2on.jpg'

import useTerm from '../images/terms_of_use_ko.pdf';
import privacyPolicy from '../images/privacy_policy_ko.pdf';
import { useContext, useEffect, useRef, useState } from 'react';

import { CheckHasMinLength, CheckHasSameValue,  CheckIsOnlyEngOrNum, CheckIsValidEmail, CheckIsEmpty, CheckIsChecked, axiosCrudOnSubmit } from "commons/modules/commonUtil";
import { Context } from 'contexts';
import { FormattedMessage, useIntl } from 'react-intl';
import { messages } from 'lang/DefineMessages';

export const JoinRecruiters = () => {
  const intl = useIntl();
  let PROC_CHK = "";

  const [state, setState] = useState({
    mbrDivi: "MBRDIVI-02",
    mbrNm: "",
    mbrPos: "",
    celpNum: "",
    linkedIn: "",
    email: "",
    mbrId: "",
    pswd: "",
    mailYn: false,
    allChckBoxActive: false,
    termYn: false,
    personalYn: false,
    promoYn: false,
    fileUploadCmplt: false
  })

  const {
    state : {
        mbr,
        server
    },
    dispatch,
  } = useContext(Context);

  const mbrNmRef = useRef(null);
  const mbrPosRef = useRef(null);
  const celpNumRef = useRef(null);
  const emailRef = useRef(null);
  const mbrIdRef = useRef(null);
  const pswdRef = useRef(null);
  const confirmPswdRef = useRef(null);
  const linkedInRef = useRef(null);
  const allChckBox = useRef(null);
  const termChckBox = useRef(null);
  const personalChckBox = useRef(null);
  const promotionChckBox = useRef(null);

  const navigate = useNavigate();

  const isRecMailChckBoxClicked = () => {
    setState({
      ...state,
      mailYn : !state.mailYn
    })
  }

  const isAllChckBoxClicked = () => {
    setState({
      ...state,
      allChckBoxActive : !state.allChckBoxActive
    })
  };

  const isTermChckBoxClicked = () => {
    setState({
      ...state,
      termYn: !state.termYn
    })
  };
  
  const isPersonalChckBoxClicked = () => {
    setState({
      ...state,
      personalYn: !state.personalYn
    })
  };
  
  const isPromotionChckBoxClicked = () => {
    setState({
      ...state,
      promoYn: !state.promoYn
    })
  };

  const clickAllChckBox = () => {
    if(!state.allChckBoxActive)
    {
      setState({
        ...state,
        allChckBoxActive : true,
        termYn : true,
        personalYn : true,
        promoYn : true
      })

      termChckBox.current.checked = true;
      personalChckBox.current.checked = true;
      promotionChckBox.current.checked = true;
    } 
    else 
    {
      setState({
        ...state,
        allChckBoxActive : false,
        termYn : false,
        personalYn : false,
        promoYn : false
      })

      termChckBox.current.checked = false;
      personalChckBox.current.checked = false;
      promotionChckBox.current.checked = false;
    }
  }

  // 체크박스 useEffect
  useEffect(() => {
    if(state.termYn && state.personalYn && state.promoYn)
    {
      setState({
        ...state,
        allChckBoxActive: true
      })
      allChckBox.current.checked = true;
    }
    else if(state.allChckBoxActive)
    {
      [state.termYn, state.personalYn, state.promotionChckBox].forEach((val) => {
        if(!val) 
        {
          setState({
            ...state,
            allChckBoxActive: false
          })
          allChckBox.current.checked = false;
        }
      })
    }
  }, [state.allChckBoxActive, state.termYn, state.personalYn, state.promoYn])

  const jsonCompResult = ( resJson ) => {
    if( resJson.resCd !== "0000" )
    {
      return;
    }
    else
    {
      if( PROC_CHK === "INS" )
      {
        if( resJson.mbrUid === "DUPL" )
        {
          toast.error(intl.formatMessage({
            id: "error.id.exist",
            defaultMessage: "이미 사용중인 아이디입니다."
          }));
          mbrIdRef.current.focus();
          return;
        }
        else
        {
          alert(intl.formatMessage({
            id: "info.recruiter.join.success",
            defaultMessage: "리크루터 가입이 완료되었습니다."
          }));
          navigate("/");
          return;
        }
      }
    }
  }

  async function mbrEmailDupl() {
    let goUrl = server.path + "/mbr/mbrEmailDupl";
    let data = {
      email : emailRef.current.value
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    if(axiosRes.data.mbrUid === "DUPL")
    {
      toast.error(intl.formatMessage({
        id: "error.email.exist",
        defaultMessage: "이미 가입된 이메일입니다."
    }));
      return;
    } 
    else if (axiosRes.data.mbrUid === "NOMBR")
    {
      toast.info(intl.formatMessage({
        id: "info.usable.email",
        defaultMessage: "사용 가능한 이메일입니다."
      }));
      emailRef.current.readOnly = true;
      return
    }
  }

  async function goCRUD() {
    if( CheckIsEmpty(mbrNmRef, 
      intl.formatMessage({
        id: "error.need.input.name",
        defaultMessage: "이름을 입력해주세요."
      })) === false ) return;
    if( CheckIsEmpty(mbrPosRef, 
      intl.formatMessage({
        id: "error.need.input.position",
        defaultMessage: "직급을 입력해주세요."
      })) === false ) return;
    if( CheckIsEmpty(celpNumRef, 
      intl.formatMessage({
        id: "error.need.input.phone",
        defaultMessage: "휴대폰번호를 입력해주세요."
      })) === false ) return;
    if( CheckIsEmpty(emailRef, 
      intl.formatMessage({
        id: "error.need.input.email",
        defaultMessage: "이메일을 입력해주세요."
      })) === false ) return;
    if( CheckIsValidEmail(emailRef, intl.formatMessage({
      id: "error.email.format.wrong",
      defaultMessage: "이메일 형식이 올바르지 않습니다"
    })) === false ) return;
    if( CheckIsEmpty(mbrIdRef, 
      intl.formatMessage({
        id: "error.need.input.id",
        defaultMessage: "아이디를 입력해주세요."
      })) === false ) return;
    if( CheckIsEmpty(pswdRef, 
      intl.formatMessage({
        id: "error.need.input.re.password",
        defaultMessage: "비밀번호를 다시 입력해주세요."
      })) === false ) return; 
    if( CheckHasMinLength(pswdRef, 
      intl.formatMessage(messages.minCharacter, {
        minLength: 8
      }), 8) === false ) return;
    if( CheckIsOnlyEngOrNum(pswdRef, intl.formatMessage({
      id: "error.only.english.number",
      defaultMessage: "비밀번호는 영어/숫자만을 포함해야 합니다."
    })) === false ) return;
    if( CheckHasSameValue(pswdRef, confirmPswdRef, 
      intl.formatMessage({
        id: "error.password.not.match",
        defaultMessage: "비밀번호가 일치하지 않습니다."
      })) === false ) return;
    if( CheckIsChecked(termChckBox, 
      intl.formatMessage({
        id: "error.need.agree.useterm",
        defaultMessage: "서비스 이용약관에 동의하셔야 합니다."
      })) === false ) return;
    if( CheckIsChecked(personalChckBox, 
      intl.formatMessage({
        id: "error.need.agree.personal.info",
        defaultMessage: "개인정보 수집 및 이용약관에 동의하셔야 합니다."
      })) === false ) return;

    cntntsCRUD();
  }

  async function cntntsCRUD() {
    PROC_CHK = 'INS';

    let goUrl = server.path + "/mbr/mbrinsert"
    //let goUrl = "/mbr";
    let axiosRes = await axiosCrudOnSubmit(state, goUrl, "POST");

    jsonCompResult(axiosRes.data);
  }

  return (
    <>
      <LoginHeader />
      <ToastContainer />
      <div className={styles.joinPage}>
          <h1>Welcome to Wecruit</h1>
          <div className='mypageWrap'>

            <form className="employersJoinForm">
              <div className={styles.selectGroup}>
                <Link to='/join-employer'>
                  <figure>
                    <img src={employerJoin} alt="인사담당자" />
                  </figure>
                </Link>
                
                <Link to='/join-recruiter'>
                  <figure className={styles.active}>
                    <img src={recruitJoin} alt="리크루터" />
                  </figure>
                </Link>
              </div>

              <div className="formSection">
                <div>
                  <label htmlFor="mbrNm">
                    <FormattedMessage
                      id="name"
                      defaultMessage="이름"
                    />*
                  </label>
                  <input type="text" id="mbrNm" 
                  placeholder={intl.formatMessage({
                    id: "placeholder.name",
                    defaultMessage: "이름을 입력하세요."
                  })}
                  ref={mbrNmRef}
                  onChange={(event)=>{
                    setState({
                      ...state,
                      mbrNm: event.target.value
                    })
                  }}/>
                </div>

                <div>
                  <label htmlFor="mbrPos">
                    <FormattedMessage
                      id="position"
                      defaultMessage="직급"
                    />*
                  </label>
                  <input type="text" 
                  placeholder={intl.formatMessage({
                    id: "placeholder.position",
                    defaultMessage: "직급을 입력하세요."
                  })}
                  ref={mbrPosRef}
                  onChange={(event)=>{
                    setState({
                      ...state,
                      mbrPos: event.target.value
                    })
                  }}/>
                </div>

                <div className="column">
                  <label htmlFor="celpNum">
                    <FormattedMessage
                      id="phone.number"
                      defaultMessage="휴대폰번호"
                    />*
                  </label>
                  
                  <select name id>
                    <option>
                      {intl.formatMessage({
                        id: "korea",
                        defaultMessage: "대한민국"
                      })} +82
                    </option>
                  </select>

                  <div>
                    <input type="text" id="celpNum"
                    placeholder={intl.formatMessage({
                      id: "placeholder.phone.number",
                      defaultMessage: "&quot;-&quot;없이 숫자만 입력하세요."
                    })}
                    ref={celpNumRef} 
                    onChange={(event)=>{
                      setState({
                        ...state,
                        celpNum: event.target.value
                    })
                    }}/>
                    <button type="button">
                      <FormattedMessage
                        id="phone.verification"
                        defaultMessage="휴대폰 인증"
                      />
                    </button>
                  </div>
                  <div>
                    <input 
                      type="text" 
                      defaultValue={intl.formatMessage({
                        id: "placeholder.verification.code",
                        defaultMessage: "인증번호를 입력해주세요"
                      })}
                      readOnly
                    />
                    <button type="button">
                      <FormattedMessage
                        id="verification.check"
                        defaultMessage="인증 확인"
                      />
                    </button>
                  </div>
                </div>

                <div>
                  <label htmlFor="linkedin">
                    <FormattedMessage
                      id="linkedin.address"
                      defaultMessage={intl.formatMessage({
                        id: "linkedin.address",
                        defaultMessage: "링크드인 주소"
                      })}
                    />
                  </label>
                  <input type="text" id="linkedin" 
                  placeholder={intl.formatMessage({
                    id: "placeholder.linkedin",
                    defaultMessage: "링크드인 주소를 입력하세요."
                  })}
                  ref={linkedInRef}
                  onChange={(event)=>{
                    setState({
                      ...state,
                      linkedIn: event.target.value
                    })
                  }}/>
                </div>

                <div>
                  <label htmlFor="email">
                    <FormattedMessage
                      id="email"
                      defaultMessage="이메일"
                    />*
                  </label>
                  <div>
                    <input type="text" id="email" 
                    placeholder={intl.formatMessage({
                      id: "placeholder.email",
                      defaultMessage: "이메일을 입력하세요."
                    })}
                    ref={emailRef}
                    onChange={(event)=>{
                      setState({
                        ...state,
                        email: event.target.value
                      })
                    }}/>
                    <button type="button" onClick={ () => {
                      if( CheckIsEmpty(emailRef, 
                        intl.formatMessage({
                          id: "error.need.input.email",
                          defaultMessage: "이메일을 입력해주세요."
                        })) === false ) return; 
                      if( CheckIsValidEmail(emailRef, intl.formatMessage({
                        id: "error.email.format.wrong",
                        defaultMessage: "이메일 형식이 올바르지 않습니다"
                      })) === false ) return;
                      mbrEmailDupl();
                    }}>
                      <FormattedMessage
                        id="check.duplicate"
                        defaultMessage="중복 확인"
                      />
                    </button>
                  </div>
                </div>

                <div className='checkboxWrap'>
                  <input type="checkbox" id="recMail" 
                    onClick={isRecMailChckBoxClicked}/>
                  <label htmlFor='recMail'>
                    <FormattedMessage
                      id="description.mail.agree"
                      defaultMessage="인사뉴스레터 및 위크루트 홍보메일 수신에 동의합니다."
                    />
                  </label>
                </div>
              </div>

              
              <div className='formSection'>
                <div>
                  <label htmlFor="mbrId">
                    <FormattedMessage
                      id="id"
                      defaultMessage="아이디"
                    />*
                  </label>
                  <input type="text" id="mbrId" 
                  placeholder={intl.formatMessage({
                    id: "placeholder.id",
                    defaultMessage: "아이디를 입력하세요."
                  })}
                  ref={mbrIdRef}
                  onChange={(event)=>{
                    setState({
                      ...state,
                      mbrId: event.target.value
                    })
                  }}/>
                </div>
                
                <div>
                  <label htmlFor="pswd">
                    <FormattedMessage
                      id="pw"
                      defaultMessage="비밀번호"
                    />*
                  </label>
                  <div className="column">
                    <input type="password" id="pswd" 
                    placeholder={intl.formatMessage({
                      id: "placeholder.pw.combination",
                      defaultMessage: "8자리 이상의 영문/숫자 조합으로 입력해주세요"
                    })}
                    ref={pswdRef}
                    onChange={(event)=>{
                      setState({
                        ...state,
                        pswd: event.target.value
                      })
                    }}/>
                    <input type="password" id="confirmPswd" 
                    placeholder={intl.formatMessage({
                      id: "pw.check",
                      defaultMessage: "비밀번호 확인"
                    })}
                    ref={confirmPswdRef}/>
                  </div>
                </div>
                <div className="tooltipBox">
                  <p>
                    <span className="material-icons">error</span> <FormattedMessage
                      id="warning"
                      defaultMessage="주의사항"
                    />
                  </p>
                  <div className="tooltip">
                    <p>
                      <FormattedMessage
                        id="tooltip.pw1"
                        defaultMessage="비밀번호 난이도: 영어, 숫자 조합"
                      />
                    </p>
                    <p>
                      <FormattedMessage
                        id="tooltip.pw2"
                        defaultMessage="비밀번호 길이: 8자 이상"
                      />
                    </p>
                  </div>
                </div>
              </div>
              
              <div className="formSection termAgree">
                <div>
                  <input type="checkbox" id="selectAll" 
                    onClick={isAllChckBoxClicked} 
                    onChange={clickAllChckBox}
                    ref={allChckBox}/>
                  <label htmlFor="selectAll">
                    <FormattedMessage
                      id="description.information.agree"
                      defaultMessage="이용약관, 개인정보 수집 및 이용, 프로모션 안내 메일 수신(선택)에 모두 동의합니다."
                    />
                  </label>
                </div>
                <div>
                  <input type="checkbox" id="term" 
                    onClick={isTermChckBoxClicked}
                    ref={termChckBox}/>
                  <label htmlFor="term">
                    <FormattedMessage
                      id="useterm.agree.required"
                      defaultMessage="이용약관 동의(필수)"
                    />
                  </label>
                  <Link to={useTerm} target="_blank">
                    <FormattedMessage
                      id="see"
                      defaultMessage="보기"
                    />
                  </Link>
                </div>
                <div>
                  <input type="checkbox" id="personalData" 
                    onClick={isPersonalChckBoxClicked}
                    ref={personalChckBox}/>
                  <label htmlFor="personalData">
                    <FormattedMessage
                      id="personal.agree.required"
                      defaultMessage="개인정보 수집 및 이용(필수)"
                    />
                  </label>
                  <Link to={privacyPolicy} target="_blank">
                    <FormattedMessage
                      id="see"
                      defaultMessage="보기"
                    />
                  </Link>
                </div>
                <div>
                  <input type="checkbox" id="promotion" 
                    onClick={isPromotionChckBoxClicked}
                    ref={promotionChckBox}/>
                  <label htmlFor="promotion">
                    <FormattedMessage
                      id="promotion.mail.agree.optional"
                      defaultMessage="프로모션 안내 메일 수신(선택)"
                    />
                  </label>
                </div>
                {/*  className="active"를 이용해 버튼 활성화 */}
                <button className="submitBtn active" type="button" onClick={goCRUD}>
                  <FormattedMessage
                    id="join.wecruit"
                    defaultMessage="가입하기"
                  />
                </button>
              </div>
              <div className="alertBox">
                <span>
                  <span className="material-icons">error</span>
                    <FormattedMessage
                      id="description.require.agreement"
                      defaultMessage="이용을 위해서는 이용약관, 개인정보 수집 및 이용에 모두 동의하셔야 합니다."
                    />
                </span>
                <span>
                  <FormattedMessage
                    id="description.channel.talk"
                    defaultMessage="기타 문의 사항은 우측 하단 채널톡을 이용해 문의 해주세요."
                  />
                </span>
              </div>
            </form>
          </div>
        </div>
        
        <LoginFooter />
    </>
  );
}