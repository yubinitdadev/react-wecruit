import { useState, useEffect, useRef, useContext } from "react";
import { useNavigate, Link } from 'react-router-dom';
import { ToastContainer, toast } from "react-toastify";
import { LoginFooter } from '../common/LoginFooter';
import { LoginHeader } from '../common/LoginHeader';
import '../scss/employer-join.scss';
import styles from '../css/login.module.css';

import employerJoin from '../images/tab_join1.jpg';
import recruitJoin from '../images/tab_join2on.jpg';

import useTerm from '../images/terms_of_use_ko.pdf';
import privacyPolicy from '../images/privacy_policy_ko.pdf';

import { CheckHasMinLength, CheckHasSameValue, CheckIsValidBizId, CheckIsOnlyEngOrNum, CheckIsValidEmail, CheckIsEmpty, CheckIsChecked, CheckIsFileEmpty, axiosCrudOnSubmit } from "commons/modules/commonUtil";
import { snglFileUpload } from "commons/modules/multipartUtil";
import { Context } from "contexts";
import { FormattedMessage, useIntl } from "react-intl";
import { messages } from "lang/DefineMessages";

export const JoinEmployers = (props) => {
  const intl = useIntl();
  let PROC_CHK = "";

  const {
    state : {
        server,
    },
    dispatch,
  } = useContext(Context);

  const [state, setState] = useState({
    mbrDivi: "MBRDIVI-01",
    email: "",
    pswd: "",
    bizNum: "",
    bizNm: "",
    bizCertThmnl : "",
    bizCertThmnlNm : "파일을 선택해주세요.",
    mbrNm: "",
    celpNum: "",
    rcmndCd: "",
    uploadFiles: [ "disable", "disable", "disable", "disable", "disable" ],
    filePosNm: ["", "bizCertThmnl", "", "", ""],
    bizFileJsonStr: ["NULL", "", "NULL", "NULL", "NULL"],
    snglImgFileJsonStr: ["NULL", "", "NULL", "NULL", "NULL"],
    allChckBoxActive: false,
    termYn: false,
    personalYn: false,
    promoYn: false,
    fileUploadCmplt: false
  })

  const emailRef = useRef(null);
  const pswdRef = useRef(null);
  const confirmPswdRef = useRef(null);
  const bizNumRef = useRef(null);
  const bizNameRef = useRef(null);
  const bizCertRef = useRef(null);
  const userNameRef = useRef(null);
  const allChckBox = useRef(null);
  const termChckBox = useRef(null);
  const personalChckBox = useRef(null);
  const promotionChckBox = useRef(null);

  const navigate = useNavigate();

  const isAllChckBoxClicked = () => {
    setState({
      ...state,
      allChckBoxActive : !state.allChckBoxActive
    })
  };

  const isTermChckBoxClicked = () => {
    setState({
      ...state,
      termYn: !state.termYn
    })
  };
  
  const isPersonalChckBoxClicked = () => {
    setState({
      ...state,
      personalYn: !state.personalYn
    })
  };
  
  const isPromotionChckBoxClicked = () => {
    setState({
      ...state,
      promoYn: !state.promoYn
    })
  };

  const clickAllChckBox = () => {
    if(!state.allChckBoxActive)
    {
      setState({
        ...state,
        allChckBoxActive : true,
        termYn : true,
        personalYn : true,
        promoYn : true
      })

      termChckBox.current.checked = true;
      personalChckBox.current.checked = true;
      promotionChckBox.current.checked = true;
    } 
    else 
    {
      setState({
        ...state,
        allChckBoxActive : false,
        termYn : false,
        personalYn : false,
        promoYn : false
      })

      termChckBox.current.checked = false;
      personalChckBox.current.checked = false;
      promotionChckBox.current.checked = false;
    }
  }

  // 체크박스 useEffect
  useEffect(() => {
    if(state.termYn && state.personalYn && state.promoYn)
    {
      setState({
        ...state,
        allChckBoxActive: true
      })
      allChckBox.current.checked = true;
    }
    else if(state.allChckBoxActive)
    {
      [state.termYn, state.personalYn, state.promotionChckBox].forEach((val) => {
        if(!val) 
        {
          setState({
            ...state,
            allChckBoxActive: false
          })
          allChckBox.current.checked = false;
        }
      })
    }
  }, [state.allChckBoxActive, state.termYn, state.personalYn, state.promoYn])

  const jsonCompResult = ( resJson ) => {
    if( resJson.resCd !== "0000" )
    {
      toast.error(intl.formatMessage({
        id: "error.error.in.server",
        defaultMessage: "서버에 오류가 발생했습니다."
    }));
      return;
    }
    else
    {
      if( PROC_CHK === "INS" )
      {
        setState({
          ...state,
          fileUploadCmplt: false
        });

        navigate("/join-complete");
        
        return;
      }
      else if( PROC_CHK === "FILEUPLOAD" )
      {
        let jsonStr = JSON.stringify(resJson.resFileList);
        let fileList = JSON.parse(jsonStr);
        let fileListPos = 0;
        let tmpList = state.snglImgFileJsonStr;

        for(let i = 0; i < fileList.length; i++)
        {
          tmpList[i+1] = JSON.stringify( fileList[fileListPos] );
        }

        setState({
          ...state,
          fileUploadCmplt: true,
          snglImgFileJsonStr: tmpList,
          bizFileJsonStr: tmpList
        });
      }
    }
  }

  useEffect(()=>{
    if(state.fileUploadCmplt) {
      cntntsCRUD(); 
    }
  }, [state.fileUploadCmplt])

  async function mbrDupl() {
    let goUrl = server.path + "/mbr/mbrdupl";
    let data = {
      email : emailRef.current.value
    };

    
    
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    if(axiosRes.data.mbrUid === "DUPL")
    {
      toast.error(intl.formatMessage({
        id: "error.email.exist",
        defaultMessage: "이미 가입된 이메일입니다."
    }));
      return;
    } 
    else if (axiosRes.data.mbrUid === "NOMBR")
    {
      toast.info(intl.formatMessage({
        id: "info.usable.id",
        defaultMessage: "사용 가능한 아이디입니다."
      }));
      return
    }
  }

  async function goCRUD() {
    if( CheckIsEmpty(emailRef, 
      intl.formatMessage({
        id: "error.need.input.email",
        defaultMessage:  "이메일을 입력해주세요."
      })) === false ) return;
    if( CheckIsValidEmail(emailRef, intl.formatMessage({
      id: "error.email.format.wrong",
      defaultMessage: "이메일 형식이 올바르지 않습니다"
    })) === false ) return;
    if( CheckIsEmpty(pswdRef, 
      intl.formatMessage({
        id: "error.need.input.password",
        defaultMessage: "비밀번호를 입력해주세요."
      })) === false ) return; 
    if( CheckHasMinLength(pswdRef,
      intl.formatMessage(messages.minCharacter, {
      minLength: 8
    }),  8) === false ) return;
    if( CheckIsOnlyEngOrNum(pswdRef, 
      intl.formatMessage({
      id: "error.only.english.number",
      defaultMessage: "비밀번호는 영어/숫자만을 포함해야 합니다."
    })) === false ) return;
    if( CheckHasSameValue(pswdRef, confirmPswdRef, 
      intl.formatMessage({
        id: "error.password.not.match",
        defaultMessage: "비밀번호가 일치하지 않습니다."
      })) === false ) return;
    if( CheckIsEmpty(bizNumRef, 
      intl.formatMessage({
        id: "error.need.input.corp.num",
        defaultMessage: "사업자등록번호를 입력해주세요."
      })) === false ) return;
    if( CheckIsValidBizId(bizNumRef,
      intl.formatMessage({
        id: "error.corp.number.format.wrong",
        defaultMessage: "사업자번호 형식이 올바르지 않습니다."
      })) === false ) return;
    if( CheckIsEmpty(bizNameRef, 
      intl.formatMessage({
        id: "error.need.input.corp.name",
        defaultMessage: "기업체명을 입력해주세요."
      })) === false ) return;
    if( CheckIsEmpty(userNameRef, 
      intl.formatMessage({
        id: "error.need.input.employer.name",
        defaultMessage: "인사담당자명을 입력해주세요."
      })) === false ) return;
    if( CheckIsChecked(termChckBox, 
      intl.formatMessage({
        id: "error.need.agree.useterm",
        defaultMessage: "서비스 이용약관에 동의하셔야 합니다."
      })) === false ) return;
    if( CheckIsChecked(personalChckBox, 
      intl.formatMessage({
        id: "error.need.agree.personal.info",
        defaultMessage: "개인정보 수집 및 이용약관에 동의하셔야 합니다."
      })) === false ) return;
    if( CheckIsFileEmpty(bizCertRef, 
      intl.formatMessage({
        id: "error.need.corp.file",
        defaultMessage: "사업자등록증 파일을 첨부해주세요."
      })) === false ) return;

    let fileStorePath = "Globals.fileStoreBizPath"; //로컬저장경로
    let fileLinkPath = "Globals.fileLinkBizPath"; //link경로
    let filePrefix = "biz"; //파일코드

    PROC_CHK = "FILEUPLOAD";

    let uploadRes = await snglFileUpload( server.path, fileStorePath, fileLinkPath, filePrefix, state.uploadFiles, state.filePosNm);
    
    jsonCompResult(uploadRes.data);
  }

  async function cntntsCRUD() {
    PROC_CHK = 'INS';

    let goUrl = server.path + "/mbr/mbrinsert"
    let axiosRes = await axiosCrudOnSubmit(state, goUrl, "POST");

    jsonCompResult(axiosRes.data);
  }

  return (
    <>
      <LoginHeader />
      <ToastContainer />
      <div className={styles.joinPage}>
        <h1>Welcome to Wecruit</h1>
        <div className='mypageWrap'>
          <form className="employersJoinForm">
            <div className={styles.selectGroup}>
              <Link to='/join-employer'>
                <figure className={styles.active}>
                  <img src={employerJoin} alt="인사담당자" />
                </figure>
              </Link>
              
              <Link to='/join-recruiter'>
                <figure>
                  <img src={recruitJoin} alt="리크루터" />
                </figure>
              </Link>
            </div>

            <div className="formSection">
              <div>
                <label htmlFor="employerEmail">
                  <FormattedMessage
                    id="email.address"
                    defaultMessage="이메일주소"
                  />(ID)*
                </label>
                <div>
                  <input 
                    type="text" 
                    placeholder={intl.formatMessage({
                      id: "placeholder.email",
                      defaultMessage: "이메일을 입력하세요."
                    })}
                    id="employerEmail" 
                    name="email"  
                    required
                    ref={emailRef}
                    onChange={(e) => {
                      setState({
                        ...state,
                        email: e.target.value
                      });
                    }}/> 
                  <button type="button" 
                  onClick={ 
                    ()=> {
                      if( CheckIsEmpty(emailRef, 
                        intl.formatMessage({
                          id: "error.need.input.email",
                          defaultMessage: "이메일을 입력해주세요."
                        })) === false ) return;
                      if( CheckIsValidEmail(emailRef, intl.formatMessage({
                        id: "error.email.format.wrong",
                        defaultMessage: "이메일 형식이 올바르지 않습니다"
                      })) === false ) return;
                      mbrDupl();
                    }}>
                      <FormattedMessage
                        id="check.duplicate"
                        defaultMessage="중복 확인"
                      />
                    </button>
                </div>
              </div>

              <div>
                <label htmlFor="employerPW">
                  <FormattedMessage
                    id="pw"
                    defaultMessage="비밀번호"
                  />*
                </label>
                <div className="column">
                  <input type="password" id="employerPW" 
                    required 
                    ref = {pswdRef}
                    placeholder={intl.formatMessage({
                      id: "placeholder.pw.combination",
                      defaultMessage: "8자리 이상의 영문/숫자 조합으로 입력해주세요"
                    })}
                    onChange={(e) => {
                      setState({
                        ...state,
                        pswd: e.target.value
                      });
                    }}/>
                  <input type="password" 
                    required 
                    ref = {confirmPswdRef}
                    placeholder={intl.formatMessage({
                      id: "pw.check",
                      defaultMessage: "비밀번호 확인"
                    })} 
                  />
                </div>
              </div>
              <div className="tooltipBox">
                <p>
                  <span className="material-icons">error</span> <FormattedMessage
                    id="warning"
                    defaultMessage="주의사항"
                  />
                </p>
                <div className="tooltip">
                  <p>
                    <FormattedMessage
                      id="tooltip.pw1"
                      defaultMessage="비밀번호 난이도: 영어, 숫자 조합"
                    />
                  </p>
                  <p>
                    <FormattedMessage
                      id="tooltip.pw2"
                      defaultMessage="비밀번호 길이: 8자 이상"
                    />
                  </p>
                </div>
              </div>
            </div>
            
            <div className='formSection'>
              <div>
                <label htmlFor="bizNum">
                  <FormattedMessage
                    id="corporate.number"
                    defaultMessage="사업자등록번호"
                  />*
                </label>
                <div>
                  <input type="text" id="bizNum" 
                    placeholder={intl.formatMessage({
                      id: "placeholder.phone.number",
                      defaultMessage: "&quot;-&quot;없이 숫자만 입력하세요."
                    })}
                    ref={bizNumRef}
                    onChange={(e) => {
                      setState({
                        ...state,
                        bizNum: e.target.value
                      });
                    }} />
                  <button type="button" onClick={() => {
                    if(CheckIsEmpty(bizNumRef, 
                      intl.formatMessage({
                        id: "error.need.input.corp.num",
                        defaultMessage: "사업자등록번호를 입력해주세요."
                      })) === false) return;
                    if(CheckIsValidBizId(bizNumRef) !== false) toast.info("알맞은 사업자 등록번호입니다.");
                    }}>
                      <FormattedMessage
                        id="check"
                        defaultMessage="확인"
                      />
                    </button>
                </div>
              </div>
              <div>
                <label htmlFor="bizNm">
                  <FormattedMessage
                    id="mypage.corporate.name"
                    defaultMessage="기업체명"
                  />*
                </label>
                <input type="text" 
                required
                ref={bizNameRef}
                placeholder={intl.formatMessage({
                  id: "placeholder.corporate.name",
                  defaultMessage: "기업체명을 입력하세요"
                })} 
                onChange={(e) => {
                  setState({
                    ...state,
                    bizNm: e.target.value
                  });
                }} />
              </div>
              <div className="tooltipBox">
                <p>
                  <span className="material-icons">error</span> <FormattedMessage
                    id="warning"
                    defaultMessage="주의사항"
                  />
                </p>
                <div className="tooltip">
                  <p>
                    <FormattedMessage
                      id="tooltip.corporate1"
                      defaultMessage="법인의 경우 약자를 활용하여 법인의 종류를 구분하여 입력바랍니다."
                    />
                  </p>
                  <p>
                    <FormattedMessage
                      id="tooltip.corporate2"
                      defaultMessage="(예) ㈜위크루트/위크루트㈜/(유)유미특허/위드회계법인(유) 등"
                    />
                  </p>
                </div>
              </div>

              <div>
                <label>
                  <FormattedMessage
                    id="corporate.file.attach"
                    defaultMessage="사업자등록증 첨부"
                  />*
                </label>
                <div className="fileUpload">
                  <input type="file" id="fileInput1" className="fileInput" 
                  accept=".png, .jpg, .jpeg, .pdf"
                  ref = {bizCertRef}
                  onChange={(e) => {
                    let tmpList = [...state.uploadFiles];
                    tmpList[1] = e.target.files[0];

                    let tmpJsonStrList = [...state.snglImgFileJsonStr];
                    tmpJsonStrList[1] = "";
                    
                    setState({
                      ...state,
                      uploadFiles: tmpList,
                      snglImgFileJsonStr: tmpJsonStrList,
                      bizCertThmnl: "",
                      bizCertThmnlNm: e.target.files[0].name
                    })
                  }}/>
                  <span className="fileName">{state.bizCertThmnlNm}</span>  
                  <label htmlFor="fileInput1">
                    <FormattedMessage
                      id="find.out"
                      defaultMessage="찾아보기"
                    />
                  </label>
                </div>
              </div>
              <div className="tooltipBox">
                <p>
                  <span className="material-icons">error</span> <FormattedMessage
                    id="warning"
                    defaultMessage="주의사항"
                  />
                </p>
                <div className="tooltip">
                  <p>
                    <FormattedMessage
                      id="description.corporate.file1"
                      defaultMessage="파일명에 특수문자가 있는 경우 등록할 수 없습니다. (국문, 영문, 숫자만 가능)"
                    />
                  </p>
                  <p>
                    <FormattedMessage
                      id="description.corporate.file2"
                      defaultMessage="구글 크롬(Chrome) 브라우저를 활용하여 업로딩을 진행바랍니다."
                    />
                  </p>
                </div>
              </div>

              <div>
                <label htmlFor="userName">
                  <FormattedMessage
                    id="employer.name"
                    defaultMessage="인사담당자명"
                  />*
                </label>
                <input type="text" id="userName" 
                required
                ref={userNameRef}
                placeholder={intl.formatMessage({
                  id: "placeholder.name",
                  defaultMessage: "이름을 입력하세요"
                })} 
                onChange={(e) => {
                  setState({
                    ...state,
                    mbrNm: e.target.value
                  });
                }}/>
              </div>
              
              <div className="column">
                <label htmlFor="userPhone">
                  <FormattedMessage
                    id="phone.number"
                    defaultMessage="휴대폰번호"
                  />*
                </label>
                <select name id>
                  <option>
                    {intl.formatMessage({
                      id: "korea",
                      defaultMessage: "대한민국"
                    })} +82
                  </option>
                </select>
                <div>
                  <input type="text" id="userPhone" 
                  placeholder={intl.formatMessage({
                    id: "placeholder.phone.number",
                    defaultMessage: "&quot;-&quot;없이 숫자만 입력하세요."
                  })}
                  onChange={(e) => {
                    setState({
                      ...state,
                      celpNum: e.target.value
                    });
                  }}/>
                  <button type="button">
                    <FormattedMessage
                      id="phone.verification"
                      defaultMessage="휴대폰 인증"
                    />
                  </button>
                </div>
                <div>
                  <input type="text" defaultValue="인증번호를 입력해주세요" readOnly />
                  <button type="button">
                    <FormattedMessage
                      id="verification.check"
                      defaultMessage="인증 확인"
                    />
                  </button>
                </div>
              </div>
            
              <div>
                <label htmlFor="recommendCode">
                  <FormattedMessage
                    id="referral.code"
                    defaultMessage="추천인 코드"
                  />
                </label>
                <input type="text" 
                placeholder={intl.formatMessage({
                  id: "placeholder.referral.code",
                  defaultMessage: "추천인 코드를 입력하세요. (선택)"
                })}
                onChange={(e) => {
                  setState({
                    ...state,
                    rcmndCd: e.target.value
                  });
                }}/>
              </div>
            </div>
            
            <div className="formSection termAgree">
              <div>
                <input type="checkbox" id="selectAll" 
                  onClick={isAllChckBoxClicked} 
                  onChange={clickAllChckBox}
                  ref={allChckBox}/>
                <label htmlFor="selectAll">
                  <FormattedMessage
                    id="description.information.agree"
                    defaultMessage="이용약관, 개인정보 수집 및 이용, 프로모션 안내 메일 수신(선택)에 모두 동의합니다."
                  />
                </label>
              </div>
              <div>
                <input type="checkbox" id="term" 
                  onClick={isTermChckBoxClicked}
                  ref={termChckBox}/>
                <label htmlFor="term">
                  <FormattedMessage
                    id="useterm.agree.required"
                    defaultMessage="이용약관 동의(필수)"
                  />
                </label>
                <Link to={useTerm} target="_blank">
                  <FormattedMessage
                    id="see"
                    defaultMessage="보기"
                  />
                </Link>
              </div>
              <div>
                <input type="checkbox" id="personalData" 
                  onClick={isPersonalChckBoxClicked}
                  ref={personalChckBox}/>
                <label htmlFor="personalData">
                  <FormattedMessage
                    id="personal.agree.required"
                    defaultMessage="개인정보 수집 및 이용(필수)"
                  />
                </label>
                <Link to={privacyPolicy} target="_blank">
                  <FormattedMessage
                    id="see"
                    defaultMessage="보기"
                  />
                </Link>
              </div>
              <div>
                <input type="checkbox" id="promotion" 
                  onClick={isPromotionChckBoxClicked}
                  ref={promotionChckBox}/>
                <label htmlFor="promotion">
                  <FormattedMessage
                    id="promotion.mail.agree.optional"
                    defaultMessage="프로모션 안내 메일 수신(선택)"
                  />
                </label>
              </div>
              {/*  className="active"를 이용해 버튼 활성화 */}
              <button className="submitBtn active" type="button" onClick={goCRUD}>
                <FormattedMessage
                  id="join.wecruit"
                  defaultMessage="가입하기"
                />
              </button>
            </div>
            <div className="alertBox">
              <span>
                <span className="material-icons">error</span>
                  <FormattedMessage
                    id="description.require.agreement"
                    defaultMessage="이용을 위해서는 이용약관, 개인정보 수집 및 이용에 모두 동의하셔야 합니다."
                  />
              </span>
              <span>
                <FormattedMessage
                  id="description.channel.talk"
                  defaultMessage="가입과 관련된 기타 문의 사항은 우측 하단 채널톡을 이용해 문의 해주세요."
                />
              </span>
            </div>
          </form>
        </div>
      </div>
      
      <LoginFooter />
    </>

  );
}