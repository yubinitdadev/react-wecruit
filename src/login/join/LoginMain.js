import { useState, useEffect, Component, useRef, useContext } from "react";
import { Link,  useNavigate } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  getAuth
} from "firebase/auth";

import { LoginFooter } from "../common/LoginFooter";
import { LoginHeader } from "../common/LoginHeader";
import styles from "../css/login.module.css";
import employerJoin from "../images/btn_join1.jpg";
import recruitJoin from "../images/btn_join2.jpg";
import Modal from "../modal/Modal";
import { FindId } from "../modal/modals/FindId";
import { FindPw } from "../modal/modals/FindPw";
import { CheckIsEmpty, axiosCrudOnSubmit } from "commons/modules/commonUtil";
import { Context } from "contexts";
import { UPD_MBRUID } from "contexts/actionTypes";
import { FormattedMessage, useIntl } from "react-intl";

export const Login = ({ login }) => {
  const intl = useIntl();
  const navigate = useNavigate();
  
  const [state, setState] = useState({
    mbrDivi: "MBRDIVI-02",
    srchEmail: "",
    srchNm: "",
    srchId: "",
    srchCelpNum: "",
    idSrchRes: "",
    pswdSrchRes: ""
  })

  const {
    state : {
        mbr,
        server
    },
    dispatch,
  } = useContext(Context);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const [procChk, setProcChk] = useState("FIRSTPAGELOAD");
  const [modalVisible1, setModalVisible1] = useState(false);
  const [modalVisible2, setModalVisible2] = useState(false);

  const idRef = useRef(null);
  const pswdRef = useRef(null);
  const srchEmailRef = useRef(null);
  const srchNmRef = useRef(null);
  const srchIdRef = useRef(null);
  const srchCelpNumRef = useRef(null);

  const openModal1 = () => {
    setProcChk("IDSRCH");
    setModalVisible1(true);
  };

  const openModal2 = () => {
    setProcChk("PSWDSRCH");
    setModalVisible2(true);
  };

  const closeModal1 = () => {
    setModalVisible1(false);
    setState({
      ...state,
      srchEmail:"",
      srchNm:"",
      idSrchRes:""
    });
    
    srchEmailRef.current.value = "";
    srchNmRef.current.value = "";
  };

  const closeModal2 = () => {
    setModalVisible2(false);
    setState({
      ...state,
      srchId:"",
      srchCelpNum:"",
      pswdSrchRes:""
    });
    
    srchIdRef.current.value = "";
    srchCelpNumRef.current.value = "";
  };

  const auth = getAuth();

  // Auth();

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  const onKeyUp =(e) => {
    if(e.key === "Enter") loginHandle();   
    return false;
  }

  const jsonCompResult = ( resJson ) => {
    if( resJson.resCd !== "0000" )
    {
      return;
    }
    else
    {
      if ( procChk === "IDSRCH")
      {
        if (resJson.mbrId === "NONE" || resJson.mbrId === "" || resJson.mbrId === null || resJson.mbrId === undefined)
        {
          setState({
            ...state,
            idSrchRes: intl.formatMessage({
              id: "error.during.save",
              defaultMessage: "입력한 정보와 일치하는 계정을 찾지 못했습니다."
            })
          })
        }
        else
        {
          /* 아이디 찾기 성공 결과 */
          setState({
            ...state,
            idSrchRes: resJson.mbrId
          })
        }
        
        return;
      }
      else if( procChk === "PSWDSRCH")
      {
        if(resJson.pswd === "NONE" || resJson.pswd === "" || resJson.pswd === null || resJson.pswd === undefined)
        {
          setState({
            ...state,
            pswdSrchRes: "입력한 정보와 일치하는 계정을 찾지 못했습니다."
          })
        }
        else
        {
          /* 비밀번호 찾기 성공 결과 */
          setState({
            ...state,
            pswdSrchRes: resJson.pswd
          })
        }
        
        return;
      }
      else 
      {
        if(resJson.mbrUid === "NOMBR")
        {
          toast.error(intl.formatMessage({
            id: "error.check.id.password",
            defaultMessage: "아이디/패스워드를 다시 확인해 주세요."
          }));
          return;
        }
        
        window.localStorage.setItem("ssnMbrUid", resJson.mbrUid);
        window.localStorage.setItem("ssnMbrDivi", resJson.mbrDivi);
        window.localStorage.setItem("ssnRefUid", resJson.refUid);
        window.localStorage.setItem("ssnBizNm", resJson.bizNm);
        window.localStorage.setItem("ssnMbrNm", resJson.mbrNm);

        dispatch({
          type: UPD_MBRUID,
          payload: {
            ssnMbrUid : resJson.mbrUid,
            ssnMbrDivi : resJson.mbrDivi,
            ssnRefUid : resJson.refUid,
            ssnBizNm : resJson.bizNm,
            ssnMbrNm : resJson.mbrNm
          },
        });

        if(resJson.mbrDivi === "MBRDIVI-99") {
          toast.success("Logged In!");
          navigate("/admin");
        } else if (resJson.mbrDivi === "MBRDIVI-01") {
          toast.success("Logged In!");
          navigate("/employers");
        } else if (resJson.mbrDivi === "MBRDIVI-02") {
          toast.success("Logged In!");
          navigate("/recruiters");
        } else if (resJson.mbrDivi === "MBRDIVI-03") {
          toast.success("Logged In!");
          navigate("/employers");
        }
      }
    }
  }

  async function srchHandle(e) {
    let goUrl = "/mbr/srch";
    let axiosRes = await axiosCrudOnSubmit(state, goUrl, "POST");

    jsonCompResult(axiosRes.data);
  }

  async function loginHandle(e) {
    //e.preventDefault();

    if( CheckIsEmpty(idRef, 
      intl.formatMessage({
        id: "error.need.input.id",
        defaultMessage: "아이디를 입력해주세요."
      })) === false ) return;
    if( CheckIsEmpty(pswdRef, 
      intl.formatMessage({
        id: "error.need.input.password",
        defaultMessage: "비밀번호를 입력해주세요."
      })) === false ) return;

    let goUrl = server.path + "/mbr/login";
    let data = {
      mbrId : email,
      pswd : password
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    jsonCompResult(axiosRes.data);

    // 파이어베이스 통신을 통한 로그인
    /*
    signInWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        setUser(auth.currentUser);
        login();
        if (email === "hunters@wecruitcorp.com") {
          toast.success("Logged In!");
          navigate("/admin");
        } else if (email === "recruiter@wecruitcorp.com") {
          toast.success("Logged In!");
          navigate("/recruiters");
        } else if (email === "employer@wecruitcorp.com") {
          toast.success("Logged In!");
          navigate("/employers");
        }
        setEmail("");
        setPassword("");
      })
      .catch((err) => {
        switch (err.code) {
          case "auth/invalid-email":
          case "auth/user-disabled":
          case "auth/user-not-found":
            toast.error("Invalid credentials");
            // setEmailError(err.message);
            break;
          case "auth/wrong-password":
            toast.error("Invalid credentials");
            break;
        }
      });
    */
  };

  return (
    <>
      <LoginHeader />
      <ToastContainer />
      <div className={styles.loginPage}>
        <h1>Welcome to Wecruit</h1>
        <div className={styles.loginWrap}>
          <form onSubmit={loginHandle}>
            <label htmlFor="userId" />
            <input
              type="text"
              id="userId"
              placeholder={intl.formatMessage({
                id: "id",
                defaultMessage: "아이디"
              })}
              required
              value={email}
              ref={idRef}
              onChange={(e) => {
                setEmail(e.target.value);
              }}

              onKeyUp={(e) => {
                onKeyUp(e);
              }}
            />

            <label htmlFor="password" />
            <input
              type="password"
              id="userPw"
              placeholder={intl.formatMessage({
                id: "pw",
                defaultMessage: "비밀번호"
              })}
              required
              value={password}
              ref={pswdRef}
              onChange={(e) => setPassword(e.target.value)}

              onKeyUp={(e) => {
                onKeyUp(e);
              }}
            />

            {/* REST API 서버를 통해 로그인 할 시 해당 태그를 사용할 것 */}
            <button className={`"submitBtn" "active" ${styles.loginButton}`} type="button" onClick={loginHandle}>Login</button>
            
            {
              /* 파이어베이스 통신을 통해 로그인 할 시 해당 태그를 사용할 것
              <input style={{ cursor: "pointer" }} type="submit" value="Login" /> 
              */
            }
          </form>
          <div className={styles.findAccount}>
            <button type="button" onClick={openModal1}>
              ID<FormattedMessage
                id="find"
                defaultMessage="찾기"
              />
            </button>
            {
              <Modal
                visible={modalVisible1}
                closable={true}
                maskClosable={true}
                onClose={closeModal1}
                inputName={intl.formatMessage({
                  id: "find.id",
                  defaultMessage: "아이디 찾기"
                })}
                onSubmit={srchHandle}
              >
                <FindId state={state} setState={setState} srchEmailRef={srchEmailRef} srchNmRef={srchNmRef}/>
              </Modal>
            }
            <button type="button" onClick={openModal2}>
              <FormattedMessage
                id="find.pw"
                defaultMessage="비밀번호 찾기"
              />
            </button>
            {
              <Modal
                visible={modalVisible2}
                closable={true}
                maskClosable={true}
                onClose={closeModal2}
                inputName={intl.formatMessage({
                  id: "find.pw",
                  defaultMessage: "비밀번호 찾기"
                })}
                onSubmit={srchHandle}
              >
              <FindPw state={state} setState={setState} srchIdRef={srchIdRef} srchCelpNumRef={srchCelpNumRef}/>
              </Modal>
            }
          </div>
          <div className={styles.linkToJoin}>
            <FormattedMessage
              id="if.not.user.yet"
              defaultMessage="아직 회원이 아니시라면?"
            />
            <Link to="/join-employer">
              <FormattedMessage
                id="join"
                defaultMessage="회원가입"
              />
            </Link>
          </div>
          <div className={styles.joinPages}>
            <Link to="/join-employer">
              <img src={employerJoin} alt="인사담당자" />
            </Link>

            <Link to="/join-recruiter">
              <img src={recruitJoin} alt="리크루터" />
            </Link>
          </div>
        </div>
      </div>
      <LoginFooter />
    </>
  );
};
