import ReactDOM from 'react-dom';
import PropTypes from 'prop-types'
import styled from 'styled-components'

const Modal = ({ onClose, maskClosable, closable, visible, inputName, closeName, children, nobutton, onSubmit }) => {
  const onMaskClick = (e) => {
    if (e.target === e.currentTarget) {
      onClose(e)
      
    }
  }

  const close = (e) => {
    if (onClose) {
      onClose(e)
      
    }
  }

  // 모달 오버레이에서 스크롤 방지
  // useEffect(() => {
  //   document.body.style.cssText = `
  //     position: fixed; 
  //     top: -${window.scrollY}px;
  //     overflow-y: scroll;
  //     width: 100%;`;
  //   return () => {
  //     const scrollY = document.body.style.top;
  //     document.body.style.cssText = '';
  //     window.scrollTo(0, parseInt(scrollY || '0', 10) * -1);
  //   };
  // }, []);
  
  return ReactDOM.createPortal(
    <>
      <ModalOverlay visible={visible} />
      <ModalWrapper  className="recommendModal" onClick={maskClosable ? onMaskClick : null} tabIndex="-1" visible={visible}>
        <ModalInner className="recommendModalWrap"  tabIndex="0">
          {children}
          { nobutton ? null : <div className="buttons">
            <button type="button" className="submitBtn active" onClick={onSubmit}>{inputName}</button>
            {closable && <button type="button" className="modalCloseBtn" onClick={close}>{closeName ? closeName : '닫기'}</button>}
          </div>}
        </ModalInner>
      </ModalWrapper>
    </>,
    document.getElementById("modal")
  )
}

Modal.propTypes = {
  visible: PropTypes.bool,
}

Modal.defaultProps = {
  closable: true,
  maskClosable: true,
  visible: false
}

const ModalWrapper = styled.div`
  display: ${(props) => (props.visible ? 'flex' : 'none')};
  align-items: center;
  justify-content:center;
  box-sizing: border-box;
  position: fixed;
  z-index: 9999;
  background-color: rgba(0, 0, 0, 0.3);
  inset: 0;
  width: 100vw;
  min-height: 100vh;
  height: 100%;
  align-items: center;
  justify-content: center;
`

const ModalOverlay = styled.div`
  box-sizing: border-box;
  display: ${(props) => (props.visible ? 'flex' : 'none')};
  position: fixed;
  inset: 0;
  background-color: rgba(0, 0, 0, 0.3);
  z-index: 999;
`

const ModalInner = styled.div`
  position: relative;
  background-color: white;
  min-width: 20vw;
  width: fit-content;
  max-height: 80vh;
  padding: 2rem;
  z-index: 9999;
  overflow-y: auto;
`

export default Modal