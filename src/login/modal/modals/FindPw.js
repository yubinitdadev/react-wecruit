import { FormattedMessage, useIntl } from "react-intl";

export const FindPw =(props) => {
  const intl = useIntl();

  return(
    <>
      <h4>
        <FormattedMessage
          id="find.pw"
          defaultMessage="비밀번호 찾기"
        />
      </h4>
      <form className="loginModalForm">
        <input type="text" 
          placeholder={intl.formatMessage({
            id: "placeholder.id.when.joined",
            defaultMessage: "가입하신 아이디를 입력해주세요"
          })} 
          ref={props.srchIdRef}
          onChange={(e)=>{
            props.setState({
              ...props.state,
              srchId: e.target.value
            })
          }}/>
        <div>
          <input type="text" 
            placeholder={intl.formatMessage({
              id: "placeholder.phone.only.number",
              defaultMessage: "휴대폰 번호를 숫자만 입력해주세요" 
            })} 
            ref={props.srchCelpNumRef}
            onChange={(e)=>{
              props.setState({
                ...props.state,
                srchCelpNum: e.target.value
              })
            }}/>
          <button type="button">
            <FormattedMessage
              id="phone.verification"
              defaultMessage="휴대폰 인증"
            /> &gt;
          </button>
        </div>
        <div>
          <input type="text" placeholder="인증번호를 입력해주세요" readOnly/>
          <button type="button">
            <FormattedMessage
              id="verify"
              defaultMessage="인증받기"
            /> &gt;
          </button>
        </div>
        <div>
          {props.state.pswdSrchRes}
        </div>
      </form>
    </>
  )
}