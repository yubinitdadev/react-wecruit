import { FormattedMessage, useIntl } from "react-intl";

export const FindId =(props) => {
  const intl = useIntl();

  return(
    <>
      <h4>
        <FormattedMessage
          id="find.id"
          defaultMessage="아이디 찾기"
        />
      </h4>
      <form className="loginModalForm">
        <select onChange={ (e)=> {
          props.setState({
            ...props.state,
            mbrDivi: e.target.value
          })
        }}>
          <option value="MBRDIVI-02">
            {intl.formatMessage({
              id: "recruiter",
              defaultMessage: "리크루터"
            })}
          </option>
          <option value="MBRDIVI-01">
            {intl.formatMessage({
              id: "employer",
              defaultMessage: "인사담당자"
            })}
          </option>
        </select>
        <input type="text" 
          placeholder={intl.formatMessage({
            id: "placeholder.email.when.joined",
            defaultMessage: "가입하신 이메일을 입력해주세요"
          })} 
          ref={props.srchEmailRef}
          onChange={(e)=>{
            props.setState({
              ...props.state,
              srchEmail: e.target.value
            });
        }}/>
        <input type="text" 
          placeholder={intl.formatMessage({
            id: "placeholder.name.when.joined",
            defaultMessage: "가입하신 성함을 입력해주세요"
          })} 
          ref={props.srchNmRef}
          onChange={(e)=>{
            props.setState({
              ...props.state,
              srchNm: e.target.value
            });
        }}/>
        <div>
          {props.state.idSrchRes}
        </div>
      </form>
    </>
  )
}