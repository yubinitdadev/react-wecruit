import { Link, useLocation, useNavigate } from "react-router-dom";
import { useContext, useEffect, useState } from "react";
import { Context } from "contexts";
import queryString from "query-string";
import * as commonUtil from "commons/modules/commonUtil";
import * as defVal from "commons/modules/defVal";
import { FormattedMessage, useIntl } from "react-intl";

export const WorkContract = () => {
  const intl = useIntl();

  let PROC_CHK = "";

  const {
    state: { server, mbr },
    dispatch,
  } = useContext(Context);

  const location = useLocation();
  const navigate = useNavigate();
  const moment = require("moment");

  const [linkPath, setLinkPath] = useState("");
  const [detailState, setDetailState] = useState(defVal.setAxiosJobState(null));
  const [cmmntState, setCmmntState] = useState({});
  const [recmmdState, setRecmmdState] = useState(
    defVal.setAxiosCnddtRcmmdState(null)
  );

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid"),
    ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi"),
  };

  if (ssn.ssnMbrUid == "" || ssn.ssnMbrDivi != "MBRDIVI-02") {
    alert(
      intl.formatMessage({
        id: "wrong.access",
        defaultMessage: "정상적인 접속이 아닙니다.",
      })
    );
    navigate("/");
  }

  const params = queryString.parse(location.search);

  useEffect(async () => {
    console.log("Page First Load EFFECT");

    initSetHandle();
  }, []);

  async function initSetHandle() {
    let goUrl = server.path + "/hire/hiredetail";
    let data = {
      ssnMbrUid: ssn.ssnMbrUid,
      ssnMbrDivi: ssn.ssnMbrDivi,
      hireUid: params.hireUid,
    };

    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");
    let tagstr = axiosRes.data.tags;
    var tags = defVal.SplitToArray(tagstr, "<;:>");
    var rgstDttm = axiosRes.data.rgstDttm + "";
    axiosRes.data.rgstDttm = commonUtil.substr(rgstDttm, 0, 10);
    var rcmndEndDttm = axiosRes.data.rcmndEndDttm + "";
    axiosRes.data.rcmndEndDttm = commonUtil.substr(rcmndEndDttm, 0, 19);

    axiosRes.data.tags = tags;
    setDetailState(axiosRes.data);

    var tmpRcmmd = await getCnddtRcmmnd();
    var tmpCmmnt = await getCmmnt();
  }

  const getCmmnt = async () => {
    let goUrl = server.path + "/hire/cmmntdetail";
    let data = {
      hireUid: params.hireUid,
      commentUid: params.commentUid,
    };

    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");
    let tmpDate = axiosRes.data.resInfo;
    var rgstDttm = tmpDate.rgstDttm + "";
    tmpDate.rgstDttm = commonUtil.substr(rgstDttm, 0, 16);
    var joinDttm = tmpDate.joinDttm + "";
    tmpDate.joinDttm = commonUtil.substr(joinDttm, 0, 16);
    setCmmntState(tmpDate);
  };

  const getCnddtRcmmnd = async () => {
    let goUrl = server.path + "/hire/cnddtrecmmnd";
    let data = {
      hireUid: params.hireUid,
      rcmndUid: params.rcmndUid,
    };

    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");
    let tmpDate = defVal.setAxiosCnddtRcmmdState(axiosRes.data.resInfo);

    if (tmpDate.rgstDttm != "" && tmpDate.rgstDttm != "N")
      tmpDate.rgstDttm = moment(tmpDate.rgstDttm).format("YYYY-MM-DD HH:mm");
    else tmpDate.rgstDttm = "";

    //console.log("tmpDate.rcmndStatDttm =========== " + tmpDate.rcmndStatDttm);

    if (tmpDate.rcmndStatDttm != "" && tmpDate.rcmndStatDttm != "N")
      tmpDate.rcmndStatDttm = moment(tmpDate.rcmndStatDttm).format(
        "YYYY-MM-DD HH:mm"
      );
    else tmpDate.rcmndStatDttm = "";

    var tmpCelp = tmpDate.cnddtCelpNum.split("-");
    try {
      tmpDate.cnddtCelpF = tmpCelp[0];
      tmpDate.cnddtCelpM = tmpCelp[1];
      tmpDate.cnddtCelpL = tmpCelp[2];
    } catch (e) {}

    let atchFilesStr = tmpDate.cnddtAtchFiles;
    if (atchFilesStr != "") {
      var atchFiles = JSON.parse(atchFilesStr);
      for (var k = 0; k < atchFiles.length; k++) {
        if (atchFiles[k].filePos == 1) {
          tmpDate.cnddtThmnlNm = atchFiles[k].orignlFileNm;
        }
        tmpDate.cnddtAtchFilesStr[atchFiles[k].filePos] = JSON.stringify(
          atchFiles[k]
        );
      }
    }

    setRecmmdState(tmpDate);
    return tmpDate;
  };

  return (
    <div
      id="wrap2"
      style={{
        fontFamily: "Nanum Gothic",
        fontSize: "12px",
        border: "0px solid #000000",
        padding: "20px",
      }}
    >
      <div
        id="right"
        style={{
          width: "878px",
          border: "1px #d7d7d7 solid",
          backgroundColor: "#ffffff",
          padding: "30px",
          marginBottom: "50px",
          margin: "0 auto",
        }}
      >
        <div id="mailbox4">
          <div id="mailbox14">
            <input type="hidden" className="contract_id" value="6" />
            <input type="hidden" className="type" value="1" />
            <p
              className="tx_h02"
              style={{
                textAlign: "center",
                fontSize: "20px",
                fontWeight: "bold",
                color: "#000000",
                padding: "15px 0",
                marginBottom: "10px",
                lineHeight: "24px",
                borderBottom: "1px #b7b7b7 solid",
              }}
            >
              <FormattedMessage
                id="recruitment.entrust.service.contract"
                defaultMessage="인재채용 업무위탁 용역 계약서"
              />
            </p>
            <p
              className="tx_h03"
              style={{
                fontSize: "13px",
                color: "#000000",
                lineHeight: "22px",
                fontWeight: "bold",
              }}
            >
              <span className="tx_b01 header_company" style={{ color: "#09C" }}>
                {detailState.bizNm}
              </span>
              (이하 &quot;위탁자&quot;라고 한다)와{" "}
              <span
                className="tx_b01 header_recruiter"
                style={{ color: "#09C" }}
              >
                {cmmntState.mbrNm}
              </span>
              <FormattedMessage
                id="contract.intro"
                defaultMessage='(이하 "수탁자"라고 한다)은/는 ㈜위크루트(커리어크레딧)을
                통해 아래와 같이 인재채용 업무위탁 용역 계약을 체결합니다.'
              />
            </p>
            <br />
            <FormattedMessage
              id="work.commitment.subject"
              defaultMessage="업무위탁 대상"
            />
            <br />
            <table
              width="100%"
              border="0"
              cellspacing="0"
              cellpadding="0"
              className="tbox"
              style={{
                borderTop: "solid 1px #e6e6e6",
                borderRight: "solid 1px #e6e6e6",
                margin: "0px",
              }}
            >
              <tr>
                <th
                  style={{
                    backgroundColor: "#E4F1F3",
                    padding: "5px",
                    textAlign: "center",
                    fontSize: "10px",
                    borderBottom: "solid 1px #e6e6e6",
                    width: "85px",
                  }}
                >
                  <FormattedMessage
                    id="corporate.name"
                    defaultMessage="기업명"
                  />
                </th>
                <td
                  colspan="3"
                  className="rt_company_name"
                  style={{
                    backgroundColor: "#fff",
                    textAlign: "left",
                    padding: "5px",
                    borderBottom: "solid 1px #e6e6e6",
                    width: "150px",
                  }}
                >
                  {detailState.bizNm}
                </td>
              </tr>
              <tr>
                <th
                  style={{
                    backgroundColor: "#E4F1F3",
                    padding: "5px",
                    textAlign: "center",
                    fontSize: "10px",
                    borderBottom: "solid 1px #e6e6e6",
                    width: "85px",
                  }}
                >
                  <FormattedMessage
                    id="recruitment.title"
                    defaultMessage="채용공고제목"
                  />
                </th>
                <td
                  colspan="3"
                  className="rt_title"
                  style={{
                    backgroundColor: "#fff",
                    textAlign: "left",
                    padding: "5px",
                    borderBottom: "solid 1px #e6e6e6",
                    width: "150px",
                  }}
                >
                  {detailState.title}
                </td>
              </tr>
              <tr>
                <th
                  style={{
                    backgroundColor: "#E4F1F3",
                    padding: "5px",
                    textAlign: "center",
                    fontSize: "10px",
                    borderBottom: "solid 1px #e6e6e6",
                    width: "85px",
                  }}
                >
                  <FormattedMessage
                    id="recruitment.registration.date"
                    defaultMessage="채용공고 등록일"
                  />
                </th>
                <td
                  className="rt_start_date"
                  style={{
                    backgroundColor: "#fff",
                    textAlign: "left",
                    padding: "5px",
                    borderBottom: "solid 1px #e6e6e6",
                    width: "150px",
                  }}
                >
                  {detailState.rgstDttm}
                </td>
                <th
                  style={{
                    backgroundColor: "#E4F1F3",
                    padding: "5px",
                    textAlign: "center",
                    fontSize: "10px",
                    borderBottom: "solid 1px #e6e6e6",
                    width: "85px",
                  }}
                >
                  <FormattedMessage
                    id="recruitment.end.date"
                    defaultMessage="채용공고 마감일"
                  />
                </th>
                <td
                  className="rt_end_date"
                  style={{
                    backgroundColor: "#fff",
                    textAlign: "left",
                    padding: "5px",
                    borderBottom: "solid 1px #e6e6e6",
                    width: "150px",
                  }}
                >
                  {detailState.rcmndEndDttm}
                </td>
              </tr>
            </table>
            <div className="candidateBlock" style={{ display: "none" }}>
              <br />
              <span className="candidateTitle">
                <FormattedMessage
                  id="recruitment.subject"
                  defaultMessage="인재채용 대상자"
                />
              </span>
              <br />
              <table
                width="100%"
                border="0"
                cellspacing="0"
                cellpadding="0"
                className="tbox candidateTable"
                style={{
                  borderTop: "solid 1px #e6e6e6",
                  borderRight: "solid 1px #e6e6e6",
                  margin: "0px",
                }}
              >
                <tr>
                  <th
                    style={{
                      backgroundColor: "#E4F1F3",
                      padding: "5px",
                      textAlign: "center",
                      fontSize: "10px",
                      borderBottom: "solid 1px #e6e6e6",
                      width: "85px",
                    }}
                  >
                    <FormattedMessage
                      id="candidate.name"
                      defaultMessage="후보자 이름"
                    />
                  </th>
                  <td
                    className="rt_candidate_name"
                    style={{
                      backgroundColor: "#fff",
                      textAlign: "left",
                      padding: "5px",
                      borderBottom: "solid 1px #e6e6e6",
                      width: "150px",
                    }}
                  >
                    {recmmdState.cnddtNm}
                  </td>
                  <th
                    style={{
                      backgroundColor: "#E4F1F3",
                      padding: "5px",
                      textAlign: "center",
                      fontSize: "10px",
                      borderBottom: "solid 1px #e6e6e6",
                      width: "85px",
                    }}
                  >
                    <FormattedMessage
                      id="recommend.date"
                      defaultMessage="추천일시"
                    />
                  </th>
                  <td
                    className="rt_candidate_offer_date"
                    style={{
                      backgroundColor: "#fff",
                      textAlign: "left",
                      padding: "5px",
                      borderBottom: "solid 1px #e6e6e6",
                      width: "150px",
                    }}
                  >
                    {recmmdState.rgstDttm}
                  </td>
                </tr>
                <tr>
                  <th
                    style={{
                      backgroundColor: "#E4F1F3",
                      padding: "5px",
                      textAlign: "center",
                      fontSize: "10px",
                      borderBottom: "solid 1px #e6e6e6",
                      width: "85px",
                    }}
                  >
                    <FormattedMessage
                      id="final.recruitment.decision.date"
                      defaultMessage="최종 채용 확정 일시"
                    />
                  </th>
                  <td
                    className="rt_candidate_confirm_date"
                    style={{
                      backgroundColor: "#fff",
                      textAlign: "left",
                      padding: "5px",
                      borderBottom: "solid 1px #e6e6e6",
                      width: "150px",
                    }}
                  >
                    {recmmdState.rcmndStatDttm}
                  </td>
                  <th
                    style={{
                      backgroundColor: "#E4F1F3",
                      padding: "5px",
                      textAlign: "center",
                      fontSize: "10px",
                      borderBottom: "solid 1px #e6e6e6",
                      width: "85px",
                    }}
                  >
                    <FormattedMessage id="note" defaultMessage="비고" />
                  </th>
                  <td
                    className="rt_candidate_phone"
                    style={{
                      backgroundColor: "#fff",
                      textAlign: "left",
                      padding: "5px",
                      borderBottom: "solid 1px #e6e6e6",
                      width: "150px",
                    }}
                  ></td>
                </tr>
              </table>
            </div>
            <br />
            <FormattedMessage
              id="recruitment.commitment.suggest.condition"
              defaultMessage="인재채용 업무위탁 제안조건"
            />
            <br />
            <table
              width="100%"
              border="0"
              cellspacing="0"
              cellpadding="0"
              className="tbox"
              style={{
                borderTop: "solid 1px #e6e6e6",
                borderRight: "solid 1px #e6e6e6",
                margin: "0px",
              }}
            >
              <tr>
                <th
                  style={{
                    backgroundColor: "#E4F1F3",
                    padding: "5px",
                    textAlign: "center",
                    fontSize: "10px",
                    borderBottom: "solid 1px #e6e6e6",
                    width: "85px",
                  }}
                >
                  <FormattedMessage id="commission" defaultMessage="수수료" />
                </th>
                <td
                  className="rt_commission"
                  style={{
                    backgroundColor: "#fff",
                    textAlign: "left",
                    padding: "5px",
                    borderBottom: "solid 1px #e6e6e6",
                    width: "150px",
                  }}
                >
                  <FormattedMessage
                    id="from.contract.salary"
                    defaultMessage="계약연봉의"
                  />{" "}
                  {cmmntState.srvcFee}%<br />
                  (
                  <FormattedMessage
                    id="vat.excluded"
                    defaultMessage="부가세 별도"
                  />
                  )
                </td>
                <th
                  style={{
                    backgroundColor: "#E4F1F3",
                    padding: "5px",
                    textAlign: "center",
                    fontSize: "10px",
                    borderBottom: "solid 1px #e6e6e6",
                    width: "85px",
                  }}
                >
                  <FormattedMessage
                    id="suggest.date"
                    defaultMessage="제안일시"
                  />
                </th>
                <td
                  className="rt_offer_date"
                  style={{
                    backgroundColor: "#fff",
                    textAlign: "left",
                    padding: "5px",
                    borderBottom: "solid 1px #e6e6e6",
                    width: "150px",
                  }}
                >
                  {cmmntState.rgstDttm}
                </td>
              </tr>
              <tr>
                <th
                  style={{
                    backgroundColor: "#E4F1F3",
                    padding: "5px",
                    textAlign: "center",
                    fontSize: "10px",
                    borderBottom: "solid 1px #e6e6e6",
                    width: "85px",
                  }}
                >
                  <FormattedMessage
                    id="guarantee.term"
                    defaultMessage="보증기간"
                  />
                </th>
                <td
                  className="rt_guarantee_periods"
                  style={{
                    backgroundColor: "#fff",
                    textAlign: "left",
                    padding: "5px",
                    borderBottom: "solid 1px #e6e6e6",
                    width: "150px",
                  }}
                >
                  {detailState.srvcWrtyPeriod}
                  <FormattedMessage
                    id="incase.retire.before.month"
                    defaultMessage="개월 이내 퇴직시"
                  />
                </td>
                <th
                  style={{
                    backgroundColor: "#E4F1F3",
                    padding: "5px",
                    textAlign: "center",
                    fontSize: "10px",
                    borderBottom: "solid 1px #e6e6e6",
                    width: "85px",
                  }}
                >
                  <FormattedMessage
                    id="guarantee.method"
                    defaultMessage="보증방식"
                  />
                </th>
                <td
                  className="rt_guarantee_methods"
                  style={{
                    backgroundColor: "#fff",
                    textAlign: "left",
                    padding: "5px",
                    borderBottom: "solid 1px #e6e6e6",
                    width: "150px",
                  }}
                >
                  {detailState.srvcWrtyMthd === "prorated"
                    ? intl.formatMessage({
                        id: "wrong.access",
                        defaultMessage: "일할계산 환불",
                      })
                    : detailState.srvcWrtyMthd === "all"
                    ? intl.formatMessage({
                        id: "suggestion.caculated.refund",
                        defaultMessage: "100% 전액 환불",
                      })
                    : detailState.srvcWrtyMthd === "etc"
                    ? intl.formatMessage({
                        id: "etc",
                        defaultMessage: "기타",
                      })
                    : ""}
                </td>
              </tr>
            </table>
            <br />
            <br />
            <FormattedMessage
              id="recruiter.info"
              defaultMessage="리크루터 정보"
            />
            <br />
            <table
              width="100%"
              border="0"
              cellspacing="0"
              cellpadding="0"
              className="tbox"
              style={{
                borderTop: "solid 1px #e6e6e6",
                borderRight: "solid 1px #e6e6e6",
                margin: "0px",
              }}
            >
              <tr>
                <th
                  style={{
                    backgroundColor: "#E4F1F3",
                    padding: "5px",
                    textAlign: "center",
                    fontSize: "10px",
                    borderBottom: "solid 1px #e6e6e6",
                    width: "85px",
                  }}
                >
                  <FormattedMessage id="name" defaultMessage="이름" />
                </th>
                <td
                  className="rt_recruiter_name"
                  style={{
                    backgroundColor: "#fff",
                    textAlign: "left",
                    padding: "5px",
                    borderBottom: "solid 1px #e6e6e6",
                    width: "150px",
                  }}
                >
                  {cmmntState.mbrNm}
                </td>
                <th
                  style={{
                    backgroundColor: "#E4F1F3",
                    padding: "5px",
                    textAlign: "center",
                    fontSize: "10px",
                    borderBottom: "solid 1px #e6e6e6",
                    width: "85px",
                  }}
                >
                  <FormattedMessage id="email" defaultMessage="이메일" />
                </th>
                <td
                  className="rt_recruiter_email"
                  style={{
                    backgroundColor: "#fff",
                    textAlign: "left",
                    padding: "5px",
                    borderBottom: "solid 1px #e6e6e6",
                    width: "150px",
                  }}
                >
                  {cmmntState.email}
                </td>
              </tr>
              <tr>
                <th
                  style={{
                    backgroundColor: "#E4F1F3",
                    padding: "5px",
                    textAlign: "center",
                    fontSize: "10px",
                    borderBottom: "solid 1px #e6e6e6",
                    width: "85px",
                  }}
                >
                  <FormattedMessage id="phone" defaultMessage="휴대폰" />
                </th>
                <td
                  className="rt_recruiter_phone"
                  style={{
                    backgroundColor: "#fff",
                    textAlign: "left",
                    padding: "5px",
                    borderBottom: "solid 1px #e6e6e6",
                    width: "150px",
                  }}
                >
                  {cmmntState.celpNum}
                </td>
                <th
                  style={{
                    backgroundColor: "#E4F1F3",
                    padding: "5px",
                    textAlign: "center",
                    fontSize: "10px",
                    borderBottom: "solid 1px #e6e6e6",
                    width: "85px",
                  }}
                >
                  <FormattedMessage
                    id="certification.date"
                    defaultMessage="인증일시"
                  />
                </th>
                <td
                  className="rt_recruiter_confirm_date"
                  style={{
                    backgroundColor: "#fff",
                    textAlign: "left",
                    padding: "5px",
                    borderBottom: "solid 1px #e6e6e6",
                    width: "150px",
                  }}
                >
                  {cmmntState.joinDttm}
                </td>
              </tr>
            </table>
            <br />
            <FormattedMessage
              id="cost.payment.bankbook"
              defaultMessage="비용결제통장"
            />
            <br />
            <table
              width="100%"
              border="0"
              cellspacing="0"
              cellpadding="0"
              className="tbox"
              style={{
                borderTop: "solid 1px #e6e6e6",
                borderRight: "solid 1px #e6e6e6",
                margin: "0px",
              }}
            >
              <tr>
                <th
                  style={{
                    backgroundColor: "#E4F1F3",
                    padding: "5px",
                    textAlign: "center",
                    fontSize: "10px",
                    borderBottom: "solid 1px #e6e6e6",
                    width: "85px",
                  }}
                >
                  <FormattedMessage
                    id="deposit.account"
                    defaultMessage="입금통장"
                  />
                </th>
                <td
                  style={{
                    backgroundColor: "#fff",
                    textAlign: "left",
                    padding: "5px",
                    borderBottom: "solid 1px #e6e6e6",
                    width: "150px",
                  }}
                >
                  <FormattedMessage
                    id="wecruit.bank.account"
                    defaultMessage="우리은행 1005-302-644462 (주)위크루트"
                  />
                </td>
              </tr>
            </table>
            <br />
            <FormattedMessage id="other.condition" defaultMessage="기타조건" />
            <br />
            <p
              className="tboxtd2"
              style={{
                backgroundColor: "#efefef",
                padding: "10px",
                textAlign: "left",
                fontSize: "12px",
                border: "solid 1px #e6e6e6",
              }}
            >
              <FormattedMessage
                id="other.condition1"
                defaultMessage="- (주)위크루트와 위탁자, 수탁자간 계약 내용은 기본 약관을 기준으로 합니다."
              />{" "}
              <br />
              <FormattedMessage
                id="other.condition2"
                defaultMessage="- 본 인재채용 업무위탁 용역 계약서는 상기에 명시된 업무위탁 대상 및 인재채용 대상자 채용추천 건에 한하여 유효합니다."
              />
              <br />
              <FormattedMessage
                id="other.condition3"
                defaultMessage="- 위탁자와 수탁자는 서비스를 이용함에 있어 신의성실의 원칙에 따르도록 합니다."
              />
              <br />
              <FormattedMessage
                id="other.condition4"
                defaultMessage="- ㈜위크루트(커리어크레딧) 약관 및 본 계약내용에 규정하지 않은 사항은 일반 상관례에 따라 이해관계자의 합의로 결정을 합니다."
              />
              <br />
            </p>
            <br />
            <p style={{ textAlign: "center" }}>
              <FormattedMessage id="contract.date" defaultMessage="계약일시" />{" "}
              : <span className="tr_contract_date">{cmmntState.rgstDttm}</span>
            </p>
            <hr
              noshade="noshade"
              style={{ border: "1px #e6e6e6 solid", margin: "10px 0" }}
            />
            <p style={{ textAlign: "right" }}>
              <FormattedMessage id="truster" defaultMessage="위탁자" /> :{" "}
              <span className="footer_company_name">{detailState.bizNm}</span>
              <br />
              <FormattedMessage id="agent" defaultMessage="중개자" /> :
              <FormattedMessage
                id="wecruit.career.credit"
                defaultMessage="㈜위크루트(커리어크레딧)"
              />
              <br />
              <FormattedMessage id="trustee" defaultMessage="수탁자" /> :{" "}
              <span className="footer_recruiter_name">{cmmntState.mbrNm}</span>
            </p>
            <hr
              noshade="noshade"
              style={{ border: "1px #e6e6e6 solid", margin: "10px 0" }}
            />
            <div
              id="btnclose"
              style={{
                width: "100%",
                paddingTop: "20px",
                textAlign: "center",
              }}
            >
              <Link to="/">
                <span
                  className="btn_close"
                  onclick="movingMain()"
                  style={{
                    cursor: "pointer",
                    backgroundColor: "#ffffff",
                    color: "#555555",
                    fontSize: "12px",
                    padding: "4px 10px",
                    border: "1px #b7b7b7 solid",
                  }}
                >
                  <FormattedMessage
                    id="go.to.home"
                    defaultMessage="홈으로 이동"
                  />
                </span>
              </Link>
            </div>
            <br />
          </div>
        </div>
      </div>
    </div>
  );
};
