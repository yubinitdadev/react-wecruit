const { defineMessages } = require("react-intl");

export const messages = defineMessages ({
  deadline: {
    id: 'recommend.deadline.error',
    defaultMessage: '인재추천 마감일을 금일({today}) 이후로 입력해 주세요.',
  },
  existingSearchfirm: {
    id: 'existing.searchfirm.free',
    defaultMessage: '<strong>기존에 거래하던 써치펌</strong>은 <strong>무료</strong>로 <strong>위크루트 시스템을 이용</strong>하실 수 있습니다.'
  },
  minCharacter: {
    id: 'error.min.character',
    defaultMessage: '비밀번호는 {minLength}자 이상이어야 합니다.',
  },
  maxCharacter: {
    id: 'error.max.character',
    defaultMessage: '추천서를 {maxLength}자 이내로 입력해주세요.',
  }
})
