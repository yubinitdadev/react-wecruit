import { FormattedMessage } from "react-intl";

export const AccessInfo =() => {
  return(  
    <>
      <h4>
        <FormattedMessage 
          id="department.id.access.check"
          defaultMessage="현업부서 아이디 권한정보 확인"
        />
      </h4>
      <table>
        <thead>
          <tr align="center">
            <td colSpan={2}>
              <FormattedMessage 
                id="accessible.function"
                defaultMessage="접근 가능 기능"
              />
            </td>
            <td>
              <FormattedMessage 
                id="employer"
                defaultMessage="인사담당자"
              />
            </td>
            <td>
              <FormattedMessage 
                id="department"
                defaultMessage="현업부서"
              />
            </td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td rowSpan={5} align="center">
              <FormattedMessage 
                id="department"
                defaultMessage="리크루터 관리"
              />
            </td>
            <td>
              <FormattedMessage 
                id="recruiter.profile.check"
                defaultMessage="리크루터 프로필 조회"
              />
            </td>
            <td>O</td>
            <td>O</td>
          </tr>
          <tr>
            <td>
              <FormattedMessage 
                id="recruiter.send.memo"
                defaultMessage="리크루터 쪽지보내기"
              />
            </td>
            <td>O</td>
            <td>O</td>
          </tr>
          <tr className="highlighted">
            <td>
              <FormattedMessage 
                id="recruiter.choose"
                defaultMessage="리크루터 선택하기"
              />
            </td>
            <td>O</td>
            <td className="red">X</td>
          </tr>
          <tr>
            <td>
              <FormattedMessage 
                id="recruiter.evaluate"
                defaultMessage="리크루터 평가하기"
              />
            </td>
            <td>O</td>
            <td>O</td>
          </tr>
          <tr className="highlighted">
            <td>
              <FormattedMessage 
                id="exsting.searchfirm.submit"
                defaultMessage="기존 거래 써치펌 등록하기"
              />
            </td>
            <td>O</td>
            <td className="red">X</td>
          </tr>
        </tbody>
        <tbody>
          <tr className="highlighted">
            <td rowSpan={3}>
              <FormattedMessage 
                id="recruitment.posting.manage"
                defaultMessage="채용공고 관리"
              />
            </td>
            <td>
              <FormattedMessage 
                id="recommendation.finish"
                defaultMessage="인재추천 마감하기"
              />
            </td>
            <td>O</td>
            <td className="red">X</td>
          </tr>
          <tr className="highlighted">
            <td>
              <FormattedMessage 
                id="recruitment.finish"
                defaultMessage="채용공고 종료하기"
              />
            </td>
            <td>O</td>
            <td className="red">X</td>
          </tr>
          <tr>
            <td>
              Q&amp;A <FormattedMessage 
                id="use.board"
                defaultMessage="게시판 사용"
              />
            </td>
            <td>O</td>
            <td>O</td>
          </tr>
        </tbody>
        <tbody>
          <tr>
            <td rowSpan={7}>
              <FormattedMessage 
                id="candidate.manage"
                defaultMessage="후보자 관리"
              />
            </td>
            <td>
              <FormattedMessage 
                id="resume.download"
                defaultMessage="이력서 다운로드"
              />
            </td>
            <td>O</td>
            <td>O</td>
          </tr>
          <tr className="highlighted">
            <td>
              <FormattedMessage 
                id="candidate.contact.open.request"
                defaultMessage="후보자 연락처 공개 요청"
              />
            </td>
            <td>O</td>
            <td className="red">X</td>
          </tr>
          <tr>
            <td>
              <FormattedMessage 
                id="candidate.memo.manage"
                defaultMessage="후보자 메모 관리"
              />
            </td>
            <td>O</td>
            <td>O</td>
          </tr>
          <tr>
            <td>
              <FormattedMessage 
                id="recruitment.step.setting"
                defaultMessage="채용단계 설정"
              />
            </td>
            <td>O</td>
            <td>O</td>
          </tr>
          <tr className="highlighted">
            <td>
              <FormattedMessage 
                id="recruitment.step.change.alert"
                defaultMessage="채용단계 변경 결과통보"
              />
            </td>
            <td>O</td>
            <td className="red">X</td>
          </tr>
          <tr className="highlighted">
            <td>
              <FormattedMessage 
                id="final.recruit.confirm"
                defaultMessage="최종채용확정"
              />  
            </td>
            <td>O</td>
            <td className="red">X</td>
          </tr>
          <tr className="highlighted">
            <td>
              <FormattedMessage 
                id="after.final.recruit.contract"
                defaultMessage="최종채용확정 후 계약서보기"
              />  
            </td>
            <td>O</td>
            <td className="red">X</td>
          </tr>
        </tbody>
      </table>
    </>
  );
}