import { FormattedMessage } from "react-intl"

export const AddSearchFirm =() => {
  return(
    <>
      <h4>
        <FormattedMessage
          id="exsting.searchfirm.submit"
          defaultMessage="기존 거래 써치펌 등록하기"
        />
      </h4>
      <div className="alertBox">
        <FormattedMessage
          id="description.headhunter"
          values = {{
            breakLine: <br/>
          }}
          defaultMessage="마음에 드는 헤드헌터를 {breakLine}기존 거래 써치펌으로 자유롭게 등록하세요!"
        />
      </div>
      <ul>
        <li>
          <FormattedMessage
            id="description.searchfirm1"
            defaultMessage="※ 채용공고에서 기존 거래 써치펌으로 리크루터를 등록하시면, 별도의 중개 수수료 없이 해당 리크루터(헤드헌터)와 거래하실 수 있습니다."
          />
        </li>
        <li>
          <FormattedMessage
            id="description.searchfirm4"
            defaultMessage="최대 30명까지 기존 거래 써치펌으로 등록할 수 있습니다."
          />
        </li>
        <li>
          <FormattedMessage
            id="description.searchfirm3"
            defaultMessage="단, 공동거래의 안전을 위해 별도의 <보증보험료>는 상호 협의하여 써치펌(리크루터)쪽에 일부 발생할 수도 있습니다."
          />
        </li>
        <p className="flexAlignCenter marginTopBottom">
          [<FormattedMessage
            id="inqury"
            defaultMessage="문의"
          />] 031-548-2310, 2311 / hunters@wecruitcorp.com
        </p>
      </ul>
      
    </>
  )
}