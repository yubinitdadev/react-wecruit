import { FormattedMessage } from "react-intl";

export const RejectComment =() => {
  return(
    <>
      <h4>
        <FormattedMessage
          id="approve.reject"
          defaultMessage="승인 거절"
        />
      </h4>
      <p>
        <FormattedMessage
          id="description.reject.reason"
          defaultMessage="거절사유 (인사담당자 화면에 그대로 노출됩니다.)"
        />
      </p>
      <textarea cols={30} rows={10}/>
    </>
  );
}