import { FormattedMessage } from "react-intl"

export const CandidateContact =() => {
  return(
    <>
      <h4>
        <FormattedMessage
          id="candidate.contact.open.request"
          defaultMessage="후보자 연락처 공개 요청"
        />
      </h4>
      <div className="alertBox">
        <FormattedMessage
          id="candidate"
          defaultMessage="후보자"
        /> : 김아무개
      </div>
      <p className="textAlignCenter">
        <FormattedMessage
          id="request.candidate.contact1"
          defaultMessage="면접을 위해 담당 리크루터에게 후보자의 휴대폰번호와 이메일주소공개를 요청합니다."
        />
      </p>
      <h5>
        [ <FormattedMessage
            id="confidential.consent.form.title"
            defaultMessage="영업비밀 보호를 위한 동의서"
          /> ]
      </h5>

      <div className="checkboxWrap">
        <input type="checkbox" id="contactAgree1"/>
        <label className="width500" htmlFor="contactAgree1">
          <FormattedMessage
            id="request.candidate.contact2"
            defaultMessage="(필수) 리크루터의 영업비밀 보호를 위해 연락처가 공개되는 경우 공개일로부터 향후 1년 동안 본 채용건 이외의 건으로 후보자를 채용하지 않겠습니다."
          />
        </label>
      </div>
      
      <div className="checkboxWrap">
        <input type="checkbox" id="contactAgree2"/>
        <label className="width500" htmlFor="contactAgree2">
          <FormattedMessage
            id="request.candidate.contact3"
            defaultMessage="(필수) 해당 기간내 후보자를 채용하는 경우에는 소정의 리크루팅 수수료를 지불하겠습니다."
          />
        </label>
      </div>

      <p className="textAlignCenter">
        <FormattedMessage
          id="agree"
          defaultMessage="이에 동의합니다."
        />
      </p>
    </>
  )
}