import { axiosCrudOnSubmit } from "commons/modules/commonUtil";
import { Context } from "contexts";
import { useContext, useEffect, useState } from "react";
import { FormattedMessage } from "react-intl";

export const AddressList =(props) => {
  const {
    state : {
        server,
        mbr
    },
    dispatch,
  } = useContext(Context);

  const [srchfirms, setSrchfirms] = useState([]);
  const [checkedSrchfirms, setCheckedSrchfirms] = useState([]);
  const [sConts, setSConts] = useState("");
  const [fltrState, setFltrState] = useState({});


  const initSetHandle = async(data) => {
    let goUrl = server.path + "/srchfirm/list"
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    setSrchfirms(axiosRes.data.srchfirmArr);

    /*
    let tmpBtnList = {
      ...buttonList
    };

    axiosRes.data.srchfirmArr.map((srchfirm,idx)=> {
      buttonList['button' + idx] = false;
    })

    setButtonList(tmpBtnList);
    */
  }

  async function goSrch() {
    let data = {
      mbrUid : props.mbrUid,
      mbrDivi : "MBRDIVI-01",
      sConts : sConts,
      sFltr : fltrState,
      sFltrCate : Object.keys(fltrState)[0],
      sFltrCont : Object.values(fltrState)[0],
    }

    initSetHandle(data);
  }

  const changeFltr = (fltr) => {
    let tmpFltr = (fltr === "ASC") ? "DESC" :
                  (fltr === "DESC") ? "ASC" :
                  "ASC";

    return tmpFltr;
  }

  useEffect( () => {
    goSrch();
  }, [fltrState]);

  useEffect( async () => {
    let data = {
      mbrUid : props.mbrUid,
      mbrDivi : "MBRDIVI-01"
    }

    initSetHandle(data);
  },[props.mbrUid])

  useEffect( () => {
    console.log(srchfirms);
  },[srchfirms])

  return(
    <div>
      <h4>
        <FormattedMessage
          id="exsting.searchfirm.address"
          defaultMessage="기존 거래 써치펌 주소록"
        />
      </h4>
      <p>
        <FormattedMessage
          id="description.searchfirm1"
          defaultMessage="※ 채용공고에서 기존 거래 써치펌으로 리크루터를 등록하시면, 별도의 중개 수수료 없이 해당 리크루터(헤드헌터)와 거래하실 수 있습니다."
        />
      <br/>
        <FormattedMessage
          id="description.searchfirm2"
          defaultMessage="(단, 계약내용에 따라 거래의 안전을 위한 별도의 보증보험료는 상호 협의하에 리크루터에게 발생할 수도 있습니다.)"
        />
      </p>
      <table>
        <thead>
          <tr>
            <th>
              <span>
                <input type="checkbox"
                  checked={(checkedSrchfirms.length === srchfirms.length) ? true : false}
                  onClick={(e)=>{
                    let tmpList = [];
                    let tmpEmailList = [];

                    if(e.target.checked){
                      srchfirms.map((srchfirm, idx)=>{
                        tmpList.push(srchfirm.srchfirmUid);
                        tmpEmailList.push(srchfirm.email);
                      })
                    } 

                    setCheckedSrchfirms(tmpList);
                    props.setSrchfirmEmails(tmpEmailList);
                  }}/>
              </span>
            </th>
            <th>
              <span>
                <FormattedMessage
                  id="number"
                  defaultMessage="번호"
                />
              </span>
            </th>
            <th  className={ fltrState.recNm === "DESC" ? "descending" :
                            fltrState.recNm === "ASC" ? "ascending" :
                            undefined } 
              onClick={() => {
                setFltrState({
                  recNm : changeFltr(fltrState.recNm)
                }); 
              }}>
              <span>
                <FormattedMessage
                  id="recruiter.name"
                  defaultMessage="리크루터 이름"
                />
              </span>
              <div className="filterArrow">
                <span />
                <span />
              </div>
            </th>
            <th  className={ fltrState.cmpnyNm === "DESC" ? "descending" :
                            fltrState.cmpnyNm === "ASC" ? "ascending" :
                            undefined } 
              onClick={() => {
                setFltrState({
                  cmpnyNm : changeFltr(fltrState.cmpnyNm)
                }); 
              }}>
              <span>
                <FormattedMessage
                  id="searchfirm.assigned"
                  defaultMessage="소속 써치펌"
                />
              </span>
              <div className="filterArrow">
                <span />
                <span />
              </div>
            </th>
            <th  className={ fltrState.email === "DESC" ? "descending" :
                            fltrState.email === "ASC" ? "ascending" :
                            undefined } 
              onClick={() => {
                setFltrState({
                  email : changeFltr(fltrState.email)
                }); 
              }}>
              <span>
                <FormattedMessage
                  id="email"
                  defaultMessage="이메일"
                />
              </span>
              <div className="filterArrow">
                <span />
                <span />
              </div>
            </th>
            <th className={ fltrState.celpNum === "DESC" ? "descending" :
                            fltrState.celpNum === "ASC" ? "ascending" :
                            undefined } 
              onClick={() => {
                setFltrState({
                  celpNum : changeFltr(fltrState.celpNum)
                }); 
              }}>
              <span>
                <FormattedMessage
                  id="phone"
                  defaultMessage="휴대폰"
                />
              </span>
              <div className="filterArrow">
                <span />
                <span />
              </div>
            </th>
          </tr>
        </thead>
        <tbody>
          {
            srchfirms.map((srchfirm, idx)=>{
              return(
                <tr key={srchfirm.srchfirmUid}>
                  <td>
                    <input type="checkbox" 
                      checked={(checkedSrchfirms.indexOf(srchfirm.srchfirmUid) !== -1) ? true : false}
                      onClick={(e)=>{
                        if(checkedSrchfirms.indexOf(srchfirm.srchfirmUid) !== -1)
                        {
                          setCheckedSrchfirms( checkedSrchfirms.filter( (srchfirmUid) => srchfirmUid !== srchfirm.srchfirmUid) );
                          
                          props.setSrchfirmEmails(
                            props.srchfirmEmails.filter( (email) => email !== srchfirm.email )
                          );
                        } 
                        else 
                        {
                          setCheckedSrchfirms( checkedSrchfirms.concat( srchfirm.srchfirmUid ) );
                          
                          props.setSrchfirmEmails(
                            props.srchfirmEmails.concat(srchfirm.email)
                          );
                        }
                      }}
                    /></td>
                  <td>{idx+1}</td>
                  <td>{srchfirm.recNm}</td>
                  <td>{srchfirm.cmpnyNm}</td>
                  <td>{srchfirm.email}</td>
                  <td>{srchfirm.celpNum}</td>
                </tr>
              )
            })
          }
        </tbody>
      </table>
    </div>
  )
}