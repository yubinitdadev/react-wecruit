import { TagTemplate } from "commons/components/tags/TagTemplate";
import DoubleModal from "modal-all/DoubleModal";
import { useEffect, useState } from "react";
import { FormattedMessage, useIntl } from "react-intl";
import { AddressList } from "./AddressList";

export const RequestMail = (props) => {
  const intl = useIntl()
  //const [detailState, setDetailState] = useState(props.detailState);
  //const [listState, setListState] = useState(props.listState);
  
  const [modalVisible1, setModalVisible1] = useState(false);
  const [tagLength, setTagLength] = useState(0)
  const [tagCnt, setTagCnt] = useState(0);
  const [srchfirmEmails, setSrchfirmEmails] = useState([]);


  useEffect(async () => {
    console.log("Page First Load EFFECT");    
    initSetHandle();          
  },[]);

  const initSetHandle = async() => {
    
  }

  const openModal1 = () => {
    setModalVisible1(true)
  }

  const closeModal1 = () => {
    setModalVisible1(false)
  }

  return(
  <>
  <h4>
    <FormattedMessage
      id="attend.mail.existing"
      defaultMessage="기존 써치펌에 인재추천 참여 요청하기"
    />
  </h4>
  <div className="infoBox">
    <FormattedMessage
      id="attend.mail.description"
      defaultMessage="본 메일은 채용공고 검수가 완료되면 검수된 내용으로 발송됩니다."
    />
    </div>
    <p className="mailContent">
      <FormattedMessage
        id="hello"
        defaultMessage="안녕하세요?"
      /> <FormattedMessage
        id="dear.recruiter"
        defaultMessage="리크루터님,"
      /> <br />
      &#60;{props.state.title}&#62;
      <FormattedMessage
        id="description.recommend.request1"
        defaultMessage="에 인재추천 참여 바랍니다."
      /> <br />
      <FormattedMessage
        id="description.recommend.request2"
        defaultMessage="위크루트 시스템에 로그인하시면 참여신청 및 인재추천이 가능합니다."
      />
    </p>
    <big>
      <FormattedMessage
        id="email.address.input"
        defaultMessage="이메일 주소 입력"
      /> (<FormattedMessage
          id="description.recommend.request3"
          defaultMessage="숨은 참조로 발송됩니다."
        /> {tagLength} / 30<FormattedMessage
          id="count"
          defaultMessage="개"
        />)
    </big>
    <form>
      {
        /*
        <TagTemplate state={props.state} setState={props.setState} tagCnt={tagCnt} setTagCnt={setTagCnt} tagLength={tagLength} setTagLength={setTagLength} tagNum={tagNum} type="email" target={"emails"}/>
        */
      }
    </form>
    
    <ul>
      <li>
        <strong>기존에 거래하던 써치펌</strong>은 <strong>무료</strong>로 <strong>위크루트 시스템을 이용</strong>하실 수 있습니다.
      </li>
      <li>
        <FormattedMessage
          id="description.searchfirm2"
          defaultMessage="(단, 계약내용에 따라 거래의 안전을 위한 별도의 보증보험료는 상호 협의하에 리크루터에게 발생할 수도 있습니다.)"
        />
      </li>
      <li>
        리크루터분들은 <strong>수신한 이메일로 회원 가입을 한 경우에 한하여 무료</strong>로
        이용하실 수 있습니다.
      </li>
    </ul>
    <div className="buttons">
      
      
      <button type="button" className="greyColoredBtn" onClick={openModal1}>
        <FormattedMessage
          id="get.address"
          defaultMessage="주소록 불러오기"
        />
      </button>
      {
        /*onClose, maskClosable, closable, visible, inputName, closeName, children, nobutton, buttonAction*/
        <DoubleModal 
          onClose={closeModal1} 
          maskClosable={true} 
          closable={true}  
          visible={modalVisible1}  
          inputName={intl.formatMessage({
            id: "get",
            defaultMessage: "불러오기"
          })}
          closeName={intl.formatMessage({
            id: "delete",
            defaultMessage: "삭제하기"
          })}
        >
          <AddressList mbrUid={props.state.mbrUid} searchFirmList={props.searchFirmList}/>
        </DoubleModal>
        
      }

      <button type="button" onClick={props.onClose} >
        <FormattedMessage
          id="recruitment.join.request"
          defaultMessage="채용공고 참여 요청하기"
        />
      </button>
    </div>
  </>
  )
}
