import { FormattedMessage } from "react-intl"

export const LeaveWecruit =(props) => {
  return(
    <>
      <h4>
        <FormattedMessage
          id="wecruit.leave"
          defaultMessage="위크루트 탈퇴하기"
        />
      </h4>
      <div className="alertBox">{props.state.mbrNm}</div>
      <p className="flexAlignCenter textAlignCenter">
        <FormattedMessage
          id="description.wecruit.leave"
          values = {{
            breakLine: <br/>
          }}
          defaultMessage="개인정보 보호를 위해 모든 정보는 삭제될 {breakLine}예정이며, 언제든지 다시 가입하실 수 있습니다.{breakLine} 정말 탈퇴하시겠습니까?"
        />
      </p>
    </>
  )
}