import { FormattedMessage } from "react-intl";

export const MatchingPosting =() => {
  return(
    <>
      <h4>
        <FormattedMessage
          id="department.matching.toggle"
          defaultMessage="현업부서 매칭/취소"
        />
      </h4>
      <div className="matchingList">
        <div>
          <input type="radio" name="departmentItem" />
          <label>test1</label>
        </div>
        <div>
          <input type="radio" name="departmentItem" />
          <label>test2</label>
        </div>
      </div>
    </>
  );
}