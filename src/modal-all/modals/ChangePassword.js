import { FormattedMessage } from "react-intl"

export const ChangePassword =() => {
  return(
    <>
      <h4>
        <FormattedMessage 
          id="pw.change"
          defaultMessage="비밀번호 변경하기"
        />
      </h4>
      <p className="textAlignCenter">
        <FormattedMessage
          id="description.pw.change"
          values = {{
            breakLine: <br/>
          }}
          defaultMessage="개인정보 보호를 위해 주기적으로{breakLine} 비밀번호를 변경하시는 게 좋습니다.{breakLine} 정말 변경하시겠습니까?"
          />
      </p>
    </>
  )
}