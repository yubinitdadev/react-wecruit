import { FormattedMessage } from "react-intl"

export const SelectRecruiter =() => {
  return(
    <>
      <h4>
        <FormattedMessage
          id="recruiter.choose"
          defaultMessage="리크루터 선택하기"
        />
      </h4>
      <div className="alertBox">
        정성미 <FormattedMessage
        id="ask.contract.recruiter"
        values = {{
          breakLine: <br/>
        }}
        defaultMessage="리크루터와 <br/> 계약체결을 하시겠습니까?"
      />
      </div>
      <p className="textAlignCenter">
        <FormattedMessage
          id="description.recommend.available"
          values = {{
            breakLine: <br/>
          }}
          defaultMessage="계약이 체결되면 <br/> 바로 인재를 추천받으실 수 있습니다."
        />
      </p>
    </>
  )
}