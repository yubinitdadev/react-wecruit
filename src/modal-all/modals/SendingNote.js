import { useState } from "react"
import { FormattedMessage } from "react-intl";

export const SendingNote =() => {
  const [textLength, setTextLength] = useState(0);
  const onChangeText = (e) => {
    setTextLength(e.target.value.length)
  }
  return(
    <>
      <h4>
        <FormattedMessage
          id="recruiter.send.memo"
          defaultMessage="리크루터에게 쪽지보내기"
        />
      </h4>
      <div className="alertBox">
        김재윤
      </div>
      <p className="marginTopBottom">
        <FormattedMessage
          id="description.only.reveal.to.recruiter"
          values = {{
            breakLine: <br/>
          }}
          defaultMessage= "문의 내용은 Q&amp;A 게시판에 작성되며 {breakLine} 작성내용은 해당 리크루터만 확인 가능합니다."
        />
      </p>
      <form className="employersForm">
        <textarea name="qsCntnt" id="qsCntnt" cols={100} rows={10} maxLength="100" onChange={onChangeText}/>
        <span className="flexAlignRight">{textLength}/100</span>
      </form>
    </>
  )
}