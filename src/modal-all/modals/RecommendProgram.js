import { FormattedMessage } from "react-intl";

export const RecommendProgram =() => {
  return(
    <>
      <h4>
        <FormattedMessage
          id="program.recommend.recruiter"
          defaultMessage="우수 리크루터 추천 프로그램"
        />
      </h4>
      <b>
        <FormattedMessage
          id="introducing"
          defaultMessage="소개"
        />
      </b>
      <p>
        위크루트가 추천하는 리크루터는 <strong>매달 한 명 이상의 성공률</strong>을 보여주신 상위 1%의 리크루터분들로 선정되었습니다.
      </p>
      <b>
        수수료
      </b>
      <ul>
        <li>
          <strong>최소값과 최대값의 평균값</strong>을 헤드헌팅 수수료로 합니다.
        </li>
        <li>
          <FormattedMessage
            id="description.headhunting.fee"
            defaultMessage="단, 최소 헤드헌팅 수수료는 15% (부가세 별도)이상으로 합니다."
          />
        </li>
      </ul>
      <div className="infoBox">
        우수 리크루터를 추천받아 프로젝트를 진행하고자 하시는<br />
        인사담당자님께서는 <strong>"예, 동의합니다."</strong> 항목을 체크해 주시기 바랍니다.
      </div>
    </>
  );
}