import { FormattedMessage } from "react-intl";

export const DeleteId =(props) => {
  let mbrIds = "";
  
  props.chckboxList.forEach((mbrId, idx) => {
    mbrIds += mbrId;

    if(idx+1 !== props.chckboxList.length) mbrIds += ",";
  })
  
  return(
    <>
      <h4>
        <FormattedMessage
          id="remove.id.confirm"
          defaultMessage="아이디 삭제 확인"
        />
      </h4>
      {/*
        <div className="alertBox">
          아이디: {mbrIds}
        </div>
      */}
      <p className="textAlignCenter">
        <FormattedMessage
          id="remove.id.ask"
          defaultMessage="삭제하시겠습니까?"
        />
      </p>
    </>
  )
}