import { FormattedMessage } from "react-intl"

export const ManageCr =() => {
  return(
    <>
      <div className="sectionHeader">
        <h3>
          <span className="material-icons-outlined">
            settings
          </span>
          <b>
            <FormattedMessage
              id="cr.manage"
              defaultMessage="CR 관리"
            />
          </b>
        </h3>

        <p>
          [(주)위크루트] 거래 고객사의 채용공고 제목
          인사 님
        </p>
      </div>
      <div className="searchBar">
        <label htmlFor="search">Search:</label>
        <input type="search" id="search" />
        <button type="button">
          <FormattedMessage
            id="search"
            defaultMessage="검색"
          />
        </button>
      </div>

      <table className="crTable">
        <thead>
          <tr>
            <th>
              <span>
                <FormattedMessage
                  id="number"
                  defaultMessage="번호"
                />
              </span>
              <div className="filterArrow">
                <span />
                <span />
              </div>
            </th>
            <th>
              <span>
                <FormattedMessage
                  id="comment"
                  defaultMessage="코멘트"
                />
              </span>
              <div className="filterArrow">
                <span />
                <span />
              </div>
            </th>
            <th>
              <span>
                <FormattedMessage
                  id="writing.date"
                  defaultMessage="작성일시"
                />
              </span>
              <div className="filterArrow">
                <span />
                <span />
              </div>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr className="noData" style={{display: 'none'}}>
            <th colSpan={9}>
              <FormattedMessage
                id="description.no.cr"
                defaultMessage="CR이 존재하지 않습니다."
              />
            </th>
          </tr>
          <tr>
            <td>1</td>
            <td>
              코멘트가 들어가는 곳
            </td>
            <td>2021-21-12 14:25:12</td>
          </tr>
        </tbody>
      </table>

      <div className="crCommentInput">
        <label htmlFor="comment">
          <FormattedMessage
            id="comment"
            defaultMessage="코멘트"
          />
        </label>
        <input type="text" id="comment" placeholder="코멘트를 입력해주세요." />
      </div>
    </>
  )
}