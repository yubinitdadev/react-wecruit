import { FormattedMessage } from "react-intl"

export const ClosingRecruit =() => {
  return(
    <>
      <h4>
        <FormattedMessage
          id="recruiter.recruiting.done"
          defaultMessage="리크루터 모집마감"
        />
      </h4>
      <div className="alertBox">
        <FormattedMessage
          id="recruiter.recruiting.done.ask"
          defaultMessage="리크루터 모집을 마감하시겠습니까?"
        />
      </div>
      <p className="textAlignCenter">
        <FormattedMessage
          id="recruiter.recruiting.done.description"
          defaultMessage="선택된 리크루터와 함께 {breakLine} 성공적인 인재채용을 기원합니다!"
        />
      </p>
    </>
  )
}