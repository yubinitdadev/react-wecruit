import { axiosCrudOnSubmit, CheckIsEmpty } from "commons/modules/commonUtil";
import { Context } from "contexts";
import { forwardRef, useContext, useRef } from "react";
import { FormattedMessage, useIntl } from "react-intl";
import { toast, ToastContainer } from "react-toastify";

export const EditDepartment = forwardRef( (props, ref) => {
  const intl = useIntl();
  const {
    state : {
        mbr,
        server
    },
    dispatch,
  } = useContext(Context);

  async function mbrDupl() {
    let goUrl = server.path + "/mbr/mbrDupl";
    let data = {
      mbrId : props.refObj.mbrIdRef.current.value
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    if(axiosRes.data.mbrUid === "DUPL")
    {
      toast.error(intl.formatMessage({
        id: "error.id.duplicated",
        defaultMessage: "이미 가입된 아이디입니다."
      }));
      return;
    } 
    else if (axiosRes.data.mbrUid === "NOMBR")
    {
      toast.info(intl.formatMessage({
        id: "info.usable.id",
        defaultMessage: "사용 가능한 아이디입니다."
      }));
      return
    }
  }

  return(
    <>
      <h4>
        <FormattedMessage
          id="department.id.edit"
          defaultMessage="현업부서 아이디 수정"
        />
      </h4>
      <p>
        <span className="red">*</span> <FormattedMessage
          id="required"
          defaultMessage="는 필수 항목입니다."
        />
      </p>
      <form className="employersForm">
        <div>
          <label htmlFor="departmentId">
            <span>*</span> <FormattedMessage
              id="id"
              defaultMessage="아이디"
            />
          </label>
          <div className="flex">
            <input type="text" id="departmentId" 
              placeholder={intl.formatMessage({
                id: "placeholder.id",
                defaultMessage: "아이디를 입력하세요."
              })}
              required 
              ref={props.refObj.mbrIdRef}
              value={props.state.mbrId} 
              onChange={(e)=>{
                props.setState({
                  ...props.state,
                  mbrId: e.target.value
                })
              }}/> 
              <button type="button" className="modalButtonStyling" 
                onClick={ 
                  ()=> {
                    if( CheckIsEmpty(props.refObj.mbrIdRef,
                      intl.formatMessage({
                        id: "error.need.input.id",
                        defaultMessage: "아이디를 입력해주세요."
                      })) === false ) return;
                    mbrDupl();
                }}>
                  <FormattedMessage
                    id="check.duplicate"
                    defaultMessage="중복 확인"
                  />
                </button>
          </div>
        </div>
        <div className="inputWidthFull">
          <label htmlFor="departmentPw">
            <span>*</span> <FormattedMessage
              id="pw"
              defaultMessage="비밀번호"
            />
          </label>
          <input type="password" id="departmentPw" 
            placeholder={intl.formatMessage({
              id: "placeholder.pw",
              defaultMessage: "비밀번호를 입력하세요."
            })}
            required
            ref={props.refObj.pswdRef} 
            value={props.state.pswd} 
            onChange={(e)=>{
              props.setState({
                ...props.state,
                pswd: e.target.value
              })
            }}/>
        </div>
        <div className="inputWidthFull">
          <label htmlFor="departmentRePw">
            <span>*</span> <FormattedMessage
              id="re.pw"
              defaultMessage="비밀번호 재확인"
            />
          </label>
          <input type="password" id="departmentRePw" 
            placeholder={intl.formatMessage({
              id: "placeholder.re.pw",
              defaultMessage: "비밀번호를 한번 더 입력해주세요."
            })}
            required
            ref={props.refObj.confirmPswdRef} 
            value={props.state.pswdConfirm} 
            onChange={(e)=>{
              props.setState({
                ...props.state,
                pswdConfirm: e.target.value
              })
            }}/>
        </div>
        <div>
          <label htmlFor="memo">
            <span>*</span> <FormattedMessage
              id="department.description.memo"
              defaultMessage="현업부서 설명 메모"
            />
          </label>
          <textarea name id cols={30} rows={10} 
            placeholder={intl.formatMessage({
              id: "department.description.memo.placeholder",
              defaultMessage: "예) 연구개발팀 홍길동 과장"
            })}
            itemID="memo"
            ref={props.refObj.introRef} 
            value={props.state.intro} 
            onChange={(e)=>{
              props.setState({
                ...props.state,
                intro: e.target.value
              })
            }}/>
        </div>
        <div>
          <label htmlFor="email">
            <span>*</span> <FormattedMessage
              id="email.address"
              defaultMessage="이메일주소"
            />
          </label>
          <input type="text" id="emailAddress"
            ref={props.refObj.emailAccountRef}
            value={(props.state.email)? props.state.email.split("@")[0] : ""} 
            onChange={(e)=>{
              props.setState({
                ...props.state,
                email: e.target.value + "@" + props.refObj.emailAddressRef.current.value
              })
            }}/> 
          @ 
          <input type="text" 
            ref={props.refObj.emailAddressRef}
            value={(props.state.email)?props.state.email.split("@")[1] : ""}/>
          <select name id="emailAddress" 
            value={(props.state.email)?props.state.email.split("@")[1] : ""}
            onChange={(e) => {
              props.setState({
                ...props.state,
                email: props.refObj.emailAccountRef.current.value + "@" + e.target.value
              })
            }}>
            <option value="">
              {intl.formatMessage({
                id: "directly.input",
                defaultMessage: "직접입력"
              })}
            </option>
            <option value="naver.com">naver.com</option>
            <option value="gmail.com">gmail.com</option>
            <option value="daum.com">daum.net</option>
            <option value="nate.com">nate.com</option>
            <option value="hanmail.com">hanmail.net</option>
            <option value="hotmail.com">hotmail.com</option>
          </select>
        </div>
        <div>
          <label htmlFor="phone">
            {intl.formatMessage({
              id: "phone.number",
              defaultMessage: "휴대폰번호"
            })}
          </label>
          <div className="flex">
            {/**
             * <select name id="phone">
              <option>010</option>
              <option>011</option>
              <option>016</option>
              <option>017</option>
              <option>018</option>
              <option>019</option>
            </select> - <input type="text" /> - <input type="text" />
             */}
            <input type="text" 
              value={props.state.celpNum}
              ref={props.refObj.celpNumRef}
              onChange={(e)=>{
                props.setState({
                  ...props.state,
                  celpNum: e.target.value
                })
              }}/>
          </div>
        </div>
      </form>
    </>
  );
})