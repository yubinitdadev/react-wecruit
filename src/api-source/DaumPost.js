import DaumPostCode from 'react-daum-postcode';

const DaumPost = (props) => {
  
  const handleComplete = (data) => {
    let fullAddress = data.address;
    let extraAddress = '';
    if (data.addressType === 'R') {
        if (data.bname !== '') {
            extraAddress += data.bname;
        }
        if (data.buildingName !== '') {
            extraAddress += (extraAddress !== '' ? `, ${data.buildingName}` : data.buildingName);
        }
        fullAddress += (extraAddress !== '' ? ` (${extraAddress})` : '');

        props.address(fullAddress);
        props.disabledControl(false);
    }
    props.onClose(true);
  }

  const addressPopupClose =() => {
    props.onClose(true);
  }

  return (
  <>
    <div className='alignRight'>
      <button type='button' onClick={addressPopupClose} className="basicButton" style={{ margin: '6px'}}>닫기</button>
    </div>
    <DaumPostCode onComplete={handleComplete} className="post-code" />
  </>
  );
}
export default DaumPost;