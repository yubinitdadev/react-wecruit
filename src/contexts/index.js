import { UPD_MBRUID } from "./actionTypes";

const { createContext, useReducer, useEffect } = require("react")

const initialState = {
    server : {
        path : "/api",
        //host : "http://localhost:9080",
        //mbrNoImg : "http://localhost:9080/upload/mbr/noman01.png"
        host : "http://13.125.230.176",   
        mbrNoImg : "http://13.125.230.176/upload/mbr/noman01.png"
    },
    mbr : {
        ssnMbrUid : "",
        mbrUid : "",
        mbrDivi: ""
    }
};

const Context = createContext({});

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case UPD_MBRUID : 
            return {
                ...state,
                mbr : {
                    ssnMbrUid : action.payload.mbrUid,
                    mbrUid: action.payload.mbrUid,
                    mbrDivi: action.payload.mbrDivi
                }
            }
        default:
            return state;
    }
};

const Provider = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialState);
    const value = { state, dispatch };
    return <Context.Provider value={value}>{ children }</Context.Provider>
};

export { Context, Provider };

