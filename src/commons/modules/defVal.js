import { useContext, useEffect, useState } from 'react';
import * as commonUtil from 'commons/modules/commonUtil';


export const FileState = () => {
    
    const state = {
        fileUploadCmplt: false,
        uploadFiles: "disable",
        filePosNm: "thmnl",
        orignlFileNm: "",
        thmnl: "",
        snglImgFileJsonStr: ""
    };
    
    return state;
}

export const setAxiosSsnState = (axiosData) => {
    
    const state = {
        mbrUid: ( axiosData ) ? axiosData.mbrUid : "",
        svcUid: ( axiosData ) ? axiosData.svcUid : "",
        mbrDivi: ( axiosData ) ?  axiosData.mbrDivi : "", 
        mbrId: ( axiosData ) ?  axiosData.mbrId : "",
        mbrNm: ( axiosData ) ?  axiosData.mbrNm : "",
        refUid: ( axiosData ) ?  axiosData.refUid : "",
        maxHire: ( axiosData ) ?  axiosData.maxHire : "0",
        maxCnddt: ( axiosData ) ?  axiosData.maxCnddt : "0",
        rmndCnddt: ( axiosData ) ?  axiosData.rmndCnddt : "0",
        recmmndChooseCnt: ( axiosData ) ?  axiosData.recmmndChooseCnt : "0",
        recmmndCnt: ( axiosData ) ?  axiosData.recmmndCnt : "0",
        cmmntChooseCnt: ( axiosData ) ?  axiosData.cmmntChooseCnt : "0",
        cmmntCnt: ( axiosData ) ?  axiosData.cmmntCnt : "0",
        replyCnt: ( axiosData ) ?  axiosData.replyCnt : "0",
        replyPoint: ( axiosData ) ?  axiosData.replyPoint : "0.0",
        
        
        bizNm : ( axiosData ) ?  axiosData.bizNm : "", 
        bizLogoThmnl : ( axiosData ) ?  axiosData.bizLogoThmnl : ""       
    };      
    
    return state;
}

export const setAxiosMbrCntState = (axiosData) => {
    
    const state = {
        totCnt: ( axiosData ) ? axiosData.totCnt : "0",
        empTotCnt: ( axiosData ) ? axiosData.empTotCnt : "0",
        recTotCnt: ( axiosData ) ?  axiosData.recTotCnt : "0", 
        dprtTotCnt: ( axiosData ) ?  axiosData.dprtTotCnt : "0"
      };      
    
    return state;
}


export const setAxiosMbrState = (axiosData) => {

    const state = {
        initSetCmplt: true,
        fileUploadCmplt: false,
        chkYn: "N",
        mbrUid: ( axiosData ) ? axiosData.mbrUid : "",
        svcUid: ( axiosData ) ? axiosData.svcUid : "",
        mbrDivi: ( axiosData ) ?  axiosData.mbrDivi : "", 
        mbrId: ( axiosData ) ?  axiosData.mbrId : "",
        mbrNm: ( axiosData ) ?  axiosData.mbrNm : "",
        pswd: ( axiosData ) ?  axiosData.pswd : "",
        brthd: ( axiosData ) ?  axiosData.brthd : "",
        gndr: ( axiosData ) ?  axiosData.gndr : "",
        celpNum: ( axiosData ) ?  axiosData.celpNum : "",
        phNum: ( axiosData ) ?  axiosData.phNum : "",
        email: ( axiosData ) ?  axiosData.email : "",
        emaiId: "",
        emailDom: "",
        emailDomSample : "",
        atchFiles: ( axiosData ) ?  axiosData.atchFiles : "",
        joinDttm: ( axiosData ) ?  axiosData.joinDttm : "",
        loginYn: ( axiosData ) ?  axiosData.loginYn : "",
        lastLoginDttm: ( axiosData ) ?  axiosData.lastLoginDttm : "",
        lastLogoutDttm: ( axiosData ) ?  axiosData.lastLogoutDttm : "",
        cnfrmYn: ( axiosData ) ?  axiosData.cnfrmYn : "",
        cnfrmDttm: ( axiosData ) ?  axiosData.cnfrmDttm : "",
        cnfrmCnclDttm: ( axiosData ) ?  axiosData.cnfrmCnclDttm : "",
        wthdrYn: ( axiosData ) ?  axiosData.wthdrYn : "",
        wthdrDttm: ( axiosData ) ?  axiosData.wthdrDttm : "",
        snsDivi: ( axiosData ) ?  axiosData.snsDivi : "",
        snsUid: ( axiosData ) ?  axiosData.snsUid : "",
        snsEml: ( axiosData ) ?  axiosData.snsEml : "",
        snsKey: ( axiosData ) ?  axiosData.snsKey : "",
        zipCd: ( axiosData ) ?  axiosData.zipCd : "",
        addr: ( axiosData ) ?  axiosData.addr : "",
        addrDtl: ( axiosData ) ?  axiosData.addrDtl : "",
        chldUid: ( axiosData ) ?  axiosData.chldUid : "",
        nickNm: ( axiosData ) ?  axiosData.nickNm : "",
        hmpg: ( axiosData ) ?  axiosData.hmpg : "",
        intro: ( axiosData ) ?  axiosData.intro : "",
        thmnl: ( axiosData ) ?  axiosData.thmnl : "",
        thmnlNm: ( axiosData ) ?  axiosData.thmnlNm : "",
        atchFiles: ( axiosData ) ?  axiosData.atchFiles : "",
        mbrPos: ( axiosData ) ?  axiosData.mbrPos : "",
        mbrGrd: ( axiosData ) ?  axiosData.mbrGrd : "",
        mbrGrp: ( axiosData ) ?  axiosData.mbrGrp : "",
        tags : [],
        tagStr : ( axiosData ) ? axiosData.tagStr : "",
        maxHire: ( axiosData ) ?  axiosData.maxHire : "",
        rgstDttm: ( axiosData ) ?  axiosData.rgstDttm : "",
        rgstrUid: ( axiosData ) ?  axiosData.rgstrUid : "",
        updtDttm: ( axiosData ) ?  axiosData.updtDttm : "",
        updtrUid: ( axiosData ) ?  axiosData.updtrUid : "",
        rcmndCd: ( axiosData ) ?  axiosData.rcmndCd : "",
        mailYn: ( axiosData ) ?  axiosData.mailYn : "",
        termYn: ( axiosData ) ?  axiosData.termYn : "",
        personalYn: ( axiosData ) ?  axiosData.personalYn : "",
        promoYn: ( axiosData ) ?  axiosData.promoYn : "",
        cmpnyUid: ( axiosData ) ?  axiosData.cmpnyUid : "",
        mbrLvl: ( axiosData ) ?  axiosData.mbrLvl : "",
        maxCnddt: ( axiosData ) ?  axiosData.maxCnddt : "",
        bankNm: ( axiosData ) ?  axiosData.bankNm : "",
        bankAccntNum: ( axiosData ) ?  axiosData.bankAccntNum : "",
        refUid: ( axiosData ) ?  axiosData.refUid : "",
        hireUid: ( axiosData ) ?  axiosData.hireUid : "",
        delYn: ( axiosData ) ?  axiosData.delYn : "",
        delDttm: ( axiosData ) ?  axiosData.delDttm : "",
        
        refMbrId: ( axiosData ) ?  axiosData.refMbrId : "",
        ssnMbrUid: "",

        bizNm : ( axiosData ) ?  axiosData.bizNm : "",
        ceoNm : ( axiosData ) ?  axiosData.ceoNm : "",
        bizNum : ( axiosData ) ?  axiosData.bizNum : "",
        bizCertThmnl : ( axiosData ) ?  axiosData.bizCertThmnl : "",
        bizCertThmnlNm : ( axiosData ) ?  axiosData.bizCertThmnlNm : "",
        saleSize : ( axiosData ) ?  axiosData.saleSize : "",
        empSize : ( axiosData ) ?  axiosData.empSize : "",
        bizSector : ( axiosData ) ?  axiosData.bizSector : "",
        bizHmpg : ( axiosData ) ?  axiosData.bizHmpg : "",
        bizPhNum: ( axiosData ) ?  axiosData.bizPhNum : "",
        bizLogoThmnl: ( axiosData ) ?  axiosData.bizLogoThmnl : "",
        bizLogoThmnlNm : ( axiosData ) ?  axiosData.bizLogoThmnlNm : "",
        bizAddr: ( axiosData ) ?  axiosData.bizAddr : "",
        bizAddrDtl: ( axiosData ) ?  axiosData.bizAddrDtl : "",

        extraThmnl: ( axiosData ) ?  axiosData.extraThmnl : "",
        extraThmnlNm: ( axiosData ) ?  axiosData.extraThmnlNm : "",

        tagCntnt : "",
        
        cmmntCnt: ( axiosData ) ?  axiosData.cmmntCnt : "0",
        hireIngCnt: ( axiosData ) ?  axiosData.hireIngCnt : "0",
        hireEndCnt: ( axiosData ) ?  axiosData.hireEndCnt : "0",
        cmmntChooseCnt: ( axiosData ) ?  axiosData.cmmntChooseCnt : "0",

        
        recmmndCnt: ( axiosData ) ?  axiosData.recmmndCnt : "0",
        recmmndChooseCnt: ( axiosData ) ?  axiosData.recmmndChooseCnt : "0",
        recmmndPaperCnt: ( axiosData ) ?  axiosData.recmmndPaperCnt : "0",
        recmmndFaceCnt: ( axiosData ) ?  axiosData.recmmndFaceCnt : "0",

        recmmndScssRate: ( axiosData ) ?  axiosData.recmmndScssRate : "0",
        recmmndPaperRate: ( axiosData ) ?  axiosData.recmmndPaperRate : "0",
        recmmndFaceRate: ( axiosData ) ?  axiosData.recmmndFaceRate : "0",
        recmmndCnfrmRate: ( axiosData ) ?  axiosData.recmmndCnfrmRate : "0",

        recmmndFrstDayAvgCnt: ( axiosData ) ?  axiosData.recmmndFrstDayAvgCnt : "0",
        recmmndDayAvgCnt: ( axiosData ) ?  axiosData.recmmndDayAvgCnt : "0",

        cateAvgCnt: ( axiosData ) ?  axiosData.cateAvgCnt : "0",
        

        replyPoint: ( axiosData ) ?  axiosData.replyPoint : "0",
        
        fileUploadCmplt: false,
        filePosNm: ["", "bizCertThmnl", "bizLogoThmnl", "", "", "", "", "", "", ""],
        uploadFiles: [ "disable", "disable", "disable", "disable", "disable", "disable", "disable", "disable", "disable", "disable" ],        
        extraUploadFiles: [ "disable", "disable", "disable", "disable", "disable", "disable", "disable", "disable", "disable", "disable" ],        
        snglImgFileJsonStr: ["NULL", "", "", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL"],
        
        mbrFilePosNm: ["", "thmnl", "", "", "", "", "", "", "", ""],
        mbrFileJsonStr: ["NULL", "", "", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL"],  
        bizFilePosNm: ["", "bizCertThmnl", "bizLogoThmnl", "", "", "", "", "", "", ""],      
        bizFileJsonStr: ["NULL", "", "", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL"],
        extraFilePosNm: ["", "extraThmnl", "", "", "", "", "", "", "", ""],  
        extraFileJsonStr: ["NULL", "", "", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL"],
      };

      
    
    return state;
}

export const setAxiosCnddtRcmmdState = (axiosData) => {
    
    const state = {
        initSetCmplt: true,
        fileUploadCmplt: false,
        chkYn: "N",
        rcmndUid: ( axiosData ) ? axiosData.rcmndUid : "",
        hireUid: ( axiosData ) ? axiosData.hireUid : "",
        rcmndStat: ( axiosData ) ?  axiosData.rcmndStat : "", 
        mbrUid: ( axiosData ) ?  axiosData.mbrUid : "",
        cnddtNm: ( axiosData ) ?  axiosData.cnddtNm : "",
        cnddtCelpNum: ( axiosData ) ?  axiosData.cnddtCelpNum : "",
        cnddtNmOrg: ( axiosData ) ?  axiosData.cnddtNm : "",
        cnddtCelpNumOrg: ( axiosData ) ?  axiosData.cnddtCelpNum : "",
        cnddtCelpNumPubYn: ( axiosData ) ?  axiosData.cnddtCelpNumPubYn : "N",
        cnddtThmnl: ( axiosData ) ?  axiosData.cnddtThmnl : "",
        cnddtThmnlNm: ( axiosData ) ?  axiosData.cnddtThmnlNm : "",
        cnddtAtchFiles: ( axiosData ) ?  axiosData.cnddtAtchFiles : "",
        cnddtIntro: ( axiosData ) ?  axiosData.cnddtIntro : "",
        empMemo: ( axiosData ) ?  axiosData.empMemo : "",
        dprtMemo: ( axiosData ) ?  axiosData.dprtMemo : "",
        cnddtCelpNumPubReq: ( axiosData ) ?  axiosData.cnddtCelpNumPubReq : "N",
        rcmndStatDttm: ( axiosData ) ?  axiosData.rcmndStatDttm : "N",
        rgstDttm: ( axiosData ) ?  axiosData.rgstDttm : "",
        rgstrUid: ( axiosData ) ?  axiosData.rgstrUid : "",
        updtDttm: ( axiosData ) ?  axiosData.updtDttm : "",
        updtrUid: ( axiosData ) ?  axiosData.updtrUid : "",

        mbrNm: ( axiosData ) ?  axiosData.mbrNm : "",
        mbrId: ( axiosData ) ?  axiosData.mbrId : "",
        replyCnt: ( axiosData ) ?  axiosData.replyCnt : "0",
        replyPoint: ( axiosData ) ?  axiosData.replyPoint : "0",
        lstReplyPoint: ( axiosData ) ?  axiosData.lstReplyPoint : "0",

        rcmndStateReqYn: ( axiosData ) ?  axiosData.rcmndStateReqYn : "",
        rcmndStateReqPreState: ( axiosData ) ?  axiosData.rcmndStateReqPreState : "",
        rcmndStateReqState: ( axiosData ) ?  axiosData.rcmndStateReqState : "",
        rcmndStateReqPreStateStr: "",
        rcmndStateReqStateStr: "",

        cnddtCelpF: "",
        cnddtCelpM: "",
        cnddtCelpL: "",
        cnddtNCelpL : "",

        filePosNm: ["", "cnddtThmnl", "", "", "", "", "", "", "", ""],
        uploadFiles: [ "disable", "disable", "disable", "disable", "disable", "disable", "disable", "disable", "disable", "disable" ],        
        snglImgFileJsonStr: ["NULL", "", "", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL"],
        
        cnddtAtchFilesStr: ["NULL", "", "", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL"]        
      };

      
    
    return state;
}

export const setAxiosJobState = (axiosData) => {
    
    const state = {
        initSetCmplt: false,
        fileUploadCmplt: false,
        chkYn: "N",
        mbrDivi: ( axiosData ) ? axiosData.mbrDivi : "",
        ssnMbrUid: "",
        hireUid : ( axiosData ) ? axiosData.hireUid : "",
        mbrUid : ( axiosData ) ? axiosData.mbrUid : "",
        mbrId : ( axiosData ) ? axiosData.mbrId : "",
        mbrNm : ( axiosData ) ? axiosData.mbrNm : "",
        pubYn : ( axiosData ) ? axiosData.pubYn : "Y",
        bizNm : ( axiosData ) ? axiosData.bizNm : "",
        bizKnd : ( axiosData ) ? axiosData.bizKnd : "",
        saleSize: ( axiosData ) ? axiosData.saleSize : "",
        empSize : ( axiosData ) ? axiosData.empSize : "",
        title : ( axiosData ) ? axiosData.title : "",
        occptnCd : ( axiosData ) ? axiosData.occptnCd : "",
        hireSize : ( axiosData ) ? axiosData.hireSize : "",
        hireKnd : ( axiosData ) ? axiosData.hireKnd : "fullTime",
        intro: ( axiosData ) ?  axiosData.intro : "",
        intro2: ( axiosData ) ?  axiosData.intro2 : "",
        rcmndUntilDon : ( axiosData ) ? axiosData.rcmndUntilDon : "N", 
        rcmndUntilDonFlag : true, 
        rcmndEndDttm : ( axiosData ) ? axiosData.rcmndEndDttm : "",
        rcmndEndYear : ( axiosData ) ? axiosData.rcmndEndYear : "",
        rcmndEndMonth : ( axiosData ) ? axiosData.rcmndEndMonth : "1",
        rcmndEndDay : ( axiosData ) ? axiosData.rcmndEndDay : "1",
        rcmndEndHour : ( axiosData ) ? axiosData.rcmndEndHour : "00",
        rcmndEndYn : ( axiosData ) ? axiosData.rcmndEndYn : "N",
        rcmndAutoEndYn : ( axiosData ) ? axiosData.rcmndAutoEndYn : "N",
        rcmndPrgMailYn : ( axiosData ) ? axiosData.rcmndPrgMailYn : "Y",
        ckData : ( axiosData ) ? axiosData.ckData : "",
        thmnl : ( axiosData ) ? axiosData.thmnl : "",
        thmnlNm : ( axiosData ) ? axiosData.thmnlNm : "",
        thmnl2 : ( axiosData ) ? axiosData.thmnl2 : "",
        thmnlNm2 : ( axiosData ) ? axiosData.thmnlNm2 : "",
        resumeFormThmnl : ( axiosData ) ? axiosData.resumeFormThmnl : "",
        resumeFormThmnlNm : ( axiosData ) ? axiosData.resumeFormThmnlNm : "",
        tags : [],
        tagStr : ( axiosData ) ? axiosData.tagStr : "",
        salaryMin : ( axiosData ) ? axiosData.salaryMin : "0",
        salaryMax : ( axiosData ) ? axiosData.salaryMax : "100",
        srvcFee : ( axiosData ) ? axiosData.srvcFee : 0,
        srvcFeePctMin : ( axiosData ) ? axiosData.srvcFeePctMin : 15,
        srvcFeePctMax : ( axiosData ) ? axiosData.srvcFeePctMax : 25,
        srvcFeeFixYn : ( axiosData ) ? axiosData.srvcFeeFixYn : "N",
        srvcWrtyPeriod : ( axiosData ) ? axiosData.srvcWrtyPeriod : "",
        srvcWrtyMthd : ( axiosData ) ? axiosData.srvcWrtyMthd : "",
        resumeForm : ( axiosData ) ? axiosData.resumeForm : "recForm",
        rcmndRecYn : ( axiosData ) ? axiosData.rcmndRecYn : "Y",
        recMthd : ( axiosData ) ? axiosData.recMthd : "rec",
        emailPubYn : ( axiosData ) ? axiosData.emailPubYn : "N",
        snsPubYn : ( axiosData ) ? axiosData.snsPubYn : "N",
        phNumPubYn : ( axiosData ) ? axiosData.phNumPubYn : "N",
        celpNumPubYn : ( axiosData ) ? axiosData.celpNumPubYn : "N",
        addrPubYn : ( axiosData ) ? axiosData.addrPubYn : "N",

        recSize : ( axiosData ) ? axiosData.recSize : "5",
        confirmCntnt : ( axiosData ) ? axiosData.confirmCntnt : "",

        recTcnt : ( axiosData ) ?  axiosData.recTcnt : "0",
        recChooseCnt : ( axiosData ) ?  axiosData.recChooseCnt : "0",
        recmmndCnt : ( axiosData ) ?  axiosData.recmmndCnt : "0",
        
        bizCateCd1 : ( axiosData ) ? [] : [],  
        bizCateCd2 : [],        
        bizCateCd3 : [],  
        empAddrCateCd1 : [],
        empAddrCateCd2 : [],
        workPosCateCd1 : [],
        workPosCateCd2 : [],
        workPosCateCd3 : [],  
        
        srchFirmCntnt: "",
        srchFirms: [],
        srchFirmUids: [],

        matchDprt: "",

        tagCntnt : "",

        days : [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31],  

        uploadFiles: [ "disable", "disable", "disable", "disable", "disable", "disable", "disable", "disable", "disable", "disable", "disable", "disable" ],
        filePosNm: ["", "thmnl", "thmnl2", "resumeFormThmnl", "", "", "", "", "", "", "", ""],
        snglImgFileJsonStr: ["NULL", "", "", "", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL"],
        
        

      };

      
    
    return state;
}

export const setAxiosQaCntntState = (axiosData) => {
    
    
    const state = {
        initSetCmplt: true,
        fileUploadCmplt: false,
        chkYn: "N",
        brdUid: ( axiosData ) ? axiosData.brdUid : "",
        hireUid: ( axiosData ) ? axiosData.hireUid : "",
        refUid: ( axiosData ) ?  axiosData.refUid : "", 
        mbrUid: ( axiosData ) ?  axiosData.mbrUid : "",
        qaType: ( axiosData ) ?  axiosData.qaType : "",
        cntnt: ( axiosData ) ?  axiosData.cntnt : "",
        rcvMbrUid: ( axiosData ) ?  axiosData.rcvMbrUid : "",
        rgstDttm: ( axiosData ) ?  axiosData.rgstDttm : "",
        rgstrUid: ( axiosData ) ?  axiosData.rgstrUid : "",
        updtDttm: ( axiosData ) ?  axiosData.updtDttm : "",
        updtrUid: ( axiosData ) ?  axiosData.updtrUid : ""
      };

      
    
    return state;
}

export const setAxiosBizGuidState = (axiosData) => {
    
    const state = {
        initSetCmplt: true,
        fileUploadCmplt: false,
        chkYn: "N",
        brdUid: ( axiosData ) ? axiosData.brdUid : "",
        brdDivi: ( axiosData ) ? axiosData.brdDivi : "",
        brdKnd: ( axiosData ) ?  axiosData.brdKnd : "", 
        mbrUid: ( axiosData ) ?  axiosData.mbrUid : "",
        title: ( axiosData ) ?  axiosData.title : "",
        cntnt: ( axiosData ) ?  axiosData.cntnt : "",
        thmnl: ( axiosData ) ?  axiosData.thmnl : "",
        atchFiles: ( axiosData ) ?  axiosData.atchFiles : "",
        rgstDttm: ( axiosData ) ?  axiosData.rgstDttm : "",
        rgstrUid: ( axiosData ) ?  axiosData.rgstrUid : "",
        updtDttm: ( axiosData ) ?  axiosData.updtDttm : "",
        updtrUid: ( axiosData ) ?  axiosData.updtrUid : "",
        
        bizGuidFilePosNm: ["", "thmnl", "", "", "", "", "", "", "", ""],
        bizGuidFileJsonStr: ["NULL", "", "", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL"],
        bizGuidlImgFileJsonStr: ["NULL", "", "", "", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL"],

        filePosNm: ["", "bizCertThmnl", "bizLogoThmnl", "", "", "", "", "", "", ""],
        uploadFiles: [ "disable", "disable", "disable", "disable", "disable", "disable", "disable", "disable", "disable", "disable" ],
        snglImgFileJsonStr: ["NULL", "", "", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL"]
      };

      
    
    return state;
}


export const setAxiosSerachFirmState = (axiosData) => {
    
    const state = {
        initSetCmplt: true,
        fileUploadCmplt: false,
        chkYn: "N",
        srchfirmUid: ( axiosData != null ) ? axiosData.srchfirmUid : "",
        mbrUid: ( axiosData != null ) ?  axiosData.mbrUid : "",
        cmpnyNm: ( axiosData != null ) ?  axiosData.cmpnyNm : "",
        recNm: ( axiosData != null ) ?  axiosData.recNm : "",
        email: ( axiosData != null ) ?  axiosData.email : "",
        emaiId: "",
        emailDom: "",
        emailDomSample : "",
        celpNum: ( axiosData != null ) ?  axiosData.celpNum : "",
        pubYn: ( axiosData != null ) ?  axiosData.pubYn : "",
        delYn: ( axiosData != null ) ?  axiosData.delYn : "",     
        hireUid:    ( axiosData != null ) ?  axiosData.hireUid : "",     
        rgstDttm: ( axiosData != null ) ?  axiosData.rgstDttm : "",
        rgstrUid: ( axiosData != null ) ?  axiosData.rgstrUid : "",
        updtDttm: ( axiosData != null ) ?  axiosData.updtDttm : "",
        updtrUid: ( axiosData != null ) ?  axiosData.updtrUid : ""
      };   
      
    
    return state;
}

export const setAxiosPageState = (axiosData) => {
    
    const state = {
        initSetCmplt: true,
        fileUploadCmplt: false,
        currentPage: ( axiosData != null ) ? axiosData.currentPage : "1",
        pageCnt: ( axiosData != null ) ?  axiosData.pageCnt : "1",
        pageSize: ( axiosData != null ) ?  axiosData.pageSize : "15",
        totRowCnt: ( axiosData != null ) ?  axiosData.totRowCnt : "0",
        pageSetStartPos: ( axiosData != null ) ?  axiosData.pageSetStartPos : "1",
        pageSetEndPos: ( axiosData != null ) ?  axiosData.pageSetEndPos : "1"
      };   
      
    
    return state;
}

export const SelCateCdCombo = () => {
    
    const array = {
        display : '',
        cateCdList1 : [],
        cateCdList2 : [],
        cateCdList3 : []
    };
   
    return array;
}

export const SelCateCdCombo2 = () => {
    
    const array = {
        display : ['','none','none','none','none','none','none','none','none'],  
        cateCdList1 : [[],[],[],[],[],[],[],[],[]],  
        cateCdList2 : [[],[],[],[],[],[],[],[],[]],
        cateCdList3 : [[],[],[],[],[],[],[],[],[]],  
    };
   
    return array;
}

export const HireStatCd = () => {
    
    const array = [
        {cateCd:"tmp", cdName:"임시저장"},
        {cateCd:"confirm", cdName:"검수중"},
        {cateCd:"declined", cdName:"거절"},
        {cateCd:"ing", cdName:"진행중"},
        {cateCd:"end", cdName:"종료"}
    ];
   
    return array;
}

export const CandidateStateCd = () => {

    const array = [
        {cateCd:"01", cdName:"추천중", clssName:"passed"},
        {cateCd:"02", cdName:"서류 합격", clssName:"passed"},
        {cateCd:"03", cdName:"서류 불합격", clssName:""},
        {cateCd:"04", cdName:"면접 합격", clssName:"passed"},
        {cateCd:"05", cdName:"면접 불합격", clssName:""},
        {cateCd:"06", cdName:"1차 면접 합격", clssName:"passed"},
        {cateCd:"07", cdName:"1차 면접 불합격", clssName:""},
        {cateCd:"08", cdName:"2차 면접 합격", clssName:"passed"},
        {cateCd:"09", cdName:"2차 면접 불합격", clssName:""},
        {cateCd:"10", cdName:"인적성 검사 합격", clssName:"passed"},
        {cateCd:"11", cdName:"인적성 검사 불합격", clssName:""},
        {cateCd:"12", cdName:"신체검사 합격", clssName:"passed"},
        {cateCd:"13", cdName:"신체검사 불합격", clssName:""},
        {cateCd:"14", cdName:"처우 협의 종료", clssName:"passed"},
        {cateCd:"15", cdName:"최종 채용 확정", clssName:"passed"}
    ];
    
    return array;
}

export const getCandidateStateCdVal = (cateCd) => {

    if( cateCd == "" || cateCd == null)
    {
        return "";
    }

    var idx = Number(cateCd) - 1;
    
    const array = [
        {cateCd:"01", cdName:"추천중", clssName:"passed"},
        {cateCd:"02", cdName:"서류 합격", clssName:"passed"},
        {cateCd:"03", cdName:"서류 불합격", clssName:""},
        {cateCd:"04", cdName:"면접 합격", clssName:"passed"},
        {cateCd:"05", cdName:"면접 불합격", clssName:""},
        {cateCd:"06", cdName:"1차 면접 합격", clssName:"passed"},
        {cateCd:"07", cdName:"1차 면접 불합격", clssName:""},
        {cateCd:"08", cdName:"2차 면접 합격", clssName:"passed"},
        {cateCd:"09", cdName:"2차 면접 불합격", clssName:""},
        {cateCd:"10", cdName:"인적성 검사 합격", clssName:"passed"},
        {cateCd:"11", cdName:"인적성 검사 불합격", clssName:""},
        {cateCd:"12", cdName:"신체검사 합격", clssName:"passed"},
        {cateCd:"13", cdName:"신체검사 불합격", clssName:""},
        {cateCd:"14", cdName:"처우 협의 종료", clssName:"passed"},
        {cateCd:"15", cdName:"최종 채용 확정", clssName:"passed"}
    ];
    
    //console.log("idx == " + array[idx]);

    return array[idx].cdName;
}

export const EmailDomCd = () => {
    
    const array = [
        {cateCd:"naver.com", cdName:"naver.com"},
        {cateCd:"gmail.com", cdName:"gmail.com"},
        {cateCd:"daum.com", cdName:"daum.com"},
        {cateCd:"nate.com", cdName:"nate.com"},
        {cateCd:"hanmail.com", cdName:"hanmail.com"},
        {cateCd:"hotmail.com", cdName:"hotmail.com"}
    ];
   
    return array;
}

export const CelpFCd = () => {
    
    const array = [
        {cateCd:"010", cdName:"010"},
        {cateCd:"011", cdName:"011"},
        {cateCd:"016", cdName:"016"},
        {cateCd:"017", cdName:"017"},
        {cateCd:"018", cdName:"018"},
        {cateCd:"019", cdName:"019"}
    ];
   
    return array;
}

export const HireSizeCd = () => {
    
    const array = [
        {cateCd:"0", cdName:"0명"},
        {cateCd:"1", cdName:"1명"},
        {cateCd:"2", cdName:"2명"},
        {cateCd:"3", cdName:"3명"},
        {cateCd:"4", cdName:"4명"},
        {cateCd:"5", cdName:"5명"},
        {cateCd:"6", cdName:"6명"},
        {cateCd:"7", cdName:"7명"},
        {cateCd:"8", cdName:"8명"},
        {cateCd:"9", cdName:"9명"},
        {cateCd:"10", cdName:"10명"}
    ];
   
    return array;
}

export const SalesSizeCd = () => {
    
    const array = [
        {cateCd:"01", cdName:"10억 미만"},
        {cateCd:"02", cdName:"10억 ~ 100억"},
        {cateCd:"03", cdName:"100 ~ 500억"},
        {cateCd:"04", cdName:"500 ~ 1,000억"},
        {cateCd:"05", cdName:"1,000 ~ 5,000억"},
        {cateCd:"06", cdName:"5,000 ~ 1조원"},
        {cateCd:"07", cdName:"1조원 이상"},
        {cateCd:"99", cdName:"비공개"},
    ];
   
    return array;
}

export const EmpSizeCd = () => {
    
    const array = [
        {cateCd:"01", cdName:"1 ~ 10명"},
        {cateCd:"02", cdName:"10 ~ 100명"},
        {cateCd:"03", cdName:"100 ~ 500명"},
        {cateCd:"04", cdName:"500 ~ 1,000명"},
        {cateCd:"05", cdName:"1,000 ~ 5,000명"},
        {cateCd:"06", cdName:"5,000 ~ 10,000명"},
        {cateCd:"07", cdName:"1만명 이상"}
    ]    

    return array;
}

export const BankCd = () => {
    
    const array = [
        {cateCd:"003", cdName:"기업은행"},
        {cateCd:"005", cdName:"외환은행"},
        {cateCd:"004", cdName:"국민은행"},
        {cateCd:"011", cdName:"농협중앙은행"},
        {cateCd:"020", cdName:"우리은행"},
        {cateCd:"088", cdName:"신한은행"},
        {cateCd:"023", cdName:"SC제일은행"},
        {cateCd:"027", cdName:"씨티은행"},
        {cateCd:"031", cdName:"대구은행"},
        {cateCd:"032", cdName:"부산은행"},
        {cateCd:"034", cdName:"광주은행"},
        {cateCd:"039", cdName:"경남은행"},
        {cateCd:"071", cdName:"우체국"},
        {cateCd:"081", cdName:"KEB 하나은행"},
        {cateCd:"007", cdName:"수협중앙회"}
    ]
    

    return array;
}

export const BizKndCd = () => {
    
    const array = [
        {cateCd:"001", cdName:"대기업"},
        {cateCd:"002", cdName:"대기업 계열사, 자회사"},
        {cateCd:"003", cdName:"중견기업(300명 이상)"},
        {cateCd:"004", cdName:"중소기업(300명 이하)"},
        {cateCd:"005", cdName:"벤처기업"},
        {cateCd:"006", cdName:"외국계(외국 투자기업)"},
        {cateCd:"007", cdName:"외국계(외국 법인기업)"},
        {cateCd:"008", cdName:"국내 공공기관, 공기업"},
        {cateCd:"009", cdName:"비영리단체, 협회, 교육재단"},
        {cateCd:"010", cdName:"외국기관, 비영리기구, 단체"}
    ]
    
    return array;
};

export const BizGuidDivi = () => {
    
    const array = [
        {cateCd:"01", cdName:"일반"},
        {cateCd:"02", cdName:"상세"},
        {cateCd:"03", cdName:"전체"}
    ];
   
    return array;
}

export const BizGuidKnd = () => {
    
    const array = [
        {cateCd:"01", cdName:"필수"},
        {cateCd:"02", cdName:"필독"},
        {cateCd:"03", cdName:"채용 Tip"}
    ];
   
    return array;
}

export const BizGuidDmmy = () => {
    
    const array = {
        chkYn: "N",
        brdUid: '',
        brdDivi: '',
        brdKnd: '',
        mbrUid: '',
        title: '',
        cntnt: '',
        thmnl: '',
        atchFiles: '',
        rgstDttm: '',
        rgstrUid: '',
        updtDttm: '',
        updtrUid: ''
      }
   
    return array;
}



export const getMbrCnt = async (data) => {
    
    let goUrl = data.serverPath + "/mbr/mbrcnt";
        
    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");
    let tmpDate = axiosRes.data.mbrcnt
    
    return tmpDate;
};


export const getBizCateCdList = async (data) => {
    let goUrl = data.serverPath + "/hireBizCateCd";
    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");
    let resArr = axiosRes.data.hireCateArr;
    
    return resArr;
};



export const getEmpAddrCateCdList = async (data) => {
    let goUrl = data.serverPath + "/empAddrCateCd";    
    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");
    let resArr = axiosRes.data.empCateArr;
    
    return resArr;
};

export const getWorkPosCateCdList = async (data) => {
    let goUrl = data.serverPath + "/workPosCateCd";    
    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");
    let resArr = axiosRes.data.workPosCateCdArr;
    
    return resArr;
};

export const getSearchFirmList = async (data) => {
    let goUrl = data.serverPath + "/srchfirm/searchfirmlist";
    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");
    let tmpArr = [];
    for(var k=0; k<axiosRes.data.resArr.length; k++)
    {
      var tmpData = setAxiosSerachFirmState(axiosRes.data.resArr[k]);
      if( axiosRes.data.resArr[k].hireUid != "" )tmpData.chkYn="Y";
      tmpArr.push(tmpData);
    }
    return tmpArr;
};

export const getMyDprtMbrList = async (data) => {
    let goUrl = data.serverPath + "/mbr/mydeprtmbrlist";
    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");
    let tmpArr = [];
    for(var k=0; k<axiosRes.data.resArr.length; k++)
    {
      var tmpData = setAxiosMbrState(axiosRes.data.resArr[k]);
      tmpArr.push(tmpData);
    }
    return tmpArr;
};

export const getMbr = async (data) => {
    let goUrl = data.serverPath + "/mbr/mbrdetail";
    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");
       
    return axiosRes.data.resInfo;
};

//////////////////////////////////////////////////////////////////////////

export const YearCnt = () => {
      
    const array = [];
    var idx = 10;
    for(var k=2022; k<(2022+idx); k++)array.push(k);
    
    return array;
};

export const BizCateCdCnt = () => {
      
    const array = [];
    var idx = 5;
    for(var k=0; k<idx; k++)array.push(k);
    
    return array;
};

export const EmpAddrCateCdCnt = () => {
    
    const array = [];
    var idx = 5;
    for(var k=0; k<idx; k++)array.push(k);
    
    return array;
};

export const WorkPosCateCdCnt = () => {
    
    const array = [];
    var idx = 5;
    for(var k=0; k<idx; k++)array.push(k);
    
    return array;
};


export const SplitToArray = (str, simb) => {
        
    var splitArr = str.split(simb);

    return splitArr;
}

