import axios from "axios";
import { toast } from "react-toastify";

export const CheckIsValidEmail = (refObj, message) => {
  // 이메일 검사: '@', '.' 이 둘다 포함될것.
  const email = refObj.current.value;
  const isValidEmail = email.includes("@") && email.includes(".");

  if (!isValidEmail) {
    toast.error(
      message !== undefined ? message : "이메일 형식이 올바르지 않습니다."
    );
    setFocus(refObj);
    return false;
  }
};

/**
 * 들어온 값이 숫자로만 구성되어있는지 확인
 */
export const CheckIsOnlyNum = (refObj, message) => {
  let regExp = RegExp(/^[-]?\d+(?:[.]\d+)?$/);

  if (!regExp.test(refObj.current.value)) {
    toast.error(
      message !== undefined ? message : "휴대폰 번호를 숫자만 입력해주세요"
    );
    setFocus(refObj);
    return false;
  }
};

/**
 * 들어온 값이 영문과 숫자로만 구성되어있는지 확인
 */
export const CheckIsOnlyEngOrNum = (refObj, message) => {
  let fieldValue = refObj.current.value;
  let regExp = /^[0-9A-Za-z]+[A-Za-z0-9]{0,19}$/g;

  // val이 영문과 숫자로만 이루어져있는지 확인
  const isOnlyEngOrNum = regExp.test(fieldValue);

  if (!isOnlyEngOrNum) {
    toast.error(
      message !== undefined
        ? message
        : "비밀번호는 영어/숫자만을 포함해야 합니다."
    );
    setFocus(refObj);
    return false;
  }
};

/**
 * Value의 length와 Parameter로 전달된 최소 길이를 비교
 */
export const CheckHasMinLength = (refObj, message, minLength) => {
  const hasMinLength = refObj.current.value.length >= minLength;

  if (!hasMinLength) {
    toast.error(
      message !== undefined
        ? message
        : `비밀번호는 ${minLength}자 이상이어야 합니다.`
    );
    setFocus(refObj);
    return false;
  }
};

/**
 * Value의 length와 Parameter로 전달된 최대 길이를 비교
 */
// export const CheckHasMaxLength = ( refObj, fieldName, maxLength ) => {
//     const hasMaxLength = refObj.current.value.length <= maxLength;

//     if(!hasMaxLength)
//     {
//         toast.error(fieldName + "는 " + maxLength + "자 이하여야 합니다.");
//         setFocus(refObj);
//         return false;
//     }
// }

/**
 * Parameter로 들어온 2개의 Value가 동일한 지 비교 확인
 */
export const CheckHasSameValue = (refObj1, refObj2, message) => {
  const hasSameValue = refObj1.current.value == refObj2.current.value;

  if (!hasSameValue) {
    toast.error(
      message !== undefined ? message : "비밀번호가 일치하지 않습니다."
    );
    setFocus(refObj2);
    return false;
  }
};

/**
 * Parameter로 들어온 2개의 Value가 동일한 지 비교 확인
 */
export const CheckIsChecked = (refObj, message) => {
  const IsChecked = refObj.current.checked;

  console.log("isChecked : " + IsChecked);

  if (!IsChecked) {
    toast.error(message !== undefined ? message : "동의가 필요합니다.");
    setFocus(refObj);
    return false;
  }
};

/**
 * 사업자 등록번호 확인 (숫자만 10자리로 들어와야 함)
 */
export const CheckIsValidBizId = (refObj, message) => {
  // bizID는 숫자만 10자리로 해서 문자열로 넘긴다.
  let bizID = refObj.current.value;

  console.log("bizId : " + bizID);

  let checkID = new Array(1, 3, 7, 1, 3, 7, 1, 3, 5, 1);
  let tmpBizID,
    i,
    chkSum = 0,
    c2,
    remander;
  bizID = bizID.replace(/-/gi, "");

  for (i = 0; i <= 7; i++) chkSum += checkID[i] * bizID.charAt(i);
  c2 = "0" + checkID[8] * bizID.charAt(8);
  c2 = c2.substring(c2.length - 2, c2.length);
  chkSum += Math.floor(c2.charAt(0)) + Math.floor(c2.charAt(1));
  remander = (10 - (chkSum % 10)) % 10;

  let isValidBizId = false;
  if (Math.floor(bizID.charAt(9)) == remander) isValidBizId = true; // OK!

  if (!isValidBizId) {
    toast.error(
      message !== undefined ? message : "사업자번호 형식이 올바르지 않습니다."
    );
    setFocus(refObj);
    return false;
  }
};

/**
 * 해당 값이 공백인지 확인한다.
 * @param {*} value
 * @returns Boolean
 */
export const CheckIsEmpty = (refObj, message) => {
  const isEmpty = refObj.current.value == "";

  if (isEmpty) {
    toast.error(message !== undefined ? message : "필수 입력사항입니다.");
    setFocus(refObj);
    return false;
  }
};

export const CheckIsEmptyFromVal = (refVal, message) => {
  if (refVal == "") {
    toast.error(message !== undefined ? message : "필수 입력사항입니다.");
    //setFocus(refObj);
    return false;
  }
};

export const CheckIsEmptyFromValAlert = (refVal, message) => {
  if (refVal == "") {
    alert(message !== undefined ? message : "필수 입력사항입니다.");
    //setFocus(refObj);
    return false;
  }
};

export const CheckIsLengthFromVal = (refVal, message, strLength) => {
  if (strLength < refVal.length) {
    toast.error(
      message !== undefined
        ? message
        : `추천서를 ${strLength}자 이내로 입력해주세요.`
    );
    //setFocus(refObj);
    return false;
  }
};

/**
 * 첨부된 파일이 존재하는지 확인한다.
 * @param {*} value
 * @returns Boolean
 */
export const CheckIsFileEmpty = (refObj, message) => {
  const isEmpty = refObj.current.value == "";

  if (isEmpty) {
    toast.error(message !== undefined ? message : "필수 입력사항입니다.");
    setFocus(refObj);
    return false;
  }
};

export const setFocus = (refObj) => {
  refObj.current.focus();
};

export const axiosMultipartSubmit = (formData, goUrl, goFunc) => {
  const headers = {
    "Content-Type": "application/x-www-form-urlencoded",
  };

  return new Promise((resolve, reject) => {
    axios
      .post(goUrl, formData, {
        headers: headers,
      })
      .then((result) => {
        resolve(result);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

axios.defaults.withCredentials = true;
export const axiosCrudOnSubmit = (data, goUrl, axiosMethod) => {
  //const CLIENT_ID = process.env.REACT_APP_CLIENT_ID;
  const formData = new FormData();
  const headers = {
    "Content-Type": "application/x-www-form-urlencoded",
    "Access-Control-Allow-Credentials": true,
  };

  const dataKeys = Object.keys(data);
  const dataVals = Object.values(data);

  dataKeys.forEach((key, idx) => {
    if (
      typeof dataVals[idx] == "object" &&
      dataVals[idx] != null &&
      dataVals[idx].length > 0
    ) {
      dataVals[idx].forEach((item, itemIdx) => {
        formData.append(key, item);
      });
    } else {
      formData.append(key, dataVals[idx]);
    }
  });

  return new Promise((resolve, reject) => {
    if (axiosMethod == "GET") {
      axios
        .get(goUrl, formData, {
          headers: headers,
        })
        .then((result) => {
          resolve(result);
        })
        .catch((error) => {
          // toast.error("error occurred during connecting with server");
          reject(error);
        });
    } else if (axiosMethod == "POST") {
      axios
        .post(goUrl, formData, {
          headers: headers,
          withCredentials: true,
        })
        .then((result) => {
          resolve(result);
        })
        .catch((error) => {
          toast.error("error occurred during connecting with server");
          reject(error);
        });
    } else if (axiosMethod == "POST") {
      axios
        .put(goUrl, formData, {
          headers: headers,
          withCredentials: true,
        })
        .then((result) => {
          resolve(result);
        })
        .catch((error) => {
          toast.error("error occurred during connecting with server");
          reject(error);
        });
    } else if (axiosMethod == "PATCH") {
      axios
        .patch(goUrl, formData, {
          headers: headers,
        })
        .then((result) => {
          resolve(result);
        })
        .catch((error) => {
          toast.error("error occurred during connecting with server");
          reject(error);
        });
    } else if (axiosMethod == "DELETE") {
      axios
        .delete(goUrl, {
          headers: headers,
          data: formData,
        })
        .then((result) => {
          resolve(result);
        })
        .catch((error) => {
          toast.error("error occurred during connecting with server");
          reject(error);
        });
    }
  });
};

export const rmvArrIdx = (arry, trgtIdx) => {
  var tmpList = [];

  for (var k = 0; k < arry.length; k++) {
    if (k != trgtIdx) {
      tmpList.push(arry[k]);
    }
  }

  return tmpList;
};

export const rmvArrRowIdxFilter = (arry, trgtIdx) => {
  var tmpList = arry.filter((arry) => arry.rowIdx !== trgtIdx);

  return tmpList;
};

export const rmvArrIdFilter = (arry, trgtIdx) => {
  var tmpList = arry.filter((arry) => arry.id !== trgtIdx);

  return tmpList;
};

export const rmvArrValueFilter = (arry, trgtVal) => {
  var tmpList = arry.filter((arry) => arry !== trgtVal);

  return tmpList;
};

export const commaNumber = (val) => {
  var commaVar = val.replace(/\B(?=(\d{3})+(?!\d))/g, ",");

  return commaVar;
};

export const substr = (str, strt, end) => {
  var resStr = str.substring(strt, end);

  return resStr;
};

export const replace = (str, simb, csimb) => {
  
  var resStr = str.replaceAll(simb, csimb);

  return resStr;
};

export const indexOf = (str, val) => {
  var resStr = str.indexOf(val);

  console.log("indexOf resStr == " + resStr);

  return resStr;
};

const moment = require("moment");
export const getToday = () => {
  return moment().format("YYYY-MM-DD");
};

export const getAddToday = (addDay) => {
  var date = new Date();

  var year = date.getFullYear(); // 년
  var month = date.getMonth(); // 월
  var day = date.getDate(); // 일

  var dttm = new Date(year, month, day + addDay + 1);
  dttm = moment(dttm).format("YYYY-MM-DD");

  return dttm;
};
