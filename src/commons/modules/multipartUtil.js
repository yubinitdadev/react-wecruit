import axios from "axios";
import { axiosMultipartSubmit } from "./commonUtil";

export function snglFileUpload(
  serverPath,
  fileStorePath,
  fileLinkPath,
  filePrefix,
  uploadFiles,
  filePosNm
) {
  return new Promise(async function (resolve, reject) {
    let axiosUrl = serverPath + "/fileUpload";

    console.log("UPLOADFILES : ");
    console.log(uploadFiles);

    let fileCnt = 0;

    uploadFiles.forEach((file, idx) => {
      if (file !== "disable") {
        fileCnt++;
      }
    });

    console.log("FILECNT : " + fileCnt);

    if (fileCnt === 0) resolve(0);

    const formData = new FormData();

    formData.append("fileStorePath", fileStorePath);
    formData.append("fileLinkPath", fileLinkPath);
    formData.append("fileObjId", "upFile");
    formData.append("filePrifix", filePrefix);

    var fileUpPos = 0;
    var fileChkCnt = 0;

    uploadFiles.forEach((file, idx) => {
      if (file !== "disable") {
        formData.append("filePos", fileUpPos);
        formData.append("filePosNm", filePosNm[fileUpPos]);
        formData.append("upFile" + fileUpPos, file, file.name);

        fileChkCnt++;
      }
      fileUpPos++;
    });

    let result;

    if (fileChkCnt > 0) {
      result = await axiosMultipartSubmit(formData, axiosUrl);
    } else {
      result = fileChkCnt;
    }

    resolve(result);
  });
}

export function snglFileUploadWithJsonStr(
  serverPath,
  fileStorePath,
  fileLinkPath,
  filePrefix,
  uploadFiles,
  filePosNm,
  jsonStrArr
) {
  return new Promise(async function (resolve, reject) {
    let axiosUrl = serverPath + "/fileUpload";

    console.log("UPLOADFILES : ");
    console.log(uploadFiles);

    let fileCnt = 0;

    uploadFiles.forEach((file, idx) => {
      if (file !== "disable" && jsonStrArr[idx] === "") {
        fileCnt++;
      }
    });

    console.log("FILECNT : " + fileCnt);

    if (fileCnt === 0) resolve(0);

    const formData = new FormData();

    formData.append("fileStorePath", fileStorePath);
    formData.append("fileLinkPath", fileLinkPath);
    formData.append("fileObjId", "upFile");
    formData.append("filePrifix", filePrefix);

    var fileUpPos = 0;
    var fileChkCnt = 0;

    uploadFiles.forEach((file, idx) => {
      if (file !== "disable") {
        formData.append("filePos", fileUpPos);
        formData.append("filePosNm", filePosNm[fileUpPos]);
        formData.append("upFile" + fileUpPos, file, file.name);

        fileChkCnt++;
      }
      fileUpPos++;
    });

    let result;

    if (fileChkCnt > 0) {
      result = await axiosMultipartSubmit(formData, axiosUrl);
    } else {
      result = fileChkCnt;
    }

    resolve(result);
  });
}
