import { useCallback, useContext, useEffect, useRef, useState } from "react";
import Modal from "modal-all/ModalAll";

// 모달창
import { MatchingPosting } from "modal-all/modals/MatchingPosting";
import { EditDepartment } from "modal-all/modals/EditDepartment";
import { AccessInfo } from "modal-all/modals/AccessInfo";
import { CreateId } from "modal-all/modals/CreateId";
import { DeleteId } from "modal-all/modals/DeleteId";
import {
  axiosCrudOnSubmit,
  CheckHasMinLength,
  CheckHasSameValue,
  CheckIsEmpty,
  CheckIsOnlyEngOrNum,
  CheckIsOnlyNum,
} from "commons/modules/commonUtil";
import { Context } from "contexts";
import { toast, ToastContainer } from "react-toastify";
import { UPD_MBRUID } from "contexts/actionTypes";
import { FormattedMessage, useIntl } from "react-intl";
import { messages } from "lang/DefineMessages";

export const Department = () => {
  const intl = useIntl();
  let PROC_CHK = "";

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid"),
    ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi"),
  };

  const createRefObj = {
    mbrIdRef: useRef(null),
    pswdRef: useRef(null),
    confirmPswdRef: useRef(null),
    introRef: useRef(null),
    emailAccountRef: useRef(null),
    emailAddressRef: useRef(null),
    celpNumRef: useRef(null),
  };

  const editRefObj = {
    mbrIdRef: useRef(null),
    pswdRef: useRef(null),
    confirmPswdRef: useRef(null),
    introRef: useRef(null),
    emailAccountRef: useRef(null),
    emailAddressRef: useRef(null),
    celpNumRef: useRef(null),
  };

  const [depts, setDepts] = useState([]);
  const [state, setState] = useState({
    mbrUid: "",
    mbrId: "",
    mbrDivi: "MBRDIVI-03",
    pswd: "",
    pswdConfirm: "",
    intro: "",
    email: "",
    celpNum: "",
  });

  const {
    state: { mbr, server },
    dispatch,
  } = useContext(Context);

  const [chckboxList, setChckboxList] = useState([]);

  const [modalVisible1, setModalVisible1] = useState(false);
  const [modalVisible2, setModalVisible2] = useState(false);
  const [modalVisible3, setModalVisible3] = useState(false);
  const [modalVisible4, setModalVisible4] = useState(false);
  const [modalVisible5, setModalVisible5] = useState(false);

  const jsonCompResult = (resJson) => {
    if (resJson.resCd !== "0000") {
      toast.error(
        intl.formatMessage({
          id: "error.error.in.server",
          defaultMessage: "서버에 오류가 발생했습니다.",
        })
      );
      return;
    } else {
      console.log("PROC_CHK : ");
      console.log(PROC_CHK);

      if (PROC_CHK === "INS") {
        toast.info(
          intl.formatMessage({
            id: "info.department.id.created",
            defaultMessage: "현업부서 아이디가 생성되었습니다.",
          })
        );
        closeModal4();
      } else if (PROC_CHK === "UPD") {
        toast.info(
          intl.formatMessage({
            id: "info.department.id.edited",
            defaultMessage: "현업부서 아이디 수정이 완료되었습니다.",
          })
        );
        closeModal3();
      } else if (PROC_CHK === "DEL") {
        toast.info(
          intl.formatMessage({
            id: "info.department.id.deleted",
            defaultMessage: "선택한 현업부서 아이디가 삭제되었습니다.",
          })
        );
        closeModal5();
      }

      let data = {
        ssnMbrUid: ssn.ssnMbrUid,
        mbrUid: mbr.mbrUid,
        mbrDivi: "MBRDIVI-03",
      };

      initSetHandle(data);

      return;
    }
  };

  async function mbrDupl() {
    let goUrl = server.path + "/mbr/mbrDupl";
    let data = {
      mbrId: state.mbrId,
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    if (axiosRes.data.mbrUid === "DUPL") {
      toast.error(
        intl.formatMessage({
          id: "error.id.duplicated",
          defaultMessage: "이미 가입된 아이디입니다.",
        })
      );
      return;
    }
  }

  const openModal1 = () => {
    setModalVisible1(true);
  };

  const openModal2 = () => {
    setModalVisible2(true);
  };

  const openModal3 = (dept) => {
    setState({
      ...state,
      mbrDivi: "MBRDIVI-03",
      mbrUid: dept.mbrUid,
      mbrId: dept.mbrId,
      pswd: dept.pswd,
      pswdConfirm: dept.pswd,
      intro: dept.intro,
      email: dept.email,
      celpNum: dept.celpNum,
    });
    setModalVisible3(true);
  };

  const openModal4 = () => {
    setState({
      ...state,
      ssnMbrUid: mbr.mbrUid,
      mbrDivi: "MBRDIVI-03",
    });

    setModalVisible4(true);
  };

  const openModal5 = () => {
    setModalVisible5(true);
  };

  const closeModal1 = () => {
    setModalVisible1(false);
  };

  const closeModal2 = () => {
    setModalVisible2(false);
  };

  const closeModal3 = () => {
    setModalVisible3(false);
  };

  const closeModal4 = () => {
    setModalVisible4(false);
  };

  const closeModal5 = () => {
    setModalVisible5(false);
  };

  const cntntsCRUD = (workDiv) => {
    let refObj = workDiv === "INS" ? createRefObj : editRefObj;

    if (
      CheckIsEmpty(
        refObj.mbrIdRef,
        intl.formatMessage({
          id: "error.need.input.id",
          defaultMessage: "아이디를 입력해주세요.",
        })
      ) === false
    )
      return;
    if (
      CheckIsEmpty(
        refObj.pswdRef,
        intl.formatMessage({
          id: "error.need.input.password",
          defaultMessage: "비밀번호를 입력해주세요.",
        })
      ) === false
    )
      return;
    if (
      CheckIsEmpty(
        refObj.confirmPswdRef,
        intl.formatMessage({
          id: "error.need.input.re.password",
          defaultMessage: "비밀번호를 다시 입력해주세요.",
        })
      ) === false
    )
      return;
    if (
      CheckIsEmpty(
        refObj.introRef,
        intl.formatMessage({
          id: "error.need.input.memo",
          defaultMessage: "설명 메모를 입력해주세요.",
        })
      ) === false
    )
      return;
    if (
      CheckIsEmpty(
        refObj.emailAccountRef,
        intl.formatMessage({
          id: "error.need.input.email.account",
          defaultMessage: "이메일 계정을 입력해주세요.",
        })
      ) === false
    )
      return;
    if (
      CheckIsEmpty(
        refObj.emailAddressRef,
        intl.formatMessage({
          id: "error.need.input.email.address",
          defaultMessage: "이메일 주소를 입력해주세요.",
        })
      ) === false
    )
      return;
    if (
      CheckHasMinLength(
        refObj.pswdRef,
        intl.formatMessage(messages.minCharacter, {
          minLength: 8,
        }),
        8
      ) === false
    )
      return;
    if (
      CheckIsOnlyEngOrNum(
        refObj.pswdRef,
        intl.formatMessage({
          id: "error.only.english.number",
          defaultMessage: "비밀번호는 영어/숫자만을 포함해야 합니다.",
        })
      ) === false
    )
      return;
    if (
      CheckHasSameValue(
        refObj.pswdRef,
        refObj.confirmPswdRef,
        intl.formatMessage({
          id: "error.password.not.match",
          defaultMessage: "비밀번호가 일치하지 않습니다.",
        })
      ) === false
    )
      return;
    if (
      CheckIsEmpty(
        refObj.emailAddressRef,
        intl.formatMessage({
          id: "error.need.input.email.address",
          defaultMessage: "이메일 주소를 입력해주세요.",
        })
      )
    ) {
      if (
        CheckIsOnlyNum(
          refObj.celpNumRef,
          intl.formatMessage({
            id: "placeholder.phone.only.number",
            defaultMessage: "휴대폰 번호를 숫자만 입력해주세요",
          })
        ) === false
      )
        return;
    }

    if (workDiv === "UPD") {
      goUpdate();
      console.log("UPD");
    } else if (workDiv === "INS") {
      goInsert();
      console.log("INS");
    }
  };

  async function goInsert() {
    PROC_CHK = "INS";

    let goUrl = server.path + "/mbr";
    let axiosRes = await axiosCrudOnSubmit(state, goUrl, "POST");

    jsonCompResult(axiosRes.data);
  }

  async function goUpdate() {
    PROC_CHK = "UPD";

    let goUrl = server.path + "/mbr/" + state.mbrUid;
    let axiosRes = await axiosCrudOnSubmit(state, goUrl, "POST");

    jsonCompResult(axiosRes.data);
  }

  const goDel = async () => {
    PROC_CHK = "DEL";

    let goUrl = server.path + "/mbr";
    let data = {
      mbrDivi: "MBRDIVI-03",
    };

    let mbrUids = "";

    if (chckboxList.length <= 0) {
      toast.error(
        intl.formatMessage({
          id: "error.select.department.id",
          defaultMessage: "선택한 현업부서 아이디가 없습니다.",
        })
      );
      return;
    }

    chckboxList.forEach((mbrUid, idx) => {
      mbrUids += mbrUid;

      if (idx + 1 !== chckboxList.length) mbrUids += ",";
    });

    data["mbrUid"] = mbrUids;

    console.log(data);

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    jsonCompResult(axiosRes.data);
  };

  const initSetHandle = async (data) => {
    let goUrl = server.path + "/mbr/mbrlist";

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    console.log("INIT AXIOS RES : ");
    console.log(axiosRes);

    setDepts(axiosRes.data.mbrArr);
  };

  const allCheckedHandle = useCallback((checked) => {
    if (checked) {
      const checkedItemList = [];

      depts.forEach((dept) => checkedItemList.push(dept.mbrUid));

      setChckboxList(checkedItemList);
    } else {
      setChckboxList([]);
    }
  });

  const oneCheckedHandle = useCallback((checked, id) => {
    if (checked) {
      setChckboxList([...chckboxList, id]);
    } else {
      setChckboxList(chckboxList.filter((elemId) => elemId !== id));
    }
  });

  useEffect(async () => {
    window.scrollTo(0, 0);

    console.log("MBR UID : ");
    console.log(window.localStorage.getItem("mbrUid"));
    console.log("MBR DIVI : ");
    console.log(window.localStorage.getItem("mbrDivi"));

    dispatch({
      type: UPD_MBRUID,
      payload: {
        mbrUid: window.localStorage.getItem("mbrUid"),
        mbrDivi: window.localStorage.getItem("mbrDivi"),
      },
    });

    let data = {
      ssnMbrUid:
        mbr.mbrUid !== "" ? mbr.mbrUid : window.localStorage.getItem("mbrUid"),
      ssnMbrDivi:
        mbr.mbrDivi !== ""
          ? mbr.mbrDivi
          : window.localStorage.getItem("mbrDivi"),
      mbrUid:
        mbr.mbrUid !== "" ? mbr.mbrUid : window.localStorage.getItem("mbrUid"),
      mbrDivi: "MBRDIVI-03",
    };

    initSetHandle(data);
  }, []);

  useEffect(async () => {
    console.log(chckboxList);
  }, [chckboxList]);

  return (
    <main className="employersMain">
      <ToastContainer />

      <section>
        <div className="idManagement">
          <h3 className="sectionTitle">
            <span>
              <FormattedMessage
                id="department.id.manage"
                defaultMessage="현업부서 아이디 관리"
              />
            </span>
          </h3>
          <div className="idTableWrap">
            <div>
              <div>
                <input
                  type="checkbox"
                  id="selectAll"
                  onChange={(e) => {
                    allCheckedHandle(e.target.checked);
                  }}
                  checked={
                    chckboxList.length === 0
                      ? false
                      : chckboxList.length === depts.length
                      ? true
                      : false
                  }
                />
                <label htmlFor="selectAll">
                  <FormattedMessage id="select.all" defaultMessage="전체선택" />
                </label>
                <div className="flexAlignRight">
                  <button
                    type="button"
                    className="modalOpenButton"
                    onClick={openModal1}
                  >
                    <FormattedMessage
                      id="department.access.info"
                      defaultMessage="권한정보"
                    />
                  </button>
                  {
                    <Modal
                      visible={modalVisible1}
                      closable={true}
                      maskClosable={true}
                      onClose={closeModal1}
                      setState={setState}
                      nobutton={true}
                    >
                      <AccessInfo />
                    </Modal>
                  }
                </div>
              </div>
              <table className="idTable">
                <tbody>
                  {depts.map((dept, idx) => (
                    <tr key={dept.mbrUid}>
                      <td>
                        <input
                          type="checkbox"
                          id={dept.mbrUid}
                          checked={
                            chckboxList.includes(dept.mbrUid) ? true : false
                          }
                          onChange={(e) => {
                            oneCheckedHandle(e.target.checked, e.target.id);
                          }}
                        />
                      </td>
                      <td>{dept.mbrId}</td>
                      <td>{dept.intro}</td>
                      <td>
                        <div>
                          <button
                            type="button"
                            className="modalOpenButton"
                            onClick={openModal2}
                          >
                            <FormattedMessage
                              id="department.matching.toggle"
                              defaultMessage="현업부서 매칭/취소"
                            />
                          </button>
                          {
                            <Modal
                              visible={modalVisible2}
                              closable={true}
                              maskClosable={true}
                              onClose={closeModal2}
                              inputName={intl.formatMessage({
                                id: "department.matching",
                                defaultMessage: "매칭하기",
                              })}
                              closeName={intl.formatMessage({
                                id: "department.matching.cancel",
                                defaultMessage: "매칭취소",
                              })}
                            >
                              <MatchingPosting />
                            </Modal>
                          }
                        </div>
                      </td>
                      <td>
                        <div>
                          <button
                            type="button"
                            className="modalOpenButton"
                            onClick={() => {
                              openModal3(dept);
                            }}
                          >
                            <FormattedMessage
                              id="department.edit.info"
                              defaultMessage="정보수정"
                            />
                          </button>
                          {
                            <Modal
                              visible={modalVisible3}
                              closable={true}
                              maskClosable={true}
                              onSubmit={() => {
                                cntntsCRUD("UPD");
                              }}
                              onClose={closeModal3}
                              setState={setState}
                              inputName={intl.formatMessage({
                                id: "edit",
                                defaultMessage: "수정하기 >",
                              })}
                            >
                              <EditDepartment
                                state={state}
                                setState={setState}
                                refObj={editRefObj}
                              />
                            </Modal>
                          }
                        </div>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div className="flexAlignRight idButtons">
          <div>
            <button
              type="button"
              className="bigButton blueColored"
              onClick={openModal4}
            >
              <FormattedMessage
                id="department.generate.id"
                defaultMessage="아이디 생성 (+)"
              />
            </button>
            {
              <Modal
                visible={modalVisible4}
                closable={true}
                maskClosable={true}
                onSubmit={() => {
                  cntntsCRUD("INS");
                }}
                onClose={closeModal4}
                setState={setState}
                inputName={intl.formatMessage({
                  id: "submit",
                  defaultMessage: "등록하기 >",
                })}
              >
                <CreateId
                  state={state}
                  setState={setState}
                  refObj={createRefObj}
                />
              </Modal>
            }
          </div>
          <button
            type="button"
            className="bigButton redColored"
            onClick={openModal5}
          >
            <FormattedMessage
              id="department.remove.id"
              defaultMessage="아이디 삭제 (-)"
            />
          </button>
          {
            <Modal
              visible={modalVisible5}
              closable={true}
              maskClosable={true}
              onSubmit={goDel}
              onClose={closeModal5}
              setState={setState}
              inputName={intl.formatMessage({
                id: "yes",
                defaultMessage: "예",
              })}
              closeName={intl.formatMessage({
                id: "no",
                defaultMessage: "아니오",
              })}
            >
              <DeleteId
                state={state}
                setState={setState}
                chckboxList={chckboxList}
              />
            </Modal>
          }
        </div>
        <div className="idManagement">
          <h3 className="sectionTitle">
            <span>
              <FormattedMessage
                id="department.matching.status"
                defaultMessage="채용공고 매칭 현황"
              />
            </span>
          </h3>
          <div className="matchingLists">
            <div>
              <b>
                <FormattedMessage id="id.test" defaultMessage="아이디테스트" />
              </b>{" "}
              <span>
                <FormattedMessage id="id" defaultMessage="아이디" />:
                아이디테스트{" "}
                <FormattedMessage id="pw" defaultMessage="비밀번호" />: 1234
              </span>
            </div>
            <ul>
              <li>
                채용공고 제목이 이 곳에
                <span className="material-icons deleteButton">close</span>
              </li>
              <li>
                채용공고 제목이 이 곳에
                <span className="material-icons deleteButton">close</span>
              </li>
            </ul>
          </div>
        </div>
      </section>
    </main>
  );
};
