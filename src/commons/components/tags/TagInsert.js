import { useState } from "react";
import { useIntl } from "react-intl";

export const TagInsert =(props) => {
  const intl = useIntl();
  const [content, setContent] = useState("");
  // const ref = useRef();

  const handleChange = (e) => {
    setContent(e.target.value);
  };

  const handleKeyPress = (e) => {
    e.preventDefault();

    if (e.key === "Enter") {
      handleSubmit();

    } else if(e.keyCode === 32) {
      handleSubmit();
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault(); // onSubmit 이벤트는 브라우저를 새로고치기 때문에 막아주기
    if (!content) return;
    // 만약 input 창이 빈채로 submit을 하려고 할 땐 return시키기
    props.onSubmit(content);
    setContent("");
    // submit을 한 후에는 input 창을 비우기
  };

  // useEffect(() => {
  //   ref.current.focus();
  // }, []);

  return(
    <>
      <input type="text" name="text" value={content} onChange={handleChange} style={{border: 'none'}}/>
      <input 
        type="submit" 
        value=
          {intl.formatMessage({
            id: "submitting",
            defaultMessage: "등록"
          })}
        className='basicButton' 
        onClick={handleSubmit}
        onKeyPress={handleKeyPress} 
        style={{display: 'none'}}
      />
    </>
  )
}