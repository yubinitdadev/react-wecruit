
export const TagItem =({tag, onRemove}) => {
const { id, text, readonly } = tag;

  return(
    <span>{text}
      <button 
        style={ (readonly !== "Y") ? { paddingRight:1} : {paddingRight:4} }
        className="delete" type="button" onClick={() => onRemove(id)}>
        {
          (readonly !== "Y")? ( <span className='material-icons'>close</span> ) :
          ('')
        }        
      </button>
    </span>
  )
}