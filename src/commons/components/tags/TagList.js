import { TagItem } from "./TagItem"

export const TagList =({tags, onRemove}) => {

  return (
    <>
      {tags.map((tag) => {
        if(tag.text !== "")
        {
          return (
            <TagItem
              tag={tag}
              key={tag.id}
              onRemove={onRemove}
            />    
          )
        }
      })}
    </>
  )
}