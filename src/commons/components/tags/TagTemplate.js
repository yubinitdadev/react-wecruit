import { useEffect, useRef, useState } from "react";
import { useIntl } from "react-intl";
import { TagInsert } from "./TagInsert";
import { TagList } from "./TagList"

export const TagTemplate = (props) => {
  const intl = useIntl();
  const handleSubmit = (text) => {
    const tag = {
      id: props.tagCnt,
      text
    };

    if(props.state.emails !== undefined && props.state.emails.length > 29){
      alert(intl.formatMessage({
        id: "input.max30",
        defaultMessage: "30개까지 입력 가능합니다!"
      }));
      return false;
    }

    props.setState({
      ...props.state,
      [props.target]: props.state[props.target].concat(tag)
    });

    props.setTagCnt(props.tagCnt + 1); // nextId를 1씩 더하기

    if(props.tagLength !== undefined) props.setTagLength(props.tagLength + 1);
  };

  const onRemove = (id) => {
    props.setState({
      ...props.state,
      [props.target]: props.state[props.target].filter((tag) => tag.id !== id)
    })
  };

  return(
    <div className="tags tagsInput">
      <div className="tagsIn">
        <TagList tags={props.state[props.target]} onRemove={onRemove}/>
        <TagInsert onSubmit={handleSubmit}/>
      </div>
    </div>
  )
}