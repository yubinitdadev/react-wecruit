import { useRef, useState } from "react";
import { FormattedMessage } from "react-intl";

export const MultipleImageExtra =(props) => {
  const { id } = props.imageInput;
  //const [imgName, setImgName] = useState('파일 이름');

  let fileChangedHandler = e => {
    console.log(id);

    console.log("FILE NAME : ");
    console.log(e.target.files[0].name);
    
    const imageNm = {
      id: id,
      fileName: e.target.files[0].name
    }

    const imageFile = {
      id: id,
      file: e.target.files[0]
    }

    let nmIdx = props.imgNmList.findIndex(object => {
      return object.id === id;
    });

    let fileIdx = props.imgFileList.findIndex(object => {
      return object.id === id;
    });

    if(nmIdx === -1 && fileIdx === -1) {
      props.setImgNmList(props.imgNmList.concat(imageNm));
      props.setImgFileList(props.imgFileList.concat(imageFile));
    } else {
      let tmpJsonStrList = [...props.state.extraSnglImgFileJsonStr];
      tmpJsonStrList[nmIdx + 1] = "";

      let tmpNmList = [...props.imgNmList];
      tmpNmList[nmIdx] = imageNm;

      let tmpFileList = [...props.imgFileList];
      tmpFileList[fileIdx] = imageFile;

      props.setState({
        ...props.state,
        extraSnglImgFileJsonStr : tmpJsonStrList
      })
      props.setImgNmList(tmpNmList);
      props.setImgFileList(tmpFileList);
    }

    /*
    let tmpList = state.extraUploadFiles;
    tmpList[id] = e.target.files[0];

    let tmpJsonStrList = state.extraSnglImgFileJsonStr;
    tmpJsonStrList[id] = "";
    
    setState({
      ...state,
      uploadFiles: tmpList,
      snglImgFileJsonStr: tmpJsonStrList,
      profThmnl: "",
      profThmnlNm: e.target.files[0].name
    })
    */

    /*
    var files = e.target.files;
    var filesArray = [].slice.call(files);
    filesArray.forEach(e => {
      setImgName(e.name);
    });
    */
  };

  return(
    <div className="multipleFile">
      <div>
        <span className="fileName fullWidth">
          {(props.imgNmList.filter(imgNm => imgNm.id === id)[0] !== undefined) 
          ? props.imgNmList.filter(imgNm => imgNm.id === id)[0].fileName 
          : "파일 이름"}
        </span>
        <label htmlFor={"fileInput" + id}>
          첨부하기 &gt;
        </label>
        <input type="file" id={"fileInput" + id} className="fileInput" accept='.jpg, .png, .gif' onChange={fileChangedHandler}/>
        <button type="button" className="formButton"  onClick={() => props.onRemove(id)}>
          <FormattedMessage
            id="delete"
            defaultMessage="삭제하기"
          /> &gt;
        </button>
      </div>
    </div>
  )
}