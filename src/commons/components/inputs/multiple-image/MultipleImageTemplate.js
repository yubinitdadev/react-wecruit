import { useEffect } from "react";
import { useIntl } from "react-intl";
import { MultipleImageExtra } from "./MultipleImageExtra";
import MultipleImageList from "./MultipleImageList";
import MultipleImageSelect from "./MultipleImageSelect";

const MultipleImageTemplate = (props) => {
  const intl = useIntl();

  useEffect(()=>{
    let tmpFiles = props.state.extraUploadFiles;

    for(let i = 0; i < props.imgFileList.length; i++)
    {
      if(props.imgFileList[i].file !== undefined)
      {
        tmpFiles[1+i] = props.imgFileList[i].file;
      } else {
        tmpFiles[1+i] = "disable";
      }
    }

    props.setState({
      ...props.state,
      extraUploadFiles: tmpFiles
    })
  }, [props.imgFileList])

  const onAddBtnClick =(e) => {
    const image = {
      id: props.extraCnt,
      content: <MultipleImageExtra />,
    }

    if(props.imageInputs.length>3){
      alert(intl.formatMessage({
        id: "input.max5",
        defaultMessage: "5개까지 입력 가능합니다!"
      }));
      return false;
    }else {
      props.setImageInputs(props.imageInputs.concat(image));
    }

    props.setExtraCnt(props.extraCnt + 1);
  }

  const onRemove = (id) => {
    let nmIdx = props.imgNmList.findIndex(object => {
      return object.id === id;
    });
    
    props.setImageInputs(props.imageInputs.filter(image => image.id !== id));
    props.setImgNmList(props.imgNmList.filter(imgNm => imgNm.id !== id ));
    props.setImgFileList(props.imgFileList.filter(imgFile => imgFile.id !== id ));

    let tmpJsonStr = [...props.state.extraSnglImgFileJsonStr];
    tmpJsonStr[nmIdx+1] = "";

    props.setState({
      ...props.state,
      extraSnglImgFileJsonStr: tmpJsonStr
    });
  };

  return(
    <>
      <MultipleImageSelect state={props.state} setState={props.setState} onClick={onAddBtnClick}/>
      <MultipleImageList state={props.state} setState={props.setState} imgNmList={props.imgNmList} setImgNmList={props.setImgNmList} 
        imgFileList={props.imgFileList} setImgFileList={props.setImgFileList} imageInputs={props.imageInputs} onRemove={onRemove}/>
    </>
  )
}

export default MultipleImageTemplate;