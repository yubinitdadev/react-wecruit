import { useState } from "react";
import { MultipleImageExtra } from "./MultipleImageExtra";

const MultipleImageList =(props) => {

  return (
    <>
      {props.imageInputs.map((imageInput) => (
        <MultipleImageExtra
          state = {props.state}
          setState = {props.setState}
          imgNmList={props.imgNmList}
          setImgNmList={props.setImgNmList}
          imgFileList={props.imgFileList}
          setImgFileList={props.setImgFileList}
          imageInput={imageInput}
          key={imageInput.id}
          onRemove={props.onRemove}
          fileName=''
        />
      ))}
    </>
  )
}

export default MultipleImageList;