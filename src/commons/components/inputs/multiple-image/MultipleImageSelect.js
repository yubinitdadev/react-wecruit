import { useState } from 'react';
import { FormattedMessage } from 'react-intl';

const MultipleImageSelect =({state, setState, onClick}) => {
  const [imgName, setImgName] = useState('파일 이름');

  let fileChangedHandler = e => {

    let tmpList = state.extraUploadFiles;
    tmpList[0] = e.target.files[0];

    let tmpJsonStrList = state.extraSnglImgFileJsonStr;
    tmpJsonStrList[0] = "";

    setState({
      ...state,
      extraUploadFiles: tmpList,
      extraSnglImgFileJsonStr: tmpJsonStrList,
      thmnl1Nm: e.target.files[0].name,
    })
    
    /*
    var files = e.target.files;
    var filesArray = [].slice.call(files);
    filesArray.forEach( e => {
      setImgName(e.name);
    });
    */
  };

  return (
    <div className="multipleFile">
      <div>
        <span className="fileName">{state.fileNmObj.thmnl1Nm}</span>
        <label htmlFor="fileInput">
          <FormattedMessage
            id="attach"
            defaultMessage="첨부하기"
          /> &gt;
        </label>
        <input type="file" id="fileInput" className="fileInput" accept='.jpg, .png, .gif' onChange={fileChangedHandler}/>
        <button type="button" className="formButton" onClick={onClick}>
          <FormattedMessage
            id="add"
            defaultMessage="추가하기"
          /> &gt;
        </button>
      </div>
    </div>
  );
}

export default MultipleImageSelect;

