import { useEffect, useRef, useState } from "react";
import { FormattedMessage, useIntl } from "react-intl";

export const BusinessTypeExtra =(props) => {
  const intl = useIntl();
  const { id } = props.type;

  const [upCd1Val, setUpCd1Val] = useState( (props.state.bizCateCd[id] !== undefined) ? props.state.bizCateCd[id].upCd1 : "" );
  const [upCd2Val, setUpCd2Val] = useState( (props.state.bizCateCd[id] !== undefined && props.state.bizCateCd[id].upCd2 !== undefined) ? props.state.bizCateCd[id].upCd2 : "" );
  const [upCd2Options, setUpCd2Options] = useState([
    { cateCd : "", cdName : <FormattedMessage
      id="select"
      defaultMessage="선택하세요"
    /> }
  ]);
  const [upCd3Options, setUpCd3Options] = useState([
    { cateCd : "", cdName : "--선택하세요--" }
  ]);
  const [extraHireCate, setExtraHireCate] = useState("");

  let cateObj = {
    upCdArr: [],
    cateCdArr: [],
    cateNmArr: [],
    upCdArr2: [],
    cateCdArr2: [],
    cateNmArr2: [],
  }

  let tmpIdx = 0;

  props.bizCateCd.forEach((cate, idx)=>{
    if( cate.lvl === "2" || cate.lvl === "3")
    {
      cateObj.upCdArr[tmpIdx] = cate.upCd;
      cateObj.cateCdArr[tmpIdx] = cate.cateCd;
      cateObj.cateNmArr[tmpIdx] = cate.cdName;
    }

    tmpIdx++;
  })

  const setCate = (val, lvl) => {
    let upCd = val;
    let tmpList = [];

    for( let i = 0; i <= tmpIdx; i++ )
    {
      if( cateObj.upCdArr[i] == upCd )
      {
        tmpList.push({
          cateCd: cateObj.cateCdArr[i],
          cdName: cateObj.cateNmArr[i]
        })
      }
    }

    if(lvl === 1) 
    {
      setUpCd2Options(tmpList);
      setUpCd3Options([
        { cateCd : "", cdName : "--선택하세요--" }
      ]);
      
      setCate(tmpList[0].cateCd, 2);
    }
    else if(lvl === 2) 
    {
      setUpCd3Options(tmpList);
      
      let tmpBizCateList = [...props.state.bizCateCd];

      let tmpIdx = tmpBizCateList.findIndex(tmpBizCate => tmpBizCate.id === id);
      
      if( tmpIdx === -1 ) tmpBizCateList[tmpIdx]["cateCd"] = tmpList[0].cateCd;
      else tmpBizCateList[tmpIdx].cateCd = (tmpBizCateList[tmpIdx].upCd1 !== undefined) ? tmpBizCateList[tmpIdx].cateCd : tmpList[0].cateCd;
      
      props.setState({
        ...props.state,
        bizCateCd: tmpBizCateList
      });
      
      setExtraHireCate( (tmpBizCateList[tmpIdx].upCd1 !== "") ? tmpBizCateList[tmpIdx].cateCd : tmpList[0].cateCd );
    }
  }

  useEffect(()=>{
    if(upCd1Val !== "" && upCd1Val !== undefined)
    {
      if(props.bizCateCd.length > 0) 
      {
        setCate(upCd1Val, 1);
      }
    }
  },[props.bizCateCd])

  return(
    <div className="extraInput">
      <span className="labelSpan" />
      <select name="businessType2-1" id="businessType2-1"
        value={upCd1Val}
        onChange={ (e) => {
          setCate(e.target.value, 1); 
          setUpCd1Val(e.target.value);
        }}>
        <option>
          --
            {intl.formatMessage({
              id: "select",
              defaultMessage: "선택하세요"
            })}
          --
        </option>
        {
          props.bizCateCd.map((cate, idx)=>{
            if(cate.lvl === "1") {
              return (
                <option key={cate.cateCd} value={cate.cateCd}>{cate.cdName}</option>
              )
            }
          }) 
        }
      </select>
      <select name="businessType2-2" id="businessType2-2"
        value={upCd2Val}
        onChange={ (e) => {
          setCate(e.target.value, 2);
          setUpCd2Val(e.target.value); 
        }}>
        {
          upCd2Options.map((option, idx) => {
            return(
              <option key={option.cateCd} value={option.cateCd}>{option.cdName}</option>
            );
          })
        }
      </select>
      <select name="businessType2-3" id="businessType2-3"
        value={extraHireCate}
        onChange={ (e) => {
          let tmpList = props.state.bizCateCd;

          let tmpIdx = tmpList.findIndex(tmpBizCate => tmpBizCate.id === id);
      
          tmpList[tmpIdx].cateCd = e.target.value;

          props.setState({
            ...props.state,
            bizCateCd: tmpList
          });

          setExtraHireCate(e.target.value);
        }}>
        {
          upCd3Options.map((option, idx) => {
            return(
              <option key={option.cateCd} value={option.cateCd}>{option.cdName}</option>
            );
          })
        }
      </select>
      <button type="button" className="formButton" onClick={() => props.onRemove(id)}>
        <FormattedMessage
          id="delete"
          defaultMessage="삭제하기"
        /> &gt;
      </button>
    </div>
  )
}