import { BusinessTypeExtra } from "./BusinessTypeExtra"

const BusinessTypeList =(props) => {

  return (
    <>
      {props.state.bizCateCd.map((type) => (
        <BusinessTypeExtra
          state={props.state} setState={props.setState} bizCateCd={props.bizCateCd}
          type={type}
          key={type.id}
          onRemove={props.onRemove}
        />
      ))}
    </>
  )
}

export default BusinessTypeList;