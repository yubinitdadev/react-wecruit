import { useEffect, useState } from "react";
import { FormattedMessage, useIntl } from "react-intl";

const BusinessTypeSelect =(props) => {
  const intl = useIntl();
  const [upCd1Val, setUpCd1Val] = useState( (props.state.bizCateSel.upCd1 !== undefined) ? props.state.bizCateSel.upCd1 : "" );
  const [upCd2Val, setUpCd2Val] = useState( (props.state.bizCateSel.upCd2 !== undefined) ? props.state.bizCateSel.upCd2 : "" );
  const [selState, setSelState] = useState({
    bizCateCmplt : false,
    bizCateSelCmplt : false,
    upCd1Val : (props.state.bizCateSel.upCd1 !== undefined) ? props.state.bizCateSel.upCd1 : "",
    upCd2Val : (props.state.bizCateSel.upCd2 !== undefined) ? props.state.bizCateSel.upCd2 : "",
    cateObj : {
      upCdArr: [],
      cateCdArr: [],
      cateNmArr: [],
    }
  });

  const [upCd2Options, setUpCd2Options] = useState([
    { cateCd : "", cdName : "--선택하세요--" }
  ]);
  const [upCd3Options, setUpCd3Options] = useState([
    { cateCd : "", cdName : "--선택하세요--" }
  ]);

  const setCate = (val, lvl) => {
    let upCd = val;
    let tmpList = [];

    for( let i = 0; i < selState.cateObj.upCdArr.length; i++ )
    {
      if( selState.cateObj.upCdArr[i] == upCd )
      {
        tmpList.push({
          cateCd: selState.cateObj.cateCdArr[i],
          cdName: selState.cateObj.cateNmArr[i]
        })
      }
    }

    if(lvl === 1) 
    {
      setUpCd2Options(tmpList);
      setUpCd3Options([
        { cateCd : "", cdName : "--선택하세요--" }
      ]);

      //console.log("UP CD2 VALUE : " + upCd2Val);\

      // 기존에 데이터베이스가 존재하거나 신규일 경우에는 이게 먹히는데...
      // 존재하는 값을 수정할 때는 upCd2Val을 수정하고 setCate해줘야함...

      setSelState({
        ...selState,
        upCd2Val:""
      })
      setCate( (upCd2Val !== "") ? upCd2Val : tmpList[0].cateCd, 2);
    }
    else if(lvl === 2) 
    {
      setUpCd3Options(tmpList);

      props.setState({
        ...props.state,
        bizCateSel: (props.state.bizCateSel.upCd1 !== undefined) ? props.state.bizCateSel.cateCd : tmpList[0].cateCd
      });
    }
  }

  useEffect(()=>{
    let tmpCateObj = {
      upCdArr: [],
      cateCdArr: [],
      cateNmArr: [],
    }

    if(props.bizCateCd.length > 0) 
    {
      let tmpCateObjIdx = 0;

      props.bizCateCd.forEach((cate, idx)=>{
        if( cate.lvl === "2" || cate.lvl === "3")
        {
          tmpCateObj.upCdArr[tmpCateObjIdx] = cate.upCd;
          tmpCateObj.cateCdArr[tmpCateObjIdx] = cate.cateCd;
          tmpCateObj.cateNmArr[tmpCateObjIdx] = cate.cdName;
        }
        tmpCateObjIdx++;
      })

      setSelState({
        ...selState,
        cateObj: tmpCateObj,
        bizCateCmplt : true
      });
    }
  }, [props.bizCateCd])

  useEffect(()=>{
    if(props.state.bizCateSel !== "" && props.state.bizCateSel.upCd1 !== undefined)
    {
      setSelState({
        ...selState,
        upCd1Val : props.state.bizCateSel.upCd1,
        upCd2Val : props.state.bizCateSel.upCd2,
        bizCateSelCmplt : true
      })
    }
  },[props.state.bizCateSel])

  useEffect(()=>{
    if(selState.upCd1Val !== "" && selState.bizCateCmplt && selState.bizCateSelCmplt) 
    {
      setCate(selState.upCd1Val, 1);
    }
  },[selState.upCd1Val, selState.bizCateCmplt, selState.bizCateSelCmplt])

  return (
    <div>
      <span htmlFor="businessType1-1" className="labelSpan">업종구분</span>
      <select name="businessType1-1" id="businessType1-1" 
        value={selState.upCd1Val}
        onChange={ (e) => {
          setCate(e.target.value, 1);
          setSelState({
            ...selState,
            upCd1Val : e.target.value
          })
        }}>
        <option>
          --
            {intl.formatMessage({
              id: "select",
              defaultMessage: "선택하세요"
            })}
          --
        </option>
        {
          props.bizCateCd.map((cate, idx)=>{
            if(cate.lvl === "1") {
              return (
                <option key={cate.cateCd} value={cate.cateCd}>{cate.cdName}</option>
              )
            }
          }) 
        }
      </select>

      <select name="businessType1-2" id="businessType1-2"
        value={selState.upCd2Val}
        onChange={ (e) => {
          setCate(e.target.value, 2); 
          setSelState({
            ...selState,
            upCd2Val : e.target.value 
          })
        }}>
        {
          upCd2Options.map((option, idx) => {
            return(
              <option key={option.cateCd} value={option.cateCd}>{option.cdName}</option>
            );
          })
        }
      </select>

      <select name="businessType1-3" id="businessType1-3"
        value = { (props.state.bizCateSel.upCd1 !== undefined ) ? props.state.bizCateSel.cateCd : props.state.bizCateSel }
        onChange={ (e) => {
          props.setState({
            ...props.state,
            bizCateSel: e.target.value
          })
        }}>
        {
          upCd3Options.map((option, idx) => {
            return(
              <option key={option.cateCd} value={option.cateCd}>{option.cdName}</option>
            );
          })
        }
      </select>

      <button type="button" className="formButton" onClick={props.onClick}>
        <FormattedMessage
          id="add"
          defaultMessage="추가하기"
        /> &gt;
      </button>
    </div>
  );
}

export default BusinessTypeSelect;

