import BusinessTypeSelect from "./BusinessTypeSelect";
import BusinessTypeList from "./BusinessTypeList";
import { useIntl } from "react-intl";

const BusinessTypeTemplate = (props) => {
  const intl = useIntl();
  const onAddBtnClick =() => {
    const type = {
      id: props.bizCateCnt,
    }

    if(props.state.bizCateCd.length > 3){
      alert(intl.formatMessage({
        id: "input.max5",
        defaultMessage: "5개까지 입력 가능합니다!"
      }));
      return false;
    }else {
      props.setState({
        ...props.state,
        bizCateCd: props.state.bizCateCd.concat(type)
      })
    }

    props.setBizCateCnt(props.bizCateCnt + 1);
  }

  const onRemove = (id) => {
    //setTypes(types.filter(type => type.id !== id));
    
    props.setState({
      ...props.state,
      bizCateCd: props.state.bizCateCd.filter(cateCd => cateCd.id !== id)
    })
  };

  return(
    <>
      <BusinessTypeSelect state={props.state} setState={props.setState} bizCateCd={props.bizCateCd} onClick={onAddBtnClick}/>
      <BusinessTypeList onRemove={onRemove} state={props.state} setState={props.setState} bizCateCd={props.bizCateCd}/>
    </>
  )
}

export default BusinessTypeTemplate;