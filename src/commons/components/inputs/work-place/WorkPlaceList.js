import { WorkPlaceExtra } from "./WorkPlaceExtra";

const WorkPlaceList =(props) => {

  return (
    <>
      {props.state.empAddrCd.map((type) => (
        <WorkPlaceExtra
          state={props.state} setState={props.setState} empAddrCateCd={props.empAddrCateCd}
          type={type}
          key={type.id}
          onRemove={props.onRemove}
        />
      ))}
    </>
  )
}

export default WorkPlaceList;