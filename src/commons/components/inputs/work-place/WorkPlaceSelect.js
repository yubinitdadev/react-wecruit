import { useEffect, useState } from "react";
import { FormattedMessage, useIntl } from "react-intl";

const WorkPlaceSelect =(props) => {
  const intl = useIntl();
  const [selState, setSelState] = useState({
    upCd1Val : (props.state.empAddrSel.upCd1 !== undefined) ? props.state.empAddrSel.upCd1 : "",
    empAddrCateCmplt : false,
    empAddrCateSelCmplt : false,
    cateObj : {
      upCdArr: [],
      cateCdArr: [],
      cateNmArr: [],
    }
  })
  
  const [upCd2Options, setUpCd2Options] = useState([
    { cateCd : "", cdName : "--선택하세요--" }
  ]);
  
  const setCate = (val, lvl) => {
    let upCd = val;
    let tmpList = [];

    for( let i = 0; i <= selState.cateObj.upCdArr.length; i++ )
    {
      if( selState.cateObj.upCdArr[i] == upCd )
      {
        tmpList.push({
          cateCd: selState.cateObj.cateCdArr[i],
          cdName: selState.cateObj.cateNmArr[i]
        })
      }
    }

    if(lvl === 1) 
    {
      setUpCd2Options(tmpList);

      props.setState({
        ...props.state,
        empAddrSel: (props.state.empAddrSel.upCd1 !== undefined) ? props.state.empAddrSel.cateCd : tmpList[0].cateCd
      });
    }
  }

  useEffect(()=>{
    let tmpCateObj = {
      upCdArr: [],
      cateCdArr: [],
      cateNmArr: [],
    }

    if(props.empAddrCateCd.length > 0) 
    {
      let tmpCateObjIdx = 0;

      props.empAddrCateCd.forEach((cate, idx)=>{
        if( cate.lvl === "2" || cate.lvl === "3")
        {
          tmpCateObj.upCdArr[tmpCateObjIdx] = cate.upCd;
          tmpCateObj.cateCdArr[tmpCateObjIdx] = cate.cateCd;
          tmpCateObj.cateNmArr[tmpCateObjIdx] = cate.cdName;
        }
        tmpCateObjIdx++;
      })

      setSelState({
        ...selState,
        cateObj: tmpCateObj,
        empAddrCateCmplt : true
      });
    }
  }, [props.empAddrCateCd])

  useEffect(()=>{
    if(props.state.empAddrSel !== "" && props.state.empAddrSel.upCd1 !== undefined)
    {
      setSelState({
        ...selState,
        upCd1Val : props.state.empAddrSel.upCd1,
        empAddrCateSelCmplt : true
      })
    }
  },[props.state.empAddrSel])

  useEffect(()=>{
    if(selState.upCd1Val !== "" && selState.empAddrCateCmplt && selState.empAddrCateSelCmplt) 
    {
      setCate(selState.upCd1Val, 1);
    }
  },[selState.upCd1Val, selState.empAddrCateCmplt, selState.empAddrCateSelCmplt])

  return (
    <div>
      <span htmlFor="businessType1-1" className="labelSpan">근무예정지</span>
      <select name="businessType1-1" id="businessType1-1"
        value={selState.upCd1Val}
        onChange={ (e) => {
          setCate(e.target.value, 1);
          setSelState({
            ...selState,
            upCd1Val: e.target.value
          }) 
        }}>
        <option>
          --
            {intl.formatMessage({
              id: "select",
              defaultMessage: "선택하세요"
            })}
          --
        </option>
        {
          props.empAddrCateCd.map((cate, idx)=>{
            if(cate.lvl === "1") {
              return (
                <option key={cate.cateCd} value={cate.cateCd}>{cate.cdName}</option>
              )
            }
          }) 
        }
      </select>

      <select name="businessType1-2" id="businessType1-2"
        value = { (props.state.empAddrSel.upCd1 !== undefined ) ? props.state.empAddrSel.cateCd : props.state.empAddrSel }
        onChange={ (e) => {
          props.setState({
            ...props.state,
            empAddrSel: e.target.value
          })
        }}>
        {
          upCd2Options.map((option, idx) => {
            return(
              <option key={option.cateCd} value={option.cateCd}>{option.cdName}</option>
            );
          })
        }
      </select>

      <button type="button" className="formButton" onClick={props.onClick}>
        <FormattedMessage
          id="add"
          defaultMessage="추가하기"
        />  &gt;
      </button>
    </div>
  );
}

export default WorkPlaceSelect;

