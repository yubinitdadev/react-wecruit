import { useEffect, useState } from "react";
import { FormattedMessage, useIntl } from "react-intl";

export const WorkPlaceExtra =(props) => {
  const { id } = props.type;
  const intl = useIntl();

  const [upCd1Val, setUpCd1Val] = useState( (props.state.empAddrCd[id] !== undefined) ? props.state.empAddrCd[id].upCd1 : "" );
  const [upCd2Options, setUpCd2Options] = useState([
    { cateCd : "", cdName : "--선택하세요--" }
  ]);

  const [extraEmpAddrCate, setExtraEmpAddrCate] = useState("");
  
  let cateObj = {
    upCdArr: [],
    cateCdArr: [],
    cateNmArr: []
  }

  let tmpIdx = 0;

  props.empAddrCateCd.forEach((cate, idx)=>{
    if( cate.lvl === "2")
    {
      cateObj.upCdArr[tmpIdx] = cate.upCd;
      cateObj.cateCdArr[tmpIdx] = cate.cateCd;
      cateObj.cateNmArr[tmpIdx] = cate.cdName;
    }

    tmpIdx++;
  })

  const setCate = (val, lvl) => {
    let upCd = val;
    let tmpList = [];

    for( let i = 0; i <= tmpIdx; i++ )
    {
      if( cateObj.upCdArr[i] == upCd )
      {
        tmpList.push({
          cateCd: cateObj.cateCdArr[i],
          cdName: cateObj.cateNmArr[i]
        })
      }
    }

    if(lvl === 1) 
    {
      setUpCd2Options(tmpList);
      
      let tmpEmpAddrCateList = [ ...props.state.empAddrCd ];

      let tmpIdx = tmpEmpAddrCateList.findIndex(tmpEmpAddrCate => tmpEmpAddrCate.id === id);

      if( tmpIdx === -1 ) tmpEmpAddrCateList[tmpIdx]["cateCd"] = tmpList[0].cateCd;
      else tmpEmpAddrCateList[tmpIdx].cateCd = (tmpEmpAddrCateList[tmpIdx].upCd1 !== undefined) ?
                                                      tmpEmpAddrCateList[tmpIdx].cateCd :tmpList[0].cateCd;
      
      props.setState({
        ...props.state,
        empAddrCd: tmpEmpAddrCateList
      });

      setExtraEmpAddrCate( (tmpEmpAddrCateList[tmpIdx].upCd1 !== "") ? tmpEmpAddrCateList[tmpIdx].cateCd : tmpList[0].cateCd);
    }
  }

  useEffect(()=>{
    if(upCd1Val !== "" && upCd1Val !== undefined)
    {
      if(props.empAddrCateCd.length > 0) 
      {
        setCate(upCd1Val, 1);
      }
    }
  },[props.empAddrCateCd])

  return(
    <div className="extraInput">
      <span className="labelSpan" />

      <select name="businessType2-1" id="businessType2-1"
        value={upCd1Val}
        onChange={ (e) => {
          setCate(e.target.value, 1);
          setUpCd1Val(e.target.value); 
        }}>
        <option>
          --
            {intl.formatMessage({
              id: "select",
              defaultMessage: "선택하세요"
            })}
          --
        </option>
        {
          props.empAddrCateCd.map((cate, idx)=>{
            if(cate.lvl === "1") {
              return (
                <option key={cate.cateCd} value={cate.cateCd}>{cate.cdName}</option>
              )
            }
          }) 
        }
      </select>
      <select name="businessType2-2" id="businessType2-2"
        value={extraEmpAddrCate}
        onChange={ (e) => {
          let tmpList = [...props.state.empAddrCd];

          let tmpIdx = tmpList.findIndex(tmpEmpAddrCate => tmpEmpAddrCate.id === id);

          tmpList[tmpIdx].cateCd = e.target.value;

          props.setState({
            ...props.state,
            empAddrCd: tmpList
          });

          setExtraEmpAddrCate(e.target.value);
        }}>
        {
          upCd2Options.map((option, idx) => {
            return(
              <option key={option.cateCd} value={option.cateCd}>{option.cdName}</option>
            );
          })
        }
      </select>
      <button type="button" className="formButton" onClick={() => props.onRemove(id)}>
        <FormattedMessage
          id="delete"
          defaultMessage="삭제하기"
        /> &gt;
      </button>
    </div>
  )
}