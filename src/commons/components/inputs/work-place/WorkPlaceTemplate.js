import { useRef, useState } from "react";
import { useIntl } from "react-intl";
import { WorkPlaceExtra } from "./WorkPlaceExtra";
import WorkPlaceList from "./WorkPlaceList";
import WorkPlaceSelect from "./WorkPlaceSelect";

const WorkPlaceTemplate = (props) => {
  const intl = useIntl();

  const onAddBtnClick =() => {
    const type = {
      id: props.empAddrCateCnt,
    }
    
    if(props.state.empAddrCd.length > 3){
      alert(intl.formatMessage({
        id: "input.max5",
        defaultMessage: "5개까지 입력 가능합니다!"
      }));
      return false;
    }else {
      props.setState({
        ...props.state,
        empAddrCd: props.state.empAddrCd.concat(type)
      })
    }

    props.setEmpAddrCateCnt(props.empAddrCateCnt + 1);
  }

  const onRemove = (id) => {
    //setTypes(types.filter(type => type.id !== id));

    props.setState({
      ...props.state,
      empAddrCd: props.state.empAddrCd.filter(cateCd => cateCd.id !== id)
    })
  };

  return(
    <>
      <WorkPlaceSelect state={props.state} setState={props.setState} empAddrCateCd={props.empAddrCateCd} onClick={onAddBtnClick}/>
      <WorkPlaceList onRemove={onRemove} state={props.state} setState={props.setState} empAddrCateCd={props.empAddrCateCd}/>
    </>
  )
}

export default WorkPlaceTemplate ;