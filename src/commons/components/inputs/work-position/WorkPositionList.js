import { WorkPositionExtra } from "./WorkPositionExtra";

const WorkPositionList =(props) => {

  return (
    <>
      {props.state.workPosCd.map((type) => (
        <WorkPositionExtra
          state={props.state} setState={props.setState} workPosCateCd={props.workPosCateCd}
          type={type}
          key={type.id}
          onRemove={props.onRemove}
        />
      ))}
    </>
  )
}

export default WorkPositionList;