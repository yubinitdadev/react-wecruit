import { useRef, useState } from "react";
import { useIntl } from "react-intl";
import { WorkPositionExtra } from "./WorkPositionExtra";
import WorkPositionList from "./WorkPositionList";
import WorkPositionSelect from "./WorkPositionSelect";

const WorkPositionTemplate =(props) => {
  const intl = useIntl();

  const onAddBtnClick =(e) => {
    const type = {
      id: props.workPosCateCnt,
    }
    
    if(props.state.workPosCd.length > 3){
      alert(intl.formatMessage({
        id: "input.max5",
        defaultMessage: "5개까지 입력 가능합니다!"
      }));
      return false;
    }else {
      props.setState({
        ...props.state,
        workPosCd: props.state.workPosCd.concat(type)
      })
    }

    props.setWorkPosCateCnt(props.workPosCateCnt + 1);
  }

  const onRemove = (id) => {
    //setTypes(types.filter(type => type.id !== id));

    props.setState({
      ...props.state,
      workPosCd: props.state.workPosCd.filter(cateCd => cateCd.id !== id)
    })
  };

  return(
    <>
      <WorkPositionSelect state={props.state} setState={props.setState} workPosCateCd={props.workPosCateCd} onClick={onAddBtnClick}/>
      <WorkPositionList onRemove={onRemove} state={props.state} setState={props.setState} workPosCateCd={props.workPosCateCd}/>
    </>
  )
}

export default WorkPositionTemplate ;