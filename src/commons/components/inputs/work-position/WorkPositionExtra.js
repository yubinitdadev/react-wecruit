import { useEffect, useState } from "react";
import { FormattedMessage, useIntl } from "react-intl";

export const WorkPositionExtra =(props) => {
  const intl = useIntl();
  const { id } = props.type;

  const [upCd1Val, setUpCd1Val] = useState( (props.state.workPosCd[id] !== undefined) ? props.state.workPosCd[id].upCd1 : "" );
  const [upCd2Options, setUpCd2Options] = useState([
    { cateCd : "", cdName : "--선택하세요--" }
  ]);

  const [extraWorkPosCate, setExtraWorkPosCate] = useState("");
  
  let cateObj = {
    upCdArr: [],
    cateCdArr: [],
    cateNmArr: []
  }

  let tmpIdx = 0;

  props.workPosCateCd.forEach((cate, idx)=>{
    if( cate.lvl === "2")
    {
      cateObj.upCdArr[tmpIdx] = cate.upCd;
      cateObj.cateCdArr[tmpIdx] = cate.cateCd;
      cateObj.cateNmArr[tmpIdx] = cate.cdName;
    }

    tmpIdx++;
  })

  const setCate = (val, lvl) => {
    let upCd = val;
    let tmpList = [];

    for( let i = 0; i <= tmpIdx; i++ )
    {
      if( cateObj.upCdArr[i] == upCd )
      {
        tmpList.push({
          cateCd: cateObj.cateCdArr[i],
          cdName: cateObj.cateNmArr[i]
        })
      }
    }

    if(lvl === 1) 
    {
      setUpCd2Options(tmpList);
      
      let tmpWorkPosCateList = [ ...props.state.workPosCd ];

      let tmpIdx = tmpWorkPosCateList.findIndex(tmpWorkPosCate => tmpWorkPosCate.id === id);

      if( tmpIdx === -1 ) tmpWorkPosCateList[tmpIdx]["cateCd"] = tmpList[0].cateCd;
      else tmpWorkPosCateList[tmpIdx].cateCd = (tmpWorkPosCateList[tmpIdx].upCd1 !== undefined) ?
                                                tmpWorkPosCateList[tmpIdx].cateCd : tmpList[0].cateCd;
      
      props.setState({
        ...props.state,
        workPosCd: tmpWorkPosCateList
      });

      setExtraWorkPosCate( (tmpWorkPosCateList[tmpIdx].upCd1 !== "") ? tmpWorkPosCateList[tmpIdx].cateCd : tmpList[0].cateCd );
    }
  }

  useEffect(()=>{
    if(upCd1Val !== "" && upCd1Val !== undefined)
    {
      if(props.workPosCateCd.length > 0) 
      {
        setCate(upCd1Val, 1);
      }
    }
  },[props.workPosCateCd])

  return(
    <div className="extraInput">
      <span className="labelSpan" />

      <select name="position2-1" id="position2-1"
        value={upCd1Val}
        onChange={ (e) => {
          setCate(e.target.value, 1); 
          setUpCd1Val(e.target.value);
        }}>
        <option>
          --
            {intl.formatMessage({
              id: "select",
              defaultMessage: "선택하세요"
            })}
          --
        </option>
        {
          props.workPosCateCd.map((cate, idx)=>{
            if(cate.lvl === "1") {
              return (
                <option key={cate.cateCd} value={cate.cateCd}>{cate.cdName}</option>
              )
            }
          }) 
        }
      </select>
      <select name="position2-2" id="position2-2"
        value={extraWorkPosCate}
        onChange={ (e) => {
          let tmpList = [...props.state.workPosCd];

          let tmpIdx = tmpList.findIndex(tmpWorkPosCate => tmpWorkPosCate.id === id);

          tmpList[tmpIdx].cateCd = e.target.value;

          props.setState({
            ...props.state,
            workPosCd: tmpList
          });

          setExtraWorkPosCate(e.target.value);
        }}>
        {
          upCd2Options.map((option, idx) => {
            return(
              <option key={option.cateCd} value={option.cateCd}>{option.cdName}</option>
            );
          })
        }
      </select>
      <button type="button" className="formButton" onClick={() => props.onRemove(id)}>
        <FormattedMessage
          id="delete"
          defaultMessage="삭제하기"
        /> &gt;
      </button>
    </div>
  )
}