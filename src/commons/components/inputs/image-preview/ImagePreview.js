import { FormattedMessage } from "react-intl"

export const ImagePreview =({children, previewButton, fileDownloadButton, thmnl, atchFiles}) => {
  return(
    <li className="adminFileBox">
      <span className="labelSpan">{children}</span>
      <div className="flex">
        <div className="flex">
          <label htmlFor="imgFile" className="basicButton">
            <FormattedMessage
              id="image.attach"
              defaultMessage="이미지 첨부"
            /> &gt;
          </label>
          <span className="fileName"></span>
          <input type="file" id="imgFile" className="fileInput" />
        </div>

        {previewButton && <div>
          <button type="button" className="basicButton">
            <FormattedMessage
              id="see.picture"
              defaultMessage="사진보기"
            />
          </button>
        </div>}

        {fileDownloadButton && <div>
          <button type="button" className="basicButton">
            <FormattedMessage
              id="file.download"
              defaultMessage="파일다운"
            />
          </button>
        </div>}
        
      </div>
    </li>
  )
}