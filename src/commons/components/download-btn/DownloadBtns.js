
// import { CopyToClipboard } from "react-copy-to-clipboard";
import { useReactToPrint } from "react-to-print";
import { CSVLink } from "react-csv";
import jsPDF from "jspdf";
import html2canvas from "html2canvas";
import { toast } from "react-toastify";
import { useState } from "react";

export const DownloadBtns = ({dataset, headers, fileName, componentRef}) => {
  // const handlePrint = useReactToPrint({
  //   content: () => componentRef.current,
  // });

  let items = "";

  for (let i = 0; i < headers.length; i++) {
    items = items.concat(headers[i].label + "\t\t");
  }

  for (let i = 0; i < dataset.length; i++) {
    items = items.concat("\n");
    items = items.concat(Object.values(dataset[i]));
  }

  // const [copiedContents, setCopiedContents] = useState(items);

  // const copyAlert = () => toast.success("Copied to clipboard");
  // const csvAlert = () => toast.success("CSV downloaded");
  const excelAlert = () => toast.success("Excel downloaded");
  // const pdfAlert = () => {
  //   toast.success("PDF downloaded");
  //   exportPdf();
  // };
  // const printAlert = () => {
  //   toast.info("Preparing to print");
  //   handlePrint();
  // };

  // const exportPdf = () => {
  //   html2canvas(document.querySelector("#capture")).then((canvas) => {
  //     // document.body.appendChild(canvas); 
      
  //     const imgData = canvas.toDataURL("image/png");
  //     const pdf = new jsPDF("p", "pt", "a4", false);
  //     pdf.addImage(imgData, "PNG", 0, 0, 600, 0, undefined, false);
  //     pdf.save(`${fileName}.pdf`);
  //   });
  // };

  return(

    <div className="downloadBtns">
      {/* <CopyToClipboard onCopy={copyAlert} text={copiedContents}>
        <button type="button">Copy</button>
      </CopyToClipboard>
      <CSVLink
        onClick={csvAlert}
        filename={`${fileName}.csv`}
        data={dataset}
        headers={headers}
      >
        <button type="button">CSV</button>
      </CSVLink> */}

      <CSVLink
        onClick={excelAlert}
        filename={`${fileName}.xls`}
        data={dataset}
        headers={headers}
      >
        <button type="button">Excel</button>
      </CSVLink>

      {/* <button onClick={pdfAlert} type="button">
        PDF
      </button>
      <button onClick={printAlert} type="button">
        Print
      </button> */}
    </div>
  )
}