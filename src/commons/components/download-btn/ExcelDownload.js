import { CSVLink } from "react-csv"
import { toast } from "react-toastify";

export const ExcelDownload = ({dataset, headers, fileName}) => {
  const excelAlert = () => toast.success("Excel downloaded");

  return(
    <div className="downloadBtns">
      <CSVLink
        onClick={excelAlert}
        filename={`${fileName}.xls`}
        data={dataset}
        headers={headers}
      >
        <button type="button">Excel</button>
      </CSVLink>
    </div>
  )
}