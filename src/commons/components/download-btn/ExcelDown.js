import { CSVLink } from "react-csv"
import { toast } from "react-toastify";

export const ExcelDown = ({dataset, headers, fileName}) => {
  const excelAlert = () => toast.success("Excel downloaded");

  return(
    <div className="downloadBtns">
      <CSVLink
        onClick={excelAlert}
        filename={`${fileName}.xls`}
        data={dataset}
        headers={headers}
      >
        <button type="button" style="display:'hidden'">Excel</button>
      </CSVLink>
    </div>
  )
}