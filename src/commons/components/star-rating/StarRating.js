export const StarRating = ({RatingNum}) => {
  return (
    <div className="stars">
      <span className="material-icons-outlined active">
        grade
      </span>
      <span className="material-icons-outlined active">
        grade
      </span>
      <span className="material-icons-outlined active">
        grade
      </span>
      <span className="material-icons-outlined">
        grade
      </span>
      <span className="material-icons-outlined">
        grade
      </span>
      {RatingNum && <span>3.0 / 5.0</span>}
    </div>
  )
}