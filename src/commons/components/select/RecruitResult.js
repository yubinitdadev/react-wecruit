import { useIntl } from "react-intl";

export const RecruitResult = () => {
  const intl = useIntl();

  return (
    <>
      <select className="recruitResult">
        <option>
          ::{" "}
          {intl.formatMessage({
            id: "recruit.step",
            defaultMessage: "채용단계",
          })}{" "}
          ::
        </option>
        <option className="passed">
          {intl.formatMessage({
            id: "document.pass",
            defaultMessage: "서류 합격",
          })}
        </option>
        <option>
          {intl.formatMessage({
            id: "document.fail",
            defaultMessage: "서류 불합격",
          })}
        </option>
        <option className="passed">
          {intl.formatMessage({
            id: "interview.pass",
            defaultMessage: "면접 합격",
          })}
        </option>
        <option>
          {intl.formatMessage({
            id: "interview.fail",
            defaultMessage: "면접 불합격",
          })}
        </option>
        <option className="passed">
          {intl.formatMessage({
            id: "interview1.pass",
            defaultMessage: "1차 면접 합격",
          })}
        </option>
        <option>
          {intl.formatMessage({
            id: "interview1.fail",
            defaultMessage: "1차 면접 불합격",
          })}
        </option>
        <option className="passed">
          {intl.formatMessage({
            id: "interview1.pass",
            defaultMessage: "1차 면접 합격",
          })}
        </option>
        <option>
          {intl.formatMessage({
            id: "interview2.fail",
            defaultMessage: "2차 면접 불합격",
          })}
        </option>
        <option className="passed">
          {intl.formatMessage({
            id: "aptitude.pass",
            defaultMessage: "인적성 검사 합격",
          })}
        </option>
        <option>
          {intl.formatMessage({
            id: "aptitude.fail",
            defaultMessage: "인적성 검사 불합격",
          })}
        </option>
        <option className="passed">
          {intl.formatMessage({
            id: "physical.test.pass",
            defaultMessage: "신체검사 합격",
          })}
        </option>
        <option>
          {intl.formatMessage({
            id: "physical.test.fail",
            defaultMessage: "신체검사 불합격",
          })}
        </option>

        <option className="passed">
          {intl.formatMessage({
            id: "deal.discussion.done",
            defaultMessage: "처우 협의 종료",
          })}
        </option>
        <option className="passed">
          {intl.formatMessage({
            id: "final.employment.confirm",
            defaultMessage: "최종 채용 확정",
          })}
        </option>
      </select>
    </>
  );
};
