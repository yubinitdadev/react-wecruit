import { useEffect, useState } from "react"

export const Pagination =(props) => {
  const postsPerPage = props.postsPerPage
  const totalPosts = props.totalPosts
  const paginate = props.paginate
  const currentPage = props.currentPage

  const [currentPageGrp, setCurrentPageGrp] = useState(1);
  const [pageSize, setPageSize] = useState(10);

  const indexOfLast = currentPageGrp * pageSize;
  const indexOfFirst = indexOfLast - pageSize;

  let pageNumbers = [];

  for (let i = 1; i <= Math.ceil(totalPosts / postsPerPage); i++) {
      pageNumbers.push(i);
  }

  const totalPageNum = pageNumbers.length;

  const goPrevPage = () => {
      paginate(currentPage - 1);
  }

  const goPrevPageGrp = () => {
      paginate(indexOfLast - 10);
  }
  
  const goNextPage = () => {
      paginate(currentPage + 1);
  }

  const goNextPageGrp = () => {
      paginate(indexOfFirst + 11);
  }

  const currentPages = (tmp) => {
      let currentPages = 0;
      currentPages = tmp.slice(indexOfFirst, indexOfLast);
      return currentPages;
  }

  pageNumbers = currentPages(pageNumbers);

  const inactiveBtnStyle = {
      cursor: "default",
      opacity: "0.3"
  }

  useEffect(() => {
      console.log("CURRENT PAGE : " + currentPage);
    
      setCurrentPageGrp( Math.ceil( currentPage / pageSize ) === 0 ? 1 : Math.ceil( currentPage / pageSize ) );
  }, [currentPage])


  return(
    <div className="paginationPub">
      <ul>
        <li onClick={currentPage - 10 <= 0 ? undefined : goPrevPageGrp}>
          <span className="material-icons" style={currentPage - 10 <= 0 ? inactiveBtnStyle : undefined}>
            keyboard_double_arrow_left
          </span>
        </li>
        <li onClick={(currentPage === 1 || currentPage === 0) ? undefined : goPrevPage}>
          <span className="material-icons" style={(currentPage === 1 || currentPage === 0) ? inactiveBtnStyle : undefined}>
            chevron_left
          </span>
        </li>

        {
          pageNumbers.map((number) => (
              <li
                  className={currentPage === number
                      ? 'active'
                      : ''}
                  key={number}
                  onClick={() => {
                      paginate(number);
                  }}>{number}</li>
          ))
        }

        <li onClick={(totalPageNum === currentPage || totalPageNum === 0) ? undefined : goNextPage}>
          <span className="material-icons" 
            style={(totalPageNum === currentPage || totalPageNum === 0) ? inactiveBtnStyle  : undefined}>
            navigate_next
          </span>
        </li>
        <li onClick={indexOfFirst + 11 > totalPageNum ? undefined : goNextPageGrp}>
          <span className="material-icons" style={indexOfFirst + 11 > totalPageNum ? inactiveBtnStyle : undefined}>
            keyboard_double_arrow_right
          </span>
        </li>
      </ul>
    </div>
  )
}