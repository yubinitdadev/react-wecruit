import { useEffect, useState } from "react"

export const Paginavi =(props) => {
  
  const totRowCnt = props.page.totRowCnt;
  const pageSize = props.page.pageSize;
  const pageUnit = props.page.pageUnit;
  const currentPage = props.page.currentPage;  
  const pageCnt = props.page.pageCnt;  
  const setCurrentPage = props.setCurrentPage;
  const goList = props.goList;
      
  let pageNumbers = [];

  for (let i = 1; i <= Math.ceil(pageCnt); i++) {
      pageNumbers.push(i);
  }

  const goToPage = (pageNum) => {
        
    if( pageNum > pageCnt)pageNum = pageCnt;
    else if( pageNum < 1)pageNum = 1;

    props.setPage(
      {
        ...props.page
        ,currentPage: pageNum
      }
    )
    
    goList();
}

  return(
    <div className="paginationPub">
      <ul>
        <li >
          <span className="material-icons" onClick={() => {goToPage(1);}}>
            keyboard_double_arrow_left
          </span>
        </li>
        <li onClick={() => {goToPage(currentPage-1);}}>
          <span className="material-icons" >
            chevron_left
          </span>
        </li>

        {
          pageNumbers.map((number) => (
              <li
                  className={currentPage === number
                      ? 'active'
                      : ''}
                  key={number}
                  onClick={() => {
                     goToPage(number);
                  }}>{number}</li>
          ))
        }

        <li onClick={() => {goToPage(currentPage+1);}}>
          <span className="material-icons" 
           >
            navigate_next
          </span>
        </li>
        <li onClick={() => {goToPage(pageCnt);}}>
          <span className="material-icons" >
            keyboard_double_arrow_right
          </span>
        </li>
      </ul>
    </div>
  )
}