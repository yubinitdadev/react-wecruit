import { useEffect, useState } from "react";
import DaumPost from "api-source/DaumPost";
import Modal from "modal-all/ModalAll";
import { FormattedMessage, useIntl } from "react-intl";

export const AddressInput =(props) => {
  const intl = useIntl();
  const [addressPopup, setAddressPopup] = useState(false);
  const [addressValue, setAddressValue] = useState('주소를 검색해주세요');
  const [disabled, setDisabled] = useState(true);
  const [modalVisible1, setModalVisible1] = useState(false)
 	// 팝업창 열기
  const handleAddressPopup =() => {
    setAddressPopup(true);
    setModalVisible1(true)
  }

 	// 팝업창 닫기
  const closePostCode = () => {
    setAddressPopup(false)
  }

  useEffect(() => {
    props.setState({
      ...props.state,
      bizAddr: addressValue
    })
  }, [addressValue])

  useEffect(() => {
    if(props.state.bizAddr !== "")
    {
      setAddressValue(props.state.bizAddr);
    }

    if(props.state.bizAddrDtl !== "")
    {
      setDisabled(false);
    }
  },[props.state])

  return (
    <>
      <ul className="oneCol">
        <li>
          <span className="labelSpan">
            <FormattedMessage
              id="company.address"
              defaultMessage="회사주소"
            />
          </span>
          <div className='flexWrap flexGap' style={{display: 'flex'}}>
            <span className="searchAddress" onClick={handleAddressPopup}>
              <FormattedMessage
                id="search.address"
                defaultMessage="주소검색"
              /> &gt;
            </span>
            <input type="text" className='fullWidth' 
              value={addressValue}
              onChange={(e) => {
              }}/>
            <input 
              type="text" 
              placeholder={intl.formatMessage({
                id: "description.address.detail",
                defaultMessage: "상세 주소를 입력해주세요."
              })}
              className='fullWidth detailAddress' 
              value={props.state.bizAddrDtl}
              disabled={disabled}
              onChange={(e) => {
                props.setState({
                  ...props.state,
                  bizAddrDtl: e.target.value
                })
              }}/>
          </div>
        </li>
      </ul>
      { addressPopup && 
        <Modal visible={modalVisible1} closable={true} maskClosable={true} nobutton={true} classAdd="mediumWidth" >
          <DaumPost address={setAddressValue} disabledControl={setDisabled} onClose={closePostCode}/>
        </Modal>
      }
    </>
  )
}