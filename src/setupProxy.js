const { createProxyMiddleware } = require("http-proxy-middleware");

module.exports = function (app) {
  app.use(
    createProxyMiddleware("/api", {
      target: "http://13.125.230.176:9080",
      //target: "http://localhost:9080",
      changeOrigin: true,
    })
  );
};
