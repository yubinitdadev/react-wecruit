import { useState, useEffect } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";


import { Login } from "../src/login/join/LoginMain";
import { CndtCntctLink } from "./dlink/CndtCntctLink";
import { QnaLink } from "./dlink/QnaLink";
import { WorkContract } from "./workcontract/WorkContract";


// scss styling import
import "./commons/scss/master-all.scss";
import "./commons/scss/table-all.scss";
import "./commons/scss/header-all.scss";
import "./commons/scss/footer-all.scss";
import "./commons/scss/form-all.scss";

import { JoinRecruiters } from "../src/login/join/RecruiterJoin";
import AdminIndex from "./admin/AdminIndex";
import RecruitersIndex from "./recruiters/RecruitersIndex";
import EmployersIndex from "./employers/EmployersIndex";
import { JoinEmployers } from "./login/join/EmployerJoin";
import { JoinComplete } from "login/join/JoinComplete";
import { Provider } from "contexts";

// locale
import { IntlProvider } from "react-intl";
import commonEnUs from 'lang/common-en-US.json';
import commonKo from 'lang/common-ko.json';
import adminEnUs from 'admin/lang/admin-en-US.json';
import adminKo from 'admin/lang/admin-ko.json';
import employerEnUs from "employers/lang/employer-en-US.json";
import employerKo from "employers/lang/employer-ko.json";
import recruiterEnUs from "recruiters/lang/recruiter-en-US.json";
import recruiterKo from "recruiters/lang/recruiter-ko.json";

function App() {
  const [user, setUser] = useState(false);
  const [userType, setUserType] = useState("common")

  // TODO: setting default language here
  const locale = localStorage.getItem("locale") ?? "ko";

  const adminKorean = {...adminKo, ...commonKo}
  const adminEnglish = {...adminEnUs, ...commonEnUs}

  const employerKorean = {...employerKo, ...commonKo}
  const employerEnglish = {...employerEnUs, ...commonEnUs}

  const recruiterKorean = {...recruiterKo, ...commonKo}
  const recruiterEnglish = {...recruiterEnUs, ...commonEnUs}

  let message = {}

  switch (userType){
    case 'admin' :
      message = { "en-US": adminEnglish, "ko": adminKorean }[locale];
      break;
    case 'employer' :
      message = { "en-US": employerEnglish, "ko": employerKorean }[locale];
      break;
    case 'recruiter' :
      message = { "en-US": recruiterEnglish, "ko": recruiterKorean }[locale];
      break;
    case 'common':
      message = { "en-US": commonEnUs, "ko": commonKo }[locale];
      break;
    default :
      return null
  }

  return (
    <Provider>
      <BrowserRouter>
        <IntlProvider locale={locale} defaultLocale='ko' messages={message}>
          <Routes>
            
          
            
          <Route path="/workcontract" element={<WorkContract />} />
          <Route path="/CndtCntctLink" element={<CndtCntctLink />} />
          <Route path="/qnalink" element={<QnaLink />} />
          
            
            
            <Route path="/" element={<Login />} />
            {/* {user && (
              <>
                {PrivateRoutes.map((route, index) => {
                  return (
                    <Route key={index} path={route.path} element={route.element} />
                  );
                })}
              </>
            )} */}
            <Route path="/admin/*" element={<AdminIndex />} />

            <Route path="/employers/*" element={<EmployersIndex />} />

            <Route path="/recruiters/*" element={<RecruitersIndex />} />

            <Route path="/join-recruiter" element={<JoinRecruiters />} />
            <Route path="/join-employer" element={<JoinEmployers />} />
            
            <Route path='/join-complete' element={<JoinComplete/>}/>

            {/* <Route path="*" element={<LoginRequired />} /> */}
          </Routes>
        </IntlProvider>
      </BrowserRouter>
    </Provider>
  );
}
export default App;