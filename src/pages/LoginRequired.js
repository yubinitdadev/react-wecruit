import { useNavigate } from "react-router-dom";

export const LoginRequired = () => {
  const navigate = useNavigate();
  return (
    <div>
      <h1>Please login to use this page.</h1>
      <button onClick={() => navigate("/")} className="basicButton">Go to login page</button>
    </div>
  );
};
