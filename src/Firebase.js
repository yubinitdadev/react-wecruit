import { initializeApp } from "firebase/app";
// import firebase from 'firebase/app'
require('firebase/auth')

const firebaseConfig = {
  apiKey: "AIzaSyCvJMGN5HNDev5v01ltwW0HgrMsXj9HKlE",
  authDomain: "wecruit-c29a5.firebaseapp.com",
  databaseURL: "https://wecruit-c29a5-default-rtdb.firebaseio.com",
  projectId: "wecruit-c29a5",
  storageBucket: "wecruit-c29a5.appspot.com",
  messagingSenderId: "286225752863",
  appId: "1:286225752863:web:54976bbe4322bcedd1062a"
};

const app = initializeApp(firebaseConfig);

export default app;