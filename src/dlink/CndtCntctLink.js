
import {Link, useLocation, useNavigate} from 'react-router-dom';
import { useContext, useEffect, useState } from 'react';
import { Context } from 'contexts';
import queryString from "query-string";

export const CndtCntctLink =() => {
  
  let PROC_CHK = ""

  const {
    state : {
        server,
        mbr
    },
    dispatch,
  } = useContext(Context);


  const location = useLocation();
  const navigate = useNavigate();
  const moment = require('moment');

  const [linkPath, setLinkPath] = useState("");
  
  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid")
    ,ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi")
  };

  const params = queryString.parse(location.search);

  useEffect(async () => {
    console.log("Page First Load EFFECT");
    initSetHandle();          
  },[])

  async function initSetHandle() {
    
    //if( params.mbrDivi == "emp" )setLinkPath("/employers/qna-manage");
    //else if( params.mbrDivi == "rec" )setLinkPath("/recruiters/qna");

    document.getElementById("drctLink").click();
    
  };
    
  
  return(
    <main className="employersMain">
        <section className="recruitLists">
          <div className="ingList">
            <h3 className="sectionTitle">
            <Link to={
                (params.mbrDivi == "rec") ? ('/recruiters/candidate-contact'):
                ('/')
              } 
              id="drctLink"
              state={{ hireUid: params.hireUid, rcmndUid: params.rcmndUid }} >
              <span style={{display:'none'}}>
                전화번호공개 <span>바로가기 -- {params.hireUid} -- {params.mbrDivi} -- {params.rcmndUid}</span>
              </span>
              
            </Link>
            </h3>
          </div>
         
        </section>
      </main>
  );
}