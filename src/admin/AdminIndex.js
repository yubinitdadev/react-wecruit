import { Link, Route, Routes } from "react-router-dom";
import { useEffect, useState } from "react";

import "./components/scss/form.scss";

import { Header } from "./components/common/Header";
import { Footer } from "./components/common/Footer";
import { Sidebar } from "./components/common/Sidebar";

// 서브 페이지
import { Employers } from "./components/employers/employers/Employers";
import { Departments } from "./components/employers/departments/Departments";
import { SearchFirm } from "./components/employers/searchFirm/SearchFirm";
import { Recruiters } from "./components/recruiters/recruiters/Recruiters";
import { Statics } from "./components/recruiters/statics/Statics";
import { Confirm } from "./components/postings/confirm/Confirm";
import { Done } from "./components/postings/done/Done";
import { Ing } from "./components/postings/ing/Ing";
import { Job } from "./components/postings/job/Job";
import { Temporary } from "./components/postings/temporary/Temporary";
import { Activity } from "./components/cr/Activity";
import { Case } from "./components/cr/Case";
import { Project } from "./components/cr/Project";
import { Notice } from "./components/board/Notice";
import { IndicatorStats } from "./components/kpi/IndicatorStats";

// 상세 페이지
import { DepartmentsDetail } from "./components/employers/departments/DepartmentsDetail";
import { EmployersDetail } from "./components/employers/employers/EmployersDetail";
import { RecruitersDetail } from "./components/recruiters/recruiters/RecruitersDetail";
import { JobDetail } from "./components/postings/job/JobDetail";
import { TempDetail } from "./components/postings/temporary/TempDetail";
import { IngDetail } from "./components/postings/ing/IngDetail";
import { ConfirmDetail } from "./components/postings/confirm/ConfirmDetail";
import { NoticeAdd } from "./components/board/NoticeAdd";
import { NoticeEdit } from "./components/board/NoticeEdit";
import { DoneDetail } from "./components/postings/done/DoneDetail";
import { ConfirmForm } from "./components/postings/confirm/ConfirmForm";

// 관리 페이지
import { Recruiter } from "./components/manage/Recruiter";
import { Candidate } from "./components/manage/Candidate";
import { Hamburger } from "./components/common/Hamburger";

import wecruitLogo from "./components/images/logo.png";
// import { PageNotFound } from "../pages/PageNotFound";

import { Main } from "./components/Main";
import { RecruiterStatics } from "./components/recruiters/statics/RecruiterStatics";
import { Qna } from "./components/manage/Qna";
import { RecruiterInfo } from "./components/manage/RecruiterInfo";
import { Corporate } from "./components/postings/corporate/Corporate";
import { CorporateAdd } from "./components/postings/corporate/CorporateAdd";
import { CorporateEdit } from "./components/postings/corporate/CorporateEdit";

function AdminIndex({ logout }) {
  const [sideOpen, setSideOpen] = useState(true);
  const [menu, setSubMenu] = useState(false);

  const handleSubMenu = (menu) => {
    setSubMenu(menu);
  };

  useEffect(() => {
    setSideOpen(menu);
  }, [menu]);

  return (
    <>
      <Header logout={logout}>
        <Link to="/admin" className={sideOpen ? "close" : null}>
          <img src={wecruitLogo} alt="위크루트 로고" />
        </Link>
        <Hamburger handleSubMenu={handleSubMenu} menuState={menu} />
      </Header>

      <div className="adminWrap">
        <Sidebar menuOn={sideOpen} />

        <Routes>
          {/* admin 페이지일 경우 */}
          <Route path="/" element={<Main />} />

          {/* 서브 페이지 */}
          <Route path="/employers" element={<Employers />} />
          <Route path="/departments" element={<Departments />} />
          <Route path="/search-firm" element={<SearchFirm />} />

          <Route path="/recruiters" element={<Recruiters />} />
          <Route path="/statics" element={<Statics />} />
          <Route path="/statics-detail" element={<RecruiterStatics />} />

          <Route path="/confirm" element={<Confirm />} />
          <Route path="/done" element={<Done />} />
          <Route path="/ing" element={<Ing />} />
          <Route path="/job" element={<Job />} />
          <Route path="/temporary" element={<Temporary />} />
          <Route path="/corporate" element={<Corporate />} />

          <Route path="/activity" element={<Activity />} />
          <Route path="/case" element={<Case />} />
          <Route path="/project" element={<Project />} />

          <Route path="/notice" element={<Notice />} />
          <Route path="/indicator" element={<IndicatorStats />} />


          {/* 상세 페이지 */}
          <Route path="/employers-detail" element={<EmployersDetail />} />
          <Route path="/departments-detail" element={<DepartmentsDetail />} />

          <Route path="/recruiter-detail" element={<RecruitersDetail />} />

          <Route path="/job-register" element={<JobDetail />} />
          <Route path="/temp-manage" element={<TempDetail />} />
          <Route path="/ing-manage" element={<IngDetail />} />

          <Route path="/corporate-add" element={<CorporateAdd />} />
          <Route path="/corporate-edit" element={<CorporateEdit />} />

          <Route path="/confirm-form" element={<ConfirmForm />} />
          <Route path="/confirm-detail" element={<ConfirmDetail />} />
          <Route path="/done-detail" element={<DoneDetail />} />

          

          <Route path="/notice-add" element={<NoticeAdd />} />
          <Route path="/notice-edit" element={<NoticeEdit />} />


          {/* 관리 페이지 */}
          <Route path="/recruiter-manage" element={<Recruiter />} />
          <Route path="/candidate-manage" element={<Candidate />} />
          <Route path="/qna-manage" element={<Qna />} />

          <Route path="/recruiter-info" element={<RecruiterInfo />} />

        </Routes>
      </div>
      <Footer />
    </>
  );
}

export default AdminIndex;
