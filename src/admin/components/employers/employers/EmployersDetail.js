import {Link, useLocation, useNavigate} from 'react-router-dom';
import { useContext, useEffect, useState } from 'react';
import { Context } from 'contexts';
import { toast, ToastContainer } from "react-toastify";
import axios from 'axios';
import fileDownload from 'js-file-download';

import { axiosCrudOnSubmit } from 'commons/modules/commonUtil';
import { snglFileUpload } from 'commons/modules/multipartUtil';
import * as defVal  from 'commons/modules/defVal';
import { AddressInput } from 'commons/components/AddressInput';
import { ImagePreview } from 'commons/components/inputs/image-preview/ImagePreview';
import { FormattedMessage, useIntl } from 'react-intl';


export const EmployersDetail = (route) => {
  const intl = useIntl();

  let PROC_CHK = "";

  const {
    state : {
        server,
        mbr
    },
    dispatch,
  } = useContext(Context);

  const location = useLocation();
  const navigate = useNavigate();
  const moment = require('moment');
  let previewWindow = "";
  let previewImg = document.createElement("img");  

  const [files, setFiles] = useState("");
  const [axiosFilesRes, setAxiosFilesRes] = useState("");
  
  const [mbrFilesRes, setMbrFilesRes] = useState([]);
  const [bizFilesRes, setBizFilesRes] = useState([]);
  const [extraFilesRes, setExtraFilesRes] = useState([]);

  const salesSizeCd = defVal.SalesSizeCd();
  const empSizeCd = defVal.EmpSizeCd();
  const [bizCateCd, setBizCateCd] = useState([]);
  const [detailState, setDetailState] = useState(defVal.setAxiosMbrState(null));
  
  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid")
    ,ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi")
  };

  let locationState = {
    mbrUid : location.state.mbrUid,
    mbrDivi : "MBRDIVI-01"
  }
  
  useEffect(async () => {
      console.log("Page First Load EFFECT");
      initSetHandle();          
  },[])

  useEffect(()=>{
    console.log("File Upload Complete EFFECT");
    
    if(detailState.fileUploadCmplt) {
      cntntsCRUD(); 
    }
    
  }, [detailState.fileUploadCmplt]) 

  async function initSetHandle() {
    detailState.filePosNm[1] = "bizCertThmnl";
    detailState.filePosNm[2] = "bizLogoThmnl";
    detailState.ssnMbrUid = ssn.ssnMbrUid;
    
    let goUrl = server.path + "/mbr/mbrdetail"
    let data = {
      ssnMbrUid : ssn.ssnMbrUid,
      ssnMbrDivi : ssn.ssnMbrDivi,
      mbrUid : locationState.mbrUid,
      mbrDivi : locationState.mbrDivi
    }
    
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");    
    setDetailState(defVal.setAxiosMbrState(axiosRes.data));

    getBizCateCdList();
    getMbrFileList();
    getBizFileList();      
  }
  
 

  useEffect(() => {
    if(detailState ){ 
      console.log("useEffect detailState === " + detailState.mbrNm) ;        
    }
  }, [detailState])

  const getBizCateCdList = async () => {
    let goUrl = server.path + "/hireBizCateCd";
    let data = {};
    
    let axiosCateRes = await axiosCrudOnSubmit(data, goUrl, "POST");  

    console.log("getBizCateCdList == " + axiosCateRes.data.hireCateArr)   

    setBizCateCd(axiosCateRes.data.hireCateArr);
  }


  useEffect(() => {
    if(bizCateCd && bizCateCd.length > 0){     
      console.log("useEffect bizCateCd == " + bizCateCd.length)   
    }
  }, [bizCateCd])


  const getMbrFileList = async () => {
    let goUrl = server.path + "/mbr/mbrfiles";
    let data = {mbrUid: locationState.mbrUid};
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    setMbrFilesRes( axiosRes.data );
  }

  useEffect(() => {
    if(mbrFilesRes && mbrFilesRes.length > 0){
      console.log("useEffect mbrFilesRes length == " + mbrFilesRes.length);

      for(let i = 0; i < mbrFilesRes.length; i++)
      {
         // console.log("files getMbrFileList "+i+" === " + mbrFilesRes[i].orignlFileNm); 
          detailState.mbrFileJsonStr[i+1] = JSON.stringify(mbrFilesRes[i])+"";          
      }
    }
  }, [mbrFilesRes])

  const getBizFileList = async () => {
    let goUrl = server.path + "/mbr/bizfiles";
    let data = {mbrUid: locationState.mbrUid};
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    setBizFilesRes( axiosRes.data );
  }

  useEffect(() => {
    if(bizFilesRes && bizFilesRes.length > 0){
        
      console.log("useEffect bizFilesRes length == " + bizFilesRes.length);
      
      for(let i = 0; i < bizFilesRes.length; i++)
        {
          //console.log("files getBizFileList "+i+" === " + bizFilesRes[i].orignlFileNm); 
          if( i == 0 ){
            detailState.bizCertThmnlNm = bizFilesRes[i].orignlFileNm;            
          }
          else 
          {
            detailState.bizLogoThmnlNm = bizFilesRes[i].orignlFileNm;
          }

          detailState.bizFileJsonStr[i+1] = JSON.stringify(bizFilesRes[i])+"";
        }
    }
  }, [bizFilesRes])

  const getExtraFileList = async () => {
    let goUrl = server.path + "/mbr/extrafiles/";
    let data = {mbrUid: locationState.mbrUid};
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    setExtraFilesRes( axiosRes.data );
  }

  useEffect(() => {
    if(extraFilesRes && extraFilesRes.length > 0){
    }
  }, [extraFilesRes])

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////
 const handleDownload = (filePath, orignlFileNm) => {
  
  

  if (filePath != "") {
    let goUrl = server.host + filePath;
    
    //alert("goUrl == " + goUrl);
    
    axios
      .get(goUrl, {
        responseType: "blob",
      })
      .then((res) => {
        fileDownload(res.data, orignlFileNm);
      });
  } else {
    
  }

  
 }

 const handlePreview = (filePath, fileNm) => {

    previewWindow = window.open("", "", "width=600,height=400,left=200,top=200");

   if(typeof(filePath) == "string" )
   {
     let goUrl = server.host + filePath; 
     previewImg.src = goUrl;
   } 
   else
   {
     let file = detailState.uploadFiles[2];
     let reader = new FileReader();
   
     reader.onload = ( function(file) {
       return function(e) {
         previewImg.src = e.target.result;
       };
     })(file);
     
     reader.readAsDataURL(file);
   } 
     
   previewWindow.document.body.appendChild(previewImg);
 }


 /////////////////////////////////////////////////////////////////////////////////////////////////
 /** 퍼블리싱 작업 시에는 해당 블록을 주석처리 해주세요 */
 const jsonCompResult = ( resJson ) => {
  if( resJson.resCd !== "0000" )
  {
    return;
  }
  else
  {
    if( PROC_CHK === "UPD" )
    {
      //navigate("/admin/employers-detail", { mbrUid : location.detailState.mbrUid });
      toast.info(intl.formatMessage({
        id: "toast.info.department.info.edit.success",
        defaultMessage: "현업부서 담당자 정보가 수정되었습니다."
      }));
      //initSetHandle();
      return;
    }
    else if( PROC_CHK === "FILEUPLOAD" )
    {
      let jsonStr = JSON.stringify(resJson.resFileList);
      let fileList = JSON.parse(jsonStr);
      let tmpFilePosNm = detailState.filePosNm;
      let tmpList = detailState.bizFileJsonStr;
      
      for(let i=0; i<fileList.length; i++)
      {
        for(let n=0; n<tmpFilePosNm.length; n++)
        {
          if( tmpFilePosNm[n] == fileList[i].filePosNm)
          {
            tmpList[n] = JSON.stringify( fileList[i] );
          }
        }
      }
     
      setDetailState({
        ...detailState,
        fileUploadCmplt: true,
        bizFileJsonStr: tmpList
      });
      
    }
  }
}

async function goCRUD() {
  let fileStorePath = "Globals.fileStoreBizPath"; //로컬저장경로
  let fileLinkPath = "Globals.fileLinkBizPath"; //link경로
  let filePrefix = "biz"; //파일코드

  PROC_CHK = "FILEUPLOAD";

  let uploadRes = await snglFileUpload( server.path, fileStorePath, fileLinkPath, filePrefix, detailState.uploadFiles, detailState.filePosNm);
  
  if(uploadRes === 0) cntntsCRUD();
  else jsonCompResult(uploadRes.data);
}

async function cntntsCRUD() {
  PROC_CHK = 'UPD';

  console.log("CNTNTS CRUD");

  let goUrl = server.path + "/mbr/mbrupdate";
  let data = {mbrUid: locationState.mbrUid};
  //console.log("cntntsCRUD == " +  detailState.snglImgFileJsonStr);
  let axiosRes = await axiosCrudOnSubmit(detailState, goUrl, "POST");

  jsonCompResult(axiosRes.data);
}


  return (
    <main>
      <section className="formSection">
        <div className="formHeader">
          <h1>
            <FormattedMessage
              id="member.manage"
              defaultMessage="회원관리"
            />
            <em>
              <FormattedMessage
                id="employer.manage"
                defaultMessage="인사담당자 관리"
              />
            </em>
          </h1>
          <h2>
            <FormattedMessage
              id="company.info"
              defaultMessage="기업정보"
            />
          </h2>
        </div>
        <article>
          <form>
            <div className="formWrap">
              <ul className="twoCol">
                <li>
                  <label htmlFor="companyName">
                    <FormattedMessage
                      id="company.name"
                      defaultMessage="기업명"
                    />
                  </label>
                  <input type="text" id="companyName" 
                    value={detailState.bizNm}
                    onChange={(e) => {
                      setDetailState({
                        ...detailState,
                        bizNm : e.target.value
                      })
                    }}/>
                </li>
                <li>
                  <label htmlFor="ceoName">
                    <FormattedMessage
                      id="ceo.name"
                      defaultMessage="대표자명"
                    />
                  </label>
                  <input type="text" id="ceoName"
                    value={detailState.ceoNm} 
                    onChange={(e) => {
                      setDetailState({
                        ...detailState,
                        ceoNm : e.target.value
                      })
                    }}/>
                </li>
              </ul>
              <ul className="twoCol">
                <li>
                  <label htmlFor="licenseNumber">
                    <FormattedMessage
                      id="corporate.number"
                      defaultMessage="사업자등록번호"
                    />
                  </label>
                  <input type="text" id="licenseNumber" 
                    value={detailState.bizNum}
                    onChange={(e) => {
                      setDetailState({
                        ...detailState,
                        bizNum : e.target.value
                      })
                    }}/>
                </li>
                <li className="adminFileBox">
                  <span className="labelSpan">
                    <FormattedMessage
                      id="corporate.attach"
                      defaultMessage="사업자 등록증 첨부"
                    />
                  </span>
                  <div className="flex">
                    <div className="flex">
                      <label htmlFor="imgFile" className="basicButton">
                        <FormattedMessage
                          id="image.attach"
                          defaultMessage="이미지 첨부"
                        /> &gt;
                      </label>
                      <span className="fileName">                      
                        {detailState.bizCertThmnlNm}                  
                      </span>
                      <input type="file" id="imgFile" className="fileInput"
                        accept=".png, .jpg, .jpeg, .pdf"
                        onChange={(e) => {
                          let tmpList = detailState.uploadFiles;
                          tmpList[1] = e.target.files[0];

                          let tmpJsonStrList = detailState.bizFileJsonStr;
                          tmpJsonStrList[1] = "";
                          
                          setDetailState({
                            ...detailState,
                            uploadFiles: tmpList,
                            bizFileJsonStr: tmpJsonStrList,
                            bizCertThmnl: "",
                            bizCertThmnlNm: e.target.files[0].name
                          })
                        }}/>
                    </div>
                    <div>
                      <button type="button" className="basicButton" onClick={() => handleDownload(detailState.bizCertThmnl, detailState.bizCertThmnlNm)}>
                        <FormattedMessage
                          id="file.download"
                          defaultMessage="파일다운"
                        />
                      </button>
                    </div>
                  </div>
                </li>
              </ul>
              <ul className="twoCol">
                <li>
                  <label htmlFor="sales">
                    <FormattedMessage
                      id="company.sales"
                      defaultMessage="매출규모"
                    />
                  </label>
                  <select name="sales" id="sales" 
                    value={detailState.saleSize}
                    onChange={(e) => {
                      setDetailState({
                        ...detailState,
                        saleSize: e.target.value
                      })
                    }}>
                    <option>
                      --
                        {intl.formatMessage({
                          id: "select",
                          defaultMessage: "선택하세요"
                        })}
                      --
                    </option>
                    {salesSizeCd.map((type, idx) => (                        
                        <option value={type.cateCd}>{type.cdName}</option>
                     ))}                    
                  </select>
                </li>
                <li>
                  <label htmlFor="employee">
                    <FormattedMessage
                      id="company.workers"
                      defaultMessage="종업원 수"
                    />
                  </label>
                  <select name="employee" id="employee" 
                    value={detailState.empSize}
                    onChange={(e) => {
                      setDetailState({
                        ...detailState,
                        empSize: e.target.value
                      })
                    }}>
                    <option>
                      --
                        {intl.formatMessage({
                          id: "select",
                          defaultMessage: "선택하세요"
                        })}
                      --
                    </option>
                    {empSizeCd.map((type, idx) => (                        
                        <option value={type.cateCd}>{type.cdName}</option>
                    ))}   
                  </select>
                </li>
              </ul>
              <ul className="twoCol">
                <li>
                  <label htmlFor="category">
                    <FormattedMessage
                      id="company.business.type"
                      defaultMessage="업종구분"
                    />
                  </label>
                  <select name="category" id="category" 
                    value={detailState.bizSector}
                    onChange={(e) => {
                      setDetailState({
                        ...detailState,
                        bizSector: e.target.value
                      })
                    }}>
                    {bizCateCd.map((type, idx) => (                        
                        (type.lvl == 1 ) ? ( 
                        <option value={type.cateCd}>{type.cdName}</option>
                        ):('')                                           
                      ))}
                  </select>
                </li>
                <li>
                  <label htmlFor="hompage">
                    <FormattedMessage
                      id="homepage"
                      defaultMessage="홈페이지"
                    />
                  </label>
                  <input type="text" id="hompage"
                    value={detailState.bizHmpg} 
                    onChange={(e) => {
                      setDetailState({
                        ...detailState,
                        bizHmpg : e.target.value
                      })
                    }}/>
                </li>
              </ul>
              <ul className="twoCol">
                <li>
                  <label htmlFor="ceoPhone">
                    <FormattedMessage
                      id="ceo.telephone.number"
                      defaultMessage="대표 전화번호"
                    />
                  </label>
                  <input type="text" id="ceoPhone" 
                    placeholder={intl.formatMessage({
                      id: "placeholder.phone.number",
                      defaultMessage: "숫자만 입력하세요."
                    })} 
                    value={detailState.bizPhNum}
                    onChange={(e) => {
                      setDetailState({
                        ...detailState,
                        bizPhNum : e.target.value
                      })
                    }}
                  />
                </li>
                <li className="adminFileBox">
                  <span className="labelSpan">
                    <FormattedMessage
                      id="company.logo.attach"
                      defaultMessage="회사로고 첨부"
                    />
                  </span>
                  <div className="flex">
                    <div className="flex">
                      <label htmlFor="imgFile2" className="basicButton">
                        <FormattedMessage
                          id="image.attach"
                          defaultMessage="이미지 첨부"
                        /> &gt;
                      </label>
                      <span className="fileName">
                          {detailState.bizLogoThmnlNm}
                      </span>
                      <input type="file" id="imgFile2" className="fileInput"
                        accept=".png, .jpg, .jpeg"
                        onChange={(e) => {
                          let tmpList = detailState.uploadFiles;
                          tmpList[2] = e.target.files[0];

                          let tmpJsonStrList = detailState.bizFileJsonStr;
                          tmpJsonStrList[2] = "";
                          
                          setDetailState({
                            ...detailState,
                            uploadFiles: tmpList,
                            bizFileJsonStr: tmpJsonStrList,
                            bizLogoThmnl: "",
                            bizLogoThmnlNm: e.target.files[0].name
                          })
                        }}/>
                    </div>
                    <div>
                      <button type="button" className="basicButton" onClick={() => handlePreview(detailState.bizLogoThmnl, detailState.bizLogoThmnlNm)}>
                        <FormattedMessage
                          id="show.picture"
                          defaultMessage="사진 보기"
                        />
                      </button>
                    </div>
                  </div>
                </li>
              </ul>
               {/* 회사 주소 daum API */}
              <AddressInput state={detailState} setState={setDetailState}/>
            </div>
            <h2>
              <FormattedMessage
                id="employer.info"
                defaultMessage="인사담당자 정보"
              />
            </h2>
            <div className="formWrap">
              <ul className="twoCol">
                <li>
                  <label htmlFor="personnelName">
                    <FormattedMessage
                      id="name"
                      defaultMessage="이름"
                    />
                  </label>
                  <input type="text" id="personnelName" 
                    value={detailState.mbrNm} 
                    onChange={(e) => {
                      setDetailState({
                        ...detailState,
                        mbrNm : e.target.value
                      })
                    }}/>
                </li>
                <li>
                  <label htmlFor="position">
                    <FormattedMessage
                      id="position"
                      defaultMessage="직급"
                    />
                  </label>
                  <input type="text" id="position" 
                    value={detailState.mbrPos} 
                    onChange={(e) => {
                      setDetailState({
                        ...detailState,
                        mbrPos : e.target.value
                      })
                    }}/>
                </li>
              </ul>
              <ul className="twoCol">
                <li>
                  <label htmlFor="personnelPhone">
                    <FormattedMessage
                      id="phone.number"
                      defaultMessage="휴대폰 번호"
                    />
                  </label>
                  <input type="text" id="personnelPhone" 
                    value={detailState.celpNum} 
                    onChange={(e) => {
                      setDetailState({
                        ...detailState,
                        celpNum : e.target.value
                      })
                    }}/>
                </li>
                <li>
                  <label htmlFor="officePhone">
                    <FormattedMessage
                      id="office.telephone.number"
                      defaultMessage="사무실 전화번호"
                    />
                  </label>
                  <input type="text" id="officePhone" 
                  placeholder={intl.formatMessage({
                    id: "placeholder.phone.number",
                    defaultMessage: "숫자만 입력하세요."
                  })}
                  value={detailState.phNum}
                    onChange={(e) => {
                      setDetailState({
                        ...detailState,
                        phNum : e.target.value
                      })
                    }}/>
                </li>
              </ul>
              <ul className="twoCol">
                <li>
                  <label htmlFor="personnelEmail">
                    <FormattedMessage
                      id="email"
                      defaultMessage="이메일"
                    />
                  </label>
                  <input type="text" id="personnelEmail" 
                    value={detailState.email} 
                    onChange={(e) => {
                      setDetailState({
                        ...detailState,
                        email : e.target.value
                      })
                    }}/>
                </li>
                <li>
                  <label htmlFor="personnelID">
                    <FormattedMessage
                      id="id"
                      defaultMessage="아이디"
                    />
                  </label>
                  <input type="text" id="personnelID" 
                    value={detailState.mbrId} readOnly/>
                </li>
              </ul>
              <ul className="twoCol">
                <li>
                  <label htmlFor="joinDate">
                    <FormattedMessage
                      id="join.date.time"
                      defaultMessage="가입일시"
                    />
                  </label>
                  <span className="inputText">{detailState.joinDttm}</span>
                </li>
              </ul>
            </div>
            <div className="buttons">
              <button type="button" onClick={goCRUD}>
                <FormattedMessage
                  id="amendment.save"
                  defaultMessage="수정내용 저장"
                />
              </button>
              <Link to='/admin/employers'>
                <FormattedMessage
                  id="list"
                  defaultMessage="리스트"
                />
              </Link>
            </div>
          </form>
        </article>
      </section>
    </main>
  );
}