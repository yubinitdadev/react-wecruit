import { Link, useLocation, useNavigate } from "react-router-dom";
import { useContext, useEffect, useRef, useState } from "react";
import { Context } from "contexts";
import axios from "axios";
import fileDownload from "js-file-download";

import { axiosCrudOnSubmit } from "commons/modules/commonUtil";
import { Pagination } from "commons/components/pagination/Pagination";
import { DownloadBtns } from "commons/components/download-btn/DownloadBtns";
import { snglFileUpload } from "commons/modules/multipartUtil";
import * as defVal from "commons/modules/defVal";
import { AddressInput } from "commons/components/AddressInput";
import { ImagePreview } from "commons/components/inputs/image-preview/ImagePreview";
import { UPD_MBRUID } from "contexts/actionTypes";
import { FormattedMessage } from "react-intl";

export const Departments = () => {
  const [depts, setDepts] = useState([]);
  const [hireTitles, setHireTitles] = useState([]);
  const [sConts, setSConts] = useState("");
  const [fltrState, setFltrState] = useState({});
  const componentRef = useRef();
  const moment = require("moment");

  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(10);

  const indexOfLast = currentPage * postsPerPage;
  const indexOfFirst = indexOfLast - postsPerPage;

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid"),
    ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi"),
  };

  let arraySimb = "<:;>";

  function currentPosts(tmp) {
    let currentPosts = 0;
    currentPosts = tmp.slice(indexOfFirst, indexOfLast);
    return currentPosts;
  }

  const {
    state: { mbr, server },
    dispatch,
  } = useContext(Context);

  const headers = [
    { label: "현업부서 아이디", key: "mbrId" },
    { label: "기업명", key: "bizNm" },
    { label: "인사담당자 아이디", key: "employerId" },
    { label: "아이디 실명", key: "mbrNm" },
    { label: "휴대폰", key: "celpNum" },
    { label: "이메일", key: "email" },
    { label: "담당 채용공고", key: "recruitment" },
    { label: "생성일시", key: "rgstDttm" },
    { label: "삭제일시", key: "wthdrDttm" },
  ];

  async function goSrch() {
    let goUrl = server.path + "/mbr/mbrlist";
    let data = {
      ssnMbrUid: ssn.ssnMbrUid,
      ssnMbrDivi: ssn.ssnMbrDivi,
      mbrUid: mbr.mbrUid,
      mbrDivi: "MBRDIVI-03",
      sConts: sConts,
      sFltr: fltrState,
      sFltrCate: Object.keys(fltrState)[0],
      sFltrCont: Object.values(fltrState)[0],
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    setDepts(axiosRes.data.mbrArr);
  }

  const changeFltr = (fltr) => {
    let tmpFltr = fltr === "ASC" ? "DESC" : fltr === "DESC" ? "ASC" : "ASC";

    return tmpFltr;
  };

  useEffect(() => {
    goSrch();
  }, [fltrState]);

  useEffect(async () => {
    window.scrollTo(0, 0);

    dispatch({
      type: UPD_MBRUID,
      payload: {
        ssnMbrUid: ssn.ssnMbrUid,
        ssnMbrDivi: ssn.ssnMbrDivi,
      },
    });

    getList();
  }, []);

  const getList = async () => {
    /** 서버와 통신 시에는 해당 코드 블록을 사용 */

    let goUrl = server.path + "/mbr/mbrlist";
    let data = {
      ssnMbrUid: ssn.ssnMbrUid,
      ssnMbrDivi: ssn.ssnMbrDivi,
      mbrDivi: "MBRDIVI-03",
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    setDepts(axiosRes.data.mbrArr);
  };

  useEffect(() => {
    if (depts && depts.length > 0) {
      console.log("hireMaps.length 3 == " + depts.length);
      for (let i = 0; i < depts.length; i++) {
        //console.log("hireMaps.length hireTitle == " + depts[i].hireTitle);
        var tmp = defVal.SplitToArray(depts[i].hireTitles, "<:;>");
        hireTitles[i] = tmp;
      }
    }
  }, [depts]);

  const splitHireTitle = (str) => {
    console.log(str);
    return str;
  };

  const onKeyUpSrch = (e) => {
    if (e.key == "Enter") goSrch();
    return false;
  };

  return (
    <main>
      <section className="tableSection">
        <div className="sectionHeader">
          <h3>
            <span className="material-icons-outlined">settings</span>
            <b>
              <FormattedMessage
                id="department.id.manage"
                defaultMessage="현업부서 아이디 관리"
              />
            </b>
          </h3>

          <DownloadBtns
            headers={headers}
            dataset={depts}
            fileName="현업부서 아이디"
            componentRef={componentRef}
          />
        </div>
        <div className="searchBar">
          <label htmlFor="search">Search:</label>
          <input
            type="search"
            id="search"
            onChange={(e) => {
              setSConts(e.target.value);
            }}
            onKeyUp={(e) => {
              onKeyUpSrch(e);
            }}
          />
          <button type="button" onClick={goSrch}>
            <FormattedMessage id="search" defaultMessage="검색" />
          </button>
        </div>

        <article id="capture" ref={componentRef}>
          <table>
            <thead>
              <tr>
                <th>
                  <span>
                    <FormattedMessage id="number" defaultMessage="번호" />
                  </span>
                </th>
                <th
                  className={
                    fltrState.mbrId === "DESC"
                      ? "descending"
                      : fltrState.mbrId === "ASC"
                      ? "ascending"
                      : undefined
                  }
                  onClick={() => {
                    setFltrState({
                      mbrId: changeFltr(fltrState.mbrId),
                    });
                  }}
                >
                  <div className="wrap">
                    <span>
                      <FormattedMessage
                        id="department.id"
                        defaultMessage="현업부서 아이디"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th
                  className={
                    fltrState.bizNm === "DESC"
                      ? "descending"
                      : fltrState.bizNm === "ASC"
                      ? "ascending"
                      : undefined
                  }
                  onClick={() => {
                    setFltrState({
                      bizNm: changeFltr(fltrState.bizNm),
                    });
                  }}
                >
                  <div className="wrap">
                    <span>
                      <FormattedMessage
                        id="company.name"
                        defaultMessage="기업명"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th
                  className={
                    fltrState.empId === "DESC"
                      ? "descending"
                      : fltrState.empId === "ASC"
                      ? "ascending"
                      : undefined
                  }
                  onClick={() => {
                    setFltrState({
                      empId: changeFltr(fltrState.empId),
                    });
                  }}
                >
                  <div className="wrap">
                    <span>
                      <FormattedMessage
                        id="employer.id"
                        defaultMessage="인사담당자 아이디"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th
                  className={
                    fltrState.idDesc === "DESC"
                      ? "descending"
                      : fltrState.idDesc === "ASC"
                      ? "ascending"
                      : undefined
                  }
                  onClick={() => {
                    setFltrState({
                      idDesc: changeFltr(fltrState.idDesc),
                    });
                  }}
                >
                  <div className="wrap">
                    <span>
                      <FormattedMessage
                        id="id.description"
                        defaultMessage="아이디 설명"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th
                  className={
                    fltrState.celpNum === "DESC"
                      ? "descending"
                      : fltrState.celpNum === "ASC"
                      ? "ascending"
                      : undefined
                  }
                  onClick={() => {
                    setFltrState({
                      celpNum: changeFltr(fltrState.celpNum),
                    });
                  }}
                >
                  <div className="wrap">
                    <span>
                      <FormattedMessage id="phone" defaultMessage="휴대폰" />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th
                  className={
                    fltrState.email === "DESC"
                      ? "descending"
                      : fltrState.email === "ASC"
                      ? "ascending"
                      : undefined
                  }
                  onClick={() => {
                    setFltrState({
                      email: changeFltr(fltrState.email),
                    });
                  }}
                >
                  <div className="wrap">
                    <span>
                      <FormattedMessage id="email" defaultMessage="이메일" />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th
                  className={
                    fltrState.hireNm === "DESC"
                      ? "descending"
                      : fltrState.hireNm === "ASC"
                      ? "ascending"
                      : undefined
                  }
                  onClick={() => {
                    setFltrState({
                      hireNm: changeFltr(fltrState.hireNm),
                    });
                  }}
                >
                  <div className="wrap">
                    <span>
                      <FormattedMessage
                        id="posting.in.charge"
                        defaultMessage="담당 채용공고"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th
                  className={
                    fltrState.rgstDttm === "DESC"
                      ? "descending"
                      : fltrState.rgstDttm === "ASC"
                      ? "ascending"
                      : undefined
                  }
                  onClick={() => {
                    setFltrState({
                      rgstDttm: changeFltr(fltrState.rgstDttm),
                    });
                  }}
                >
                  <div className="wrap">
                    <span>
                      <FormattedMessage
                        id="creation.date"
                        defaultMessage="생성일시"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th
                  className={
                    fltrState.wthdrDttm === "DESC"
                      ? "descending"
                      : fltrState.wthdrDttm === "ASC"
                      ? "ascending"
                      : undefined
                  }
                  onClick={() => {
                    setFltrState({
                      wthdrDttm: changeFltr(fltrState.wthdrDttm),
                    });
                  }}
                >
                  <div className="wrap">
                    <span>
                      <FormattedMessage
                        id="deletion.date"
                        defaultMessage="삭제일시"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
              </tr>
            </thead>
            <tbody>
              <tr className="noData" style={{ display: "none" }}>
                <th colSpan={9}>No data available in table</th>
              </tr>
              {currentPosts(depts).map((dept, idx) => (
                <tr key={dept.mbrUid}>
                  <td>{indexOfFirst + idx + 1}</td>
                  <td>
                    <Link
                      to="/admin/departments-detail"
                      state={{ mbrUid: dept.mbrUid }}
                    >
                      {dept.mbrId}
                    </Link>
                  </td>
                  <td>{dept.bizNm}</td>
                  <td>{dept.refMbrId}</td>
                  <td>{dept.intro}</td>
                  <td>{dept.celpNum}</td>
                  <td>{dept.email}</td>
                  <td>
                    {hireTitles[idx]
                      ? hireTitles[idx].map((type, idx) => <div>{type}</div>)
                      : ""}
                  </td>
                  <td>
                    {moment(dept.rgstDttm).format("YYYY년 MM월 DD일 HH시 mm분")}
                  </td>
                  <td>
                    {dept.wthdrDttm !== ""
                      ? moment(dept.wthdrDttm).format(
                          "YYYY년 MM월 DD일 HH시 mm분"
                        )
                      : ""}
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </article>

        <Pagination
          postsPerPage={postsPerPage}
          totalPosts={depts.length}
          paginate={setCurrentPage}
          currentPage={currentPage}
        />
      </section>
    </main>
  );
};
