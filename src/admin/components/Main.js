
import '../components/scss/graph.scss';
import { ProjectGraph } from './graph/ProjectGraph';
import { QnaGraph } from './graph/QnaGraph';
import { UserGraph } from "./graph/UserGraph";

import { DownloadBtns } from 'commons/components/download-btn/DownloadBtns';
import { Pagination } from 'commons/components/pagination/Pagination';
import { axiosCrudOnSubmit } from 'commons/modules/commonUtil';
import * as commonUtil from 'commons/modules/commonUtil';
import { Context } from 'contexts';
import { UPD_MBRUID } from 'contexts/actionTypes';
import { useContext, useEffect, useRef, useState } from 'react';
import {Link} from 'react-router-dom';
import { toast, ToastContainer } from "react-toastify";
import { FormattedMessage, useIntl } from 'react-intl';

export const Main =() => {
  const intl = useIntl();

  const {
    state : {
      server,
      mbr
    },
    dispatch,
  } = useContext(Context);

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid")
    ,ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi")
  };

    
  const moment = require('moment');
  const componentRef = useRef();

  const [listState, setListStat] = useState([]);
  const [hireListState, setHireListStat] = useState([]);
  const [qsListState, setQsListStat] = useState([]);
  const [sConts, setSConts] = useState("");
  const [fltrState, setFltrState] = useState({});

  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(10);

  const [recRankListState, setRecRankListState] = useState([]);
  const [newMbrListState, setNewMbrListState] = useState([]);
  const [workPosListState, setWorkPosListState] = useState([]);

  const indexOfLast = currentPage * postsPerPage;
  const indexOfFirst = indexOfLast - postsPerPage;

  function currentPosts(tmp) {
    let currentPosts = 0;
    currentPosts = tmp.slice(indexOfFirst, indexOfLast);
    return currentPosts;
  }

  const [currentTab, setCurrentTab] = useState(0);

  useEffect(async() => {
    window.scrollTo(0,0);
    initSetHandle();
  }, [])

  const initSetHandle = async () => {
    let goUrl = server.path + "/home/joincntlist"
    let data = { 
      sStrtDttm: (document.getElementById("sStrtDttm"))?(document.getElementById("sStrtDttm").value):('')
      ,sEndDttm: (document.getElementById("sEndDttm"))?(document.getElementById("sEndDttm").value):('')     
    }

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    let resMbrArr = axiosRes.data.resMbrArr;
    let resHireArr = axiosRes.data.resHireArr;
    let resQsArr = axiosRes.data.resQsArr;
    
    
    let tmpMbrArr = [];
    resMbrArr.forEach((resData, idx)=>{
      resData["sttcDttm"] = moment(resData.sttcDttm).format("MM월DD일");
      tmpMbrArr.push(resData);
    })
    setListStat(tmpMbrArr);

    let tmpHireArr = [];
    resHireArr.forEach((resData, idx)=>{
      resData["sttcDttm"] = moment(resData.sttcDttm).format("MM월DD일");
      tmpHireArr.push(resData);
    })
    setHireListStat(tmpHireArr);

    let tmpResQsArr = [];
    resQsArr.forEach((resData, idx)=>{
      resData["sttcDttm"] = moment(resData.sttcDttm).format("MM월DD일");
      tmpResQsArr.push(resData);
    })
    setQsListStat(tmpResQsArr);
    



    var tmpRankList = await getRecRankList(data);
    setRecRankListState(tmpRankList);
    
    var tmpNewMbrlist = await getNewMbrList(data);
    setNewMbrListState(tmpNewMbrlist);

    var tmpWorkPosList = await getWorkPosList(data);
    setWorkPosListState(tmpWorkPosList);
  }

  const getNewMbrList = async (data) => {
    let goUrl = server.path + "/home/newmbrlist";
    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");
    let resArr = axiosRes.data.resArr;
    
    return resArr;
  };

  const getRecRankList = async (data) => {
    let goUrl = server.path + "/home/recranklist";
    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");
    let resArr = axiosRes.data.resArr;
    
    return resArr;
  };

  const getWorkPosList = async (data) => {
    let goUrl = server.path + "/home/workposlist";
    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");
    let resArr = axiosRes.data.resArr;
    
    return resArr;
  };


  const menuArr = [
    { name: '유저', content: <UserGraph listState={listState}/> },
    { name: '프로젝트', content: <ProjectGraph listState={hireListState}/> },
    { name: 'Q&A', content: <QnaGraph listState={qsListState}/> },
  ];

  const selectMenuHandler = (index) => {
    setCurrentTab(index);
  };

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  let PROC_CHK = "";

  const jsonCompResult = ( resJson ) => {
    console.log("PROC_CHK : " + PROC_CHK);
    
    if( resJson.resCd !== "0000" )
    {
      toast.error(intl.formatMessage({
        id: "error.during.save",
        defaultMessage: "저장 중 오류가 발생했습니다."
      }));
      return;
    }
    else
    {
      if( PROC_CHK === "UPDCNFRM" )
      {
        PROC_CHK = "";   
        initSetHandle();
        
      }      
    }

    
  }
  
  const goCnfrmCRUD = async (uid, div) => {
    var data={
      mbrUid: uid
      ,cnfrmYn: div
      ,ssnMbrUid: ssn.ssnMbrUid
    }
    
    PROC_CHK = 'UPDCNFRM';
  
    let goUrl = server.path + "/mbr/mbrcnfrm";
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    
    jsonCompResult(axiosRes.data);

    //closeQnaModal();
  }

  const OnErrorMbrImg = (id) => {
    document.getElementById(id).src=server.mbrNoImg;
  }
  

  return(
    <main>
      <section>
        <div className="adminMainGraph">
          <div className="dateFilter">
            <input type="date" id="sStrtDttm"></input> ~ <input type="date" id="sEndDttm"></input>
            <button type="button" onClick={initSetHandle}>
              <FormattedMessage
                id="search"
                defaultMessage="검색"
              />
            </button>
          </div>

          <ul className="graphTap">
            {menuArr.map((ele, index)=>{
              return (
                <li
                key={index}
                className={currentTab === index ? "active" : ""}
                onClick={()=> selectMenuHandler(index)}
              >
                {ele.name}
              </li>
                )
            })}
          </ul>

          {menuArr[currentTab].content}
          
          <article>
            <div className="statsBox">
              <p className="statsTitle">
                <span className="material-icons">
                sync_alt
                </span>
                <FormattedMessage
                  id="recruiter.rating"
                  defaultMessage="리크루터 순위"
                />
              </p>

              <ol>

              {
                recRankListState.map((list, idx)=>
                  (
                    (idx < 3)?
                      (
                        <li>
                            <div>
                              <figure style={{overflow:'hidden'}}>
                                  <img name={"recThmnl"+idx} id={"recThmnl"+idx} src={list.thmnl} style={{width:'100%', height:'100%'}}
                                      onError={(e) => OnErrorMbrImg("recThmnl"+idx)}
                                  />                               
                              </figure>
                              <div>
                                <span>{list.mbrNm}</span>
                                <span>
                                  {idx+1}<FormattedMessage
                                    id="ranking"
                                    defaultMessage="위"
                                  />
                                </span>
                              </div>
                            </div>

                            <ul>
                              <li>
                                <span>{list.cmmntChooseCnt}</span>
                                <span>
                                  <FormattedMessage
                                    id="selection"
                                    defaultMessage="선택"
                                  />
                                </span>
                              </li>
                              <li>
                                <span>{list.recmmndCnt}</span>
                                <span>
                                  <FormattedMessage
                                    id="recommendation"
                                    defaultMessage="추천"
                                  />
                                </span>
                              </li>
                              <li>
                                <span>{list.recmmndCnfrmCnt}</span>
                                <span>
                                  <FormattedMessage
                                    id="accomplish"
                                    defaultMessage="성사"
                                  />
                                </span>
                              </li>
                            </ul>
                        </li>
                      ):('')
                  )
                )
              }
              </ol>
            </div>

            <div className="candidatePosition">
              <p className="statsTitle">
                <span className="material-icons">
                  sync_alt
                </span>
                <FormattedMessage
                  id="candidate.number.position"
                  defaultMessage="포지션별 후보자수"
                />
              </p>

              <ol>
              {
                workPosListState.map((list, idx)=>
                  (
                    (idx < 3)?
                      (
                        <li>
                            <div>
                              <div>
                                <span>
                                  {list.workPosCnt}<FormattedMessage
                                    id="people.count"
                                    defaultMessage="명"
                                  />
                                </span>
                                <span>{list.cdName}</span>
                              </div>
                            </div>
                        </li>
                      ):('')
                  )
                )
              }
              </ol>
            </div>

            <div className="joinRequest">
              <p className="statsTitle">
                <span className="material-icons">sync_alt</span>
                <FormattedMessage
                  id="request.join"
                  defaultMessage="가입요청"
                />
              </p>

              <ol>
              {
                newMbrListState.map((list, idx)=>
                  (
                    (idx < 3)?
                      (
                        <li>
                            <div>
                              <figure style={{overflow:'hidden'}}>
                                  <img name={"empThmnl"+idx} id={"empThmnl"+idx} src={list.thmnl} style={{width:'100%', height:'100%'}}
                                      onError={(e) => OnErrorMbrImg("empThmnl"+idx)}
                                  />                               
                              </figure>
                              <div>
                                <span>{list.bizNm}</span>
                                <span>{list.mbrNm}</span>
                              </div>
                            </div>

                            <div className="requestButton">
                              <span onClick={() => {goCnfrmCRUD(list.mbrUid, 'R')}}>
                                <FormattedMessage
                                  id="reject"
                                  defaultMessage="거절"
                                />
                              </span>
                              <span onClick={() => {goCnfrmCRUD(list.mbrUid, 'Y')}}>
                                <FormattedMessage
                                  id="accept"
                                  defaultMessage="수락"
                                />
                              </span>
                            </div>
                        </li>
                      ):('')
                  )
                )
              }
              </ol>
            </div>
          </article>
        </div>
      </section>
    </main>
  )
}