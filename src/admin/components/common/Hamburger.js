export const Hamburger =({handleSubMenu, menuState}) => {
  const menuToggleHandler = () => {
    handleSubMenu(!menuState);
  }

  return(
    <div className="hamburgerBtn" onClick={menuToggleHandler}>
      <span></span>
      <span></span>
      <span></span>
    </div>
  )
}