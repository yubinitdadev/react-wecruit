import { useNavigate } from "react-router-dom";
import styled from "styled-components";
import { getAuth, signOut } from "firebase/auth";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import { useContext, useState } from "react";
import "commons/scss/header-all.scss";
import { Context } from "contexts";
import { axiosCrudOnSubmit } from "commons/modules/commonUtil";
import LocaleSelect from "employers/components/common/LocaleSelect";
import { FormattedMessage, useIntl } from 'react-intl';

export const Header = ({ menu, children, logout }) => {
  const intl = useIntl();
  const navigate = useNavigate();
  const auth = getAuth();
  const [adminOn, setAdminOn] = useState(false);

  const {
    state : {
        mbr,
        server
    },
    dispatch,
  } = useContext(Context);

  const jsonCompResult = (resJson) => {
    if( resJson.resCd !== "0000" )
    {
      toast.error(intl.formatMessage({
        id: "error.fail.logout",
        defaultMessage: "로그아웃에 실패하였습니다."
      }));
      return;
    }
    else
    {
      navigate("/");
    }
  }

  async function handleLogout() {
    let goUrl = server.path + "/mbr/logout";
    let data = {
      ssnMbrUid : (window.localStorage.getItem("ssnMbrUid")) ? (window.localStorage.getItem("ssnMbrUid")):(''),
      mbrUid : (window.localStorage.getItem("ssnMbrUid")) ? (window.localStorage.getItem("ssnMbrUid")):('')
    };

    window.localStorage.clear();

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    jsonCompResult(axiosRes.data);
  }

  const handleAdminButton = () => {
    setAdminOn(!adminOn);
  };

  return (
    <>
      <ToastContainer />
      
      <HeaderWrap
        className={menu ? "adminHeader close" : "adminHeader"}
        menuState={menu}
      >
        
        <div className="navLeft">{children}</div>
  
        <div className="localeWrap">
          <LocaleSelect/>
    
          <button
            type="button"
            onClick={handleAdminButton}
            className={adminOn ? "adminLogoutBtn active" : "adminLogoutBtn"}
          >
            관리자
            <div>
              <span className="material-icons-outlined">vpn_key</span>
              <div
                onClick={() => {
                  
                  handleLogout();
                  
                  /*
                  signOut(auth)
                    .then(() => {
                      navigate("/");
                      logout();
                    })
                    .catch((error) => {
                      toast.error("Cannot sign out");
                    });
                  */
                }}
              >
                <FormattedMessage
                  id="logout"
                  defaultMessage="로그아웃"
                />
              </div>
            </div>
          </button>
        </div>
      </HeaderWrap>
    </>
  );
};
const HeaderWrap = styled.div`
  background: white;
`;
