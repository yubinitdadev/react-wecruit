import '../scss/side-bar.scss'

import {Link, useLocation, useNavigate} from 'react-router-dom';
import { useContext, useEffect, useState } from 'react';
import { Context } from 'contexts';
import * as defVal  from 'commons/modules/defVal';
import * as commonUtil from 'commons/modules/commonUtil';
import { FormattedMessage, useIntl } from 'react-intl';

export const Sidebar = ({menuOn})=>{
  const intl = useIntl();
  let PROC_CHK = ""

  const {
    state : {
        server,
        mbr
    },
    dispatch,
  } = useContext(Context);

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid")
    ,ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi")
  };

  const location = useLocation();
  const navigate = useNavigate();
 
  if( ssn.ssnMbrUid == "" || 
        ssn.ssnMbrDivi != "MBRDIVI-99" )
    {
      alert(intl.formatMessage({
        id: "wrong.access",
        defaultMessage: "정상적인 접속이 아닙니다."
      }));
      navigate("/");
    }

  return(
    <nav className={menuOn ? 'close' : null}>
      <div className="submenu">
        <h2>
          <FormattedMessage
            id="employer"
            defaultMessage="인사담당자"
          />
        </h2>
        <div className="subpages">
          <Link to='/admin/employers'>
            <span className="material-icons-outlined">
            diamond
            </span>
            <b>
              <FormattedMessage
                id="employer.manage"
                defaultMessage="인사담당자 관리"
              />
            </b>
          </Link>
          <Link to='/admin/departments'>
            <span className="material-icons-outlined">
            diamond
            </span>
            <b>
              <FormattedMessage
                id="department.id.manage"
                defaultMessage="현업부서 아이디 관리"
              />
            </b>
          </Link>
          <Link to='/admin/search-firm'>
            <span className="material-icons-outlined">
            diamond
            </span>
            <b>
              <FormattedMessage
                id="exsting.searchfirm.address"
                defaultMessage="기존 거래 써치펌 주소록"
              />
            </b>
          </Link>
        </div>
      </div>
  
      <div className="submenu">
        <h2>
          <FormattedMessage
            id="recruiter"
            defaultMessage="리크루터"
          />
        </h2>
        <div className="subpages">
          <Link to='/admin/recruiters'>
            <span className="material-icons-outlined">
            diamond
            </span>
            <b>
              <FormattedMessage
                id="recruiter.manage"
                defaultMessage="리크루터 관리"
              />
            </b>
          </Link>
          <Link to='/admin/statics'>
            <span className="material-icons-outlined">
            diamond
            </span>
            <b>
              <FormattedMessage
                id="recruiter.statics"
                defaultMessage="리크루터 통계"
              />
            </b>
          </Link>
        </div>
      </div>
  
      <div className="submenu">
        <h2>
          <FormattedMessage
            id="posting.manage"
            defaultMessage="공고관리"
          />
        </h2>
        <div className="subpages">
          <Link to='/admin/job'>
            <span className="material-icons-outlined">
            diamond
            </span>
            <b>
              <FormattedMessage
                id="posting.register"
                defaultMessage="채용공고 등록하기"
              />
            </b>
          </Link>
          <Link to='/admin/temporary'>
            <span className="material-icons-outlined">
            diamond
            </span>
            <b>
              <FormattedMessage
                id="posting.temp.storage"
                defaultMessage="임시저장 채용공고"
              />
            </b>
          </Link>
          <Link to='/admin/confirm'>
            <span className="material-icons-outlined">
            diamond
            </span>
            <b>
              <FormattedMessage
                id="posting.confirm"
                defaultMessage="검수중 채용공고"
              />
            </b>
          </Link>
          <Link to='/admin/ing'>
            <span className="material-icons-outlined">
            diamond
            </span>
            <b>
              <FormattedMessage
                id="posting.ing"
                defaultMessage="진행중 채용공고"
              />
            </b>
          </Link>
          <Link to='/admin/done'>
            <span className="material-icons-outlined">
            diamond
            </span>
            <b>
              <FormattedMessage
                id="posting.done"
                defaultMessage="종료된 채용공고"
              />
            </b>
          </Link>
          <Link to='/admin/corporate'>
            <span className="material-icons-outlined">
            diamond
            </span>
            <b>
              <FormattedMessage
                id="corporate.manage"
                defaultMessage="기업 관리"
              />
            </b>
          </Link>
        </div>
      </div>
  
      <div className="submenu">
        <h2>CR</h2>
        <div className="subpages">
          <Link to='/admin/activity'>
            <span className="material-icons-outlined">
            diamond
            </span>
            <b>
              <FormattedMessage
                id="recruiter.activity"
                defaultMessage="리크루터 활동현황 보기"
              />
            </b>
          </Link>
          <Link to='/admin/case'>
            <span className="material-icons-outlined">
            diamond
            </span>
            <b>
              <FormattedMessage
                id="recruiter.case.manage"
                defaultMessage="리크루터별 진행가능 건수 관리"
              />
            </b>
          </Link>
          <Link to='/admin/project'>
            <span className="material-icons-outlined">
            diamond
            </span>
            <b>
              <FormattedMessage
                id="project.all.manage"
                defaultMessage="프로젝트별 전체 관리현황 보기"
              />
            </b>
          </Link>
        </div>
      </div>
  
      <div className="submenu">
        <h2>
          <FormattedMessage
            id="board.manage"
            defaultMessage="게시판 관리"
          />
        </h2>
        <div className="subpages">
          <Link to='/admin/notice'>
            <span className="material-icons-outlined">
            diamond
            </span>
            <b>
              <FormattedMessage
                id="notice.list"
                defaultMessage="공지사항 리스트"
              />
            </b>
          </Link>
        </div>
      </div>

      <div className="submenu">
        <h2>KPI</h2>
        <div className="subpages">
          <Link to='/admin/indicator'>
            <span className="material-icons-outlined">diamond</span>
            <b>
              <FormattedMessage
                id="indicator.statics"
                defaultMessage="지표 통계"
              />
            </b>
          </Link>
        </div>
      </div>
    </nav>
  )
}