
import {Link, useLocation, useNavigate} from 'react-router-dom';
import { useContext, useEffect, useRef, useState } from 'react';
import { Context } from 'contexts';
import axios from 'axios';
import fileDownload from 'js-file-download';

import { axiosCrudOnSubmit } from 'commons/modules/commonUtil';
import * as commonUtil from 'commons/modules/commonUtil';
import { Pagination } from 'commons/components/pagination/Pagination';
import { DownloadBtns } from 'commons/components/download-btn/DownloadBtns';
import { snglFileUpload } from 'commons/modules/multipartUtil';
import * as defVal  from 'commons/modules/defVal';
import { AddressInput } from 'commons/components/AddressInput';
import { ImagePreview } from 'commons/components/inputs/image-preview/ImagePreview';
import { UPD_MBRUID } from 'contexts/actionTypes';
import { FormattedMessage } from 'react-intl';

export const UserStats = (props) => {
  
  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(10);

  const indexOfLast = currentPage * postsPerPage;
  const indexOfFirst = indexOfLast - postsPerPage;

  function currentPosts(tmp) {
    let currentPosts = 0;
    currentPosts = tmp.slice(indexOfFirst, indexOfLast);
    return currentPosts;
  }

  const headers = [
    { label: "일", key: "sttcDttm" },
    { label: "인사 담당자 가입", key: "empCnt" },
    { label: "인사 담당자 탈퇴", key: "empWrthCnt" },
    { label: "인사 담당자 총원", key: "empTcnt" },
    { label: "리크루터 담당자 가입", key: "recCnt" },
    { label: "리크루터 담당자 탈퇴", key: "recWrthCnt" },
    { label: "리크루터 담당자 총원", key: "recTcnt" },
    { label: "댓글 배율", key: "qsRate" },
    { label: "댓글 HR", key: "empQsCnt" },
    { label: "댓글 리크루터", key: "recQsCnt" },
    { label: "댓글 관리자", key: "admnQsCnt" },
  ];


  return (
    <article id="capture">
      <table>
        <thead>
          <tr>
            <th colSpan={1} rowSpan={2}>
              <span>
                <FormattedMessage
                  id="date.day"
                  defaultMessage="일"
                />
              </span>
            </th>
            <th colSpan={3} rowSpan={1}>
              <span>
                <FormattedMessage
                  id="employer"
                  defaultMessage="인사담당자"
                />
              </span>
            </th>
            <th colSpan={3} rowSpan={1}>
              <span>
                <FormattedMessage
                  id="recruiter"
                  defaultMessage="리크루터"
                />
              </span>
            </th>
            <th colSpan={1} rowSpan={2}>
              <span>
                <FormattedMessage
                  id="ratio"
                  defaultMessage="비율"
                />(b/a)
              </span>
            </th>
            <th colSpan={3} rowSpan={1}>
              <span>
                <FormattedMessage
                  id="comment"
                  defaultMessage="댓글"
                />
              </span>
            </th>
          </tr>

          <tr>
            <th colSpan={1} rowSpan={1}>
              <span>
                <FormattedMessage
                  id="join"
                  defaultMessage="가입"
                />
              </span>
            </th>
            <th colSpan={1} rowSpan={1}>
              <span>
                <FormattedMessage
                  id="leave"
                  defaultMessage="탈퇴"
                />
              </span>
            </th>
            <th colSpan={1} rowSpan={1}>
              <span>
                <FormattedMessage
                  id="total.number"
                  defaultMessage="총원"
                />(a)
              </span>
            </th>

            <th colSpan={1} rowSpan={1}>
              <span>
                <FormattedMessage
                  id="join"
                  defaultMessage="가입"
                />
              </span>
            </th>
            <th colSpan={1} rowSpan={1}>
              <span>
                <FormattedMessage
                  id="leave"
                  defaultMessage="탈퇴"
                />
              </span>
            </th>
            <th colSpan={1} rowSpan={1}>
              <span>
                <FormattedMessage
                  id="total.number"
                  defaultMessage="총원"
                />(b)
              </span>
            </th>

            <th colSpan={1} rowSpan={1}>
              <span>HR</span>
            </th>
            <th colSpan={1} rowSpan={1}>
              <span>
                <FormattedMessage
                  id="recruiter"
                  defaultMessage="리크루터"
                />
              </span>
            </th>
            <th colSpan={1} rowSpan={1}>
              <span>
                <FormattedMessage
                  id="administrator"
                  defaultMessage="관리자"
                />
              </span>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr className="noData" style={{display: 'none'}}>
            <th colSpan={9}>No data available in table</th>
          </tr>
          {currentPosts(props.listState).map((list, idx)=>
            (
              <tr>
                <td>{list.sttcDttm}</td>
                <td>{list.empCnt}</td>
                <td>{list.empWrthCnt}</td>
                <td>{list.empTcnt}</td>
                <td>{list.recCnt}</td>
                <td>{list.recWrthCnt}</td>
                <td>{list.recTcnt}</td>
                <td>{list.qsRate}</td>
                <td>{list.empQsCnt}</td>
                <td>{list.recQsCnt}</td>
                <td>{list.admnQsCnt}</td>
              </tr>
            )
            )
          }
        </tbody>

        <tfoot>
          <tr>
            <td className="textBold">
              <FormattedMessage
                id="total"
                defaultMessage="총계"
              />
            </td>
            <td>{props.sumState.empCnt}</td>
            <td>{props.sumState.empWrthCnt}</td>
            <td>{props.sumState.empTcnt}</td>
            <td>{props.sumState.recCnt}</td>
            <td>{props.sumState.recWrthCnt}</td>
            <td>{props.sumState.recTcnt}</td>
            <td>{(props.sumState.qsRate == 0 )? '0.00' : props.sumState.qsRate }</td>
            <td>{props.sumState.empQsCnt}</td>
            <td>{props.sumState.recQsCnt}</td>
            <td>{props.sumState.admnQsCnt}</td>
          </tr>
        </tfoot>
      </table>

      <Pagination postsPerPage={postsPerPage} totalPosts={props.listState.length} paginate={setCurrentPage} currentPage={currentPage}/>
    </article>

  )
}