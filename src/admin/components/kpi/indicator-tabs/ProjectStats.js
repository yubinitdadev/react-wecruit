import {Link, useLocation, useNavigate} from 'react-router-dom';
import { useContext, useEffect, useRef, useState } from 'react';
import { Context } from 'contexts';
import axios from 'axios';
import fileDownload from 'js-file-download';

import { axiosCrudOnSubmit } from 'commons/modules/commonUtil';
import * as commonUtil from 'commons/modules/commonUtil';
import { Pagination } from 'commons/components/pagination/Pagination';
import { DownloadBtns } from 'commons/components/download-btn/DownloadBtns';
import { snglFileUpload } from 'commons/modules/multipartUtil';
import * as defVal  from 'commons/modules/defVal';
import { AddressInput } from 'commons/components/AddressInput';
import { ImagePreview } from 'commons/components/inputs/image-preview/ImagePreview';
import { UPD_MBRUID } from 'contexts/actionTypes';
import { FormattedMessage } from 'react-intl';


export const ProjectStats = (props) => {

  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(10);

  const indexOfLast = currentPage * postsPerPage;
  const indexOfFirst = indexOfLast - postsPerPage;

    
  function currentPosts(tmp) {
    let currentPosts = 0;
    currentPosts = tmp.slice(indexOfFirst, indexOfLast);
    return currentPosts;
  }
  
  const hireHeaders = [
    { label: "일", key: "sttcDttm" },
    { label: "신규공고", key: "hireNewCnt" },
    { label: "재공고", key: "hireReCnt" },
    { label: "전체 공고", key: "hireCnt" },
    { label: "성사 프로젝트", key: "hireSuccCnt" },
    { label: "활성화 프로젝트", key: "hireYCnt" }
  ];
 
  
  return(
    <article id="capture">
      <table>
        <thead>
          <tr>
            <th>
              <span>
                <FormattedMessage
                  id="date.day"
                  defaultMessage="일"
                />
              </span>
            </th>
            <th>
              <span>
                <FormattedMessage
                  id="posting.new"
                  defaultMessage="신규공고"
                />
              </span>
            </th>
            <th>
              <span>
                <FormattedMessage
                  id="posting.re"
                  defaultMessage="재공고"
                />
              </span>
            </th>
            <th>
              <span>
                <FormattedMessage
                  id="posting.every"
                  defaultMessage="전체 공고"
                />
              </span>
            </th>
            <th>
              <span>
                <FormattedMessage
                  id="project.achieved"
                  defaultMessage="성사 프로젝트"
                />
              </span>
            </th>
            <th>
              <span>
                <FormattedMessage
                  id="project.active"
                  defaultMessage="활성화 프로젝트"
                />
              </span>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr className="noData" style={{display: 'none'}}>
            <th colSpan={9}>No data available in table</th>
          </tr>
          
          {currentPosts(props.listState).map((list, idx)=>
              (
                <tr>
                  <td>{list.sttcDttm}</td>
                  <td>{list.hireNewCnt}</td>
                  <td>{list.hireReCnt}</td>
                  <td>{list.hireCnt}</td>
                  <td>{list.hireSuccCnt}</td>
                  <td>{list.hireYCnt}</td>
                </tr>
              )
            )
          }
        </tbody>

        <tfoot>
          <tr>
            <td className="textBold">
              <FormattedMessage
                id="total"
                defaultMessage="총계"
              />
            </td>
            <td>{props.sumState.hireNewCnt}</td>
            <td>{props.sumState.hireReCnt}</td>
            <td>{props.sumState.hireCnt}</td>
            <td>{props.sumState.hireSuccCnt}</td>
            <td>{props.sumState.hireYCnt}</td>
          </tr>
        </tfoot>
      </table>

      <Pagination postsPerPage={postsPerPage} totalPosts={props.listState.length} paginate={setCurrentPage} currentPage={currentPage}/>
    </article>
  )
}