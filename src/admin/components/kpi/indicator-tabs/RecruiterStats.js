import {Link, useLocation, useNavigate} from 'react-router-dom';
import { useContext, useEffect, useRef, useState } from 'react';
import { Context } from 'contexts';
import axios from 'axios';
import fileDownload from 'js-file-download';

import { axiosCrudOnSubmit } from 'commons/modules/commonUtil';
import * as commonUtil from 'commons/modules/commonUtil';
import { Pagination } from 'commons/components/pagination/Pagination';
import { DownloadBtns } from 'commons/components/download-btn/DownloadBtns';
import { snglFileUpload } from 'commons/modules/multipartUtil';
import * as defVal  from 'commons/modules/defVal';
import { AddressInput } from 'commons/components/AddressInput';
import { ImagePreview } from 'commons/components/inputs/image-preview/ImagePreview';
import { UPD_MBRUID } from 'contexts/actionTypes';
import { FormattedMessage } from 'react-intl';

export const RecruiterStats = (props) => {

  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(10);

  const indexOfLast = currentPage * postsPerPage;
  const indexOfFirst = indexOfLast - postsPerPage;

    
  function currentPosts(tmp) {
    let currentPosts = 0;
    currentPosts = tmp.slice(indexOfFirst, indexOfLast);
    return currentPosts;
  }

  const recHeaders = [
    { label: "일", key: "sttcDttm" },
    { label: "선택된 프로젝트", key: "cmmntChooseCnt" },
    { label: "추천한 후보자수", key: "cmmntCnt" },
    { label: "성사된 프로젝트 수", key: "hireSuccCnt" }
  ];

  return(
    <article id="capture">
      <table>
        <thead>
          <tr>
            <th>
              <span>
                <FormattedMessage
                  id="date.day"
                  defaultMessage="일"
                />
              </span>
            </th>
            <th>
              <span>
                <FormattedMessage
                  id="project.selected"
                  defaultMessage="선택된 프로젝트"
                />
              </span>
            </th>
            <th>
              <span>
                <FormattedMessage
                  id="candidate.recommend.number"
                  defaultMessage="추천한 후보자수"
                />
              </span>
            </th>
            <th>
              <span>
                <FormattedMessage
                  id="project.achieved.number"
                  defaultMessage="성사된 프로젝트 수"
                />
              </span>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr className="noData" style={{display: 'none'}}>
            <th colSpan={9}>No data available in table</th>
          </tr>
          
          {currentPosts(props.listState).map((list, idx)=>
              (
      
                <tr>
                  <td>{list.sttcDttm}</td>
                  <td>{list.cmmntChooseCnt}</td>
                  <td>{list.cmmntCnt}</td>
                  <td>{list.hireSuccCnt}</td>
                </tr>
              )
            )
          }
                    
        </tbody>

        <tfoot>
          <tr>
            <td className="textBold">
              <FormattedMessage
                id="total"
                defaultMessage="총계"
              />
            </td>
            <td>{props.sumState.cmmntChooseCnt}</td>
            <td>{props.sumState.cmmntCnt}</td>
            <td>{props.sumState.hireSuccCnt}</td>
          </tr>
        </tfoot>
      </table>

      <Pagination postsPerPage={postsPerPage} totalPosts={props.listState.length} paginate={setCurrentPage} currentPage={currentPage}/>
    </article>
  )
}