import {Link, useLocation, useNavigate} from 'react-router-dom';
import { useContext, useEffect, useRef, useState } from 'react';
import { Context } from 'contexts';
import axios from 'axios';
import fileDownload from 'js-file-download';

import { axiosCrudOnSubmit } from 'commons/modules/commonUtil';
import * as commonUtil from 'commons/modules/commonUtil';
import { Pagination } from 'commons/components/pagination/Pagination';
import { DownloadBtns } from 'commons/components/download-btn/DownloadBtns';
import { snglFileUpload } from 'commons/modules/multipartUtil';
import * as defVal  from 'commons/modules/defVal';
import { AddressInput } from 'commons/components/AddressInput';
import { ImagePreview } from 'commons/components/inputs/image-preview/ImagePreview';
import { UPD_MBRUID } from 'contexts/actionTypes';
import { FormattedMessage } from 'react-intl';


export const CandidateStats = (props) => {


  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(10);

  const indexOfLast = currentPage * postsPerPage;
  const indexOfFirst = indexOfLast - postsPerPage;

  function currentPosts(tmp) {
    let currentPosts = 0;
    currentPosts = tmp.slice(indexOfFirst, indexOfLast);
    return currentPosts;
  }

  const workPosHeaders = [
    { label: "일", key: "cateCd1Cnt" },
    { label: "경영·사무", key: "cateCd2Cnt" },
    { label: "영업·고객상담", key: "cateCd3Cnt" },
    { label: "생산·제조", key: "cateCd4Cnt" },
    { label: "IT·인터넷", key: "cateCd5Cnt" },
    { label: "전문직", key: "cateCd6Cnt" },
    { label: "교육", key: "cateCd7Cnt" },
    { label: "미디어", key: "cateCd8Cnt" },
    { label: "건설", key: "cateCd9Cnt" },
    { label: "유통·무역", key: "cateCd10Cnt" },
    { label: "서비스", key: "cateCd11Cnt" },
    { label: "디자인", key: "cateCd12Cnt" },
    { label: "의료", key: "cateCd13Cnt" },
    { label: "총계", key: "cateSumCnt" }
  ];

  
  return(
    <article id="capture">
      <table>
        <thead>
          <tr>
            <th>
              <span>
                <FormattedMessage
                  id="date.day"
                  defaultMessage="일"
                />
              </span>
            </th>
            <th>
              <span>
                <FormattedMessage
                  id="kpi.job1"
                  defaultMessage="경영·사무"
                />
              </span>
            </th>   
            <th>
              <span>
                <FormattedMessage
                  id="kpi.job2"
                  defaultMessage="영업·고객상담"
                />
              </span>
            </th>
            <th>
              <span>
                <FormattedMessage
                  id="kpi.job3"
                  defaultMessage="생산·제조"
                />
              </span>
            </th> 
            <th>
              <span>
                <FormattedMessage
                  id="kpi.job4"
                  defaultMessage="IT·인터넷"
                />
              </span>
            </th> 
            <th>
              <span>
                <FormattedMessage
                  id="kpi.job5"
                  defaultMessage="전문직"
                />
              </span>
            </th> 
            <th>
              <span>
                <FormattedMessage
                  id="kpi.job6"
                  defaultMessage="교육"
                />
              </span>
            </th>  
            <th>
              <span>
                <FormattedMessage
                  id="kpi.job7"
                  defaultMessage="미디어"
                />
              </span>
            </th> 
            <th>
              <span>특
                <FormattedMessage
                  id="kpi.job8"
                  defaultMessage="특수계층·공공"
                />
              </span>
            </th>   
            <th>
              <span>
                <FormattedMessage
                  id="kpi.job9"
                  defaultMessage="건설"
                />
              </span>
            </th>   
            <th>
              <span>
                <FormattedMessage
                  id="kpi.job10"
                  defaultMessage="유통·무역"
                />
              </span>
            </th>   
            <th>
              <span>
                <FormattedMessage
                  id="kpi.job11"
                  defaultMessage="서비스"
                />
              </span>
            </th> 
            <th>
              <span>
                <FormattedMessage
                  id="kpi.job12"
                  defaultMessage="디자인"
                />
              </span>
            </th> 
            <th>
              <span>
                <FormattedMessage
                  id="kpi.job13"
                  defaultMessage="의료"
                />
              </span>
            </th>            
            <th>
              <span>
                <FormattedMessage
                  id="total"
                  defaultMessage="총계"
                />
              </span>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr className="noData" style={{display: 'none'}}>
            <th colSpan={9}>No data available in table</th>
          </tr>
          {
            (currentPosts(props.listState).map((list, idx)=>
                (
                  <tr>
                    <td>{list.sttcDttm}</td>
                    <td>{list.cateCd1Cnt}</td>
                    <td>{list.cateCd2Cnt}</td>
                    <td>{list.cateCd3Cnt}</td>
                    <td>{list.cateCd4Cnt}</td>
                    <td>{list.cateCd5Cnt}</td>
                    <td>{list.cateCd6Cnt}</td>
                    <td>{list.cateCd7Cnt}</td>
                    <td>{list.cateCd8Cnt}</td>
                    <td>{list.cateCd9Cnt}</td>
                    <td>{list.cateCd10Cnt}</td>
                    <td>{list.cateCd11Cnt}</td>
                    <td>{list.cateCd12Cnt}</td>
                    <td>{list.cateCd13Cnt}</td>
                    <td>{list.cateSumCnt}</td>
                  </tr>
                )
              )
            )
          }
        </tbody>

        <tfoot>
          <tr>
            <td className="textBold">
              <FormattedMessage
                id="total"
                defaultMessage="총계"
              />
            </td>
            <td>{props.sumState.cateCd1Cnt}</td>
            <td>{props.sumState.cateCd2Cnt}</td>
            <td>{props.sumState.cateCd3Cnt}</td>
            <td>{props.sumState.cateCd4Cnt}</td>
            <td>{props.sumState.cateCd5Cnt}</td>
            <td>{props.sumState.cateCd6Cnt}</td>
            <td>{props.sumState.cateCd7Cnt}</td>
            <td>{props.sumState.cateCd8Cnt}</td>
            <td>{props.sumState.cateCd9Cnt}</td>
            <td>{props.sumState.cateCd10Cnt}</td>
            <td>{props.sumState.cateCd11Cnt}</td>
            <td>{props.sumState.cateCd12Cnt}</td>
            <td>{props.sumState.cateCd13Cnt}</td>
            <td>{props.sumState.cateSumCnt}</td>
          </tr>
        </tfoot>
      </table>

      <Pagination postsPerPage={postsPerPage} totalPosts={props.listState.length} paginate={setCurrentPage} currentPage={currentPage}/>
    </article>
  )
}