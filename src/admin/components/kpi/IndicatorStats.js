import { useContext, useEffect, useState, useRef } from 'react';
import {Link, useLocation, useNavigate} from 'react-router-dom';
import { Context } from 'contexts';
import { UserStats } from './indicator-tabs/UserStats';
import { CandidateStats } from './indicator-tabs/CandidateStats';
import { ProjectStats } from './indicator-tabs/ProjectStats';
import { RecruiterStats } from './indicator-tabs/RecruiterStats';
import { ExcelDownload } from 'commons/components/download-btn/ExcelDownload';

import { axiosCrudOnSubmit } from 'commons/modules/commonUtil';
import * as commonUtil from 'commons/modules/commonUtil';
import { Pagination } from 'commons/components/pagination/Pagination';
import { DownloadBtns } from 'commons/components/download-btn/DownloadBtns';
import { snglFileUpload } from 'commons/modules/multipartUtil';
import * as defVal  from 'commons/modules/defVal';
import { AddressInput } from 'commons/components/AddressInput';
import { ImagePreview } from 'commons/components/inputs/image-preview/ImagePreview';
import { UPD_MBRUID } from 'contexts/actionTypes';
import { FormattedMessage } from 'react-intl';

export const IndicatorStats = () => {
  
  const {
    state : {
        server,
        mbr
    },
    dispatch,
  } = useContext(Context);

  const location = useLocation();
  const navigate = useNavigate();
  const moment = require('moment');
  let previewWindow = "";
  let previewImg = document.createElement("img");  
  
  const componentRef = useRef();

  const [detailState, setDetailState] = useState(defVal.setAxiosMbrState(null));
  const [currentTab, setCurrentTab] = useState(0);

  
  const [fileName, setFileName] = useState("");
  const [listState, setListState] = useState([]);

  const [userSumState, setUserSumState] = useState({});
  const [userListState, setUserListState] = useState([]);
  const [workPosSumState, setWorkPosSumState] = useState({});
  const [workPosListState, setWorkPosListState] = useState([]);
  const [hireSumState, setHireSumState] = useState({});
  const [hireListState, setHireListState] = useState([]);
  const [recSumState, setRecSumState] = useState({});
  const [recListState, setRecListState] = useState([]);

  
  const [workPosCateCdListState, setWorkPosCateCdListState] = useState([]);

  const [sStrtDttm, setSStrtDttm] = useState("");
  const [sEndDttm, setSEndDttm] = useState("");

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid")
    ,ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi")
  };

  useEffect(async () => {
    console.log("Page First Load EFFECT");
    initSetHandle();          
  },[])

  const reRender = () => {
    setDetailState({
      ...detailState,
      initSetCmplt: true
    })      
  };

  const initSetHandle = async () => {
    
    var today = commonUtil.getToday();
    var sStrtDttm = commonUtil.getAddToday(-10);
    var sEndDttm = today;

    if( document.getElementById("sStrtDttm").value == "" )document.getElementById("sStrtDttm").value = sStrtDttm;
    if( document.getElementById("sEndDttm").value == "" )document.getElementById("sEndDttm").value = sEndDttm;
    
    var tmpUserStatList = await getUserStatList();
    
  }

  const getUserStatList = async () => {
    let goUrl = server.path + "/sttcs/userstatlist";
    let data = {
      ssnMbrUid : ssn.ssnMbrUid,
      ssnMbrDivi : ssn.ssnMbrDivi,
      mbrUid : mbr.mbrUid,
      sStrtDttm : (document.getElementById("sStrtDttm").value)?(document.getElementById("sStrtDttm").value):(''),
      sEndDttm : (document.getElementById("sEndDttm").value)?(document.getElementById("sEndDttm").value):(''),
      sFltr : "",
      sFltrCate : "",
      sFltrCont : ""
    }

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    setUserSumState(axiosRes.data.resInfo);
    setUserListState(axiosRes.data.resArr);      
    
    return axiosRes.data.resArr;
  };

  const getWorkPosStatList = async () => {
    let goUrl = server.path + "/sttcs/workposcndttstatList";
    let data = {
      ssnMbrUid : ssn.ssnMbrUid,
      ssnMbrDivi : ssn.ssnMbrDivi,
      mbrUid : mbr.mbrUid,
      sStrtDttm : (document.getElementById("sStrtDttm").value)?(document.getElementById("sStrtDttm").value):(''),
      sEndDttm : (document.getElementById("sEndDttm").value)?(document.getElementById("sEndDttm").value):(''),
      sFltr : "",
      sFltrCate : "",
      sFltrCont : ""
    }

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    
    //console.log("axiosRes.data.resInfo.cateCd1Cnt == " + JSON.stringify(axiosRes.data.resInfo));
    //console.log("axiosRes.data.resInfo.cateCd1Cnt == " + axiosRes.data.resInfo.cateCd1Cnt);

    setWorkPosSumState(axiosRes.data.resInfo);
    setWorkPosListState(axiosRes.data.resArr);  

    return axiosRes.data.resArr;    
    
  }

  const getWorkPosCateCdList = async () => {
    
    let goUrl = server.path + "/workPosCateCd";    
    var data={};
    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");
    let resArr = axiosRes.data.workPosCateCdArr;

    return resArr;
};



async function getHireStatList() {
  let goUrl = server.path + "/sttcs/hirestatlist";
  let data = {
    ssnMbrUid : ssn.ssnMbrUid,
    ssnMbrDivi : ssn.ssnMbrDivi,
    mbrUid : mbr.mbrUid,
    sStrtDttm : (document.getElementById("sStrtDttm").value)?(document.getElementById("sStrtDttm").value):(''),
    sEndDttm : (document.getElementById("sEndDttm").value)?(document.getElementById("sEndDttm").value):(''),
    sFltr : "",
    sFltrCate : "",
    sFltrCont : ""
  }

  let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

  setHireSumState(axiosRes.data.resInfo);
  setHireListState(axiosRes.data.resArr);      
}

async function getRecStatList() {
  let goUrl = server.path + "/sttcs/recactstatlist";
  let data = {
    ssnMbrUid : ssn.ssnMbrUid,
    ssnMbrDivi : ssn.ssnMbrDivi,
    mbrUid : mbr.mbrUid,
    sStrtDttm : (document.getElementById("sStrtDttm").value)?(document.getElementById("sStrtDttm").value):(''),
    sEndDttm : (document.getElementById("sEndDttm").value)?(document.getElementById("sEndDttm").value):(''),
    sFltr : "",
    sFltrCate : "",
    sFltrCont : ""
  }

  let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

  setRecSumState(axiosRes.data.resInfo);
  setRecListState(axiosRes.data.resArr);      
}


  const indicatorArr = [
    { name: '서비스 사용자', content: <UserStats listState={userListState} sumState={userSumState} /> },
    { name: '포지션별 후보자수  ', content: <CandidateStats listState={workPosListState} sumState={workPosSumState}/> },
    { name: '활동 프로젝트', content: <ProjectStats listState={hireListState} sumState={hireSumState} /> },
    { name: '리크루터별 활동', content: <RecruiterStats listState={recListState} sumState={recSumState} /> }
  ];

  const goSrch = async () => {
    var index = currentTab;
    selectMenuHandler(index);
    
  };

  const selectMenuHandler = async (index) => {
    
    document.getElementById("userExcel").style.display = "none";
    document.getElementById("workPosExcel").style.display = "none";
    document.getElementById("hireExcel").style.display = "none";
    document.getElementById("recExcel").style.display = "none";

    if(index == 0 ){
      var tmpUserList = await getUserStatList();
      document.getElementById("userExcel").style.display = "";
    }
    else if(index == 1 )
    {
      var tmpWorkPosList = await getWorkPosStatList();
      document.getElementById("workPosExcel").style.display = "";
    }
    else if(index == 2 )
    {
      var tmpHireList = await getHireStatList();
      document.getElementById("hireExcel").style.display = "";
    }
    else if(index == 3 )
    {
      var tmpRecList = await getRecStatList();
      document.getElementById("recExcel").style.display = "";
    }
    
    
    setCurrentTab(index);
  };

  const userHeaders = [
    { label: "일", key: "sttcDttm" },
    { label: "인사 담당자 가입", key: "empCnt" },
    { label: "인사 담당자 탈퇴", key: "empWrthCnt" },
    { label: "인사 담당자 총원", key: "empTcnt" },
    { label: "리크루터 담당자 가입", key: "recCnt" },
    { label: "리크루터 담당자 탈퇴", key: "recWrthCnt" },
    { label: "리크루터 담당자 총원", key: "recTcnt" },
    { label: "댓글 배율", key: "qsRate" },
    { label: "댓글 HR", key: "empQsCnt" },
    { label: "댓글 리크루터", key: "recQsCnt" },
    { label: "댓글 관리자", key: "admnQsCnt" },
  ];

  const workPosHeaders = [
    { label: "일", key: "sttcDttm" },
    { label: "경영·사무", key: "cateCd1Cnt" },
    { label: "영업·고객상담", key: "cateCd2Cnt" },
    { label: "생산·제조", key: "cateCd3Cnt" },
    { label: "IT·인터넷", key: "cateCd4Cnt" },
    { label: "전문직", key: "cateCd5Cnt" },
    { label: "교육", key: "cateCd6Cnt" },
    { label: "미디어", key: "cateCd7Cnt" },
    { label: "특수계층·공공", key: "cateCd8Cnt" },
    { label: "건설", key: "cateCd9Cnt" },
    { label: "유통·무역", key: "cateCd10Cnt" },
    { label: "서비스", key: "cateCd11Cnt" },
    { label: "디자인", key: "cateCd12Cnt" },
    { label: "의료", key: "cateCd13Cnt" },
    { label: "총계", key: "cateSumCnt" }
  ];


  const hireHeaders = [
    { label: "일", key: "sttcDttm" },
    { label: "신규공고", key: "hireNewCnt" },
    { label: "재공고", key: "hireReCnt" },
    { label: "전체 공고", key: "hireCnt" },
    { label: "성사 프로젝트", key: "hireSuccCnt" },
    { label: "활성화 프로젝트", key: "hireYCnt" }
  ];

  const recHeaders = [
    { label: "일", key: "sttcDttm" },
    { label: "선택된 프로젝트", key: "cmmntChooseCnt" },
    { label: "추천한 후보자수", key: "cmmntCnt" },
    { label: "성사된 프로젝트 수", key: "hireSuccCnt" }
  ];  

  return(
    <main>
      <section className="tableSection">
        <div className="sectionHeader">
          <h3>
            <span className="material-icons-outlined">
              settings
            </span>
            <b>
              <FormattedMessage
                id="indicator.statics"
                defaultMessage="지표 통계"
              />
            </b>
          </h3>
          
          <span id="userExcel" style={{display:'none'}}>
            <ExcelDownload headers={userHeaders} dataset={userListState} fileName="서비스 사용자"/>
          </span>
          <span id="workPosExcel" style={{display:'none'}}>
            <ExcelDownload headers={workPosHeaders} dataset={workPosListState} fileName="포지션별 후보자수"/>
          </span>
          <span id="hireExcel" style={{display:'none'}}>
            <ExcelDownload headers={hireHeaders} dataset={hireListState} fileName="활동 프로젝트"/>
          </span>
          <span id="recExcel" style={{display:'none'}}>
            <ExcelDownload headers={recHeaders} dataset={recListState} fileName="리크루터 활동"/>
          </span>

        </div>

        <div className="dateFilter">
            <input type="date" id="sStrtDttm"></input> ~ <input type="date" id="sEndDttm"></input>
            <button type="button" onClick={goSrch}>
              <FormattedMessage
                id="search"
                defaultMessage="검색"
              />
            </button>
        </div>

        <ul className="indicatorTap">
          {indicatorArr.map((ele, index)=>{
            return (
              <li
              key={index}
              className={currentTab === index ? "active" : ""}
              onClick={()=> selectMenuHandler(index)}
              >
              {ele.name}
            </li>
              )
          })}
        </ul>

        <div className="indicatorTable">{indicatorArr[currentTab].content}</div>


      </section>
    </main>
  )
}