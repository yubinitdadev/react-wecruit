import React, { useContext } from 'react';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { Context } from 'contexts';
import { snglFileUpload } from 'commons/modules/multipartUtil';
import { hydrate } from 'react-dom';

const EditorComponent = (props) => {
	const {
		state : {
			server,
			mbr
		},
		dispatch,
	} = useContext(Context);

	//data = { props.state[props.target] };
	console.log("props.target == " + props.target);
	console.log("props.target == " + props.state[props.target]);



	function uploadAdapter(loader) {
		return {
		  upload: () => {
			return new Promise((resolve, reject) => {
				const body = new FormData();

				loader.file.then(async(file) => {
					body.append("files", file);

					let fileStorePath = "Globals.fileStoreCkPath"; //로컬저장경로
    				let fileLinkPath = "Globals.fileLinkCkPath"; //link경로
    				let filePrefix = "ck"; //파일코드

					let uploadFiles = [ "disable", file, "disable", "disable", "disable" ];
					let filePosNm =  ["", file.name, "", "", ""];

					let uploadRes = await snglFileUpload( server.path, fileStorePath, fileLinkPath, filePrefix, uploadFiles, filePosNm);

					let fileData = uploadRes.data.resFileList[0];

					resolve({
						default: `${server.path}${fileData.streFilePath}${fileData.streFileNm}`
					})
				});
			});
		  }
		};
	}
	
	function uploadPlugin(editor) {
		editor.plugins.get("FileRepository").createUploadAdapter = (loader) => {
			return uploadAdapter(loader);
		};
	}

  	return (

		

		<CKEditor
			editor = { ClassicEditor }
			config={{
				extraPlugins: [uploadPlugin]
			}}
			data = { props.state[props.target] }
			onChange = {(e, editor) => {
				props.setState({
					...props.state,
					[props.target]: editor.getData(),
					//ckData: editor.getData()
				})
			}}
		/>
  );
};

export default EditorComponent;