
import {Link, useLocation, useNavigate} from 'react-router-dom';
import { useContext, useEffect, useState } from 'react';
import { Context } from 'contexts';
import { toast, ToastContainer } from "react-toastify";
import axios from 'axios';
import fileDownload from 'js-file-download';

import { axiosCrudOnSubmit } from 'commons/modules/commonUtil';
import { snglFileUpload } from 'commons/modules/multipartUtil';
import * as defVal  from 'commons/modules/defVal';
import { AddressInput } from 'commons/components/AddressInput';
import { TagTemplate } from 'commons/components/tags/TagTemplate';
import EditorComponent from 'admin/components/editor/EditorComponent';
import { ImagePreview } from 'commons/components/inputs/image-preview/ImagePreview';
import { FormattedMessage, useIntl } from 'react-intl';

export const RecruitersDetail = () => {
  const intl = useIntl();
  let PROC_CHK = "";

  const {
    state : {
        server,
        mbr
    },
    dispatch,
  } = useContext(Context);

  
  const location = useLocation();
  const navigate = useNavigate();
  const moment = require('moment');
  let previewWindow = "";
  let previewImg = document.createElement("img");  

  const [files, setFiles] = useState("");
  const [axiosFilesRes, setAxiosFilesRes] = useState("");
  
  const [mbrFilesRes, setMbrFilesRes] = useState([]);
  const [bizFilesRes, setBizFilesRes] = useState([]);
  const [extraFilesRes, setExtraFilesRes] = useState([]);

  const salesSizeCd = defVal.SalesSizeCd();
  const empSizeCd = defVal.EmpSizeCd();
  const bankCd = defVal.BankCd();
  
  const [bizCateCd, setBizCateCd] = useState([]);
  const [detailState, setDetailState] = useState(defVal.setAxiosMbrState(null));
  const [tagCnt, setTagCnt] = useState(0);

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid")
    ,ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi")
  };

  let locationState = {
    mbrUid : location.state.mbrUid,
    mbrDivi : "MBRDIVI-02",
    backUrl: (location.state.backUrl)?(location.state.backUrl):('/admin/recruiters')
  }

  useEffect(async () => {
    console.log("Page First Load EFFECT");
    initSetHandle();          
  },[])

  useEffect(()=>{
    console.log("File Upload Complete EFFECT");
    console.log("FILE UPLOAD CMPLT?");
    //console.log(detailState.fileUploadCmplt);
    
    if(detailState.fileUploadCmplt) {
      cntntsCRUD(); 
    }
    
  }, [detailState.fileUploadCmplt]) 

  let backUrl = document.referrer;

  async function initSetHandle() {

    backUrl = document.referrer;
    console.log("backUrl == " + backUrl)

    detailState.filePosNm[1] = "thmnlNm";
    detailState.filePosNm[2] = "extraThmnlNm";
    detailState.ssnMbrUid = ssn.ssnMbrUid;
    
    let goUrl = server.path + "/mbr/mbrdetail"
    let data = {
      ssnMbrUid : ssn.ssnMbrUid,
      ssnMbrDivi : ssn.ssnMbrDivi,
      mbrUid : locationState.mbrUid,
      mbrDivi : locationState.mbrDivi
    }
    
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");    
    console.log("axiosRes.data === " + axiosRes.data) ;
    setDetailState(defVal.setAxiosMbrState(axiosRes.data));

    getBizCateCdList();
    getMbrFileList();
    getBizFileList();   
    getExtraFileList();   
  }

  const getDetail = async () => {
    let goUrl = server.path + "/mbr/mbrdetail"
    let data = {
      ssnMbrUid : ssn.ssnMbrUid,
      ssnMbrDivi : ssn.ssnMbrDivi,
      mbrUid : locationState.mbrUid,
      mbrDivi : locationState.mbrDivi
    }
    
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");  
    
    setDetailState(defVal.setAxiosMbrState(axiosRes.data));
  }

  useEffect(() => {
    if( detailState ){   
      //console.log("useEffect detailState1  === " + detailState.mbrNm) ;
      //console.log("useEffect detailState2  === " + detailState.bizNm) ;
      detailState.filePosNm[1] = "thmnl";
      detailState.filePosNm[2] = "extraThmnl";
      detailState.mbrFilePosNm[1] = "thmnl";
      detailState.extraFilePosNm[1] = "extraThmnl";
      detailState.ssnMbrUid = ssn.ssnMbrUid;
    }
  }, [detailState])

  const getBizCateCdList = async () => {
    let goUrl = server.path + "/hireBizCateCd";
    let data = {};
    
    let axiosCateRes = await axiosCrudOnSubmit(data, goUrl, "POST");  
    setBizCateCd(axiosCateRes.data.hireCateArr);
  }

  useEffect(() => {
    if(bizCateCd && bizCateCd.length > 0){        
    }
  }, [bizFilesRes])


  const getMbrFileList = async () => {
    let goUrl = server.path + "/mbr/mbrfiles";
    let data = {mbrUid: locationState.mbrUid};
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    setMbrFilesRes( axiosRes.data );
  }

  useEffect(() => {
    if(mbrFilesRes && mbrFilesRes.length > 0){
      console.log("useEffect mbrFilesRes length == " + mbrFilesRes.length);

      for(let i = 0; i < mbrFilesRes.length; i++)
      {
          console.log("files getMbrFileList "+i+" === " + mbrFilesRes[i].orignlFileNm); 
          detailState.thmnlNm = mbrFilesRes[i].orignlFileNm;
          detailState.mbrFileJsonStr[i+1] = JSON.stringify(mbrFilesRes[i])+"";          
      }
    }
  }, [mbrFilesRes])

  const getBizFileList = async () => {
    let goUrl = server.path + "/mbr/bizfiles";
    let data = {mbrUid: locationState.mbrUid};
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    setBizFilesRes( axiosRes.data );
  }

  useEffect(() => {
    if(bizFilesRes && bizFilesRes.length > 0){
        
      console.log("useEffect bizFilesRes length == " + bizFilesRes.length);
      
      for(let i = 0; i < bizFilesRes.length; i++)
        {
          //console.log("files getBizFileList "+i+" === " + bizFilesRes[i].orignlFileNm); 
          if( i == 0 ){
            detailState.bizCertThmnlNm = bizFilesRes[i].orignlFileNm;            
          }
          else 
          {
            detailState.bizLogoThmnlNm = bizFilesRes[i].orignlFileNm;
          }

          detailState.bizFileJsonStr[i+1] = JSON.stringify(bizFilesRes[i])+"";
        }
    }
  }, [bizFilesRes])

  const getExtraFileList = async () => {
    let goUrl = server.path + "/mbr/extrafiles/";
    let data = {mbrUid: locationState.mbrUid};
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    setExtraFilesRes( axiosRes.data );
  }

  useEffect(() => {
    if(extraFilesRes && extraFilesRes.length > 0){
      console.log("useEffect extraFilesRes length == " + mbrFilesRes.length);

      for(let i = 0; i < extraFilesRes.length; i++)
      {
         // console.log("files getMbrFileList "+i+" === " + mbrFilesRes[i].orignlFileNm); 
          detailState.extraThmnlNm = extraFilesRes[i].orignlFileNm;
          detailState.extraFileJsonStr[i+1] = JSON.stringify(extraFilesRes[i])+"";          
      }
    }
  }, [extraFilesRes])

  /////////////////////////////////////////////////////////////////////////
  const handleDownload = (filePath, fileNm) => {
    if(typeof(filePath) == "string" )
    {
       let goUrl = server.path + filePath;    
       axios.get(goUrl, {
        responseType: 'blob',
      })
      .then((res) => {
        fileDownload(res.data, fileNm);
      })
    } else {
      fileDownload(detailState.uploadFiles[1], fileNm);
    }
   }
  
   const handlePreview = (filePath, fileNm) => {
  
      previewWindow = window.open("", "", "width=600,height=400,left=200,top=200");
  
     if(typeof(filePath) == "string" )
     {
       let goUrl = server.path + filePath; 
       previewImg.src = goUrl;
     } 
     else
     {
       let file = detailState.uploadFiles[2];
       let reader = new FileReader();
     
       reader.onload = ( function(file) {
         return function(e) {
           previewImg.src = e.target.result;
         };
       })(file);
       
       reader.readAsDataURL(file);
     } 
       
     previewWindow.document.body.appendChild(previewImg);
   }

  /////////////////////////////////////////////////////////////////////////
  const jsonCompResult = ( resJson ) => {
    
    if( resJson.resCd !== "0000" )
    {
      toast.error(intl.formatMessage({
        id: "error.during.save",
        defaultMessage: "저장 중 오류가 발생했습니다."
      }));
      return;
    }
    else
    {
      if( PROC_CHK === "UPD" )
      {
        PROC_CHK = "";
        //navigate("/admin/employers-detail", { mbrUid : location.state.mbrUid });
        toast.info(intl.formatMessage({
          id: "info.recruiter.info.edited",
          defaultMessage: "리쿠르터 정보가 수정되었습니다."
        }));        
        //initSetHandle();
        return;
      }
      else if( PROC_CHK === "UPDPSWD" )
      {
        PROC_CHK = "";
        toast.info(intl.formatMessage({
            id: "info.password.changed",
            defaultMessage: "비밀번호가 변경되었습니다"
          }));      
        return;
      }
      else if( PROC_CHK === "FILEUPLOAD" )
      {
        PROC_CHK = "";
        let jsonStr = JSON.stringify(resJson.resFileList);
        let fileList = JSON.parse(jsonStr);

       
        let tmpMbrFilePosNm = detailState.mbrFilePosNm;
        let tmpMbrList = detailState.mbrFileJsonStr;
        let tmpExtraFilePosNm = detailState.extraFilePosNm;
        let tmpExtraList = detailState.extraFileJsonStr;

        let pos = 0;
        for(var i=0; i<fileList.length; i++ )
        {
          for(let n=0; n<tmpMbrFilePosNm.length; n++)
          {
            if( tmpMbrFilePosNm[n] == fileList[i].filePosNm)
            {
              tmpMbrList[n] = JSON.stringify( fileList[i] );
            }
          }

          for(let n=0; n<tmpExtraFilePosNm.length; n++)
          {
            if( tmpExtraFilePosNm[n] == fileList[i].filePosNm)
            {
              tmpExtraList[n] = JSON.stringify( fileList[i] );
            }
          }
        }
        
        setDetailState({
          ...detailState,
          fileUploadCmplt: true
          ,mbrFileJsonStr: tmpMbrList
          ,extraFileJsonStr: tmpExtraList
        });
        

        
      }
    }
  }
  
  async function goPswdCRUD() {
    PROC_CHK = "UPDPSWD";

    var tmpPswd = document.getElementById("password").value;

    let goUrl = server.path + "/mbr/pswd";
    let data = {
      pswd: tmpPswd,
      mbrUid: detailState.mbrUid
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    jsonCompResult(axiosRes.data);
  }

  async function goCRUD() {
    let fileStorePath = "Globals.fileStoreBizPath"; //로컬저장경로
    let fileLinkPath = "Globals.fileLinkBizPath"; //link경로
    let filePrefix = "biz"; //파일코드

    PROC_CHK = "FILEUPLOAD";

    let uploadRes = await snglFileUpload( server.path, fileStorePath, fileLinkPath, filePrefix, detailState.uploadFiles, detailState.filePosNm);
    //jsonCompResult(uploadRes.data);
    if(uploadRes === 0) cntntsCRUD();
    else jsonCompResult(uploadRes.data);
  }

  async function cntntsCRUD() {
    PROC_CHK = 'UPD';
  
    console.log("CNTNTS CRUD");
  
    let goUrl = server.path + "/mbr/mbrupdate";
    let data = {mbrUid: locationState.mbrUid};
    //console.log("cntntsCRUD == " +  detailState.snglImgFileJsonStr);
    let axiosRes = await axiosCrudOnSubmit(detailState, goUrl, "POST");
  
    jsonCompResult(axiosRes.data);
  }

  return (
    <main>
      <ToastContainer />
      <section className="formSection">
        <div className="formHeader">
          <h1>
            <FormattedMessage
              id="member.manage"
              defaultMessage="회원관리"
            />
            <em>
              <FormattedMessage
                id="recruiter.manage"
                defaultMessage="리크루터 관리"
              />
            </em>
          </h1>
          <h2>
            <FormattedMessage
              id="personal.information"
              defaultMessage="개인정보"
            />
          </h2>
        </div>
        <article>
          <form>
            <div className="formWrap">
              <ul className="twoCol">
                <li>
                  <label htmlFor="recruiterName">
                    <FormattedMessage
                      id="name"
                      defaultMessage="이름"
                    />
                  </label>
                  <input type="text" id="recruiterName" 
                    value={detailState.mbrNm} 
                    onChange={(e) => {
                      setDetailState({
                        ...detailState,
                        mbrNm : e.target.value
                      })
                    }}/>
                </li>
                <li>
                  <label htmlFor="position">
                    <FormattedMessage
                      id="position"
                      defaultMessage="직급"
                    />
                  </label>
                  <input type="text" id="position" 
                    value={detailState.mbrPos} 
                    onChange={(e) => {
                      setDetailState({
                        ...detailState,
                        mbrPos : e.target.value
                      })
                    }}/>
                </li>
              </ul>
              <ul className="twoCol">
                <li>
                  <label htmlFor="maxCase">
                    <FormattedMessage
                      id="maximum.case.available"
                      defaultMessage="최대 진행 가능 건수"
                    />
                  </label>
                  <input type="text" id="maxCase" 
                    value={detailState.maxHire} 
                    onChange={(e) => {
                      setDetailState({
                        ...detailState,
                        maxHire : e.target.value
                      })
                    }}/>
                </li>
                <li>
                  <label htmlFor="possible">
                    <FormattedMessage
                      id="recommendable.people.number"
                      defaultMessage="추천가능 인재명수"
                    />
                  </label>
                  <input type="text" id="possible" 
                    value={detailState.maxCnddt} 
                    onChange={(e) => {
                      setDetailState({
                        ...detailState,
                        maxCnddt : e.target.value
                      })
                    }}/>
                </li>
              </ul>
              <ul className="twoCol">
                <li>
                  <label htmlFor="joinDate">
                    <FormattedMessage
                      id="join.date"
                      defaultMessage="가입일"
                    />
                  </label>
                  <span>{detailState.joinDttm}</span>
                </li>
                <li>
                  <span className="labelSpan">
                    <FormattedMessage
                      id="gender"
                      defaultMessage="성별"
                    />
                  </span>
                  <div className="radioInput">
                    <input type="radio" id="male" name="gender" value="male" 
                      checked={(detailState.gndr === "male") ? true:false} 
                      onChange={ (e) => {
                        setDetailState({
                          ...detailState,
                          gndr: e.target.value
                        })
                      }}/>
                    <label htmlFor="male">
                      <FormattedMessage
                        id="male"
                        defaultMessage="남"
                      />
                    </label>
                    <input type="radio" id="female" name="gender" value="female" 
                      checked={(detailState.gndr === "female") ? true:false}
                      onChange={ (e) => {
                        setDetailState({
                          ...detailState,
                          gndr: e.target.value
                        })
                      }}/>
                    <label htmlFor="female">
                      <FormattedMessage
                        id="female"
                        defaultMessage="여"
                      />
                    </label>
                  </div>
                </li>
              </ul>
              <ul className="twoCol">
                <li>
                  <label htmlFor="birth">
                    <FormattedMessage
                      id="birth.date"
                      defaultMessage="생년월일"
                    />(8<FormattedMessage
                      id="character"
                      defaultMessage="자"
                    />)
                  </label>
                  <input type="text" id="birth" 
                    value={detailState.brthd}
                    onChange={(e) => {
                      setDetailState({
                        ...detailState,
                        brthd : e.target.value
                      })
                    }}/>
                </li>
                <li>
                  <label htmlFor="phone">
                    <FormattedMessage
                      id="phone.number"
                      defaultMessage="휴대폰 번호"
                    />
                  </label>
                  <input type="text" id="phone" 
                    value={detailState.celpNum} 
                    onChange={(e) => {
                      setDetailState({
                        ...detailState,
                        celpNum : e.target.value
                      })
                    }}/>
                </li>
              </ul>
              <ul className="twoCol">
                <li>
                  <label htmlFor="email">
                    <FormattedMessage
                      id="email"
                      defaultMessage="이메일"
                    />
                  </label>
                  <input type="text" id="email" 
                    value={detailState.email} 
                    onChange={(e) => {
                      setDetailState({
                        ...detailState,
                        email : e.target.value
                      })
                    }}/>
                </li>
                <li>
                  <label htmlFor="id">
                    <FormattedMessage
                      id="id"
                      defaultMessage="아이디"
                    />
                  </label>
                  <input type="text" id="id" 
                    value={detailState.mbrId} readOnly/>
                </li>
              </ul>
              <ul className="twoCol">
                <li>
                  <label htmlFor="password">
                    <FormattedMessage
                      id="password"
                      defaultMessage="비밀번호"
                    />
                  </label>
                  <input type="text" id="password" 
                    onChange={(e) => {
                      setDetailState({
                        ...detailState,
                        pswd : e.target.value
                      })
                    }}/>
                  <button type="button" className="formButton" onClick={goPswdCRUD}>
                    <FormattedMessage
                      id="change"
                      defaultMessage="변경"
                    /> &gt;
                  </button>
                </li>
              </ul>
              <ul className="oneCol">
                <li className="adminFileBox">
                  <span className="labelSpan">
                    <FormattedMessage
                      id="profile.picture"
                      defaultMessage="프로필 사진"
                    />
                  </span>
                  <div className="flex">
                    <div className="flex">
                      <label htmlFor="imgFile" className="basicButton">
                        <FormattedMessage
                          id="image.attach"
                          defaultMessage="이미지 첨부"
                        /> &gt;
                      </label>
                      <span className="fileName">
                        {detailState.thmnlNm}
                      </span>
                      <input type="file" id="imgFile" className="fileInput" 
                          accept=".png, .jpg, .jpeg, .pdf"
                          onChange={(e) => {
                            let tmpList = detailState.uploadFiles;
                            tmpList[1] = e.target.files[0];
  
                            let tmpJsonStrList = detailState.snglImgFileJsonStr;
                            tmpJsonStrList[1] = "";
                            
                            setDetailState({
                              ...detailState,
                              uploadFiles: tmpList,
                              snglImgFileJsonStr: tmpJsonStrList,
                              thmnl: "",
                              thmnlNm: e.target.files[0].name
                            })
                        }}/>
                    </div>
                    <div>
                      <button type="button" className="basicButton" onClick={() => handlePreview(detailState.thmnl, detailState.thmnlNm)}>
                        <FormattedMessage
                          id="show.picture"
                          defaultMessage="사진 보기"
                        />
                      </button>
                    </div>
                  </div>
                </li>
              </ul>
              <ul className="oneCol">
                <span className="labelSpan">
                  <FormattedMessage
                    id="interest.keyword"
                    defaultMessage="관심분야 키워드"
                  />
                </span>
                <div className='flexDirectionColumn'>
                  <p>
                    <FormattedMessage
                      id="input.interest"
                      defaultMessage="관심분야를 입력해주세요."
                    />
                    <FormattedMessage
                      id="spacebar.enter"
                      defaultMessage="(스페이스바나 엔터를 누르면 입력됩니다.)"
                    />
                  </p>
                </div>
              </ul>

              <ul className="oneCol">
                <li>
                  <label htmlFor="link">
                    <FormattedMessage
                      id="linkedin.link"
                      defaultMessage="링크드인 링크 수정"
                    />
                  </label>
                  <input type="text" className="fullWidth" 
                    value={detailState.hmpg} 
                    onChange={(e) => {
                      setDetailState({
                        ...detailState,
                        hmpg : e.target.value
                      })
                    }}/>
                </li>
              </ul>
              <ul className="oneCol">
                <label htmlFor="introduction">
                  <FormattedMessage
                    id="recruiter.self.intro"
                    defaultMessage="리크루터 자기소개"
                  />
                </label>
              </ul>
            </div>
            <h2>
              <FormattedMessage
                id="company.info.add"
                defaultMessage="회사 정보 추가"
              />
            </h2>
            <div className="formWrap">
              <ul className="twoCol">
                <li>
                  <label htmlFor="companyName">
                    <FormattedMessage
                      id="company.name"
                      defaultMessage="회사명"
                    /> 
                  </label>
                  <input type="text" id="companyName" 
                    value={detailState.bizNm}
                    onChange={(e) => {
                      setDetailState({
                        ...detailState,
                        bizNm : e.target.value
                      })
                    }}/>
                </li>
                <li>
                  <label htmlFor="ceoName">
                    <FormattedMessage
                      id="ceo.name"
                      defaultMessage="대표자명"
                    />
                  </label>
                  <input type="text" id="ceoName" 
                    value={detailState.ceoNm} 
                    onChange={(e) => {
                      setDetailState({
                        ...detailState,
                        ceoNm : e.target.value
                      })
                    }}/>
                </li>
              </ul>
              <ul className="twoCol">
                <li>
                  <label htmlFor="licenseNumber">
                    <FormattedMessage
                      id="corporate.number"
                      defaultMessage="사업자등록번호"
                    />
                  </label>
                  <input type="text" id="licenseNumber" 
                    value={detailState.bizNum}
                    onChange={(e) => {
                      setDetailState({
                        ...detailState,
                        bizNum : e.target.value
                      })
                    }}/>
                </li>
                <li>
                  <label htmlFor="hompage">
                    <FormattedMessage
                      id="homepage"
                      defaultMessage="홈페이지"
                    />
                  </label>
                  <input type="text" id="hompage" 
                    value={detailState.bizHmpg} 
                    onChange={(e) => {
                      setDetailState({
                        ...detailState,
                        bizHmpg : e.target.value
                      })
                    }}/>
                </li>
              </ul>

              {/* 회사 주소 daum API */}
              <AddressInput state={detailState} setState={setDetailState}/>
            </div>
            <h2>
              <FormattedMessage
                id="identity.info.add"
                defaultMessage="신원인증 정보 추가"
              />
            </h2>
            <div className="formWrap">
              <ul className="oneCol">
                <li className="adminFileBox adminFlexColumn">
                  <p>
                    <FormattedMessage
                      id="upload.attach.file"
                      defaultMessage="첨부파일 등록 (기업 - 사업자등록증 / 개인 - 주민등록증 or 운전면허증)"
                    />
                  </p>
                  <label htmlFor="extraImgFile" className="basicButton">
                    <FormattedMessage
                      id="image.attachment"
                      defaultMessage="이미지 첨부"
                    /> &gt;
                  </label>
                  <span className="fileName">
                    {detailState.extraThmnlNm}
                  </span>
                  <input type="file" id="extraImgFile" className="fileInput" 
                    onChange={(e) => {
                      let tmpList = detailState.uploadFiles;
                      tmpList[2] = e.target.files[0];

                      let tmpJsonStrList = detailState.snglImgFileJsonStr;
                      tmpJsonStrList[2] = "";
                      
                      setDetailState({
                        ...detailState,
                        uploadFiles: tmpList,
                        snglImgFileJsonStr: tmpJsonStrList,
                        extraThmnl: "",
                        extraThmnlNm: e.target.files[0].name
                      })
                    }}/>
                </li>
              </ul>
              <ul className="oneCol">
                <li>
                  <label htmlFor="account">
                    <FormattedMessage
                      id="account.manage"
                      defaultMessage="계좌관리"
                    />
                  </label>
                  <select name="account" id="account" className="marginRight6" 
                    value={detailState.bankNm}
                    onChange={(e)=>{
                      setDetailState({
                        ...detailState,
                        bankNm: e.target.value
                      })
                    }}>
                    <option value="">
                      {intl.formatMessage({
                        id: "select.bank",
                        defaultMessage: "은행선택" 
                      })} 
                    </option>
                    {bankCd.map((type, idx) => (                        
                    <option value={type.cateCd}>{type.cdName}</option>
                    ))}                       
                  </select>
                  <input type="text" 
                    placeholder={intl.formatMessage({
                      id: "placeholder.account",
                      defaultMessage: "계좌번호를 입력해주세요" 
                    })}className="fullWidth" 
                    value={detailState.bankAccntNum}
                    onChange={(e)=>{
                      setDetailState({
                        ...detailState,
                        bankAccntNum : e.target.value
                      })
                    }}/>
                </li>
              </ul>
            </div>
            <div className="buttons">
              <button type="button" onClick={goCRUD}>
                <FormattedMessage
                  id="save"
                  defaultMessage="저장하기"
                /> &gt;
              </button>
              <Link to={locationState.backUrl}>
                <FormattedMessage
                  id="list"
                  defaultMessage="리스트"
                />
              </Link>
            </div>
          </form>
        </article>
      </section>
    </main>
  );
}