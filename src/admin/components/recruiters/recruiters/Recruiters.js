import {Link, useLocation, useNavigate} from 'react-router-dom';
import { useCallback, useContext, useEffect, useRef, useState } from 'react';
import { Context } from 'contexts';
import axios from 'axios';
import fileDownload from 'js-file-download';

import { axiosCrudOnSubmit } from 'commons/modules/commonUtil';
import { Pagination } from 'commons/components/pagination/Pagination';
import { DownloadBtns } from 'commons/components/download-btn/DownloadBtns';
import { snglFileUpload } from 'commons/modules/multipartUtil';
import * as defVal  from 'commons/modules/defVal';
import { AddressInput } from 'commons/components/AddressInput';
import { ImagePreview } from 'commons/components/inputs/image-preview/ImagePreview';
import { UPD_MBRUID } from 'contexts/actionTypes';
import { FormattedMessage } from 'react-intl';

export const Recruiters = () => {

  const [listState, setListState] = useState([]);
  const [sConts, setSConts] = useState("");
  const [fltrState, setFltrState] = useState({});
  const [checkedMailYns, setCheckedMailYns] = useState([]);
  const componentRef = useRef();
  const moment = require('moment');
  
  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid")
    ,ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi")
  };

  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(10);

  const indexOfLast = currentPage * postsPerPage;
  const indexOfFirst = indexOfLast - postsPerPage;

  let arraySimb = "<:;>";

  const {
    state : {
        mbr,
        server
    },
    dispatch,
  } = useContext(Context);

  function currentPosts(tmp) {
      let currentPosts = 0;
      currentPosts = tmp.slice(indexOfFirst, indexOfLast);
      return currentPosts;
  }
  

  const headers = [
    { label: "아이디", key: "mbrId" },
    { label: "이름", key: "mbrNm" },
    { label: "휴대폰", key: "celpNum" },
    { label: "이메일", key: "email" },
    { label: "가입일시", key: "joinDttm" },
    { label: "탈퇴일시", key: "wthdrDttm" },
    { label: "메일링", key: "mailYn" },
  ];

  function currentPosts(tmp) {
    let currentPosts = 0;
    currentPosts = tmp.slice(indexOfFirst, indexOfLast);
    return currentPosts;
  }

  const changeFltr = (fltr) => {
    let tmpFltr = (fltr === "ASC") ? "DESC" :
                  (fltr === "DESC") ? "ASC" :
                  "ASC";

    return tmpFltr;
  }

  useEffect(async () => {
    console.log("Page First Load EFFECT");
    initSetHandle();      
    
  },[fltrState])

  async function initSetHandle() {
    let goUrl = server.path + "/mbr/mbrlist";
    let data = {
      ssnMbrUid : ssn.ssnMbrUid,
      ssnMbrDivi : ssn.ssnMbrDivi,
      mbrUid : mbr.mbrUid,
      mbrDivi : "MBRDIVI-02",
      sConts : sConts,
      sFltr : fltrState,
      sFltrCate : Object.keys(fltrState)[0],
      sFltrCont : Object.values(fltrState)[0],
    }

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    setListState(axiosRes.data.mbrArr);      
  }

  useEffect( async () => {
    window.scrollTo(0,0);
  },[listState])

  ///////////////////////////////////////////////////////////////////////////
  const setMailYn = useCallback(async (e, mbrUid) => {
    
    let goUrl = server.path + "/mbr/mailYn";
    
    let data = {
      mailYn: (e.target.checked) ? "Y" : "N",
      mbrUid : mbrUid
    }

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    if(e.target.checked)e.target.checked = false;
    else e.target.checked = true;
  })
  
  const onKeyUpSrch =(e) => {
    if(e.key == "Enter")initSetHandle();   
    return false;
  }

  return (
    <main>
    <section className="tableSection">
      <div className="sectionHeader">
        <h3>
          <span className="material-icons-outlined">
            settings
          </span>
          <b>
            <FormattedMessage
              id="recruiter.manage"
              defaultMessage="리크루터 관리"
            />
          </b>
        </h3>
        <DownloadBtns headers={headers} dataset={listState} fileName="리크루터 관리" componentRef={componentRef}/>
      </div>
      <div className="searchBar">
        <label htmlFor="search">Search:</label>
        <input type="search" id="search" 
          onChange={(e) => {
            setSConts(e.target.value);
          }}
          
          onKeyUp={(e) => {onKeyUpSrch(e);}}
          />
        <button type="button" onClick={initSetHandle}>
          <FormattedMessage
            id="search"
            defaultMessage="검색"
          />
        </button>
      </div>
      <article id="capture" ref={componentRef}>
        <table>
          <thead>
            <tr>
              <th>
                <div className='wrap'>
                  <span>
                    <FormattedMessage
                      id="number"
                      defaultMessage="번호"
                    />
                  </span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </div>
              </th>
              <th className={ fltrState.mbrId === "DESC" ? "descending" :
                                fltrState.mbrId === "ASC" ? "ascending" :
                                undefined } 
                    onClick={() => {
                      setFltrState({
                        mbrId : changeFltr(fltrState.mbrId)
                      });
                    }}>
                <div className='wrap'>
                  <span>
                    <FormattedMessage
                      id="id"
                      defaultMessage="아이디"
                    />
                  </span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </div>
              </th>
              <th className={ fltrState.mbrNm === "DESC" ? "descending" :
                                fltrState.mbrNm === "ASC" ? "ascending" :
                                undefined } 
                    onClick={() => {
                      setFltrState({
                        mbrNm : changeFltr(fltrState.mbrNm)
                      });
                    }}>
                <span>
                  <FormattedMessage
                    id="name"
                    defaultMessage="이름"
                  />
                </span>
                <div className="filterArrow">
                  <span />
                  <span />
                </div>
              </th>
              <th className={ fltrState.celpNum === "DESC" ? "descending" :
                                fltrState.celpNum === "ASC" ? "ascending" :
                                undefined } 
                    onClick={() => {
                      setFltrState({
                        celpNum : changeFltr(fltrState.celpNum)
                      });
                    }}>
                <span>
                  <FormattedMessage
                    id="phone"
                    defaultMessage="휴대폰"
                  />
                </span>
                <div className="filterArrow">
                  <span />
                  <span className="none" />
                </div>
              </th>
              <th className={ fltrState.email === "DESC" ? "descending" :
                                fltrState.email === "ASC" ? "ascending" :
                                undefined } 
                    onClick={() => {
                      setFltrState({
                        email : changeFltr(fltrState.email)
                      });
                    }}>
                <span>
                  <FormattedMessage
                    id="email"
                    defaultMessage="이메일"
                  />
                </span>
                <div className="filterArrow">
                  <span />
                  <span />
                </div>
              </th>
              <th className={ fltrState.joinDttm === "DESC" ? "descending" :
                                fltrState.joinDttm === "ASC" ? "ascending" :
                                undefined } 
                    onClick={() => {
                      setFltrState({
                        joinDttm : changeFltr(fltrState.joinDttm)
                      });
                    }}>
                <span>
                  <FormattedMessage
                    id="join.date.time"
                    defaultMessage="가입일시"
                  />
                </span>
                <div className="filterArrow">
                  <span />
                  <span />
                </div>
              </th>
              <th className={ fltrState.wthdrDttm === "DESC" ? "descending" :
                                fltrState.wthdrDttm === "ASC" ? "ascending" :
                                undefined } 
                    onClick={() => {
                      setFltrState({
                        wthdrDttm : changeFltr(fltrState.wthdrDttm)
                      });
                    }}>
                <span>
                  <FormattedMessage
                    id="leave.date.time"
                    defaultMessage="탈퇴일시"
                  />
                </span>
                <div className="filterArrow">
                  <span />
                  <span />
                </div>
              </th>
              <th>
                <span>
                  <FormattedMessage
                    id="mailing"
                    defaultMessage="메일링"
                  />
                </span>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr className="noData" style={{display: 'none'}}>
              <th colSpan={9}>No data available in table</th>
            </tr>
            {currentPosts(listState).map((list, idx)=>(
              <tr key={list.mbrUid}>
                <td>{indexOfFirst + idx + 1}</td>
                <td>
                  <Link to='/admin/recruiter-detail'
                  state={{ mbrUid: list.mbrUid }}>
                    {list.mbrId}
                  </Link>
                </td>
                <td>{list.mbrNm}</td>
                <td>{list.celpNum}</td>
                <td>{list.email}</td>
                <td>{ moment(list.joinDttm).format("YYYY년 MM월 DD일 HH시 mm분") }</td>
                <td>{ (list.wthdrDttm !== "") ? moment(list.wthdrDttm).format("YYYY년 MM월 DD일 HH시 mm분") : "" }</td>
                <td>
                  <input type="checkbox" key={list.mbrUid} id={list.mbrUid} 
                    checked={ (list.mailYn === 'Y')? true:false }
                    onClick={(e) => {
                      //checkHandle(e, idx);
                      setMailYn(e, list.mbrUid);
                    }}/>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </article>

      <Pagination postsPerPage={postsPerPage} totalPosts={listState.length} paginate={setCurrentPage} currentPage={currentPage}/>

    </section>
  </main>
  );
}