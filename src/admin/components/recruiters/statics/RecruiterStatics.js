import { StarRating } from 'commons/components/star-rating/StarRating';
import profileSampleImg from '../../images/profile-sample.jpg';

import {Link, useLocation, useNavigate} from 'react-router-dom';
import { useContext, useEffect, useState } from 'react';
import { Context } from 'contexts';
import axios from 'axios';
import fileDownload from 'js-file-download';

import { axiosCrudOnSubmit } from 'commons/modules/commonUtil';
import { snglFileUpload } from 'commons/modules/multipartUtil';
import * as defVal  from 'commons/modules/defVal';
import { AddressInput } from 'commons/components/AddressInput';
import { TagTemplate } from 'commons/components/tags/TagTemplate';
import EditorComponent from 'admin/components/editor/EditorComponent';
import { ImagePreview } from 'commons/components/inputs/image-preview/ImagePreview';
import { FormattedMessage } from 'react-intl';

export const RecruiterStatics =() => {

  let PROC_CHK = "";

  const {
    state : {
        server,
        mbr
    },
    dispatch,
  } = useContext(Context);

  
  const location = useLocation();
  const navigate = useNavigate();
  const moment = require('moment');
  let previewWindow = "";
  let previewImg = document.createElement("img");  

  const [files, setFiles] = useState("");
  const [axiosFilesRes, setAxiosFilesRes] = useState("");
  
  const [mbrFilesRes, setMbrFilesRes] = useState([]);
  const [bizFilesRes, setBizFilesRes] = useState([]);
  const [extraFilesRes, setExtraFilesRes] = useState([]);

  const salesSizeCd = defVal.SalesSizeCd();
  const empSizeCd = defVal.EmpSizeCd();
  const bankCd = defVal.BankCd();
  
  const [bizCateCd, setBizCateCd] = useState([]);
  const [detailState, setDetailState] = useState(defVal.setAxiosMbrState(null));
  const [tagCnt, setTagCnt] = useState(0);

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid")
    ,ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi")
  };

  let locationState = {
    mbrUid : location.state.mbrUid,
    mbrDivi : "MBRDIVI-02",
    backUrl: (location.state.backUrl)?(location.state.backUrl):('/admin/recruiters')
  }

  useEffect(async () => {
    console.log("Page First Load EFFECT");
    initSetHandle();          
  },[])

  
  async function initSetHandle() {

    detailState.filePosNm[1] = "thmnlNm";
    detailState.filePosNm[2] = "extraThmnlNm";
    detailState.ssnMbrUid = ssn.ssnMbrUid;
    
    let goUrl = server.path + "/mbr/mbrdetail"
    let data = {
      ssnMbrUid : ssn.ssnMbrUid,
      ssnMbrDivi : ssn.ssnMbrDivi,
      mbrUid : locationState.mbrUid,
      mbrDivi : locationState.mbrDivi
    }
    
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST"); 

    let tagstr = axiosRes.data.tags;
    var tags = defVal.SplitToArray(tagstr, "<;:>");
    axiosRes.data.tags = tags;

    setDetailState(axiosRes.data);    

  }
  

  const OnErrorMbrImg = (id) => {
    document.getElementById(id).src=server.mbrNoImg;
  }

  return(
    <main className='adminRecruiterStatics'>
      <section>
        <h2 className="sectionTitle">
          <span>
            <FormattedMessage
              id="select.bank"
              defaultMessage="은행선택"
            />
          </span> 
          <em>
            <FormattedMessage
              id="recruiter.show.detail"
              defaultMessage="리크루터 자세히보기"
            />
          </em>
        </h2>

        <div className="recruiterMypage">
          <div className="userInfo">
            <figure>
              
              <img 
                name="recThmnl" id="recThmnl" src={detailState.thmnl}
                onError={(e) => OnErrorMbrImg("recThmnl")} 
                alt="프로필"
              />
            </figure>
            <div>
              <h3>{detailState.mbrNm}</h3>
              
              <div className="stars">
                {(detailState.replyPoint >= 1)?(<span className="material-icons-outlined active">grade</span>):(<span className="material-icons-outlined ">grade</span>)}
                {(detailState.replyPoint >= 2)?(<span className="material-icons-outlined active">grade</span>):(<span className="material-icons-outlined ">grade</span>)}
                {(detailState.replyPoint >= 3)?(<span className="material-icons-outlined active">grade</span>):(<span className="material-icons-outlined ">grade</span>)}
                {(detailState.replyPoint >= 4)?(<span className="material-icons-outlined active">grade</span>):(<span className="material-icons-outlined ">grade</span>)}
                {(detailState.replyPoint >= 5)?(<span className="material-icons-outlined active">grade</span>):(<span className="material-icons-outlined ">grade</span>)}
                <span>&nbsp;&nbsp;{detailState.replyPoint} / 5.0</span>
              </div>

              <p>
                <span>{detailState.mbrId}</span>
                <span>{detailState.celpNum}</span>
                <span>{detailState.email}</span>
              </p>
            </div>
          </div>
          <div className="userActivity">
            <ul>
              <li>
                <FormattedMessage
                  id="project.ing"
                  defaultMessage="진행중 프로젝트"
                />
              </li>
              <li>{detailState.hireIngCnt}</li>
            </ul>
            <ul>
              <li>
                <FormattedMessage
                  id="recommend.success.rate"
                  defaultMessage="인재추천 성공율"
                />
              </li>
              <li>{detailState.recmmndScssRate}% &nbsp;<em>( {detailState.recmmndChooseCnt} <FormattedMessage
                  id="case"
                  defaultMessage="건"
                /> / {detailState.recmmndCnt}<FormattedMessage
                id="case"
                defaultMessage="건"
              /> )</em></li>              
            </ul>
            <ul>
              <li>
                <FormattedMessage
                  id="document.pass.rate"
                  defaultMessage="서류 합격율"
                />
              </li>
              <li>{detailState.recmmndPaperRate}% &nbsp;<em>( {detailState.recmmndPaperCnt}<FormattedMessage
                  id="case"
                  defaultMessage="건"
                /> / {detailState.recmmndCnt}<FormattedMessage
                id="case"
                defaultMessage="건"
              /> )</em></li>
            </ul>
            <ul>
              <li>
                <FormattedMessage
                  id="interview.pass.rate"
                  defaultMessage="면접 합격율"
                />
              </li>
              <li>{detailState.recmmndFaceRate}% &nbsp;<em>( {detailState.recmmndFaceCnt}<FormattedMessage
                  id="case"
                  defaultMessage="건"
                /> / {detailState.recmmndCnt}<FormattedMessage
                id="case"
                defaultMessage="건"
              /> )</em></li>
            </ul>
            <ul>
              <li>
                <FormattedMessage
                  id="project.done"
                  defaultMessage="종료된 프로젝트"
                />
              </li>
              <li>{detailState.hireEndCnt}</li>
            </ul>
          </div>
          <div className="average userActivity">
            <ul>
              <li>
                <FormattedMessage
                  id="average.recommend.number.position"
                  defaultMessage="포지션별 평균 추천인원"
                />
              </li>
              <li>{detailState.cateAvgRate}% <em>( {detailState.cateAvgCnt}<FormattedMessage
                  id="case"
                  defaultMessage="건"
                /> / {detailState.recmmndCnt}<FormattedMessage
                  id="case"
                  defaultMessage="건"
                /> )</em></li>
            </ul>
            <ul>
              <li>
                <FormattedMessage
                  id="average.first.time.recommend"
                  defaultMessage="첫 후보자 추천까지 평균소요시간"
                />
              </li>
              <li>{detailState.recmmndFrstDayAvgCnt*24}H ({detailState.recmmndFrstDayAvgCnt}D)</li>             
            </ul>
            <ul>
              <li>
                <FormattedMessage
                  id="average.time.recommend"
                  defaultMessage="후보자 추천시 평균소요시간"
                />
              </li>
              <li>{detailState.recmmndDayAvgCnt*24}H ({detailState.recmmndDayAvgCnt}D)</li>
            </ul>
          </div>
          <div className="selfIntro">
            <h4>
              <FormattedMessage
                id="recruiter.select"
                defaultMessage="리크루터 자기소개"
              />
            </h4>
            <div className="tags">
              <div className='tagList'>
                  {detailState.tags.map((text, idx) => (
                    (text !== "")?
                      (<span>{text}</span>  ):('')
                  ))}
              </div>
            </div>

            <div className='introductionText' dangerouslySetInnerHTML={{__html:detailState.intro}}>             
            </div>
          </div>
        </div>
        
        <div className="flexAlignCenter" >
          <Link to='/admin/statics' className='formButton' style={{marginTop:'20px', background:'#F9F9F9', border:'1px solid #D7D7D7', padding:'10px'}}>
            <FormattedMessage
              id="show.list"
              defaultMessage="리스트 보기"
            /> &gt;
          </Link>
        </div>
        
      </section>
      
    </main>
  )
}