import { ExcelDownload } from "commons/components/download-btn/ExcelDownload";

import { Link, useLocation, useNavigate } from "react-router-dom";
import { useContext, useEffect, useRef, useState } from "react";
import { Context } from "contexts";
import axios from "axios";
import fileDownload from "js-file-download";

import { axiosCrudOnSubmit } from "commons/modules/commonUtil";
import { Pagination } from "commons/components/pagination/Pagination";
import { DownloadBtns } from "commons/components/download-btn/DownloadBtns";
import { snglFileUpload } from "commons/modules/multipartUtil";
import * as defVal from "commons/modules/defVal";
import { AddressInput } from "commons/components/AddressInput";
import { ImagePreview } from "commons/components/inputs/image-preview/ImagePreview";
import { UPD_MBRUID } from "contexts/actionTypes";
import { FormattedMessage, useIntl } from "react-intl";

export const Statics = () => {
  const intl = useIntl();
  let PROC_CHK = "";

  const {
    state: { mbr, server },
    dispatch,
  } = useContext(Context);

  const [listState, setListState] = useState([]);
  const [sConts, setSConts] = useState("");
  const [fltrState, setFltrState] = useState({});
  const componentRef = useRef();
  const moment = require("moment");

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid"),
    ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi"),
  };

  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(10);

  const indexOfLast = currentPage * postsPerPage;
  const indexOfFirst = indexOfLast - postsPerPage;

  let arraySimb = "<:;>";

  const headers = [
    { label: "아이디", key: "mbrId" },
    { label: "성명", key: "mbrNm" },
    { label: "전체 지원 프로젝트", key: "cmmntCnt" },
    { label: "진행중 프로젝트", key: "cmmntChooseCnt" },
    { label: "인재추천 성공율", key: "recmmndScssRate" },
    { label: "서류 합격율", key: "recmmndPaperRate" },
    { label: "면접 합격율", key: "recmmndCnrctRate" },
    { label: "종료된 프로젝트", key: "hireEndCnt" },
    { label: "평균 추천인원", key: "recmmndCntAvg" },
    { label: "첫후보자 추천 평균시간", key: "recmmndFrstDayAvgCnt" },
    { label: "후보자 추천 평균시간", key: "recmmndDayAvgCnt" },
  ];

  function currentPosts(tmp) {
    let currentPosts = 0;
    currentPosts = tmp.slice(indexOfFirst, indexOfLast);
    return currentPosts;
  }

  useEffect(async () => {
    console.log("Page First Load EFFECT");
    initSetHandle();
  }, [fltrState]);

  async function initSetHandle() {
    let goUrl = server.path + "/sttcs/recstatlist";
    let data = {
      ssnMbrUid: ssn.ssnMbrUid,
      ssnMbrDivi: ssn.ssnMbrDivi,
      mbrUid: mbr.mbrUid,
      mbrDivi: "MBRDIVI-01",
      sConts: document.getElementById("sConts")
        ? document.getElementById("sConts").value
        : "",
      sFltr: fltrState,
      sFltrCate: Object.keys(fltrState)[0],
      sFltrCont: Object.values(fltrState)[0],
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    let resArr = axiosRes.data.resArr;
    for (var k = 0; k < resArr.length; k++) {
      resArr[k].recmmndScssRate =
        "(" +
        resArr[k].recmmndChooseCnt +
        "/" +
        resArr[k].recmmndCnt +
        ") " +
        resArr[k].recmmndScssRate +
        "%";
      resArr[k].recmmndPaperRate =
        "(" +
        resArr[k].recmmndPaperCnt +
        "/" +
        resArr[k].recmmndCnt +
        ") " +
        resArr[k].recmmndPaperRate +
        "%";
      resArr[k].recmmndFaceRate =
        "(" +
        resArr[k].recmmndFaceCnt +
        "/" +
        resArr[k].recmmndCnt +
        ") " +
        resArr[k].recmmndFaceRate +
        "%";

      resArr[k].recmmndFrstDayAvgCnt =
        resArr[k].recmmndFrstTmAvgCnt +
        "H (" +
        resArr[k].recmmndFrstDayAvgCnt +
        "D)";
      resArr[k].recmmndDayAvgCnt =
        resArr[k].recmmndTmAvgCnt + "H (" + resArr[k].recmmndDayAvgCnt + "D)";
    }

    setListState(resArr);
  }

  useEffect(async () => {
    window.scrollTo(0, 0);
  }, [listState]);

  ////////////////////////////////////////////////////////////////////////

  async function changeFltr(key, fltr) {
    let tmpFltr = fltr === "ASC" ? "DESC" : fltr === "DESC" ? "ASC" : "ASC";

    setFltrState({ [key]: tmpFltr });

    initSetHandle();
  }

  const onKeyUpSrch = (e) => {
    if (e.key === "Enter") initSetHandle();
    return false;
  };

  return (
    <main>
      <section className="tableSection">
        <div className="sectionHeader">
          <h3>
            <span className="material-icons-outlined">settings</span>
            <b>
              <FormattedMessage
                id="recruiter.statics"
                defaultMessage="리크루터 통계"
              />
            </b>
          </h3>

          <ExcelDownload
            headers={headers}
            dataset={listState}
            fileName="리크루터 통계 자료"
          />
        </div>
        <div className="searchBar">
          <label htmlFor="search">Search:</label>
          <input
            type="search"
            id="sConts"
            onKeyUp={(e) => {
              onKeyUpSrch(e);
            }}
          />
          <button type="button" onClick={initSetHandle}>
            <FormattedMessage id="search" defaultMessage="검색" />
          </button>
        </div>
        <article>
          <table>
            <thead>
              <tr>
                <th>
                  <span>
                    <FormattedMessage id="number" defaultMessage="번호" />
                  </span>
                </th>
                <th
                  className={
                    fltrState.mbrId === "DESC"
                      ? "descending"
                      : fltrState.mbrId === "ASC"
                      ? "ascending"
                      : undefined
                  }
                  onClick={() => {
                    changeFltr("mbrId", fltrState.mbrId);
                  }}
                >
                  <div className="wrap">
                    <span>
                      <FormattedMessage id="id" defaultMessage="아이디" />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th
                  className={
                    fltrState.mbrNm === "DESC"
                      ? "descending"
                      : fltrState.mbrNm === "ASC"
                      ? "ascending"
                      : undefined
                  }
                  onClick={() => {
                    changeFltr("mbrNm", fltrState.mbrNm);
                  }}
                >
                  <span>
                    <FormattedMessage id="name" defaultMessage="이름" />
                  </span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th
                  className={
                    fltrState.hireIngCnt === "DESC"
                      ? "descending"
                      : fltrState.hireIngCnt === "ASC"
                      ? "ascending"
                      : undefined
                  }
                  onClick={() => {
                    changeFltr("hireIngCnt", fltrState.hireIngCnt);
                  }}
                >
                  <div className="wrap">
                    <span>
                      <FormattedMessage
                        id="project.all.supported"
                        defaultMessage="전체 지원 프로젝트"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span className="none" />
                    </div>
                  </div>
                </th>
                <th
                  className={
                    fltrState.cmmntChooseCnt === "DESC"
                      ? "descending"
                      : fltrState.cmmntChooseCnt === "ASC"
                      ? "ascending"
                      : undefined
                  }
                  onClick={() => {
                    changeFltr("cmmntChooseCnt", fltrState.cmmntChooseCnt);
                  }}
                >
                  <div className="wrap">
                    <span>
                      <FormattedMessage
                        id="project.ing"
                        defaultMessage="진행중 프로젝트"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th
                  className={
                    fltrState.recmmndScssRate === "DESC"
                      ? "descending"
                      : fltrState.recmmndScssRate === "ASC"
                      ? "ascending"
                      : undefined
                  }
                  onClick={() => {
                    changeFltr("recmmndScssRate", fltrState.recmmndScssRate);
                  }}
                >
                  <div className="wrap">
                    <span>
                      <FormattedMessage
                        id="recommend.success.rate"
                        defaultMessage="인재추천 성공율"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th
                  className={
                    fltrState.recmmndPaperRate === "DESC"
                      ? "descending"
                      : fltrState.recmmndPaperRate === "ASC"
                      ? "ascending"
                      : undefined
                  }
                  onClick={() => {
                    changeFltr("recmmndPaperRate", fltrState.recmmndPaperRate);
                  }}
                >
                  <div className="wrap">
                    <span>
                      <FormattedMessage
                        id="document.pass.rate"
                        defaultMessage="서류 합격율"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th
                  className={
                    fltrState.recmmndCnrctRate === "DESC"
                      ? "descending"
                      : fltrState.recmmndCnrctRate === "ASC"
                      ? "ascending"
                      : undefined
                  }
                  onClick={() => {
                    changeFltr("recmmndCnrctRate", fltrState.recmmndCnrctRate);
                  }}
                >
                  <div className="wrap">
                    <span>
                      <FormattedMessage
                        id="interview.pass.rate"
                        defaultMessage="면접 합격율"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th
                  className={
                    fltrState.hireEndCnt === "DESC"
                      ? "descending"
                      : fltrState.hireEndCnt === "ASC"
                      ? "ascending"
                      : undefined
                  }
                  onClick={() => {
                    changeFltr("hireEndCnt", fltrState.hireEndCnt);
                  }}
                >
                  <div className="wrap">
                    <span>
                      <FormattedMessage
                        id="project.done"
                        defaultMessage="종료된 프로젝트"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th
                  className={
                    fltrState.recmmndCntAvg === "DESC"
                      ? "descending"
                      : fltrState.recmmndCntAvg === "ASC"
                      ? "ascending"
                      : undefined
                  }
                  onClick={() => {
                    changeFltr("recmmndCntAvg", fltrState.recmmndCntAvg);
                  }}
                >
                  <div className="wrap">
                    <span>
                      <FormattedMessage
                        id="average.recommend.number"
                        defaultMessage="평균 추천인원"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th
                  className={
                    fltrState.recmmndFrstDayAvgCnt === "DESC"
                      ? "descending"
                      : fltrState.recmmndCrecmmndFrstDayAvgCntntAvg === "ASC"
                      ? "ascending"
                      : undefined
                  }
                  onClick={() => {
                    changeFltr(
                      "recmmndFrstDayAvgCnt",
                      fltrState.recmmndFrstDayAvgCnt
                    );
                  }}
                >
                  <div className="wrap">
                    <span>
                      <FormattedMessage
                        id="average.first.time.recommend.short"
                        defaultMessage="첫 후보자 추천 소요시간"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th
                  className={
                    fltrState.recmmndDayAvgCnt === "DESC"
                      ? "descending"
                      : fltrState.recmmndDayAvgCnt === "ASC"
                      ? "ascending"
                      : undefined
                  }
                  onClick={() => {
                    changeFltr("recmmndDayAvgCnt", fltrState.recmmndDayAvgCnt);
                  }}
                >
                  <span>
                    <FormattedMessage
                      id="average.time.recommend.short"
                      defaultMessage="후보자추천 평균시간"
                    />
                  </span>
                </th>
              </tr>
            </thead>
            <tbody>
              <tr className="noData" style={{ display: "none" }}>
                <th colSpan={9}>No data available in table</th>
              </tr>
              {currentPosts(listState).map((list, idx) => (
                <tr key={list.mbrUid}>
                  <td>{indexOfFirst + idx + 1}</td>
                  <td>
                    <Link
                      to="/admin/statics-detail"
                      state={{ mbrUid: list.mbrUid }}
                    >
                      {list.mbrId}
                    </Link>
                  </td>
                  <td>{list.mbrNm}</td>
                  <td>{list.cmmntCnt}</td>
                  <td>{list.hireIngCnt}</td>
                  <td>{list.recmmndScssRate}</td>
                  <td>{list.recmmndPaperRate}</td>
                  <td>{list.recmmndFaceRate}</td>
                  <td>{list.hireEndCnt}</td>
                  <td>{list.recmmndCntAvg}</td>
                  <td>{list.recmmndFrstDayAvgCnt}</td>
                  <td>{list.recmmndDayAvgCnt}</td>
                </tr>
              ))}
              <tr></tr>
            </tbody>
          </table>
        </article>

        <Pagination
          postsPerPage={postsPerPage}
          totalPosts={listState.length}
          paginate={setCurrentPage}
          currentPage={currentPage}
        />
      </section>
    </main>
  );
};
