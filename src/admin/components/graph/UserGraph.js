import "../scss/graph.scss";
import DashboardGraph from "../graph/graph";
import { FormattedMessage } from "react-intl";

export const UserGraph = (props) => {
  let empArr = [];
  let recArr = [];
  let empCnt = 0;
  let recCnt = 0;
  for (var k = 0; k < props.listState.length; k++) {
    var tmpRecData = {
      x: props.listState[k].sttcDttm,
      y: parseInt(props.listState[k].recCnt),
    };
    recCnt = recCnt + parseInt(props.listState[k].recCnt);
    recArr.push(tmpRecData);

    var tmpEmpData = {
      x: props.listState[k].sttcDttm,
      y: parseInt(props.listState[k].empCnt),
    };
    empCnt = empCnt + parseInt(props.listState[k].empCnt);
    empArr.push(tmpEmpData);
  }

  const data = [
    {
      id: "인사 담당자",
      color: "hsl(240, 0%, 45%)",
      data: empArr,
    },
    {
      id: "리크루터",
      color: "hsl(351, 80%, 61%)",
      data: recArr,
    },
  ];
  return (
    <>
      <ul className="graphIndex">
        <li className="redColored">
          <span></span>
          <div>
            <p>
              <FormattedMessage id="employer" defaultMessage="인사담당자" />
            </p>
            <p>
              <strong>{empCnt}</strong>
              <FormattedMessage id="people.count" defaultMessage="명" />
            </p>
          </div>
        </li>
        <li className="greyColored">
          <span></span>
          <div>
            <p>
              <FormattedMessage id="recruiter" defaultMessage="리크루터" />
            </p>
            <p>
              <strong>{recCnt}</strong>
              <FormattedMessage id="people.count" defaultMessage="명" />
            </p>
          </div>
        </li>
      </ul>
      <DashboardGraph data={data} />
    </>
  );
};
