import "../scss/graph.scss";
import DashboardGraph from "../graph/graph";
import { FormattedMessage } from "react-intl";

export const ProjectGraph = (props) => {
  let totArr = [];
  let pubArr = [];
  let cntrctArr = [];
  let totCnt = 0;
  let pubCnt = 0;
  let cntrctCnt = 0;

  for (var k = 0; k < props.listState.length; k++) {
    var tmpTotData = {
      x: props.listState[k].sttcDttm,
      y: parseInt(props.listState[k].hireTotCnt),
    };
    totCnt = totCnt + parseInt(props.listState[k].hireTotCnt);
    totArr.push(tmpTotData);

    var tmpPubData = {
      x: props.listState[k].sttcDttm,
      y: parseInt(props.listState[k].hirePubCnt),
    };
    pubCnt = pubCnt + parseInt(props.listState[k].hirePubCnt);
    pubArr.push(tmpPubData);

    var tmpCntrctData = {
      x: props.listState[k].sttcDttm,
      y: parseInt(props.listState[k].cntrctCnt),
    };
    cntrctCnt = cntrctCnt + parseInt(props.listState[k].cntrctCnt);
    cntrctArr.push(tmpCntrctData);
  }

  const data = [
    {
      id: "전체",
      color: "hsl(240, 0%, 45%)",
      data: totArr,
    },
    {
      id: "활성화",
      color: "hsl(351, 80%, 61%)",
      data: pubArr,
    },
    {
      id: "성사됨",
      color: "hsl(203, 8%, 20%)",
      data: cntrctArr,
    },
  ];

  return (
    <>
      <ul className="graphIndex">
        <li className="greyColored">
          <span></span>
          <div>
            <p>
              <FormattedMessage id="all" defaultMessage="전체" />
            </p>
            <p>
              <strong>{totCnt}</strong>
              <FormattedMessage id="count" defaultMessage="개" />
            </p>
          </div>
        </li>
        <li className="blackColored">
          <span></span>
          <div>
            <p>
              <FormattedMessage id="active" defaultMessage="활성화" />
            </p>
            <p>
              <strong>{pubCnt}</strong>
              <FormattedMessage id="count" defaultMessage="개" />
            </p>
          </div>
        </li>
        <li className="redColored">
          <span></span>
          <div>
            <p>
              <FormattedMessage id="accomplished" defaultMessage="성사됨" />
            </p>
            <p>
              <strong>{cntrctCnt}</strong>
              <FormattedMessage id="count" defaultMessage="개" />
            </p>
          </div>
        </li>
      </ul>

      <DashboardGraph data={data} />
    </>
  );
};
