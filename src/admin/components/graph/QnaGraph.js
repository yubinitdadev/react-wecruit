import "../scss/graph.scss";
import DashboardGraph from "../graph/graph";
import { FormattedMessage } from "react-intl";

export const QnaGraph = (props) => {
  let empQsCntArr = [];
  let recQsCntArr = [];
  let admnQsCntArr = [];
  let empQsCnt = 0;
  let recQsCnt = 0;
  let admnQsCnt = 0;

  for (var k = 0; k < props.listState.length; k++) {
    var tmpEmpQsData = {
      x: props.listState[k].sttcDttm,
      y: parseInt(props.listState[k].empQsCnt),
    };
    empQsCnt = empQsCnt + parseInt(props.listState[k].empQsCnt);
    empQsCntArr.push(tmpEmpQsData);

    var tmpRecQsData = {
      x: props.listState[k].sttcDttm,
      y: parseInt(props.listState[k].recQsCnt),
    };
    recQsCnt = recQsCnt + parseInt(props.listState[k].recQsCnt);
    recQsCntArr.push(tmpRecQsData);

    var tmpAdmnData = {
      x: props.listState[k].sttcDttm,
      y: parseInt(props.listState[k].admnQsCnt),
    };
    admnQsCnt = admnQsCnt + parseInt(props.listState[k].admnQsCnt);
    admnQsCntArr.push(tmpAdmnData);
  }

  const data = [
    {
      id: "인사담당자",
      color: "hsl(351, 80%, 61%)",
      data: empQsCntArr,
    },
    {
      id: "리크루터",
      color: "hsl(240, 0%, 45%)",
      data: recQsCntArr,
    },
    {
      id: "관리자",
      color: "hsl(203, 8%, 20%)",
      data: admnQsCntArr,
    },
  ];

  return (
    <>
      <ul className="graphIndex">
        <li className="redColored">
          <span></span>
          <div>
            <p>
              <FormattedMessage id="employer" defaultMessage="인사담당자" />
            </p>
            <p>
              <strong>{empQsCnt}</strong>
              <FormattedMessage id="count" defaultMessage="개" />
            </p>
          </div>
        </li>
        <li className="greyColored">
          <span></span>
          <div>
            <p>
              <FormattedMessage id="recruiter" defaultMessage="리크루터" />
            </p>
            <p>
              <strong>{recQsCnt}</strong>
              <FormattedMessage id="count" defaultMessage="개" />
            </p>
          </div>
        </li>
        <li className="blackColored">
          <span></span>
          <div>
            <p>
              <FormattedMessage id="administrator" defaultMessage="관리자" />
            </p>
            <p>
              <strong>{admnQsCnt}</strong>
              <FormattedMessage id="count" defaultMessage="개" />
            </p>
          </div>
        </li>
      </ul>
      <DashboardGraph data={data} />
    </>
  );
};
