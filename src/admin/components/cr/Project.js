import { ExcelDownload } from 'commons/components/download-btn/ExcelDownload';
import Modal from "modal-all/ModalAll";
import { ManageCr } from "modal-all/modals/ManageCr";

import {Link, useLocation, useNavigate} from 'react-router-dom';
import { useContext, useEffect, useRef, useState } from 'react';
import { Context } from 'contexts';
import axios from 'axios';
import fileDownload from 'js-file-download';

import { axiosCrudOnSubmit } from 'commons/modules/commonUtil';
import * as commonUtil from 'commons/modules/commonUtil';
import { Pagination } from 'commons/components/pagination/Pagination';
import { DownloadBtns } from 'commons/components/download-btn/DownloadBtns';
import { snglFileUpload } from 'commons/modules/multipartUtil';
import * as defVal  from 'commons/modules/defVal';
import { AddressInput } from 'commons/components/AddressInput';
import { ImagePreview } from 'commons/components/inputs/image-preview/ImagePreview';
import { UPD_MBRUID } from 'contexts/actionTypes';
import { FormattedMessage, useIntl } from 'react-intl';

export const Project =()=>{
  const intl = useIntl();

  let PROC_CHK = "";

  const {
    state : {
        mbr,
        server
    },
    dispatch,
  } = useContext(Context);


  const [listState, setListState] = useState([]);
  const [sConts, setSConts] = useState("");
  const [fltrState, setFltrState] = useState({});
  const componentRef = useRef();
  const moment = require('moment');

  
  const bizKndCd = defVal.BizKndCd();
  const salesSizeCd = defVal.SalesSizeCd();
  const empSizeCd = defVal.EmpSizeCd();
  const hireSizeCd = defVal.HireSizeCd();
  const hireStatCd = defVal.HireStatCd();

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid")
    ,ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi")
  };

  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(10);

  const indexOfLast = currentPage * postsPerPage;
  const indexOfFirst = indexOfLast - postsPerPage;

  let arraySimb = "<:;>";

  useEffect(async () => {
    console.log("Page First Load EFFECT");
    initSetHandle();      
    
  },[fltrState])
  
  async function initSetHandle() {
    
    var today = commonUtil.getToday();
    var sStrtDttm = commonUtil.getAddToday(-10);
    var sEndDttm = today;
    
    
    let goUrl = server.path + "/sttcs/hiretotstatlist";
    let data = {
      ssnMbrUid : ssn.ssnMbrUid,
      ssnMbrDivi : ssn.ssnMbrDivi,
      mbrUid : mbr.mbrUid,
      mbrDivi : "MBRDIVI-01",
      sHireStat : (document.getElementById("sHireStat"))?(document.getElementById("sHireStat").value):(''),
      sConts : (document.getElementById("sConts"))?(document.getElementById("sConts").value):(''),
      sStrtDttm : (document.getElementById("sStrtDttm").value)?(document.getElementById("sStrtDttm").value):(sStrtDttm),
      sEndDttm : (document.getElementById("sEndDttm").value)?(document.getElementById("sEndDttm").value):(sEndDttm),
      sFltr : fltrState,
      sFltrCate : Object.keys(fltrState)[0],
      sFltrCont : Object.values(fltrState)[0],
    }

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    let resArr = axiosRes.data.resArr;
    
   
    for(var k=0; k<resArr.length; k++)
    {
      if( resArr[k].confirmTmCnt == null )resArr[k].confirmTmCnt="";
      else resArr[k].confirmTmCnt = resArr[k].confirmTmCnt+"H ("+ resArr[k].confirmDtCnt+ "D)";

      if( resArr[k].minChooseTmCnt == null )resArr[k].minChooseTmCnt="";
      else resArr[k].minChooseTmCnt = resArr[k].minChooseTmCnt+"H ("+ resArr[k].minChooseDtCnt+ "D)";

      for(var m=0; m<hireStatCd.length; m++)
      {
        if(hireStatCd[m].cateCd == resArr[k].hireStat )
        {
          resArr[k].hireStat = hireStatCd[m].cdName;
          break;
        }
      }   
      
      if( resArr[k].chooseCmmntABCnt > 0 ) resArr[k].chooseCmmntABCnt = resArr[k].chooseCmmntABCnt + " / 0" 
      else if( resArr[k].chooseCmmntABCnt > 0 ) resArr[k].chooseCmmntABCnt = "0 / " + (resArr[k].chooseCmmntABCnt*-1) 
      else resArr[k].chooseCmmntABCnt = "0 / 0";

      if( resArr[k].recmmntABCnt > 0 ) resArr[k].recmmntABCnt = resArr[k].recmmntABCnt + " / 0" 
      else if( resArr[k].recmmntABCnt > 0 ) resArr[k].recmmntABCnt = "0 / " + (resArr[k].recmmntABCnt*-1) 
      else resArr[k].recmmntABCnt = "0 / 0";

      if( resArr[k].qaABCnt > 0 ) resArr[k].qaABCnt = resArr[k].qaABCnt + " / 0" 
      else if( resArr[k].qaABCnt > 0 ) resArr[k].qaABCnt = "0 / " + (resArr[k].qaABCnt*-1) 
      else resArr[k].qaABCnt = "0 / 0";

      if( resArr[k].recmmnt01ABCnt > 0 ) resArr[k].recmmnt01ABCnt = resArr[k].recmmnt01ABCnt + " / 0" 
      else if( resArr[k].recmmnt01ABCnt > 0 ) resArr[k].recmmnt01ABCnt = "0 / " + (resArr[k].recmmnt01ABCnt*-1) 
      else resArr[k].recmmnt01ABCnt = "0 / 0";

      if( resArr[k].recmmnt02ABCnt > 0 ) resArr[k].recmmnt02ABCnt = resArr[k].recmmnt02ABCnt + " / 0" 
      else if( resArr[k].recmmnt02ABCnt > 0 ) resArr[k].recmmnt02ABCnt = "0 / " + (resArr[k].recmmnt02ABCnt*-1) 
      else resArr[k].recmmnt02ABCnt = "0 / 0";

      if( resArr[k].recmmnt04ABCnt > 0 ) resArr[k].recmmnt04ABCnt = resArr[k].recmmnt04ABCnt + " / 0" 
      else if( resArr[k].recmmnt04ABCnt > 0 ) resArr[k].recmmnt04ABCnt = "0 / " + (resArr[k].recmmnt04ABCnt*-1) 
      else resArr[k].recmmnt04ABCnt = "0 / 0";

      if( resArr[k].recmmnt15ABCnt > 0 ) resArr[k].recmmnt15ABCnt = resArr[k].recmmnt15ABCnt + " / 0" 
      else if( resArr[k].recmmnt15ABCnt > 0 ) resArr[k].recmmnt15ABCnt = "0 / " + (resArr[k].recmmnt15ABCnt*-1) 
      else resArr[k].recmmnt15ABCnt = "0 / 0";

      if( resArr[k].replyABCnt > 0 ) resArr[k].replyABCnt = resArr[k].replyABCnt + " / 0" 
      else if( resArr[k].replyABCnt > 0 ) resArr[k].replyABCnt = "0 / " + (resArr[k].replyABCnt*-1) 
      else resArr[k].replyABCnt = "0 / 0";
      
    }
    

    setListState(resArr);   
    
    if( document.getElementById("sStrtDttm").value == "" )document.getElementById("sStrtDttm").value = sStrtDttm;
    if( document.getElementById("sEndDttm").value == "" )document.getElementById("sEndDttm").value = sEndDttm;
    //
    //document.getElementById("sEndDttm").value = ;
  }

  

  useEffect( async () => {
    window.scrollTo(0,0);
  },[listState])


  ////////////////////////////////////////////////////////////////////////
  
  function currentPosts(tmp) {
    let currentPosts = 0;
    currentPosts = tmp.slice(indexOfFirst, indexOfLast);
    return currentPosts;
  }  

  async function changeFltr(key, fltr){
    
    let tmpFltr = (fltr == "ASC") ? "DESC" :
                  (fltr == "DESC") ? "ASC" :
                  "ASC";
    
    setFltrState({[key]:tmpFltr});

    //initSetHandle();
  }

  useEffect( async () => {
    initSetHandle();
  },[fltrState])

  const onKeyUpSrch =(e) => {
    if(e.key == "Enter")initSetHandle();   
    return false;
  }




  ///////////////////////////////////////////////////////////////////////

const [buttonOn, setButtonOn] = useState(false);
const [modalVisible1, setModalVisible1] = useState(false)




const openModal1 = () => {
  setModalVisible1(true)
}

const closeModal1 = () => {
  setModalVisible1(false)
}


const [buttonList, setButtonList] = useState({});

const handleButtonClick = (idx) => {

  let tmpBtnKey = "button" + idx;
  let tmpBtnVal = buttonList[tmpBtnKey];

  setButtonList({
    ...buttonList,
    [tmpBtnKey] : !tmpBtnVal
  })
};



///////////////////////////////////////////////////////////////////////////////

const headers = [
  { label: "기업명", key: "bizNm" },
  { label: "채용공고제목", key: "title" },
  { label: "채용등록일", key: "rgstDttm" },
  { label: "검수 완료일", key: "confirmDttm" },
  { label: "리크루터 선택일시", key: "minChooseDttm" },
  { label: "검수 소요시간", key: "confirmTmCnt" },
  { label: "리크루터 선택 소요시간", key: "minChooseTmCnt" },
  { label: "상태구분", key: "hireStat" },
  { label: "리크루터 모집마감", key: "recEndDttm" },
  { label: "인재 추천마감", key: "empRcmndEndDttm" },
  { label: "프로젝트 종료", key: "doneDttm" },
  { label: "선택R A", key: "chooseCmmntACnt" },
  { label: "선택R B", key: "chooseCmmntBCnt" },
  { label: "선택R 증감", key: "chooseCmmntABCnt" },
  { label: "후보자 A", key: "recmmntACnt" },
  { label: "후보자 B", key: "recmmntBCnt" },
  { label: "후보자 증감", key: "recmmntABCnt" },
  { label: "Q&A A", key: "qaACnt" },
  { label: "Q&A B", key: "qaBCnt" },
  { label: "Q&A 증감", key: "qaABCnt" },
  { label: "채용전형 A", key: "recmmnt01ACnt" },
  { label: "채용전형 B", key: "recmmnt01BCnt" },
  { label: "채용전형 증감", key: "recmmnt01ABCnt" },
  { label: "서류전형 A", key: "recmmnt02ACnt" },
  { label: "서류전형 B", key: "recmmnt02BCnt" },
  { label: "서류전형 증감", key: "recmmnt02ABCnt" },
  { label: "면접전형 A", key: "recmmnt04ACnt" },
  { label: "면접전형 B", key: "recmmnt04BCnt" },
  { label: "면접전형 증감", key: "recmmnt04ABCnt" },
  { label: "최종채용 A", key: "recmmnt15ACnt" },
  { label: "최종채용 B", key: "recmmnt15BCnt" },
  { label: "최종채용 증감", key: "recmmnt15ABCnt" },
  { label: "CR A", key: "replyACnt" },
  { label: "CR B", key: "replyBCnt" },
  { label: "CR 증감", key: "replyABCnt" }
];


  return (  
    <main>    
      <section className="tableSection tableScroll clear">
        <div className="sectionHeader">
          <h3>
            <span className="material-icons-outlined">
              settings
            </span>
            <b>
              <FormattedMessage
                id="project.all.manage"
                defaultMessage="프로젝트별 전체 관리현황 보기"
              />
            </b>
          </h3>

          <ExcelDownload headers={headers} dataset={listState} fileName="프로젝트별 전체 관리현황 보기"/>
        </div>
        <div className="searchSelect">
          <select id="sHireStat">
            <option value="">
              {intl.formatMessage({
                id: "posting.all",
                defaultMessage: "전체 채용공고"
              })}
            </option>
            <option value="ing">
              {intl.formatMessage({
                id: "posting.ing",
                defaultMessage: "진행중 채용공고"
              })}
            </option>
            <option value="end">
              {intl.formatMessage({
                id: "posting.done",
                defaultMessage: "종료된 채용공고"
              })}
            </option>
          </select>
          <input type="date" id="sStrtDttm"/>
          <input type="date" id="sEndDttm"/>
          <input type="search" id="sConts"  onKeyUp={(e) => {onKeyUpSrch(e);}}/>
          <button type="button" onClick={initSetHandle}>
            <FormattedMessage
              id="search"
              defaultMessage="검색"
            />
          </button>
        </div> 
        <article className="clear">
          <table>
            <thead>
              <tr>
                <th colSpan={1} rowSpan={2}>
                  <span>번호</span>                  
                </th>
                <th colSpan={1} rowSpan={2}
                  className={ fltrState.bizNm == "DESC" ? "descending" :
                  fltrState.bizNm == "ASC" ? "ascending" :
                  undefined } 
                  onClick={() => {
                    changeFltr("bizNm", fltrState.bizNm);   
                  }}>
                  <span>
                    <FormattedMessage
                      id="company.name"
                      defaultMessage="기업명"
                    />
                  </span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th colSpan={1} rowSpan={2}
                  className={ fltrState.title == "DESC" ? "descending" :
                  fltrState.title == "ASC" ? "ascending" :
                  undefined } 
                  onClick={() => {
                    changeFltr("title", fltrState.title);  
                  }}
                  >
                  <span>
                    <FormattedMessage
                      id="recruitment.title"
                      defaultMessage="채용공고 제목"
                    />
                  </span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th colSpan={1} rowSpan={2}
                className={ fltrState.rgstDttm == "DESC" ? "descending" :
                fltrState.rgstDttm == "ASC" ? "ascending" :
                undefined } 
                onClick={() => {
                  changeFltr("rgstDttm", fltrState.rgstDttm);  
                }}>
                  <span>
                    <FormattedMessage
                      id="recruitment.registration.date"
                      defaultMessage="채용등록일"
                    />
                  </span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th colSpan={1} rowSpan={2}
                className={ fltrState.cnfrmDttm == "DESC" ? "descending" :
                fltrState.cnfrmDttm == "ASC" ? "ascending" :
                undefined } 
                onClick={() => {
                  changeFltr("cnfrmDttm", fltrState.cnfrmDttm);  
                }}>
                  <span>
                    <FormattedMessage
                      id="inspection.complete.date"
                      defaultMessage="검수 완료일"
                    />
                  </span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th colSpan={1} rowSpan={2}
                className={ fltrState.minChooseDttm == "DESC" ? "descending" :
                fltrState.minChooseDttm == "ASC" ? "ascending" :
                undefined } 
                onClick={() => {
                  changeFltr("minChooseDttm", fltrState.minChooseDttm);  
                }}>
                  <span>
                    <FormattedMessage
                      id="recruiter.select.date"
                      defaultMessage="리크루터 선택일시"
                    />
                  </span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th colSpan={1} rowSpan={2}
                className={ fltrState.confirmTmCnt == "DESC" ? "descending" :
                fltrState.confirmTmCnt == "ASC" ? "ascending" :
                undefined } 
                onClick={() => {
                  changeFltr("confirmTmCnt", fltrState.confirmTmCnt);  
                }}>
                  <span>
                    <FormattedMessage
                      id="inspection.time.required"
                      defaultMessage="검수 소요시간"
                    />
                  </span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th colSpan={1} rowSpan={2}
                className={ fltrState.minChooseTmCnt == "DESC" ? "descending" :
                fltrState.minChooseTmCnt == "ASC" ? "ascending" :
                undefined } 
                onClick={() => {
                  changeFltr("minChooseTmCnt", fltrState.minChooseTmCnt);  
                }}>
                  <span>
                    <FormattedMessage
                      id="recruiter.select.time.required"
                      defaultMessage="리크루터 선택 소요시간"
                    />
                  </span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th colSpan={4} rowSpan={1}>
                  <FormattedMessage
                    id="status"
                    defaultMessage="상태"
                  />
                </th>
                <th colSpan={3} rowSpan={1}>
                  <FormattedMessage
                    id="recruiter.been.selected"
                    defaultMessage="선택된 리크루터"
                  />
                </th>
                <th colSpan={3} rowSpan={1}>
                  <FormattedMessage
                    id="candidate.recommend"
                    defaultMessage="후보자 추천"
                  />
                </th>
                <th colSpan={3} rowSpan={1}>
                  <FormattedMessage
                    id="qna.comment"
                    defaultMessage="Q&amp;A 댓글"
                  />
                </th>
                <th colSpan={3} rowSpan={1}>
                  <FormattedMessage
                    id="recruitment.progress"
                    defaultMessage="채용전형 진행현황"
                  />
                </th>
                <th colSpan={3} rowSpan={1}>
                  <FormattedMessage
                    id="recruitment.document.progress"
                    defaultMessage="서류전형 현황"
                  />
                </th>
                <th colSpan={3} rowSpan={1}>
                  <FormattedMessage
                    id="recruitment.interview.progress"
                    defaultMessage="면접전형 현황"
                  />
                </th>
                <th colSpan={3} rowSpan={1}>
                  <FormattedMessage
                    id="recruitment.determined"
                    defaultMessage="최종채용확정"
                  />
                </th>
                <th colSpan={4} rowSpan={1}>
                  <FormattedMessage
                    id="cr.activity"
                    defaultMessage="CR 활동"
                  />
                </th>
              </tr>
              <tr>
                <th colSpan={1} rowSpan={1}
                className={ fltrState.hireStat === "DESC" ? "descending" :
                fltrState.hireStat === "ASC" ? "ascending" :
                undefined } 
                onClick={() => {
                  changeFltr("hireStat", fltrState.hireStat);                       
                }}>
                  <span>
                    <FormattedMessage
                      id="status.category"
                      defaultMessage="상태구분"
                    />
                  </span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th colSpan={1} rowSpan={1}
                className={ fltrState.recEndDttm === "DESC" ? "descending" :
                fltrState.recEndDttm === "ASC" ? "ascending" :
                undefined } 
                onClick={() => {
                  changeFltr("recEndDttm", fltrState.recEndDttm);                       
                }}>
                  <span>
                    <FormattedMessage
                      id="recruiter.recruiting.done"
                      defaultMessage="리크루터 모집마감"
                    />
                  </span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th colSpan={1} rowSpan={1}
                className={ fltrState.empRcmndEndDttm === "DESC" ? "descending" :
                fltrState.empRcmndEndDttm === "ASC" ? "ascending" :
                undefined } 
                onClick={() => {
                  changeFltr("empRcmndEndDttm", fltrState.empRcmndEndDttm);                       
                }}>
                  <span>
                    <FormattedMessage
                      id="recommendation.done"
                      defaultMessage="인재 추천마감"
                    />
                  </span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th colSpan={1} rowSpan={1}
                className={ fltrState.doneDttm === "DESC" ? "descending" :
                fltrState.doneDttm === "ASC" ? "ascending" :
                undefined } 
                onClick={() => {
                  changeFltr("doneDttm", fltrState.doneDttm);                       
                }}>
                  <span>
                    <FormattedMessage
                      id="project.finish"
                      defaultMessage="프로젝트 종료"
                    />
                  </span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th colSpan={1} rowSpan={1}
                className={ fltrState.chooseCmmntACnt === "DESC" ? "descending" :
                fltrState.chooseCmmntACnt === "ASC" ? "ascending" :
                undefined } 
                onClick={() => {
                  changeFltr("chooseCmmntACnt", fltrState.chooseCmmntACnt);                       
                }}>
                  <span>A</span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th colSpan={1} rowSpan={1}
                className={ fltrState.chooseCmmntBCnt === "DESC" ? "descending" :
                fltrState.chooseCmmntBCnt === "ASC" ? "ascending" :
                undefined } 
                onClick={() => {
                  changeFltr("chooseCmmntBCnt", fltrState.chooseCmmntBCnt);                       
                }}>
                  <span>B</span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th colSpan={1} rowSpan={1}
                className={ fltrState.chooseCmmntABCnt === "DESC" ? "descending" :
                fltrState.chooseCmmntABCnt === "ASC" ? "ascending" :
                undefined } 
                onClick={() => {
                  changeFltr("chooseCmmntABCnt", fltrState.chooseCmmntABCnt);                       
                }}>
                  <span>
                    <FormattedMessage
                      id="plus.minus"
                      defaultMessage="증감"
                    />
                  </span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th colSpan={1} rowSpan={1}
                className={ fltrState.recmmntACnt === "DESC" ? "descending" :
                fltrState.recmmntACnt === "ASC" ? "ascending" :
                undefined } 
                onClick={() => {
                  changeFltr("recmmntACnt", fltrState.recmmntACnt);                       
                }}>
                  <span>A</span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th colSpan={1} rowSpan={1}
                className={ fltrState.recmmntBCnt === "DESC" ? "descending" :
                fltrState.recmmntBCnt === "ASC" ? "ascending" :
                undefined } 
                onClick={() => {
                  changeFltr("recmmntBCnt", fltrState.recmmntBCnt);                       
                }}>
                  <span>B</span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th colSpan={1} rowSpan={1}
                className={ fltrState.recmmntABCnt === "DESC" ? "descending" :
                fltrState.recmmntABCnt === "ASC" ? "ascending" :
                undefined } 
                onClick={() => {
                  changeFltr("recmmntABCnt", fltrState.recmmntABCnt);                       
                }}>
                  <span>
                    <FormattedMessage
                      id="plus.minus"
                      defaultMessage="증감"
                    />
                  </span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th colSpan={1} rowSpan={1}
                className={ fltrState.qaACnt === "DESC" ? "descending" :
                fltrState.qaACnt === "ASC" ? "ascending" :
                undefined } 
                onClick={() => {
                  changeFltr("qaACnt", fltrState.qaACnt);                       
                }}>
                  <span>A</span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th colSpan={1} rowSpan={1}
                className={ fltrState.qaBCnt === "DESC" ? "descending" :
                fltrState.qaBCnt === "ASC" ? "ascending" :
                undefined } 
                onClick={() => {
                  changeFltr("qaBCnt", fltrState.qaBCnt);                       
                }}>
                  <span>B</span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th colSpan={1} rowSpan={1}
                className={ fltrState.qaABCnt === "DESC" ? "descending" :
                fltrState.qaABCnt === "ASC" ? "ascending" :
                undefined } 
                onClick={() => {
                  changeFltr("qaABCnt", fltrState.qaABCnt);                       
                }}>
                  <span>
                    <FormattedMessage
                      id="plus.minus"
                      defaultMessage="증감"
                    />
                  </span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th colSpan={1} rowSpan={1}
                className={ fltrState.recmmnt01ACnt === "DESC" ? "descending" :
                fltrState.recmmnt01ACnt === "ASC" ? "ascending" :
                undefined } 
                onClick={() => {
                  changeFltr("recmmnt01ACnt", fltrState.recmmnt01ACnt);                       
                }}>
                  <span>A</span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th colSpan={1} rowSpan={1}
                className={ fltrState.recmmnt01BCnt === "DESC" ? "descending" :
                fltrState.recmmnt01BCnt === "ASC" ? "ascending" :
                undefined } 
                onClick={() => {
                  changeFltr("recmmnt01BCnt", fltrState.recmmnt01BCnt);                       
                }}>
                  <span>B</span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th colSpan={1} rowSpan={1}
                className={ fltrState.recmmnt01ABCnt === "DESC" ? "descending" :
                fltrState.recmmnt01ABCnt === "ASC" ? "ascending" :
                undefined } 
                onClick={() => {
                  changeFltr("recmmnt01ABCnt", fltrState.recmmnt01ABCnt);                       
                }}>
                  <span>
                    <FormattedMessage
                      id="plus.minus"
                      defaultMessage="증감"
                    />
                  </span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>

                <th colSpan={1} rowSpan={1}
                className={ fltrState.recmmnt02ACnt === "DESC" ? "descending" :
                fltrState.recmmnt02ACnt === "ASC" ? "ascending" :
                undefined } 
                onClick={() => {
                  changeFltr("recmmnt02ACnt", fltrState.recmmnt02ACnt);                       
                }}>
                  <span>A</span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th colSpan={1} rowSpan={1}
                className={ fltrState.recmmnt02BCnt === "DESC" ? "descending" :
                fltrState.recmmnt02BCnt === "ASC" ? "ascending" :
                undefined } 
                onClick={() => {
                  changeFltr("recmmnt02BCnt", fltrState.recmmnt02BCnt);                       
                }}>
                  <span>B</span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th colSpan={1} rowSpan={1}
                className={ fltrState.recmmnt02ABCnt === "DESC" ? "descending" :
                fltrState.recmmnt02BCnt === "ASC" ? "ascending" :
                undefined } 
                onClick={() => {
                  changeFltr("recmmnt02ABCnt", fltrState.recmmnt02ABCnt);                       
                }}>
                  <span>
                    <FormattedMessage
                      id="plus.minus"
                      defaultMessage="증감"
                    />
                  </span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                
                <th colSpan={1} rowSpan={1}
                className={ fltrState.recmmnt04ACnt === "DESC" ? "descending" :
                fltrState.recmmnt04ACnt === "ASC" ? "ascending" :
                undefined } 
                onClick={() => {
                  changeFltr("recmmnt04ACnt", fltrState.recmmnt04ACnt);                       
                }}>
                  <span>A</span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th colSpan={1} rowSpan={1}
                className={ fltrState.recmmnt04BCnt === "DESC" ? "descending" :
                fltrState.recmmnt04BCnt === "ASC" ? "ascending" :
                undefined } 
                onClick={() => {
                  changeFltr("recmmnt04BCnt", fltrState.recmmnt04BCnt);                       
                }}>
                  <span>B</span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th colSpan={1} rowSpan={1}
                className={ fltrState.recmmnt04ABCnt === "DESC" ? "descending" :
                fltrState.recmmnt04ABCnt === "ASC" ? "ascending" :
                undefined } 
                onClick={() => {
                  changeFltr("recmmnt04ABCnt", fltrState.recmmnt04ABCnt);                       
                }}>
                   <span>
                    <FormattedMessage
                      id="plus.minus"
                      defaultMessage="증감"
                    />
                  </span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th colSpan={1} rowSpan={1}
                className={ fltrState.recmmnt15ACnt === "DESC" ? "descending" :
                fltrState.recmmnt15ACnt === "ASC" ? "ascending" :
                undefined } 
                onClick={() => {
                  changeFltr("recmmnt15ACnt", fltrState.recmmnt15ACnt);                       
                }}>
                  <span>A</span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th colSpan={1} rowSpan={1}
                className={ fltrState.recmmnt15BCnt === "DESC" ? "descending" :
                fltrState.recmmnt15BCnt === "ASC" ? "ascending" :
                undefined } 
                onClick={() => {
                  changeFltr("recmmnt15BCnt", fltrState.recmmnt15BCnt);                       
                }}>
                  <span>B</span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th colSpan={1} rowSpan={1}
                className={ fltrState.recmmnt15ABCnt === "DESC" ? "descending" :
                fltrState.recmmnt15ABCnt === "ASC" ? "ascending" :
                undefined } 
                onClick={() => {
                  changeFltr("recmmnt15ABCnt", fltrState.recmmnt15ABCnt);                       
                }}>
                  <span>
                    <FormattedMessage
                      id="plus.minus"
                      defaultMessage="증감"
                    />
                  </span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th colSpan={1} rowSpan={1}
                className={ fltrState.replyACnt === "DESC" ? "descending" :
                fltrState.replyACnt === "ASC" ? "ascending" :
                undefined } 
                onClick={() => {
                  changeFltr("replyACnt", fltrState.replyACnt);                       
                }}>
                  <span>A</span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th colSpan={1} rowSpan={1}
                className={ fltrState.replyBCnt === "DESC" ? "descending" :
                fltrState.replyBCnt === "ASC" ? "ascending" :
                undefined } 
                onClick={() => {
                  changeFltr("replyBCnt", fltrState.replyBCnt);                       
                }}>
                  <span>B</span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th colSpan={1} rowSpan={1}
                className={ fltrState.replyABCnt === "DESC" ? "descending" :
                fltrState.replyABCnt === "ASC" ? "ascending" :
                undefined } 
                onClick={() => {
                  changeFltr("replyABCnt", fltrState.replyABCnt);                       
                }}>
                  <span>
                    <FormattedMessage
                      id="plus.minus"
                      defaultMessage="증감"
                    />
                  </span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th colSpan={1} rowSpan={1}>
                  <span>
                    <FormattedMessage
                      id="etc"
                      defaultMessage="기타"
                    />
                  </span>
                </th>
              </tr>
            </thead>
            <tbody>
              <tr className="noData" style={{display: 'none'}}>
                <th colSpan={9}>No data available in table</th>
              </tr>
              {currentPosts(listState).map((list, idx)=>(
                  <tr>
                    <td>{idx+1}</td>
                    <td>{list.bizNm}</td>
                    <td className="width250">{list.title}</td>
                    <td>{list.rgstDttm}</td>
                    <td>{list.confirmDttm}</td>
                    <td>{list.minChooseDttm}</td>
                    <td>{list.confirmTmCnt}</td>
                    <td>{list.minChooseTmCnt}</td>
                    <td>{list.hireStat}</td>
                    <td>{list.recEndDttm}</td>
                    <td>{list.empRcmndEndDttm}</td>
                    <td>{list.doneDttm}</td>
                    
                    <td>{list.chooseCmmntACnt}</td>
                    <td>{list.chooseCmmntBCnt}</td>
                    <td>{list.chooseCmmntABCnt}</td>
                    
                    <td>{list.recmmntACnt}</td>
                    <td>{list.recmmntBCnt}</td>
                    <td>{list.recmmntABCnt}</td>

                    <td>{list.qaACnt}</td>
                    <td>{list.qaBCnt}</td>
                    <td>{list.qaABCnt}</td>

                    <td>{list.recmmnt01ACnt}</td>
                    <td>{list.recmmnt01BCnt}</td>
                    <td>{list.recmmnt01ABCnt}</td>

                    <td>{list.recmmnt02ACnt}</td>
                    <td>{list.recmmnt02BCnt}</td>
                    <td>{list.recmmnt02ABCnt}</td>

                    <td>{list.recmmnt04ACnt}</td>
                    <td>{list.recmmnt04BCnt}</td>
                    <td>{list.recmmnt04ABCnt}</td>

                    <td>{list.recmmnt15ACnt}</td>
                    <td>{list.recmmnt15BCnt}</td>
                    <td>{list.recmmnt15ABCnt}
                    </td>

                    <td>{list.replyACnt}</td>
                    <td>{list.replyBCnt}</td>
                    <td>{list.replyABCnt}</td>

                    <td className="additionalFunction">
                      <button id={"openMenuBtn" + idx} onClick={ () => {
                      handleButtonClick(idx);
                      }}>
                        <FormattedMessage
                          id="additional.function"
                          defaultMessage="추가기능"
                        />
                        <span className="material-icons">
                          keyboard_arrow_down
                        </span>
                      </button>
                      
                      <ul key={idx+1} id={"menuBtn" + idx}  
                        className = {buttonList['button' + idx] ? "active" : ""}>
                        <li>
                          <Link to='/admin/recruiter-manage' state={{ hireUid: list.hireUid }}>
                            <span className="material-icons">
                              content_copy
                            </span>
                            <FormattedMessage
                              id="recruiter.manage"
                              defaultMessage="리크루터 관리"
                            />
                          </Link>
                        </li>
                        <li>
                          <Link to='/admin/candidate-manage' state={{ hireUid: list.hireUid }}>
                            <span className="material-icons-outlined">
                              local_offer
                            </span>
                            <FormattedMessage
                              id="candidate.manage"
                              defaultMessage="후보자 관리"
                            />
                          </Link>
                        </li>
                        <li onClick={openModal1}>
                          <span className="material-icons-outlined">
                            local_offer
                          </span>
                          <FormattedMessage
                            id="cr.manage"
                            defaultMessage="CR 관리"
                          />
                        </li>
                      </ul>
                      {
                        <Modal visible={modalVisible1} closable={true} maskClosable={true} onClose={closeModal1} nobutton={true} >
                          <ManageCr />
                        </Modal>
                      } 
                    </td>
                  </tr>
                ))}
            </tbody>
          </table>
        </article>
      
        <Pagination postsPerPage={postsPerPage} totalPosts={listState.length} paginate={setCurrentPage} currentPage={currentPage}/>
      </section>
    </main>
  );
}