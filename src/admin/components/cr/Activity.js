import { ExcelDownload } from 'commons/components/download-btn/ExcelDownload';
import {Link, useLocation, useNavigate} from 'react-router-dom';
import { useContext, useEffect, useRef, useState } from 'react';
import { Context } from 'contexts';

import { axiosCrudOnSubmit } from 'commons/modules/commonUtil';
import * as commonUtil from 'commons/modules/commonUtil';
import { Pagination } from 'commons/components/pagination/Pagination';
import { DownloadBtns } from 'commons/components/download-btn/DownloadBtns';
import { snglFileUpload } from 'commons/modules/multipartUtil';
import * as defVal  from 'commons/modules/defVal';
import { AddressInput } from 'commons/components/AddressInput';
import { ImagePreview } from 'commons/components/inputs/image-preview/ImagePreview';
import { UPD_MBRUID } from 'contexts/actionTypes';
import { FormattedMessage } from 'react-intl';

export const Activity =()=>{

  let PROC_CHK = "";

  const {
    state : {
        mbr,
        server
    },
    dispatch,
  } = useContext(Context);


  const [listState, setListState] = useState([]);
  const [sConts, setSConts] = useState("");
  const [fltrState, setFltrState] = useState({});
  const componentRef = useRef();
  const moment = require('moment');

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid")
    ,ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi")
  };

  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(10);

  const indexOfLast = currentPage * postsPerPage;
  const indexOfFirst = indexOfLast - postsPerPage;

  let arraySimb = "<:;>";

  
  function currentPosts(tmp) {
    let currentPosts = 0;
    currentPosts = tmp.slice(indexOfFirst, indexOfLast);
    return currentPosts;
  }

  useEffect(async () => {
    console.log("Page First Load EFFECT");
    initSetHandle();      
    
  },[fltrState])
  
  async function initSetHandle() {
    
    //var tmpArr = [];
    //setListState(tmpArr); 
    
    let goUrl = server.path + "/sttcs/recactivitylist";
    let data = {
      ssnMbrUid : ssn.ssnMbrUid,
      ssnMbrDivi : ssn.ssnMbrDivi,
      mbrUid : mbr.mbrUid,
      mbrDivi : "MBRDIVI-01",
      sConts : (document.getElementById("sConts"))?(document.getElementById("sConts").value):(''),
      sFltr : fltrState,
      sFltrCate : Object.keys(fltrState)[0],
      sFltrCont : Object.values(fltrState)[0],
    }

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    let resArr = axiosRes.data.resArr;
    for(var k=0; k<resArr.length; k++)
    {
      if(resArr[k].crecChooseDttm == "" )resArr[k].rcmmndStat = "지연"
      else resArr[k].rcmmndStat = "추천"

      resArr[k].hireTmCnt = resArr[k].hireTmCnt +"H (" + resArr[k].hireDayCnt + "D)";      
    }    

    setListState(resArr);      
  }

  useEffect( async () => {
    //window.scrollTo(0,0);
  },[listState])

  ////////////////////////////////////////////////////////////////////////
  async function changeFltr(key, fltr){
    
    let tmpFltr = (fltr == "ASC") ? "DESC" :
                  (fltr == "DESC") ? "ASC" :
                  "ASC";
    
    setFltrState({[key]:tmpFltr});

    //initSetHandle();
  }

  useEffect( async () => {
    initSetHandle();
  },[fltrState])

  const onKeyUpSrch =(e) => {
    if(e.key == "Enter")initSetHandle();   
    return false;
  }

  /////////////////////////////////////////////////////////////////////////

  const headers = [
    { label: "기업명", key: "bizNm" },
    { label: "채용공고 제목", key: "title" },
    { label: "검수일시", key: "confirmDttm" },
    { label: "선택(계약)일시", key: "crecChooseDttm" },
    { label: "계약 R", key: "crecMbrNm" },
    { label: "전화번호", key: "crecCelpNum" },
    { label: "후보자수", key: "rcmmndCnt" },
    { label: "진행상황", key: "rcmmndStat" },
    { label: "시간(일)", key: "hireTmCnt" }
  ];

  return (  
    <main>
      <section className="tableSection">
        <div className="sectionHeader">
          <h3>
            <span className="material-icons-outlined">
              settings
            </span>
            <b>
              <FormattedMessage
                id="recruiter.activity"
                defaultMessage="리크루터 활동현황 보기"
              />
            </b>
          </h3>
        
          <ExcelDownload headers={headers} dataset={listState} fileName="리크루터 활동현황 보기"/>

        </div>
        <div className="searchBar">
          <label htmlFor="search">Search : </label>
          <input type="search" id="sConts"  onKeyUp={(e) => {onKeyUpSrch(e);}}/>
            <button type="button" onClick={initSetHandle}>
              <FormattedMessage
                id="search"
                defaultMessage="검색"
              />
            </button>
        </div>
        <article>
          <table>
            <thead>
              <tr>
                <th>
                  <span>
                    <FormattedMessage
                      id="number"
                      defaultMessage="번호"
                    />
                  </span>
                </th>
                <th className={ fltrState.bizNm == "DESC" ? "descending" :
                                fltrState.bizNm == "ASC" ? "ascending" :
                                undefined } 
                                onClick={() => {
                                  changeFltr("bizNm", fltrState.bizNm);                   
                                }}
                  >
                  <div className='wrap'>
                    <span>
                      <FormattedMessage
                        id="company.name"
                        defaultMessage="기업명"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th className={ fltrState.title == "DESC" ? "descending" :
                                fltrState.title == "ASC" ? "ascending" :
                                undefined } 
                                onClick={() => {
                                  changeFltr("title", fltrState.title);                   
                                }}
                >
                  <div className='wrap'>
                    <span>
                      <FormattedMessage
                        id="recruitment.title"
                        defaultMessage="채용공고 제목"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th className={ fltrState.confirmDttm == "DESC" ? "descending" :
                                fltrState.confirmDttm == "ASC" ? "ascending" :
                                undefined } 
                                onClick={() => {
                                  changeFltr("confirmDttm", fltrState.confirmDttm);                   
                                }}
                  >
                  <div className='wrap'>
                    <span>
                      <FormattedMessage
                        id="inspection.date"
                        defaultMessage="검수일시"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th className={ fltrState.crecChooseDttm == "DESC" ? "descending" :
                                fltrState.crecChooseDttm == "ASC" ? "ascending" :
                                undefined } 
                                onClick={() => {
                                  changeFltr("crecChooseDttm", fltrState.crecChooseDttm);                   
                                }}
                  >
                  <div className='wrap'>
                      <span>
                        <FormattedMessage
                          id="contract.date"
                          defaultMessage="선택(계약)일시"
                        />
                      </span>
                      <div className="filterArrow">
                        <span />
                        <span />
                      </div>
                  </div>
                </th>
                <th className={ fltrState.crecMbrNm == "DESC" ? "descending" :
                                fltrState.crecMbrNm == "ASC" ? "ascending" :
                                undefined } 
                                onClick={() => {
                                  changeFltr("crecMbrNm", fltrState.crecMbrNm);                   
                                }}
                >
                  <div className='wrap'>
                    <span>
                      <FormattedMessage
                        id="contract.r"
                        defaultMessage="계약 R"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th className={ fltrState.recMbrCelpNum == "DESC" ? "descending" :
                                fltrState.recMbrCelpNum == "ASC" ? "ascending" :
                                undefined } 
                                onClick={() => {
                                  changeFltr("recMbrCelpNum", fltrState.recMbrCelpNum);                   
                                }}
                    >
                  <div className='wrap'>
                    <span>
                      <FormattedMessage
                        id="telephone.number"
                        defaultMessage="전화번호"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th className={ fltrState.rcmmndCnt == "DESC" ? "descending" :
                                fltrState.rcmmndCnt == "ASC" ? "ascending" :
                                undefined } 
                                onClick={() => {
                                  changeFltr("rcmmndCnt", fltrState.rcmmndCnt);                   
                                }}
                >
                  <div className='wrap'>
                    <span>
                      <FormattedMessage
                        id="candidate.number"
                        defaultMessage="후보자수"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th className={ fltrState.rcmmndStat == "DESC" ? "descending" :
                                fltrState.rcmmndStat == "ASC" ? "ascending" :
                                undefined } 
                                onClick={() => {
                                  changeFltr("rcmmndStat", fltrState.rcmmndStat);                   
                                }}
                >
                  <div className='wrap'>
                    <span>
                      <FormattedMessage
                        id="progress"
                        defaultMessage="진행상황"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th className={ fltrState.hireDayCnt == "DESC" ? "descending" :
                                fltrState.hireDayCnt == "ASC" ? "ascending" :
                                undefined } 
                                onClick={() => {
                                  changeFltr("hireDayCnt", fltrState.hireDayCnt);                   
                                }}
                >
                  <div className='wrap'>
                    <span>
                      <FormattedMessage
                        id="day"
                        defaultMessage="시간(일)"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
              </tr>
            </thead>
            <tbody>
              <tr className="noData" style={{display: 'none'}}>
                <th colSpan={9}>No data available in table</th>
              </tr>
              {currentPosts(listState).map((list, idx)=>
                  (
                    <tr>
                      <td>{indexOfFirst + idx + 1}</td>
                      <td>{list.bizNm}</td>
                      <td>{list.title}</td>
                      <td>{commonUtil.substr(list.confirmDttm,0,19)}</td>
                      <td>
                          <div
                            style={{
                              padding: "1px",
                              border: "0px solid #D7D7D7",
                            }}
                            dangerouslySetInnerHTML={{ __html: commonUtil.replace(list.crecChooseDttm, '<;:>', '<br>')  }}
                          ></div>
                     
                      </td>
                      <td>
                        <div
                            style={{
                              padding: "1px",
                              border: "0px solid #D7D7D7",
                            }}
                            dangerouslySetInnerHTML={{ __html: commonUtil.replace(list.crecMbrNm, '<;:>', '<br>')  }}
                          ></div>
                      </td>
                      <td>
                          <div
                            style={{
                              padding: "1px",
                              border: "0px solid #D7D7D7",
                            }}
                            dangerouslySetInnerHTML={{ __html: commonUtil.replace(list.crecCelpNum, '<;:>', '<br>')  }}
                          ></div>
                      </td>
                      <td>{list.rcmmndCnt}</td>
                      <td className={
                          (list.cntrcDttm === "" )?('red'):('blue')
                        }>
                        {list.rcmmndStat}
                      </td>
                      <td>
                        {list.hireTmCnt}
                      </td>
                    </tr>
                  )
                )
              }
              
            </tbody>
          </table>
        </article>
        
        <Pagination postsPerPage={postsPerPage} totalPosts={listState.length} paginate={setCurrentPage} currentPage={currentPage}/>
      </section>
    </main>
  );
}