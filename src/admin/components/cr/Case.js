import { ExcelDownload } from 'commons/components/download-btn/ExcelDownload';
import {Link, useLocation, useNavigate} from 'react-router-dom';
import { useContext, useEffect, useRef, useState } from 'react';
import { Context } from 'contexts';
import axios from 'axios';
import fileDownload from 'js-file-download';

import { axiosCrudOnSubmit } from 'commons/modules/commonUtil';
import * as commonUtil from 'commons/modules/commonUtil';
import { Pagination } from 'commons/components/pagination/Pagination';
import { DownloadBtns } from 'commons/components/download-btn/DownloadBtns';
import { snglFileUpload } from 'commons/modules/multipartUtil';
import * as defVal  from 'commons/modules/defVal';
import { AddressInput } from 'commons/components/AddressInput';
import { ImagePreview } from 'commons/components/inputs/image-preview/ImagePreview';
import { UPD_MBRUID } from 'contexts/actionTypes';
import { FormattedMessage } from 'react-intl';

export const Case =()=>{

  let PROC_CHK = "";

  const {
    state : {
        mbr,
        server
    },
    dispatch,
  } = useContext(Context);


  const [listState, setListState] = useState([]);
  const [sConts, setSConts] = useState("");
  const [fltrState, setFltrState] = useState({});
  const componentRef = useRef();
  const moment = require('moment');

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid")
    ,ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi")
  };

  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(10);

  const indexOfLast = currentPage * postsPerPage;
  const indexOfFirst = indexOfLast - postsPerPage;

  let arraySimb = "<:;>";

  
  
  useEffect(async () => {
    console.log("Page First Load EFFECT");
    initSetHandle();      
    
  },[fltrState])
  
  async function initSetHandle() {
    let goUrl = server.path + "/sttcs/recpsblstatelist";
    let data = {
      ssnMbrUid : ssn.ssnMbrUid,
      ssnMbrDivi : ssn.ssnMbrDivi,
      mbrUid : mbr.mbrUid,
      mbrDivi : "MBRDIVI-01",
      sConts : (document.getElementById("sConts"))?(document.getElementById("sConts").value):(''),
      sFltr : fltrState,
      sFltrCate : Object.keys(fltrState)[0],
      sFltrCont : Object.values(fltrState)[0],
    }

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    let resArr = axiosRes.data.resArr;
    for(var k=0; k<resArr.length; k++)
    {
      resArr[k].joinDttm = commonUtil.substr(resArr[k].joinDttm,0,19)
      resArr[k].wthdrDttm = commonUtil.substr(resArr[k].wthdrDttm,0,19)
      
      resArr[k].rmnCnt = parseInt(resArr[k].rmnCnt)
    }
    
    setListState(resArr);      
  }

  useEffect( async () => {
    window.scrollTo(0,0);
  },[listState])

  /////////////////////////////////////////////////////////////

  function currentPosts(tmp) {
    let currentPosts = 0;
    currentPosts = tmp.slice(indexOfFirst, indexOfLast);
    return currentPosts;
  }


  async function changeFltr(key, fltr){
    
    let tmpFltr = (fltr === "ASC") ? "DESC" :
                  (fltr === "DESC") ? "ASC" :
                  "ASC";
    
    setFltrState({[key]:tmpFltr});

    initSetHandle();
  }

  const onKeyUpSrch =(e) => {
    if(e.key == "Enter")initSetHandle();   
    return false;
  }

  ///////////////////////////////////////////////////////////

  const headers = [
    { label: "아이디", key: "mbrId" },
    { label: "이름", key: "mbrNm" },
    { label: "가입일", key: "joinDttm" },
    { label: "탈퇴일", key: "wthdrDttm" },
    { label: "현재 지행 중 건수", key: "cmmntCnt" },
    { label: "최대 진행가능 건수", key: "maxHire" },
    { label: "잔여 진행가능 건수", key: "rmnCnt" },
    { label: "기본 추천가능 건수", key: "maxCnddt" }
  ];


  return (  
    <main>
      <section className="tableSection">
        <div className="sectionHeader">
          <h3>
            <span className="material-icons-outlined">
              settings
            </span>
            <b>
              <FormattedMessage
                id="recruiter.case.manage"
                defaultMessage="리크루터별 진행가능 건수 관리"
              />
            </b>
          </h3>
        
          <ExcelDownload headers={headers} dataset={listState} fileName="리크루터별 진행가능 건수 관리"/>

        </div>
        <div className="searchBar">
        <label htmlFor="search">Search : </label>
          <input type="search" id="sConts"  onKeyUp={(e) => {onKeyUpSrch(e);}}/>
            <button type="button" onClick={initSetHandle}>
              <FormattedMessage
                id="search"
                defaultMessage="검색"
              />
            </button>
        </div>
        <article>
          <table>
            <thead>
              <tr>
                <th>
                  <span>
                    <FormattedMessage
                      id="number"
                      defaultMessage="번호"
                    />
                  </span>
                  
                </th>
                <th className={ fltrState.mbrId === "DESC" ? "descending" :
                                fltrState.mbrId === "ASC" ? "ascending" :
                                undefined } 
                                onClick={() => {
                                  changeFltr("mbrId", fltrState.mbrId);                   
                                }}
                >
                  <div className='wrap'>
                    <span>
                      <FormattedMessage
                        id="id"
                        defaultMessage="아이디"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th className={ fltrState.mbrNm === "DESC" ? "descending" :
                                fltrState.mbrNm === "ASC" ? "ascending" :
                                undefined } 
                                onClick={() => {
                                  changeFltr("mbrNm", fltrState.mbrNm);                   
                                }}
                >
                  <div className='wrap'>
                    <span>
                      <FormattedMessage
                        id="name"
                        defaultMessage="이름"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th className={ fltrState.joinDttm === "DESC" ? "descending" :
                                fltrState.joinDttm === "ASC" ? "ascending" :
                                undefined } 
                                onClick={() => {
                                  changeFltr("joinDttm", fltrState.joinDttm);                   
                                }}
                >
                  <div classNamewrap>
                    <span>
                      <FormattedMessage
                        id="join.date"
                        defaultMessage="가입일"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th className={ fltrState.wthdrDttm === "DESC" ? "descending" :
                                fltrState.wthdrDttm === "ASC" ? "ascending" :
                                undefined } 
                                onClick={() => {
                                  changeFltr("wthdrDttm", fltrState.wthdrDttm);                   
                                }}
                >
                  <div className='wrap'>
                    <span>
                      <FormattedMessage
                        id="leave.date"
                        defaultMessage="탈퇴일"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th className={ fltrState.cmmntCnt === "DESC" ? "descending" :
                                fltrState.cmmntCnt === "ASC" ? "ascending" :
                                undefined } 
                                onClick={() => {
                                  changeFltr("cmmntCnt", fltrState.cmmntCnt);                   
                                }}
                >
                  <div className='wrap'>
                    <span>
                      <FormattedMessage
                        id="ing.case"
                        defaultMessage="현재 진행 중 건수"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th className={ fltrState.maxHire === "DESC" ? "descending" :
                                fltrState.maxHire === "ASC" ? "ascending" :
                                undefined } 
                                onClick={() => {
                                  changeFltr("maxHire", fltrState.maxHire);                   
                                }}
                >
                  <div className='wrap'>
                    <span>
                      <FormattedMessage
                        id="maximum.case"
                        defaultMessage="최대 진행가능 건수"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th className={ fltrState.rmnCnt === "DESC" ? "descending" :
                                fltrState.rmnCnt === "ASC" ? "ascending" :
                                undefined } 
                                onClick={() => {
                                  changeFltr("rmnCnt", fltrState.rmnCnt);                   
                                }}
                >
                  <div className='wrap'>
                    <span>
                      <FormattedMessage
                        id="remain.case"
                        defaultMessage="잔여 진행가능 건수"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th  className={ fltrState.maxCnddt === "DESC" ? "descending" :
                                fltrState.maxCnddt === "ASC" ? "ascending" :
                                undefined } 
                                onClick={() => {
                                  changeFltr("maxCnddt", fltrState.maxCnddt);                   
                                }}
                >
                  <div className='wrap'>
                    <span>
                      <FormattedMessage
                        id="recommendable.case"
                        defaultMessage="기본 추천가능 건수"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th>
                  <span>
                    <FormattedMessage
                      id="etc"
                      defaultMessage="기타"
                    />
                  </span>
                </th>
              </tr>
            </thead>
            <tbody>
              <tr className="noData" style={{display: 'none'}}>
                <th colSpan={9}>No data available in table</th>
              </tr>
              {currentPosts(listState).map((list, idx)=>
                  (
                    <tr>
                      <td>{indexOfFirst + idx + 1}</td>
                      <td>{list.mbrId}</td>
                      <td>{list.mbrNm}</td>
                      <td>{commonUtil.substr(list.joinDttm,0,19)}</td>
                      <td>{commonUtil.substr(list.wthdrDttm,0,19)}</td>
                      <td>{list.cmmntCnt}</td>
                      <td>{list.maxHire}</td>
                      <td>{list.rmnCnt}</td>
                      <td>{list.maxCnddt}</td>
                      <td>
                        <Link to='/admin/recruiter-detail' className='buttonLink cyan' 
                          state={{ mbrUid: list.mbrUid, backUrl: '/admin/case' }}>변경하기</Link>
                      </td>
                    </tr>
                  )
                )
              }
            </tbody>
          </table>
        </article>

        <Pagination postsPerPage={postsPerPage} totalPosts={listState.length} paginate={setCurrentPage} currentPage={currentPage}/>
      </section>
    </main>
  );
}