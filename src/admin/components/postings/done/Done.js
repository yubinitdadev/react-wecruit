import { DownloadBtns } from 'commons/components/download-btn/DownloadBtns';
import { Pagination } from 'commons/components/pagination/Pagination';
import { axiosCrudOnSubmit } from 'commons/modules/commonUtil';
import { Context } from 'contexts';
import { UPD_MBRUID } from 'contexts/actionTypes';
import { useContext, useEffect, useRef, useState } from 'react';
import {Link} from 'react-router-dom';
import Modal from 'modal-all/ModalAll';
import { ManageCr } from 'modal-all/modals/ManageCr';
import { FormattedMessage } from 'react-intl';

export const Done =()=>{
const [buttonOn, setButtonOn] = useState(false);
const [modalVisible1, setModalVisible1] = useState(false)

const openModal1 = () => {
  setModalVisible1(true)
}

const closeModal1 = () => {
  setModalVisible1(false)
}


const {
  state : {
    server,
    mbr
  },
  dispatch,
} = useContext(Context);

const moment = require('moment');
const componentRef = useRef();

const [confirms, setConfirms] = useState([]);
const [sConts, setSConts] = useState("");
const [fltrState, setFltrState] = useState({});

const [currentPage, setCurrentPage] = useState(1);
const [postsPerPage, setPostsPerPage] = useState(10);

const indexOfLast = currentPage * postsPerPage;
const indexOfFirst = indexOfLast - postsPerPage;

function currentPosts(tmp) {
  let currentPosts = 0;
  currentPosts = tmp.slice(indexOfFirst, indexOfLast);
  return currentPosts;
}

async function goSrch() {
  let goUrl = server.path + "/hire/list"
  let data = {
    ssnMbrUid : mbr.mbrUid,
    mbrUid : mbr.mbrUid,
    mbrDivi : "MBRDIVI-01",
    hireStat : "end",
    sConts : sConts,
    sFltr : fltrState,
    sFltrCate : Object.keys(fltrState)[0],
    sFltrCont : Object.values(fltrState)[0],
  }

  initSetHandle();
}

const changeFltr = (fltr) => {
  let tmpFltr = (fltr === "ASC") ? "DESC" :
                (fltr === "DESC") ? "ASC" :
                "ASC";

  return tmpFltr;
}

const initSetHandle = async () => {
  let goUrl = server.path + "/hire/hirelist"

  let data = {
    hireStat : "end",
    sConts : sConts,
    sFltr : fltrState,
    sFltrCate : Object.keys(fltrState)[0],
    sFltrCont : Object.values(fltrState)[0],
  }
  
  let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

  let axiosResData = axiosRes.data.hireArr;
  let tmpAxiosResData = [];

  axiosResData.forEach((resData, idx)=>{
    
    resData["rgstDttm"] = moment(resData.rgstDttm).format("YYYY년 MM월 DD일 HH시 mm분");
    resData["cnfrmDttm"] = moment(resData.cnfrmDttm).format("YYYY년 MM월 DD일 HH시 mm분");
    
    if( resData["rcmndUntilDon"] == "Y" )resData["rcmndEndDttm"] = "채용시까지";
    else resData["rcmndEndDttm"] = moment(resData.rcmndEndDttm).format("YYYY년 MM월 DD일 HH시");

    tmpAxiosResData.push(resData);
  })

  setConfirms(tmpAxiosResData);
}

useEffect( () => {
  goSrch();
}, [fltrState]);

useEffect(async() => {
  window.scrollTo(0,0);
  initSetHandle();
}, [])

const headers = [
  { label: "채용등록일", key: "rgstDttm" },
  { label: "검수완료일", key: "cnfrmDttm" },
  { label: "채용마감일", key: "rcmndEndDttm" },
  { label: "기업명", key: "bizNm" },
  { label: "담당자명", key: "mbrNm" },
  { label: "채용공고 제목", key: "title" },
  { label: "제안R", key: "cmmntChooseCnt" },
  { label: "계약R", key: "recmmndChooseCnt" },
  { label: "전체R", key: "cmmntCnt" },
  { label: "후보자수", key: "recmmndCnt" },
  
];

const onKeyUpSrch =(e) => {
  if(e.key == "Enter")
  {
    setCurrentPage(1);
    initSetHandle();   
  }
  return false;
}

const [buttonList, setButtonList] = useState(-1);
const [preMenuIdx, setPreMenuIdx] = useState(-1);

const handleButtonClick = (idx) => {

  if( preMenuIdx == idx )
  {
    setPreMenuIdx(-1);
    setButtonList(-1);
  }
  else
  {  
    setPreMenuIdx(idx);
    setButtonList(idx);
  }
};

  return (  
    <main>
        <section className="tableSection">
          <div className="sectionHeader">
            <h3>
              <span className="material-icons-outlined">
                settings
              </span>
              <b>
                <FormattedMessage
                  id="posting.done"
                  defaultMessage="종료된 채용공고"
                />
              </b>
            </h3>
            <DownloadBtns headers={headers} dataset={confirms} fileName="종료된 채용공고" componentRef={componentRef}/>
          </div>

          <div className="searchBar">
            <label htmlFor="search">Search:</label>
            <input type="search" id="search" 
              onChange={(e) => {
                setSConts(e.target.value);
              }}
              
              onKeyUp={(e) => {onKeyUpSrch(e);}}
              />
            <button type="button" onClick={initSetHandle}>
              <FormattedMessage
                id="search"
                defaultMessage="검색"
              />
            </button>
          </div>

          <article>
            <table>
              <thead>
                <tr>
                  <th>
                    <span>
                      <FormattedMessage
                        id="number"
                        defaultMessage="번호"
                      />
                    </span>                    
                  </th>
                  
                  <th className={ fltrState.rgstDttm === "DESC" ? "descending" :
                                fltrState.rgstDttm === "ASC" ? "ascending" :
                                undefined } 
                    onClick={() => {
                      setFltrState({
                        rgstDttm : changeFltr(fltrState.rgstDttm)
                      });
                    }}>
                    <div className='wrap'>
                      <span>
                        <FormattedMessage
                          id="recruitment.registration.date"
                          defaultMessage="채용등록일"
                        />
                      </span>
                      <div className="filterArrow">
                        <span />
                        <span />
                      </div>
                    </div>
                  </th>
                  <th className={ fltrState.cnfrmDttm === "DESC" ? "descending" :
                                fltrState.cnfrmDttm === "ASC" ? "ascending" :
                                undefined } 
                    onClick={() => {
                      setFltrState({
                        cnfrmDttm : changeFltr(fltrState.cnfrmDttm)
                      });
                    }}>
                    <div className='wrap'>
                      <span>
                        <FormattedMessage
                          id="inspection.complete.date"
                          defaultMessage="검수 완료일"
                        />
                      </span>
                      <div className="filterArrow">
                        <span />
                        <span />
                      </div>
                    </div>
                  </th>
                  <th className={ fltrState.rcmndEndDttm === "DESC" ? "descending" :
                                fltrState.rcmndEndDttm === "ASC" ? "ascending" :
                                undefined } 
                    onClick={() => {
                      setFltrState({
                        rcmndEndDttm : changeFltr(fltrState.rcmndEndDttm)
                      });
                    }}>
                    <div className='wrap'>
                      <span>
                        <FormattedMessage
                          id="recruit.deadline"
                          defaultMessage="채용 마감일"
                        />
                      </span>
                      <div className="filterArrow">
                        <span />
                        <span />
                      </div>
                    </div>
                  </th>
                  <th className={ fltrState.bizNm === "DESC" ? "descending" :
                                fltrState.bizNm === "ASC" ? "ascending" :
                                undefined } 
                    onClick={() => {
                      setFltrState({
                        bizNm : changeFltr(fltrState.bizNm)
                      });
                    }}>
                    <div className='wrap'>
                      <span>
                        <FormattedMessage
                          id="company.name"
                          defaultMessage="기업명"
                        />
                      </span>
                      <div className="filterArrow">
                        <span />
                        <span />
                      </div>
                    </div>
                  </th>
                  <th className={ fltrState.mbrNm === "DESC" ? "descending" :
                                fltrState.mbrNm === "ASC" ? "ascending" :
                                undefined } 
                    onClick={() => {
                      setFltrState({
                        mbrNm : changeFltr(fltrState.mbrNm)
                      });
                    }}>
                    <div className='wrap'>
                      <span>
                        <FormattedMessage
                          id="manager.name"
                          defaultMessage="담당자 이름"
                        />
                      </span>
                      <div className="filterArrow">
                        <span />
                        <span />
                      </div>
                    </div>
                  </th>
                  <th className={ fltrState.title === "DESC" ? "descending" :
                                fltrState.title === "ASC" ? "ascending" :
                                undefined } 
                    onClick={() => {
                      setFltrState({
                        title : changeFltr(fltrState.title)
                      });
                    }}>
                    <div className='wrap'>
                      <span>
                        <FormattedMessage
                          id="recruitment.title"
                          defaultMessage="채용공고 제목"
                        />
                      </span>
                      <div className="filterArrow">
                        <span />
                        <span />
                      </div>
                    </div>
                  </th>
                  <th className={ fltrState.cmmntChooseCnt === "DESC" ? "descending" :
                                fltrState.cmmntChooseCnt === "ASC" ? "ascending" :
                                undefined } 
                    onClick={() => {
                      setFltrState({
                        cmmntChooseCnt : changeFltr(fltrState.cmmntChooseCnt)
                      });
                    }}>
                    <div className='wrap'>
                      <span>
                        <FormattedMessage
                          id="suggestion.r"
                          defaultMessage="제안 R"
                        />
                      </span>
                      <div className="filterArrow">
                        <span />
                        <span />
                      </div>
                    </div>
                  </th>

                  <th className={ fltrState.recmmndChooseCnt === "DESC" ? "descending" :
                                fltrState.recmmndChooseCnt === "ASC" ? "ascending" :
                                undefined } 
                    onClick={() => {
                      setFltrState({
                        recmmndChooseCnt : changeFltr(fltrState.recmmndChooseCnt)
                      });
                    }}>
                    <div className='wrap'>
                      <span>
                        <FormattedMessage
                          id="contract.r"
                          defaultMessage="계약 R"
                        />
                      </span>
                      <div className="filterArrow">
                        <span />
                        <span />
                      </div>
                    </div>
                  </th>
                  <th className={ fltrState.cmmntCnt === "DESC" ? "descending" :
                                fltrState.cmmntCnt === "ASC" ? "ascending" :
                                undefined } 
                    onClick={() => {
                      setFltrState({
                        cmmntCnt : changeFltr(fltrState.cmmntCnt)
                      });
                    }}>
                    <div className='wrap'>
                      <span>
                        <FormattedMessage
                          id="all.r"
                          defaultMessage="전체 R"
                        />
                      </span>
                      <div className="filterArrow">
                        <span />
                        <span />
                      </div>
                    </div>
                  </th>

                  <th className={ fltrState.recmmndCnt === "DESC" ? "descending" :
                                fltrState.recmmndCnt === "ASC" ? "ascending" :
                                undefined } 
                    onClick={() => {
                      setFltrState({
                        recmmndCnt : changeFltr(fltrState.recmmndCnt)
                      });
                    }}>
                    <div className='wrap'>
                      <span>
                        <FormattedMessage
                          id="candidate.number"
                          defaultMessage="후보자수"
                        />
                      </span>
                      <div className="filterArrow">
                        <span />
                        <span />
                      </div>
                    </div>
                  </th>
                  <th>
                    <span>
                      <FormattedMessage
                        id="etc"
                        defaultMessage="기타"
                      />
                    </span>
                  </th>
                </tr>
              </thead>
              <tbody>
                
                <tr className="noData" style={{display: 'none'}}>
                  <th colSpan={9}>No data available in table</th>
                </tr>
                {currentPosts(confirms).map((list, idx)=>(
                    <tr key={list.hireUid}>
                      <td>{indexOfFirst + idx + 1}</td>
                      <td>{list.rgstDttm}</td>
                      <td>{list.cnfrmDttm}</td>
                      <td>{list.rcmndEndDttm}</td>
                      <td>{list.bizNm}</td>
                      <td>{list.mbrNm}</td>
                      <td>
                        <Link to='/admin/done-detail' state={{ hireUid: list.hireUid, mbrUid: list.mbrUid }}>{list.title}</Link>
                      </td>
                      <td>{list.cmmntChooseCnt}</td>
                      <td>{list.recmmndChooseCnt}</td>
                      <td>{list.cmmntCnt}</td>
                      <td>{list.recmmndCnt}</td>
                      <td className="additionalFunction">
                        <button id={"openMenuBtn" + idx} 
                        onClick={ () => {handleButtonClick(idx);}}>
                          <FormattedMessage
                            id="additional.function"
                            defaultMessage="추가기능"
                          />
                          <span className="material-icons">
                            keyboard_arrow_down
                          </span>
                        </button>
                        <ul key={idx+1} id={"menuBtn" + idx}  
                            className = {(buttonList === idx ) ? "active" : ""}>
                          <li>
                            <Link to='/admin/recruiter-manage' state={{ hireUid: list.hireUid }}>
                              <span className="material-icons">
                                content_copy
                              </span>
                              <FormattedMessage
                                id="recruiter.manage"
                                defaultMessage="리크루터 관리"
                              />
                            </Link>
                          </li>
                          <li>
                            <Link to='/admin/candidate-manage' state={{ hireUid: list.hireUid }}>
                              <span className="material-icons-outlined">
                                local_offer
                              </span>
                              <FormattedMessage
                                id="candidate.manage"
                                defaultMessage="후보자 관리"
                              />
                            </Link>
                          </li>
                          <li onClick={openModal1}>
                            <span className="material-icons-outlined">
                              local_offer
                            </span>
                            <FormattedMessage
                              id="cr.manage"
                              defaultMessage="CR 관리"
                            />
                          </li>
                        </ul>
                        {
                          <Modal visible={modalVisible1} closable={true} maskClosable={true} onClose={closeModal1} nobutton={true} >
                            <ManageCr />
                          </Modal>
                        } 
                      </td>
                    </tr>
                )
                )}
              </tbody>
            </table>
          </article>

          <Pagination postsPerPage={postsPerPage} totalPosts={confirms.length} paginate={setCurrentPage} currentPage={currentPage}/>
        </section>
      </main>
  );
}