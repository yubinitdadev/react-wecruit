import {Link, useLocation, useNavigate} from 'react-router-dom';
import { useContext, useEffect, useRef, useState } from 'react';
import { Context } from 'contexts';
import { ToastContainer } from "react-toastify";
import axios from 'axios';
import fileDownload from 'js-file-download';

import { axiosCrudOnSubmit } from 'commons/modules/commonUtil';
import { Pagination } from 'commons/components/pagination/Pagination';
import { DownloadBtns } from 'commons/components/download-btn/DownloadBtns';
import { snglFileUpload } from 'commons/modules/multipartUtil';
import * as defVal  from 'commons/modules/defVal';
import { AddressInput } from 'commons/components/AddressInput';
import { ImagePreview } from 'commons/components/inputs/image-preview/ImagePreview';
import { UPD_MBRUID } from 'contexts/actionTypes';
import { FormattedMessage } from 'react-intl';

export const Temporary =()=>{
  
  const [listState, setListState] = useState([]);
  const [sConts, setSConts] = useState("");
  const [fltrState, setFltrState] = useState({});
  const componentRef = useRef();
  const moment = require('moment');

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid")
    ,ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi")
  };
  
  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(10);

  const indexOfLast = currentPage * postsPerPage;
  const indexOfFirst = indexOfLast - postsPerPage;

  let arraySimb = "<:;>";

  const {
    state : {
        mbr,
        server
    },
    dispatch,
  } = useContext(Context);

  function currentPosts(tmp) {
    let currentPosts = 0;
    currentPosts = tmp.slice(indexOfFirst, indexOfLast);
    return currentPosts;
}

const changeFltr = (fltr) => {
  let tmpFltr = (fltr === "ASC") ? "DESC" :
                (fltr === "DESC") ? "ASC" :
                "ASC";

  return tmpFltr;
}

 
  useEffect( () => {
    initSetHandle();
  }, [fltrState]);

  useEffect(async () => {
    window.scrollTo(0,0);
    initSetHandle();
  }, [])

  const initSetHandle = async () => {
    let goUrl = server.path + "/hire/hirelist"
    
    let data = {
      hireStat : "tmp",
      sConts : sConts,
      sFltr : fltrState,
      sFltrCate : Object.keys(fltrState)[0],
      sFltrCont : Object.values(fltrState)[0],
    }

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    let axiosResData = axiosRes.data.hireArr;
    let tmpAxiosResData = [];

    axiosResData.forEach((resData, idx)=>{
      resData["rcmndEndDttm"] = moment(resData.rcmndEndDttm).format("YYYY년 MM월 DD일 HH시");

      tmpAxiosResData.push(resData);
    })

    setListState(tmpAxiosResData);
  }

  const onKeyUpSrch =(e) => {
    if(e.key == "Enter")
    {
      setCurrentPage(1);
      initSetHandle();   
    }
    return false;
  }

  return (  
    <main>
        <ToastContainer />
        <section className="tableSection">
          <div className="sectionHeader">
            <h3>
              <span className="material-icons-outlined">
                settings
              </span>
              <b>
                <FormattedMessage
                  id="posting.temp.storage"
                  defaultMessage="임시저장 채용공고"
                />
              </b>
            </h3></div>
          <div className="searchBar">
            <label htmlFor="search">Search:</label>
            <input type="search" id="search" 
              onChange={(e) => {
                setSConts(e.target.value);
              }}
              
              onKeyUp={(e) => {onKeyUpSrch(e);}}
              />
            <button type="button" onClick={initSetHandle}>
              <FormattedMessage
                id="search"
                defaultMessage="검색"
              />
            </button>
          </div>
          <article>
            <table>
              <thead>
                <tr>
                  <th>
                    <span>
                      <FormattedMessage
                        id="number"
                        defaultMessage="번호"
                      />
                    </span>
                  </th>
                  <th className={ fltrState.mbrId === "DESC" ? "descending" :
                                fltrState.mbrId === "ASC" ? "ascending" :
                                undefined } 
                    onClick={() => {
                      setFltrState({
                        mbrId : changeFltr(fltrState.mbrId)
                      });
                    }}>
                    <span>
                      <FormattedMessage
                        id="id"
                        defaultMessage="아이디"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </th>
                  <th className={ fltrState.bizNm === "DESC" ? "descending" :
                                fltrState.bizNm === "ASC" ? "ascending" :
                                undefined } 
                    onClick={() => {
                      setFltrState({
                        bizNm : changeFltr(fltrState.bizNm)
                      });
                    }}>
                    <span>
                      <FormattedMessage
                        id="company.name"
                        defaultMessage="기업명"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </th>
                  <th className={ fltrState.mbrNm === "DESC" ? "descending" :
                                fltrState.mbrNm === "ASC" ? "ascending" :
                                undefined } 
                    onClick={() => {
                      setFltrState({
                        mbrNm : changeFltr(fltrState.mbrNm)
                      });
                    }}>
                    <span>
                      <FormattedMessage
                        id="manager.name"
                        defaultMessage="담당자 이름"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </th>
                  <th className={ fltrState.title === "DESC" ? "descending" :
                                fltrState.title === "ASC" ? "ascending" :
                                undefined } 
                    onClick={() => {
                      setFltrState({
                        title : changeFltr(fltrState.title)
                      });
                    }}>
                    <span>
                      <FormattedMessage
                        id="recruitment.title"
                        defaultMessage="채용공고 제목"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </th>
                  <th className={ fltrState.rgstDttm === "DESC" ? "descending" :
                                fltrState.rgstDttm === "ASC" ? "ascending" :
                                undefined } 
                    onClick={() => {
                      setFltrState({
                        rgstDttm : changeFltr(fltrState.rgstDttm)
                      });
                    }}>
                    <span>
                      <FormattedMessage
                        id="temp.save.date"
                        defaultMessage="임시저장 일시"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody>
                {currentPosts(listState).map((list, idx)=>(
                  <tr key={list.hireUid}>
                    <td>{indexOfFirst + idx + 1}</td>
                    <td>{list.mbrId}</td>
                    <td>{list.bizNm}</td>
                    <td>{list.mbrNm}</td>
                    <td>
                      <Link to='/admin/temp-manage' state={{ hireUid: list.hireUid }}>{list.title}</Link>
                    </td>
                    <td>{moment(list.rgstDttm).format("YYYY년 MM월 DD일 HH시 mm분")}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </article>
          
          <Pagination postsPerPage={postsPerPage} totalPosts={listState.length} paginate={setCurrentPage} currentPage={currentPage}/>

        </section>
      </main>
  );
}