import { Link, useLocation, useNavigate } from "react-router-dom";
import { useContext, useEffect, useState } from "react";
import { Context } from "contexts";
import axios from "axios";
import fileDownload from "js-file-download";
import Modal from "modal-all/ModalAll";

import { axiosCrudOnSubmit } from "commons/modules/commonUtil";
import * as commonUtil from "commons/modules/commonUtil";
import { snglFileUpload } from "commons/modules/multipartUtil";
import * as defVal from "commons/modules/defVal";
import { AddressInput } from "commons/components/AddressInput";
import { ImagePreview } from "commons/components/inputs/image-preview/ImagePreview";
import { TagTemplate } from "commons/components/tags/TagTemplate";
import BusinessTypeTemplate from "commons/components/inputs/business-type/BusinessTypeTemplate";
import WorkPlaceTemplate from "commons/components/inputs/work-place/WorkPlaceTemplate";
import WorkPositionTemplate from "commons/components/inputs/work-position/WorkPositionTemplate";
import { UPD_MBRUID } from "contexts/actionTypes";
import { FormattedMessage, useIntl } from "react-intl";

export const TempDetail = () => {
  const intl = useIntl();

  let PROC_CHK = "";

  const {
    state: { server, mbr },
    dispatch,
  } = useContext(Context);

  const location = useLocation();
  const navigate = useNavigate();
  const moment = require("moment");
  let previewWindow = "";
  let previewImg = document.createElement("img");

  const [files, setFiles] = useState("");
  const [axiosFilesRes, setAxiosFilesRes] = useState("");

  const [mbrFilesRes, setMbrFilesRes] = useState([]);
  const [bizFilesRes, setBizFilesRes] = useState([]);
  const [extraFilesRes, setExtraFilesRes] = useState([]);

  const bizKndCd = defVal.BizKndCd();
  const salesSizeCd = defVal.SalesSizeCd();
  const empSizeCd = defVal.EmpSizeCd();
  const hireSizeCd = defVal.HireSizeCd();
  const bizGuidDivi = defVal.BizGuidDivi();
  const bizGuidKnd = defVal.BizGuidKnd();

  const [bizCate, setBizCate] = useState([]);
  const [empAddrCate, setEmpAddrCate] = useState([]);
  const [workPosCate, setWorkPosCate] = useState([]);
  const [bizGuidListState, setBizGuidListState] = useState([]);
  const [guid01Cnt, setGuid01Cnt] = useState(0);
  const [guid02Cnt, setGuid02Cnt] = useState(0);
  const [detailState, setDetailState] = useState(defVal.setAxiosJobState(null));

  const [modalVisible1, setModalVisible1] = useState(false);
  const [titleLength, setTitleLength] = useState("0");
  const [untilDone, setUntilDone] = useState(false);
  const [tagCnt, setTagCnt] = useState(0);

  const [bizCateCdCombo, setBizCateCdCombo] = useState([
    defVal.SelCateCdCombo(),
  ]);
  const [empAddrCateCdCombo, setEmpAddrCateCdCombo] = useState([
    defVal.SelCateCdCombo(),
  ]);
  const [workPosCateCdCombo, setWorkPosCateCdCombo] = useState([
    defVal.SelCateCdCombo(),
  ]);

  let atchFiles;

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid"),
    ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi"),
  };

  let locationState = {
    hireUid: location.state.hireUid,
  };

  useEffect(async () => {
    console.log("Page First Load EFFECT");
    initSetHandle();
  }, []);

  async function initSetHandle() {
    detailState.ssnMbrUid = ssn.ssnMbrUid;

    let goUrl = server.path + "/hire/hiredetail";
    let data = {
      ssnMbrUid: ssn.ssnMbrUid,
      ssnMbrDivi: ssn.ssnMbrDivi,
      hireUid: locationState.hireUid,
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    let tagstr = axiosRes.data.tags;
    var tags = defVal.SplitToArray(tagstr, "<;:>");
    
    axiosRes.data.thmnlNm = "";
    axiosRes.data.thmnlNm2 = "";
    axiosRes.data.resumeFormThmnlNm = "";

    let atchFilesStr = axiosRes.data.atchFiles;
    if (atchFilesStr != "") {
      atchFiles = JSON.parse(atchFilesStr);
      for (var k = 0; k < atchFiles.length; k++) {
        if (atchFiles[k].filePos == 1)
          axiosRes.data.thmnlNm = atchFiles[k].orignlFileNm;
        else if (atchFiles[k].filePos == 2)
          axiosRes.data.thmnlNm2 = atchFiles[k].orignlFileNm;
        else if (atchFiles[k].filePos == 3)
          axiosRes.data.resumeFormThmnlNm = atchFiles[k].orignlFileNm;
      }
    }

    axiosRes.data.tags = tags;
    setDetailState(axiosRes.data);

    getBizCateList();
    getEmpAddrCateList();
    getWorkPosCateList();

    //일반모집요강
    var tmpBizGuidList = await getBizGuidList(axiosRes.data.mbrUid);
  }

  const getBizGuidList = async (mbrUid) => {
    let goUrl = server.path + "/corporate/bizguidlist";
    let data = {
      mbrUid: mbrUid,
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    let tmpArr = axiosRes.data.resArr;    

    for(var k=0; k<tmpArr.length; k++)
    {
      if( tmpArr[k].thmnl != "")
      {
        let fileList = JSON.parse(tmpArr[k].atchFiles);
        tmpArr[k].orignlFileNm = fileList[0].orignlFileNm;
      }

      if( tmpArr[k].brdDivi != "02" )setGuid01Cnt(1);
      else setGuid02Cnt(1);
    }

    setBizGuidListState(tmpArr);
    return tmpArr;
  };


  const getBizCateList = async () => {
    let goUrl = server.path + "/hire/bizcate";
    let data = {
      hireUid: locationState.hireUid,
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    setBizCate(axiosRes.data.bizCateArr);
  };

  const getWorkPosCateList = async () => {
    let goUrl = server.path + "/hire/workposcate";
    let data = {
      hireUid: locationState.hireUid,
    };

    let axiosCateRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    setWorkPosCate(axiosCateRes.data.workPosCateArr);
  };

  const getEmpAddrCateList = async () => {
    let goUrl = server.path + "/hire/empaddrcate";
    let data = {
      hireUid: locationState.hireUid,
    };

    let axiosCateRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    setEmpAddrCate(axiosCateRes.data.empAddrCateArr);
  };

  const reRender = () => {
    setDetailState({
      ...detailState,
      initSetCmplt: true,
    });
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////
 const handleDownload = (filePath, orignlFileNm) => {
  
    if (filePath != "") {
      let goUrl = server.host + filePath;
      
      //alert("goUrl == " + goUrl);
      
      axios
        .get(goUrl, {
          responseType: "blob",
        })
        .then((res) => {
          fileDownload(res.data, orignlFileNm);
        });
    } else {
      
    }

  }

  return (
    <main>
      <section className="formSection">
        <div className="formHeader">
          <h1>
            <FormattedMessage id="posting.manage" defaultMessage="공고관리" />
            <em>
              <FormattedMessage
                id="posting.temp.storage"
                defaultMessage="임시저장 채용공고"
              />
            </em>
          </h1>
          <h2>{detailState.bizNm}</h2>
        </div>
        <article>
          <form>
            <h2>
              <FormattedMessage id="company.info" defaultMessage="기업정보" />
            </h2>
            <div className="formWrap">
              <ul className="twoCol">
                <li>
                  <span className="labelSpan">
                    <FormattedMessage
                      id="company.name.open"
                      defaultMessage="기업명 공개"
                    />
                  </span>
                  <span className="inputText">
                    {detailState.pubYn === "Y" ? (
                      detailState.bizNm
                    ) : (
                      <FormattedMessage id="secret" defaultMessage="비공개" />
                    )}
                  </span>
                </li>
                <li>
                  <span className="labelSpan">
                    <FormattedMessage
                      id="company.type"
                      defaultMessage="기업형태"
                    />
                  </span>
                  <span className="inputText">
                    {bizKndCd.map((type, idx) =>
                      detailState.bizKnd === type.cateCd ? type.cdName : ""
                    )}
                  </span>
                </li>
              </ul>
              <ul className="twoCol">
                <li>
                  <span className="labelSpan">
                    <FormattedMessage
                      id="company.sales"
                      defaultMessage="매출규모"
                    />
                  </span>
                  <span className="inputText">
                    {salesSizeCd.map((type, idx) =>
                      detailState.saleSize === type.cateCd ? type.cdName : ""
                    )}
                  </span>
                </li>
                <li>
                  <span className="labelSpan">
                    <FormattedMessage
                      id="company.workers"
                      defaultMessage="종업원 수"
                    />
                  </span>
                  <span className="inputText">
                    {empSizeCd.map((type, idx) =>
                      detailState.empSize === type.cateCd ? type.cdName : ""
                    )}
                  </span>
                </li>
              </ul>
              <ul className="oneCol">
                <li>
                  <div className="adminFlexColumn">
                    {bizCate.length === 0 ? (
                      <div>
                        <span className="labelSpan">
                          <FormattedMessage
                            id="company.business.type"
                            defaultMessage="업종구분"
                          />
                        </span>
                        <span className="inputText"></span>
                        <span className="inputText"></span>
                        <span className="inputText"></span>
                      </div>
                    ) : (
                      bizCate.map((type, rowidx) => (
                        <div>
                          <span className="labelSpan">
                            {rowidx === 0
                              ? `${intl.formatMessage({
                                  id: "company.business.type",
                                  defaultMessage: "업종구분",
                                })}`
                              : ""}
                          </span>
                          <span className="inputText">{type.cateNm1}</span>
                          <span className="inputText">
                            {type.cateNm2 !== "" ? " > " + type.cateNm2 : ""}
                          </span>
                          <span className="inputText">
                            {type.cateNm3 !== "" ? " > " + type.cateNm3 : ""}
                          </span>
                        </div>
                      ))
                    )}
                  </div>
                </li>
              </ul>
              <ul className="oneCol">
                <li>
                  <div className="adminFlexColumn">
                    {empAddrCate.length === 0 ? (
                      <div>
                        <span className="labelSpan">
                          <FormattedMessage
                            id="company.workplace"
                            defaultMessage="근무 예정지"
                          />
                        </span>
                        <span className="inputText"></span>
                        <span className="inputText"></span>
                        <span className="inputText"></span>
                      </div>
                    ) : (
                      empAddrCate.map((type, rowidx) => (
                        <div>
                          <span className="labelSpan">
                            {rowidx === 0
                              ? `${intl.formatMessage({
                                  id: "company.workplace",
                                  defaultMessage: "근무 예정지",
                                })}`
                              : ""}
                          </span>
                          <span className="inputText">{type.cateNm1}</span>
                          <span className="inputText">
                            {type.cateNm2 !== "" ? " > " + type.cateNm2 : ""}
                          </span>
                        </div>
                      ))
                    )}
                  </div>
                </li>
              </ul>
            </div>
            <h2>
              <FormattedMessage
                id="recruitment.content"
                defaultMessage="모집내용"
              />
            </h2>
            <div className="formWrap">
              <ul className="oneCol">
                <li>
                  <span className="labelSpan">
                    <FormattedMessage
                      id="recruitment.title"
                      defaultMessage="채용공고 제목"
                    />
                  </span>
                  <span className="inputText">{detailState.title}</span>
                </li>
              </ul>
              <ul className="oneCol">
                <li>
                  <span className="labelSpan">
                    <FormattedMessage
                      id="recruitment.position.classify"
                      defaultMessage="직종별 구분"
                    />
                  </span>
                  <div className="adminFlexColumn">
                    {workPosCate.length === 0 ? (
                      <div>
                        <span className="inputText"></span>
                        <span className="inputText"></span>
                      </div>
                    ) : (
                      workPosCate.map((type, rowidx) => (
                        <div>
                          <span className="inputText">{type.cateNm1}</span>
                          <span className="inputText">
                            {type.cateNm2 !== "" ? " > " + type.cateNm2 : ""}
                          </span>
                        </div>
                      ))
                    )}
                  </div>
                </li>
              </ul>
              <ul className="twoCol">
                <li>
                  <span className="labelSpan">
                    <FormattedMessage
                      id="recruitment.people.number"
                      defaultMessage="모집인원"
                    />
                  </span>
                  <span className="inputText">
                    {hireSizeCd.map((type, idx) =>
                      detailState.hireSize === type.cateCd ? type.cdName : ""
                    )}
                  </span>
                </li>
                <li>
                  <span className="labelSpan">
                    <FormattedMessage
                      id="recruitment.employment.form"
                      defaultMessage="고용형태"
                    />
                  </span>
                  <span className="inputText">
                    {commonUtil.indexOf(detailState.hireKnd, "fullTime") >
                    -1 ? (
                      <FormattedMessage
                        id="recruitment.employment.permanent"
                        defaultMessage="정규직"
                      />
                    ) : (
                      ""
                    )}

                    {commonUtil.indexOf(detailState.hireKnd, "fullTime") > -1 &&
                    commonUtil.indexOf(detailState.hireKnd, "cntrct") > -1
                      ? ", "
                      : ""}

                    {commonUtil.indexOf(detailState.hireKnd, "cntrct") > -1 ? (
                      <FormattedMessage
                        id="recruitment.employment.contract"
                        defaultMessage="계약직"
                      />
                    ) : (
                      ""
                    )}
                  </span>
                </li>
              </ul>
              <ul className="oneCol">
                <li>
                  <span className="labelSpan">
                    <FormattedMessage
                      id="recruitment.deadline"
                      defaultMessage="인재추천 마감일"
                    />
                  </span>
                  <span className="inputText">
                    {untilDone === true
                      ? `${intl.formatMessage({
                          id: "until.hired",
                          defaultMessage: "채용시까지",
                        })}`
                      : moment(detailState.rcmndEndDttm).format(
                          "YYYY년 MM월 DD일 HH시"
                        )}
                  </span>
                </li>
              </ul>
              <ul className="twoCol">
                <li>
                  <span className="labelSpan">
                    <FormattedMessage
                      id="recruitment.recommend.auto.close"
                      defaultMessage="인재추천 자동마감"
                    />
                  </span>
                  <span className="inputText">
                    {detailState.rcmndAutoEndYn === "Y" ? (
                      <FormattedMessage id="yes" defaultMessage="예" />
                    ) : (
                      <FormattedMessage id="no" defaultMessage="아니오" />
                    )}
                  </span>
                </li>
                <li>
                  <span className="labelSpan">
                    <FormattedMessage
                      id="recruitment.progress.mailing"
                      defaultMessage="진행과정 메일링"
                    />
                  </span>
                  <span className="inputText">
                    {detailState.rcmndPrgMailYn === "Y" ? (
                      <FormattedMessage id="mail.agree" defaultMessage="수신" />
                    ) : (
                      <FormattedMessage
                        id="mail.disagree"
                        defaultMessage="미수신"
                      />
                    )}
                  </span>
                </li>
              </ul>
            </div>
            <h2>
              <FormattedMessage
                id="normal.application.guideline"
                defaultMessage="일반 모집요강"
              />
            </h2>

            <article style={{display:(guid01Cnt == 0)? 'none':'' }}>
              <table>
                <thead>
                  <tr>
                    <th>
                      <FormattedMessage
                        id="application.guideline"
                        defaultMessage="모집요강"
                      />
                    </th>
                    <th>
                      <FormattedMessage id="prologue" defaultMessage="머릿말" />
                    </th>
                    <th>
                      <FormattedMessage id="content" defaultMessage="내용" />
                    </th>
                    <th>
                      <FormattedMessage
                        id="file.attached"
                        defaultMessage="첨부파일"
                      />
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {bizGuidListState.map((type, idx) =>
                    type.brdDivi !== "02" ? (
                      <tr>
                        <td>
                          {bizGuidDivi.map((type1, idx1) =>
                            type1.cateCd === type.brdDivi ? type1.cdName : ""
                          )}
                        </td>
                        <td>
                          {bizGuidKnd.map((type2, idx2) =>
                            type2.cateCd === type.brdKnd ? type2.cdName : ""
                          )}
                        </td>
                        <td>{type.cntnt}</td>
                        <td>
                          <span>{type.orignlFileNm}</span>
                            {
                              type.thmnl != '' ? 
                              (
                                <button type="button" className="basicButton" style={{marginLeft:'10px', display:(type.thmnl == '')? 'none' : ''}}
                                onClick={() => handleDownload(type.thmnl, type.orignlFileNm)}>
                                  <FormattedMessage
                                    id="file.download"
                                    defaultMessage="파일다운"
                                  />
                                </button>  
                              ) : ("")
                            }
                        </td>
                      </tr>
                    ) : (
                      ""
                    )
                  )}
                </tbody>
              </table>
            </article>

            <div
              className="formWrap"
              style={{ padding: "10px", marginBottom: "10px" }}
            >
              <div
                style={{
                  background: "#ffffff",
                  padding: "20px",
                  border: "1px solid #D7D7D7",
                }}
                dangerouslySetInnerHTML={{ __html: detailState.intro }}
              ></div>
            </div>
            <div className="fileBoxWrap">
              <span className="fileName">{detailState.thmnlNm}</span>

              <button type="button" className="basicButton" style={{marginLeft:'10px', display:(detailState.thmnlNm == '')? 'none' : ''}}
              onClick={() => handleDownload(detailState.thmnl, detailState.thmnlNm)}>
                <FormattedMessage
                  id="file.download"
                  defaultMessage="파일다운"
                />
              </button>  

            </div>

            <h2>
              <FormattedMessage
                id="detail.application.guideline"
                defaultMessage="상세 모집요강"
              />{" "}
              <em>
                (
                <FormattedMessage
                  id="detail.application.warning"
                  defaultMessage="상세 모집요강은 선택된 리크루터에게만 공개됩니다."
                />
                )
              </em>
            </h2>

            <article style={{display:(guid02Cnt == 0)? 'none':'' }}>
              <table>
                <thead>
                  <tr>
                    <th>
                      <FormattedMessage
                        id="application.guideline"
                        defaultMessage="모집요강"
                      />
                    </th>
                    <th>
                      <FormattedMessage id="prologue" defaultMessage="머릿말" />
                    </th>
                    <th>
                      <FormattedMessage id="content" defaultMessage="내용" />
                    </th>
                    <th>
                      <FormattedMessage
                        id="file.attached"
                        defaultMessage="첨부파일"
                      />
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {bizGuidListState.map((type, idx) =>
                    type.brdDivi === "02" ? (
                      <tr>
                        <td>
                          {bizGuidDivi.map((type1, idx1) =>
                            type1.cateCd === type.brdDivi ? type1.cdName : ""
                          )}
                        </td>
                        <td>
                          {bizGuidKnd.map((type2, idx2) =>
                            type2.cateCd === type.brdKnd ? type2.cdName : ""
                          )}
                        </td>
                        <td>{type.cntnt}</td>
                        <td>
                          <span>{type.orignlFileNm}</span>
                            {
                              type.thmnl != '' ? 
                              (
                                <button type="button" className="basicButton" style={{marginLeft:'10px', display:(type.thmnl == '')? 'none' : ''}}
                                onClick={() => handleDownload(type.thmnl, type.orignlFileNm)}>
                                  <FormattedMessage
                                    id="file.download"
                                    defaultMessage="파일다운"
                                  />
                                </button>  
                              ) : ("")
                            }
                        </td>
                      </tr>
                    ) : (
                      ""
                    )
                  )}
                </tbody>
              </table>
            </article>

            <div className="formWrap" style={{ marginBottom: "10px" }}>
              <i>
                <FormattedMessage
                  id="detail.application.description"
                  defaultMessage="위크루트 매니저가 검수과정을 통해 별도로 입력합니다."
                />
              </i>
            </div>
            
            <div className="fileBoxWrap">
              <span className="fileName">{detailState.thmnlNm2}</span>

              <button type="button" className="basicButton" style={{marginLeft:'10px', display:(detailState.thmnlNm2 == '')? 'none' : ''}}
              onClick={() => handleDownload(detailState.thmnl2, detailState.thmnlNm2)}>
                <FormattedMessage
                  id="file.download"
                  defaultMessage="파일다운"
                />
              </button>  
            </div>    

            <h2>
              <FormattedMessage
                id="searching.keyword"
                defaultMessage="인재써칭 핵심 키워드"
              />
            </h2>
            <div className="formWrap">
              <ul className="oneCol">
                <div className="tags">
                  <div className="tagList">
                    {detailState.tags.map((text, idx) => (
                      <span
                        style={{
                          background: "#0F7EC3",
                          color: "#ffffff",
                          borderRadius: "20px",
                        }}
                      >
                        {text}
                      </span>
                    ))}
                  </div>
                </div>
              </ul>
            </div>
            <h2>
              <FormattedMessage
                id="suggestion.setting"
                defaultMessage="제안 설정"
              />
            </h2>
            <div className="formWrap">
              <ul className="oneCol">
                <li>
                  <span className="labelSpan">
                    <FormattedMessage
                      id="suggestion.expected.salary"
                      defaultMessage="예상 연봉범위"
                    />
                  </span>
                  <div>
                    <span>
                      <FormattedMessage id="minimum" defaultMessage="최소" />
                    </span>
                    <span className="inputText">
                      {detailState.salaryMin}{" "}
                      <FormattedMessage id="currency" defaultMessage="만원" />
                    </span>{" "}
                    ~
                    <span>
                      {" "}
                      <FormattedMessage
                        id="maximum"
                        defaultMessage="최대"
                      />{" "}
                    </span>
                    <span className="inputText">
                      {detailState.salaryMax}{" "}
                      <FormattedMessage id="currency" defaultMessage="만원" />
                    </span>
                  </div>
                </li>
              </ul>
              <ul className="oneCol">
                <li>
                  <span className="labelSpan">
                    <FormattedMessage
                      id="suggestion.service.fee"
                      defaultMessage="서비스 수수료율"
                    />
                  </span>
                  {detailState.srvcFeeFixYn === "Y" ? (
                    <div>
                      <span>
                        <FormattedMessage
                          id="suggestion.fixed.fee.system"
                          defaultMessage="정액제"
                        />{" "}
                        (
                        <FormattedMessage
                          id="description.vat.not.included"
                          defaultMessage="부가세 별도입니다."
                        />
                        )
                      </span>
                    </div>
                  ) : (
                    <div>
                      <span>
                        <FormattedMessage id="minimum" defaultMessage="최소" />
                      </span>
                      <span className="inputText">
                        {detailState.srvcFeePctMin}%
                      </span>
                      <span>
                        ~{" "}
                        <FormattedMessage id="maximum" defaultMessage="최대" />
                      </span>
                      <span className="inputText">
                        {detailState.srvcFeePctMax}%{" "}
                      </span>
                      <label htmlFor="certainAmount">
                        (
                        <FormattedMessage
                          id="description.vat.not.included"
                          defaultMessage="부가세 별도입니다."
                        />
                        )
                      </label>
                    </div>
                  )}
                </li>
              </ul>
              <ul className="oneCol">
                <li>
                  <label htmlFor="guaranteeCondition">
                    <FormattedMessage
                      id="suggestion.service.guarantee"
                      defaultMessage="서비스 보증조건"
                    />
                  </label>
                  <span className="inputText">
                    {detailState.srvcWrtyPeriod}
                    <FormattedMessage id="months" defaultMessage="개월" />
                    <FormattedMessage
                      id="suggestion.incase.retire"
                      defaultMessage="이내 퇴직시"
                    />
                    {detailState.srvcWrtyMthd === "prorated" ? (
                      <FormattedMessage
                        id="suggestion.caculated.refund"
                        defaultMessage="일할계산 환불"
                      />
                    ) : detailState.srvcWrtyMthd === "all" ? (
                      <FormattedMessage
                        id="suggestion.all.refund"
                        defaultMessage="100% 전액 환불"
                      />
                    ) : detailState.srvcWrtyMthd === "etc" ? (
                      <FormattedMessage id="etc" defaultMessage="기타" />
                    ) : (
                      ""
                    )}
                  </span>
                </li>
              </ul>
              <ul className="oneCol">
                <li>
                  <span className="labelSpan">
                    <FormattedMessage
                      id="resume.format"
                      defaultMessage="이력서 양식"
                    />
                  </span>
                  <span className="inputText">
                    {detailState.resumeForm === "recForm" ? (
                      <FormattedMessage
                        id="resume.format.free"
                        defaultMessage="리크루터 자유 양식 이력서"
                      />
                    ) : detailState.resumeForm === "cmpnyForm" ? (
                      <FormattedMessage
                        id="resume.format.compnay"
                        defaultMessage="회사 소정 양식 이력서"
                      />
                    ) : (
                      ""
                    )}
                  </span>
                </li>
              </ul>
            </div>
            <h2>
              <FormattedMessage
                id="setting.condition"
                defaultMessage="설정 조건"
              />
            </h2>
            <div className="formWrap">
              <ul className="oneCol">
                <li>
                  <span className="labelSpan">
                    <FormattedMessage
                      id="recommend.recruiter"
                      defaultMessage="우수 리크루터 추천"
                    />
                  </span>
                  <span className="inputText">
                    {detailState.rcmndRecYn === "Y" ? (
                      <FormattedMessage
                        id="yes.agree"
                        defaultMessage="예, 동의합니다."
                      />
                    ) : detailState.rcmndRecYn === "N" ? (
                      <FormattedMessage
                        id="no.disagree"
                        defaultMessage="아니오. 동의하지 않습니다."
                      />
                    ) : (
                      <FormattedMessage id="error" defaultMessage="오류" />
                    )}
                  </span>
                </li>
              </ul>
              <ul className="oneCol">
                <li>
                  <span className="labelSpan">
                    <FormattedMessage
                      id="recruit.method"
                      defaultMessage="리크루터 모집방법"
                    />
                  </span>
                  <span className="inputText onlyWecruit">
                    {detailState.recMthd === "rec" ? (
                      <FormattedMessage
                        id="recruit.method1"
                        defaultMessage="위크루트에 등록된 리크루터만 참여 가능"
                      />
                    ) : detailState.recMthd === "recAndSrchfirm" ? (
                      <FormattedMessage
                        id="recruit.method2"
                        defaultMessage="위크루트에 등록된 리크루터 + 기존 거래 써치펌 참여 가능"
                      />
                    ) : detailState.recMthd === "srchfirm" ? (
                      <FormattedMessage
                        id="recruit.method3"
                        defaultMessage="기존 거래 써치펌만 참여 가능"
                      />
                    ) : (
                      ""
                    )}
                  </span>
                </li>
              </ul>
            </div>
            <h2>
              <FormattedMessage
                id="employer.contact.info.open"
                defaultMessage="인사담당자 연락처 공개 설정"
              />
              <em>
                <FormattedMessage
                  id="employer.contact.info.open.alert1"
                  defaultMessage="(연락처 공개 설정은 선택된 리크루터에게만 공개됩니다.)"
                />
              </em>
            </h2>
            <div className="formWrap">
              <ul className="oneCol">
                <li>
                  <div className="checkboxList">
                    <input
                      type="checkbox"
                      id="employerEmailDisposal"
                      disabled
                      readOnly
                      checked={detailState.emailPubYn === "Y" ? true : false}
                    />
                    <label htmlFor="employerEmailDisposal">
                      <FormattedMessage id="email" defaultMessage="이메일" />
                    </label>

                    <input
                      type="checkbox"
                      id="employerSnsDisposal"
                      disabled
                      readOnly
                      checked={detailState.snsPubYn === "Y" ? true : false}
                    />
                    <label htmlFor="employerSnsDisposal">
                      <FormattedMessage
                        id="sns"
                        defaultMessage="SNS (라인, 카카오톡 등)"
                      />
                    </label>

                    <input
                      type="checkbox"
                      id="employerPhoneDisposal"
                      disabled
                      readOnly
                      checked={detailState.celpNumPubYn === "Y" ? true : false}
                    />
                    <label htmlFor="employerPhoneDisposal">
                      <FormattedMessage
                        id="phone.number"
                        defaultMessage="휴대폰 번호"
                      />
                    </label>

                    <input
                      type="checkbox"
                      id="employerOfficeDisposal"
                      disabled
                      readOnly
                      checked={detailState.phNumPubYn === "Y" ? true : false}
                    />
                    <label htmlFor="employerOfficeDisposal">
                      <FormattedMessage
                        id="office.number"
                        defaultMessage="사무실 전화"
                      />
                    </label>

                    <input
                      type="checkbox"
                      id="employerAddressDisposal"
                      disabled
                      readOnly
                      checked={detailState.addrPubYn === "Y" ? true : false}
                    />
                    <label htmlFor="employerAddressDisposal">
                      <FormattedMessage
                        id="address.open"
                        defaultMessage="주소공개 및 방문"
                      />
                    </label>

                    <p>
                      <FormattedMessage
                        id="employer.contact.info.open.alert2"
                        defaultMessage="* 위크루트는 Q&amp;A 게시판에 댓글을 남기시면 자동으로 담당자 업무용 이메일로 해당 내용이 발송됩니다."
                      />
                    </p>
                  </div>
                </li>
              </ul>
            </div>
            <div className="flexAlignCenter">
              <Link to="/admin/temporary" className="formButton">
                <FormattedMessage id="show.list" defaultMessage="리스트 보기" />{" "}
                &gt;
              </Link>
            </div>
          </form>
        </article>
      </section>
    </main>
  );
};
