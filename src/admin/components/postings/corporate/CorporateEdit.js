import { Link, useLocation, useNavigate } from "react-router-dom";
import { useContext, useEffect, useRef, useState } from "react";
import { Context } from "contexts";
import { toast, ToastContainer } from "react-toastify";
import axios from "axios";
import fileDownload from "js-file-download";

import { axiosCrudOnSubmit } from "commons/modules/commonUtil";
import { Pagination } from "commons/components/pagination/Pagination";
import { DownloadBtns } from "commons/components/download-btn/DownloadBtns";
import { snglFileUpload } from "commons/modules/multipartUtil";
import * as defVal from "commons/modules/defVal";
import { AddressInput } from "commons/components/AddressInput";
import { ImagePreview } from "commons/components/inputs/image-preview/ImagePreview";
import { UPD_MBRUID } from "contexts/actionTypes";
import { FormattedMessage, useIntl } from "react-intl";

export const CorporateEdit = () => {
  const intl = useIntl();

  const [state, setState] = useState({});
  const [listState, setListState] = useState([]);
  const [fileListState, setFileListState] = useState([]);
  const [sConts, setSConts] = useState("");
  const [fltrState, setFltrState] = useState({});
  const [bizMbrListState, setBizMbrListState] = useState([]);
  const [bizGuidListState, setBizGuidListState] = useState([]);

  const bizGuidDivi = defVal.BizGuidDivi();
  const bizGuidKnd = defVal.BizGuidKnd();

  const location = useLocation();
  const navigate = useNavigate();
  const moment = require("moment");
  let previewWindow = "";
  let previewImg = document.createElement("img");
  const componentRef = useRef();

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid"),
    ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi"),
  };

  let locationState = {
    sDtlCont: location.state.sDtlCont,
  };

  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(10);

  const indexOfLast = currentPage * postsPerPage;
  const indexOfFirst = indexOfLast - postsPerPage;

  let arraySimb = "<:;>";

  const {
    state: { mbr, server },
    dispatch,
  } = useContext(Context);

  function currentPosts(tmp) {
    let currentPosts = 0;
    currentPosts = tmp.slice(indexOfFirst, indexOfLast);
    return currentPosts;
  }

  const changeFltr = (fltr) => {
    let tmpFltr = fltr === "ASC" ? "DESC" : fltr === "DESC" ? "ASC" : "ASC";

    return tmpFltr;
  };

  useEffect(async () => {
    console.log("Page First Load EFFECT");
    setSConts(locationState.sDtlCont);

    let goUrl = server.path + "/corporate/bizlist";
    let data = {
      ssnMbrUid: ssn.ssnMbrUid,
      ssnMbrDivi: ssn.ssnMbrDivi,
      mbrUid: mbr.mbrUid,
      mbrDivi: "MBRDIVI-01",
      sConts: locationState.sDtlCont,
      sFltr: fltrState,
      sFltrCate: Object.keys(fltrState)[0],
      sFltrCont: Object.values(fltrState)[0],
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    setListState(axiosRes.data.resArr);

    //goSrch();
    initSetHandle();
  }, [fltrState]);

  async function initSetHandle() {
    let data = {
      stateYn: true,
    };
    setState(data);
  }

  async function goSrch() {
    let goUrl = server.path + "/corporate/bizlist";
    let data = {
      ssnMbrUid: ssn.ssnMbrUid,
      ssnMbrDivi: ssn.ssnMbrDivi,
      mbrUid: mbr.mbrUid,
      mbrDivi: "MBRDIVI-01",
      sConts: sConts,
      sFltr: fltrState,
      sFltrCate: Object.keys(fltrState)[0],
      sFltrCont: Object.values(fltrState)[0],
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    for(var k=0; k<axiosRes.data.resArr.length; k++)
    {
      axiosRes.data.resArr[k].selYn = "N";
    }

    setListState(axiosRes.data.resArr);
  }

  useEffect(async () => {
    window.scrollTo(0, 0);
    getBizGuidList();
    //getBizMbrList();
    
  }, [listState]);

  const getBizMbrList = async (data) => {
    
    let goUrl = server.path + "/corporate/bizguidmbrlist";
    
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    let tmpArr = axiosRes.data.resArr;

    for(var k=0; k<listState.length; k++)
    {
      for(var kk=0; kk<tmpArr.length; kk++)
      {
        if( listState[k].mbrUid == tmpArr[kk].mbrUid)listState[k].selYn = "Y";
      }      
    }

    setBizMbrListState(tmpArr);
  };

  const getBizGuidList = async () => {
    var mbrUid = "NONE";
    for (var k = 0; k < listState.length; k++) {
      mbrUid = mbrUid + "," + listState[k].mbrUid;
    }

    let goUrl = server.path + "/corporate/bizguidlist";
    let data = {
      mbrUid: mbrUid,
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    let tmpArr = axiosRes.data.resArr;
    

    setBizGuidListState(tmpArr);
    
    var brdUid = "NONE";
    for(var kk=0; kk<tmpArr.length; kk++)
    {
      brdUid = brdUid+","+tmpArr[kk].brdUid;      
    }

    var data2 = {
      brdUid: brdUid,
    };

    getBizMbrList(data2);

  };

  

  useEffect(async () => {
    if (bizGuidListState && bizGuidListState.length > 0) {
      setBizGuidFiles();
    }
  }, [bizGuidListState]);

  const setBizGuidFiles = async () => {
    if (bizGuidListState && bizGuidListState.length > 0) {
      var tmpList = [];
      for (var k = 0; k < bizGuidListState.length; k++) {
        const fileArray = defVal.FileState();

        var jsonStr = "";
        var fileList = "";
        var orignlFileNm = "";

        if (bizGuidListState[k].atchFiles != "") {
          jsonStr = bizGuidListState[k].atchFiles;
          fileList = JSON.parse(jsonStr);
          if (fileList[0] && fileList[0].orignlFileNm) {
            orignlFileNm = fileList[0].orignlFileNm;
          }
        }

        fileArray.orignlFileNm = orignlFileNm;
        fileArray.filePosNm = "thmnl";
        fileArray.uploadFiles = "disable";
        fileArray.thmnl = bizGuidListState[k].thmnl;
        fileArray.snglImgFileJsonStr = bizGuidListState[k].atchFiles;
        tmpList.push(fileArray);
      }
      setFileListState(tmpList);
      initSetHandle();
    }
  };

  useEffect(async () => {
    if (fileListState && fileListState.length > 0) {
    }
  }, [fileListState]);

  const onKeyUp = (e) => {
    if (e.key == "Enter") goSrch();
    return false;
  };

  //const [types, setTypes] = useState([]);
  const nextId = useRef(0);

  const onAddBtnClick = (e) => {
    const array = defVal.BizGuidDmmy();
    bizGuidListState.push(array);

    const fileArray = defVal.FileState();
    fileListState.push(fileArray);
    initSetHandle();
    nextId.current += 1;
  };

  function onKeyDown(keyEvent) {
    if ((keyEvent.charCode || keyEvent.keyCode) === 13) {
      keyEvent.preventDefault();
    }
  }

  let clickFilePos = 0;
  const clickFiles = (idx) => {
    clickFilePos = idx;
  };

  const chngFiles = (e) => {
    console.log("clickFilePos = 0 == " + clickFilePos);

    fileListState[clickFilePos].uploadFiles = e.target.files[0];
    fileListState[clickFilePos].orignlFileNm = e.target.files[0].name;
    fileListState[clickFilePos].snglImgFileJsonStr = "";

    initSetHandle();
  };

  ////////////////////////////////////////////////////////////////////////////////////////////
  let PROC_CHK = "";
  const jsonCompResult = (resJson) => {
    if (resJson.resCd !== "0000") {
      return;
    } else {
      if (PROC_CHK === "UPD") {
        toast.info(
          intl.formatMessage({
            id: "info.corp.info.saved",
            defaultMessage: "기업관리 정보가 저장되었습니다.",
          })
        );
        //getBizGuidList();
        //initSetHandle();
        navigate("/admin/corporate");
        return;
      } else if (PROC_CHK === "DEL") {
        toast.info(
          intl.formatMessage({
            id: "info.corp.info.deleted",
            defaultMessage: "기업관리 정보가 삭제되었습니다.",
          })
        );
        initSetHandle();
        return;
      } else if (PROC_CHK === "FILEUPLOAD") {
        let jsonStr = JSON.stringify(resJson.resFileList);
        let fileList = JSON.parse(jsonStr);
        console.log("FILEUPLOAD fileList == " + fileList.length);

        let tmpList = listState;
        for (let i = 0; i < fileList.length; i++) {
          var pos = fileList[i].filePos;
          console.log("FILEUPLOAD pos == " + pos);
          console.log("FILEUPLOAD stringify == " + JSON.stringify(fileList[i]));
          bizGuidListState[pos].atchFiles = JSON.stringify(fileList[i]);
        }

        initSetHandle();
        cntntsCRUD();
      }
    }
  };

  async function goCRUD() {
    let fileStorePath = "Globals.fileStoreBizGuidPath"; //로컬저장경로
    let fileLinkPath = "Globals.fileLinkBizGuidPath"; //link경로
    let filePrefix = "bizguid"; //파일코드

    PROC_CHK = "FILEUPLOAD";

    if (listState.length == 0) {
      toast.error(
        intl.formatMessage({
          id: "error.need.corp.name",
          defaultMessage: "기업명을 검색해주세요.",
        })
      );
      return false;
    }

    if (bizGuidListState.length == 0) {
      toast.error(
        intl.formatMessage({
          id: "error.need.knowhow.memo",
          defaultMessage: "노하우 메모를 입력해주세요.",
        })
      );
      return false;
    }

    var valYn = "Y";
    for (var k = 0; k < bizGuidListState.length; k++) {
      if (
        bizGuidListState[k].brdDivi == "" ||
        bizGuidListState[k].brdKnd == "" ||
        bizGuidListState[k].cntnt == ""
      ) {
        valYn = "N";
        break;
      }
    }

    if (valYn == "N") {
      toast.error(
        intl.formatMessage({
          id: "error.need.knowhow.memo",
          defaultMessage: "노하우 메모를 입력해주세요.",
        })
      );
      return false;
    }

    var uploadFiles = [];
    var filePosNm = [];
    for (var k = 0; k < fileListState.length; k++) {
      console.log("filePosNm == " + fileListState[k].filePosNm);
      filePosNm.push(fileListState[k].filePosNm);
      uploadFiles.push(fileListState[k].uploadFiles);
    }

    let uploadRes = await snglFileUpload(
      server.path,
      fileStorePath,
      fileLinkPath,
      filePrefix,
      uploadFiles,
      filePosNm
    );

    if (uploadRes === 0) cntntsCRUD();
    else jsonCompResult(uploadRes.data);
  }

  async function cntntsCRUD() {
    PROC_CHK = "UPD";

    console.log("CNTNTS CRUD");

    var brdUid = [];
    var brdDivi = [];
    var brdKnd = [];
    var mbrUid = [];
    var title = [];
    var cntnt = [];
    var thmnl = [];
    var atchFiles = [];

    var mbrUidStr = "";
    for (var k = 0; k < bizMbrListState.length; k++) {
      if (k == 0) mbrUidStr = bizMbrListState[k].mbrUid;
      else mbrUidStr = mbrUidStr + "," + bizMbrListState[k].mbrUid;
    }

    brdUid.push("NONE");
    brdDivi.push("NONE");
    brdKnd.push("NONE");
    mbrUid.push("NONE");
    title.push("NONE");
    cntnt.push("NONE");
    thmnl.push("NONE");
    atchFiles.push("NONE");
    for (var k = 0; k < bizGuidListState.length; k++) {
      brdUid.push(bizGuidListState[k].brdUid);
      brdDivi.push(bizGuidListState[k].brdDivi);
      brdKnd.push(bizGuidListState[k].brdKnd);
      mbrUid.push(mbrUidStr);
      title.push(bizGuidListState[k].title);
      cntnt.push(bizGuidListState[k].cntnt);
      thmnl.push(bizGuidListState[k].thmnl);
      atchFiles.push(bizGuidListState[k].atchFiles);
    }

    brdUid.push("NONE");
    brdDivi.push("NONE");
    brdKnd.push("NONE");
    mbrUid.push("NONE");
    title.push("NONE");
    cntnt.push("NONE");
    thmnl.push("NONE");
    atchFiles.push("NONE");

    let goUrl = server.path + "/corporate/bizguidinsert";
    let data = {
      ssnMbrUid: ssn.ssnMbrUid,
      ssnMbrDivi: ssn.ssnMbrDivi,
      brdUid: brdUid,
      brdDivi: brdDivi,
      brdKnd: brdKnd,
      mbrUid: mbrUid,
      title: title,
      cntnt: cntnt,
      thmnl: thmnl,
      atchFiles: atchFiles,
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    jsonCompResult(axiosRes.data);
  }

  async function onRemove(idx) {
    var brdUid = bizGuidListState[idx].brdUid;

    if (brdUid != "" && !window.confirm("삭제하시겠습니까?")) return;

    PROC_CHK = "DEL";

    if (brdUid != "") {
      let goUrl = server.path + "/corporate/bizguiddelete";
      let data = {
        ssnMbrUid: ssn.ssnMbrUid,
        ssnMbrDivi: ssn.ssnMbrDivi,
        brdUid: brdUid,
      };
      let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
      jsonCompResult(axiosRes.data);
    }

    var tmpList = [];
    var tmpFileList = [];
    for (var k = 0; k < bizGuidListState.length; k++) {
      if (k != idx) {
        tmpList.push(bizGuidListState[k]);
        tmpFileList.push(fileListState[k]);
      }
    }

    setBizGuidListState(tmpList);
    setFileListState(tmpFileList);
    initSetHandle();
  }

  const selEmp = async (idx) => {
   
    var tmp = {};
    var tmpArr = bizMbrListState;
    
    listState[idx].selYn = "Y";

    tmp.mbrUid = listState[idx].mbrUid;
    tmp.mbrId = listState[idx].mbrId;
    tmp.mbrNm = listState[idx].mbrNm;

    tmpArr.push(tmp);
    
    setBizMbrListState(tmpArr);

    reRender();

  }

  const rmvEmp = async (idx, mbrUid) => {
   
    var tmp = {};
    var tmpArr=[];
    var rmvPos=0;

    for( var k=0; k<bizMbrListState.length; k++ )
    {
      if( k != idx )
      {
        tmpArr.push(bizMbrListState[k]);
      }
    }

    for( var k=0; k<listState.length; k++ )
    {
      if( listState[k].mbrUid == mbrUid )
      {
        listState[k].selYn = "N";
      }
    }

    setBizMbrListState(tmpArr);

    reRender();

  }

  const reRender = () => {
    
    setState({
      ...state,
      stateYn: true
    })
          
  };

  return (
    <main>
      <section className="formSection">
        <div className="formHeader">
          <h1>
            <FormattedMessage
              id="corporate.manage"
              defaultMessage="기업 관리"
            />{" "}
            <em>
              <FormattedMessage
                id="edit.corporate"
                defaultMessage="기업 수정하기"
              />
            </em>
          </h1>
          <h2>
            <FormattedMessage id="company.info" defaultMessage="기업정보" />
            <em>
              (
              <FormattedMessage
                id="description.corporate.auto.fill"
                defaultMessage="담당자 선택시, 기업명은 자동 기입됩니다."
              />
              )
            </em>
          </h2>
        </div>

        <article>
          <form onKeyDown={onKeyDown}>
            <div className="formWrap">
              <ul className="oneCol">
                <li>
                  <label htmlFor="corporateTitle">
                    <FormattedMessage
                      id="company.name"
                      defaultMessage="기업명"
                    />
                  </label>
                  <input
                    type="search"
                    id="corporateTitle"
                    className="fullWidth"
                    placeholder={intl.formatMessage({
                      id: "placeholder.no.corporate.name",
                      defaultMessage: "입력된 기업명이 없습니다.",
                    })}
                    value={sConts}
                    onChange={(e) => {
                      setSConts(e.target.value);
                    }}
                    onKeyUp={(e) => {
                      onKeyUp(e);
                    }}
                  />
                  <button
                    type="button"
                    className="basicButton"
                    onClick={goSrch}
                  >
                    검색
                  </button>
                </li>
              </ul>
              <ul className="oneCol">
                <li>
                  <label>
                    <FormattedMessage
                      id="manager.name"
                      defaultMessage="담당자 이름"
                    />
                  </label>
                  <span>
                    {bizMbrListState.length === 0 ? (
                      <FormattedMessage
                        id="description.manager.name.here"
                        defaultMessage="이 곳에 담당자명이 표시됩니다."
                      />
                    ) : (
                      ""
                    )}

                    {bizMbrListState.map((type, idx) => (
                      <span style={{border:'1px solid #cccccc', borderRadius:'5px', padding:'2px 4px 2px 4px', marginRight:'5px'}}>{type.mbrNm}&nbsp;
                        <button 
                          className="delete" type="button" style={{paddingTop:'4px'}}>
                          <span className='material-icons' style={{fontSize:'15px', paddingTop:'4px'}} 
                          onClick={() => {rmvEmp(idx, type.mbrUid)}}>close</span>
                        </button>
                      </span>  
                    ))}
                  </span>
                </li>
              </ul>
            </div>

            <div className="corporateList">
              <article>
                <table>
                  <thead>
                    <tr>
                      <th>
                        <span>
                          <FormattedMessage id="number" defaultMessage="번호" />
                        </span>
                        <div className="filterArrow">
                          <span />
                          <span />
                        </div>
                      </th>
                      <th>
                        <span>
                          <FormattedMessage
                            id="company.name"
                            defaultMessage="기업명"
                          />
                        </span>
                        <div className="filterArrow">
                          <span />
                          <span />
                        </div>
                      </th>
                      <th>
                        <span>
                          <FormattedMessage
                            id="manager.id"
                            defaultMessage="담당자 ID"
                          />
                        </span>
                        <div className="filterArrow">
                          <span />
                          <span />
                        </div>
                      </th>

                      <th>
                        <span>
                          <FormattedMessage
                            id="manager.name"
                            defaultMessage="담당자 이름"
                          />
                        </span>
                        <div className="filterArrow">
                          <span />
                          <span />
                        </div>
                      </th>
                      <th>
                        <span>
                          <FormattedMessage
                            id="information.reflection"
                            defaultMessage="정보 반영"
                          />
                        </span>
                        <div className="filterArrow">
                          <span />
                          <span />
                        </div>
                      </th>
                    </tr>
                  </thead>

                  <tbody>
                    {currentPosts(listState).map((list, idx) => (
                      <tr>
                        <td>{idx + 1}</td>
                        <td>{list.bizNm}</td>
                        <td>{list.mbrId}</td>
                        <td>{list.mbrNm}</td>
                        <td>
                          {list.selYn != 'Y' ? (
                              <button type="button" onClick={() => {selEmp(idx)}} >
                                <FormattedMessage
                                  id="selection"
                                  defaultMessage="선택"
                                />
                              </button>
                            ) : ("")
                          } 
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </article>
              <Pagination
                postsPerPage={postsPerPage}
                totalPosts={listState.length}
                paginate={setCurrentPage}
                currentPage={currentPage}
              />
            </div>

            <div className="formHeader flex">
              <h2>
                <FormattedMessage
                  id="knowhow.memo"
                  defaultMessage="노하우 메모"
                />
              </h2>
              <button
                type="button"
                className="basicButton marginInline"
                onClick={onAddBtnClick}
              >
                +{" "}
                <FormattedMessage
                  id="column.addition"
                  defaultMessage="행 추가"
                />
              </button>
            </div>

            <article>
              <table>
                <thead>
                  <tr>
                    <th>
                      <FormattedMessage
                        id="application.guideline"
                        defaultMessage="모집요강"
                      />
                    </th>
                    <th>
                      <FormattedMessage id="prologue" defaultMessage="머릿말" />
                    </th>
                    <th>
                      <FormattedMessage id="content" defaultMessage="내용" />
                    </th>
                    <th>
                      <FormattedMessage
                        id="file.attached"
                        defaultMessage="첨부파일"
                      />
                    </th>
                    <th>
                      <FormattedMessage id="management" defaultMessage="관리" />
                    </th>
                  </tr>
                </thead>

                <tbody>
                  {bizGuidListState.map((type, idx) =>
                    idx >= 0 ? (
                      <tr>
                        <td>
                          <select
                            name="guid"
                            id="guid"
                            value={bizGuidListState[idx].brdDivi}
                            onChange={(e) => {
                              bizGuidListState[idx].brdDivi = e.target.value;
                              initSetHandle();
                            }}
                          >
                            <option value="">
                              --
                              {intl.formatMessage({
                                id: "select",
                                defaultMessage: "선택하세요",
                              })}
                              --
                            </option>
                            {bizGuidDivi.map((type, idx) => (
                              <option value={type.cateCd}>{type.cdName}</option>
                            ))}
                          </select>
                        </td>
                        <td>
                          <select
                            name="guid"
                            id="guid"
                            value={bizGuidListState[idx].brdKnd}
                            onChange={(e) => {
                              bizGuidListState[idx].brdKnd = e.target.value;
                              initSetHandle();
                            }}
                          >
                            <option value="">
                              --
                              {intl.formatMessage({
                                id: "select",
                                defaultMessage: "선택하세요",
                              })}
                              --
                            </option>
                            {bizGuidKnd.map((type, idx) => (
                              <option value={type.cateCd}>{type.cdName}</option>
                            ))}
                          </select>
                        </td>
                        <td className="width250">
                          <textarea
                            name="guidtxt"
                            id="guidtxt"
                            value={bizGuidListState[idx].cntnt}
                            onChange={(e) => {
                              bizGuidListState[idx].cntnt = e.target.value;
                              initSetHandle();
                            }}
                          ></textarea>
                        </td>
                        <td>
                          <div className="filebox">
                            <div>
                              <label
                                htmlFor="fileInput"
                                className="basicButton"
                                onClick={(e) => {
                                  clickFiles(idx);
                                }}
                              >
                                <FormattedMessage
                                  id="file.attachment"
                                  defaultMessage="파일 첨부"
                                />{" "}
                                &gt;
                              </label>
                              <input
                                type="file"
                                id="fileInput"
                                name="fileInput"
                                className="fileInput"
                                accept=".png, .jpg, .jpeg, .pdf"
                                onChange={(e) => {
                                  chngFiles(e);
                                }}
                              />
                            </div>
                            <div>
                              {fileListState[idx]
                                ? fileListState[idx].orignlFileNm
                                : ""}
                            </div>
                          </div>
                        </td>
                        <td>
                          <button
                            type="button"
                            className="rowDeleteButton"
                            onClick={() => onRemove(idx)}
                          >
                            <FormattedMessage
                              id="deletion"
                              defaultMessage="삭제"
                            />
                          </button>
                        </td>
                      </tr>
                    ) : (
                      ""
                    )
                  )}
                </tbody>
              </table>
            </article>

            <div className="buttons">
              <input
                type="button"
                value={intl.formatMessage({
                  id: "saving",
                  defaultMessage: "저장",
                })}
                onClick={goCRUD}
              />
            </div>
          </form>
        </article>
      </section>
    </main>
  );
};
