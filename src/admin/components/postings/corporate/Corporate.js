import {Link, useLocation, useNavigate} from 'react-router-dom';
import { useContext, useEffect, useRef, useState } from 'react';
import { Context } from 'contexts';
import axios from 'axios';
import fileDownload from 'js-file-download';

import { axiosCrudOnSubmit } from 'commons/modules/commonUtil';
import { Pagination } from 'commons/components/pagination/Pagination';
import { DownloadBtns } from 'commons/components/download-btn/DownloadBtns';
import { snglFileUpload } from 'commons/modules/multipartUtil';
import * as defVal  from 'commons/modules/defVal';
import { AddressInput } from 'commons/components/AddressInput';
import { ImagePreview } from 'commons/components/inputs/image-preview/ImagePreview';
import { UPD_MBRUID } from 'contexts/actionTypes';
import { FormattedMessage } from 'react-intl';

export const Corporate =() => {

  const [listState, setListState] = useState([]);
  const [sConts, setSConts] = useState("");
  const [fltrState, setFltrState] = useState({});
  const componentRef = useRef();
  const moment = require('moment');

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid")
    ,ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi")
  };

  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(10);

  const indexOfLast = currentPage * postsPerPage;
  const indexOfFirst = indexOfLast - postsPerPage;

  let arraySimb = "<:;>";

  const {
    state : {
        mbr,
        server
    },
    dispatch,
  } = useContext(Context);

  const headers = [
    { label: "번호", key: "mbrId" },
    { label: "기업명", key: "bizNm" },
    { label: "인사담당자 정보", key: "mbrId" }
  ];

  function currentPosts(tmp) {
    let currentPosts = 0;
    currentPosts = tmp.slice(indexOfFirst, indexOfLast);
    return currentPosts;
  }

  const changeFltr = (fltr) => {
    let tmpFltr = (fltr === "ASC") ? "DESC" :
                  (fltr === "DESC") ? "ASC" :
                  "ASC";

    return tmpFltr;
  }

  useEffect(async () => {
    console.log("Page First Load EFFECT");
    initSetHandle();      
    
  },[fltrState])

  async function initSetHandle() {
    let goUrl = server.path + "/corporate/corporatelist";
    let data = {
      ssnMbrUid : ssn.ssnMbrUid,
      ssnMbrDivi : ssn.ssnMbrDivi,
      mbrUid : mbr.mbrUid,
      mbrDivi : "MBRDIVI-01",
      sConts : sConts,
      sFltr : fltrState,
      sFltrCate : Object.keys(fltrState)[0],
      sFltrCont : Object.values(fltrState)[0],
    }

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    setListState(axiosRes.data.resArr);      
  }

  useEffect( async () => {
    window.scrollTo(0,0);
  },[listState])

  const onKeyUp =(e) => {
    if(e.key == "Enter")initSetHandle();   
    return false;
  }

  async function setDel(idx){
    console.log("onRemove idx == " + idx);
    
    let goUrl = server.path + "/corporate/corporatedelete";
    let data = {
      ssnMbrUid : ssn.ssnMbrUid,
      ssnMbrDivi : ssn.ssnMbrDivi,
      bizNm : idx,
      mbrDivi : "MBRDIVI-01"
    }

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
  };

  return(
    <main>
      <section className="tableSection">
        <div className="sectionHeader">
          <h3>
            <span className="material-icons-outlined">settings</span>
            <b>
              <FormattedMessage
                id="corporate.manage"
                defaultMessage="기업 관리"
              />
            </b>
          </h3>
        </div>

        <Link to="/admin/corporate-add">
          <div className="linkButton">
            <FormattedMessage
              id="corporate.add"
              defaultMessage="기업 추가"
            />
            <span className="material-icons-outlined">add</span>
          </div>
        </Link>

        <div className="searchBar">
          <label htmlFor="search">Search:</label>
          <input type="search" id="search" 
            onChange={(e) => {
              setSConts(e.target.value);
            }}

            onKeyUp={(e) => {
              onKeyUp(e);
            }}
          />
          <button type="button" onClick={initSetHandle}>
            <FormattedMessage
              id="search"
              defaultMessage="검색"
            />
          </button>
        </div>

        <article>
          <table>
            <thead>
              <tr>
                <th>
                  <span>
                    <FormattedMessage
                      id="number"
                      defaultMessage="번호"
                    />
                  </span>
                </th>
                <th className={ fltrState.bizNm === "DESC" ? "descending" :
                                fltrState.bizNm === "ASC" ? "ascending" :
                                undefined } 
                    onClick={() => {
                      setFltrState({
                        bizNm : changeFltr(fltrState.bizNm)
                      }); 
                }}>
                  <span>
                    <FormattedMessage
                      id="company.name"
                      defaultMessage="기업명"
                    />
                  </span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th className={ fltrState.mbrNm === "DESC" ? "descending" :
                                fltrState.mbrNm === "ASC" ? "ascending" :
                                undefined } 
                    onClick={() => {
                      setFltrState({
                        mbrNm : changeFltr(fltrState.mbrNm)
                      }); 
                }}>
                  <span>
                    <FormattedMessage
                      id="employer.info"
                      defaultMessage="인사담당자 정보"
                    />
                  </span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                
                <th>
                  <span>
                    <FormattedMessage
                      id="modification"
                      defaultMessage="편집"
                    />
                  </span>
                </th>
                <th>
                  <span>
                    <FormattedMessage
                      id="deletion"
                      defaultMessage="삭제"
                    />
                  </span>
                </th>
              </tr>
            </thead>
            <tbody>
              <tr className="noData" style={{display: 'none'}}>
                <th colSpan={9}>No data available in table</th>
              </tr>
              {currentPosts(listState).map((list, idx)=>(
              <tr>
                <td>{idx+1}</td>
                <td>{list.bizNm}</td>
                <td>
                  {list.mbrNmStr}
                </td>
                <td>
                  <Link to="/admin/corporate-edit"
                    state={{ sDtlCont: list.bizNm }}>
                    <FormattedMessage
                      id="editing"
                      defaultMessage="수정"
                    />
                  </Link>
                </td>
                <td>
                  <button type="button" className="rowDeleteButton" onClick={() => setDel(list.bizNm)}>
                  <FormattedMessage
                      id="deletion"
                      defaultMessage="삭제"
                    />
                  </button>
                </td>
              </tr>
              ))}
            </tbody>
          </table>
        </article>
        
        <Pagination postsPerPage={postsPerPage} totalPosts={listState.length} paginate={setCurrentPage} currentPage={currentPage}/>
      </section>
    </main>
  )
}