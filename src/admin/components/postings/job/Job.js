import {Link, useLocation, useNavigate} from 'react-router-dom';
import { useContext, useEffect, useRef, useState } from 'react';
import { Context } from 'contexts';
import axios from 'axios';
import fileDownload from 'js-file-download';

import { axiosCrudOnSubmit } from 'commons/modules/commonUtil';
import { Pagination } from 'commons/components/pagination/Pagination';
import { DownloadBtns } from 'commons/components/download-btn/DownloadBtns';
import { snglFileUpload } from 'commons/modules/multipartUtil';
import * as defVal  from 'commons/modules/defVal';
import { AddressInput } from 'commons/components/AddressInput';
import { ImagePreview } from 'commons/components/inputs/image-preview/ImagePreview';
import { UPD_MBRUID } from 'contexts/actionTypes';
import { FormattedMessage } from 'react-intl';

export const Job =()=>{
  
  const [listState, setListState] = useState([]);
  const [sConts, setSConts] = useState("");
  const [fltrState, setFltrState] = useState({});
  const componentRef = useRef();
  const moment = require('moment');

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid")
    ,ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi")
  };

  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(10);

  const indexOfLast = currentPage * postsPerPage;
  const indexOfFirst = indexOfLast - postsPerPage;

  let arraySimb = "<:;>";

  const {
    state : {
        mbr,
        server
    },
    dispatch,
  } = useContext(Context);

  /*
  const headers = [
    { label: "기업명", key: "bizNm" },
    { label: "아이디", key: "mbrId" },
    { label: "담당자 이름", key: "mbrNm" },
    { label: "휴대폰", key: "celpNum" },
    { label: "이메일", key: "email" }
  ];
  */

  function currentPosts(tmp) {
      let currentPosts = 0;
      currentPosts = tmp.slice(indexOfFirst, indexOfLast);
      return currentPosts;
  }

  const changeFltr = (fltr) => {
    let tmpFltr = (fltr === "ASC") ? "DESC" :
                  (fltr === "DESC") ? "ASC" :
                  "ASC";

    return tmpFltr;
  }
  
  useEffect(async () => {
    console.log("Page First Load EFFECT");
    initSetHandle();      
    
  },[fltrState])

  async function initSetHandle() {
    let goUrl = server.path + "/mbr/mbrlist";
    let data = {
      ssnMbrUid : ssn.ssnMbrUid,
      ssnMbrDivi : ssn.ssnMbrDivi,
      mbrUid : mbr.mbrUid,
      mbrDivi : "MBRDIVI-01",
      sConts : sConts,
      sFltr : fltrState,
      sFltrCate : Object.keys(fltrState)[0],
      sFltrCont : Object.values(fltrState)[0],
      ifWthdr : "Y"
    }

    console.log("data.mbrDivi ==  " + data.mbrDivi);

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    setListState(axiosRes.data.mbrArr);      
  }

  useEffect( async () => {
    window.scrollTo(0,0);
  },[listState])

  return (  
    <main>
        <section className="tableSection">
          <div className="sectionHeader">
            <h3>
              <span className="material-icons-outlined">
                settings
              </span>
              <b>
                <FormattedMessage
                  id="posting.register"
                  defaultMessage="채용공고 등록하기"
                />
              </b>
            </h3></div>
          <div className="searchBar">
            <label htmlFor="search">Search:</label>
            <input type="search" id="search" 
              onChange={(e) => {
                setSConts(e.target.value);
            }}/>
            <button type="button" onClick={initSetHandle}>
              <FormattedMessage
                id="search"
                defaultMessage="검색"
              />
            </button>
          </div>
          <article>
            <table>
              <thead>
                <tr>
                  <th>
                    <span>
                      <FormattedMessage
                        id="number"
                        defaultMessage="번호"
                      />
                    </span>
                  </th>
                  <th className={ fltrState.bizNm === "DESC" ? "descending" :
                                fltrState.bizNm === "ASC" ? "ascending" :
                                undefined } 
                    onClick={() => {
                      setFltrState({
                        bizNm : changeFltr(fltrState.bizNm)
                      }); 
                  }}>
                    <span>
                      <FormattedMessage
                        id="company.name"
                        defaultMessage="기업명"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </th>
                  <th className={ fltrState.mbrId === "DESC" ? "descending" :
                                fltrState.mbrId === "ASC" ? "ascending" :
                                undefined } 
                    onClick={() => {
                      setFltrState({
                        mbrId : changeFltr(fltrState.mbrId)
                      }); 
                  }}>
                    <span>
                      <FormattedMessage
                        id="id"
                        defaultMessage="아이디"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </th>
                  <th className={ fltrState.mbrNm === "DESC" ? "descending" :
                                fltrState.mbrNm === "ASC" ? "ascending" :
                                undefined } 
                    onClick={() => {
                      setFltrState({
                        mbrNm : changeFltr(fltrState.mbrNm)
                      }); 
                  }}>
                    <span>
                      <FormattedMessage
                        id="manager.name"
                        defaultMessage="담당자 이름"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span className="none" />
                    </div>
                  </th>
                  <th className={ fltrState.celpNum === "DESC" ? "descending" :
                                fltrState.celpNum === "ASC" ? "ascending" :
                                undefined } 
                    onClick={() => {
                      setFltrState({
                        celpNum : changeFltr(fltrState.celpNum)
                      }); 
                  }}>
                    <span>
                      <FormattedMessage
                        id="phone"
                        defaultMessage="휴대폰"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </th>
                  <th className={ fltrState.email === "DESC" ? "descending" :
                                fltrState.email === "ASC" ? "ascending" :
                                undefined } 
                    onClick={() => {
                      setFltrState({
                        email : changeFltr(fltrState.email)
                      }); 
                  }}>
                    <span>
                      <FormattedMessage
                        id="email"
                        defaultMessage="이메일"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </th>
                  <th>
                    <span>
                      <FormattedMessage
                        id="posting.registration"
                        defaultMessage="채용공고 등록"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody>
                {currentPosts(listState).map((list, idx)=>(
                  <tr key={list.mbrUid}>
                    <td>{indexOfFirst + idx + 1}</td>
                    <td>{list.bizNm}</td>
                    <td>{list.mbrId}</td>
                    <td>{list.mbrNm}</td>
                    <td>{list.celpNum}</td>
                    <td>{list.email}</td>
                    <td>
                      <Link to='/admin/job-register' className='buttonLink' 
                        state={{ mbrUid: list.mbrUid, bizNm: list.bizNm }}>
                        <FormattedMessage
                          id="posting.registration"
                          defaultMessage="채용공고 등록"
                        /> &gt;</Link>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </article>
          
          <Pagination postsPerPage={postsPerPage} totalPosts={listState.length} paginate={setCurrentPage} currentPage={currentPage}/>

        </section>
      </main>
  );
}