import {Link, useLocation, useNavigate} from 'react-router-dom';
import { useContext, useEffect, useState } from 'react';
import { Context } from 'contexts';
import { toast, ToastContainer } from "react-toastify";
import axios from 'axios';
import fileDownload from 'js-file-download';
import Modal from "modal-all/ModalAll";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import { RecommendProgram } from "modal-all/modals/RecommendProgram";

import { axiosCrudOnSubmit } from 'commons/modules/commonUtil';
import * as commonUtil from 'commons/modules/commonUtil';
import { snglFileUpload } from 'commons/modules/multipartUtil';
import * as defVal  from 'commons/modules/defVal';
import { AddressInput } from 'commons/components/AddressInput';
import { ImagePreview } from 'commons/components/inputs/image-preview/ImagePreview';
import { TagTemplate } from "commons/components/tags/TagTemplate";
import BusinessTypeTemplate from "commons/components/inputs/business-type/BusinessTypeTemplate";
import WorkPlaceTemplate from "commons/components/inputs/work-place/WorkPlaceTemplate";
import WorkPositionTemplate from "commons/components/inputs/work-position/WorkPositionTemplate";
import { UPD_MBRUID } from "contexts/actionTypes";

import EditorComponent from "../../editor/EditorComponent";
import { FormattedMessage, useIntl } from 'react-intl';
import { messages } from 'lang/DefineMessages';

export const JobDetail = () => {
  const intl = useIntl();
  
  let PROC_CHK = ""

  const {
    state : {
        server,
        mbr
    },
    dispatch,
  } = useContext(Context);

  const location = useLocation();
  const navigate = useNavigate();
  const moment = require('moment');
  let previewWindow = "";
  let previewImg = document.createElement("img");  

  const [files, setFiles] = useState("");
  const [axiosFilesRes, setAxiosFilesRes] = useState("");
  
  const [mbrFilesRes, setMbrFilesRes] = useState([]);
  const [bizFilesRes, setBizFilesRes] = useState([]);
  const [extraFilesRes, setExtraFilesRes] = useState([]);

  const bizKndCd = defVal.BizKndCd();
  const salesSizeCd = defVal.SalesSizeCd();
  const empSizeCd = defVal.EmpSizeCd();
  const hireSizeCd = defVal.HireSizeCd();
  const bizGuidDivi = defVal.BizGuidDivi();
  const bizGuidKnd = defVal.BizGuidKnd();
  
  const [bizCate, setBizCate] = useState([]);
  const [empAddrCate, setEmpAddrCate] = useState([]);
  const [workPosCate, setWorkPosCate] = useState([]);

  const [bizCateCd, setBizCateCd] = useState([]); const [bizCateCdCnt, setBizCateCdCnt] = useState([]);
  const [empAddrCateCd, setEmpAddrCateCd] = useState([]); const [empAddrCateCdCnt, setEmpAddrCateCdCnt] = useState([]);
  const [workPosCateCd, setWorkPosCateCd] = useState([]); const [workPosCateCdCnt, setWorkPosCateCdCnt] = useState([]);
  const [bizGuidListState, setBizGuidListState] = useState([]);
  const [guid01Cnt, setGuid01Cnt] = useState(0);
  const [guid02Cnt, setGuid02Cnt] = useState(0);
  const [detailState, setDetailState] = useState(defVal.setAxiosJobState(null));

  const [modalVisible1, setModalVisible1] = useState(false);
  const [titleLength, setTitleLength] = useState('0');
  const [untilDone, setUntilDone] = useState(false);
  const [tagCnt, setTagCnt] = useState(0);

  const [bizCateCdCombo, setBizCateCdCombo] = useState([defVal.SelCateCdCombo()]);
  const [empAddrCateCdCombo, setEmpAddrCateCdCombo] = useState([defVal.SelCateCdCombo()]);
  const [workPosCateCdCombo, setWorkPosCateCdCombo] = useState([defVal.SelCateCdCombo()]);
  

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid")
    ,ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi")
  };

  let locationState = {
    mbrUid : location.state.mbrUid,
    mbrDivi : "MBRDIVI-01",
    bizNm : location.state.bizNm
  }
  
  useEffect(async () => {
      console.log("Page First Load EFFECT");
      initSetHandle();          
  },[])

  async function initSetHandle() {
    
    detailState.ssnMbrUid = ssn.ssnMbrUid;
    
    let goUrl = server.path + "/hire/lsthiredetail"
    
    let data = {
      ssnMbrUid : ssn.ssnMbrUid,
      ssnMbrDivi : ssn.ssnMbrDivi,
      mbrUid : locationState.mbrUid,
      mbrDivi : locationState.mbrDivi
    }

    //상세정보 가저오기
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    var tmpDetail = defVal.setAxiosJobState(axiosRes.data.resInfo);

    tmpDetail.title="";
    tmpDetail.hireSize="0";
    tmpDetail.hireKnd="";
    tmpDetail.rcmndAutoEndYn="Y";
    tmpDetail.rcmndPrgMailYn="Y";    
    tmpDetail.intro="";
    tmpDetail.intro2="";
    tmpDetail.salaryMin="0";
    tmpDetail.salaryMax="0";
    tmpDetail.srvcFeePctMin="0";
    tmpDetail.srvcFeePctMax="0";
    tmpDetail.srvcFeeFixYn="N";
    tmpDetail.srvcWrtyPeriod="1";
    tmpDetail.srvcWrtyMthd="";
    tmpDetail.emailPubYn="N";
    tmpDetail.snsPubYn="N";
    tmpDetail.phNumPubYn="N";
    tmpDetail.celpNumPubYn="N";
    tmpDetail.addrPubYn="N";
    
    
    

    setDetailState(tmpDetail);

    
    
    //await getBizCateCdList();
    //await getEmpAddrCateCdList();
    //await getWorkPosCateCdList();

    setBizCateCd(axiosRes.data.resBizCateCdArr);
    setEmpAddrCateCd(axiosRes.data.empAddrCateCdArr);
    setWorkPosCateCd(axiosRes.data.workPosCateCdArr);

    setBizCate(axiosRes.data.bizCateArr);
    setEmpAddrCate(axiosRes.data.empAddrCateArr);
    setWorkPosCate(axiosRes.data.workPosCateArr);
    
    //일반모집요강
    var tmpBizGuidList = await getBizGuidList(locationState.mbrUid);
  };

  const getBizGuidList = async (mbrUid) => {
    let goUrl = server.path + "/corporate/bizguidlist";
    let data = {
      mbrUid: mbrUid,
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    let tmpArr = axiosRes.data.resArr;    

    for(var k=0; k<tmpArr.length; k++)
    {
      if( tmpArr[k].thmnl != "")
      {
        let fileList = JSON.parse(tmpArr[k].atchFiles);
        tmpArr[k].orignlFileNm = fileList[0].orignlFileNm;
      }

      if( tmpArr[k].brdDivi != "02" )setGuid01Cnt(1);
      else setGuid02Cnt(1);
    }

    setBizGuidListState(tmpArr);
    return tmpArr;
  };

  useEffect(() => {
    if (bizCate && bizCate.length > 0) {
      var tmpBizCateCdList = bizCateCd;

      //업종구분 가저오기
      var tmpBizCateList = bizCate;

      console.log("4 tmpBizCateList  == " + bizCateCd.length);
      console.log("4 tmpBizCateList  == " + tmpBizCateList.length);

      //업종구분 combo추가하기
      for (var k = 1; k < tmpBizCateList.length; k++) addNoRenerBizCate();
      for (var k = 0; k < tmpBizCateList.length; k++) {
        detailState.bizCateCd1[k] = tmpBizCateList[k].cateCd1;
        detailState.bizCateCd2[k] = tmpBizCateList[k].cateCd2;
        detailState.bizCateCd3[k] = tmpBizCateList[k].cateCd3;

        var childCateList2 = getBizCateChildFilter(
          tmpBizCateCdList,
          tmpBizCateList[k].cateCd1,
          1
        );
        bizCateCdCombo[k].cateCdList2 = childCateList2;

        var childCateList3 = getBizCateChildFilter(
          tmpBizCateCdList,
          tmpBizCateList[k].cateCd2,
          2
        );
        bizCateCdCombo[k].cateCdList3 = childCateList3;
      }

      reRender();
    }
  }, [bizCate]);

  const addNoRenerBizCate = () => {
    var data = defVal.SelCateCdCombo();
    bizCateCdCombo.push(data);

    if (!detailState.bizCateCd1 || detailState.bizCateCd1.length == 0) {
      detailState.bizCateCd1.push("NONE");
      detailState.bizCateCd2.push("NONE");
      detailState.bizCateCd3.push("NONE");
    }

    detailState.bizCateCd1.push("NONE");
    detailState.bizCateCd2.push("NONE");
    detailState.bizCateCd3.push("NONE");
  };

  const getBizCateChildFilter = (cateList, val, lvl) => {
    var chkLvl = lvl + 1;

    const cateArray = [];
    for (var k = 0; k < cateList.length; k++) {
      if (cateList[k].lvl == chkLvl && cateList[k].upCd == val) {
        cateArray.push({
          lvl: cateList[k].lvl,
          cateCd: cateList[k].cateCd,
          cdName: cateList[k].cdName,
        });
      }
    }

    return cateArray;
  };

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


  useEffect(() => {
    if (empAddrCate && empAddrCate.length > 0) {
      //근무지역 가저오기
      var tmpEmpAddrCateCdList = empAddrCateCd;
      var tmpEmpAddrCateList = empAddrCate;

      //근무지역 combo추가하기
      for (var k = 1; k < tmpEmpAddrCateList.length; k++)
        addNoRenerEmpAddrCate();
      for (var k = 0; k < tmpEmpAddrCateList.length; k++) {
        detailState.empAddrCateCd1[k] = tmpEmpAddrCateList[k].cateCd1;
        detailState.empAddrCateCd2[k] = tmpEmpAddrCateList[k].cateCd2;

        var childCateList2 = getEmpAddrCateChildFilter(
          tmpEmpAddrCateCdList,
          tmpEmpAddrCateList[k].cateCd1,
          1
        );
        empAddrCateCdCombo[k].cateCdList2 = childCateList2;
      }

      reRender();
    }
  }, [empAddrCate]);


  const addNoRenerEmpAddrCate = () => {
    var data = defVal.SelCateCdCombo();
    empAddrCateCdCombo.push(data);

    if (detailState.empAddrCateCd1.length == 0) {
      detailState.empAddrCateCd1.push("NONE");
      detailState.empAddrCateCd2.push("NONE");
    }

    detailState.empAddrCateCd1.push("NONE");
    detailState.empAddrCateCd2.push("NONE");
  };


  const getEmpAddrCateChildFilter = (cateList, val, lvl) => {
    var chkLvl = lvl + 1;

    const cateArray = [];
    for (var k = 0; k < cateList.length; k++) {
      if (cateList[k].lvl == chkLvl && cateList[k].upCd == val) {
        cateArray.push({
          lvl: cateList[k].lvl,
          cateCd: cateList[k].cateCd,
          cdName: cateList[k].cdName,
        });
      }
    }

    return cateArray;
  };


  const getBizCateCdList = async () => {
    let goUrl = server.path + "/hireBizCateCd";
    let data = {};    
    let axiosCateRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    setBizCateCd(axiosCateRes.data.hireCateArr);
  }

  useEffect(() => {
    if(bizCateCd && bizCateCd.length > 0){        
    }
  }, [bizCateCd]);

  useEffect(() => {
    if(bizCateCdCnt && bizCateCdCnt.length > 0){        
    }
  }, [bizCateCdCnt]);

  const getEmpAddrCateCdList = async () => {
    let goUrl = server.path + "/empAddrCateCd";
    let data = {};
    
    let axiosCateRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    setEmpAddrCateCd(axiosCateRes.data.empCateArr);
  };

  useEffect(() => {
    if(empAddrCateCd && empAddrCateCd.length > 0){        
    }
  }, [empAddrCateCd]);

  const getWorkPosCateCdList = async () => {
    let goUrl = server.path + "/workPosCateCd";
    let data = {};
    
    let axiosCateRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    setWorkPosCateCd(axiosCateRes.data.workPosCateCdArr);
  };

  useEffect(() => {
    if(workPosCateCd && workPosCateCd.length > 0){        
    }
  }, [workPosCateCd]);

  const reRender = () => {
    setDetailState({
      ...detailState,
      initSetCmplt: true
    })      
  };

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  const addBizCate = () => {  
    
    if( bizCateCdCombo.length == 5 )
    {
      alert(intl.formatMessage({
        id: "job.type.max5",
        defaultMessage: "업종구분은 5개까지 입력 가능합니다!"
      }));
      return false;
    }

    var data = defVal.SelCateCdCombo();
    bizCateCdCombo.push(data); 

    if( detailState.bizCateCd1.length == 0 )
    {
      detailState.bizCateCd1.push("NONE");
      detailState.bizCateCd2.push("NONE");   
      detailState.bizCateCd3.push("NONE");
    }

    detailState.bizCateCd1.push("NONE");
    detailState.bizCateCd2.push("NONE");   
    detailState.bizCateCd3.push("NONE");
    
    reRender();
  }

  
  const rmvBizCate = (rowidx) => {  
    
    var tmpList = commonUtil.rmvArrIdx(bizCateCdCombo, rowidx);
    setBizCateCdCombo(tmpList);
   
    var tmpList1 = commonUtil.rmvArrIdx(detailState.bizCateCd1, rowidx); 
    var tmpList2 = commonUtil.rmvArrIdx(detailState.bizCateCd2, rowidx); 
    var tmpList3 = commonUtil.rmvArrIdx(detailState.bizCateCd3, rowidx); 
    
    setDetailState({
      ...detailState,
      bizCateCd1: tmpList1,
      bizCateCd2: tmpList2,
      bizCateCd3: tmpList3
    })
    
       
  }


  const chngBizCate = (rowidx, val, lvl) => {  
    var chkLvl = lvl + 1;
        
    var tmpList1 = detailState.bizCateCd1; 
    var tmpList2 = detailState.bizCateCd2; 
    var tmpList3 = detailState.bizCateCd3; 

    var tmpComboList1 = bizCateCdCombo[rowidx].cateCdList1;
    var tmpComboList2 = bizCateCdCombo[rowidx].cateCdList2; 
    var tmpComboList3 = bizCateCdCombo[rowidx].cateCdList3; 

    
    if( lvl <= 1){
      tmpList2[rowidx] = "";
      tmpComboList2 = "";
    };
    if( lvl <= 2)
    {
      tmpList3[rowidx] = "";
      tmpComboList3 = "";
    }
    
    if( lvl == 1 )tmpList1[rowidx] = val;
    else if( lvl == 2 )tmpList2[rowidx] = val;
    else if( lvl == 3 )tmpList3[rowidx] = val;
    
    const cateArray=[];
    for(var k=0; k<bizCateCd.length; k++)
    {
      if( bizCateCd[k].lvl == chkLvl && bizCateCd[k].upCd == val)
      {
        cateArray.push({lvl: bizCateCd[k].lvl, cateCd: bizCateCd[k].cateCd, cdName: bizCateCd[k].cdName});
      }
    }    

    if( chkLvl == 2) 
    {
      tmpComboList2 = cateArray;
      bizCateCdCombo[rowidx].cateCdList2 = tmpComboList2;
    }
    else if( chkLvl == 3) 
    {
      tmpComboList3 = cateArray; 
      bizCateCdCombo[rowidx].cateCdList3 = tmpComboList3; 
    }
    
    setDetailState({
      ...detailState,
      bizCateCd1: tmpList1,
      bizCateCd2: tmpList2,
      bizCateCd3: tmpList3
    })
    
    
  } 
  
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  const addEmpAddrCate = () => {  
    
    if( empAddrCateCdCombo.length == 5 )
    {
      alert(intl.formatMessage({
        id: "workplace.max5",
        defaultMessage: "근무예정지는 5개까지 입력 가능합니다!"
      }));
      return false;
    }

    var data = defVal.SelCateCdCombo();
    empAddrCateCdCombo.push(data); 

    if( detailState.empAddrCateCd1.length == 0 )
    {
      detailState.empAddrCateCd1.push("NONE");
      detailState.empAddrCateCd2.push("NONE");
    }

    detailState.empAddrCateCd1.push("NONE");
    detailState.empAddrCateCd2.push("NONE");  
    
    reRender();
  }

  const rmvEmpAddrCate = (rowidx) => {  
    
    var tmpList = commonUtil.rmvArrIdx(empAddrCateCdCombo, rowidx);
    setEmpAddrCateCdCombo(tmpList);
   
    var tmpList1 = commonUtil.rmvArrIdx(detailState.empAddrCateCd1, rowidx); 
    var tmpList2 = commonUtil.rmvArrIdx(detailState.empAddrCateCd2, rowidx); 
    
    setDetailState({
      ...detailState,
      empAddrCateCd1: tmpList1,
      empAddrCateCd2: tmpList2
    })   
    
  }
  

  const chngEmpAddrCate = (rowidx, val, lvl) => {  

    var chkLvl = lvl + 1;
        
    var tmpList1 = detailState.empAddrCateCd1; 
    var tmpList2 = detailState.empAddrCateCd2; 

    var tmpComboList1 = empAddrCateCdCombo[rowidx].cateCdList1;
    var tmpComboList2 = empAddrCateCdCombo[rowidx].cateCdList2; 
    
    if( lvl <= 1){
      tmpList2[rowidx] = "";
      tmpComboList2 = "";
    };
        
    if( lvl == 1 )tmpList1[rowidx] = val;
    else if( lvl == 2 )tmpList2[rowidx] = val;
   
    const cateArray=[];
    for(var k=0; k<empAddrCateCd.length; k++)
    {
      if( empAddrCateCd[k].lvl == chkLvl && empAddrCateCd[k].upCd == val)
      {
        cateArray.push({lvl: empAddrCateCd[k].lvl, cateCd: empAddrCateCd[k].cateCd, cdName: empAddrCateCd[k].cdName});
      }
    }    

    if( chkLvl == 2) 
    {
      tmpComboList2 = cateArray;
      empAddrCateCdCombo[rowidx].cateCdList2 = tmpComboList2;
    }
    
    setDetailState({
      ...detailState,
      empAddrCateCd1: tmpList1,
      empAddrCateCd2: tmpList2
    })
    
  } 

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  const addWorkPosCate = () => {  
    
    if( workPosCateCdCombo.length == 5 )
    {
      alert(intl.formatMessage({
        id: "workplace.max5",
        defaultMessage: "직종별구분은 5개까지 입력 가능합니다!"
      }));
      return false;
    }

    var data = defVal.SelCateCdCombo();
    workPosCateCdCombo.push(data); 

    if( detailState.workPosCateCd1.length == 0 )
    {
      detailState.workPosCateCd1.push("NONE");
      detailState.workPosCateCd2.push("NONE");   
      detailState.workPosCateCd3.push("NONE");
    }

    detailState.workPosCateCd1.push("NONE");
    detailState.workPosCateCd2.push("NONE");   
    detailState.workPosCateCd3.push("NONE");
    
    reRender();    
    
  }

  const rmvWorkPosCate = (rowidx) => {  
    
    var tmpList = commonUtil.rmvArrIdx(workPosCateCdCombo, rowidx);
    setWorkPosCateCdCombo(tmpList);
   
    var tmpList1 = commonUtil.rmvArrIdx(detailState.workPosCateCd1, rowidx); 
    var tmpList2 = commonUtil.rmvArrIdx(detailState.workPosCateCd2, rowidx); 
    var tmpList3 = commonUtil.rmvArrIdx(detailState.workPosCateCd3, rowidx); 
    
    setDetailState({
      ...detailState,
      workPosCateCd1: tmpList1,
      workPosCateCd2: tmpList2,
      workPosCateCd3: tmpList3
    })
    
  }


  const chngWorkPosCate = (rowidx, val, lvl) => {  
    
    var chkLvl = lvl + 1;
        
    var tmpList1 = detailState.workPosCateCd1; 
    var tmpList2 = detailState.workPosCateCd2; 
    var tmpList3 = detailState.workPosCateCd3; 

    var tmpComboList1 = workPosCateCdCombo[rowidx].cateCdList1;
    var tmpComboList2 = workPosCateCdCombo[rowidx].cateCdList2; 
    var tmpComboList3 = workPosCateCdCombo[rowidx].cateCdList3; 

    
    if( lvl <= 1){
      tmpList2[rowidx] = "";
      tmpComboList2 = "";
    };
    if( lvl <= 2)
    {
      tmpList3[rowidx] = "";
      tmpComboList3 = "";
    }
    
    if( lvl == 1 )tmpList1[rowidx] = val;
    else if( lvl == 2 )tmpList2[rowidx] = val;
    else if( lvl == 3 )tmpList3[rowidx] = val;
    
    const cateArray=[];
    for(var k=0; k<workPosCateCd.length; k++)
    {
      if( workPosCateCd[k].lvl == chkLvl && workPosCateCd[k].upCd == val)
      {
        cateArray.push({lvl: workPosCateCd[k].lvl, cateCd: workPosCateCd[k].cateCd, cdName: workPosCateCd[k].cdName});
      }
    }    

    if( chkLvl == 2) 
    {
      tmpComboList2 = cateArray;
      workPosCateCdCombo[rowidx].cateCdList2 = tmpComboList2;
    }
    else if( chkLvl == 3) 
    {
      tmpComboList3 = cateArray; 
      workPosCateCdCombo[rowidx].cateCdList3 = tmpComboList3; 
    }
    
    setDetailState({
      ...detailState,
      workPosCateCd1: tmpList1,
      workPosCateCd2: tmpList2,
      workPosCateCd3: tmpList3
    })
    
    
  } 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  const addTag =(e) => {
    if(e.key == "Enter")
    {
      if(detailState.tags.length == 29){
        alert(intl.formatMessage({
          id: "input.max30",
          defaultMessage: "30개까지 입력 가능합니다!"
        }));
        detailState.tagCntnt = "";
        return false;
      }
            
      var duplYn = "N";
      
      for(var k=0; k<detailState.tags.length; k++)
      {
        if( detailState.tags[k] == e.target.value)
        {
          duplYn = "Y";
          break;
        }
      }
      
      if( duplYn == "N")detailState.tags.push(e.target.value);      
      detailState.tagCntnt = "";
      reRender();
    }

    return false;
  }

  const rmvTag = (idx) => {
    var tmpList = commonUtil.rmvArrIdx(detailState.tags, idx);
    setDetailState({
      ...detailState,
      tags : tmpList
    });    
  }




  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  const years = defVal.YearCnt();
  const months = [1,2,3,4,5,6,7,8,9,10,11,12];

  let hours = [];
  let srvcFeePctMinList = [""];
  let srvcFeePctMaxList = [""];

  for(let i = 0; i <= 23; i++)
  {
    if(i >= 10) hours.push( i );
    else hours.push( "0" + i );
  }

  for(let i = 0; i <= 32; i++)
  {
    srvcFeePctMinList.push( 3 + i );
    srvcFeePctMaxList.push( 3 + i );
  }

  const date = new Date();
 
  

  const openModal1 = () => {
    setModalVisible1(true)
  }

  const closeModal1 = () => {
    setModalVisible1(false)
  }

  const [formChecked, setFormChekced] = useState(false);
  const radioChangeHandler = event => {
    const target = event.target
    const value = target.value

    if (value === "cmpnyForm"){
      setFormChekced(true);
    } else {
      setFormChekced(false);
    }

    let tmpList = detailState.uploadFiles;
    tmpList[3] = "disable";

    let tmpJsonStrList = detailState.snglImgFileJsonStr;
    tmpJsonStrList[3] = "";

    setDetailState({
      ...detailState,
      resumeForm: value,
      uploadFiles: tmpList,
      snglImgFileJsonStr: tmpJsonStrList,
      resumeFormThmnl: "",
      resumeFormThmnlNm : "파일 이름"                            
    })
  }

  const isUntilDoneClicked = () => {
    setUntilDone(!untilDone);

    if(!untilDone)
    {
      setDetailState({
        ...detailState,
        rcmndEndYear : '2222'
      })
    } else {
      setDetailState({
        ...detailState,
        rcmndEndYear : date.getFullYear().toString()
      })
    }
  };

  const isSrvcFeeFixYnChecked = (e) => {
    setDetailState({
      ...detailState,
      srvcFeePctMin : "",
      srvcFeePctMax : "",
      srvcFeeFixYn : (e.target.checked) ? "Y" : "N"
    });
  };

  

  const daysInMonth = (month, year) => {
    let tmpList = [];

    for(let i = 1; i <= new Date(year, month ,0).getDate(); i++)
    {
      tmpList.push(i);
    }
    
    return tmpList;
  }

  const [days, setDays] = useState( daysInMonth( 1, date.getFullYear() ) );

  

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  

  async function goFileUpload() {
    let fileStorePath = "Globals.fileStoreHirePath"; //로컬저장경로
    let fileLinkPath = "Globals.fileStoreHirePath"; //link경로
    let filePrefix = "hire"; //파일코드

    PROC_CHK = "FILEUPLOAD";

    let uploadRes = await snglFileUpload( server.path, fileStorePath, fileLinkPath, filePrefix, detailState.uploadFiles, detailState.filePosNm);

    //console.log("FILE UPLOAD RESULT : ");
    //console.log(uploadRes);

    if(uploadRes === 0) cntntsCRUD();
    else jsonCompResult(uploadRes.data);
  }

  
  useEffect(()=>{
    if(detailState.crudStateSetCmplt)
    {
      goFileUpload();

      //cntntsCRUD();
    }
  },[detailState.crudStateSetCmplt])

  /////////////////////////////////////////////////////////////////////////////////////////////

  const onChangeVal = (e, key) => {

    if(key == "hireKnd" )
    {
      var hireKnd = "";
      if( document.getElementById("permanent").checked == true )hireKnd="fullTime";
      if( document.getElementById("contract").checked == true )
      {
        if( hireKnd == "" )hireKnd="cntrct";
        else hireKnd=hireKnd+","+"cntrct";
      }

      setDetailState({
        ...detailState,
        [key]: hireKnd
      });
    }
    else
    {

    //

      setDetailState({
        ...detailState,
        [key]: e.target.value
      });
    } 
  }

  ///////////////////////////////////////////////////////////////////////////////////////////
  const jsonCompResult = ( resJson ) => {
    console.log("PROC_CHK : " + PROC_CHK);
    
    if( resJson.resCd !== "0000" )
    {
      toast.error(intl.formatMessage({
        id: "error.during.save",
        defaultMessage: "저장 중 오류가 발생했습니다."
      }));
      return;
    }
    else
    {
      if( PROC_CHK === "INS" )
      {
        toast.info(intl.formatMessage({
          id: "info.recruit.posting.added",
          defaultMessage: "채용 공고가 등록되었습니다."
        }));
        let tmpPath = (detailState.hireStat === "tmp") ? "temporary" : "confirm";
        navigate("/admin/" + tmpPath, { mbrUid : location.state.mbrUid });

        return;
      }
      else if( PROC_CHK === "FILEUPLOAD" )
      {
        let jsonStr = JSON.stringify(resJson.resFileList);
        let fileList = JSON.parse(jsonStr);
        let filePos = 0;
        let textAreaCnt = detailState.snglImgFileJsonStr.length;
        let tmpList = detailState.snglImgFileJsonStr;

        for(let i = 0; i < fileList.length; i++)
        {
          filePos = fileList[i].filePos;
          detailState.snglImgFileJsonStr[filePos] = JSON.stringify( fileList[i] );          
        }

        cntntsCRUD();
        
      }
    }
  }

  const goCRUD = async (hireStat) => {
    
    if( !detailState.rcmndEndYear) detailState.rcmndEndYear = "2022";
    if( !detailState.rcmndEndMonth) detailState.rcmndEndMonth = "1";
    if( !detailState.rcmndEndDay) detailState.rcmndEndDay = "1";
    if( !detailState.rcmndEndHour) detailState.rcmndEndHour = "00";

    if(detailState.rcmndEndYear == "" )detailState.rcmndEndYear = "2022";
    if(detailState.rcmndEndYear == "2222" )detailState.rcmndEndYear = "2022";
    if(detailState.rcmndEndMonth == "" )detailState.rcmndEndMonth = "1";
    if(detailState.rcmndEndDay == "" )detailState.rcmndEndMonth = "1";
    if(detailState.rcmndEndHour == "" ) detailState.rcmndEndHour = "00";

    if( detailState.rcmndUntilDon == "Y")
    {
      detailState.rcmndEndYear = "9999";
      detailState.rcmndEndMonth = "12";
      detailState.rcmndEndDay = "31";
      detailState.rcmndEndHour = "00";
    }

    let tmpRcmndEndMonth = (detailState.rcmndEndMonth < 10) ? "0" + detailState.rcmndEndMonth : detailState.rcmndEndMonth;
    let tmpRcmndEndDay = ( parseInt(detailState.rcmndEndDay) < 10 ) ? "0" + detailState.rcmndEndDay : detailState.rcmndEndDay;
    let tmpRcmndEndDt = detailState.rcmndEndYear + "-" + tmpRcmndEndMonth + "-" +  tmpRcmndEndDay;
    let tmpRcmndEndDttm = detailState.rcmndEndYear + tmpRcmndEndMonth + tmpRcmndEndDay + detailState.rcmndEndHour + "0000";

    if( commonUtil.CheckIsEmptyFromVal(detailState.bizKnd, 
      intl.formatMessage({
        id: "error.need.input.business.type",
        defaultMessage: "기업형태를 입력해주세요."
      })) == false ) return;
    if( commonUtil.CheckIsEmptyFromVal(detailState.saleSize, 
      intl.formatMessage({
        id: "error.need.input.sale.size",
        defaultMessage: "매출규모를 입력해주세요."
      })) == false ) return;
    if( commonUtil.CheckIsEmptyFromVal(detailState.empSize, 
      intl.formatMessage({
        id: "error.need.input.workers",
        defaultMessage: "종업원수를 입력해주세요."
      })) == false ) return;
    if( commonUtil.CheckIsEmptyFromVal(detailState.bizCateCd1, 
      intl.formatMessage({
        id: "error.need.input.job.type",
        defaultMessage: "업종구분을 입력해주세요."
      })) == false ) return;
    if( commonUtil.CheckIsEmptyFromVal(detailState.bizCateCd2, 
      intl.formatMessage({
        id: "error.need.input.job.type",
        defaultMessage: "업종구분을 입력해주세요."
      })) == false ) return;
    if( commonUtil.CheckIsEmptyFromVal(detailState.bizCateCd3, 
      intl.formatMessage({
        id: "error.need.input.job.type",
        defaultMessage: "업종구분을 입력해주세요."
      })) == false ) return;
    if( commonUtil.CheckIsEmptyFromVal(detailState.empAddrCateCd1, 
      intl.formatMessage({
        id: "error.need.input.workplace",
        defaultMessage: "근무예정지를 입력해주세요."
      })) == false ) return;
    if( commonUtil.CheckIsEmptyFromVal(detailState.empAddrCateCd2, 
      intl.formatMessage({
        id: "error.need.input.workplace",
        defaultMessage: "근무예정지를 입력해주세요."
      })) == false ) return;
    if( commonUtil.CheckIsEmptyFromVal(detailState.title, 
      intl.formatMessage({
        id: "error.need.input.posting.title",
        defaultMessage: "채용공고 제목을 입력해주세요."
      })) == false ) return;
    if( commonUtil.CheckIsEmptyFromVal(detailState.workPosCateCd1, 
      intl.formatMessage({
        id: "error.need.input.work.category",
        defaultMessage: "직종별구분을 입력해주세요."
      })) == false ) return;
    if( commonUtil.CheckIsEmptyFromVal(detailState.workPosCateCd2, 
      intl.formatMessage({
        id: "error.need.input.work.category",
        defaultMessage: "직종별구분을 입력해주세요."
      })) == false ) return;

    if( commonUtil.getToday() >= tmpRcmndEndDt)
    {
      toast.error(intl.formatMessage(messages.deadline, {
        today: commonUtil.getToday()
      }));
      return;
    }

    if( commonUtil.CheckIsEmptyFromVal(detailState.ckData, 
      intl.formatMessage({
        id: "error.need.input.normal.recruitment",
        defaultMessage: "일반 모집요강 내용을 입력해주세요."
      })) == false ) return;

    if( commonUtil.CheckIsEmptyFromVal(detailState.tags, 
      intl.formatMessage({
        id: "error.need.input.searching.keyword",
        defaultMessage: "인재써칭 핵심 키워드를 입력해주세요."
      })) == false ) return;

    if( parseInt(detailState.salaryMax) < parseInt(detailState.salaryMin) )
    {
      toast.error(intl.formatMessage({
        id: "error.max.price.little.than.min",
        defaultMessage: "예상연봉 최대 금액은 최소 금액보다 커야 합니다."
      }))
      
      return;
      
    } 

    detailState.mbrUid = locationState.mbrUid;
    detailState.rcmndEndDttm = tmpRcmndEndDttm;
    detailState.srvcFeePctMin = (detailState.srvcFeeFixYn === "Y") ? "" : detailState.srvcFeePctMin;
    detailState.srvcFeePctMax = (detailState.srvcFeeFixYn === "Y") ? "" : detailState.srvcFeePctMax;
    detailState.hireStat = hireStat;

    let tmpList = detailState.tags;

    let fileStorePath = "Globals.fileStoreHirePath"; //로컬저장경로
    let fileLinkPath = "Globals.fileLinkHirePath"; //link경로
    let filePrefix = "hire"; //파일코드

    PROC_CHK = "FILEUPLOAD";

    let uploadRes = await snglFileUpload( server.path, fileStorePath, fileLinkPath, filePrefix, detailState.uploadFiles, detailState.filePosNm);
    

    if(uploadRes === 0) cntntsCRUD();
    else jsonCompResult(uploadRes.data);
  }

  const cntntsCRUD = async () => {
    PROC_CHK = 'INS';
      
    let goUrl = server.path + "/hire/hireinsert";
    let axiosRes = await axiosCrudOnSubmit(detailState, goUrl, "POST");
    
    jsonCompResult(axiosRes.data);
  }


  /////////////////////////////////////////////////////////////////////////////////////////////////////////////
 const handleDownload = (filePath, orignlFileNm) => {
  
    if (filePath != "") {
      let goUrl = server.host + filePath;
      
      //alert("goUrl == " + goUrl);
      
      axios
        .get(goUrl, {
          responseType: "blob",
        })
        .then((res) => {
          fileDownload(res.data, orignlFileNm);
        });
    } else {
      
    }

  }

  return (
    <main>
      <ToastContainer />
      <section className="formSection">
        <div className="formHeader">
          <h1>
            <FormattedMessage
              id="posting.manage"
              defaultMessage="공고관리"
            />
            <em>
              <FormattedMessage
                id="recruit.manage"
                defaultMessage="채용공고 관리하기"
              />
            </em>
          </h1>
          <strong>{locationState.bizNm}</strong>
        </div>
        <article>
          <form>
            <h2>
              <FormattedMessage
                id="company.info"
                defaultMessage="기업정보"
              />
            </h2>
            <div className="formWrap">
              <ul className="twoCol">
                <li>
                  <span className="labelSpan">
                    <FormattedMessage
                      id="company.name.open"
                      defaultMessage="기업명 공개"
                    />
                  </span>
                  <div className="radioInput">
                    <input type="radio" id="agree" name="disposal" value="Y" 
                      checked={(detailState.pubYn == "Y") ? true:false} 
                      onChange={ (e) => {
                        setDetailState({
                          ...detailState,
                          pubYn: e.target.value
                        })
                      }}/>
                    <label htmlFor="agree">
                      <FormattedMessage
                        id="yes"
                        defaultMessage="예"
                      />
                    </label>
                    <input type="radio" id="disagree" name="disposal" value="N" 
                      checked={(detailState.pubYn == "N") ? true:false} 
                      onChange={ (e) => {
                        setDetailState({
                          ...detailState,
                          pubYn: e.target.value
                        })
                      }}/>
                    <label htmlFor="disagree">
                      <FormattedMessage
                        id="no"
                        defaultMessage="아니오"
                      />
                    </label>
                  </div>
                </li>
                <li>
                  <label htmlFor="businessForm">
                    <FormattedMessage
                      id="company.type"
                      defaultMessage="기업형태"
                    />
                  </label>
                  <select name="businessForm" id="businessForm"
                    value={detailState.bizKnd}
                    onChange={(e)=>{
                      setDetailState({
                        ...detailState,
                        bizKnd: e.target.value
                      })
                    }}>
                    <option value="">
                      --
                        {intl.formatMessage({
                          id: "select",
                          defaultMessage: "선택하세요"
                        })}
                      --
                    </option>
                    {bizKndCd.map((type, idx) => (                        
                      <option value={type.cateCd}>{type.cdName}</option>
                    ))}   
                  </select>
                </li>
              </ul>
              <ul className="twoCol">
                <li>
                  <label htmlFor="sales">
                    <FormattedMessage
                      id="company.sales"
                      defaultMessage="매출규모"
                    />
                  </label>
                  <select name="sales" id="sales"
                    value={detailState.saleSize}
                    onChange={(e)=>{
                      setDetailState({
                        ...detailState,
                        saleSize: e.target.value
                      })
                    }}>
                    <option value="">
                      --
                        {intl.formatMessage({
                          id: "select",
                          defaultMessage: "선택하세요"
                        })}
                      --
                    </option>
                    {salesSizeCd.map((type, idx) => (                        
                      <option value={type.cateCd}>{type.cdName}</option>
                    ))}   
                  </select>
                </li>
                <li>
                  <label htmlFor="employee">
                    <FormattedMessage
                      id="company.workers"
                      defaultMessage="종업원 수"
                    />
                  </label>
                  <select name="employee" id="employee" 
                    value={detailState.empSize}
                    onChange={(e)=>{
                      setDetailState({
                        ...detailState,
                        empSize: e.target.value
                      });
                    }}>
                    <option value="">
                      --
                        {intl.formatMessage({
                          id: "select",
                          defaultMessage: "선택하세요"
                        })}
                      --
                    </option>
                    {empSizeCd.map((type, idx) => (                        
                      <option value={type.cateCd}>{type.cdName}</option>
                    ))}  
                  </select>
                </li>
              </ul>
              <ul className="oneCol">
                <li>
                  <div className="adminFlexColumn">                    
                    {
                      bizCateCdCombo.map((type, rowidx) => (                        
                        <div style={{display: bizCateCdCombo[rowidx].display}}>
                          <span htmlFor="businessType1-1" className="labelSpan">{(rowidx === 0 ) 
                          ? (`${intl.formatMessage({
                            id: "company.business.type",
                            defaultMessage: "업종구분"
                          })}`) 
                          :('')}</span>
                          <select name="businessType1-1" id="businessType1-1"
                            value={detailState.bizCateCd1[rowidx]}
                            onChange={(e) => {
                              chngBizCate(rowidx, e.target.value, 1);
                            }}>
                            <option value="">
                              --
                                {intl.formatMessage({
                                  id: "select",
                                  defaultMessage: "선택하세요"
                                })}
                              --
                            </option>
                            {bizCateCd.map((type1, idx1) => (                        
                                (type1.lvl == 1) ? (
                                  <option value={type1.cateCd}>{type1.cdName}</option>
                                ):('')
                            ))}                        
                          </select>
                          <select name="businessType1-1" id="businessType1-1"
                            value={detailState.bizCateCd2[rowidx]}
                            onChange={(e) => {
                              chngBizCate(rowidx, e.target.value, 2);
                            }}>
                            <option value="">
                              --
                                {intl.formatMessage({
                                  id: "select",
                                  defaultMessage: "선택하세요"
                                })}
                              --
                            </option>
                            {
                              (bizCateCdCombo[rowidx].cateCdList2.length > 0)?
                              (
                              bizCateCdCombo[rowidx].cateCdList2.map((type2, idx2) => 
                                  (
                                    <option value={type2.cateCd}>{type2.cdName}</option>
                                  )
                                ) 
                              ): ('')
                            }                                                                              
                          </select>
                          <select name="businessType1-1" id="businessType1-1"
                            value={detailState.bizCateCd3[rowidx]}
                            onChange={(e) => {
                              chngBizCate(rowidx, e.target.value, 3);
                            }}>
                            <option value="">
                              --
                                {intl.formatMessage({
                                  id: "select",
                                  defaultMessage: "선택하세요"
                                })}
                              --
                            </option>
                            {
                              (bizCateCdCombo[rowidx].cateCdList3.length > 0)?
                              (
                              bizCateCdCombo[rowidx].cateCdList3.map((type3, idx3) => 
                                  (
                                    <option value={type3.cateCd}>{type3.cdName}</option>
                                  )
                                ) 
                              ): ('')
                            }        
                          </select>
                          {
                            (rowidx == 0 ) ? (<button type="button" className="formButton" onClick={() => {addBizCate();}}>
                              <FormattedMessage
                                id="add"
                                defaultMessage="추가하기"
                              /> &gt;
                            </button>) :
                            (<button type="button" className="formButton" onClick={() => {rmvBizCate(rowidx);}}>
                              <FormattedMessage
                                id="delete"
                                defaultMessage="삭제하기"
                              /> &gt;
                            </button>)
                          }                          
                        </div>
                    ))}  
                  </div>
                </li>
              </ul>
              <ul className="oneCol">
                <li>
                  <div className="adminFlexColumn">
                    {
                      empAddrCateCdCombo.map((type, rowidx) => ( 
                        <div style={{display: empAddrCateCdCombo[rowidx].display}}>
                          <span htmlFor="businessType1-1" className="labelSpan">{(rowidx == 0 ) 
                          ? ((`${intl.formatMessage({
                              id: "company.workplace",
                              defaultMessage: "근무예정지"
                            })}`)) 
                          :('')}</span>
                          <select name="businessType1-1" id="businessType1-1"
                            value={detailState.empAddrCateCd1[rowidx]}
                            onChange={(e) => {
                              chngEmpAddrCate(rowidx, e.target.value, 1);
                            }}>
                            <option value="">
                              --
                                {intl.formatMessage({
                                  id: "select",
                                  defaultMessage: "선택하세요"
                                })}
                              --
                            </option>
                            {empAddrCateCd.map((type1, idx1) => (                        
                                (type1.lvl == 1) ? (
                                  <option value={type1.cateCd}>{type1.cdName}</option>
                                ):('')
                            ))}                        
                          </select>
                          <select name="businessType1-1" id="businessType1-1"
                            value={detailState.empAddrCateCd2[rowidx]}
                            onChange={(e) => {
                              chngEmpAddrCate(rowidx, e.target.value, 2);
                            }}>
                            <option value="">
                              --
                                {intl.formatMessage({
                                  id: "select",
                                  defaultMessage: "선택하세요"
                                })}
                              --
                            </option>
                            {
                              (empAddrCateCdCombo[rowidx].cateCdList2.length > 0)?
                                (
                                  empAddrCateCdCombo[rowidx].cateCdList2.map((type2, idx2) => 
                                    (
                                      <option value={type2.cateCd}>{type2.cdName}</option>
                                    )
                                  ) 
                                ): ('')
                            }                                                                              
                          </select>
                          {
                            (rowidx == 0 ) ? (<button type="button" className="formButton" onClick={() => {addEmpAddrCate();}}>
                              <FormattedMessage
                                id="add"
                                defaultMessage="추가하기"
                              /> &gt;
                            </button>) :
                            (<button type="button" className="formButton" onClick={() => {rmvEmpAddrCate(rowidx);}}>
                              <FormattedMessage
                                id="delete"
                                defaultMessage="삭제하기"
                              /> &gt;
                            </button>)
                          }     
                        </div>                      
                      ))                      
                    }                 
                  </div>
                </li>
              </ul>
            </div>
            <h2>
              <FormattedMessage
                id="recruitment.content"
                defaultMessage="모집내용"
              />
            </h2>
            <div className="formWrap">
              <ul className="oneCol">
                <li>
                  <label htmlFor="postingTitle">
                    <FormattedMessage
                      id="recruitment.title"
                      defaultMessage="채용공고 제목"
                    />
                  </label>
                  <input type="text" id="postingTitle" className="fullWidth" maxLength='50' 
                    value={detailState.title}
                    onChange={(e)=>{
                      setDetailState({
                        ...detailState,
                        title : e.target.value
                      });
                      setTitleLength(e.target.value.length);
                    }}/>
                  <em>{titleLength} / 50</em>
                </li>
              </ul>
              <ul className="oneCol">
                <li>
                  <div className="flexColmn">
                    {
                      workPosCateCdCombo.map((type, rowidx) => (                        
                        <div style={{display: workPosCateCdCombo[rowidx].display}}>
                          <span htmlFor="businessType1-1" className="labelSpan">{(rowidx == 0 ) 
                          ? ((`${intl.formatMessage({
                              id: "job.type",
                              defaultMessage: "직종"
                            })}`)) :('')}</span>
                          <select name="businessType1-1" id="businessType1-1"
                            value={detailState.workPosCateCd1[rowidx]}
                            onChange={(e) => {
                              chngWorkPosCate(rowidx, e.target.value, 1);
                            }}>
                            <option value="">
                              --
                                {intl.formatMessage({
                                  id: "select",
                                  defaultMessage: "선택하세요"
                                })}
                              --
                            </option>
                            {workPosCateCd.map((type1, idx1) => (                        
                              (type1.lvl == 1) ? (
                                <option value={type1.cateCd}>{type1.cdName}</option>
                              ):('')
                            ))}                        
                          </select>
                          <select name="businessType1-1" id="businessType1-1"
                            value={detailState.workPosCateCd2[rowidx]}
                            onChange={(e) => {
                              chngWorkPosCate(rowidx, e.target.value, 2);
                            }}>
                            <option value="">
                              --
                                {intl.formatMessage({
                                  id: "select",
                                  defaultMessage: "선택하세요"
                                })}
                              --
                            </option>
                            {
                              (workPosCateCdCombo[rowidx].cateCdList2.length > 0)?
                                (
                                  workPosCateCdCombo[rowidx].cateCdList2.map((type2, idx2) => 
                                    (
                                      <option value={type2.cateCd}>{type2.cdName}</option>
                                    )
                                  ) 
                                ): ('')
                            }                                                                              
                          </select>                          
                          {
                            (rowidx == 0 ) 
                            ? (<button type="button" className="formButton" onClick={() => {addWorkPosCate();}}>
                              <FormattedMessage
                                id="add"
                                defaultMessage="추가하기"
                              /> &gt;
                            </button>) :
                            (<button type="button" className="formButton" onClick={() => {rmvWorkPosCate(rowidx);}}>
                              <FormattedMessage
                                id="delete"
                                defaultMessage="삭제하기"
                              /> &gt;
                            </button>)
                          }                
                        </div>
                    ))}  
                  </div>
                </li>
              </ul>
              <ul className="twoCol">
                <li>
                  <label htmlFor="numberofPeople">
                    <FormattedMessage
                      id="recruitment.people.number"
                      defaultMessage="모집인원"
                    />
                  </label>
                  <select name="numberofPeople" id="numberofPeople"
                    value={detailState.hireSize}
                    onChange={(e)=>{
                      setDetailState({
                        ...detailState,
                        hireSize: e.target.value
                      })
                    }}>
                    {hireSizeCd.map((type, idx) => (                        
                      <option value={type.cateCd}>{type.cdName}</option>
                    ))}   
                  </select>
                </li>
                <li>
                  <span className="labelSpan">
                    <FormattedMessage
                      id="recruitment.employment.form"
                      defaultMessage="고용형태"
                    />
                  </span>
                  <div className="radioInput">
                    <input type="checkbox" id="permanent" name="permanent" value="fullTime" 
                      checked={(commonUtil.indexOf(detailState.hireKnd, "fullTime") > -1) ? true:false} 
                      
                      onChange={ (e) => {
                        onChangeVal(e, "hireKnd");
                      }}                      
                      />
                    <label htmlFor="permanent">
                      <FormattedMessage
                        id="recruitment.employment.permanent"
                        defaultMessage="정규직"
                      />
                    </label>
                                        
                    <input type="checkbox" id="contract" name="contract" value="cntrct" 
                      checked={(commonUtil.indexOf(detailState.hireKnd, "cntrct") > -1) ? true:false} 
                      onChange={ (e) => {
                        onChangeVal(e, "hireKnd");
                      }}
                      />
                    <label htmlFor="contract">
                      <FormattedMessage
                        id="recruitment.employment.contract"
                        defaultMessage="계약직"
                      />
                    </label>
                  </div>
                </li>
              </ul>
              <ul className="oneCol">
                <li>
                  <span className="labelSpan">
                    <FormattedMessage
                      id="recruitment.deadline"
                      defaultMessage="인재추천 마감일"
                    />
                  </span>
                  <div>
                    <select name="deadlineYear" 
                      value={detailState.rcmndEndYear}
                      disabled={ (untilDone)? true:false }
                      onChange={(e)=>{
                        setDetailState({
                          ...detailState,
                          rcmndEndYear : e.target.value
                        });

                        daysInMonth(detailState.rcmndEndMonth, e.target.value);
                      }}>
                      {years.map((year, idx) => (
                        <option key={year} value={year}>
                          {year}{intl.formatMessage({
                              id: "year",
                              defaultMessage: "년"
                            })}
                        </option>  
                      ))}
                    </select>
                    <select name="deadlineMonth" className="shortWidth"
                      value={detailState.rcmndEndMonth}
                      disabled={ (untilDone)? true:false }
                      onChange={(e)=>{
                        setDetailState({
                          ...detailState,
                          rcmndEndMonth : e.target.value
                        });

                        setDays(daysInMonth(e.target.value, detailState.rcmndEndYear));}}>
                      {months.map((month, idx) => (
                        <option key={month} value={month}>
                          {month}{intl.formatMessage({
                            id: "month",
                            defaultMessage: "월"
                          })}
                        </option>  
                      ))}
                    </select>
                    <select name="deadlineDay" className="shortWidth"
                      value={detailState.rcmndEndDay}
                      disabled={ (untilDone)? true:false }
                      onChange={(e)=>{
                        setDetailState({
                          ...detailState,
                          rcmndEndDay : e.target.value
                        });}}>
                      {days.map((day, idx) => (
                        <option key={day} value={day}>
                          {day}{intl.formatMessage({
                            id: "date.day",
                            defaultMessage: "일"
                          })}
                        </option>  
                      ))}
                    </select>
                    <select name="deadlineTime" className="shortWidth"
                      value={detailState.rcmndEndHour}
                      disabled={ (untilDone)? true:false }
                      onChange={(e)=>{
                        setDetailState({
                          ...detailState,
                          rcmndEndHour : e.target.value
                        });}}>
                      {hours.map((hour, idx) => (
                        <option key={hour} value={hour}>
                          {hour}{intl.formatMessage({
                            id: "hour",
                            defaultMessage: "시"
                          })}
                        </option>  
                      ))}
                    </select>
                    <input type="checkbox" id="untilDone" 
                      onClick={isUntilDoneClicked}/>
                    <label htmlFor="untilDone">
                      <FormattedMessage
                        id="recruitment.or.until.hired"
                        defaultMessage="또는 채용시까지"
                      />
                    </label>
                  </div>
                </li>
              </ul>
              <ul className="twoCol">
                <li>
                  <span className="labelSpan">
                    <FormattedMessage
                      id="recruitment.recommend.auto.close"
                      defaultMessage="인재추천 자동마감"
                    />
                  </span>
                  <div className="radioInput">
                    <input type="radio" id="close" name="autoClose"
                      value="Y" 
                      checked={ (detailState.rcmndAutoEndYn === "Y") ? true:false }  
                      onChange={ (e) => {
                        setDetailState({
                          ...detailState,
                          rcmndAutoEndYn: e.target.value
                        })
                      }}/>
                    <label htmlFor="close">
                      <FormattedMessage
                        id="yes"
                        defaultMessage="예"
                      />
                    </label>
                    <input type="radio" id="open" name="autoClose"
                      value="N" 
                      checked={ (detailState.rcmndAutoEndYn === "N") ? true:false }  
                      onChange={ (e) => {
                        setDetailState({
                          ...detailState,
                          rcmndAutoEndYn: e.target.value
                        })
                      }}/>
                    <label htmlFor="open">
                      <FormattedMessage
                        id="no"
                        defaultMessage="아니오"
                      />
                    </label>
                  </div>
                </li>
                <li>
                  <span className="labelSpan">
                    <FormattedMessage
                      id="recruitment.progress.mailing"
                      defaultMessage="진행과정 메일링"
                    />
                  </span>
                  <div className="radioInput">
                    <input type="radio" id="mailAgree" name="progressMailing"
                      value="Y" 
                      checked={ (detailState.rcmndPrgMailYn === "Y") ? true:false }  
                      onChange={ (e) => {
                        setDetailState({
                          ...detailState,
                          rcmndPrgMailYn: e.target.value
                        })
                      }}/>
                    <label htmlFor="mailAgree">
                      <FormattedMessage
                        id="mail.agree"
                        defaultMessage="수신"
                      />
                    </label>
                    <input type="radio" id="mailDisagree" name="progressMailing" 
                      value="N" 
                      checked={ (detailState.rcmndPrgMailYn === "N") ? true:false }  
                      onChange={ (e) => {
                        setDetailState({
                          ...detailState,
                          rcmndPrgMailYn: e.target.value
                        })
                      }}/>
                    <label htmlFor="mailDisagree">
                      <FormattedMessage
                        id="mail.disagree"
                        defaultMessage="미수신"
                      />
                    </label>
                  </div>
                </li>
              </ul>
            </div>
            <h2>
              <FormattedMessage
                id="normal.application.guideline"
                defaultMessage="일반 모집요강"
              />
            </h2>
            
            <article style={{display:(guid01Cnt == 0)? 'none':'' }}>
              <table>
                <thead>
                  <tr>
                    <th>
                      <FormattedMessage
                        id="application.guideline"
                        defaultMessage="모집요강"
                      />
                    </th>
                    <th>
                      <FormattedMessage id="prologue" defaultMessage="머릿말" />
                    </th>
                    <th>
                      <FormattedMessage id="content" defaultMessage="내용" />
                    </th>
                    <th>
                      <FormattedMessage
                        id="file.attached"
                        defaultMessage="첨부파일"
                      />
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {bizGuidListState.map((type, idx) =>
                    type.brdDivi !== "02" ? (
                      <tr>
                        <td>
                          {bizGuidDivi.map((type1, idx1) =>
                            type1.cateCd === type.brdDivi ? type1.cdName : ""
                          )}
                        </td>
                        <td>
                          {bizGuidKnd.map((type2, idx2) =>
                            type2.cateCd === type.brdKnd ? type2.cdName : ""
                          )}
                        </td>
                        <td>{type.cntnt}</td>
                        <td>
                          <span>{type.orignlFileNm}</span>
                            {
                              type.thmnl != '' ? 
                              (
                                <button type="button" className="basicButton" style={{marginLeft:'10px', display:(type.thmnl == '')? 'none' : ''}}
                                onClick={() => handleDownload(type.thmnl, type.orignlFileNm)}>
                                  <FormattedMessage
                                    id="file.download"
                                    defaultMessage="파일다운"
                                  />
                                </button>  
                              ) : ("")
                            }
                        </td>
                      </tr>
                    ) : (
                      ""
                    )
                  )}
                </tbody>
              </table>
            </article>

            <div className="fileBoxWrap">
              <div className="fileBox">
                <div>
                  <span className="fileName">{detailState.thmnlNm}</span>
                  <label htmlFor="fileInput1">
                    <FormattedMessage
                      id="file.attachment"
                      defaultMessage="파일 첨부"
                    /> &gt;
                  </label>
                  <input type="file" id="fileInput1" className="fileInput" 
                    onChange={(e) => {
                      let tmpList = detailState.uploadFiles;
                      tmpList[1] = e.target.files[0];

                      let tmpJsonStrList = detailState.snglImgFileJsonStr;
                      tmpJsonStrList[1] = "";
                      
                      setDetailState({
                        ...detailState,
                        uploadFiles: tmpList,
                        snglImgFileJsonStr: tmpJsonStrList,
                        thmnl: "",
                        thmnlNm : e.target.files[0].name
                        }
                      )
                    }}/>
                </div>
              </div>
            </div>
            
            <div className="formWrap">
              <CKEditor
                editor={ClassicEditor}
                data={detailState.intro}
                onChange={(event, editor) => {
                  detailState.intro = editor.getData();
                }}
              />
              
            </div>
            
            <h2>
              <FormattedMessage
                id="detail.application.guideline"
                defaultMessage="상세 모집요강"
              /> <em>(
                <FormattedMessage
                  id="detail.application.warning"
                  defaultMessage="상세 모집요강은 선택된 리크루터에게만 공개됩니다."
                />)</em>
            </h2>

            <article style={{display:(guid02Cnt == 0)? 'none':'' }}>
              <table>
                <thead>
                  <tr>
                    <th>
                      <FormattedMessage
                        id="application.guideline"
                        defaultMessage="모집요강"
                      />
                    </th>
                    <th>
                      <FormattedMessage id="prologue" defaultMessage="머릿말" />
                    </th>
                    <th>
                      <FormattedMessage id="content" defaultMessage="내용" />
                    </th>
                    <th>
                      <FormattedMessage
                        id="file.attached"
                        defaultMessage="첨부파일"
                      />
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {bizGuidListState.map((type, idx) =>
                    type.brdDivi === "02" ? (
                      <tr>
                        <td>
                          {bizGuidDivi.map((type1, idx1) =>
                            type1.cateCd === type.brdDivi ? type1.cdName : ""
                          )}
                        </td>
                        <td>
                          {bizGuidKnd.map((type2, idx2) =>
                            type2.cateCd === type.brdKnd ? type2.cdName : ""
                          )}
                        </td>
                        <td>{type.cntnt}</td>
                        <td>
                          <span>{type.orignlFileNm}</span>
                            {
                              type.thmnl != '' ? 
                              (
                                <button type="button" className="basicButton" style={{marginLeft:'10px', display:(type.thmnl == '')? 'none' : ''}}
                                onClick={() => handleDownload(type.thmnl, type.orignlFileNm)}>
                                  <FormattedMessage
                                    id="file.download"
                                    defaultMessage="파일다운"
                                  />
                                </button>  
                              ) : ("")
                            }
                        </td>
                      </tr>
                    ) : (
                      ""
                    )
                  )}
                </tbody>
              </table>
            </article>

            <div className="fileUploadGuide">
              <p>
                <FormattedMessage
                  id="file.upload.guideline1"
                  defaultMessage="※ 파일은 1개만 업로드 가능합니다."
                />
              </p>
              <p>
                <FormattedMessage
                  id="file.upload.guideline2"
                  defaultMessage="※ 2개 이상의 파일을 업로드하고 싶다면, 압축 파일(.zip) 형태로 1개의 파일만 업로드해주세요. (최대 10MB까지 첨부 가능)"
                />
              </p>
            </div>

            <div className="fileBoxWrap">
              <div className="fileBox">
                <div>
                  <span className="fileName">{detailState.thmnlNm2}</span>
                  <label htmlFor="fileInput2">
                    <FormattedMessage
                      id="file.attachment"
                      defaultMessage="파일 첨부"
                    /> &gt;
                  </label>
                  <input type="file" id="fileInput2" className="fileInput" 
                    onChange={(e) => {
                      let tmpList = detailState.uploadFiles;
                      tmpList[2] = e.target.files[0];

                      let tmpJsonStrList = detailState.snglImgFileJsonStr;
                      tmpJsonStrList[2] = "";
                      
                      setDetailState({
                        ...detailState,
                        uploadFiles: tmpList,
                        snglImgFileJsonStr: tmpJsonStrList,
                        thmnl2: "",
                        thmnlNm2 : e.target.files[0].name                        
                      })
                    }}/>
                </div>
              </div>
            </div>
            
            <div className="formWrap">
              <div className="formWrapInputs">
                <div className="infoBox">
                  <FormattedMessage
                    id="detail.application.description"
                    defaultMessage="위크루트 매니저가 검수과정을 통해 별도로 입력합니다."
                  />
                </div>
              </div>
            </div>
            
            <h2>
              <FormattedMessage
                id="searching.keyword"
                defaultMessage="인재써칭 핵심 키워드"
              />
            </h2>
            <div className="formWrap">
              <ul className="oneCol">
                <div className="tags tagsInput">
                  <div className="tagsIn">
                    {detailState.tags.map((text, idx) => (
                      <span>{text}
                        <button 
                          className="delete" type="button">
                          <span className='material-icons' onClick={() => {rmvTag(idx)}}>close</span>
                        </button>
                      </span>  
                    ))}
                    
                    <input type="text" value={detailState.tagCntnt}  style={{border: 'none'}}
                      onChange={(e)=>{
                        setDetailState({
                          ...detailState,
                          tagCntnt : e.target.value
                        });
                      }}                        
                      
                      onKeyUp={(e) => {
                        addTag(e);
                      }}
                    />
                  </div>
                </div>
              </ul>
            </div>
            <h2>
              <FormattedMessage
                id="suggestion.setting"
                defaultMessage="제안 설정"
              />
            </h2>
            <div className="formWrap">
              <ul className="oneCol">
                <li>
                  <span className="labelSpan">
                    <FormattedMessage
                      id="suggestion.expected.salary"
                      defaultMessage="예상 연봉범위"
                    />
                  </span>
                  <div>
                    <span>
                      <FormattedMessage
                        id="minimum"
                        defaultMessage="최소"
                      />
                    </span>
                    <input type="text" value={detailState.salaryMin}
                      onChange={(e)=>{
                        setDetailState({
                          ...detailState,
                          salaryMin : e.target.value
                        });
                      }}
                      /> <FormattedMessage
                        id="currency"
                        defaultMessage="만원"
                      /> ~
                    <span>
                      <FormattedMessage
                        id="maximum"
                        defaultMessage="최대"
                      />
                    </span>
                    <input type="text" value={detailState.salaryMax}
                      onChange={(e)=>{
                        setDetailState({
                          ...detailState,
                          salaryMax : e.target.value
                        });
                      }}
                      /> <FormattedMessage
                        id="currency"
                        defaultMessage="만원"
                      />
                  </div>
                </li>
              </ul>
              <ul className="oneCol">
                <li>
                  <span className="labelSpan">
                    <FormattedMessage
                      id="suggestion.service.fee"
                      defaultMessage="서비스 수수료율"
                    />
                  </span>
                  <div>
                    <span>
                      <FormattedMessage
                        id="minimum"
                        defaultMessage="최소"
                      /> </span>
                    <select name="minCommision" className="shortWidth" 
                      disabled={ (detailState.srvcFeeFixYn === "Y")? true:false }
                      value={detailState.srvcFeePctMin}
                      onChange={(e)=>{
                        let targetVal = e.target.value;
                        
                        if( parseInt(e.target.value) > parseInt(detailState.srvcFeePctMax) )
                        {
                          toast.error(intl.formatMessage({
                            id: "error.min.fee.bigger.than.max",
                            defaultMessage: "최소 수수료율이 최대 수수료율보다 큽니다."
                          }))
                          targetVal = detailState.srvcFeePctMax;
                        }
                        
                        setDetailState({
                          ...detailState,
                          srvcFeePctMin : targetVal
                        });
                      }}>
                        {srvcFeePctMinList.map((srvcFeePctMin, idx) => {
                          if(srvcFeePctMin === "") { return (
                            <option key={srvcFeePctMin} value={srvcFeePctMin}>
                              {intl.formatMessage({
                                id: "suggestion.fixed.fee",
                                defaultMessage: "정액"
                              })}
                            </option>  
                          )} 
                          else {
                            return (
                              <option key={srvcFeePctMin} value={srvcFeePctMin}>{srvcFeePctMin}%</option>  
                            )
                          }
                        })}
                    </select>
                    <span> ~ <FormattedMessage
                        id="maximum"
                        defaultMessage="최대"
                      /> </span>
                    <select name="maxCommision" className="shortWidth" 
                      disabled={ (detailState.srvcFeeFixYn === "Y") ? true:false }
                      value={detailState.srvcFeePctMax}
                      onChange={(e)=>{
                        let targetVal = e.target.value;
                        
                        if( parseInt(e.target.value) < parseInt(detailState.srvcFeePctMin) )
                        {
                          toast.error(intl.formatMessage({
                            id: "error.max.fee.bigger.than.max",
                            defaultMessage: "최대 수수료율이 최소 수수료율보다 작습니다."
                          }))
                          targetVal = detailState.srvcFeePctMin;
                        }

                        setDetailState({
                          ...detailState,
                          srvcFeePctMax : targetVal
                        });
                      }}>
                      {srvcFeePctMinList.map((srvcFeePctMin, idx) => {
                        if(srvcFeePctMin == "") { return (
                          <option key={srvcFeePctMin} value={srvcFeePctMin}>
                            {intl.formatMessage({
                              id: "suggestion.fixed.fee",
                              defaultMessage: "정액"
                            })}
                          </option>  
                        ) } 
                        else {
                          return (
                            <option key={srvcFeePctMin} value={srvcFeePctMin}>{srvcFeePctMin}%</option>  
                          )
                        }
                      })}
                    </select>
                    <input type="checkbox" id="certainAmount" onClick={(e)=>{isSrvcFeeFixYnChecked(e);}}/>
                    <label htmlFor="certainAmount">
                      <FormattedMessage
                        id="suggestion.service.fee.warning"
                        defaultMessage="또는 정액 (부가세 별도입니다.)"
                      />
                    </label>
                  </div>
                </li>
              </ul>
              <ul className="oneCol">
                <li>
                  <label htmlFor="guaranteeCondition">
                    <FormattedMessage
                      id="suggestion.service.guarantee"
                      defaultMessage="서비스 보증조건"
                    />
                  </label>
                  <div>
                    <select name="guaranteeCondition" id="guaranteeCondition" className="shortWidth" 
                      value={detailState.srvcWrtyPeriod}
                      onChange={(e)=>{
                        setDetailState({
                          ...detailState,
                          srvcWrtyPeriod : e.target.value
                        });
                      }}>
                      {months.map((month, idx) => (
                        <option key={month} value={month}>
                          {month}
                          {intl.formatMessage({
                            id: "months",
                            defaultMessage: "개월"
                          })}
                        </option>  
                      ))}
                    </select>
                    <span> <FormattedMessage
                        id="suggestion.incase.retire"
                        defaultMessage="이내 퇴직시"
                      /> </span>
                    <select name="guaranteeCondition" id="guaranteeCondition" className="shortWidth"
                      value={detailState.srvcWrtyMthd}
                      onChange={(e)=>{
                        setDetailState({
                          ...detailState,
                          srvcWrtyMthd : e.target.value
                        });
                      }}>
                      <option value="prorated">
                        {intl.formatMessage({
                          id: "suggestion.caculated.refund",
                          defaultMessage: "일할계산 환불"
                        })}
                      </option>
                      <option value="all">
                        {intl.formatMessage({
                          id: "suggestion.all.refund",
                          defaultMessage: "100% 전액 환불"
                        })}
                      </option>
                      <option value="etc">
                        {intl.formatMessage({
                          id: "etc",
                          defaultMessage: "기타"
                        })}
                      </option>
                    </select>
                  </div>
                </li>
              </ul>
              <ul className="oneCol">
                <li>
                  <span className="labelSpan">
                    <FormattedMessage
                      id="resume.format"
                      defaultMessage="이력서 양식"
                    />
                  </span>
                  <div>
                    <div className="radioInput resumeClickEvent">
                      <input type="radio" id="recruiterForm" name="resumeForm" value="recForm"
                        checked={!formChecked}
                        onChange={(e)=>{
                          radioChangeHandler(e);                          
                        }}/>
                      <label htmlFor="recruiterForm">
                        <FormattedMessage
                          id="resume.format.free"
                          defaultMessage="리크루터 자유 양식 이력서"
                        />
                      </label>
                      <input type="radio" id="companyForm" name="resumeForm" value="cmpnyForm" className="resumeUpload" 
                        checked={formChecked} 
                        onChange={(e)=>{
                          radioChangeHandler(e);
                        }}/>
                      <label htmlFor="companyForm">
                        <FormattedMessage
                          id="resume.format.compnay"
                          defaultMessage="회사 소정 양식 이력서"
                        />
                      </label>
                    </div>
                    {formChecked === true ?
                      <div className="adminFileBox inlineFileBox">
                        <div>
                          <label htmlFor="fileInput3">
                            <FormattedMessage
                              id="resume.file.attachment"
                              defaultMessage="이력서 양식 첨부"
                            /> &gt;
                          </label>
                          <span className="fileName">{detailState.resumeFormThmnlNm}</span>
                          <input type="file" id="fileInput3" className="fileInput" 
                            onChange={(e) => {
                              let tmpList = detailState.uploadFiles;
                              tmpList[3] = e.target.files[0];

                              let tmpJsonStrList = detailState.snglImgFileJsonStr;
                              tmpJsonStrList[3] = "";
                              
                              setDetailState({
                                ...detailState,
                                uploadFiles: tmpList,
                                snglImgFileJsonStr: tmpJsonStrList,
                                resumeFormThmnl: "",
                                resumeFormThmnlNm : e.target.files[0].name                                
                              })
                            }}/>
                        </div>
                      </div>
                    : null }
                  </div>
                </li>
              </ul>
            </div>
            <h2>
              <FormattedMessage
                id="setting.condition"
                defaultMessage="설정 조건"
              />
            </h2>
            <div className="formWrap">
              <ul className="oneCol">
                <li>
                  <span className="labelSpan">
                    <FormattedMessage
                      id="recommend.recruiter"
                      defaultMessage="우수 리크루터 추천"
                    />
                  </span>
                  <div>
                    <p>
                      <FormattedMessage
                        id="recommendation.wecruit.suggestion"
                        defaultMessage="위크루트가 추천하는 우수 리크루터 제안 받기"
                      />
                    </p>
                    <div className="radioInput">
                      <input type="radio" id="recommendAgree" name="recommendQuestion" 
                        checked = {(detailState.rcmndRecYn === "Y") ? true : false}
                        onChange={()=>{
                          setDetailState({
                            ...detailState,
                            rcmndRecYn: "Y"
                          });
                        }}/>
                      <label htmlFor="recommendAgree">
                        <FormattedMessage
                          id="no.disagree"
                          defaultMessage="예, 동의합니다."
                        />
                      </label>
                      <input type="radio" id="recommendDisagree" name="recommendQuestion" 
                        checked = {(detailState.rcmndRecYn === "N") ? true : false}
                        onChange={()=>{
                          setDetailState({
                            ...detailState,
                            rcmndRecYn: "N"
                          });
                        }}/>
                      <label htmlFor="recommendDisagree">
                        <FormattedMessage
                          id="no"
                          defaultMessage="아니오"
                        />
                      </label>
                    </div>
                    <button type="button" className="modalOpenButton" onClick={openModal1}>
                      <FormattedMessage
                        id="recommend.recruiter.program"
                        defaultMessage="우수 리크루터 추천 프로그램 자세히 보기"
                      />
                    </button>
                    {
                      <Modal visible={modalVisible1} closable={true} maskClosable={true} onClose={closeModal1} nobutton={true} >
                        <RecommendProgram />
                      </Modal>
                    }
                  </div>
                </li>
              </ul>
              <ul className="oneCol">
                <li>
                  <span className="labelSpan">
                    <FormattedMessage
                      id="recruit.method"
                      defaultMessage="리크루터 모집방법"
                    />
                  </span>
                  <div>
                    <div className="radioList">
                      <div>
                        <input type="radio" id="recruitMethod1" name="recruitMethod" 
                          checked = {(detailState.recMthd === "rec") ? true : false}
                          onChange={()=>{
                            setDetailState({
                              ...detailState,
                              recMthd: "rec"
                            });
                          }}/>
                        <label htmlFor="recruitMethod1">
                          <FormattedMessage
                            id="recruit.method1"
                            defaultMessage="위크루트에 등록된 리크루터만 참여 가능"
                          />
                        </label>
                      </div>
                      <div>
                        <input type="radio" id="recruitMethod2" name="recruitMethod" 
                          checked = {(detailState.recMthd === "recAndSrchfirm") ? true : false}
                          onChange={()=>{
                            setDetailState({
                              ...detailState,
                              recMthd: "recAndSrchfirm"
                            });
                          }}/>
                        <label htmlFor="recruitMethod2">
                          <FormattedMessage
                            id="recruit.method2"
                            defaultMessage="위크루트에 등록된 리크루터 + 기존 거래 써치펌 참여 가능"
                          />
                        </label>
                      </div>
                      <div>
                        <input type="radio" id="recruitMethod3" name="recruitMethod" 
                          checked = {(detailState.recMthd === "srchfirm") ? true : false}
                          onChange={()=>{
                            setDetailState({
                              ...detailState,
                              recMthd: "srchfirm"
                            });
                          }}/>
                        <label htmlFor="recruitMethod3">
                          <FormattedMessage
                            id="recruit.method3"
                            defaultMessage="기존 거래 써치펌만 참여 가능"
                          />
                        </label>
                      </div>
                    </div>
                  </div></li>
              </ul>
            </div>
            <h2>
              <FormattedMessage
                id="employer.contact.info.open"
                defaultMessage="인사담당자 연락처 공개 설정"
              />
              <em>
                <FormattedMessage
                  id="employer.contact.info.open.alert1"
                  defaultMessage="(연락처 공개 설정은 선택된 리크루터에게만 공개됩니다.)"
                />
              </em>
            </h2>
            <div className="formWrap">
              <ul className="oneCol">
                <li>
                  <div className="checkboxList">
                    <input type="checkbox" id="employerEmailDisposal" 
                      checked={(detailState.emailPubYn === "Y") ? true : false}
                      onChange={(e)=>{
                        setDetailState({
                          ...detailState,
                          emailPubYn: (e.target.checked) ? "Y" : "N"
                        });
                      }}/>
                    <label htmlFor="employerEmailDisposal">
                      <FormattedMessage
                        id="email"
                        defaultMessage="이메일"
                      />
                    </label>
                    <input type="checkbox" id="employerSnsDisposal" 
                      checked={(detailState.snsPubYn === "Y") ? true : false}
                      onChange={(e)=>{
                        setDetailState({
                          ...detailState,
                          snsPubYn: (e.target.checked) ? "Y" : "N"
                        });
                      }}/>
                    <label htmlFor="employerSnsDisposal">
                      <FormattedMessage
                        id="sns"
                        defaultMessage="SNS (라인, 카카오톡 등)"
                      />
                    </label>
                    <input type="checkbox" id="employerOfficeDisposal" 
                      checked={(detailState.phNumPubYn === "Y") ? true : false}
                      onChange={(e)=>{
                        setDetailState({
                          ...detailState,
                          phNumPubYn: (e.target.checked) ? "Y" : "N"
                        });
                      }}/>
                    <label htmlFor="employerOfficeDisposal">
                      <FormattedMessage
                        id="office.number"
                        defaultMessage="사무실 전화"
                      />
                    </label>
                    <input type="checkbox" id="employerPhoneDisposal" 
                      checked={(detailState.celpNumPubYn === "Y") ? true : false}
                      onChange={(e)=>{
                        setDetailState({
                          ...detailState,
                          celpNumPubYn: (e.target.checked) ? "Y" : "N"
                        });
                      }}/>
                    <label htmlFor="employerPhoneDisposal">
                      <FormattedMessage
                        id="phone.number"
                        defaultMessage="휴대폰 번호"
                      />
                    </label>
                    <input type="checkbox" id="employerAddressDisposal" 
                      checked={(detailState.addrPubYn === "Y") ? true : false}
                      onChange={(e)=>{
                        setDetailState({
                          ...detailState,
                          addrPubYn: (e.target.checked) ? "Y" : "N"
                        });
                      }}/>
                    <label htmlFor="employerAddressDisposal">
                      <FormattedMessage
                        id="address.open"
                        defaultMessage="주소공개 및 방문"
                      />
                    </label>
                    <p>
                      <FormattedMessage
                        id="employer.contact.info.open.alert2"
                        defaultMessage="* 위크루트는 Q&amp;A 게시판에 댓글을 남기시면 자동으로 담당자 업무용 이메일로 해당 내용이 발송됩니다."
                      />
                    </p>
                  </div>
                </li>
              </ul>
            </div>
            <div className="alignRight">
              <button type="button" className="formButton" onClick={() => {goCRUD("tmp");}}>
                <FormattedMessage
                  id="temp.save"
                  defaultMessage="임시저장"
                /> &gt;
              </button>
            </div>
            <div className="buttons">
              <button type="button" className="pointColoredBtn" onClick={() => {goCRUD("confirm");}}>
                <FormattedMessage
                  id="posting.register"
                  defaultMessage="채용공고 등록하기"
                />
              </button>
            </div>
          </form>
        </article>
      </section>
    </main>
  );
}