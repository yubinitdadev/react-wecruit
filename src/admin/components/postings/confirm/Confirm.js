import { DownloadBtns } from 'commons/components/download-btn/DownloadBtns';
import { Pagination } from 'commons/components/pagination/Pagination';
import { axiosCrudOnSubmit } from 'commons/modules/commonUtil';
import { Context } from 'contexts';
import { UPD_MBRUID } from 'contexts/actionTypes';
import { useContext, useEffect, useRef, useState } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import {Link} from 'react-router-dom';
import { ToastContainer } from 'react-toastify';

export const Confirm =()=>{
  const intl = useIntl();

  const {
    state : {
      server,
      mbr
    },
    dispatch,
  } = useContext(Context);
  
  const moment = require('moment');
  const componentRef = useRef();

  const [confirms, setConfirms] = useState([]);
  const [sConts, setSConts] = useState("");
  const [fltrState, setFltrState] = useState({});

  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(10);

  const indexOfLast = currentPage * postsPerPage;
  const indexOfFirst = indexOfLast - postsPerPage;

  function currentPosts(tmp) {
    let currentPosts = 0;
    currentPosts = tmp.slice(indexOfFirst, indexOfLast);
    return currentPosts;
  }

  async function goSrch() {
    let goUrl = server.path + "/hire/list"
    let data = {
      ssnMbrUid : mbr.mbrUid,
      mbrUid : mbr.mbrUid,
      mbrDivi : "MBRDIVI-01",
      hireStat : "confirming",
      sConts : sConts,
      sFltr : fltrState,
      sFltrCate : Object.keys(fltrState)[0],
      sFltrCont : Object.values(fltrState)[0],
    }

    initSetHandle(data);
  }

  const changeFltr = (fltr) => {
    let tmpFltr = (fltr === "ASC") ? "DESC" :
                  (fltr === "DESC") ? "ASC" :
                  "ASC";

    return tmpFltr;
  }

  useEffect(async() => {
    window.scrollTo(0,0);
    initSetHandle();
  }, [])

  const initSetHandle = async () => {
    let goUrl = server.path + "/hire/hirelist"
    
    let data = {
      hireStat : "confirming",
      sConts : sConts,
      sFltr : fltrState,
      sFltrCate : Object.keys(fltrState)[0],
      sFltrCont : Object.values(fltrState)[0],
    }

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    let axiosResData = axiosRes.data.hireArr;
    let tmpAxiosResData = [];

    axiosResData.forEach((resData, idx)=>{
      resData["rgstDttm"] = moment(resData.rgstDttm).format("YYYY년 MM월 DD일 HH시 mm분");
      
      if( resData["rcmndUntilDon"] == "Y" )resData["rcmndEndDttm"] = "채용시까지";
      else resData["rcmndEndDttm"] = moment(resData.rcmndEndDttm).format("YYYY년 MM월 DD일 HH시");

      if(resData["srvcFeePct"] == "Y")  resData["srvcFeePct"] = "정액제"
      else resData["srvcFeePct"] = Math.round(resData.srvcFeePct) + "%" 

      
      if(resData["hireStat"] == "ing") resData["hireStat"] = "검수완료"
      else if(resData["hireStat"] == "confirm") resData["hireStat"] = "검수중"
      else if(resData["hireStat"] == "declined") resData["hireStat"] = "거절"
      else resData["hireStat"] = "";
      

      tmpAxiosResData.push(resData);
    })

    setConfirms(tmpAxiosResData);
  }

  
  useEffect( () => {
    initSetHandle();
  }, [fltrState]);
  
  
  const headers = [
    { label: "채용공고일시", key: "rgstDttm" },
    { label: "기업명", key: "bizNm" },
    { label: "아이디", key: "mbrId" },
    { label: "담당자 이름", key: "mbrNm" },
    { label: "채용공고 제목", key: "title" },
    { label: "서비스 수수료율", key: "srvcFeePct" },
    { label: "채용마감 날짜", key: "rcmndEndDttm" },
    { label: "검수상태", key: "hireStat" },
  ];

  const onKeyUpSrch =(e) => {
    if(e.key === "Enter")
    {
      setCurrentPage(1);
      initSetHandle();   
    }
    return false;
  }

  return(
    <main>
      <ToastContainer />      
      <section className="tableSection">
        <div className="sectionHeader">
          <h3>
            <span className="material-icons-outlined">
              settings
            </span>
            <b>
              <FormattedMessage
                id="posting.confirm"
                defaultMessage="검수중 채용공고"
              />
            </b>
          </h3>
          <DownloadBtns headers={headers} dataset={confirms} fileName="검수중 채용공고" componentRef={componentRef}/>

        </div>
        <div className="searchBar">
          <label htmlFor="search">Search:</label>
          <input type="search" id="search" 
            onChange={(e) => {
              setSConts(e.target.value);
            }}
            
            onKeyUp={(e) => {onKeyUpSrch(e);}}
            />
          <button type="button" onClick={goSrch}>
            <FormattedMessage
              id="search"
              defaultMessage="검색"
            />
          </button>
        </div>
        <article id="capture" ref={componentRef}>
          <table>
            <thead>
              <tr>
                <th>
                  <span>번호</span>
                </th>
                <th className={ fltrState.rgstDttm === "DESC" ? "descending" :
                                fltrState.rgstDttm === "ASC" ? "ascending" :
                                undefined } 
                    onClick={() => {
                      setFltrState({
                        rgstDttm : changeFltr(fltrState.rgstDttm)
                      });
                    }}>
                  <div className='wrap'>
                    <span>
                      <FormattedMessage
                        id="posting.date"
                        defaultMessage="채용공고일시"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th className={ fltrState.bizNm === "DESC" ? "descending" :
                                fltrState.bizNm === "ASC" ? "ascending" :
                                undefined } 
                    onClick={() => {
                      setFltrState({
                        bizNm : changeFltr(fltrState.bizNm)
                      });
                    }}>
                  <div className='wrap'>
                    <span>
                      <FormattedMessage
                        id="company.name"
                        defaultMessage="기업명"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th className={ fltrState.mbrId === "DESC" ? "descending" :
                                fltrState.mbrId === "ASC" ? "ascending" :
                                undefined } 
                    onClick={() => {
                      setFltrState({
                        mbrId : changeFltr(fltrState.mbrId)
                      });
                    }}>
                  <div className='wrap'>
                    <span>
                      <FormattedMessage
                        id="id"
                        defaultMessage="아이디"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th className={ fltrState.mbrNm === "DESC" ? "descending" :
                                fltrState.mbrNm === "ASC" ? "ascending" :
                                undefined } 
                    onClick={() => {
                      setFltrState({
                        mbrNm : changeFltr(fltrState.mbrNm)
                      });
                    }}>
                  <div className='wrap'>
                    <span>
                      <FormattedMessage
                        id="manager.name"
                        defaultMessage="담당자 이름"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th className={ fltrState.title === "DESC" ? "descending" :
                                fltrState.title === "ASC" ? "ascending" :
                                undefined } 
                    onClick={() => {
                      setFltrState({
                        title : changeFltr(fltrState.title)
                      });
                    }}>
                  <div className='wrap'>
                    <span>
                      <FormattedMessage
                        id="recruitment.title"
                        defaultMessage="채용공고 제목"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th className={ fltrState.srvcFeePct === "DESC" ? "descending" :
                                fltrState.srvcFeePct === "ASC" ? "ascending" :
                                undefined } 
                    onClick={() => {
                      setFltrState({
                        srvcFeePct : changeFltr(fltrState.srvcFeePct)
                      });
                    }}>
                  <div className='wrap'>
                    <span>
                      <FormattedMessage
                        id="suggestion.service.fee"
                        defaultMessage="서비스 수수료율"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th className={ fltrState.rcmndEndDttm === "DESC" ? "descending" :
                                fltrState.rcmndEndDttm === "ASC" ? "ascending" :
                                undefined } 
                    onClick={() => {
                      setFltrState({
                        rcmndEndDttm : changeFltr(fltrState.rcmndEndDttm)
                      });
                    }}>
                  <div className='wrap'>
                    <span>
                      <FormattedMessage
                        id="recruit.deadline.date"
                        defaultMessage="채용마감 날짜"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
                <th className={ fltrState.hireStat === "DESC" ? "descending" :
                                fltrState.hireStat === "ASC" ? "ascending" :
                                undefined } 
                    onClick={() => {
                      setFltrState({
                        hireStat : changeFltr(fltrState.hireStat)
                      });
                    }}>
                  <div className='wrap'>
                    <span>
                      <FormattedMessage
                        id="inspection.status"
                        defaultMessage="검수상태"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </div>
                </th>
              </tr>
            </thead>
            <tbody>
              {currentPosts(confirms).map((confirm, idx)=>(
                <tr key={confirm.hireUid}>
                  <td>{indexOfFirst + idx + 1}</td>
                  <td>{confirm.rgstDttm}</td>
                  <td>{confirm.bizNm}</td>
                  <td>{confirm.mbrId}</td>
                  <td>{confirm.mbrNm}</td>
                  <td>
                  <Link to={(confirm.hireStat ==="검수중") ? '/admin/confirm-form' : "/admin/confirm-detail"} 
                    state={{ hireUid: confirm.hireUid, mbrUid: confirm.mbrUid }}>{confirm.title}</Link>
                  </td>
                  <td>{confirm.srvcFeePct}</td>
                  <td>{confirm.rcmndEndDttm}</td>
                  <td className="confirmStatus">
                    {
                      (confirm.hireStat ==="검수완료") 
                      ? ( <button type="button" className="confirmDone">
                        <FormattedMessage
                          id="inspection.done"
                          defaultMessage="검수완료"
                        />
                      </button> ) :
                      (confirm.hireStat ==="검수중") 
                      ? ( <button type="button" className="confirming">
                        <FormattedMessage
                          id="inspection.ing"
                          defaultMessage="검수중"
                        />
                      </button> ) :
                      (confirm.hireStat ==="거절") 
                      ? ( <button type="button" className="rejected">
                        <FormattedMessage
                          id="reject"
                          defaultMessage="거절"
                        />
                      </button> ) :
                      ( '' )
                    }
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </article>

        <Pagination postsPerPage={postsPerPage} totalPosts={confirms.length} paginate={setCurrentPage} currentPage={currentPage}/>

      </section>
    </main>
  )
}