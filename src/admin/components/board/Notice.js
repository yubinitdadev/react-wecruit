import React, { useState, useRef, useEffect, useContext } from "react";
import { Link } from "react-router-dom";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { DownloadBtns } from "commons/components/download-btn/DownloadBtns";
import { Pagination } from "commons/components/pagination/Pagination";
import { Context } from "contexts";
import { axiosCrudOnSubmit } from "commons/modules/commonUtil";
import { FormattedMessage, useIntl } from "react-intl";

export const Notice = (props, ref) => {
  const intl = useIntl();
  let PROC_CHK = "";

  const moment = require('moment');
  
  const componentRef = useRef();
  const [notis, setNotis] = useState([]);
  const [sConts, setSConts] = useState("");
  const [fltrState, setFltrState] = useState({});

  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(10);

  const indexOfLast = currentPage * postsPerPage;
  const indexOfFirst = indexOfLast - postsPerPage;

  function currentPosts(tmp) {
      let currentPosts = 0;
      currentPosts = tmp.slice(indexOfFirst, indexOfLast);
      return currentPosts;
  }

  const {
    state : {
        mbr,
        server
    },
    dispatch,
  } = useContext(Context);

  const headers = [
    { label: "번호", key: "idx" },
    { label: "공개구분", key: "pubYn" },
    { label: "카테고리", key: "cateCd" },
    { label: "제목", key: "title" },
    { label: "작성일", key: "rgstDttm" },
    { label: "조회수", key: "pvCnt" },
  ];

  const jsonCompResult = async ( resJson ) => {
    if( resJson.resCd !== "0000" )
    {
      return;
    }
    else
    {
      if( PROC_CHK === "SETDEL" )
      {
        toast.info(intl.formatMessage({
          id: "info.deleted",
          defaultMessage: "삭제되었습니다."
        }));
        
        let data = {
          ssnMbrUid : mbr.mbrUid,
        }
        
        getNotiList(data);
      }
    }
  }

  async function goSrch() {
    let data = {
      ssnMbrUid : mbr.mbrUid,
      sConts : sConts,
      sFltr : fltrState,
      sFltrCate : Object.keys(fltrState)[0],
      sFltrCont : Object.values(fltrState)[0],
    }

    getNotiList(data);
  }

  const changeFltr = (fltr) => {
    let tmpFltr = (fltr === "ASC") ? "DESC" :
                  (fltr === "DESC") ? "ASC" :
                  "ASC";

    return tmpFltr;
  }

  useEffect( () => {
    goSrch();
  }, [fltrState]);

  const [buttonList, setButtonList] = useState({});

  const handleButtonClick = (idx) => {

    let tmpBtnKey = "button" + idx;
    let tmpBtnVal = buttonList[tmpBtnKey];

    setButtonList({
      ...buttonList,
      [tmpBtnKey] : !tmpBtnVal
    })
  };

  async function setDelCRUD(brdUid, val)
  {
    if( val == "Y" ) val="N";
    else val="Y";

    PROC_CHK = "SETDEL";

    let goUrl = server.path + "/noti/notidelete"
    let data = {
      ssnMbrUid : mbr.mbrUid,
      delYn : val,
      brdUid : brdUid
    }

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    jsonCompResult(axiosRes.data);
  }

  const getNotiList = async (data) => {
    let goUrl = server.path + "/noti/list"
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    setNotis(axiosRes.data.notiArr);

    let tmpBtnList = {
      ...buttonList
    };

    axiosRes.data.notiArr.map((noti,idx)=> {
      tmpBtnList['button' + idx] = false;
    })

    setButtonList(tmpBtnList);
  }

  useEffect( async () => {
    window.scrollTo(0,0);

    let data = {
      ssnMbrUid : mbr.mbrUid
    }

    getNotiList(data);
  },[])

  return (
    <main>
      <ToastContainer autoClose={2000} />
      <section className="tableSection">
        <div className="sectionHeader">
          <h3>
            <span className="material-icons-outlined">settings</span>
            <b>
              <FormattedMessage
                id="notice.list"
                defaultMessage="공지사항 리스트"
              />
            </b>
          </h3>

        <DownloadBtns headers={headers} dataset={notis} fileName="공지사항" componentRef={componentRef}/>

        </div>
        <Link to="/admin/notice-add">
          <div className="linkButton">
            <FormattedMessage
              id="notice.create"
              defaultMessage="공지사항 생성"
            />
            <span className="material-icons-outlined">add</span>
          </div>
        </Link>

        <div className="searchBar">
          <label htmlFor="search">Search:</label>
          <input type="search" id="search" 
            onChange={(e) => {
              setSConts(e.target.value);
          }}/>
          <button type="button" onClick={goSrch}>
            <FormattedMessage
              id="search"
              defaultMessage="검색"
            />
          </button>
        </div>

        <article id="capture" ref={componentRef}>
          <table>
            <thead>
              <th>
                <span>
                  <FormattedMessage
                    id="number"
                    defaultMessage="번호"
                  />    
                </span>
                {/* 
                <div className="filterArrow">
                  <span />
                  <span />
                </div>
                */}
              </th>
              <th className={ fltrState.pubYn === "DESC" ? "descending" :
                              fltrState.pubYn === "ASC" ? "ascending" :
                              undefined } 
                  onClick={() => {
                    setFltrState({
                      pubYn : changeFltr(fltrState.pubYn)
                    }); 
                }}>
                <span>
                  <FormattedMessage
                    id="open.classify"
                    defaultMessage="공개구분"
                  /> 
                </span>
                <div className="filterArrow">
                  <span />
                  <span />
                </div>
              </th>
              <th className={ fltrState.cateCd === "DESC" ? "descending" :
                              fltrState.cateCd === "ASC" ? "ascending" :
                              undefined } 
                  onClick={() => {
                    setFltrState({
                      cateCd : changeFltr(fltrState.cateCd)
                    }); 
                }}>
                <span>
                  <FormattedMessage
                    id="category"
                    defaultMessage="카테고리"
                  /> 
                </span>
                <div className="filterArrow">
                  <span />
                  <span />
                </div>
              </th>
              <th className={ fltrState.title === "DESC" ? "descending" :
                              fltrState.title === "ASC" ? "ascending" :
                              undefined } 
                  onClick={() => {
                    setFltrState({
                      title : changeFltr(fltrState.title)
                    }); 
                }}>
                <span>
                  <FormattedMessage
                    id="title"
                    defaultMessage="제목"
                  /> 
                </span>
                <div className="filterArrow">
                  <span />
                  <span />
                </div>
              </th>
              <th className={ fltrState.rgstDttm === "DESC" ? "descending" :
                              fltrState.rgstDttm === "ASC" ? "ascending" :
                              undefined } 
                  onClick={() => {
                    setFltrState({
                      rgstDttm : changeFltr(fltrState.rgstDttm)
                    }); 
                }}>
                <span>
                  <FormattedMessage
                    id="reporting.date"
                    defaultMessage="작성일"
                  /> 
                </span>
                <div className="filterArrow">
                  <span />
                  <span />
                </div>
              </th>
              <th className={ fltrState.pvCnt === "DESC" ? "descending" :
                              fltrState.pvCnt === "ASC" ? "ascending" :
                              undefined } 
                  onClick={() => {
                    setFltrState({
                      pvCnt : changeFltr(fltrState.pvCnt)
                    }); 
                }}>
                <span>
                  <FormattedMessage
                    id="view.count"
                    defaultMessage="조회수"
                  /> 
                </span>
                <div className="filterArrow">
                  <span />
                  <span />
                </div>
              </th>
              <th>
                <span>
                  <FormattedMessage
                    id="etc"
                    defaultMessage="기타"
                  />
                </span>
              </th>
            </thead>
            <tbody>
              {currentPosts(notis).map((data, idx) => (
                <tr key={idx}>
                  <td>{idx + 1}</td>
                  <td>
                    { 
                      (data.pubKnd === "emp") 
                      ? <FormattedMessage
                          id="employer"
                          defaultMessage="인사담당자"
                        /> : 
                      (data.pubKnd === "rec") 
                      ? <FormattedMessage
                          id="recruiter"
                          defaultMessage="리크루터"
                        /> :
                        <FormattedMessage
                          id="no.input"
                          defaultMessage="미입력"
                        />
                    }
                  </td>
                  <td>
                    { 
                      (data.cateCd === "01" ) 
                      ? <FormattedMessage
                          id="project.add"
                          defaultMessage="프로젝트 등록"
                        /> : 
                      (data.cateCd === "02" ) 
                      ? <FormattedMessage
                          id="service.use"
                          defaultMessage="서비스 이용"
                        /> :
                      (data.cateCd === "03" ) 
                      ? <FormattedMessage
                          id="pay.cost"
                          defaultMessage="비용 결제"
                        /> :
                      (data.cateCd === "04" ) 
                      ? <FormattedMessage
                          id="etc"
                          defaultMessage="기타"
                        /> :
                        <FormattedMessage
                          id="no.input"
                          defaultMessage="미입력"
                        />
                    }
                  </td>
                  <td>{data.title}</td>
                  <td>{ moment(data.rgstDttm).format("YYYY년 MM월 DD일 HH시 mm분") }</td>
                  <td>{data.pvCnt}</td>
                  <td className="additionalFunction">
                    <button id={"openMenuBtn" + idx} onClick={ () => {
                      handleButtonClick(idx);
                    }}>
                      <FormattedMessage
                        id="additional.function"
                        defaultMessage="추가기능"
                      />
                      <span className="material-icons">
                        keyboard_arrow_down
                      </span>
                    </button>
                    <ul key={data.brdUid} id={"menuBtn" + idx} className = {buttonList['button' + idx] ? "active" : ""}>
                      <li className={ "editBtn" + idx }>
                        <Link 
                          to = "/admin/notice-edit" 
                          state={{ brdUid: data.brdUid }}>
                          <span className = "material-icons-outlined">edit</span>
                          <FormattedMessage
                            id="notice.eidt"
                            defaultMessage="공지사항 수정하기"
                          />
                        </Link>
                      </li>
                      <li className={ "delBtn" + idx } 
                        onClick= {()=>{
                          console.log(data.brdUid);
                          setDelCRUD( data.brdUid, "N" );
                        }}>
                        <span className = "material-icons">delete_outline</span>
                          <FormattedMessage
                            id="notice.delete"
                            defaultMessage="공지사항 삭제하기"
                          />
                      </li>
                    </ul>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </article>
        
        <Pagination postsPerPage={postsPerPage} totalPosts={notis.length} paginate={setCurrentPage} currentPage={currentPage}/>
      </section>
    </main>
  );
};
