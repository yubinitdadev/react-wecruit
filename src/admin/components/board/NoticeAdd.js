import { axiosCrudOnSubmit } from 'commons/modules/commonUtil';
import { Context } from 'contexts';
import {useContext, useState} from 'react'
import { FormattedMessage, useIntl } from 'react-intl';
import { useNavigate } from 'react-router-dom';
import { toast, ToastContainer } from 'react-toastify';
// import { getDatabase, ref, set } from "firebase/database";
import app from '../../../Firebase'
import EditorComponent from '../editor/EditorComponent'

export const NoticeAdd = () => {
  const intl = useIntl();
  let PROC_CHK = "";
  const navigate = useNavigate();

  const {
    state : {
        server,
        mbr
    },
    dispatch,
  } = useContext(Context);

  const [state, setState] = useState({
    ssnMbrUid: mbr.mbrUid,
    pubKnd : "emp",
    cateCd : "",
    title: "",
    ckData: "",
  });

  const jsonCompResult = ( resJson ) => {
    if( resJson.resCd !== "0000" )
    {
      return;
    }
    else
    {
      if( PROC_CHK === "INS" )
      {
        if( resJson.mbrUid === "DUPL" )
        {
          toast.error(intl.formatMessage({
            id: "error.id.exist",
            defaultMessage: "이미 사용중인 아이디입니다."
          }));
          return;
        }
        else
        {
          navigate("/admin/notice");
          return;
        }
      }
    }
  }

  async function goCRUD() {
    cntntsCRUD();
  }

  async function cntntsCRUD() {
    PROC_CHK = 'INS';
    let goUrl = server.path + "/noti/notiinsert"
    let axiosRes = await axiosCrudOnSubmit(state, goUrl, "POST");

    jsonCompResult(axiosRes.data);
  }

  return (
    <main>
      <ToastContainer />
      <section className="formSection">
        <div className="formHeader">
          <h1>
            <FormattedMessage
              id="notice.creating"
              defaultMessage="공지사항 생성"
            />
          </h1>
          <h2>(주)위크루트</h2>
        </div>
        <article>
          <form>
            <div className="formWrap">
              <ul className="oneCol">
                <li>
                  <span className="labelSpan">
                    <FormattedMessage
                      id="open.classify"
                      defaultMessage="공개구분"
                    />
                  </span>
                  <div className="radioInput">
                    <input 
                      type="radio" 
                      id="employer" 
                      name="noticeTarget"
                      value="emp"
                      checked={(state.pubKnd === "emp") ? true:false}
                      onChange={(e)=>{
                        setState({
                          ...state,
                          pubKnd: e.target.value
                        })
                      }}/>
                    <label htmlFor="employer">
                      <FormattedMessage
                        id="employer"
                        defaultMessage="인사담당자"
                      />
                    </label>
                    
                    <input 
                      type="radio" 
                      id="recruiter" 
                      name="noticeTarget" 
                      value="rec"
                      checked={(state.pubKnd === "rec") ? true:false}
                      onChange={(e)=>{
                        setState({
                          ...state,
                          pubKnd: e.target.value
                        })
                      }}/>
                    <label htmlFor="recruiter">
                      <FormattedMessage
                        id="recruiter"
                        defaultMessage="리크루터"
                      />
                    </label>
                  </div>
                </li>
              </ul>
              <ul className="oneCol">
                <li>
                  <label htmlFor="noticeCategory">
                    <FormattedMessage
                      id="category.select"
                      defaultMessage="카테고리 선택"
                    />
                  </label>
                  <select 
                    name="noticeCategory" 
                    id="noticeCategory"
                    value={state.cateCd}
                    onChange={(e)=>{
                      setState({
                        ...state,
                        cateCd: e.target.value
                      });
                    }}
                  >
                    <option value="01">
                      {intl.formatMessage({
                        id: "project.add",
                        defaultMessage: "프로젝트 등록"
                      })}
                    </option>
                    <option value="02">
                      {intl.formatMessage({
                        id: "service.use",
                        defaultMessage: "서비스 이용"
                      })}
                    </option>
                    <option value="03">
                      {intl.formatMessage({
                        id: "pay.cost",
                        defaultMessage: "비용 결제"
                      })}
                    </option>
                    <option value="04">
                      {intl.formatMessage({
                        id: "etc",
                        defaultMessage: "기타"
                      })}
                    </option>
                  </select>
                </li>
              </ul>
              <ul className="oneCol">
                <li>
                  <label htmlFor="noticeTitle">
                    <FormattedMessage
                      id="title"
                      defaultMessage="제목"
                    />
                  </label>
                  <input 
                    type="text" 
                    id="noticeTitle" 
                    className="fullWidth" 
                    placeholder={intl.formatMessage({
                      id: "placeholder.title",
                      defaultMessage: "제목을 입력해주세요"
                    })} 
                    value={state.title}
                    onChange={(e)=>{
                      setState({
                        ...state,
                        title: e.target.value
                      })
                    }}
                  />
                </li>
              </ul>
              <ul className="oneCol">
                <li>
                  <label htmlFor="noticeContent">
                    <FormattedMessage
                      id="content"
                      defaultMessage="내용"
                    />
                  </label>
                  <EditorComponent state={state} setState={setState} target={"ckData"}/>
                </li>
              </ul>
            </div>
            <div className="flexAlignCenter">
              <button 
                className="linkButton"
                type="button" 
                onClick={goCRUD}
              >
                <FormattedMessage
                  id="notice.creating"
                  defaultMessage="공지사항 생성"
                />
              </button>
            </div>
          </form>
        </article>
      </section>
    </main>
  );
}