import { axiosCrudOnSubmit } from "commons/modules/commonUtil";
import { Context } from "contexts";
import { useContext, useEffect, useState } from "react";
import { FormattedMessage, useIntl } from "react-intl";
import { useLocation, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import EditorComponent from "../editor/EditorComponent";

export const NoticeEdit = () => {
  const intl = useIntl();

  let PROC_CHK = "";
  const navigate = useNavigate();
  const location = useLocation();

  const {
    state : {
        server,
        mbr
    },
    dispatch,
  } = useContext(Context);

  const [state, setState] = useState({
    ssnMbrUid: mbr.mbrUid,
    brdUid : "",
    pubKnd : "emp",
    cateCd : "",
    title: "",
    ckData: "",
    tmpCkData: "",
    initSetCmplt: false,
  });

  const jsonCompResult = ( resJson ) => {
    if( resJson.resCd !== "0000" )
    {
      toast.error(intl.formatMessage({
        id: "error.during.edit",
        defaultMessage: "수정 중 오류가 발생했습니다."
      }));
    }
    else
    {
      if( PROC_CHK === "UPD" )
      {
        navigate("/admin/notice");
        return;
      }
    }
  }

  async function cntntsCRUD() {
    PROC_CHK = 'UPD';
    let goUrl = server.path + "/noti/notiupdate";

    let axiosRes = await axiosCrudOnSubmit(state, goUrl, "POST");

    console.log("AXIOS RESULT : ");
    console.log(axiosRes);

    jsonCompResult(axiosRes.data);
  }

  async function initSetHandle() {
    let goUrl = server.path + "/noti/notidetail";
    let data = {
      ssnMbrUid : mbr.mbrUid,
      brdUid: location.state.brdUid
    }

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    let axiosData = axiosRes.data;

    console.log("INIT RES DATA : ");
    console.log(axiosData);

    setState({
      ...state,
      brdUid : axiosData.brdUid,
      pubKnd : axiosData.pubKnd,
      cateCd : axiosData.cateCd,
      title: axiosData.title,
      initSetCmplt: true,
      tmpCkData: axiosData.cntnt,
    })
  }

  useEffect(async () => {
    initSetHandle();
  },[])

  useEffect(async () => {
    if(state.initSetCmplt)
    {
      setState({
        ...state,
        initSetCmplt: false,
        ckData: state.tmpCkData,
      })
    }
  },[state.initSetCmplt])

  return (
    <main>
      <section className="formSection">
        <div className="formHeader">
          <h1>
            <FormattedMessage
              id="notice.eidting"
              defaultMessage="공지사항 수정"
            />
          </h1>
          <h2>(주)위크루트</h2>
        </div>
        <article>
          <form>
            <div className="formWrap">
              <ul className="oneCol">
                <li>
                  <span className="labelSpan">
                    <FormattedMessage
                      id="open.classify"
                      defaultMessage="공개구분"
                    />
                  </span>
                  <div className="radioInput">
                    <input type="radio" 
                      id="employer" 
                      name="noticeTarget" 
                      value="emp"
                      checked={(state.pubKnd === "emp") 
                      ? true:false}
                      onChange={(e)=>{
                        setState({
                          ...state,
                          pubKnd: e.target.value
                        })
                      }}/>
                    <label htmlFor="employer">
                      <FormattedMessage
                        id="employer"
                        defaultMessage="인사담당자"
                      />
                    </label>
                    <input type="radio" 
                      id="recruiter" 
                      name="noticeTarget" 
                      value="rec"
                      checked={(state.pubKnd === "rec") ? true:false}
                      onChange={(e)=>{
                        setState({
                          ...state,
                          pubKnd: e.target.value
                        })
                      }}/>
                    <label htmlFor="recruiter">
                      <FormattedMessage
                        id="recruiter"
                        defaultMessage="리크루터"
                      />
                    </label>
                  </div>
                </li>
              </ul>
              <ul className="oneCol">
                <li>
                  <label htmlFor="noticeCategory">
                    <FormattedMessage
                      id="category.select"
                      defaultMessage="카테고리 선택"
                    />
                  </label>
                  <select name="noticeCategory" 
                    id="noticeCategory"
                    value={state.cateCd}
                    onChange={(e)=>{
                      setState({
                        ...state,
                        cateCd: e.target.value
                      });
                    }}>
                    <option value="01">
                      {intl.formatMessage({
                        id: "project.add",
                        defaultMessage: "프로젝트 등록"
                      })}
                    </option>
                    <option value="02">
                      {intl.formatMessage({
                        id: "service.use",
                        defaultMessage: "서비스 이용"
                      })}
                    </option>
                    <option value="03">
                      {intl.formatMessage({
                        id: "pay.cost",
                        defaultMessage: "비용 결제"
                      })}
                    </option>
                    <option value="04">
                      {intl.formatMessage({
                        id: "etc",
                        defaultMessage: "기타"
                      })}
                    </option>
                  </select>
                </li>
              </ul>
              <ul className="oneCol">
                <li>
                  <label htmlFor="noticeTitle">
                    <FormattedMessage
                      id="title"
                      defaultMessage="제목"
                    />
                  </label>
                  <input type = "text" 
                    id = "noticeTitle" 
                    className = "fullWidth" 
                    placeholder = {intl.formatMessage({
                      id: "placeholder.title",
                      defaultMessage: "제목을 입력해주세요"
                    })} 
                    value = {state.title}
                    onChange = {(e)=>{

                      console.log("changed!");

                      setState({
                        ...state,
                        title: e.target.value
                      })
                    }}/>
                </li>
              </ul>
              <ul className="oneCol">
                <li>
                  <label htmlFor="noticeContent">
                    <FormattedMessage
                      id="content"
                      defaultMessage="내용"
                    />
                  </label>
                  <EditorComponent state={state} setState={setState} target={"ckData"}/>
                </li>
              </ul>
            </div>
            <div className="flexAlignCenter">
              <button type="button" className="linkButton" onClick={cntntsCRUD}>
                <FormattedMessage
                  id="notice.eidting"
                  defaultMessage="공지사항 수정"
                />
              </button>
            </div>
          </form>
        </article>
      </section>
    </main>
      
  );
}