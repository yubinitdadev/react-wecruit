import lightBulb from "../images/header-bulb-light-small.png";
import thumbsUpImg from "commons/images/thumbs-up-line.png";

import Modal from "modal-all/ModalAll";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { useContext, useEffect, useState } from "react";
import { Context } from "contexts";
import { toast, ToastContainer } from "react-toastify";
import axios from "axios";
import * as commonUtil from "commons/modules/commonUtil";
import * as defVal from "commons/modules/defVal";
import { FormattedMessage, useIntl } from "react-intl";

export const Qna = () => {
  const intl = useIntl();

  let PROC_CHK = "";

  const {
    state: { server, mbr },
    dispatch,
  } = useContext(Context);

  const location = useLocation();
  const navigate = useNavigate();
  const moment = require("moment");
  let previewWindow = "";
  let previewImg = document.createElement("img");

  const [mbrCnt, setMbrCnt] = useState({});
  const [detailState, setDetailState] = useState(defVal.setAxiosJobState(null));
  const [qaListState, setQaListState] = useState([]);
  const [qaWrtrListState, setQaWrtrListState] = useState([]);
  const [qaCntnt, setQaCntnt] = useState(defVal.setAxiosQaCntntState());

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid"),
    ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi"),
  };

  let locationState = {
    hireUid: location.state.hireUid,
    mbrUid: location.state.mbrUid,
  };

  useEffect(async () => {
    console.log("Page First Load EFFECT");
    initSetHandle();
  }, []);

  const reRender = () => {
    setDetailState({
      ...detailState,
      initSetCmplt: true,
    });
  };

  async function initSetHandle() {
    let goUrl = server.path + "/hire/hiredetail";
    let data = {
      ssnMbrUid: ssn.ssnMbrUid,
      ssnMbrDivi: ssn.ssnMbrDivi,
      hireUid: locationState.hireUid,
    };

    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");
    setDetailState(axiosRes.data);

    var tmpMbrCnt = await getMbrCnt();
    var tmpqaList = await getQaList();
  }

  const getMbrCnt = async () => {
    let goUrl = server.path + "/mbr/mbrcnt";
    let data = {
      hireUid: locationState.hireUid,
    };

    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");
    let tmpDate = axiosRes.data.mbrcnt;
    setMbrCnt(tmpDate);
    return tmpDate;
  };

  const getQaList = async () => {
    let goUrl = server.path + "/qa/qalist";
    let data = {
      hireUid: locationState.hireUid,
      sMbrUid: "",
      sRcvMbrUid: "",
      sConts: document.getElementById("sConts")
        ? document.getElementById("sConts").value
        : "",
    };

    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");
    var tmpDate = axiosRes.data.resArr;
    setQaListState(tmpDate);
    setQaWrtrListState(axiosRes.data.resMbrArr);
    return tmpDate;
  };

  //////////////////////////////////////////////////////////////////////////////////////////////////

  const [rcmmndEndModal, setRcmmndEndModal] = useState(false);
  const [hireDoneModal, setHireDoneModal] = useState(false);

  const openRcmmndEndModal = () => {
    setRcmmndEndModal(true);
  };
  const closeRcmmndEndModal = () => {
    setRcmmndEndModal(false);
  };

  const openHireDoneModal = () => {
    setHireDoneModal(true);
  };
  const closeHireDoneModal = () => {
    setHireDoneModal(false);
  };

  ////////////////////////////////////////////////////////////////////////////////////////////////

  function onKeyDownForm(keyEvent) {
    if ((keyEvent.charCode || keyEvent.keyCode) === 13) {
      keyEvent.preventDefault();
    }
  }

  const onKeyUp = (e, uid, lvl, seq) => {
    if (e.key == "Enter") goCRUD(uid, lvl, seq);
    return false;
  };

  const onKeyUpSrch = (e) => {
    if (e.key == "Enter") {
      getQaList();
    }
    return false;
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////

  const jsonCompResult = (resJson) => {
    console.log("PROC_CHK : " + PROC_CHK);

    if (resJson.resCd !== "0000") {
      toast.error(
        intl.formatMessage({
          id: "error.during.save",
          defaultMessage: "저장 중 오류가 발생했습니다.",
        })
      );
      return;
    } else {
      if (PROC_CHK === "INS") {
        PROC_CHK = "";
        getQaList();
      } else if (PROC_CHK === "UPDHIREDONE") {
        PROC_CHK = "";
        detailState.hireStat = "end";
        //reRender();
      } else if (PROC_CHK === "UPDRCMMND") {
        PROC_CHK = "";
        detailState.rcmndEndYn = "Y";

        //reRender();
      } else if (PROC_CHK === "DELQA") {
        PROC_CHK = "";
        getQaList();
      }
    }
  };

  const goCRUD = async (uid, lvl, seq) => {
    var data = defVal.setAxiosQaCntntState(null);
    data.cntnt = document.getElementById("qsCntnt").value;
    data.mbrUid = ssn.ssnMbrUid;
    data.mbrDivi = ssn.ssnMbrDivi;
    data.hireUid = locationState.hireUid;
    data.lvl = lvl;
    data.refUid = uid;
    data.rcvKnd = document.getElementById("rcvKnd").value;

    alert(
      intl.formatMessage({
        id: "info.qna.added",
        defaultMessage: "qna가 등록되었습니다.",
      })
    );

    console.log("rcvKnd == " + document.getElementById("rcvKnd").value);

    var txt = intl.formatMessage({
      id: "error.need.input.question",
      defaultMessage: "질문을 입력해주세요.",
    });
    if (lvl == 2) {
      data.refUid = uid;
      data.cntnt = qaListState[seq].repCntnt;
      data.rcvKnd = "";
      //alert(data.cntnt)
      txt = intl.formatMessage({
        id: "error.need.input.answer",
        defaultMessage: "답변을 입력해주세요.",
      });
    }

    if (commonUtil.CheckIsEmptyFromVal(data.cntnt, txt) === false) return;

    document.getElementById("qsCntnt").value = "";

    PROC_CHK = "INS";
    cntntsCRUD(data);
  };

  const cntntsCRUD = async (data) => {
    PROC_CHK = "INS";

    let goUrl = server.path + "/qa/qainsert";
    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");

    jsonCompResult(axiosRes.data);
  };

  const goRcmmndCRUD = async () => {
    console.log("goRcmmndCRUD == " + locationState.hireUid);

    PROC_CHK = "UPDRCMMND";

    var data = {
      hireUid: locationState.hireUid,
      mbrUid: ssn.ssnMbrUid,
    };

    let goUrl = server.path + "/hire/hirercmmndend";
    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");

    jsonCompResult(axiosRes.data);

    closeRcmmndEndModal();
  };

  const goHireStatCRUD = async () => {
    PROC_CHK = "UPDHIREDONE";

    var data = {
      hireUid: locationState.hireUid,
      mbrUid: ssn.ssnMbrUid,
    };

    let goUrl = server.path + "/hire/hiredone";
    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");

    jsonCompResult(axiosRes.data);

    closeHireDoneModal();
  };

  const delQa = async (uid) => {
    PROC_CHK = "DELQA";

    var data = {
      brdUid: uid,
    };

    let goUrl = server.path + "/qa/qadelete";
    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");

    jsonCompResult(axiosRes.data);

    //closeHireDoneModal();
  };

  return (
    <main className="adminCustom">
      <section className="recruitLists">
        <div className="ingList">
          <h3 className="sectionTitle">
            <span>
              {detailState.hireStat === "ing"
                ? `${intl.formatMessage({
                    id: "status.ing",
                    defaultMessage: "진행중",
                  })}`
                : `${intl.formatMessage({
                    id: "status.done",
                    defaultMessage: "종료된",
                  })}`}
              &nbsp;
              <FormattedMessage id="recruitment" defaultMessage="채용공고" />
            </span>
            <em>
              <FormattedMessage id="qna.manage" defaultMessage="Q&amp;A 관리" />
            </em>
          </h3>
          <div className="noRecruitList" style={{ display: "none" }}>
            <FormattedMessage
              id="description.no.ing.posting"
              defaultMessage="진행중인 채용공고가 없습니다."
            />
          </div>
          <ul>
            <li className="ingListColumn">
              <div className="listTop">
                <div className="listLeft">
                  <h3>
                    <Link
                      to="/recruiters/ing-detail"
                      state={{
                        hireUid: detailState.hireUid,
                        mbrUid: detailState.mbrUid,
                      }}
                    >
                      {detailState.title}
                    </Link>
                  </h3>
                  <p>
                    <FormattedMessage
                      id="registration.date"
                      defaultMessage="등록일"
                    />{" "}
                    : &nbsp;{moment(detailState.rgstDttm).format("YYYY-MM-DD")}
                    &nbsp; /{" "}
                    <FormattedMessage
                      id="recruitment.deadline"
                      defaultMessage="인재추천 마감일"
                    />{" "}
                    :{" "}
                    {detailState.rcmndUntilDon === "Y"
                      ? `${intl.formatMessage({
                          id: "until.hired",
                          defaultMessage: "채용시까지",
                        })}`
                      : commonUtil.substr(detailState.rcmndEndDttm, 0, 19)}
                    &nbsp; /{" "}
                    <strong>
                      <FormattedMessage
                        id="recruitment.people.number"
                        defaultMessage="모집인원"
                      />{" "}
                      : {detailState.hireSize}
                      <FormattedMessage id="people.count" defaultMessage="명" />
                    </strong>
                  </p>
                  <div>
                    {detailState.rcmndEndYn === "Y" ? (
                      <button type="button" className="recruitingClosed">
                        <FormattedMessage
                          id="recommend.finished"
                          defaultMessage="인재추천 마감"
                        />
                      </button>
                    ) : (
                      <button
                        type="button"
                        className="modalButtonStyling cyanColored"
                        onClick={openRcmmndEndModal}
                      >
                        <FormattedMessage
                          id="recommend.finish"
                          defaultMessage="인재추천 마감하기"
                        />{" "}
                        &gt;
                      </button>
                    )}

                    {
                      <Modal
                        visible={rcmmndEndModal}
                        closable={true}
                        maskClosable={true}
                        onSubmit={goRcmmndCRUD}
                        onClose={closeRcmmndEndModal}
                        inputName="예"
                        closeName="아니오"
                      >
                        <>
                          <h4>
                            <FormattedMessage
                              id="recommend.finish"
                              defaultMessage="인재추천 마감하기"
                            />
                          </h4>
                          <div className="alertBox">
                            <FormattedMessage
                              id="recommend.finish.ask"
                              defaultMessage="인재추천을 마감하시겠습니까?"
                            />
                          </div>
                          <p className="textAlignCenter">
                            <FormattedMessage
                              id="recommend.finish.ask"
                              values={{
                                breakLine: <br />,
                              }}
                              defaultMessage="이후 귀사의 채용진행단계에 맞춰 {breakLine}좋은 인재를 채용하시기 바랍니다."
                            />
                          </p>
                        </>
                      </Modal>
                    }

                    {detailState.hireStat === "end" ? (
                      <button
                        type="button"
                        className="recruitingClosed"
                        style={{ marginLeft: "5px" }}
                      >
                        <FormattedMessage
                          id="recruit.finished"
                          defaultMessage="채용공고 종료"
                        />
                      </button>
                    ) : (
                      <button
                        type="button"
                        className="modalButtonStyling marginInline"
                        onClick={openHireDoneModal}
                      >
                        <FormattedMessage
                          id="recruit.finish"
                          defaultMessage="채용공고 종료하기"
                        />{" "}
                        &gt;
                      </button>
                    )}

                    {
                      <Modal
                        visible={hireDoneModal}
                        closable={true}
                        maskClosable={true}
                        onSubmit={goHireStatCRUD}
                        onClose={closeHireDoneModal}
                        inputName={intl.formatMessage({
                          id: "yes",
                          defaultMessage: "예",
                        })}
                        closeName={intl.formatMessage({
                          id: "no",
                          defaultMessage: "아니오",
                        })}
                      >
                        <>
                          <h4>
                            <FormattedMessage
                              id="recruit.finish"
                              defaultMessage="채용공고 종료하기"
                            />
                          </h4>
                          <div className="alertBox">
                            <FormattedMessage
                              id="recruit.finish.ask"
                              defaultMessage="채용공고를 종료하시겠습니까?"
                            />
                          </div>
                          <p className="textAlignCenter">
                            <FormattedMessage
                              id="recruit.finish.description"
                              values={{
                                breakLine: <br />,
                              }}
                              defaultMessage="선택된 리크루터와 함께 {breakLine} 성공적인 인재채용을 기원합니다!"
                            />
                          </p>
                        </>
                      </Modal>
                    }
                  </div>
                </div>
                <div className="flexColumn listRight">
                  <div className="threeSections">
                    <div>
                      <span>
                        <FormattedMessage
                          id="recruiter.suggested"
                          defaultMessage="제안한 리크루터"
                        />
                      </span>
                      <Link
                        to="/admin/recruiter-manage"
                        state={{
                          hireUid: detailState.hireUid,
                          mbrUid: detailState.mbrUid,
                        }}
                      >
                        {detailState.cmmntCnt}
                        <FormattedMessage
                          id="people.count"
                          defaultMessage="명"
                        />
                      </Link>
                    </div>
                    <div>
                      <span>
                        <FormattedMessage
                          id="recruiter.selected"
                          defaultMessage="선택한 리크루터"
                        />
                      </span>
                      <Link
                        to="/admin/recruiter-manage"
                        state={{
                          hireUid: detailState.hireUid,
                          mbrUid: detailState.mbrUid,
                        }}
                      >
                        <strong>{detailState.cmmntChooseCnt}</strong>/
                        {mbrCnt.recTotCnt}
                        <FormattedMessage
                          id="people.count"
                          defaultMessage="명"
                        />
                      </Link>
                    </div>
                    <div>
                      <span>
                        <FormattedMessage
                          id="candidate.recommended"
                          defaultMessage="추천된 후보자"
                        />
                      </span>
                      <Link
                        to="/admin/candidate-manage"
                        state={{
                          hireUid: detailState.hireUid,
                          mbrUid: detailState.mbrUid,
                        }}
                      >
                        <strong>0</strong>/{detailState.recmmndCnt}
                        <FormattedMessage
                          id="people.count"
                          defaultMessage="명"
                        />
                      </Link>
                    </div>
                  </div>
                  <div className="qnaStatus">
                    <Link
                      to="/admin/qna-manage"
                      state={{
                        hireUid: detailState.hireUid,
                        mbrUid: detailState.mbrUid,
                      }}
                    >
                      Q&amp;A{" "}
                      <span>
                        {detailState.qaCnt}
                        <FormattedMessage id="count" defaultMessage="개" />
                      </span>
                    </Link>
                  </div>
                  <p>
                    <FormattedMessage
                      id="description.open.to.recruiter"
                      defaultMessage="* 채용진행 현황은 참여중인 리크루터에게도 공개됩니다."
                    />
                  </p>
                </div>
              </div>
            </li>
          </ul>
        </div>

        <form
          action
          className="flex employersForm marginTopBottom"
          onKeyDown={onKeyDownForm}
        >
          <input
            type="search"
            name="sConts"
            id="sConts"
            placeholder={intl.formatMessage({
              id: "placeholder.recruiter.name",
              defaultMessage: "리크루터 이름을 입력하세요.",
            })}
            onKeyUp={(e) => {
              onKeyUpSrch(e);
            }}
          />
          <input
            type="button"
            value={intl.formatMessage({
              id: "search",
              defaultMessage: "검색",
            })}
            className="basicButton"
            onClick={(e) => {
              getQaList();
            }}
          />
        </form>

        <div className="qnaList">
          <div className="flexSpaceBetween">
            <h5>
              <FormattedMessage id="recruiter" defaultMessage="리크루터" />{" "}
              Q&amp;A
            </h5>
            <span>
              <FormattedMessage id="all" defaultMessage="전체" /> Q&amp;A{" "}
              <strong>{qaListState.length}</strong>
              <FormattedMessage id="count" defaultMessage="개" />
            </span>
          </div>

          <p className="lightColored">
            <img src={lightBulb} alt="전구 모양 아이콘" />
            <FormattedMessage
              id="description.quick.feedback"
              defaultMessage="빠른 피드백은 리크루터를 춤추게 합니다!"
            />
          </p>

          <div className="commentInput">
            <select name="rcvKnd" id="rcvKnd">
              <option value="">
                {intl.formatMessage({
                  id: "all",
                  defaultMessage: "전체",
                })}
              </option>
              <option value="cmmnd">
                {intl.formatMessage({
                  id: "recruiter.suggested",
                  defaultMessage: "제안한 리크루터",
                })}
              </option>
              <option value="choose">
                {intl.formatMessage({
                  id: "recruiter.been.selected",
                  defaultMessage: "선택된 리크루터",
                })}
              </option>
              {qaWrtrListState.map((list, idx) =>
                ssn.ssnMbrUid !== list.mbrUid && list.mbrNm != "" ? (
                  <option value={list.mbrUid}>{list.mbrNm}</option>
                ) : (
                  ""
                )
              )}
            </select>
            <input
              type="text"
              name="qsCntnt"
              id="qsCntnt"
              placeholder={intl.formatMessage({
                id: "description.message.to.employer",
                defaultMessage: "리크루터에게 전달할 내용을 알려주세요",
              })}
              onKeyUp={(e) => {
                onKeyUp(e, "", 1, 0);
              }}
            />
            <input
              type="button"
              value={intl.formatMessage({
                id: "upload.writing",
                defaultMessage: "글올리기",
              })}
              className="basicButton"
              onClick={(e) => {
                goCRUD("", 1, 0);
              }}
            />
          </div>
          <ul>
            {qaListState.map((list, idx) =>
              list.lvl == 1 ? (
                <li>
                  <span>
                    <strong>
                      {list.mbrNm != "" ? list.mbrNm : list.mbrId}{" "}
                    </strong>{" "}
                    | <time>{commonUtil.substr(list.rgstDttm, 0, 19)}</time>
                    <span
                      className="material-icons deleteButton"
                      onClick={(e) => {
                        delQa(list.brdUid);
                      }}
                    >
                      close
                    </span>
                  </span>
                  <p>{list.cntnt}</p>
                  <p className="lightColored">
                    <img
                      src={thumbsUpImg}
                      alt="엄지 손가락 위로 향한 손 아이콘"
                    />
                    <FormattedMessage
                      id="description.thanks.for.comment"
                      defaultMessage="소중한 코멘트 감사합니다."
                    />
                  </p>
                  {qaListState.map((sublist, subidx) =>
                    sublist.lvl == 2 && sublist.refUid == list.refUid ? (
                      <ul className="replyText">
                        <li>
                          <span className="material-icons">
                            subdirectory_arrow_right
                          </span>
                          <span>
                            {sublist.mbrNm != ""
                              ? sublist.mbrNm
                              : sublist.mbrId}{" "}
                            |{" "}
                            <time>
                              {commonUtil.substr(sublist.rgstDttm, 0, 19)}
                            </time>
                            <span
                              className="material-icons deleteButton"
                              onClick={(e) => {
                                delQa(list.repUid);
                              }}
                            >
                              close
                            </span>
                          </span>
                          <p>{sublist.cntnt}</p>
                        </li>
                      </ul>
                    ) : (
                      ""
                    )
                  )}

                  <div className="reply">
                    <input
                      type="text"
                      value={list.repCntnt}
                      onChange={(e) => {
                        qaListState[idx].repCntnt = e.target.value;
                        reRender();
                      }}
                      onKeyUp={(e) => {
                        onKeyUp(e, list.brdUid, 2, idx);
                      }}
                    />
                    <input
                      type="button"
                      className="basicButton"
                      value={intl.formatMessage({
                        id: "write.comment",
                        defaultMessage: "답글쓰기",
                      })}
                      onClick={(e) => {
                        goCRUD(list.brdUid, 2, idx);
                      }}
                    />
                  </div>
                </li>
              ) : (
                ""
              )
            )}
          </ul>
        </div>
      </section>
    </main>
  );
};
