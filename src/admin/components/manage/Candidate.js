import Modal from "modal-all/ModalAll";
import { CandidateContact } from "modal-all/modals/CandidateContact";
import { SendingNote } from "modal-all/modals/SendingNote";
import { RecruitResult } from "commons/components/select/RecruitResult";

import { Link, useLocation, useNavigate } from "react-router-dom";
import { useContext, useEffect, useState } from "react";
import { Context } from "contexts";
import { toast, ToastContainer } from "react-toastify";
import axios from "axios";
import fileDownload from "js-file-download";
import { CSVLink } from "react-csv";

import { axiosCrudOnSubmit } from "commons/modules/commonUtil";
import * as commonUtil from "commons/modules/commonUtil";
import { snglFileUpload } from "commons/modules/multipartUtil";
import * as defVal from "commons/modules/defVal";
import { AddressInput } from "commons/components/AddressInput";
import { ImagePreview } from "commons/components/inputs/image-preview/ImagePreview";
import { TagTemplate } from "commons/components/tags/TagTemplate";
import BusinessTypeTemplate from "commons/components/inputs/business-type/BusinessTypeTemplate";
import WorkPlaceTemplate from "commons/components/inputs/work-place/WorkPlaceTemplate";
import WorkPositionTemplate from "commons/components/inputs/work-position/WorkPositionTemplate";
import { UPD_MBRUID } from "contexts/actionTypes";
import { FormattedMessage, useIntl } from "react-intl";

export const Candidate = () => {
  const intl = useIntl();
  let PROC_CHK = "";

  const {
    state: { server, mbr },
    dispatch,
  } = useContext(Context);

  const location = useLocation();
  const navigate = useNavigate();
  const moment = require("moment");
  let previewWindow = "";
  let previewImg = document.createElement("img");

  const candidateStateCd = defVal.CandidateStateCd();

  const [mbrCnt, setMbrCnt] = useState({});
  const [recmmdState, setRecmmdState] = useState(
    defVal.setAxiosCnddtRcmmdState(null)
  );
  const [detailState, setDetailState] = useState(defVal.setAxiosJobState(null));
  const [qaListState, setQaListState] = useState([]);
  const [hireCnddtRcmmdList, setHireCnddtRcmmdList] = useState([]);
  const [excelList, setExcelList] = useState([]);

  const [empUid, setEmpUid] = useState("");
  const [uid, setUid] = useState("");
  const [seq, setSeq] = useState("");
  const [cnddtNm, setCnddtNm] = useState("");
  const [empMemo, setEmpMemo] = useState("");
  const [dprtMemo, setDprtMemo] = useState("");

  const [star1ActiveState, setStar1ActiveState] = useState("");
  const [star2ActiveState, setStar2ActiveState] = useState("");
  const [star3ActiveState, setStar3ActiveState] = useState("");
  const [star4ActiveState, setStar4ActiveState] = useState("");
  const [star5ActiveState, setStar5ActiveState] = useState("");

  let atchFiles;

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid"),
    ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi"),
  };

  let locationState = {
    hireUid: location.state.hireUid,
    mbrUid: location.state.mbrUid,
  };

  useEffect(async () => {
    console.log("Page First Load EFFECT");
    window.scrollTo(0, 0);
    initSetHandle();
  }, []);

  const reRender = () => {
    setDetailState({
      ...detailState,
      initSetCmplt: true,
    });
  };

  async function initSetHandle() {
    let goUrl = server.path + "/hire/hiredetail";
    let data = {
      ssnMbrUid: ssn.ssnMbrUid,
      ssnMbrDivi: ssn.ssnMbrDivi,
      hireUid: locationState.hireUid,
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    setDetailState(axiosRes.data);

    var tmpMbrCnt = await getMbrCnt();
    var tmpHireRcmmdList = await getHireCnddtRcmmdList();
  }

  const getMbrCnt = async () => {
    let goUrl = server.path + "/mbr/mbrcnt";
    let data = {
      hireUid: locationState.hireUid,
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    let tmpDate = axiosRes.data.mbrcnt;
    setMbrCnt(tmpDate);
    return tmpDate;
  };

  const getHireCnddtRcmmdList = async () => {
    let goUrl = server.path + "/hire/cnddtrecmmndlist";
    var sConts = "";
    try {
      sConts = document.getElementById("sConts").value;
    } catch (e) {}

    let data = {
      hireUid: locationState.hireUid,
      sConts: sConts,
      sRecmmndMbrUid: "",
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    let tmpArr = [];
    for (var k = 0; k < axiosRes.data.resArr.length; k++) {
      var tmpData = defVal.setAxiosCnddtRcmmdState(axiosRes.data.resArr[k]);
      tmpArr.push(tmpData);
    }

    setHireCnddtRcmmdList(tmpArr);
    return tmpArr;
  };

  ///////////////////////////////////////////////////////////////////////////

  const [rcmmndEndModal, setRcmmndEndModal] = useState(false);
  const [hireDoneModal, setHireDoneModal] = useState(false);

  const openRcmmndEndModal = () => {
    setRcmmndEndModal(true);
  };
  const closeRcmmndEndModal = () => {
    setRcmmndEndModal(false);
  };

  const openHireDoneModal = () => {
    setHireDoneModal(true);
  };
  const closeHireDoneModal = () => {
    setHireDoneModal(false);
  };

  const [qnaModal, setQnaVisible] = useState(false);
  const [replyModal, setReplyModal] = useState(false);
  const [celpNumPubModal, setCelpNumPubModal] = useState(false);
  const [rcmmndCnfrmModal, setRcmmndCnfrmModal] = useState(false);
  const [memoModal, setMemoModal] = useState(false);

  const openQnaModal = (uid, seq, nm) => {
    setUid(uid);
    setSeq(seq);
    setCnddtNm(nm);

    setQnaVisible(true);
  };

  const closeQnaModal = () => {
    setQnaVisible(false);
  };

  const openReplyModal = (uid, seq, nm) => {
    setUid(uid);
    setSeq(seq);
    setCnddtNm(nm);

    setReplyModal(true);
  };

  const closeReplyModal = () => {
    setReplyModal(false);
  };

  const openRcmmndCnfrmModal = (uid, seq, nm, empUid) => {
    //alert("aaaaaaaaaaaa");
    setEmpUid(empUid);
    setUid(uid);
    setSeq(seq);
    setCnddtNm(nm);

    setRcmmndCnfrmModal(true);
  };

  const closeRcmmndCnfrmModal = () => {
    setRcmmndCnfrmModal(false);
  };

  const openMemoModal = (uid, seq, nm) => {
    setUid(uid);
    setSeq(seq);
    setCnddtNm(nm);

    document.getElementById("empMemo").value = hireCnddtRcmmdList[seq].empMemo;
    document.getElementById("dprtMemo").value =
      hireCnddtRcmmdList[seq].dprtMemo;

    setMemoModal(true);
  };

  const closeMemoModal = () => {
    setMemoModal(false);
  };

  const openCelpNumPubModal = (uid, seq, nm) => {
    setUid(uid);
    setSeq(seq);
    setCnddtNm(nm);
    setCelpNumPubModal(true);
  };

  const closeCelpNumPubModal = () => {
    setCelpNumPubModal(false);
  };

  //////////////////////////////////////////////////////////////////////////////////////

  const onKeyUpSrch = (e) => {
    if (e.key == "Enter") {
      //getQaList();
    }
    return false;
  };

  const chkeckAll = (e) => {
    var chkYn = "N";
    if (document.getElementById("selectAll").checked == true) {
      chkYn = "Y";
    }

    for (var k = 0; k < hireCnddtRcmmdList.length; k++) {
      hireCnddtRcmmdList[k].chkYn = chkYn;
    }

    reRender();
  };

  const downloadAll = async (e) => {
    var chkCnt = 0;

    var tmpList = [];
    var tmpNmList = [];
    for (var k = 0; k < hireCnddtRcmmdList.length; k++) {
      if (hireCnddtRcmmdList[k].chkYn == "Y") {
        var filePath = hireCnddtRcmmdList[k].cnddtThmnl;
        var fileNm = hireCnddtRcmmdList[k].cnddtAtchFiles;

        var fileListJson = JSON.parse(fileNm);
        var orignlFileNm = fileListJson[0].orignlFileNm;

        //alert(orignlFileNm);

        if (filePath != "") {
          tmpList.push(filePath);
          tmpNmList.push(orignlFileNm);
          //handleDownload(k, filePath, fileNm);
        }
      }
    }

    if (tmpList.length == 0) {
      alert(
        intl.formatMessage({
          id: "error.select.candidate.download",
          defaultMessage: "다운로드할 후보자를 선택해 주세요.",
        })
      );
    }

    for (var k = 0; k < tmpList.length; k++) {
      //console.log("tmpList == " + tmpList[k]);
    }

    let goUrl = server.path + "/hire/cnddtfiledown";
    let data = {
      ssnMbrUid: ssn.ssnMbrUid,
      ssnMbrDivi: ssn.ssnMbrDivi,
      filePath: tmpList,
      fileNm: tmpNmList,
    };

    PROC_CHK = "FILEDONW";

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    console.log(axiosRes.data);
    jsonCompResult(axiosRes.data);
  };

  const handleZipDownload = (filePath, fileNm) => {
    if (filePath == "") {
      toast.error(
        intl.formatMessage({
          id: "error.no.attached.file",
          defaultMessage: "첨부된 파일이 없습니다.",
        })
      );
      return;
    }

    if (filePath != "") {
      let goUrl = server.host + filePath;
      axios
        .get(goUrl, {
          responseType: "blob",
        })
        .then((res) => {
          fileDownload(res.data, fileNm);
        });
    }
  };

  ////////////////////////////////////////////////////////////////////////////////

  const handleDownload = (idx, filePath, fileNm) => {
    if (filePath == "") {
      toast.error(
        intl.formatMessage({
          id: "error.no.attached.file",
          defaultMessage: "첨부된 파일이 없습니다.",
        })
      );
      return;
    }

    var orignlFileNm = fileNm;

    try {
      let fileListJson = JSON.parse(fileNm);
      orignlFileNm = fileListJson[0].orignlFileNm;
    } catch (e) {}

    if (filePath != "") {
      let goUrl = server.host + filePath;
      axios
        .get(goUrl, {
          responseType: "blob",
        })
        .then((res) => {
          fileDownload(res.data, orignlFileNm);
        });
    } else {
      fileDownload(detailState.uploadFiles[idx], orignlFileNm);
    }
  };

  const handlePreview = (idx, filePath) => {
    previewWindow = window.open(
      "",
      "",
      "width=600,height=400,left=200,top=200"
    );

    if (filePath != "") {
      let goUrl = server.host + filePath;
      previewImg.src = goUrl;
    } else {
      let file = detailState.uploadFiles[idx];
      let reader = new FileReader();

      reader.onload = (function (file) {
        return function (e) {
          previewImg.src = e.target.result;
        };
      })(file);

      reader.readAsDataURL(file);
    }

    previewWindow.document.body.appendChild(previewImg);
  };

  const [textLength, setTextLength] = useState(0);
  const [qsCntnt, setQsCntnt] = useState("");
  const onChangeText = (e) => {
    setTextLength(e.target.value.length);
  };

  /////////////////////////////////////////////////////////////////////////////

  const jsonCompResult = (resJson) => {
    console.log("PROC_CHK : " + PROC_CHK);

    if (resJson.resCd !== "0000") {
      toast.error(
        intl.formatMessage({
          id: "error.during.save",
          defaultMessage: "저장 중 오류가 발생했습니다.",
        })
      );
      return;
    } else {
      if (PROC_CHK === "INS") {
        PROC_CHK = "";
        initSetHandle();
      } else if (PROC_CHK === "UPDHIREDONE") {
        PROC_CHK = "";
        detailState.hireStat = "end";
        reRender();
      } else if (PROC_CHK === "UPDRCMMND") {
        PROC_CHK = "";
        detailState.rcmndEndYn = "Y";
        reRender();
      } else if (PROC_CHK === "SETRECMMNDCNFRMUPD") {
        PROC_CHK = "";
        hireCnddtRcmmdList[seq].rcmndStat = "15";
        reRender();
      } else if (PROC_CHK === "UPDRCMNDMEMO") {
        PROC_CHK = "";
        hireCnddtRcmmdList[seq].empMemo =
          document.getElementById("empMemo").value;
        hireCnddtRcmmdList[seq].dprtMemo =
          document.getElementById("dprtMemo").value;
        reRender();
      } else if (PROC_CHK === "INSRECREPLY") {
        PROC_CHK = "";
        document.getElementById("replyCntnt").value = "";
        onClickStar(0);
        getHireCnddtRcmmdList();
        //reRender();
      } else if (PROC_CHK === "FILEDONW") {
        PROC_CHK = "";
        //alert(resJson.downPath);
        handleZipDownload(resJson.downPath, "이력서.zip");
      } else if (PROC_CHK === "SETALLSTATEUPD") {
        PROC_CHK = "";
        getHireCnddtRcmmdList();
      }
    }
  };

  const cntntsCRUD = async (data) => {
    PROC_CHK = "INS";

    let goUrl = server.path + "/qa/qainsert";
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    jsonCompResult(axiosRes.data);
  };

  const goRcmmndCRUD = async () => {
    PROC_CHK = "UPDRCMMND";

    var data = {
      hireUid: locationState.hireUid,
    };

    let goUrl = server.path + "/hire/hirercmmndend";
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    jsonCompResult(axiosRes.data);

    closeRcmmndEndModal();
  };

  const goHireStatCRUD = async () => {
    PROC_CHK = "UPDHIREDONE";

    var data = {
      hireUid: locationState.hireUid,
    };

    let goUrl = server.path + "/hire/hiredone";
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    jsonCompResult(axiosRes.data);

    closeHireDoneModal();
    window.location.reload();
  };

  const goStateCRUD = async (uid, seq) => {
    PROC_CHK = "SETSTATEUPD";
    var data = {
      mbrUid: locationState.mbrUid,
      rcmndUid: uid,
      rcmndStat: hireCnddtRcmmdList[seq].rcmndStat,
    };

    let goUrl = server.path + "/hire/cnddtrecmmndstatupdate";
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    jsonCompResult(axiosRes.data);
  };

  const goCelpNumPubCRUD = async () => {
    if (
      document.getElementById("contactAgree1").checked == false ||
      document.getElementById("contactAgree2").checked == false
    ) {
      alert(
        intl.formatMessage({
          id: "secret.consent.form",
          defaultMessage: "영업비밀 보호를 위한 동의를해 주세요.",
        })
      );
      return;
    }

    PROC_CHK = "SETCELPNUMPUBUPD";
    var data = {
      mbrUid: locationState.mbrUid,
      rcmndUid: uid,
    };

    let goUrl = server.path + "/hire/cnddtcelpnumpubupdate";
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    jsonCompResult(axiosRes.data);

    closeCelpNumPubModal();
  };

  const goRecmmndCnfrmCRUD = async () => {
    PROC_CHK = "SETRECMMNDCNFRMUPD";
    var data = {
      mbrUid: empUid,
      rcmndUid: uid,
      rcmndStat: "15",
      mbrDivi: "MBRDIVI-99",
    };

    let goUrl = server.path + "/hire/cnddtrecmmndstatupdate";
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    jsonCompResult(axiosRes.data);

    closeRcmmndCnfrmModal();
  };

  const onKeyUpQs = (e) => {
    if (e.key == "Enter") goQaCRUD();
    return false;
  };

  const goQaCRUD = async () => {
    var data = defVal.setAxiosQaCntntState(null);

    data.cntnt = document.getElementById("qsCntnt").value;
    data.mbrUid = ssn.ssnMbrUid;
    data.hireUid = locationState.hireUid;
    data.lvl = 1;
    data.refUid = "";
    data.rcvKnd = uid;

    if (
      commonUtil.CheckIsEmptyFromVal(
        data.cntnt,
        intl.formatMessage({
          id: "error.need.input.content",
          defaultMessage: "내용을 입력해주세요.",
        })
      ) === false
    )
      return;

    document.getElementById("qsCntnt").value = "";

    PROC_CHK = "INSQA";

    let goUrl = server.path + "/qa/qainsert";
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    jsonCompResult(axiosRes.data);

    closeQnaModal();
  };

  const goMemoCRUD = async () => {
    var data = {
      empMemo: document.getElementById("empMemo").value,
      dprtMemo: document.getElementById("dprtMemo").value,
      mbrUid: ssn.ssnMbrUid,
      rcmndUid: uid,
    };

    PROC_CHK = "UPDRCMNDMEMO";

    let goUrl = server.path + "/hire/cnddtrecmmndmemoupdate";
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    jsonCompResult(axiosRes.data);

    closeMemoModal();
  };

  const onClickStar = (seq) => {
    setStar1ActiveState("");
    setStar2ActiveState("");
    setStar3ActiveState("");
    setStar4ActiveState("");
    setStar5ActiveState("");

    if (seq >= 1) setStar1ActiveState("active");
    if (seq >= 2) setStar2ActiveState("active");
    if (seq >= 3) setStar3ActiveState("active");
    if (seq >= 4) setStar4ActiveState("active");
    if (seq >= 5) setStar5ActiveState("active");

    setSeq(seq);
  };

  const goReplyCRUD = async () => {
    var data = {
      cntnt: document.getElementById("replyCntnt").value,
      point: seq,
      wrtrUid: ssn.ssnMbrUid,
      mbrUid: uid,
      hireUid: locationState.hireUid,
    };

    PROC_CHK = "INSRECREPLY";

    let goUrl = server.path + "/hire/recreplyinsert";
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    jsonCompResult(axiosRes.data);

    closeReplyModal();
  };

  ////////////////////////////////////////////////////////////////////////////

  const [modalVisible1, setModalVisible1] = useState(false);
  const [modalVisible2, setModalVisible2] = useState(false);
  const [modalVisible3, setModalVisible3] = useState(false);

  const openModal1 = () => {
    setModalVisible1(true);
  };

  const openModal2 = () => {
    setModalVisible2(true);
  };

  const openModal3 = () => {
    setModalVisible3(true);
  };

  const closeModal1 = () => {
    setModalVisible1(false);
  };

  const closeModal2 = () => {
    setModalVisible2(false);
  };

  const closeModal3 = () => {
    setModalVisible3(false);
  };

  const onChangeCheck = (e, key, idx) => {
    if (hireCnddtRcmmdList[idx].chkYn == "Y")
      hireCnddtRcmmdList[idx].chkYn = "N";
    else hireCnddtRcmmdList[idx].chkYn = "Y";

    reRender();
  };

  const goStateAllCRUD = async () => {
    var chkCnt = 0;
    var tmpList = [];
    for (var k = 0; k < hireCnddtRcmmdList.length; k++) {
      if (hireCnddtRcmmdList[k].chkYn == "Y") {
        tmpList.push(hireCnddtRcmmdList[k].rcmndUid);
        chkCnt++;
      }
    }

    if (chkCnt == 0) {
      alert(
        intl.formatMessage({
          id: "select.candidate",
          defaultMessage: "후보자를 선택해 주세요.",
        })
      );
      return;
    }

    if (document.getElementById("rcmndStatAll").value == "") {
      alert(
        intl.formatMessage({
          id: "select.recruit.step",
          defaultMessage: "채용단계를 선택해 주세요.",
        })
      );
      return;
    }

    //alert(document.getElementById("rcmndStatAll").value);
    //alert(detailState.mbrUid);
    //return;

    PROC_CHK = "SETALLSTATEUPD";
    var data = {
      mbrUid: detailState.mbrUid,
      mbrDivi: ssn.ssnMbrDivi,
      rcmndUid: tmpList,
      rcmndStat: document.getElementById("rcmndStatAll").value,
    };

    let goUrl = server.path + "/hire/cnddtrecmmndstatupdate";
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    jsonCompResult(axiosRes.data);
  };

  const downloadListAll = (e) => {
    var chkCnt = 0;

    var tmpList = [];
    for (var k = 0; k < hireCnddtRcmmdList.length; k++) {
      if (hireCnddtRcmmdList[k].chkYn == "Y") {
        var tmp = hireCnddtRcmmdList[k];
        if (tmp.cnddtCelpNumPubYn != "Y") tmp.cnddtCelpNum = "비공개";
        tmp.rgstDttm = moment(tmp.rgstDttm).format("YYYY-MM-DD");

        tmp.rcmndStatNm = defVal.getCandidateStateCdVal(tmp.rcmndStat);

        tmpList.push(tmp);
      }
    }

    if (tmpList.length == 0) {
      alert(
        intl.formatMessage({
          id: "error.select.candidate.download",
          defaultMessage: "다운로드할 후보자를 선택해 주세요.",
        })
      );

      return;
    }

    setExcelList(tmpList);
  };

  const headers = [
    { label: "후보자명", key: "cnddtNm" },
    { label: "연락처", key: "cnddtCelpNum" },
    { label: "담당리크루터", key: "mbrNm" },
    { label: "추천일시", key: "rgstDttm" },
    { label: "채용단계", key: "rcmndStatNm" },
    { label: "별점", key: "lstReplyPoint" },
    { label: "내용", key: "cnddtIntro" },
  ];

  useEffect(async () => {
    if (excelList.length > 0) {
      console.log("excelList");
      document.getElementById("execelDown").click();
    }
  }, [excelList]);

  return (
    <main className="adminCustom">
      <section className="recruitLists">
        <div className="ingList">
          <h3 className="sectionTitle">
            <span>
              {detailState.hireStat === "ing" ? "진행중" : "종료된"}
              &nbsp;
              <FormattedMessage id="recruitment" defaultMessage="채용공고" />
            </span>
            <em>
              <FormattedMessage
                id="candidate.manage"
                defaultMessage="후보자 관리"
              />
            </em>
          </h3>
          <div className="noRecruitList" style={{ display: "none" }}>
            <FormattedMessage
              id="description.no.ing.posting"
              defaultMessage="진행중인 채용공고가 없습니다."
            />
          </div>
          <ul>
            <li className="ingListColumn">
              <div className="listTop">
                <div className="listLeft">
                  <h3>
                    <Link
                      to="/recruiters/ing-detail"
                      state={{
                        hireUid: detailState.hireUid,
                        mbrUid: detailState.mbrUid,
                      }}
                    >
                      {detailState.title}
                    </Link>
                  </h3>
                  <p>
                    <FormattedMessage
                      id="registration.date"
                      defaultMessage="등록일"
                    />{" "}
                    : &nbsp;{moment(detailState.rgstDttm).format("YYYY-MM-DD")}
                    &nbsp; /{" "}
                    <FormattedMessage
                      id="recruitment.deadline"
                      defaultMessage="인재추천 마감일"
                    />{" "}
                    :{" "}
                    {detailState.rcmndUntilDon === "Y"
                      ? `${intl.formatMessage({
                          id: "until.hired",
                          defaultMessage: "채용시까지",
                        })}`
                      : commonUtil.substr(detailState.rcmndEndDttm, 0, 19)}
                    &nbsp; /{" "}
                    <strong>
                      <FormattedMessage
                        id="recruitment.people.number"
                        defaultMessage="모집인원"
                      />
                      : {detailState.hireSize}
                      <FormattedMessage id="people.count" defaultMessage="명" />
                    </strong>
                  </p>

                  <div>
                    {detailState.rcmndEndYn === "Y" ? (
                      <button type="button" className="recruitingClosed">
                        <FormattedMessage
                          id="recruitment.deadline"
                          defaultMessage="인재추천 마감일"
                        />
                      </button>
                    ) : (
                      <button
                        type="button"
                        className="modalButtonStyling cyanColored"
                        onClick={openRcmmndEndModal}
                      >
                        인재추천 마감하기 &gt;
                      </button>
                    )}

                    {
                      <Modal
                        visible={rcmmndEndModal}
                        closable={true}
                        maskClosable={true}
                        onSubmit={goRcmmndCRUD}
                        onClose={closeRcmmndEndModal}
                        inputName={intl.formatMessage({
                          id: "yes",
                          defaultMessage: "예",
                        })}
                        closeName={intl.formatMessage({
                          id: "no",
                          defaultMessage: "아니오",
                        })}
                      >
                        <>
                          <h4>
                            <FormattedMessage
                              id="recommend.finish"
                              defaultMessage="인재추천 마감하기"
                            />
                          </h4>
                          <div className="alertBox">
                            <FormattedMessage
                              id="recommend.finish.ask"
                              defaultMessage="인재추천을 마감하시겠습니까?"
                            />
                          </div>
                          <p className="textAlignCenter">
                            <FormattedMessage
                              id="recommend.finish.ask"
                              values={{
                                breakLine: <br />,
                              }}
                              defaultMessage="이후 귀사의 채용진행단계에 맞춰 {breakLine}좋은 인재를 채용하시기 바랍니다."
                            />
                          </p>
                        </>
                      </Modal>
                    }

                    {detailState.hireStat === "end" ? (
                      <button
                        type="button"
                        className="recruitingClosed"
                        style={{ marginLeft: "5px" }}
                      >
                        <FormattedMessage
                          id="recruit.finished"
                          defaultMessage="채용공고 종료"
                        />
                      </button>
                    ) : (
                      <button
                        type="button"
                        className="modalButtonStyling marginInline"
                        onClick={openHireDoneModal}
                      >
                        <FormattedMessage
                          id="recruit.finish"
                          defaultMessage="채용공고 종료하기"
                        />{" "}
                        &gt;
                      </button>
                    )}

                    {
                      <Modal
                        visible={hireDoneModal}
                        closable={true}
                        maskClosable={true}
                        onSubmit={goHireStatCRUD}
                        onClose={closeHireDoneModal}
                        inputName={intl.formatMessage({
                          id: "yes",
                          defaultMessage: "예",
                        })}
                        closeName={intl.formatMessage({
                          id: "no",
                          defaultMessage: "아니오",
                        })}
                      >
                        <>
                          <h4>
                            <FormattedMessage
                              id="recruit.finish"
                              defaultMessage="채용공고 종료하기"
                            />
                          </h4>
                          <div className="alertBox">
                            <FormattedMessage
                              id="recruit.finish.ask"
                              defaultMessage="채용공고를 종료하시겠습니까?"
                            />
                          </div>
                          <p className="textAlignCenter">
                            <FormattedMessage
                              id="recruit.finish.description"
                              values={{
                                breakLine: <br />,
                              }}
                              defaultMessage="선택된 리크루터와 함께 {breakLine} 성공적인 인재채용을 기원합니다!"
                            />
                          </p>
                        </>
                      </Modal>
                    }
                  </div>
                </div>
                <div className="flexColumn listRight">
                  <div className="threeSections">
                    <div>
                      <span>
                        <FormattedMessage
                          id="recruiter.suggested"
                          defaultMessage="제안한 리크루터"
                        />
                      </span>
                      <Link
                        to="/admin/recruiter-manage"
                        state={{
                          hireUid: detailState.hireUid,
                          mbrUid: detailState.mbrUid,
                        }}
                      >
                        {detailState.cmmntCnt}
                        <FormattedMessage
                          id="people.count"
                          defaultMessage="명"
                        />
                      </Link>
                    </div>
                    <div>
                      <span>
                        <FormattedMessage
                          id="recruiter.selected"
                          defaultMessage="선택한 리크루터"
                        />
                      </span>
                      <Link
                        to="/admin/recruiter-manage"
                        state={{
                          hireUid: detailState.hireUid,
                          mbrUid: detailState.mbrUid,
                        }}
                      >
                        <strong>{detailState.cmmntChooseCnt}</strong>/
                        {mbrCnt.recTotCnt}
                        <FormattedMessage
                          id="people.count"
                          defaultMessage="명"
                        />
                      </Link>
                    </div>
                    <div>
                      <span>
                        <FormattedMessage
                          id="candidate.recommended"
                          defaultMessage="추천된 후보자"
                        />
                      </span>
                      <Link
                        to="/admin/candidate-manage"
                        state={{
                          hireUid: detailState.hireUid,
                          mbrUid: detailState.mbrUid,
                        }}
                      >
                        <strong>0</strong>/{detailState.recmmndCnt}
                        <FormattedMessage
                          id="people.count"
                          defaultMessage="명"
                        />
                      </Link>
                    </div>
                  </div>
                  <div className="qnaStatus">
                    <Link
                      to="/admin/qna-manage"
                      state={{
                        hireUid: detailState.hireUid,
                        mbrUid: detailState.mbrUid,
                      }}
                    >
                      Q&amp;A{" "}
                      <span>
                        {detailState.qaCnt}
                        <FormattedMessage id="count" defaultMessage="개" />
                      </span>
                    </Link>
                  </div>
                  <p>
                    <FormattedMessage
                      id="description.open.to.recruiter"
                      defaultMessage="* 채용진행 현황은 참여중인 리크루터에게도 공개됩니다."
                    />
                  </p>
                </div>
              </div>
            </li>
          </ul>
        </div>
        <div className="candidateList">
          <div className="flexSpaceBetween">
            <div>
              <input
                type="checkbox"
                id="selectAll"
                name="selectAll"
                className="marginInline"
                value="N"
                onClick={(e) => {
                  chkeckAll(e);
                }}
              />
              <label htmlFor="selectAll">
                <FormattedMessage id="select.all" defaultMessage="전체선택" />
              </label>
            </div>
            <div className="candidateButtons">
              <CSVLink
                id="execelDown"
                filename={`추천된후보자리스트.xls`}
                data={excelList}
                headers={headers}
              ></CSVLink>

              <button
                type="button"
                className="basicButton"
                onClick={(e) => {
                  downloadListAll(e);
                }}
              >
                <FormattedMessage
                  id="candidate.recommended.list.download"
                  defaultMessage="추천된 후보자 리스트 다운로드"
                />
              </button>

              <select
                className="recruitResult"
                name="rcmndStatAll"
                id="rcmndStatAll"
              >
                <option value="">
                  ::{" "}
                  {intl.formatMessage({
                    id: "recruit.step",
                    defaultMessage: "채용단계",
                  })}{" "}
                  ::
                </option>
                {candidateStateCd.map((list, idx) => (
                  <option value={list.cateCd} className={list.clssName}>
                    {list.cdName}
                  </option>
                ))}
              </select>

              <button
                type="button"
                className="blueButton"
                onClick={() => {
                  goStateAllCRUD();
                }}
              >
                <FormattedMessage
                  id="all.result.notice"
                  defaultMessage="일괄 결과통보"
                />{" "}
                &gt;
              </button>

              <button
                type="button"
                className="blueButton"
                onClick={(e) => {
                  downloadAll(e);
                }}
              >
                <FormattedMessage
                  id="selected.resume.download"
                  defaultMessage="선택한 이력서 일괄 다운로드"
                />{" "}
                &gt;
              </button>
            </div>
          </div>

          <form className="flex employersForm marginTopBottom">
            <input
              type="search"
              placeholder={intl.formatMessage({
                id: "placeholder.candidate.name",
                defaultMessage: "후보자 이름을 입력하세요.",
              })}
            />
            <input
              type="submit"
              value={intl.formatMessage({
                id: "search",
                defaultMessage: "검색",
              })}
              className="basicButton"
            />
          </form>

          <ul className="recruiters">
            {hireCnddtRcmmdList.map((list, idx) => (
              <li>
                <div className="candidateInfo">
                  <div>
                    <span className="candidateName">
                      <input
                        type="checkbox"
                        checked={list.chkYn === "Y" ? true : false}
                        onChange={(e) => {
                          onChangeCheck(e, "chkYn", idx);
                        }}
                      />
                      &nbsp;{list.cnddtNm}
                      <em>&nbsp;({list.replyCnt})</em>
                    </span>

                    <span>
                      <FormattedMessage id="contact" defaultMessage="연락처" />{" "}
                      | &nbsp;
                      {list.cnddtCelpNumPubYn !== "Y" ? (
                        <b className="red">
                          <FormattedMessage
                            id="secret"
                            defaultMessage="비공개"
                          />
                        </b>
                      ) : (
                        list.cnddtCelpNum
                      )}
                    </span>

                    <button
                      type="button"
                      className="basicButton"
                      onClick={() => {
                        openCelpNumPubModal(list.rcmndUid, idx, list.cnddtNm);
                      }}
                    >
                      <FormattedMessage
                        id="candidate.contact.open.request"
                        defaultMessage="후보자 연락처 공개요청"
                      />{" "}
                      &gt;
                    </button>
                    <button type="button" className="basicButton">
                      <FormattedMessage
                        id="candidate.info.delete"
                        defaultMessage="후보자 정보 삭제"
                      />
                    </button>
                  </div>
                  {
                    <Modal
                      visible={modalVisible1}
                      closable={true}
                      maskClosable={true}
                      onClose={closeModal1}
                      inputName={intl.formatMessage({
                        id: "yes",
                        defaultMessage: "예",
                      })}
                      closeName={intl.formatMessage({
                        id: "no",
                        defaultMessage: "아니오",
                      })}
                    >
                      <CandidateContact />
                    </Modal>
                  }
                  <p>
                    <FormattedMessage
                      id="recruiter.charged"
                      defaultMessage="담당 리크루터"
                    />
                    : &nbsp;
                    <strong style={{ marginRight: "5px" }}>{list.mbrNm}</strong>
                    <button
                      type="button"
                      style={{ marginRight: "5px" }}
                      className="modalOpenButton"
                      onClick={(e) => {
                        openReplyModal(list.mbrUid, idx, list.mbrNm);
                      }}
                    >
                      [
                      <FormattedMessage
                        id="evaluate"
                        defaultMessage="평가하기"
                      />
                      ]
                    </button>
                    <button
                      type="button"
                      className="modalOpenButton"
                      onClick={(e) => {
                        openQnaModal(list.mbrUid, idx, list.mbrNm);
                      }}
                    >
                      [
                      <FormattedMessage
                        id="send.memo"
                        defaultMessage="쪽지보내기"
                      />
                      ]
                    </button>
                    <time>
                      <FormattedMessage
                        id="recommend.date"
                        defaultMessage="추천일시"
                      />{" "}
                      : {moment(list.rgstDttm).format("YYYY-MM-DD")}
                    </time>
                  </p>
                  {
                    <Modal
                      visible={modalVisible2}
                      closable={true}
                      maskClosable={true}
                      onClose={closeModal2}
                      inputName={intl.formatMessage({
                        id: "send.memo",
                        defaultMessage: "쪽지보내기",
                      })}
                      closeName={intl.formatMessage({
                        id: "no",
                        defaultMessage: "아니오",
                      })}
                    >
                      <SendingNote />
                    </Modal>
                  }

                  <p style={{ whiteSpace: "pre-wrap" }}>{list.cnddtIntro}</p>
                </div>
                <div className="notification">
                  <button
                    type="button"
                    className="basicButton"
                    onClick={() => {
                      handleDownload(idx, list.cnddtThmnl, list.cnddtAtchFiles);
                    }}
                  >
                    <FormattedMessage
                      id="resume.download"
                      defaultMessage="이력서 다운로드"
                    />{" "}
                    &gt;
                  </button>
                  <form>
                    <select
                      className="recruitResult"
                      value={list.rcmndStat}
                      onChange={(e) => {
                        hireCnddtRcmmdList[idx].rcmndStat = e.target.value;
                        reRender();
                      }}
                    >
                      <option value="01">
                        ::{" "}
                        {intl.formatMessage({
                          id: "recruit.step",
                          defaultMessage: "채용단계",
                        })}{" "}
                        ::
                      </option>
                      {candidateStateCd.map((list, idx) =>
                        idx > 0 ? (
                          <option value={list.cateCd} className={list.clssName}>
                            {list.cdName}
                          </option>
                        ) : (
                          ""
                        )
                      )}
                    </select>

                    <input
                      type="submit"
                      className="basicButton"
                      value={`${intl.formatMessage({
                        id: "result.notice",
                        defaultMessage: "결과통보",
                      })} >`}
                      onClick={() => {
                        goStateCRUD(list.rcmndUid, idx);
                      }}
                    />
                  </form>
                  {list.rcmndStat === "15" ? (
                    <div className="finalDecisionDone">
                      <FormattedMessage
                        id="status.recruit.decide"
                        defaultMessage="최종 채용 확정"
                      />
                    </div>
                  ) : (
                    <button
                      type="button"
                      className="blueButton"
                      onClick={() => {
                        openRcmmndCnfrmModal(
                          list.rcmndUid,
                          idx,
                          list.cnddtNm,
                          list.mbrUid
                        );
                      }}
                    >
                      <FormattedMessage
                        id="status.recruit.decide"
                        defaultMessage="최종 채용 확정"
                      />
                    </button>
                  )}
                </div>
              </li>
            ))}
          </ul>
        </div>
      </section>

      {
        /*최종 채용확정*/
        <Modal
          visible={rcmmndCnfrmModal}
          closable={true}
          maskClosable={true}
          onSubmit={goRecmmndCnfrmCRUD}
          onClose={closeRcmmndCnfrmModal}
          inputName={intl.formatMessage({
            id: "yes",
            defaultMessage: "예",
          })}
          closeName={intl.formatMessage({
            id: "no",
            defaultMessage: "아니오",
          })}
        >
          <>
            <h4>
              <FormattedMessage
                id="recruit.decide"
                defaultMessage="최종 채용 확정하기"
              />
            </h4>
            <div className="alertBox">
              <FormattedMessage
                id="candidate.name"
                defaultMessage="후보자 이름"
              />{" "}
              : {cnddtNm}
            </div>
            <p className="flexAlignCenter textAlignCenter">
              <FormattedMessage
                id="description.recruit.decide"
                defaultMessage="채용을 확정합니다."
              />
            </p>
          </>
        </Modal>
      }

      {
        <Modal
          visible={celpNumPubModal}
          closable={true}
          maskClosable={true}
          onSubmit={goCelpNumPubCRUD}
          onClose={closeCelpNumPubModal}
          inputName={intl.formatMessage({
            id: "agree",
            defaultMessage: "이에 동의합니다.",
          })}
          closeName={intl.formatMessage({
            id: "no",
            defaultMessage: "아니오",
          })}
        >
          <h4>
            <FormattedMessage
              id="candidate.contact.open.request"
              defaultMessage="후보자 연락처 공개요청"
            />
          </h4>
          <div className="alertBox">
            <FormattedMessage id="candidate" defaultMessage="후보자" /> :{" "}
            {cnddtNm}
          </div>
          <p className="textAlignCenter">
            <FormattedMessage
              id="description.request.recruiter.contact"
              defaultMessage="면접을 위해 담당 리크루터에게 후보자의 휴대폰번호와 이메일주소공개를 요청합니다."
            />
          </p>
          <h5>
            [{" "}
            <FormattedMessage
              id="confidential.consent.form.title"
              defaultMessage="영업비밀 보호를 위한 동의서"
            />{" "}
            ]
          </h5>

          <div className="checkboxWrap">
            <input type="checkbox" id="contactAgree1" />
            <label className="width500">
              <FormattedMessage
                id="confidential.consent.form.check1"
                defaultMessage="(필수) 리크루터의 영업비밀 보호를 위해 연락처가 공개되는 경우 공개일로부터 향후 1년 동안 본 채용건 이외의 건으로 후보자를 채용하지 않겠습니다."
              />
            </label>
          </div>

          <div className="checkboxWrap">
            <input type="checkbox" id="contactAgree2" />
            <label className="width500">
              <FormattedMessage
                id="confidential.consent.form.check2"
                defaultMessage="(필수) 해당 기간내 후보자를 채용하는 경우에는 소정의 리크루팅 수수료를 지불하겠습니다."
              />
            </label>
          </div>
        </Modal>
      }
      {
        <Modal
          visible={qnaModal}
          closable={true}
          maskClosable={true}
          onSubmit={goQaCRUD}
          onClose={closeQnaModal}
          inputName={intl.formatMessage({
            id: "send.memo",
            defaultMessage: "쪽지보내기",
          })}
          closeName={intl.formatMessage({
            id: "no",
            defaultMessage: "아니오",
          })}
        >
          <>
            <h4>
              <FormattedMessage
                id="recruiter.send.memo"
                defaultMessage="리크루터에게 쪽지보내기"
              />
            </h4>
            <div className="alertBox">{cnddtNm}</div>
            <p className="marginTopBottom">
              <FormattedMessage
                id="description.only.reveal.to.recruiter"
                values={{
                  breakLine: <br />,
                }}
                defaultMessage="문의 내용은 Q&amp;A 게시판에 작성되며 {breakLine} 작성내용은 해당 리크루터만 확인 가능합니다."
              />
            </p>
            <form className="employersForm">
              <textarea
                name="qsCntnt"
                id="qsCntnt"
                cols={100}
                rows={10}
                maxLength="100"
                onChange={onChangeText}
                onKeyUp={onKeyUpQs}
              />
              <span className="flexAlignRight">{textLength}/100</span>
            </form>
          </>
        </Modal>
      }

      {
        <Modal
          visible={replyModal}
          closable={true}
          maskClosable={true}
          onSubmit={goReplyCRUD}
          onClose={closeReplyModal}
          inputName={intl.formatMessage({
            id: "evaluate",
            defaultMessage: "평가하기",
          })}
          closeName={intl.formatMessage({
            id: "no",
            defaultMessage: "아니오",
          })}
        >
          <h4>
            <FormattedMessage
              id="recruiter.evaluate"
              defaultMessage="리쿠르터 평가하기"
            />
          </h4>
          <div
            style={{
              fontSize: "19px",
              marginTop: "20px",
              marginBottom: "20px",
              fontWeight: "bold",
            }}
          >
            <FormattedMessage id="recruitment" defaultMessage="채용공고" /> :{" "}
            {detailState.title}
          </div>
          <div className="alertBox">
            <FormattedMessage id="candidate" defaultMessage="후보자" /> :{" "}
            {cnddtNm}
          </div>

          <div
            className="stars"
            style={{ textAlign: "center", marginBottom: "20px" }}
          >
            <span
              className={"material-icons-outlined " + star1ActiveState}
              id="star1Off"
              style={{ fontSize: "50px", cursor: "pointer" }}
              onClick={() => {
                onClickStar(1);
              }}
            >
              grade
            </span>
            <span
              className={"material-icons-outlined " + star2ActiveState}
              id="star2Off"
              style={{ fontSize: "50px", cursor: "pointer" }}
              onClick={() => {
                onClickStar(2);
              }}
            >
              grade
            </span>
            <span
              className={"material-icons-outlined " + star3ActiveState}
              id="star3Off"
              style={{ fontSize: "50px", cursor: "pointer" }}
              onClick={() => {
                onClickStar(3);
              }}
            >
              grade
            </span>
            <span
              className={"material-icons-outlined " + star4ActiveState}
              id="star4Off"
              style={{ fontSize: "50px", cursor: "pointer" }}
              onClick={() => {
                onClickStar(4);
              }}
            >
              grade
            </span>
            <span
              className={"material-icons-outlined " + star5ActiveState}
              id="star5Off"
              style={{ fontSize: "50px", cursor: "pointer" }}
              onClick={() => {
                onClickStar(5);
              }}
            >
              grade
            </span>
          </div>

          <div style={{ textAlign: "center", marginBottom: "20px" }}>
            <FormattedMessage
              id="description.secret.to.recruiter"
              defaultMessage="평가 코멘트 내용은 리쿠르터는 볼 수 없습니다."
            />
          </div>

          <form className="employersForm">
            <textarea name id="replyCntnt" cols={30} rows={10} />
          </form>
        </Modal>
      }

      {
        <Modal
          visible={memoModal}
          closable={true}
          maskClosable={true}
          onSubmit={goMemoCRUD}
          onClose={closeMemoModal}
          inputName={intl.formatMessage({
            id: "saving",
            defaultMessage: "저장",
          })}
          closeName={intl.formatMessage({
            id: "no",
            defaultMessage: "아니오",
          })}
        >
          <h4>
            <FormattedMessage
              id="candidate.memo"
              defaultMessage="후보자 메모"
            />
          </h4>
          <div className="alertBox">
            <FormattedMessage id="candidate" defaultMessage="후보자" /> :{" "}
            {cnddtNm}
          </div>
          <form className="employersForm">
            <label htmlFor="empMemo">
              <FormattedMessage
                id="employer.comment"
                defaultMessage="인사담당자 코멘트"
              />{" "}
              :
            </label>
            <textarea name id="empMemo" cols={30} rows={10} />
            <label htmlFor="dprtMemo">
              <FormattedMessage
                id="department.person.comment"
                defaultMessage="현업부서 담당자 코멘트"
              />{" "}
              :
            </label>
            <textarea name id="dprtMemo" cols={30} rows={10} />
          </form>
        </Modal>
      }
    </main>
  );
};
