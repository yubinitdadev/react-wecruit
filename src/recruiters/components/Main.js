import * as commonUtil from "commons/modules/commonUtil";
import { Context } from "contexts";
import { UPD_MBRUID } from "contexts/actionTypes";
import { useContext, useEffect, useRef, useState } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import { CopyToClipboard } from "react-copy-to-clipboard";
import { FormattedMessage, useIntl } from "react-intl";
import CandidateContact from "./common/CandidateContact";

export const Main = () => {
  const intl = useIntl();

  const {
    state: { server, mbr },
    dispatch,
  } = useContext(Context);

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid"),
    ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi"),
  };

  const location = useLocation();
  const navigate = useNavigate();
  const moment = require("moment");
  let previewWindow = "";
  let previewImg = document.createElement("img");
  const componentRef = useRef();

  const [mbrCnt, setMbrCnt] = useState({});
  const [listState, setListState] = useState([]);
  const [listCntState, setListCntState] = useState({});
  const [sConts, setSConts] = useState("");
  const [fltrState, setFltrState] = useState({});

  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(10);

  let prtnrChkCnt = 0;
  let ingChkCnt = 0;
  let cmmntChkCnt = 0;
  let endChkCnt = 0;

  useEffect(async () => {
    console.log("Page First Load EFFECT");
    await initSetHandle();
  }, []);

  const initSetHandle = async () => {
    let goUrl = server.path + "/hire/hirelist";
    let data = {
      ssnMbrUid: ssn.ssnMbrUid,
      mbrUid: "",
      mbrDivi: "MBRDIVI-02",
      sRecMbrUid: ssn.ssnMbrUid,
      sRcvMbrUid: ssn.ssnMbrUid,
      hireStat: "",
    };

    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");
    let axiosResData = axiosRes.data.hireArr;
    let tmpAxiosResData = [];

    let prtnrCnt = 0;
    let ingCnt = 0;
    let cmmntCnt = 0;
    let endCnt = 0;

    axiosResData.forEach((resData, idx) => {
      resData["rcmndEndDttm"] = moment(resData.rcmndEndDttm).format(
        "YYYY-MM-DD HH:mm"
      );

      //거래고객사( 써치펌에 리크루터가 등록되고 리크루터를 채택하지 않은 상태 )
      if (
        resData.hireStat == "ing" &&
        resData.myCmmntCnt == 0 &&
        resData.mySrchFirmMappCnt > 0
      )
        prtnrCnt++;
      //제안한 채용공고( 리크루터를 채택하지 않은 상태 )
      if (
        resData.hireStat == "ing" &&
        resData.myCmmntCnt > 0 &&
        resData.myCmmntChooseCnt == 0
      )
        cmmntCnt++;
      //진행중 채용공고( 리크루터를 채택한 상태 )
      if (resData.hireStat == "ing" && resData.myCmmntChooseCnt > 0) ingCnt++;
      //종료 채용공고( 리크루터가 제안했거나 거래고객사인 상태 )
      else if (resData.hireStat == "end" && resData.myCmmntChooseCnt > 0)
        endCnt++;

      tmpAxiosResData.push(resData);
    });

    setListState(tmpAxiosResData);
    var tmpCnt = {
      prtnrCnt: prtnrCnt,
      ingCnt: ingCnt,
      cmmntCnt: cmmntCnt,
      endCnt: endCnt,
    };
    setListCntState(tmpCnt);

    var tmpMbrCnt = await getMbrCnt();
  };

  const getMbrCnt = async () => {
    let goUrl = server.path + "/mbr/mbrcnt";
    let data = {
      hireUid: "",
    };

    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");
    let tmpDate = axiosRes.data.mbrcnt;
    setMbrCnt(tmpDate);
    return tmpDate;
  };

  ////////////////////////////////////////////////////////////////////////////////////

  const onKeyUp = (e) => {
    if (e.key == "Enter") {
      document.getElementById("srchLink").click();
    }
    return false;
  };

  function onKeyDown(keyEvent) {
    if ((keyEvent.charCode || keyEvent.keyCode) === 13) {
      keyEvent.preventDefault();
    }
  }

  return (
    <main className="dividedSections recruitersMain noBorder">
      <section className="recruitNoticeStats">
        <div>
          <p>
            <FormattedMessage
              id="posting.suggested"
              defaultMessage="제안한 채용공고"
            />
          </p>
          <Link to="/recruiters/suggest">{listCntState.cmmntCnt}</Link>
        </div>
        <div>
          <p>
            <FormattedMessage
              id="posting.ing"
              defaultMessage="진행중 채용공고"
            />
          </p>
          <Link to="/recruiters/ing">{listCntState.ingCnt}</Link>
        </div>
        <div>
          <p>
            <FormattedMessage
              id="posting.done"
              defaultMessage="종료된 채용공고"
            />
          </p>
          <Link to="/recruiters/done">{listCntState.endCnt}</Link>
        </div>
      </section>
      <section>
        <form
          className="searchForm recruitersForm"
          onKeyDown={(e) => {
            onKeyDown(e);
          }}
        >
          <input
            type="search"
            placeholder={intl.formatMessage({
              id: "placeholder.search.word",
              defaultMessage: "검색어를 입력하세요.",
            })}
            id="sConts"
            onChange={(e) => {
              setSConts(e.target.value);
            }}
            onKeyUp={(e) => {
              onKeyUp(e);
            }}
          />
          <Link
            id="srchLink"
            name="srchLink"
            to="/recruiters/search"
            state={{ sConts: sConts }}
          >
            <input
              type="button"
              className="basicButton"
              value={intl.formatMessage({
                id: "search",
                defaultMessage: "검색",
              })}
            />
          </Link>
        </form>
      </section>
      <section className="recruitLists">
        <div className="suggestList">
          <h2 className="sectionTitle">
            <span>
              <FormattedMessage
                id="posting.partner"
                defaultMessage="거래 고객사의 채용공고"
              />
            </span>
            <Link to="/recruiters/partner">
              <FormattedMessage id="show.all" defaultMessage="전체보기" /> &gt;
            </Link>
          </h2>

          <div
            className="noRecruitList"
            style={{ display: listCntState.prtnrCnt == 0 ? "" : "none" }}
          >
            <FormattedMessage
              id="description.no.partner.posting"
              defaultMessage="거래 고객사의 채용공고가 없습니다."
            />
          </div>

          <ul>
            {listState.map((list, idx) =>
              list.hireStat == "ing" &&
              list.myCmmntCnt == 0 &&
              list.mySrchFirmMappCnt > 0 ? (
                (prtnrChkCnt = prtnrChkCnt + 1) < 4 ? (
                  <li>
                    <div className="listLeft">
                      <h3>
                        <Link
                          to="/recruiters/partner-detail"
                          state={{ hireUid: list.hireUid, mbrUid: list.mbrUid }}
                        >
                          [{list.pubYn == "Y" ? list.bizNm : "*****"}]{" "}
                          {list.title}
                        </Link>
                      </h3>
                      <p>
                        <FormattedMessage
                          id="registration.date"
                          defaultMessage="등록일"
                        />{" "}
                        : &nbsp;{moment(list.rgstDttm).format("YYYY-MM-DD")}
                        &nbsp; /{" "}
                        <FormattedMessage
                          id="recruitment.deadline"
                          defaultMessage="인재추천 마감일"
                        />{" "}
                        :{" "}
                        {list.rcmndUntilDon === "Y"
                          ? `${intl.formatMessage({
                              id: "until.hired",
                              defaultMessage: "채용시까지",
                            })}`
                          : list.rcmndEndDttm}
                        &nbsp; /{" "}
                        <strong>
                          <FormattedMessage
                            id="recruitment.people.number"
                            defaultMessage="모집인원"
                          />{" "}
                          : {list.hireSize}
                          <FormattedMessage
                            id="people.count"
                            defaultMessage="명"
                          />
                        </strong>
                      </p>
                      <ul>
                        <li>
                          <FormattedMessage
                            id="suggestion.service.fee"
                            defaultMessage="서비스 수수료율"
                          />{" "}
                          : &nbsp;
                          {list.srvcFeeFixYn === "Y"
                            ? `${intl.formatMessage({
                                id: "suggestion.fixed.fee.system",
                                defaultMessage: "정액제",
                              })}`
                            : list.srvcFeePctMin +
                              "% ~ " +
                              list.srvcFeePctMax +
                              "%"}
                        </li>
                        <li>
                          <FormattedMessage
                            id="suggestion.service.guarantee"
                            defaultMessage="서비스 보증조건"
                          />{" "}
                          : &nbsp;
                          {list.srvcWrtyPeriod}
                          <FormattedMessage
                            id="months"
                            defaultMessage="개월"
                          />{" "}
                          <FormattedMessage
                            id="suggestion.incase.retire"
                            defaultMessage="이내 퇴직시"
                          />
                          {list.srvcWrtyMthd === "prorated"
                            ? `${intl.formatMessage({
                                id: "suggestion.caculated.refund",
                                defaultMessage: "일할계산 환불",
                              })}`
                            : list.srvcWrtyMthd === "all"
                            ? `${intl.formatMessage({
                                id: "suggestion.all.refund",
                                defaultMessage: "100% 전액 환불",
                              })}`
                            : list.srvcWrtyMthd === "etc"
                            ? `${intl.formatMessage({
                                id: "etc",
                                defaultMessage: "기타",
                              })}`
                            : ""}
                        </li>
                      </ul>
                    </div>
                    <div className="listRight">
                      <div className="threeSections">
                        <div>
                          <span>
                            <FormattedMessage
                              id="recruiter.suggested"
                              defaultMessage="제안한 리크루터"
                            />
                          </span>
                          <em>
                            {list.cmmntCnt}
                            <FormattedMessage
                              id="people.count"
                              defaultMessage="명"
                            />
                          </em>
                        </div>
                        <div>
                          <span>
                            <FormattedMessage
                              id="recruiter.selected"
                              defaultMessage="선택한 리크루터"
                            />
                          </span>
                          <em>
                            <strong>{list.cmmntChooseCnt}</strong>/
                            {list.recSize}
                            <FormattedMessage
                              id="people.count"
                              defaultMessage="명"
                            />
                          </em>
                        </div>
                        <div>
                          <span>
                            <FormattedMessage
                              id="candidate.recommended"
                              defaultMessage="추천된 후보자"
                            />
                          </span>
                          <em>
                            {list.recmmndCnt}
                            <FormattedMessage
                              id="people.count"
                              defaultMessage="명"
                            />
                            ({list.chkingRecmmndCnt}
                            <FormattedMessage
                              id="people.count"
                              defaultMessage="명"
                            />
                            /{list.nopassRecmmndCnt}
                            <FormattedMessage
                              id="people.count"
                              defaultMessage="명"
                            />
                            )
                          </em>
                        </div>
                      </div>
                      <div className="qnaStatus">
                        <Link
                          to="/recruiters/qna"
                          state={{ hireUid: list.hireUid, mbrUid: list.mbrUid }}
                        >
                          Q&amp;A{" "}
                          <span>
                            {list.qaCnt}
                            <FormattedMessage id="count" defaultMessage="개" />\
                          </span>
                        </Link>
                      </div>
                    </div>
                  </li>
                ) : (
                  ""
                )
              ) : (
                ""
              )
            )}
          </ul>
        </div>

        <div className="suggestList">
          <h2 className="sectionTitle">
            <span>
              <FormattedMessage
                id="posting.suggested"
                defaultMessage="제안한 채용공고"
              />
            </span>
            <Link to="/recruiters/suggest">
              <FormattedMessage id="show.all" defaultMessage="전체보기" /> &gt;
            </Link>
          </h2>

          <div
            className="noRecruitList"
            style={{ display: listCntState.cnfrmCnt == 0 ? "" : "none" }}
          >
            <FormattedMessage
              id="description.no.partner.posting"
              defaultMessage="제안한 채용공고가 없습니다."
            />
          </div>

          <ul>
            {listState.map((list, idx) =>
              list.hireStat === "ing" &&
              list.myCmmntCnt > 0 &&
              list.myCmmntChooseCnt == 0 ? (
                (cmmntChkCnt = cmmntChkCnt + 1) < 4 ? (
                  <li>
                    <div className="listLeft">
                      <h3>
                        <Link
                          to="/recruiters/suggest-detail"
                          state={{ hireUid: list.hireUid, mbrUid: list.mbrUid }}
                        >
                          [{list.pubYn == "Y" ? list.bizNm : "*****"}]{" "}
                          {list.title}
                        </Link>
                      </h3>
                      <p>
                        <FormattedMessage
                          id="registration.date"
                          defaultMessage="등록일"
                        />{" "}
                        : &nbsp;{moment(list.rgstDttm).format("YYYY-MM-DD")}
                        &nbsp; /{" "}
                        <FormattedMessage
                          id="recruitment.deadline"
                          defaultMessage="인재추천 마감일"
                        />{" "}
                        :{" "}
                        {list.rcmndUntilDon === "Y"
                          ? `${intl.formatMessage({
                              id: "until.hired",
                              defaultMessage: "채용시까지",
                            })}`
                          : list.rcmndEndDttm}
                        &nbsp; /{" "}
                        <strong>
                          <FormattedMessage
                            id="recruitment.people.number"
                            defaultMessage="모집인원"
                          />{" "}
                          : {list.hireSize}
                          <FormattedMessage
                            id="people.count"
                            defaultMessage="명"
                          />
                        </strong>
                      </p>
                      <ul>
                        <li>
                          <FormattedMessage
                            id="suggestion.service.fee"
                            defaultMessage="서비스 수수료율"
                          />{" "}
                          : &nbsp;
                          {list.srvcFeeFixYn === "Y"
                            ? `${intl.formatMessage({
                                id: "suggestion.fixed.fee.system",
                                defaultMessage: "정액제",
                              })}`
                            : list.srvcFeePctMin +
                              "% ~ " +
                              list.srvcFeePctMax +
                              "%"}
                        </li>
                        <li>
                          <FormattedMessage
                            id="suggestion.service.guarantee"
                            defaultMessage="서비스 보증조건"
                          />{" "}
                          : &nbsp;
                          {list.srvcWrtyPeriod}
                          <FormattedMessage
                            id="months"
                            defaultMessage="개월"
                          />{" "}
                          <FormattedMessage
                            id="suggestion.incase.retire"
                            defaultMessage="이내 퇴직시"
                          />
                          {list.srvcWrtyMthd === "prorated"
                            ? `${intl.formatMessage({
                                id: "suggestion.caculated.refund",
                                defaultMessage: "일할계산 환불",
                              })}`
                            : list.srvcWrtyMthd === "all"
                            ? `${intl.formatMessage({
                                id: "suggestion.all.refund",
                                defaultMessage: "100% 전액 환불",
                              })}`
                            : list.srvcWrtyMthd === "etc"
                            ? `${intl.formatMessage({
                                id: "etc",
                                defaultMessage: "기타",
                              })}`
                            : ""}
                        </li>
                      </ul>
                    </div>
                    <div className="listRight">
                      <div className="status">
                        <FormattedMessage
                          id="suggestion.ing"
                          defaultMessage="제안 진행중"
                        />
                      </div>

                      <div className="threeSections">
                        <div>
                          <span>
                            <FormattedMessage
                              id="recruiter.suggested"
                              defaultMessage="제안한 리크루터"
                            />
                          </span>
                          <em>
                            {list.cmmntCnt}
                            <FormattedMessage
                              id="people.count"
                              defaultMessage="명"
                            />
                          </em>
                        </div>
                        <div>
                          <span>
                            <FormattedMessage
                              id="recruiter.selected"
                              defaultMessage="선택한 리크루터"
                            />
                          </span>
                          <em>
                            <strong>{list.cmmntChooseCnt}</strong>/
                            {list.recSize}
                            <FormattedMessage
                              id="people.count"
                              defaultMessage="명"
                            />
                          </em>
                        </div>
                        <div>
                          <span>
                            <FormattedMessage
                              id="candidate.recommended"
                              defaultMessage="추천된 후보자"
                            />
                          </span>
                          <em>
                            {list.recmmndCnt}
                            <FormattedMessage
                              id="people.count"
                              defaultMessage="명"
                            />
                            ({list.chkingRecmmndCnt}
                            <FormattedMessage
                              id="people.count"
                              defaultMessage="명"
                            />
                            /{list.nopassRecmmndCnt}
                            <FormattedMessage
                              id="people.count"
                              defaultMessage="명"
                            />
                            )
                          </em>
                        </div>
                      </div>
                      <div className="qnaStatus">
                        <Link
                          to="/recruiters/qna"
                          state={{ hireUid: list.hireUid, mbrUid: list.mbrUid }}
                        >
                          Q&amp;A{" "}
                          <span>
                            {list.qaCnt}
                            <FormattedMessage id="count" defaultMessage="개" />
                          </span>
                        </Link>
                      </div>
                    </div>
                  </li>
                ) : (
                  ""
                )
              ) : (
                ""
              )
            )}
          </ul>
        </div>

        <div className="ingList">
          <h2 className="sectionTitle">
            <span>
              <FormattedMessage
                id="posting.ing"
                defaultMessage="진행중 채용공고"
              />
            </span>
            <Link to="/recruiters/ing">
              <FormattedMessage id="show.all" defaultMessage="전체보기" /> &gt;
            </Link>
          </h2>
          <div
            className="noRecruitList"
            style={{ display: listCntState.ingCnt == 0 ? "" : "none" }}
          >
            <FormattedMessage
              id="description.no.ing.posting"
              defaultMessage="진행중인 채용공고가 없습니다."
            />
          </div>

          <ul>
            {listState.map((list, idx) =>
              list.hireStat === "ing" && list.myCmmntChooseCnt > 0 ? (
                (ingChkCnt = ingChkCnt + 1) < 4 ? (
                  <li>
                    <div className="listLeft">
                      <h3>
                        <Link
                          to="/recruiters/ing-detail"
                          state={{ hireUid: list.hireUid, mbrUid: list.mbrUid }}
                        >
                          [{list.pubYn == "Y" ? list.bizNm : "*****"}]{" "}
                          {list.title}
                        </Link>
                      </h3>
                      <p>
                        <FormattedMessage
                          id="registration.date"
                          defaultMessage="등록일"
                        />{" "}
                        : &nbsp;{moment(list.rgstDttm).format("YYYY-MM-DD")}
                        &nbsp; /{" "}
                        <FormattedMessage
                          id="recruitment.deadline"
                          defaultMessage="인재추천 마감일"
                        />{" "}
                        :{" "}
                        {list.rcmndUntilDon === "Y"
                          ? `${intl.formatMessage({
                              id: "until.hired",
                              defaultMessage: "채용시까지",
                            })}`
                          : list.rcmndEndDttm}
                        &nbsp; /{" "}
                        <strong>
                          <FormattedMessage
                            id="recruitment.people.number"
                            defaultMessage="모집인원"
                          />
                          : {list.hireSize}
                          <FormattedMessage
                            id="people.count"
                            defaultMessage="명"
                          />
                        </strong>
                      </p>
                      <ul>
                        <li>
                          <FormattedMessage
                            id="suggestion.service.fee"
                            defaultMessage="서비스 수수료율"
                          />{" "}
                          : &nbsp;
                          {list.srvcFeeFixYn === "Y"
                            ? `${intl.formatMessage({
                                id: "suggestion.fixed.fee.system",
                                defaultMessage: "정액제",
                              })}`
                            : list.srvcFeePctMin +
                              "% ~ " +
                              list.srvcFeePctMax +
                              "%"}
                        </li>
                        <li>
                          <FormattedMessage
                            id="suggestion.service.guarantee"
                            defaultMessage="서비스 보증조건"
                          />{" "}
                          : &nbsp;
                          {list.srvcWrtyPeriod}
                          <FormattedMessage
                            id="months"
                            defaultMessage="개월"
                          />{" "}
                          <FormattedMessage
                            id="suggestion.incase.retire"
                            defaultMessage="이내 퇴직시"
                          />
                          {list.srvcWrtyMthd === "prorated"
                            ? `${intl.formatMessage({
                                id: "suggestion.caculated.refund",
                                defaultMessage: "일할계산 환불",
                              })}`
                            : list.srvcWrtyMthd === "all"
                            ? `${intl.formatMessage({
                                id: "suggestion.all.refund",
                                defaultMessage: "100% 전액 환불",
                              })}`
                            : list.srvcWrtyMthd === "etc"
                            ? `${intl.formatMessage({
                                id: "etc",
                                defaultMessage: "기타",
                              })}`
                            : ""}
                        </li>
                      </ul>
                    </div>
                    <div className="listRight">
                      {list.rcmndEndYn === "Y" ? (
                        <button type="button" className="recommendDone">
                          <FormattedMessage
                            id="candidate.recommend.finished"
                            defaultMessage="후보자 추천마감"
                          />
                        </button>
                      ) : (
                        <Link
                          to="/recruiters/candidate-recommend"
                          className="recommendCandidate"
                          state={{ hireUid: list.hireUid, mbrUid: list.mbrUid }}
                        >
                          <FormattedMessage
                            id="candidate.recommend"
                            defaultMessage="후보자 추천하기"
                          />{" "}
                          &gt;
                        </Link>
                      )}

                      <div className="threeSections">
                        <div>
                          <span>
                            <FormattedMessage
                              id="recruiter.suggested"
                              defaultMessage="제안한 리크루터"
                            />
                          </span>
                          <em>
                            {list.cmmntCnt}
                            <FormattedMessage
                              id="people.count"
                              defaultMessage="명"
                            />
                          </em>
                        </div>
                        <div>
                          <span>
                            <FormattedMessage
                              id="recruiter.selected"
                              defaultMessage="선택한 리크루터"
                            />
                          </span>
                          <em>
                            <strong>{list.cmmntChooseCnt}</strong>/
                            {list.recSize}
                            <FormattedMessage
                              id="people.count"
                              defaultMessage="명"
                            />
                          </em>
                        </div>
                        <div>
                          <span>
                            <FormattedMessage
                              id="recruiter.recommended"
                              defaultMessage="추천된 후보자"
                            />
                          </span>
                          <Link
                            to="/recruiters/candidate"
                            state={{
                              hireUid: list.hireUid,
                              mbrUid: list.mbrUid,
                            }}
                          >
                            {list.recmmndCnt}
                            <FormattedMessage
                              id="people.count"
                              defaultMessage="명"
                            />
                            ({list.chkingRecmmndCnt}
                            <FormattedMessage
                              id="people.count"
                              defaultMessage="명"
                            />
                            /{list.nopassRecmmndCnt}
                            <FormattedMessage
                              id="people.count"
                              defaultMessage="명"
                            />
                            )
                          </Link>
                        </div>
                      </div>
                      <div className="qnaStatus">
                        <Link
                          to="/recruiters/qna"
                          state={{ hireUid: list.hireUid, mbrUid: list.mbrUid }}
                        >
                          Q&amp;A{" "}
                          <span>
                            {list.qaCnt}
                            <FormattedMessage id="count" defaultMessage="개" />
                          </span>
                        </Link>
                      </div>
                    </div>
                  </li>
                ) : (
                  ""
                )
              ) : (
                ""
              )
            )}
          </ul>
        </div>

        <div className="doneList">
          <h2 className="sectionTitle">
            <span>
              <FormattedMessage
                id="posting.done"
                defaultMessage="종료된 채용공고"
              />
            </span>
            <Link to="/recruiters/done">
              <FormattedMessage id="show.all" defaultMessage="전체보기" /> &gt;
            </Link>
          </h2>
          <div
            className="noRecruitList"
            style={{ display: listCntState.endCnt == 0 ? "" : "none" }}
          >
            <FormattedMessage
              id="description.no.done.posting"
              defaultMessage="종료된 채용공고가 없습니다."
            />
          </div>
          <ul>
            {listState.map((list, idx) =>
              list.hireStat == "end" && list.myCmmntChooseCnt > 0 ? (
                (endChkCnt = endChkCnt + 1) < 4 ? (
                  <li>
                    <div className="listLeft">
                      <h3>
                        <Link
                          to="/recruiters/done-detail"
                          state={{ hireUid: list.hireUid, mbrUid: list.mbrUid }}
                        >
                          [{list.pubYn == "Y" ? list.bizNm : "*****"}]{" "}
                          {list.title}
                        </Link>
                      </h3>
                      <p>
                        <FormattedMessage
                          id="registration.date"
                          defaultMessage="등록일"
                        />{" "}
                        : &nbsp;{moment(list.rgstDttm).format("YYYY-MM-DD")}
                        &nbsp; /{" "}
                        <FormattedMessage
                          id="recruitment.deadline"
                          defaultMessage="인재추천 마감일"
                        />{" "}
                        :{" "}
                        {list.rcmndUntilDon === "Y"
                          ? `${intl.formatMessage({
                              id: "until.hired",
                              defaultMessage: "채용시까지",
                            })}`
                          : list.rcmndEndDttm}
                        &nbsp; /{" "}
                        <strong>
                          <FormattedMessage
                            id="recruitment.people.number"
                            defaultMessage="모집인원"
                          />{" "}
                          : {list.hireSize}
                          <FormattedMessage
                            id="people.count"
                            defaultMessage="명"
                          />
                        </strong>
                      </p>
                      <ul>
                        <li>
                          <FormattedMessage
                            id="suggestion.service.fee"
                            defaultMessage="서비스 수수료율"
                          />{" "}
                          : &nbsp;
                          {list.srvcFeeFixYn === "Y"
                            ? `${intl.formatMessage({
                                id: "suggestion.fixed.fee.system",
                                defaultMessage: "정액제",
                              })}`
                            : list.srvcFeePctMin +
                              "% ~ " +
                              list.srvcFeePctMax +
                              "%"}
                        </li>
                        <li>
                          <FormattedMessage
                            id="suggestion.service.guarantee"
                            defaultMessage="서비스 보증조건"
                          />{" "}
                          : &nbsp;
                          {list.srvcWrtyPeriod}
                          <FormattedMessage
                            id="months"
                            defaultMessage="개월"
                          />{" "}
                          <FormattedMessage
                            id="suggestion.incase.retire"
                            defaultMessage="이내 퇴직시"
                          />
                          {list.srvcWrtyMthd === "prorated"
                            ? `${intl.formatMessage({
                                id: "suggestion.caculated.refund",
                                defaultMessage: "일할계산 환불",
                              })}`
                            : list.srvcWrtyMthd === "all"
                            ? `${intl.formatMessage({
                                id: "suggestion.all.refund",
                                defaultMessage: "100% 전액 환불",
                              })}`
                            : list.srvcWrtyMthd === "etc"
                            ? `${intl.formatMessage({
                                id: "etc",
                                defaultMessage: "기타",
                              })}`
                            : ""}
                        </li>
                      </ul>
                    </div>
                    <div className="listRight">
                      <div className="threeSections">
                        <div>
                          <span>
                            <FormattedMessage
                              id="recruiter.suggested"
                              defaultMessage="제안한 리크루터"
                            />
                          </span>
                          <em>
                            {list.cmmntCnt}
                            <FormattedMessage
                              id="people.count"
                              defaultMessage="명"
                            />
                          </em>
                        </div>
                        <div>
                          <span>
                            <FormattedMessage
                              id="recruiter.selected"
                              defaultMessage="선택한 리크루터"
                            />
                          </span>
                          <em>
                            <strong>{list.cmmntChooseCnt}</strong>/
                            {list.recSize}
                            <FormattedMessage
                              id="people.count"
                              defaultMessage="명"
                            />
                          </em>
                        </div>
                        <div>
                          <span>
                            <FormattedMessage
                              id="candidate.recommended"
                              defaultMessage="추천된 후보자"
                            />
                          </span>
                          <Link
                            to="/recruiters/candidate"
                            state={{
                              hireUid: list.hireUid,
                              mbrUid: list.mbrUid,
                              backUrl: "/recruiters/done-detail",
                            }}
                          >
                            {list.recmmndCnt}
                            <FormattedMessage
                              id="people.count"
                              defaultMessage="명"
                            />
                            ({list.chkingRecmmndCnt}
                            <FormattedMessage
                              id="people.count"
                              defaultMessage="명"
                            />
                            /{list.nopassRecmmndCnt}
                            <FormattedMessage
                              id="people.count"
                              defaultMessage="명"
                            />
                            )
                          </Link>
                        </div>
                      </div>
                      <div className="qnaStatus">
                        <Link
                          to="/recruiters/qna"
                          state={{
                            hireUid: list.hireUid,
                            mbrUid: list.mbrUid,
                            backUrl: "/recruiters/done-detail",
                          }}
                        >
                          Q&amp;A{" "}
                          <span>
                            {list.qaCnt}
                            <FormattedMessage id="count" defaultMessage="개" />
                          </span>
                        </Link>
                      </div>
                    </div>
                  </li>
                ) : (
                  ""
                )
              ) : (
                ""
              )
            )}
          </ul>
        </div>
      </section>
    </main>
  );
};
