
import 'commons/scss/footer-all.scss';
import { FormattedMessage } from 'react-intl';

export const Footer = () => {
  return (
    <footer>
      <div className="footerWrap">
        <span>
          <FormattedMessage
            id="footer.nav1"
            defaultMessage="서비스 소개"
          />
        </span>
        <span>
          <FormattedMessage
            id="footer.nav2"
            defaultMessage="이용요금"
          />
        </span>
        <span>
          <FormattedMessage
            id="footer.nav3"
            defaultMessage="자주 묻는 질문"
          />
        </span>
        <span>
          <FormattedMessage
            id="footer.nav4"
            defaultMessage="블로그"
          />
        </span>
        <span>
          <FormattedMessage
            id="footer.nav5"
            defaultMessage="회사 소개"
          />
        </span>
      </div>
      <p>Copyright ⓒ WECRUIT. All Right Reserved.</p>
    </footer>
  );
}