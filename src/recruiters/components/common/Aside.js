
import contact from '../images/contact.jpg';
import '../scss/aside.scss';
import { StarRating } from 'commons/components/star-rating/StarRating';


import {Link, useLocation, useNavigate} from 'react-router-dom';
import { useContext, useEffect, useState } from 'react';
import { Context } from 'contexts';
import * as defVal  from 'commons/modules/defVal';
import * as commonUtil from 'commons/modules/commonUtil';
import { FormattedMessage, useIntl } from 'react-intl';

export const Aside =()=>{
  const intl = useIntl()

  let PROC_CHK = ""

  const {
    state : {
        server,
        mbr
    },
    dispatch,
  } = useContext(Context);

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid")
    ,ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi")
  };

  
  const location = useLocation();
  const navigate = useNavigate();

  const [state, setState] = useState({initSet:false});
  const [ssnState, setSsnState] = useState(defVal.setAxiosSsnState(null));
  let ssnMbrData = defVal.setAxiosSsnState(null);  

  useEffect(async () => {

    if( ssn.ssnMbrUid == "" || 
        ssn.ssnMbrDivi != "MBRDIVI-02" )
    {
      alert(intl.formatMessage({
        id: "wrong.access",
        defaultMessage: "정상적인 접속이 아닙니다."
      }));
      navigate("/");
    }
  

    console.log("Page First Load EFFECT");
    initSetHandle();          
  },[])

  async function initSetHandle() {

    let goUrl = server.path + "/mbr/mbrdetail"
    let data = {
      ssnMbrUid : ssn.ssnMbrUid,
      ssnMbrDivi : ssn.ssnMbrDivi,
      mbrUid : ssn.ssnMbrUid,
    }

    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");
    setSsnState(defVal.setAxiosSsnState(axiosRes.data));
  }

  return(
    <aside>
      <section className="asidePadding recruiterProfile">
        <p>{ssnState.mbrNm}</p>
        <em>{ssnState.bizNm}</em>

        <div className="stars" style={{display:'none'}}>
          {(ssnState.replyPoint >= 1)?(<span className="material-icons-outlined active">grade</span>):(<span className="material-icons-outlined ">grade</span>)}
          {(ssnState.replyPoint >= 2)?(<span className="material-icons-outlined active">grade</span>):(<span className="material-icons-outlined ">grade</span>)}
          {(ssnState.replyPoint >= 3)?(<span className="material-icons-outlined active">grade</span>):(<span className="material-icons-outlined ">grade</span>)}
          {(ssnState.replyPoint >= 4)?(<span className="material-icons-outlined active">grade</span>):(<span className="material-icons-outlined ">grade</span>)}
          {(ssnState.replyPoint >= 5)?(<span className="material-icons-outlined active">grade</span>):(<span className="material-icons-outlined ">grade</span>)}
          <span>&nbsp;&nbsp;{ssnState.replyPoint} / 5.0</span>
        </div>
        

      </section>
      <section className="asidePadding availableCase">
        <p>최대 진행가능 프로젝트 : {ssnState.maxHire}건</p>
        <p>현재 진행중인 프로젝트 : {ssnState.cmmntChooseCnt}건</p>
        <p>잔여 진행가능 프로젝트 : {ssnState.maxHire - ssnState.cmmntChooseCnt}건</p>
      </section>
      <section className="recruitFormList">
        <Link to='/recruiters/job-posting'>
          <p>모집중인 채용공고를 확인하세요</p>
          채용공고 전체보기
          <span className="material-icons">
            keyboard_double_arrow_right
          </span>
        </Link>
      </section>
      <nav>
        <Link to='/recruiters/notice'>
          ㆍ<FormattedMessage
            id="notice"
            defaultMessage="공지사항"
          />
        </Link>

        <div className="subLinks">
          <Link to='/recruiters/partner' className="lightGreen">
            ㆍ<FormattedMessage
              id="posting.partner"
              defaultMessage="거래 고객사의 채용공고"
            />
          </Link>
          <Link to='/recruiters/suggest' className="lightGreen">
            ㆍ<FormattedMessage
              id="posting.suggested"
              defaultMessage="제안한 채용공고"
            />
          </Link>
          <Link to='/recruiters/ing' className="lightGreen">
            ㆍ<FormattedMessage
                id="posting.ing"
                defaultMessage="진행중 채용공고"
              />
          </Link>
          <Link to='/recruiters/done' className="lightGreen">
            ㆍ<FormattedMessage
                id="posting.done"
                defaultMessage="종료된 채용공고"
              />
          </Link>
          <Link to='/recruiters/payment'>
            ㆍ<FormattedMessage
                id="payment"
                defaultMessage="비용 관리"
              />
          </Link>
          <Link to='/recruiters/mypage'>
            ㆍ<FormattedMessage
                id="mypage"
                defaultMessage="마이페이지"
              />
          </Link>
        </div>
      </nav>
      <article>
        <img src={contact} alt="자동화 레퍼런스 체크 솔루션 체커 서비스 바로가기" />
      </article>
    </aside>
  );
}
