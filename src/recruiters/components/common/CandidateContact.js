import wecruitLogo from "../images/logo.png";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { useContext, useEffect, useState } from "react";
import { toast, ToastContainer } from "react-toastify";
import { Context } from "contexts";
import axios from "axios";
import * as commonUtil from "commons/modules/commonUtil";
import * as defVal from "commons/modules/defVal";
import { FormattedMessage, useIntl } from "react-intl";

const CandidateContact = () => {
  
  const intl = useIntl();

  let PROC_CHK = "";

  const {
    state: { server, mbr },
    dispatch,
  } = useContext(Context);

    
  const location = useLocation();
  const navigate = useNavigate();
  const moment = require("moment");

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid")
    ,ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi")
  };

  let locationState = {
    hireUid: location.state.hireUid
    ,rcmndUid: location.state.rcmndUid
  };

  const celpFCd = defVal.CelpFCd();

  const [detailState, setDetailState] = useState(defVal.setAxiosJobState(null));
  const [recmmdState, setRecmmdState] = useState(defVal.setAxiosCnddtRcmmdState(null));
  const [ssnState, setSsnState] = useState(defVal.setAxiosSsnState(null));


  useEffect(async () => {
    console.log("Page First Load EFFECT");
    initSetHandle();          
  },[])

  async function initSetHandle() {

    let goUrl = server.path + "/mbr/mbrdetail"
    let data = {
      ssnMbrUid : ssn.ssnMbrUid,
      ssnMbrDivi : ssn.ssnMbrDivi,
      mbrUid : ssn.ssnMbrUid,
    }

    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");
    setSsnState(defVal.setAxiosSsnState(axiosRes.data));


    goUrl = server.path + "/hire/hiredetail";
    data = {
      ssnMbrUid: ssn.ssnMbrUid,
      ssnMbrDivi: ssn.ssnMbrDivi,
      hireUid: locationState.hireUid,
      sRecMbrUid: ssn.ssnMbrUid,
    };

    let axiosDtlRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");
    setDetailState(axiosDtlRes.data);

    
    goUrl = server.path + "/hire/cnddtrecmmnd";
    data = {
      ssnMbrUid: ssn.ssnMbrUid,
      ssnMbrDivi: ssn.ssnMbrDivi,
      hireUid: locationState.hireUid,
      rcmndUid: locationState.rcmndUid
    };

    

    let axiosCndtRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");
    let tmpDate = defVal.setAxiosCnddtRcmmdState(axiosCndtRes.data.resInfo);

    var tmpCelp = tmpDate.cnddtCelpNum.split("-");
    try {
      tmpDate.cnddtCelpF = tmpCelp[0];
      tmpDate.cnddtCelpM = tmpCelp[1];
      tmpDate.cnddtCelpL = tmpCelp[2];
    } catch (e) {}

    setRecmmdState(tmpDate);
    
  }

  ////////////////////////////////////////////////////////////////////////////////////////////////////////

  const jsonCompResult = (resJson) => {
    console.log("PROC_CHK : " + PROC_CHK);

    if (resJson.resCd !== "0000") {
      toast.error(
        intl.formatMessage({
          id: "error.during.save",
          defaultMessage: "저장 중 오류가 발생했습니다.",
        })
      );
      return;
    } else {
      if (PROC_CHK == "INS") {
        toast.info(
          "연락처가 공개 되었습니다."
        );

      }      
    }
  };

  const goCRUD = async () => {
    
    if (
      commonUtil.CheckIsEmptyFromVal(
        recmmdState.cnddtCelpM,
        intl.formatMessage({
          id: "error.need.input.phone",
          defaultMessage: "휴대폰번호를 입력해주세요.",
        })
      ) == false
    )return;
    
    if (
      commonUtil.CheckIsEmptyFromVal(
        recmmdState.cnddtCelpL,
        intl.formatMessage({
          id: "error.need.input.phone",
          defaultMessage: "휴대폰번호를 입력해주세요.",
        })
      ) == false
    )return;
   
    PROC_CHK = "FILEUPLOAD";

    cntntsCRUD();
   
  };

  const cntntsCRUD = async () => {
    PROC_CHK = "INS";

    recmmdState.mbrUid = ssn.ssnMbrUid;
    recmmdState.hireUid = locationState.hireUid;
    recmmdState.rcmndUid = locationState.rcmndUid;

    let goUrl = server.path + "/hire/cnddtrecmmndpubupdate";
    let axiosRes = await commonUtil.axiosCrudOnSubmit(recmmdState, goUrl, "POST");

    jsonCompResult(axiosRes.data);
  };
  
  return (
    <div class="candidateContact">
      <div className="mailWrap">
        <header>
          <img src={wecruitLogo} alt="위크루트 로고" />
          <span>hunters.wecruitpro.com</span>
        </header>

        <div className="content">
          <h1>
            <p>대한민국 No.1 온라인 헤드헌팅 플랫폼</p>
            <p>우수한 인재를 빠르고 쉽게 채용할 수 있는 곳, 헌터스</p>
          </h1>

          <section class="intro">
            <p>
              안녕하세요? <strong>{ssnState.mbrNm}</strong>님,
            </p>
            <p>
            {detailState.mbrNm} 채용 담당자가 {detailState.title}에 추천하신 {recmmdState.cnddtNm}님의 연락처 공개를
              요청하였습니다.
            </p>
          </section>

          <section>
            <p>후보자의 연락처를 입력해주세요.</p>

            <article>
              <ul>
                <li>
                  <select name="phoneNumber" id="phoneNumber">
                    {celpFCd.map((type, idx) => (
                          <option value={type.cateCd}>{type.cdName}</option>
                        ))}
                  </select>
                </li>
                <li>-</li>
                <li>
                    <input
                        type="text"
                        value={recmmdState.cnddtCelpM}
                        maxLength="4"
                        onChange={(e) => {
                          setRecmmdState({
                            ...recmmdState,
                            cnddtCelpM: e.target.value,
                          });
                        }}
                      />
                </li>
                <li>-</li>
                <li>
                <input
                        type="text"
                        value={recmmdState.cnddtCelpL}
                        maxLength="4"
                        onChange={(e) => {
                          setRecmmdState({
                            ...recmmdState,
                            cnddtCelpL: e.target.value,
                          });
                        }}
                      />
                </li>
              </ul>
            </article>
          </section>

          <section>
            <p>후보자의 연락처 공개를 동의하시면 아래 버튼을 클릭하세요.</p>
          </section>
        </div>

        <div class="agreeButton flexAlignCenter">
          <button
            onClick={() => {
              goCRUD();
            }}>네! 동의합니다.</button>
        </div>
      </div>
    </div>
  );
};

export default CandidateContact;
