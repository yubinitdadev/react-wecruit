import membership from '../images/sign_w.png';

import { DownloadBtns } from 'commons/components/download-btn/DownloadBtns';
import { Pagination } from 'commons/components/pagination/Pagination';
import { axiosCrudOnSubmit } from 'commons/modules/commonUtil';
import { Context } from 'contexts';
import { UPD_MBRUID } from 'contexts/actionTypes';
import { useContext, useEffect, useRef, useState } from 'react';
import {Link} from 'react-router-dom';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import * as commonUtil from 'commons/modules/commonUtil';
import * as defVal  from 'commons/modules/defVal';
import { FormattedMessage } from 'react-intl';

export const JobPosting = () => {
  const {
    state : {
      server,
      mbr
    },
    dispatch,
  } = useContext(Context);

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid")
    ,ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi")
  };
  
  const moment = require('moment');
  const componentRef = useRef();

  const [noDataState, setNoDataState] = useState("none");
  const [mbrCnt, setMbrCnt] = useState({});
  const [listState, setListState] = useState([]);
  const [sConts, setSConts] = useState("");
  const [fltrState, setFltrState] = useState({});

  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(10);

  const indexOfLast = currentPage * postsPerPage;
  const indexOfFirst = indexOfLast - postsPerPage;

  function currentPosts(tmp) {
    let currentPosts = 0;
    currentPosts = tmp.slice(indexOfFirst, indexOfLast);
    return currentPosts;
  }

  async function goSrch() {
    
    let data = {
      ssnMbrUid : ssn.ssnMbrUid,
      mbrUid : "",
      mbrDivi : "MBRDIVI-03",
      hireStat : "recallnew",
      sRecMbrUid : ssn.ssnMbrUid,
      sConts : (document.getElementById("sConts"))?(document.getElementById("sConts").value):(''),
      sPageSize : (document.getElementById("sPageSize"))?(document.getElementById("sPageSize").value):('15'),
      sFltr : fltrState,
      sFltrCate : Object.keys(fltrState)[0],
      sFltrCont : Object.values(fltrState)[0],
    }

    initSetHandle(data);
  }

  async function changeFltr(key, fltr){
    
    let tmpFltr = (fltr === "ASC") ? "DESC" :
                  (fltr === "DESC") ? "ASC" :
                  "ASC";
    
    setFltrState({[key]:tmpFltr});

    goSrch();
  }

  const initSetHandle = async (data) => {
    let goUrl = server.path + "/hire/hirelist"
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    let axiosResData = axiosRes.data.hireArr;
    let tmpList = [];
    axiosResData.forEach((resData, idx)=>{
      resData["rcmndEndDttm"] = moment(resData.rcmndEndDttm).format("YYYY-MM-DD HH:mm");
      resData["workPosStr"] = defVal.SplitToArray(resData.workPosStr, "<;:>");

      tmpList.push(resData);
    })

    var tmpMbrCnt = await getMbrCnt();
    if( tmpList.length == 0)setNoDataState("");
    setListState(tmpList);

    
  }

  const getMbrCnt = async () => {
    let goUrl = server.path + "/mbr/mbrcnt";
    let data = {
      hireUid : ""
    }; 
    
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    let tmpDate = axiosRes.data.mbrcnt
    setMbrCnt(tmpDate);    
    return tmpDate;
  }

  useEffect( () => {
    //goSrch();
  }, [fltrState]);

  useEffect(async() => {
    window.scrollTo(0,0);

    let data = {
      ssnMbrUid : ssn.ssnMbrUid,
      mbrUid : "",
      mbrDivi : "MBRDIVI-03",
      hireStat : "recallnew",
      sRecMbrUid : ssn.ssnMbrUid,
      sConts : "",
      sPageSize : '15',
      sFltr : fltrState,
      sFltrCate : Object.keys(fltrState)[0],
      sFltrCont : Object.values(fltrState)[0],
    }

    initSetHandle(data);

    document.getElementById("sPageSize").value = "15";
  }, [])

  //////////////////////////////////////////////////////////////////
  async function onChangeSrch(){
    goSrch();
  }
  
  const onKeyUp =(e) => {
    if(e.key == "Enter")goSrch();
    return false;
   }

  return (
    <main className='recruitersMain'>
        <section>
          <div>
            <h2 className="sectionTitle"><span>신규 채용공고 전체보기</span></h2>
            <div className="flexSpaceBetween">
              <select name="sPageSize" id="sPageSize"
                    onChange={() => {
                        onChangeSrch();                   
                    }}
                >
                <option value="5">5</option>
                <option value="15">15</option>
                <option value="30">30</option>
                <option value="10000">All</option>
              </select>
              <div>
                <label htmlFor="search">
                  <FormattedMessage
                    id="search"
                    defaultMessage="검색"
                  />&nbsp;&nbsp;:&nbsp;&nbsp;</label>
                <input type="text" id="sConts"  
                onKeyUp={(e) => {
                  onKeyUp(e);
                }}/>
              </div>
            </div>
            <table>
              <thead>
                <tr>
                  <th>
                    <span>
                      <FormattedMessage
                        id="number"
                        defaultMessage="번호"
                      />
                    </span>                    
                  </th>
                  <th className={ fltrState.rgstDttm === "DESC" ? "descending" :
                                    fltrState.rgstDttm === "ASC" ? "ascending" :
                                    undefined } 
                      onClick={() => {
                        changeFltr("rgstDttm", fltrState.rgstDttm);                   
                      }}>
                    <span>
                      <FormattedMessage
                        id="registration.date"
                        defaultMessage="등록일"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </th>
                  <th className={ fltrState.title === "DESC" ? "descending" :
                                    fltrState.title === "ASC" ? "ascending" :
                                    undefined } 
                      onClick={() => {
                        changeFltr("title", fltrState.title);                   
                      }}>
                    <span>
                      <FormattedMessage
                        id="recruitment.title"
                        defaultMessage="채용공고 제목"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </th>
                  <th>
                    <span>
                      <FormattedMessage
                        id="position.business.type"
                        defaultMessage="직종/업종"
                      />
                    </span>
                  </th>
                  <th className={ fltrState.hireSize === "DESC" ? "descending" :
                                    fltrState.hireSize === "ASC" ? "ascending" :
                                    undefined } 
                      onClick={() => {
                        changeFltr("hireSize", fltrState.hireSize);                   
                      }}>
                    <span>
                      <FormattedMessage
                        id="recruitment.people.number"
                        defaultMessage="모집인원"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </th>
                  <th>
                    <span>
                      <FormattedMessage
                        id="attend.condition"
                        defaultMessage="참여조건"
                      />
                    </span>                    
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr className="noData" style={{display: noDataState}}>
                  <td colSpan={6}>
                    <FormattedMessage
                        id="description.no.searchfirm"
                        defaultMessage="등록된 기존 거래 써치펌이 없습니다."
                      />
                  </td>
                </tr>                
                {
                  listState.map((list, idx)=>
                    (
                      <>
                      <tr>
                        <td>{idx+1}</td>
                        <td>{moment(list.rgstDttm).format("YYYY-MM-DD")}</td>
                        <td className="width250">
                            <Link to={(list.myCmmntCnt == 0 )?('/recruiters/partner-detail'):('/recruiters/suggest-detail')}
                            state={{ hireUid: list.hireUid, mbrUid: list.mbrUid }}>
                              [{(list.pubYn == 'Y')?(list.bizNm):('*****')}] {list.title}</Link>
                        </td>
                        <td>
                          {
                            list.workPosStr.map((type, idx)=>
                            (<p>
                              {type} 
                            </p>)
                            )
                          }
                        </td>
                        <td>{list.hireSize}</td>
                        <td><img src={membership} alt="w가 쓰여진 깃발" /></td>
                      </tr>
                      </>
                    )
                  )
                }
              </tbody>
            </table>
          </div>
        </section>
      </main>
  );
}