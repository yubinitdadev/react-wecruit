
import {Link, useLocation, useNavigate} from 'react-router-dom';
import { useCallback, useContext, useEffect, useState } from 'react';
import { Context } from 'contexts';
import { toast, ToastContainer } from "react-toastify";
import axios from 'axios';
import fileDownload from 'js-file-download';
import Modal from "modal-all/ModalAll";
import { LeaveWecruit } from "modal-all/modals/LeaveWecruit";
import { ChangePassword } from "modal-all/modals/ChangePassword";

import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { axiosCrudOnSubmit } from 'commons/modules/commonUtil';
import * as commonUtil from 'commons/modules/commonUtil';
import { snglFileUpload } from 'commons/modules/multipartUtil';
import * as defVal  from 'commons/modules/defVal';
import { AddressInput } from 'commons/components/AddressInput';
import { TagTemplate } from 'commons/components/tags/TagTemplate';
import EditorComponent from 'admin/components/editor/EditorComponent';
import { ImagePreview } from 'commons/components/inputs/image-preview/ImagePreview';
import { FormattedMessage, useIntl } from 'react-intl';


export const MypageEdit = () => {
  const intl = useIntl();

  let PROC_CHK = "";

  const {
    state : {
        server,
        mbr
    },
    dispatch,
  } = useContext(Context);

  
  const location = useLocation();
  const navigate = useNavigate();
  const moment = require('moment');
  let previewWindow = "";
  let previewImg = document.createElement("img");  

  const [files, setFiles] = useState("");
  const [axiosFilesRes, setAxiosFilesRes] = useState("");
  
  const [mbrFilesRes, setMbrFilesRes] = useState([]);
  const [bizFilesRes, setBizFilesRes] = useState([]);
  const [extraFilesRes, setExtraFilesRes] = useState([]);

  const salesSizeCd = defVal.SalesSizeCd();
  const empSizeCd = defVal.EmpSizeCd();
  const bankCd = defVal.BankCd();
  
  const [bizCateCd, setBizCateCd] = useState([]);
  const [detailState, setDetailState] = useState(defVal.setAxiosMbrState(null));
  const [tagCnt, setTagCnt] = useState(0);

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid")
    ,ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi")
  };

  let locationState = {
    mbrUid : location.state.mbrUid,
    mbrDivi : "MBRDIVI-02"
  }

  useEffect(async () => {
    console.log("Page First Load EFFECT");
    initSetHandle();          
  },[])

  const reRender = () => {
    setDetailState({
      ...detailState,
      initSetCmplt: true
    })      
  };

  useEffect(()=>{
    console.log("File Upload Complete EFFECT");    
    if(detailState.fileUploadCmplt) {
      cntntsCRUD(); 
    }
    
  }, [detailState.fileUploadCmplt]) 

  async function initSetHandle() {
    detailState.filePosNm[1] = "thmnlNm";
    detailState.filePosNm[2] = "extraThmnlNm";
    detailState.ssnMbrUid = ssn.ssnMbrUid;
    
    let goUrl = server.path + "/mbr/mbrdetail"
    let data = {
      ssnMbrUid : ssn.ssnMbrUid,
      ssnMbrDivi : ssn.ssnMbrDivi,
      mbrUid : locationState.mbrUid,
      mbrDivi : locationState.mbrDivi
    }
    
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");    
    var tmpDetail = defVal.setAxiosMbrState(axiosRes.data);

    //키워드 처리
    let tagstr = axiosRes.data.tags;
    console.log("tagstr == " + tagstr);
    var tags = defVal.SplitToArray(tagstr, "<;:>");
    axiosRes.data.tags = tags;
    tmpDetail.tags = tags;

    setDetailState(tmpDetail);

    getBizCateCdList();
    getMbrFileList();
    getBizFileList();   
    getExtraFileList();   
  }

  const getDetail = async () => {
    let goUrl = server.path + "/mbr/mbrdetail"
    let data = {
      ssnMbrUid : ssn.ssnMbrUid,
      ssnMbrDivi : ssn.ssnMbrDivi,
      mbrUid : locationState.mbrUid,
      mbrDivi : locationState.mbrDivi
    }
    
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");  
    
    setDetailState(defVal.setAxiosMbrState(axiosRes.data));
  }

  useEffect(() => {
    if( detailState ){   
      //console.log("useEffect detailState1  === " + detailState.mbrNm) ;
      //console.log("useEffect detailState2  === " + detailState.bizNm) ;
      detailState.filePosNm[1] = "thmnl";
      detailState.filePosNm[2] = "extraThmnl";
      detailState.mbrFilePosNm[1] = "thmnl";
      detailState.extraFilePosNm[1] = "extraThmnl";
      detailState.ssnMbrUid = ssn.ssnMbrUid;
    }
  }, [detailState])

  const getBizCateCdList = async () => {
    let goUrl = server.path + "/hireBizCateCd";
    let data = {};
    
    let axiosCateRes = await axiosCrudOnSubmit(data, goUrl, "POST");  
    setBizCateCd(axiosCateRes.data.hireCateArr);
  }

  useEffect(() => {
    if(bizCateCd && bizCateCd.length > 0){        
    }
  }, [bizFilesRes])


  const getMbrFileList = async () => {
    let goUrl = server.path + "/mbr/mbrfiles";
    let data = {mbrUid: locationState.mbrUid};
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    setMbrFilesRes( axiosRes.data );
  }

  useEffect(() => {
    if(mbrFilesRes && mbrFilesRes.length > 0){
      console.log("useEffect mbrFilesRes length == " + mbrFilesRes.length);

      for(let i = 0; i < mbrFilesRes.length; i++)
      {
          console.log("files getMbrFileList "+i+" === " + mbrFilesRes[i].orignlFileNm); 
          detailState.thmnlNm = mbrFilesRes[i].orignlFileNm;
          detailState.mbrFileJsonStr[i+1] = JSON.stringify(mbrFilesRes[i])+"";          
      }
    }
  }, [mbrFilesRes])

  const getBizFileList = async () => {
    let goUrl = server.path + "/mbr/bizfiles";
    let data = {mbrUid: locationState.mbrUid};
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    setBizFilesRes( axiosRes.data );
  }

  useEffect(() => {
    if(bizFilesRes && bizFilesRes.length > 0){
      for(let i = 0; i < bizFilesRes.length; i++)
        {
          if( i == 0 ){
            detailState.bizCertThmnlNm = bizFilesRes[i].orignlFileNm;            
          }
          else 
          {
            detailState.bizLogoThmnlNm = bizFilesRes[i].orignlFileNm;
          }

          detailState.bizFileJsonStr[i+1] = JSON.stringify(bizFilesRes[i])+"";
        }
    }
  }, [bizFilesRes])

  const getExtraFileList = async () => {
    let goUrl = server.path + "/mbr/extrafiles/";
    let data = {mbrUid: locationState.mbrUid};
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    setExtraFilesRes( axiosRes.data );
  }

  useEffect(() => {
    if(extraFilesRes && extraFilesRes.length > 0){
      console.log("useEffect extraFilesRes length == " + mbrFilesRes.length);

      for(let i = 0; i < extraFilesRes.length; i++)
      {
         // console.log("files getMbrFileList "+i+" === " + mbrFilesRes[i].orignlFileNm); 
          detailState.extraThmnlNm = extraFilesRes[i].orignlFileNm;
          detailState.extraFileJsonStr[i+1] = JSON.stringify(extraFilesRes[i])+"";          
      }
    }
  }, [extraFilesRes])

  /////////////////////////////////////////////////////////////////////////
  const handleDownload = (idx, filePath, fileNm) => {
    if(filePath != "" )
    {
       let goUrl = server.host + filePath; 
       axios.get(goUrl, {
        responseType: 'blob',
      })
      .then((res) => {
        fileDownload(res.data, fileNm);
      })
    } else {
      fileDownload(detailState.uploadFiles[idx], fileNm);
    }
   }
  
   const handlePreview = (idx, filePath) => {
  
      previewWindow = window.open("", "", "width=600,height=400,left=200,top=200");
  
     if(filePath != "")
     {
       let goUrl = server.host + filePath; 
       previewImg.src = goUrl;
     } 
     else
     {
       let file = detailState.uploadFiles[idx];
       let reader = new FileReader();
     
       reader.onload = ( function(file) {
         return function(e) {
           previewImg.src = e.target.result;
         };
       })(file);
       
       reader.readAsDataURL(file);
     } 
       
     previewWindow.document.body.appendChild(previewImg);
   }

   ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  const addTag =(e) => {
    if(e.key == "Enter")
    {
      if(detailState.tags.length == 29){
        alert(intl.formatMessage({
          id: "input.max30",
          defaultMessage: "30개까지 입력 가능합니다!"
        }));
        detailState.tagCntnt = "";
        return false;
      }
            
      var duplYn = "N";
      
      for(var k=0; k<detailState.tags.length; k++)
      {
        if( detailState.tags[k] == e.target.value)
        {
          duplYn = "Y";
          break;
        }
      }
      
      if( duplYn == "N")detailState.tags.push(e.target.value);      
      detailState.tagCntnt = "";
      reRender();
    }

    return false;
  }

  const rmvTag = (idx) => {
    var tmpList = commonUtil.rmvArrIdx(detailState.tags, idx);
    setDetailState({
      ...detailState,
      tags : tmpList
    });    
  }

  /////////////////////////////////////////////////////////////////////////
  const [modalVisible1, setModalVisible1] = useState(false)
  const [modalVisible2, setModalVisible2] = useState(false)

  const openModal1 = () => {
    setModalVisible1(true)
  }

  const closeModal1 = () => {
    setModalVisible1(false)
  }

  const openModal2 = () => {
    setModalVisible2(true)
  }

  const closeModal2 = () => {
    setModalVisible2(false)
  }


  /////////////////////////////////////////////////////////////////////////
  const jsonCompResult = ( resJson ) => {
        
    if( resJson.resCd !== "0000" )
    {
      toast.error(intl.formatMessage({
        id: "error.during.save",
        defaultMessage: "저장 중 오류가 발생했습니다."
      }));
      return;
    }
    else
    {
      if( PROC_CHK == "UPD" )
      {
        PROC_CHK = "";
        navigate("/recruiters/mypage");
        //toatYn = "N";
        
        //initSetHandle();
        console.log("UPDATE");
        //return;
      }
      else if( PROC_CHK === "UPDPSWD" )
      {
        PROC_CHK = "";
        toast.info(intl.formatMessage({
            id: "info.password.changed",
            defaultMessage: "비밀번호가 변경되었습니다"
          }));      
        //return;
      }
      else if( PROC_CHK === "SETWTHDR" )
      {
        PROC_CHK = "";
        window.localStorage.clear();
        navigate("/");
      }
      

      else if( PROC_CHK === "FILEUPLOAD" )
      {
        PROC_CHK = "";
        let jsonStr = JSON.stringify(resJson.resFileList);
        let fileList = JSON.parse(jsonStr);

        let tmpMbrFilePosNm = detailState.mbrFilePosNm;
        let tmpMbrList = detailState.mbrFileJsonStr;
        let tmpExtraFilePosNm = detailState.extraFilePosNm;
        let tmpExtraList = detailState.extraFileJsonStr;

        let pos = 0;
        for(var i=0; i<fileList.length; i++ )
        {
          for(let n=0; n<tmpMbrFilePosNm.length; n++)
          {
            if( tmpMbrFilePosNm[n] == fileList[i].filePosNm)
            {
              tmpMbrList[n] = JSON.stringify( fileList[i] );
            }
          }

          for(let n=0; n<tmpExtraFilePosNm.length; n++)
          {
            if( tmpExtraFilePosNm[n] == fileList[i].filePosNm)
            {
              tmpExtraList[n] = JSON.stringify( fileList[i] );
            }
          }
        }

        detailState.mbrFileJsonStr = tmpMbrList;
        detailState.extraFileJsonStr = tmpExtraList;

        PROC_CHK = "UPD";
        cntntsCRUD();

        
      }
    }

    PROC_CHK = "";
  }
  
  async function goCRUD() {
    let fileStorePath = "Globals.fileStoreBizPath"; //로컬저장경로
    let fileLinkPath = "Globals.fileLinkBizPath"; //link경로
    let filePrefix = "biz"; //파일코드

    PROC_CHK = "FILEUPLOAD";

    let uploadRes = await snglFileUpload( server.path, fileStorePath, fileLinkPath, filePrefix, detailState.uploadFiles, detailState.filePosNm);
    //jsonCompResult(uploadRes.data);
    if(uploadRes === 0) cntntsCRUD();
    else jsonCompResult(uploadRes.data);
  }

  async function cntntsCRUD() {
    PROC_CHK = 'UPD';
  
    console.log("CNTNTS CRUD");
  
    let goUrl = server.path + "/mbr/mbrupdate";
    //console.log("cntntsCRUD == " +  detailState.snglImgFileJsonStr);
    let axiosRes = await axiosCrudOnSubmit(detailState, goUrl, "POST");
  
    jsonCompResult(axiosRes.data);
  }

  async function goPswdCRUD() {
    
    closeModal1();
    
    PROC_CHK = "UPDPSWD";

    let goUrl = server.path + "/mbr/pswd";
    let data = {
      pswd: detailState.pswd,
      mbrUid: detailState.mbrUid
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    jsonCompResult(axiosRes.data);
  }

  const setWthdrYn = useCallback(async () => {
    closeModal2();
    
    PROC_CHK = 'SETWTHDR';

    let goUrl = server.path + "/mbr/wthdrYn";
    
    let data = {
      wthdrYn: "Y",
      mbrUid : ssn.ssnMbrUid
    }

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    jsonCompResult(axiosRes.data);
  })


  return (
    <main className='recruitersMain'>
        <ToastContainer />
        <section>
          <div>
            <h2 className="sectionTitle">
              <span>
                <FormattedMessage
                  id="mypage"
                  defaultMessage="마이페이지"
                />
                <em>
                  <FormattedMessage
                    id="mypage.self.intro.edit"
                    defaultMessage="자기소개 수정하기"
                  />
                </em>
              </span>
            </h2>
            <form className="recruitForm recruitersForm">
              <div className="formWrap">
                <h2>
                  <FormattedMessage
                    id="basic.info.add"
                    defaultMessage="기본 정보 추가"
                  />
                </h2>
                <div className="formWrapInputs">
                  <ul className="twoCol">
                    <li>
                      <span className="labelSpan">
                        <FormattedMessage
                          id="gender"
                          defaultMessage="성별"
                        />
                      </span>
                      <div>
                      <input type="radio" id="male" name="gender" value="male" 
                      checked={(detailState.gndr === "male") ? true:false} 
                      onChange={ (e) => {
                        setDetailState({
                          ...detailState,
                          gndr: e.target.value
                        })
                      }}/>
                    <label htmlFor="male">
                      <FormattedMessage
                        id="male"
                        defaultMessage="남"
                      />
                    </label>
                    <input type="radio" id="female" name="gender" value="female" 
                      checked={(detailState.gndr === "female") ? true:false}
                      onChange={ (e) => {
                        setDetailState({
                          ...detailState,
                          gndr: e.target.value
                        })
                      }}/>
                    <label htmlFor="female">
                      <FormattedMessage
                        id="female"
                        defaultMessage="여"
                      />
                    </label>
                      </div>
                    </li>
                    <li>
                      <label htmlFor="birth">
                        <FormattedMessage
                          id="birth"
                          defaultMessage="생년월일"
                        />
                      </label>
                      <input type="text" id="birth" 
                        placeholder="(예:19800101)" 
                        value={detailState.brthd}
                        onChange={(e) => {
                          setDetailState({
                            ...detailState,
                            brthd : e.target.value
                          })
                        }}/>
                    </li>
                  </ul>
                  <ul>
                    <li>
                      <span className="labelSpan">
                        <FormattedMessage
                          id="profile.image"
                          defaultMessage="프로필 사진"
                        />
                      </span>
                      <div className="fileBox">
                        <label htmlFor="fileInput1" className="basicButton">
                          <FormattedMessage
                            id="image.attachment"
                            defaultMessage="이미지 첨부"
                          /> &gt;
                        </label>
                        <span className="fileName">{detailState.thmnlNm}</span>
                        <span>( <FormattedMessage
                            id="recommend.size"
                            defaultMessage="권장사이즈"
                          />: 200*200 <FormattedMessage
                          id="pixel"
                          defaultMessage="픽셀"
                        />)</span>
                        <input type="file" id="fileInput1" className="fileInput" accept='.jpg, .png, .gif'
                          onChange={(e) => {
                            let tmpList = detailState.uploadFiles;
                            tmpList[1] = e.target.files[0];
  
                            let tmpJsonStrList = detailState.snglImgFileJsonStr;
                            tmpJsonStrList[1] = "";
                            
                            setDetailState({
                              ...detailState,
                              uploadFiles: tmpList,
                              snglImgFileJsonStr: tmpJsonStrList,
                              thmnl: "",
                              thmnlNm: e.target.files[0].name
                            })
                          }}/>
                      </div>
                      <div>
                        <button type="button" className="basicButton" onClick={(e) => {handlePreview(1, detailState.thmnl)}}>
                          <FormattedMessage
                            id="see.picture"
                            defaultMessage="사진보기"
                          />
                        </button>
                      </div>
                    </li>
                  </ul>
                  <ul>
                    <li>
                      <span className="labelSpan">
                        <FormattedMessage
                          id="interest.keyword"
                          defaultMessage="관심분야 키워드"
                        />
                      </span>
                      <div className="tagsInputs">
                        <p>
                          <FormattedMessage
                            id="input.interest"
                            defaultMessage="관심분야를 입력해주세요."
                          /> <FormattedMessage
                          id="spacebar.enter"
                          defaultMessage="(스페이스바나 엔터를 누르면 입력됩니다.)"
                        />
                        </p>
                        <div className='tagList'>
                          {detailState.tags.map((text, idx) => (
                            <span>{text}
                              <button 
                                className="delete" type="button">
                                <span className='material-icons' onClick={() => {rmvTag(idx)}}>close</span>
                              </button>
                            </span>  
                          ))}
                          <input type="text" value={detailState.tagCntnt}  style={{border: 'none'}}
                            onChange={(e)=>{
                              setDetailState({
                                ...detailState,
                                tagCntnt : e.target.value
                              });
                            }}   
                            onKeyUp={(e) => {
                              addTag(e);
                            }}
                          />
                        </div>
                      </div>
                    </li>
                  </ul>
                  <ul>
                    <li>
                      <label htmlFor="linkedIn">
                        <FormattedMessage
                          id="linkedin.link"
                          defaultMessage="링크드인 링크 수정"
                        />
                      </label>
                      <input type="text" className="fullWidth" id="linkedIn" 
                        placeholder={intl.formatMessage({
                          id: "placeholder.linkedin.link",
                          defaultMessage: "링크드인 주소가 있다면 입력해주세요."
                        })} 
                        value={detailState.hmpg} 
                        onChange={(e) => {
                          setDetailState({
                            ...detailState,
                            hmpg : e.target.value
                          })
                        }}/>
                    </li>
                  </ul>
                  <ul>
                    <li>
                      <label htmlFor="selfIntro">
                        <FormattedMessage
                          id="recruiter.self.intro"
                          defaultMessage="리크루터 자기소개"
                        />
                      </label>
                      <CKEditor
                        editor={ClassicEditor}
                        data={detailState.intro}
                        onChange={(event, editor) => {
                          detailState.intro = editor.getData();
                        }}
                      />      
                    </li>
                  </ul>
                </div>
              </div>
              <div className="formWrap">
                <h2>
                  <FormattedMessage
                    id="company.info.add"
                    defaultMessage="회사 정보 추가"
                  />
                </h2>
                <div className="formWrapInputs">
                  <ul className="twoCol">
                    <li>
                      <label htmlFor="companyName">
                        <FormattedMessage
                          id="company.name"
                          defaultMessage="회사명"
                        />    
                      </label>
                      <input type="text" id="companyName" 
                        placeholder={intl.formatMessage({
                          id: "placeholder.company.name",
                          defaultMessage: "회사명을 입력해주세요."
                        })} 
                        value={detailState.bizNm}
                        onChange={(e) => {
                          setDetailState({
                            ...detailState,
                            bizNm : e.target.value
                          })
                        }}/>
                    </li>
                    <li>
                      <label htmlFor="ceoName">
                        <FormattedMessage
                          id="ceo.name"
                          defaultMessage="대표자명"
                        />
                      </label>
                      <input type="text" id="ceoName" 
                        placeholder={intl.formatMessage({
                          id: "placeholder.ceo.name",
                          defaultMessage: "대표자명을 입력해주세요."
                        })}  
                        value={detailState.ceoNm} 
                        onChange={(e) => {
                          setDetailState({
                            ...detailState,
                            ceoNm : e.target.value
                          })
                        }}/>
                    </li>
                  </ul>
                  <ul className="twoCol">
                    <li>
                      <label htmlFor="corpNum">
                        <FormattedMessage
                          id="corporate.number"
                          defaultMessage="사업자등록번호"
                        />
                      </label>
                      <input type="text" id="corpNum" 
                        placeholder={intl.formatMessage({
                          id: "placeholder.corporate.number",
                          defaultMessage: "사업자등록번호를 입력해주세요."
                        })} 
                        value={detailState.bizNum}
                        onChange={(e) => {
                          setDetailState({
                            ...detailState,
                            bizNum : e.target.value
                          })
                        }}/>
                    </li>
                    <li>
                      <label htmlFor="siteUrl">
                        <FormattedMessage
                          id="homepage"
                          defaultMessage="홈페이지"
                        />
                      </label>
                      <input type="text" id="siteUrl" 
                        placeholder={intl.formatMessage({
                          id: "placeholder.homepage",
                          defaultMessage: "홈페이지 주소를 입력해주세요." 
                        })}
                        value={detailState.bizHmpg} 
                        onChange={(e) => {
                          setDetailState({
                            ...detailState,
                            bizHmpg : e.target.value
                          })
                        }}/>
                    </li>
                  </ul>
                  {/* 회사 주소 daum API 
                    <AddressInput state={detailState} setDetailState={setDetailState}/>
                   */}
                </div>
                <div className="formWrap">
                  <h2>
                    <FormattedMessage
                      id="identity.info.add"
                      defaultMessage="신원인증 정보 추가"
                    />
                  </h2>
                  <div className="formWrapInputs">
                    <ul>
                      <li className="fileColumn">
                        <p>
                          <FormattedMessage
                            id="upload.attach.file"
                            defaultMessage="첨부파일 등록 (기업 - 사업자등록증 / 개인 - 주민등록증 or 운전면허증)"
                          />
                        </p>
                        <div className="fileBox">
                          <label htmlFor="fileInput2" className="basicButton">
                            <FormattedMessage
                              id="image.attachment"
                              defaultMessage="이미지 첨부"
                            /> &gt;
                          </label>
                          <span className="fileName">{detailState.extraThmnlNm}</span>
                          <input type="file" id="fileInput2" className="fileInput" 
                            accept='.jpg, .png, .gif'
                            onChange={(e) => {
                              let tmpList = detailState.uploadFiles;
                              tmpList[2] = e.target.files[0];

                              let tmpJsonStrList = detailState.snglImgFileJsonStr;
                              tmpJsonStrList[2] = "";
                              
                              setDetailState({
                                ...detailState,
                                uploadFiles: tmpList,
                                snglImgFileJsonStr: tmpJsonStrList,
                                extraThmnl: "",
                                extraThmnlNm: e.target.files[0].name
                              })
                            }}
                          />
                        </div>
                        </li>
                    </ul>
                    <ul>
                      <li>
                        <label htmlFor="bankAccount">
                          <FormattedMessage
                            id="account.manage"
                            defaultMessage="계좌관리"
                          />
                        </label>
                        
                        <select name="bankAccount" id="bankAccount" className="marginInline" 
                          value={detailState.bankNm}
                          onChange={(e)=>{
                            setDetailState({
                              ...detailState,
                              bankNm: e.target.value
                            })
                          }}>
                          <option value="">
                            {intl.formatMessage({
                              id: "select.bank",
                              defaultMessage: "은행선택" 
                            })} 
                          </option>
                          {bankCd.map((type, idx) => (                        
                              <option value={type.cateCd}>{type.cdName}</option>
                          ))}   
                        </select>

                        <input type="text" placeholder={intl.formatMessage({
                          id: "placeholder.account",
                          defaultMessage: "계좌번호를 입력해주세요" 
                        })}
                          value={detailState.bankAccntNum}
                          onChange={(e)=>{
                            setDetailState({
                              ...detailState,
                              bankAccntNum : e.target.value
                            })
                          }}/>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="formWrap">
                  <h2>
                    <FormattedMessage
                      id="recruiter.join.info"
                      defaultMessage="리크루터 가입 정보"
                    />
                  </h2>
                  <div className="formWrapInputs">
                    <ul className="twoCol">
                      <li>
                        <label htmlFor="recruiterName">
                          <FormattedMessage
                            id="name"
                            defaultMessage="이름"
                          />
                        </label>
                        <input type="text" id="recruiterName" 
                          value={detailState.mbrNm} 
                          onChange={(e) => {
                            setDetailState({
                              ...detailState,
                              mbrNm : e.target.value
                            })
                          }}/>
                      </li>
                      <li>
                        <label htmlFor="position">
                          <FormattedMessage
                            id="position"
                            defaultMessage="직급"
                          />
                        </label>
                        <input type="text" id="position" 
                          value={detailState.mbrPos} 
                          onChange={(e) => {
                            setDetailState({
                              ...detailState,
                              mbrPos : e.target.value
                            })
                          }}/>
                      </li>
                    </ul>
                    <ul className="twoCol">
                      <li>
                        <label htmlFor="phone">
                          <FormattedMessage
                            id="phone.number"
                            defaultMessage="휴대폰 번호"
                          />
                        </label>
                        <input type="text" id="phone" 
                          value={detailState.celpNum} 
                          onChange={(e) => {
                            setDetailState({
                              ...detailState,
                              celpNum : e.target.value
                            })
                          }}/>
                      </li>
                      <li>
                        <label htmlFor="email">
                          <FormattedMessage
                            id="email"
                            defaultMessage="이메일"
                          />
                        </label>
                        <input type="text" id="email" 
                          value={detailState.email} 
                          onChange={(e) => {
                            setDetailState({
                              ...detailState,
                              email : e.target.value
                            })
                          }}/>
                      </li>
                    </ul>
                    <ul className="twoCol">
                      <li>
                        <label htmlFor="recruiterId">
                          <FormattedMessage
                            id="id"
                            defaultMessage="아이디"
                          />
                        </label>
                        <input type="text" id="recruiterId" 
                          value={detailState.mbrId} readOnly/>
                      </li>
                      <li>
                        <label htmlFor="recruiterPw">
                          <FormattedMessage
                            id="pw"
                            defaultMessage="비밀번호"
                          />
                        </label>
                        <input type="text" id="recruiterPw" 
                          value={detailState.pswd} 
                          onChange={(e) => {
                            setDetailState({
                              ...detailState,
                              pswd : e.target.value
                            })
                          }}/>
                        
                        <button type="button" className="basicButton marginInline" onClick={openModal1}>
                          <FormattedMessage
                            id="change"
                            defaultMessage="변경"
                          /> &gt;
                        </button>
                        {
                          <Modal visible={modalVisible1} closable={true} maskClosable ={true} onSubmit={goPswdCRUD} onClose={closeModal1} 
                          inputName={intl.formatMessage({
                            id: "yes",
                            defaultMessage: "예" 
                          })} 
                          closeName={intl.formatMessage({
                            id: "no",
                            defaultMessage: "아니오" 
                          })} >
                            <ChangePassword />
                          </Modal>
                        }             
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </form></div>
          <div className="flexAlignRight">
            <button className="modalOpenButton basicColored" type="button" onClick={openModal2}>
              회원탈퇴 &gt;
            </button>
            {
              <Modal visible={modalVisible2} closable={true} maskClosable={true} onSubmit={setWthdrYn} onClose={closeModal2} 
              inputName={intl.formatMessage({
                id: "yes",
                defaultMessage: "예" 
              })} 
              closeName={intl.formatMessage({
                id: "no",
                defaultMessage: "아니오" 
              })}>
                <LeaveWecruit state={detailState} />
              </Modal>
            } 
          </div>
          <div className="flexAlignCenter">
            <button className="bigButton blueColored marginTopBottom"  type="button" onClick={goCRUD}>
              <FormattedMessage
                id="save"
                defaultMessage="저장하기"
              /> &gt;
            </button>            
          </div>
        </section>
      </main>
  );
} 