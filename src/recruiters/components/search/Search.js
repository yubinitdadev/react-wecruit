import '../scss/search.scss';

import * as commonUtil from 'commons/modules/commonUtil';
import * as defVal from 'commons/modules/defVal';
import { Context } from 'contexts';
import { UPD_MBRUID } from 'contexts/actionTypes';
import { useContext, useEffect, useRef, useState } from 'react';
import {Link, useLocation, useNavigate} from 'react-router-dom';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { FormattedMessage, useIntl } from 'react-intl';

export const Search = () => {
  const intl = useIntl();

  const {
    state : {
      server,
      mbr
    },
    dispatch,
  } = useContext(Context);

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid")
    ,ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi")
  };
  
  const location = useLocation();
  const navigate = useNavigate();
  const moment = require('moment');
  let previewWindow = "";
  let previewImg = document.createElement("img");  
  const componentRef = useRef();
  

  const [mbrCnt, setMbrCnt] = useState({});
  const [listState, setListState] = useState([]);
  const [listCntState, setListCntState] = useState({});
  const [sConts, setSConts] = useState("");
  const [fltrState, setFltrState] = useState({});

  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(10);

  
  let locationState = {
    sConts : location.state.sConts
  }
  
  let prtnrChkCnt = 0;
  let ingChkCnt = 0;
  let cmmntChkCnt = 0;
  let endChkCnt = 0;

  useEffect(async () => {
    console.log("Page First Load EFFECT");
    document.getElementById("sConts").value = locationState.sConts;
    await initSetHandle();          
  },[])

  const initSetHandle = async () => {
    
    let goUrl = server.path + "/hire/hirelist"

    let data = {
      ssnMbrUid : ssn.ssnMbrUid,
      mbrUid : "",
      mbrDivi : "MBRDIVI-02",
      sConts: (document.getElementById("sConts").value)?(document.getElementById("sConts").value):(''),
      sRecMbrUid : ssn.ssnMbrUid,
      hireStat : "recsrch"
    }

   
    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");
    let axiosResData = axiosRes.data.hireArr;
    let tmpAxiosResData = [];

    let prtnrCnt = 0;
    let ingCnt = 0;
    let cmmntCnt = 0;
    let endCnt = 0;

    axiosResData.forEach((resData, idx)=>{
      resData["rcmndEndDttm"] = moment(resData.rcmndEndDttm).format("YYYY-MM-DD HH:mm");
      
      resData["bizCateStr"] = defVal.SplitToArray(resData.bizCateStr, "<;:>");
      resData["tags"] = defVal.SplitToArray(resData.tags, "<;:>");
      
      tmpAxiosResData.push(resData);
      
      //거래고객사( 써치펌에 리크루터가 등록되고 리크루터를 채택하지 않은 상태 )
      if( resData.hireStat == "ing" && resData.myCmmntCnt == 0 && resData.recSrchFirmCnt > 0  )prtnrCnt++;
      //제안한 채용공고( 리크루터를 채택하지 않은 상태 )
      if( resData.hireStat == "ing" && resData.myCmmntCnt > 0 )cmmntCnt++;
      //진행중 채용공고( 리크루터를 채택한 상태 )
      if( resData.hireStat == "ing" && resData.myCmmntChooseCnt > 0)ingCnt++;
      //종료 채용공고( 리크루터가 제안했거나 거래고객사인 상태 )      
      else if( resData.hireStat == "end" && ( resData.myCmmntCnt > 0 || resData.recSrchFirmCnt > 0 ) )endCnt++;     
    })

    setListState(tmpAxiosResData);
    var tmpCnt = {
      ingCnt : ingCnt,
      cmmntCnt : cmmntCnt,
      endCnt : endCnt,
    }
    setListCntState(tmpCnt);

    var tmpMbrCnt = await getMbrCnt();

    
  }

  const getMbrCnt = async () => {
    let goUrl = server.path + "/mbr/mbrcnt";
    let data = {
      hireUid : ""
    }; 
    
    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");
    let tmpDate = axiosRes.data.mbrcnt
    setMbrCnt(tmpDate);    
    return tmpDate;
  }

  ////////////////////////////////////////////////////////////////////////////////////

  const goSearch =() => {
    //var sConts = document.getElementById("sConts").value;
    //navigate("/recruiters/search", { sConts : sConts } );
    
    //return false;
    initSetHandle();
   }

   const onKeyUp =(e) => {
    if(e.key == "Enter")
    {
      goSearch();
    }
    return false;
   }

   function onKeyDown(keyEvent) {
    if ((keyEvent.charCode || keyEvent.keyCode) === 13) {
      keyEvent.preventDefault();
    }
  }

  return (
    <main className="dividedSections recruitersMain noBorder">
      <section className="recruitNoticeStats">
        <div>
          <p>
            <FormattedMessage
              id="posting.suggested"
              defaultMessage="제안한 채용공고"
            />
          </p>
          <Link to='/recruiters/suggest'>{listCntState.cmmntCnt}</Link>
        </div>
        <div>
          <p>
            <FormattedMessage
              id="posting.ing"
              defaultMessage="진행중 채용공고"
            />
          </p>
          <Link to='/recruiters/ing'>{listCntState.ingCnt}</Link>
        </div>
        <div>
          <p>
            <FormattedMessage
              id="posting.done"
              defaultMessage="종료된 채용공고"
            />
          </p>
          <Link to='/recruiters/done'>{listCntState.endCnt}</Link>
        </div>
      </section>
      <section>
        <form className="searchForm recruitersForm" onKeyDown={(e) => {onKeyDown(e)}}>
          <input 
            type="search" 
            placeholder={intl.formatMessage({
              id: "placeholder.search.word",
              defaultMessage: "검색어를 입력하세요." 
            })}
            id="sConts"  
            onKeyUp={(e) => {
              onKeyUp(e);
            }}
          />
          
          <input type="button" className="basicButton" value="검색"
            onClick={(e) => {
              goSearch();
            }}
          />
        </form>
      </section>
      <section className="searchResult">
        <p><strong>test</strong> <FormattedMessage
              id="searched.result"
              defaultMessage="검색결과"
            /> : <FormattedMessage
            id="total"
            defaultMessage="총"
          /> <strong>{listState.length}</strong><FormattedMessage
          id="case"
          defaultMessage="건"
        /></p>
        <ul>
          
        
        {
          listState.map((list, idx)=>
            (

              <li>
                <Link to={
                  (list.hireStat === 'end')? ('/recruiters/done-detail'):
                  (list.hireStat === 'ing' && list.myCmmntCnt == 0 && list.recSrchFirmCnt > 0)? ('/recruiters/partner-detail'):
                  (list.hireStat === 'ing' && list.myCmmntCnt > 0 && list.myCmmntChooseCnt == 0 )? ('/recruiters/suggest-detail'):
                  (list.hireStat === 'ing' && list.myCmmntChooseCnt > 0 )? ('/recruiters/ing-detail'):
                  ('/recruiters/ing-detail')
                }
                state={{ hireUid: list.hireUid, mbrUid: list.mbrUid }}>{list.title}</Link>
                
                  {
                    (list.hireStat === 'end')? (<span className="basicButton">
                      <FormattedMessage
                        id="posting.done"
                        defaultMessage="종료된 채용공고"
                      /></span>):
                    (list.rcmndEndYn === 'Y')? ( <span className="basicButton">
                      <FormattedMessage
                        id="candidate.recommend.finished"
                        defaultMessage="후보자 추천마감"
                      /></span>):
                    (list.recEndYn === 'Y')? (<span className="basicButton">
                      <FormattedMessage
                        id="recruiter.finished"
                        defaultMessage="리크루터 마감"
                      /></span>): 
                    (list.hireStat === 'ing' && list.recEndYn !== 'Y' 
                      && list.myCmmntCnt > 0 && list.myCmmntChooseCnt === 0)? (<span className="basicButton">
                        <FormattedMessage
                          id="suggestion.ing"
                          defaultMessage="제안 진행중"
                        /></span>):
                    (list.hireStat === 'ing'  
                      && list.myCmmntCnt === 0 && list.myCmmntChooseCnt === 0)? (<span className="basicButton">
                        <FormattedMessage
                          id="posting.new"
                          defaultMessage="신규채용 공고"
                        />
                      </span>):
                    ('')                   
                  }                   
                
                <ul>
                  <li>
                    <FormattedMessage
                      id="registration.date"
                      defaultMessage="등록일"
                    /> : &nbsp;{moment(list.rgstDttm).format("YYYY-MM-DD")} 
                    &nbsp;/ 
                    <FormattedMessage
                      id="recruitment.deadline"
                      defaultMessage="인재추천 마감일"
                    /> : {(list.rcmndUntilDon === "Y") 
                    ? (`${intl.formatMessage({
                      id: "until.hired",
                      defaultMessage: "채용시까지"
                    })}`) : (list.rcmndEndDttm)}  
                    &nbsp;
                    / <strong><FormattedMessage
                                id="recruitment.people.number"
                                defaultMessage="모집인원"
                              /> 
                    : {list.hireSize}<FormattedMessage
                    id="people.count"
                    defaultMessage="명"
                  /></strong>
                  </li>
                  <li><FormattedMessage
                      id="business.type"
                      defaultMessage="업종"
                    /> : &nbsp;
                      {
                        list.bizCateStr.map((type, idx)=>
                          (idx === 0 )?(type):('')                        
                        )
                      }   
                  </li>
                  <li>
                    <FormattedMessage
                      id="keyword"
                      defaultMessage="키워드"
                    /> : &nbsp;
                      {
                        list.tags.map((type, idx)=>
                        ( <strong>#{type} </strong>)                        
                        )
                      }                     
                  </li>
                </ul>
              </li>
            )
          )
        }
        </ul>
      </section>
    </main>
  );
}