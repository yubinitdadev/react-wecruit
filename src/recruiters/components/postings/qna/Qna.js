import { Link, useLocation, useNavigate } from "react-router-dom";
import { useContext, useEffect, useState } from "react";
import { Context } from "contexts";
import { toast, ToastContainer } from "react-toastify";
import axios from "axios";
import fileDownload from "js-file-download";
import Modal from "modal-all/ModalAll";

import { axiosCrudOnSubmit } from "commons/modules/commonUtil";
import * as commonUtil from "commons/modules/commonUtil";
import { snglFileUpload } from "commons/modules/multipartUtil";
import * as defVal from "commons/modules/defVal";
import { AddressInput } from "commons/components/AddressInput";
import { ImagePreview } from "commons/components/inputs/image-preview/ImagePreview";
import { TagTemplate } from "commons/components/tags/TagTemplate";
import BusinessTypeTemplate from "commons/components/inputs/business-type/BusinessTypeTemplate";
import WorkPlaceTemplate from "commons/components/inputs/work-place/WorkPlaceTemplate";
import WorkPositionTemplate from "commons/components/inputs/work-position/WorkPositionTemplate";
import { UPD_MBRUID } from "contexts/actionTypes";
import { FormattedMessage, useIntl } from "react-intl";

export const Qna = () => {
  const intl = useIntl();

  let PROC_CHK = "";

  const {
    state: { server, mbr },
    dispatch,
  } = useContext(Context);

  const location = useLocation();
  const navigate = useNavigate();
  const moment = require("moment");
  let previewWindow = "";
  let previewImg = document.createElement("img");

  const [files, setFiles] = useState("");
  const [axiosFilesRes, setAxiosFilesRes] = useState("");

  const [mbrFilesRes, setMbrFilesRes] = useState([]);
  const [bizFilesRes, setBizFilesRes] = useState([]);
  const [extraFilesRes, setExtraFilesRes] = useState([]);

  const bizKndCd = defVal.BizKndCd();
  const salesSizeCd = defVal.SalesSizeCd();
  const empSizeCd = defVal.EmpSizeCd();
  const hireSizeCd = defVal.HireSizeCd();
  const celpFCd = defVal.CelpFCd();

  const [mbrCnt, setMbrCnt] = useState({});
  const [bizCate, setBizCate] = useState([]);
  const [empAddrCate, setEmpAddrCate] = useState([]);
  const [workPosCate, setWorkPosCate] = useState([]);

  const [recmmdState, setRecmmdState] = useState(
    defVal.setAxiosCnddtRcmmdState(null)
  );
  const [detailState, setDetailState] = useState(defVal.setAxiosJobState(null));
  const [qaListState, setQaListState] = useState([]);
  const [qaCntnt, setQaCntnt] = useState(defVal.setAxiosQaCntntState());

  const [modalVisible1, setModalVisible1] = useState(false);
  const [titleLength, setTitleLength] = useState("0");
  const [untilDone, setUntilDone] = useState(false);
  const [tagCnt, setTagCnt] = useState(0);

  const [bizCateCdCombo, setBizCateCdCombo] = useState([
    defVal.SelCateCdCombo(),
  ]);
  const [empAddrCateCdCombo, setEmpAddrCateCdCombo] = useState([
    defVal.SelCateCdCombo(),
  ]);
  const [workPosCateCdCombo, setWorkPosCateCdCombo] = useState([
    defVal.SelCateCdCombo(),
  ]);

  let atchFiles;

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid"),
    ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi"),
  };

  let locationState = {
    hireUid: location.state.hireUid,
    mbrUid: location.state.mbrUid,
    backUrl: location.state.backUrl ? location.state.backUrl : "",
  };

  useEffect(async () => {
    console.log("Page First Load EFFECT");
    initSetHandle();
  }, []);

  async function initSetHandle() {
    //detailState.ssnMbrUid = ssn.ssnMbrUid;

    let goUrl = server.path + "/hire/hiredetail";
    let data = {
      ssnMbrUid: ssn.ssnMbrUid,
      ssnMbrDivi: ssn.ssnMbrDivi,
      hireUid: locationState.hireUid,
      sRecMbrUid: ssn.ssnMbrUid,
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    setDetailState(axiosRes.data);

    var tmpMbrCnt = await getMbrCnt();
    var tmpqaList = await getQaList();
  }

  const getMbrCnt = async () => {
    let goUrl = server.path + "/mbr/mbrcnt";
    let data = {
      hireUid: locationState.hireUid,
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    let tmpDate = axiosRes.data.mbrcnt;
    setMbrCnt(tmpDate);
    return tmpDate;
  };

  const getQaList = async () => {
    let goUrl = server.path + "/qa/qalist";
    let data = {
      hireUid: locationState.hireUid,
      sRcvMbrUid : ssn.ssnMbrUid
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    var tmpDate = axiosRes.data.resArr;
    setQaListState(tmpDate);
    return tmpDate;
  };

  const reRender = () => {
    setDetailState({
      ...detailState,
      initSetCmplt: true,
    });
  };

  ////////////////////////////////////////////////////////////////////////////////

  const handleDownload = (idx, filePath, fileNm) => {
    if (filePath != "") {
      let goUrl = server.host + filePath;
      axios
        .get(goUrl, {
          responseType: "blob",
        })
        .then((res) => {
          fileDownload(res.data, fileNm);
        });
    } else {
      fileDownload(detailState.uploadFiles[idx], fileNm);
    }
  };

  const handlePreview = (idx, filePath) => {
    previewWindow = window.open(
      "",
      "",
      "width=600,height=400,left=200,top=200"
    );

    if (filePath != "") {
      let goUrl = server.host + filePath;
      previewImg.src = goUrl;
    } else {
      let file = detailState.uploadFiles[idx];
      let reader = new FileReader();

      reader.onload = (function (file) {
        return function (e) {
          previewImg.src = e.target.result;
        };
      })(file);

      reader.readAsDataURL(file);
    }

    previewWindow.document.body.appendChild(previewImg);
  };

  const onKeyUp = (e, uid, lvl, seq) => {
    if (e.key == "Enter") goCRUD(uid, lvl, seq);
    return false;
  };

  //////////////////////////////////////////////////////////////////////////////////////////////////

  const jsonCompResult = (resJson) => {
    console.log("PROC_CHK : " + PROC_CHK);

    if (resJson.resCd !== "0000") {
      toast.error(
        intl.formatMessage({
          id: "error.during.save",
          defaultMessage: "저장 중 오류가 발생했습니다.",
        })
      );
      return;
    } else {
      if (PROC_CHK === "INS") {
        PROC_CHK = "";
        initSetHandle();
      }
      if (PROC_CHK === "DUPL") {
        if (resJson.rcmndUid === "DUPL")
          toast.error(
            intl.formatMessage({
              id: "error.already.recommended",
              defaultMessage: "이미 추천된 후보자입니다.",
            })
          );
        else
          toast.info(
            intl.formatMessage({
              id: "info.recommendable.candidate",
              defaultMessage: "추천 가능한 후보자입니다.",
            })
          );
        PROC_CHK = "";
      } else if (PROC_CHK === "FILEUPLOAD") {
        let jsonStr = JSON.stringify(resJson.resFileList);
        let fileList = JSON.parse(jsonStr);
        let filePos = 0;

        for (let k = 0; k < fileList.length; k++) {
          filePos = fileList[k].filePos;
          recmmdState.cnddtAtchFilesStr[filePos] = JSON.stringify(fileList[k]);
        }

        cntntsCRUD();
      }
    }
  };

  const goCRUD = async (uid, lvl, seq) => {
    var data = defVal.setAxiosQaCntntState(null);
    data.cntnt = document.getElementById("qsCntnt").value;
    data.mbrUid = ssn.ssnMbrUid;
    data.hireUid = locationState.hireUid;
    data.lvl = lvl;
    data.refUid = uid;

    var txt = intl.formatMessage({
      id: "error.need.input.question",
      defaultMessage: "질문을 입력해주세요.",
    });
    if (lvl == 2) {
      data.refUid = uid;
      data.cntnt = qaListState[seq].repCntnt;
      //alert(data.cntnt)
      txt = intl.formatMessage({
        id: "error.need.input.answer",
        defaultMessage: "답변을 입력해주세요.",
      });
    }

    if (commonUtil.CheckIsEmptyFromVal(data.cntnt, txt) === false) return;

    document.getElementById("qsCntnt").value = "";

    PROC_CHK = "INS";
    cntntsCRUD(data);
  };

  const cntntsCRUD = async (data) => {
    PROC_CHK = "INS";

    let goUrl = server.path + "/qa/qainsert";
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    jsonCompResult(axiosRes.data);
  };

  return (
    <main className="recruitersMain">
      <section className="recruitLists">
        <div className="ingList">
          <ul>
            <li>
              <div className="listLeft">
                <h3>
                  <Link
                    to={
                      locationState.backUrl != ""
                        ? locationState.backUrl
                        : "/recruiters/ing-detail"
                    }
                    state={{
                      hireUid: detailState.hireUid,
                      mbrUid: detailState.mbrUid,
                    }}
                  >
                    [{detailState.pubYn == "Y" ? detailState.bizNm : "*****"}]{" "}
                    {detailState.title}
                  </Link>
                </h3>
                <p>
                  <FormattedMessage
                    id="registration.date"
                    defaultMessage="등록일"
                  />{" "}
                  : &nbsp;{moment(detailState.rgstDttm).format("YYYY-MM-DD")}
                  &nbsp; /{" "}
                  <FormattedMessage
                    id="recruitment.deadline"
                    defaultMessage="인재추천 마감일"
                  />{" "}
                  :{" "}
                  {detailState.rcmndUntilDon === "Y"
                    ? `${intl.formatMessage({
                        id: "until.hired",
                        defaultMessage: "채용시까지",
                      })}`
                    : commonUtil.substr(detailState.rcmndEndDttm, 0, 19)}
                  &nbsp; /{" "}
                  <strong>
                    <FormattedMessage
                      id="recruitment.people.number"
                      defaultMessage="모집인원"
                    />{" "}
                    : {detailState.hireSize}
                    <FormattedMessage id="people.count" defaultMessage="명" />
                  </strong>
                </p>
                <ul>
                  <li>
                    <FormattedMessage
                      id="suggestion.service.fee"
                      defaultMessage="서비스 수수료율"
                    />{" "}
                    : &nbsp;
                    {detailState.srvcFeeFixYn === "Y"
                      ? `${intl.formatMessage({
                          id: "suggestion.fixed.fee.system",
                          defaultMessage: "정액제",
                        })}`
                      : detailState.srvcFeePctMin +
                        "% ~ " +
                        detailState.srvcFeePctMax +
                        "%"}
                  </li>
                  <li>
                    <FormattedMessage
                      id="suggestion.service.guarantee"
                      defaultMessage="서비스 보증조건"
                    />{" "}
                    : &nbsp;
                    {detailState.srvcWrtyPeriod}
                    <FormattedMessage id="months" defaultMessage="개월" />{" "}
                    <FormattedMessage
                      id="suggestion.incase.retire"
                      defaultMessage="이내 퇴직시"
                    />
                    {detailState.srvcWrtyMthd === "prorated"
                      ? `${intl.formatMessage({
                          id: "suggestion.caculated.refund",
                          defaultMessage: "일할계산 환불",
                        })}`
                      : detailState.srvcWrtyMthd === "all"
                      ? `${intl.formatMessage({
                          id: "suggestion.all.refund",
                          defaultMessage: "100% 전액 환불",
                        })}`
                      : detailState.srvcWrtyMthd === "etc"
                      ? `${intl.formatMessage({
                          id: "etc",
                          defaultMessage: "기타",
                        })}`
                      : ""}
                  </li>
                </ul>
              </div>
              <div className="listRight">
                {detailState.hireStat === "end" ? (
                  ""
                ) : detailState.rcmndEndYn === "Y" ? (
                  <button type="button" className="recommendDone">
                    <FormattedMessage
                      id="candidate.recommend.finished"
                      defaultMessage="후보자 추천마감"
                    />
                  </button>
                ) : detailState.hireStat === "ing" &&
                  detailState.myCmmntChooseCnt > 0 &&
                  detailState.rcmndEndYn !== "Y" ? (
                  <Link
                    to="/recruiters/candidate-recommend"
                    className="recommendCandidate"
                    state={{
                      hireUid: detailState.hireUid,
                      mbrUid: detailState.mbrUid,
                    }}
                  >
                    <FormattedMessage
                      id="candidate.recommend"
                      defaultMessage="후보자 추천하기"
                    />{" "}
                    &gt;
                  </Link>
                ) : detailState.recEndYn === "Y" ? (
                  <div className="recommendDone">
                    <FormattedMessage
                      id="recruiter.finished"
                      defaultMessage="리크루터 마감"
                    />
                  </div>
                ) : /*거래고객사 채용공고*/
                detailState.hireStat === "ing" &&
                  detailState.recEndYn !== "Y" &&
                  detailState.myCmmntCnt === 0 ? (
                  ""
                ) : /*제안진행중 채용공고*/
                detailState.hireStat === "ing" &&
                  detailState.recEndYn !== "Y" &&
                  detailState.myCmmntCnt > 0 &&
                  detailState.myCmmntChooseCnt === 0 ? (
                  <div className="status">
                    <FormattedMessage
                      id="suggestion.ing"
                      defaultMessage="제안 진행중"
                    />
                  </div>
                ) : (
                  ""
                )}

                <div className="threeSections">
                  <div>
                    <span>
                      <FormattedMessage
                        id="recruiter.suggested"
                        defaultMessage="제안한 리크루터"
                      />
                    </span>
                    <em>
                      {detailState.cmmntCnt}
                      <FormattedMessage id="people.count" defaultMessage="명" />
                    </em>
                  </div>
                  <div>
                    <span>
                      <FormattedMessage
                        id="recruiter.selected"
                        defaultMessage="선택한 리크루터"
                      />
                    </span>
                    <em>
                      <strong>{detailState.cmmntChooseCnt}</strong>/
                      {detailState.recSize}
                      <FormattedMessage id="people.count" defaultMessage="명" />
                    </em>
                  </div>
                  <div>
                    <span>
                      <FormattedMessage
                        id="candidate.recommended"
                        defaultMessage="추천된 후보자"
                      />
                    </span>
                    <Link
                      to="/recruiters/candidate"
                      state={{
                        hireUid: detailState.hireUid,
                        mbrUid: detailState.mbrUid,
                      }}
                    >
                      {detailState.recmmndCnt}
                      <FormattedMessage id="people.count" defaultMessage="명" />
                      ({detailState.chkingRecmmndCnt}
                      <FormattedMessage id="people.count" defaultMessage="명" />
                      /{detailState.nopassRecmmndCnt}
                      <FormattedMessage id="people.count" defaultMessage="명" />
                      )
                    </Link>
                  </div>
                </div>
                <div className="qnaStatus">
                  <Link to="/recruiters/qna">
                    Q&amp;A{" "}
                    <span>
                      {detailState.qaCnt}
                      <FormattedMessage id="count" defaultMessage="개" />
                    </span>
                  </Link>
                </div>
              </div>
            </li>
          </ul>
        </div>

        <div className="qnaList">
          <div className="flexSpaceBetween">
            <h5>
              <FormattedMessage id="recruiter" defaultMessage="리크루터" />{" "}
              Q&amp;A
            </h5>
            <span>
              <FormattedMessage id="all" defaultMessage="전체" /> Q&amp;A{" "}
              <strong>{qaListState.length}</strong>
              <FormattedMessage id="count" defaultMessage="개" />
            </span>
          </div>
          <div className="commentInput">
            <input
              type="text"
              name="qsCntnt"
              id="qsCntnt"
              placeholder={intl.formatMessage({
                id: "description.message.to.employer",
                defaultMessage: "인사 담당자에게 전달할 내용을 알려주세요",
              })}
              onKeyUp={(e) => {
                onKeyUp(e, "", 1, 0);
              }}
            />
            <input
              type="button"
              value={intl.formatMessage({
                id: "upload.writing",
                defaultMessage: "글올리기",
              })}
              className="basicButton"
              onClick={(e) => {
                goCRUD("", 1, 0);
              }}
            />
          </div>

          <ul>
            <input
              type="text"
              name="repCntnt"
              id="repCntnt"
              value=""
              style={{ display: "none" }}
            />
            {qaListState.map((list, idx) =>
              list.lvl == 1  ? (
                <li>
                  <span>
                    {list.mbrNm != "" ? list.mbrNm : list.mbrId} |{" "}
                    <time>{commonUtil.substr(list.rgstDttm, 0, 19)}</time>
                  </span>
                  <p>{list.cntnt}</p>

                  {qaListState.map((sublist, subidx) =>
                    sublist.lvl == 2 && sublist.refUid == list.refUid ? (
                      <ul className="replyText">
                        <li>
                          <span className="material-icons">
                            subdirectory_arrow_right
                          </span>
                          <span>
                            {sublist.mbrNm != ""
                              ? sublist.mbrNm
                              : sublist.mbrId}{" "}
                            |{" "}
                            <time>
                              {commonUtil.substr(sublist.rgstDttm, 0, 19)}
                            </time>
                          </span>
                          <p>{sublist.cntnt}</p>
                        </li>
                      </ul>
                    ) : (
                      ""
                    )
                  )}

                  <div className="reply">
                    <input
                      type="text"
                      value={list.repCntnt}
                      onChange={(e) => {
                        qaListState[idx].repCntnt = e.target.value;
                        reRender();
                      }}
                      onKeyUp={(e) => {
                        onKeyUp(e, list.brdUid, 2, idx);
                      }}
                    />
                    <input
                      type="button"
                      className="basicButton"
                      value={intl.formatMessage({
                        id: "upload.writing",
                        defaultMessage: "답글쓰기",
                      })}
                      onClick={(e) => {
                        goCRUD(list.brdUid, 2, idx);
                      }}
                    />
                  </div>
                </li>
              ) : (
                ""
              )
            )}
          </ul>
        </div>
      </section>
    </main>
  );
};
