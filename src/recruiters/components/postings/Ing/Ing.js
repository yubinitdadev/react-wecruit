import { DownloadBtns } from "commons/components/download-btn/DownloadBtns";
import { Pagination } from "commons/components/pagination/Pagination";
import { axiosCrudOnSubmit } from "commons/modules/commonUtil";
import { Context } from "contexts";
import { UPD_MBRUID } from "contexts/actionTypes";
import { useContext, useEffect, useRef, useState } from "react";
import { Link } from "react-router-dom";
import { CopyToClipboard } from "react-copy-to-clipboard";
import { FormattedMessage, useIntl } from "react-intl";

export const Ing = () => {
  const intl = useIntl();

  const {
    state: { server, mbr },
    dispatch,
  } = useContext(Context);

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid"),
    ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi"),
  };

  const moment = require("moment");
  const componentRef = useRef();

  const [noDataState, setNoDataState] = useState("none");
  const [mbrCnt, setMbrCnt] = useState({});
  const [listState, setListState] = useState([]);
  const [sConts, setSConts] = useState("");
  const [fltrState, setFltrState] = useState({});

  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(10);

  const indexOfLast = currentPage * postsPerPage;
  const indexOfFirst = indexOfLast - postsPerPage;

  function currentPosts(tmp) {
    let currentPosts = 0;
    currentPosts = tmp.slice(indexOfFirst, indexOfLast);
    return currentPosts;
  }

  async function goSrch() {
    let goUrl = server.path + "/hire/hirelist";
    let data = {
      ssnMbrUid: ssn.ssnMbrUid,
      mbrUid: "",
      mbrDivi: "MBRDIVI-03",
      hireStat: "recing",
      sRecMbrUid: ssn.ssnMbrUid,
      sRcvMbrUid: ssn.ssnMbrUid,
      sConts: sConts,
      sFltr: fltrState,
      sFltrCate: Object.keys(fltrState)[0],
      sFltrCont: Object.values(fltrState)[0],
    };

    initSetHandle(data);
  }

  const changeFltr = (fltr) => {
    let tmpFltr = fltr === "ASC" ? "DESC" : fltr === "DESC" ? "ASC" : "ASC";

    return tmpFltr;
  };

  const initSetHandle = async (data) => {
    let goUrl = server.path + "/hire/hirelist";
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    let axiosResData = axiosRes.data.hireArr;
    let tmpList = [];
    axiosResData.forEach((resData, idx) => {
      resData["rcmndEndDttm"] = moment(resData.rcmndEndDttm).format(
        "YYYY-MM-DD HH:mm"
      );

      tmpList.push(resData);
    });

    var tmpMbrCnt = await getMbrCnt();
    if (tmpList.length == 0) setNoDataState("");
    setListState(tmpList);
  };

  const getMbrCnt = async () => {
    let goUrl = server.path + "/mbr/mbrcnt";
    let data = {
      hireUid: "",
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    let tmpDate = axiosRes.data.mbrcnt;
    setMbrCnt(tmpDate);
    return tmpDate;
  };

  useEffect(() => {
    goSrch();
  }, [fltrState]);

  useEffect(async () => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <main className="recruitersMain">
      <section className="recruitLists">
        <div className="ingList">
          <h2 className="sectionTitle">
            <span>
              <FormattedMessage
                id="posting.ing"
                defaultMessage="진행중 채용공고"
              />
            </span>
          </h2>
          <div className="noRecruitList" style={{ display: noDataState }}>
            <FormattedMessage
              id="description.no.ing.posting"
              defaultMessage="진행중인 채용공고가 없습니다."
            />
          </div>
          <ul>
            {listState.map((list, idx) => (
              <li>
                <div className="listLeft">
                  <h3>
                    <Link
                      to="/recruiters/ing-detail"
                      state={{ hireUid: list.hireUid, mbrUid: list.mbrUid }}
                    >
                      [{list.pubYn == "Y" ? list.bizNm : "*****"}] {list.title}
                    </Link>
                  </h3>
                  <p>
                    <FormattedMessage
                      id="registration.date"
                      defaultMessage="등록일"
                    />{" "}
                    : &nbsp;{moment(list.rgstDttm).format("YYYY-MM-DD")}
                    &nbsp; /{" "}
                    <FormattedMessage
                      id="recruitment.deadline"
                      defaultMessage="인재추천 마감일"
                    />{" "}
                    :{" "}
                    {list.rcmndUntilDon === "Y"
                      ? `${intl.formatMessage({
                          id: "until.hired",
                          defaultMessage: "채용시까지",
                        })}`
                      : list.rcmndEndDttm}
                    &nbsp; /{" "}
                    <strong>
                      <FormattedMessage
                        id="recruitment.people.number"
                        defaultMessage="모집인원"
                      />{" "}
                      : {list.hireSize}
                      <FormattedMessage id="people.count" defaultMessage="명" />
                    </strong>
                  </p>
                  <ul>
                    <li>
                      <FormattedMessage
                        id="suggestion.service.fee"
                        defaultMessage="서비스 수수료율"
                      />{" "}
                      : &nbsp;
                      {list.srvcFeeFixYn === "Y"
                        ? `${intl.formatMessage({
                            id: "suggestion.fixed.fee.system",
                            defaultMessage: "정액제",
                          })}`
                        : list.srvcFeePctMin +
                          "% ~ " +
                          list.srvcFeePctMax +
                          "%"}
                    </li>
                    <li>
                      <FormattedMessage
                        id="suggestion.service.guarantee"
                        defaultMessage="서비스 보증조건"
                      />{" "}
                      : &nbsp;
                      {list.srvcWrtyPeriod}
                      <FormattedMessage
                        id="months"
                        defaultMessage="개월"
                      />{" "}
                      <FormattedMessage
                        id="suggestion.incase.retire"
                        defaultMessage="이내 퇴직시"
                      />
                      {list.srvcWrtyMthd === "prorated"
                        ? `${intl.formatMessage({
                            id: "suggestion.caculated.refund",
                            defaultMessage: "일할계산 환불",
                          })}`
                        : list.srvcWrtyMthd === "all"
                        ? `${intl.formatMessage({
                            id: "suggestion.all.refund",
                            defaultMessage: "100% 전액 환불",
                          })}`
                        : list.srvcWrtyMthd === "etc"
                        ? `${intl.formatMessage({
                            id: "etc",
                            defaultMessage: "기타",
                          })}`
                        : ""}
                    </li>
                  </ul>
                </div>
                <div className="listRight">
                  {list.rcmndEndYn === "Y" ? (
                    <button type="button" className="recommendDone">
                      <FormattedMessage
                        id="candidate.recommend.finished"
                        defaultMessage="후보자 추천마감"
                      />
                    </button>
                  ) : (
                    <Link
                      to="/recruiters/candidate-recommend"
                      className="recommendCandidate"
                      state={{ hireUid: list.hireUid, mbrUid: list.mbrUid }}
                    >
                      <FormattedMessage
                        id="candidate.recommend"
                        defaultMessage="후보자 추천하기"
                      />{" "}
                      &gt;
                    </Link>
                  )}

                  <div className="threeSections">
                    <div>
                      <span>
                        <FormattedMessage
                          id="recruiter.suggested"
                          defaultMessage="제안한 리크루터"
                        />
                      </span>
                      <em>
                        {list.cmmntCnt}
                        <FormattedMessage
                          id="people.count"
                          defaultMessage="명"
                        />
                      </em>
                    </div>
                    <div>
                      <span>
                        <FormattedMessage
                          id="recruiter.selected"
                          defaultMessage="선택한 리크루터"
                        />
                      </span>
                      <em>
                        <strong>{list.cmmntChooseCnt}</strong>/{list.recSize}
                        <FormattedMessage
                          id="people.count"
                          defaultMessage="명"
                        />
                      </em>
                    </div>
                    <div>
                      <span>
                        <FormattedMessage
                          id="candidate.recommended"
                          defaultMessage="추천된 후보자"
                        />
                      </span>
                      <Link
                        to="/recruiters/candidate"
                        state={{ hireUid: list.hireUid, mbrUid: list.mbrUid }}
                      >
                        {list.recmmndCnt}
                        <FormattedMessage
                          id="people.count"
                          defaultMessage="명"
                        />
                        ({list.chkingRecmmndCnt}
                        <FormattedMessage
                          id="people.count"
                          defaultMessage="명"
                        />
                        /{list.nopassRecmmndCnt}
                        <FormattedMessage
                          id="people.count"
                          defaultMessage="명"
                        />
                        )
                      </Link>
                    </div>
                  </div>
                  <div className="qnaStatus">
                    <Link
                      to="/recruiters/qna"
                      state={{ hireUid: list.hireUid, mbrUid: list.mbrUid }}
                    >
                      Q&amp;A{" "}
                      <span>
                        {list.qaCnt}
                        <FormattedMessage id="count" defaultMessage="개" />
                      </span>
                    </Link>
                  </div>
                </div>
              </li>
            ))}
          </ul>
        </div>
        <ul className="listNotice">
          <li>
            <FormattedMessage
              id="description.ing1"
              defaultMessage="ㆍ 채용제안에 입찰되어 진행중인 채용공고 목록입니다."
            />
          </li>
          <li>
            <FormattedMessage
              id="description.ing2"
              defaultMessage="ㆍ 후보자 추천하기 버튼을 누르시면 채용건에 관하여 후보자를 추천하실 수 있습니다."
            />
          </li>
          <li>
            <FormattedMessage
              id="description.ing3"
              defaultMessage="ㆍ 인사담당자가 후보자 추천 마감 버튼을 누르면 더 이상 후보자를 추천하실 수 없습니다."
            />
          </li>
        </ul>
      </section>
    </main>
  );
};
