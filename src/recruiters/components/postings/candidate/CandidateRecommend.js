import { Link, useLocation, useNavigate } from "react-router-dom";
import { useContext, useEffect, useState } from "react";
import { Context } from "contexts";
import { toast, ToastContainer } from "react-toastify";
import axios from "axios";
import fileDownload from "js-file-download";
import Modal from "modal-all/ModalAll";

import { axiosCrudOnSubmit } from "commons/modules/commonUtil";
import * as commonUtil from "commons/modules/commonUtil";
import { snglFileUpload } from "commons/modules/multipartUtil";
import * as defVal from "commons/modules/defVal";
import { AddressInput } from "commons/components/AddressInput";
import { ImagePreview } from "commons/components/inputs/image-preview/ImagePreview";
import { TagTemplate } from "commons/components/tags/TagTemplate";
import BusinessTypeTemplate from "commons/components/inputs/business-type/BusinessTypeTemplate";
import WorkPlaceTemplate from "commons/components/inputs/work-place/WorkPlaceTemplate";
import WorkPositionTemplate from "commons/components/inputs/work-position/WorkPositionTemplate";
import { UPD_MBRUID } from "contexts/actionTypes";
import { FormattedMessage, useIntl } from "react-intl";
import { messages } from "lang/DefineMessages";

export const CandidateRecommend = () => {
  const intl = useIntl();

  let PROC_CHK = "";

  const {
    state: { server, mbr },
    dispatch,
  } = useContext(Context);

  const location = useLocation();
  const navigate = useNavigate();
  const moment = require("moment");
  let previewWindow = "";
  let previewImg = document.createElement("img");

  const [files, setFiles] = useState("");
  const [axiosFilesRes, setAxiosFilesRes] = useState("");

  const [mbrFilesRes, setMbrFilesRes] = useState([]);
  const [bizFilesRes, setBizFilesRes] = useState([]);
  const [extraFilesRes, setExtraFilesRes] = useState([]);

  const bizKndCd = defVal.BizKndCd();
  const salesSizeCd = defVal.SalesSizeCd();
  const empSizeCd = defVal.EmpSizeCd();
  const hireSizeCd = defVal.HireSizeCd();
  const celpFCd = defVal.CelpFCd();

  const [fileName, setFileName] = useState("");

  const [mbrCnt, setMbrCnt] = useState({});
  const [bizCate, setBizCate] = useState([]);
  const [empAddrCate, setEmpAddrCate] = useState([]);
  const [workPosCate, setWorkPosCate] = useState([]);

  const [invalid, setInvalid] = useState(true);

  const [recmmdState, setRecmmdState] = useState(
    defVal.setAxiosCnddtRcmmdState(null)
  );
  const [detailState, setDetailState] = useState(defVal.setAxiosJobState(null));

  const [modalVisible1, setModalVisible1] = useState(false);
  const [titleLength, setTitleLength] = useState("0");
  const [untilDone, setUntilDone] = useState(false);
  const [tagCnt, setTagCnt] = useState(0);

  const [bizCateCdCombo, setBizCateCdCombo] = useState([
    defVal.SelCateCdCombo(),
  ]);
  const [empAddrCateCdCombo, setEmpAddrCateCdCombo] = useState([
    defVal.SelCateCdCombo(),
  ]);
  const [workPosCateCdCombo, setWorkPosCateCdCombo] = useState([
    defVal.SelCateCdCombo(),
  ]);

  let atchFiles;

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid"),
    ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi"),
  };

  let locationState = {
    hireUid: location.state.hireUid,
    rcmndUid: location.state.rcmndUid ? location.state.rcmndUid : "",
  };

  useEffect(async () => {
    window.scrollTo(0, 0);
    initSetHandle();
  }, []);

  async function initSetHandle() {
    setRecmmdState(defVal.setAxiosCnddtRcmmdState(null));

    detailState.ssnMbrUid = ssn.ssnMbrUid;

    let goUrl = server.path + "/hire/hiredetail";
    let data = {
      ssnMbrUid: ssn.ssnMbrUid,
      ssnMbrDivi: ssn.ssnMbrDivi,
      hireUid: locationState.hireUid,
    };
    setFileName("");

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    let tagstr = axiosRes.data.tags;
    var tags = defVal.SplitToArray(tagstr, "<;:>");

    let atchFilesStr = axiosRes.data.atchFiles;
    if (atchFilesStr != "") {
      atchFiles = JSON.parse(atchFilesStr);
      for (var k = 0; k < atchFiles.length; k++) {
        if (atchFiles[k].filePos == 1)
          axiosRes.data.cnddtThmnlNm = atchFiles[k].orignlFileNm;
        else if (atchFiles[k].filePos == 2)
          axiosRes.data.thmnlNm2 = atchFiles[k].orignlFileNm;
        else if (atchFiles[k].filePos == 3)
          axiosRes.data.resumeFormThmnlNm = atchFiles[k].orignlFileNm;
      }
    }

    axiosRes.data.tags = tags;
    setDetailState(axiosRes.data);

    var tmpMbrCnt = await getMbrCnt();
    var tmpRcmmd;
    if (locationState.rcmndUid != "") {
      tmpRcmmd = await getCnddtRcmmnd();
    }

    getBizCateList();
    getEmpAddrCateList();
    getWorkPosCateList();
  }

  const getMbrCnt = async () => {
    let goUrl = server.path + "/mbr/mbrcnt";
    let data = {
      hireUid: locationState.hireUid,
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    let tmpDate = axiosRes.data.mbrcnt;
    setMbrCnt(tmpDate);
    return tmpDate;
  };

  const getCnddtRcmmnd = async () => {
    let goUrl = server.path + "/hire/cnddtrecmmnd";
    let data = {
      hireUid: locationState.hireUid,
      rcmndUid: locationState.rcmndUid,
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    let tmpDate = defVal.setAxiosCnddtRcmmdState(axiosRes.data.resInfo);

    var tmpCelp = tmpDate.cnddtCelpNum.split("-");
    try {
      tmpDate.cnddtCelpF = tmpCelp[0];
      tmpDate.cnddtCelpM = tmpCelp[1];
      tmpDate.cnddtCelpL = tmpCelp[2];
    } catch (e) {}

    let atchFilesStr = tmpDate.cnddtAtchFiles;
    if (atchFilesStr != "") {
      var atchFiles = JSON.parse(atchFilesStr);
      for (var k = 0; k < atchFiles.length; k++) {
        if (atchFiles[k].filePos == 1) {
          tmpDate.cnddtThmnlNm = atchFiles[k].orignlFileNm;
        }
        tmpDate.cnddtAtchFilesStr[atchFiles[k].filePos] = JSON.stringify(
          atchFiles[k]
        );
      }
    }

    setRecmmdState(tmpDate);
    return tmpDate;
  };

  const getBizCateList = async () => {
    let goUrl = server.path + "/hire/bizcate";
    let data = {
      hireUid: locationState.hireUid,
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    setBizCate(axiosRes.data.bizCateArr);
  };

  const getWorkPosCateList = async () => {
    let goUrl = server.path + "/hire/workposcate";
    let data = {
      hireUid: locationState.hireUid,
    };

    let axiosCateRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    setWorkPosCate(axiosCateRes.data.workPosCateArr);
  };

  const getEmpAddrCateList = async () => {
    let goUrl = server.path + "/hire/empaddrcate";
    let data = {
      hireUid: locationState.hireUid,
    };

    let axiosCateRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    setEmpAddrCate(axiosCateRes.data.empAddrCateArr);
  };

  const reRender = () => {
    setDetailState({
      ...detailState,
      initSetCmplt: true,
    });
  };

  const reRenderRecmmd = () => {
    setRecmmdState({
      ...recmmdState,
      initSetCmplt: true,
    });
  };

  ////////////////////////////////////////////////////////////////////////////////

  const handleDownload = (idx, filePath, fileNm) => {
    if (filePath != "") {
      let goUrl = server.host + filePath;
      axios
        .get(goUrl, {
          responseType: "blob",
        })
        .then((res) => {
          fileDownload(res.data, fileNm);
        });
    } else {
      fileDownload(detailState.uploadFiles[idx], fileNm);
    }
  };

  const handlePreview = (idx, filePath) => {
    previewWindow = window.open(
      "",
      "",
      "width=600,height=400,left=200,top=200"
    );

    if (filePath != "") {
      let goUrl = server.host + filePath;
      previewImg.src = goUrl;
    } else {
      let file = detailState.uploadFiles[idx];
      let reader = new FileReader();

      reader.onload = (function (file) {
        return function (e) {
          previewImg.src = e.target.result;
        };
      })(file);

      reader.readAsDataURL(file);
    }

    previewWindow.document.body.appendChild(previewImg);
  };

  //////////////////////////////////////////////////////////////////////////////////////////////////

  const jsonCompResult = (resJson) => {
    console.log("PROC_CHK : " + PROC_CHK);

    if (resJson.resCd !== "0000") {
      toast.error(
        intl.formatMessage({
          id: "error.during.save",
          defaultMessage: "저장 중 오류가 발생했습니다.",
        })
      );
      return;
    } else {
      if (PROC_CHK === "INS") {
        if (resJson.rcmndUid == "DUPL") {
          toast.error(
            intl.formatMessage({
              id: "error.already.recommended",
              defaultMessage: "이미 추천된 후보자입니다.",
            })
          );
          PROC_CHK = "";
          return;
        }
        PROC_CHK = "";

        toast.info(
          intl.formatMessage({
            id: "info.recommend.success",
            defaultMessage: "추천 되었습니다.",
          })
        );
        initSetHandle();
      }
      if (PROC_CHK === "DUPL") {
        if (resJson.rcmndUid == "DUPL")
          toast.error(
            intl.formatMessage({
              id: "error.already.recommended",
              defaultMessage: "이미 추천된 후보자입니다.",
            })
          );
        else
          toast.info(
            intl.formatMessage({
              id: "info.recommendable.candidate",
              defaultMessage: "추천 가능한 후보자입니다.",
            })
          );
        PROC_CHK = "";
      } else if (PROC_CHK === "FILEUPLOAD") {
        let jsonStr = JSON.stringify(resJson.resFileList);
        let fileList = JSON.parse(jsonStr);
        let filePos = 0;

        for (let k = 0; k < fileList.length; k++) {
          filePos = fileList[k].filePos;
          recmmdState.cnddtAtchFilesStr[filePos] = JSON.stringify(fileList[k]);
        }

        cntntsCRUD();
      }
    }
  };

  const goCRUD = async () => {
    if (
      commonUtil.CheckIsEmptyFromVal(
        recmmdState.cnddtNm,
        intl.formatMessage({
          id: "error.need.input.candidate.name",
          defaultMessage: "후보자 이름을 입력해주세요.",
        })
      ) === false
    )
      return;
    if (
      commonUtil.CheckIsEmptyFromVal(
        recmmdState.cnddtCelpL,
        intl.formatMessage({
          id: "error.need.input.phone",
          defaultMessage: "휴대폰번호를 입력해주세요.",
        })
      ) === false
    )
      return;
    if (
      commonUtil.CheckIsEmptyFromVal(
        recmmdState.cnddtThmnlNm,
        intl.formatMessage({
          id: "error.need.input.resume",
          defaultMessage: "이력서를 첨부해주세요.",
        })
      ) === false
    )
      return;
    if (
      commonUtil.CheckIsEmptyFromVal(
        recmmdState.cnddtIntro,
        intl.formatMessage({
          id: "error.need.input.recommendation",
          defaultMessage: "추천서를 입력해주세요.",
        })
      ) === false
    )
      return;
    if (
      commonUtil.CheckIsLengthFromVal(
        recmmdState.cnddtIntro,
        intl.formatMessage(messages.minCharacter, {
          maxLength: 500,
        }),
        500
      ) === false
    )
      return;

    let fileStorePath = "Globals.fileStoreHirePath"; //로컬저장경로
    let fileLinkPath = "Globals.fileLinkHirePath"; //link경로
    let filePrefix = "hire"; //파일코드

    PROC_CHK = "FILEUPLOAD";

    let uploadRes = await snglFileUpload(
      server.path,
      fileStorePath,
      fileLinkPath,
      filePrefix,
      recmmdState.uploadFiles,
      recmmdState.filePosNm
    );

    if (uploadRes === 0) cntntsCRUD();
    else jsonCompResult(uploadRes.data);
  };
  const cntntsCRUD = async () => {
    PROC_CHK = "INS";

    recmmdState.mbrUid = ssn.ssnMbrUid;
    recmmdState.hireUid = locationState.hireUid;
    recmmdState.rcmndUid = locationState.rcmndUid;

    let goUrl = server.path + "/hire/cnddtrecmmndinsert";
    if (locationState.rcmndUid !== "")
      goUrl = server.path + "/hire/cnddtrecmmndupdate";
    let axiosRes = await axiosCrudOnSubmit(recmmdState, goUrl, "POST");

    jsonCompResult(axiosRes.data);
  };

  const chkDupl = async () => {
    PROC_CHK = "DUPL";

    if (
      commonUtil.CheckIsEmptyFromVal(
        recmmdState.cnddtNm,
        intl.formatMessage({
          id: "error.need.input.candidate.name",
          defaultMessage: "후보자 이름을 입력해주세요.",
        })
      ) === false
    )
      return;
    if (
      commonUtil.CheckIsEmptyFromVal(
        recmmdState.cnddtCelpL,
        intl.formatMessage({
          id: "error.need.input.phone",
          defaultMessage: "휴대폰번호를 입력해주세요.",
        })
      ) === false
    )
      return;
    if (
      commonUtil.CheckIsEmptyFromVal(
        recmmdState.cnddtCelpL,
        intl.formatMessage({
          id: "error.need.input.phone",
          defaultMessage: "휴대폰번호를 입력해주세요.",
        })
      ) === false
    )
      return;

    recmmdState.mbrUid = ssn.ssnMbrUid;
    recmmdState.hireUid = locationState.hireUid;

    let goUrl = server.path + "/hire/cnddtrecmmnddupl";
    let axiosRes = await axiosCrudOnSubmit(recmmdState, goUrl, "POST");

    jsonCompResult(axiosRes.data);
  };

  return (
    <main className="recruitersMain">
      <section className="recruitLists">
        <div className="ingList">
          <h2 className="sectionTitle">
            <span>
              {detailState.hireStat === "end"
                ? `${intl.formatMessage({
                    id: "status.done",
                    defaultMessage: "종료된",
                  })}`
                : detailState.hireStat === "ing" &&
                  detailState.recEndYn !== "Y" &&
                  detailState.myCmmntCnt === 0
                ? `${intl.formatMessage({
                    id: "status.partners",
                    defaultMessage: "거래 고객사의",
                  })}`
                : detailState.hireStat === "ing" &&
                  detailState.recEndYn !== "Y" &&
                  detailState.myCmmntCnt > 0 &&
                  detailState.myCmmntChooseCnt === 0
                ? `${intl.formatMessage({
                    id: "status.suggested",
                    defaultMessage: "제안한",
                  })}`
                : detailState.hireStat === "ing"
                ? `${intl.formatMessage({
                    id: "status.suggested",
                    defaultMessage: "제안한",
                  })}`
                : `${intl.formatMessage({
                    id: "status.suggested",
                    defaultMessage: "제안한",
                  })}`}
              &nbsp;
              <FormattedMessage id="posting" defaultMessage="채용공고" />{" "}
              <em>
                <FormattedMessage
                  id="candidate.manage"
                  defaultMessage="후보자 관리"
                />
              </em>
            </span>
          </h2>
          <div className="noRecruitList" style={{ display: "none" }}>
            <FormattedMessage
              id="description.no.ing.posting"
              defaultMessage="진행중인 채용공고가 없습니다."
            />
          </div>
          <ul>
            <li>
              <div className="listLeft">
                <h3>
                  <Link
                    to="/recruiters/ing-detail"
                    state={{
                      hireUid: detailState.hireUid,
                      mbrUid: detailState.mbrUid,
                    }}
                  >
                    {detailState.title}
                  </Link>
                </h3>
                <p>
                  <FormattedMessage
                    id="registration.date"
                    defaultMessage="등록일"
                  />{" "}
                  : &nbsp;{moment(detailState.rgstDttm).format("YYYY-MM-DD")}
                  &nbsp; /{" "}
                  <FormattedMessage
                    id="recruitment.deadline"
                    defaultMessage="인재추천 마감일"
                  />{" "}
                  :{" "}
                  {detailState.rcmndUntilDon === "Y"
                    ? `${intl.formatMessage({
                        id: "until.hired",
                        defaultMessage: "채용시까지",
                      })}`
                    : commonUtil.substr(detailState.rcmndEndDttm, 0, 19)}
                  &nbsp; /{" "}
                  <strong>
                    <FormattedMessage
                      id="recruitment.people.number"
                      defaultMessage="모집인원"
                    />{" "}
                    : {detailState.hireSize}
                    <FormattedMessage id="people.count" defaultMessage="명" />
                  </strong>
                </p>
                <ul>
                  <li>
                    <FormattedMessage
                      id="suggestion.service.fee"
                      defaultMessage="서비스 수수료율"
                    />
                    : &nbsp;
                    {detailState.srvcFeeFixYn === "Y"
                      ? `${intl.formatMessage({
                          id: "suggestion.fixed.fee.system",
                          defaultMessage: "정액제",
                        })}`
                      : detailState.srvcFeePctMin +
                        "% ~ " +
                        detailState.srvcFeePctMax +
                        "%"}
                  </li>
                  <li>
                    <FormattedMessage
                      id="suggestion.service.guarantee"
                      defaultMessage="서비스 보증조건"
                    />{" "}
                    : &nbsp;
                    {detailState.srvcWrtyPeriod}
                    <FormattedMessage id="months" defaultMessage="개월" />{" "}
                    <FormattedMessage
                      id="suggestion.incase.retire"
                      defaultMessage="이내 퇴직시"
                    />
                    {detailState.srvcWrtyMthd === "prorated"
                      ? `${intl.formatMessage({
                          id: "suggestion.caculated.refund",
                          defaultMessage: "일할계산 환불",
                        })}`
                      : detailState.srvcWrtyMthd === "all"
                      ? `${intl.formatMessage({
                          id: "suggestion.all.refund",
                        })}`
                      : detailState.srvcWrtyMthd === "etc"
                      ? `${intl.formatMessage({
                          id: "etc",
                          defaultMessage: "기타",
                        })}`
                      : ""}
                  </li>
                </ul>
              </div>
              <div className="listRight">
                <button
                  type="button"
                  className="recommendDone"
                  style={{ display: "none" }}
                >
                  <FormattedMessage
                    id="candidate.recommend.finished"
                    defaultMessage="후보자 추천마감"
                  />
                </button>
                <div className="threeSections">
                  <div>
                    <span>
                      <FormattedMessage
                        id="recruiter.suggested"
                        defaultMessage="제안한 리크루터"
                      />
                    </span>
                    <em>
                      {mbrCnt.totCnt}
                      <FormattedMessage id="people.count" defaultMessage="명" />
                    </em>
                  </div>
                  <div>
                    <span>
                      <FormattedMessage
                        id="recruiter.selected"
                        defaultMessage="선택한 리크루터"
                      />
                    </span>
                    <em>
                      <strong>{detailState.cmmntChooseCnt}</strong>/
                      {detailState.recTcnt}
                      <FormattedMessage id="people.count" defaultMessage="명" />
                    </em>
                  </div>
                  <div>
                    <span>
                      <FormattedMessage
                        id="candidate.recommended"
                        defaultMessage="추천된 후보자"
                      />
                    </span>
                    <Link
                      to="/recruiters/candidate"
                      state={{
                        hireUid: detailState.hireUid,
                        mbrUid: detailState.mbrUid,
                      }}
                    >
                      {detailState.recmmndCnt}
                      <FormattedMessage id="people.count" defaultMessage="명" />
                    </Link>
                  </div>
                </div>
                <div className="qnaStatus">
                  <Link
                    to="/recruiters/qna"
                    state={{
                      hireUid: detailState.hireUid,
                      mbrUid: detailState.mbrUid,
                    }}
                  >
                    Q&amp;A{" "}
                    <span>
                      {detailState.qaCnt}
                      <FormattedMessage id="count" defaultMessage="개" />
                    </span>
                  </Link>
                </div>
              </div>
            </li>
          </ul>
        </div>
        <div className="recommendCandidate">
          <form className="recruitersForm">
            <div className="formWrap">
              <h3 className="formTitle">
                <span>
                  <FormattedMessage
                    id="candidate.recommend"
                    defaultMessage="후보자 추천하기"
                  />
                </span>
              </h3>
              <ul>
                <li>
                  <FormattedMessage
                    id="candidate.recommend.description1"
                    defaultMessage="※ 후보자의 연락처는 담당 리크루터의 동의없이 인사담당자에게 공개되지 않습니다."
                  />
                </li>
                <li>
                  <FormattedMessage
                    id="candidate.recommend.description2"
                    defaultMessage="※ 부정확한 정보를 입력해서 동일 후보자 중복 추천 문제가 발생하는 경우에는, 정확한 정보를 입력하신 담당 리크루터에게 우선권이 있습니다."
                  />
                </li>
                <li>
                  <FormattedMessage
                    id="candidate.recommend.description3"
                    values={{
                      breakLine: <br />,
                    }}
                    defaultMessage="※ 후보자의 동의없이 이력정보를 고객사(채용예정사)에 추천하는 행위는 개인정보보호법에 위반되며 당사는 이에 대해 법률적 책임을 지지 않습니다.<br />
                  동의없이 추천된 행위는 무효로 간주합니다."
                  />
                </li>
              </ul>
              <div className="formWrapInputs flexAlignCenter">
                <ul>
                  <li>
                    <label htmlFor="cadidateName">
                      <FormattedMessage
                        id="candidate.name"
                        defaultMessage="후보자 이름"
                      />
                    </label>
                    <input
                      type="text"
                      id="cnddtNm"
                      name="cnddtNm"
                      value={recmmdState.cnddtNm}
                      onChange={(e) => {
                        setRecmmdState({
                          ...recmmdState,
                          cnddtNm: e.target.value,
                        });
                      }}
                    />
                  </li>
                  <li>
                    <span className="labelSpan">
                      <FormattedMessage
                        id="duplicate.check"
                        defaultMessage="중복체크"
                      />
                    </span>
                    <button
                      type="button"
                      className="basicButton"
                      onClick={() => {
                        chkDupl();
                      }}
                    >
                      <FormattedMessage
                        id="candidate.duplicate.check"
                        defaultMessage="후보자 중복체크"
                      />{" "}
                      &gt;
                    </button>
                  </li>
                </ul>
                <div className="inputRight">
                  <span className="labelSpan">
                    <FormattedMessage id="phone" defaultMessage="휴대폰" />
                  </span>
                  <div className="phoneInput">
                    <div>
                      <input
                        type="radio"
                        id="phone"
                        name="number"
                        value="Y"
                        checked={
                          recmmdState.cnddtCelpNumPubYn === "Y" ? true : false
                        }
                        onClick={(e) => {
                          setRecmmdState({
                            ...recmmdState,
                            cnddtCelpNumPubYn: "Y",
                          });
                        }}
                      />
                      <label htmlFor="phone">
                        <FormattedMessage id="number" defaultMessage="번호" />
                      </label>
                      <select
                        value={recmmdState.CelpFCd}
                        onChange={(e) => {
                          setRecmmdState({
                            ...recmmdState,
                            cnddtCelpF: e.target.value,
                          });
                        }}
                      >
                        {celpFCd.map((type, idx) => (
                          <option value={type.cateCd}>{type.cdName}</option>
                        ))}
                      </select>
                      -{" "}
                      <input
                        type="text"
                        value={recmmdState.cnddtCelpM}
                        maxLength="4"
                        onChange={(e) => {
                          setRecmmdState({
                            ...recmmdState,
                            cnddtCelpM: e.target.value,
                          });
                        }}
                      />
                      -{" "}
                      <input
                        type="text"
                        value={recmmdState.cnddtCelpL}
                        maxLength="4"
                        onChange={(e) => {
                          setRecmmdState({
                            ...recmmdState,
                            cnddtCelpL: e.target.value,
                          });
                        }}
                      />
                    </div>
                    <div>
                      <input
                        type="radio"
                        id="backNumber"
                        name="number"
                        value="N"
                        checked={
                          recmmdState.cnddtCelpNumPubYn === "N" ? true : false
                        }
                        onClick={(e) => {
                          setRecmmdState({
                            ...recmmdState,
                            cnddtCelpNumPubYn: "N",
                          });
                        }}
                      />
                      <label htmlFor="backNumber">
                        <FormattedMessage
                          id="last.number"
                          defaultMessage="뒷자리"
                        />
                      </label>
                      <input
                        type="text"
                        value="***"
                        disabled
                        className="shortInput"
                      />{" "}
                      - <input type="text" value="****" disabled />-{" "}
                      <input
                        type="text"
                        value={recmmdState.cnddtCelpL}
                        maxLength="4"
                        onChange={(e) => {
                          setRecmmdState({
                            ...recmmdState,
                            cnddtCelpL: e.target.value,
                          });
                        }}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="formWrap">
              <h3 className="formTitle">
                <span>
                  <FormattedMessage
                    id="file.or.resume.attach"
                    defaultMessage="이력서 및 기타 파일 첨부"
                  />
                </span>
              </h3>
              <ul>
                <li>
                  <FormattedMessage
                    id="file.upload.guideline1"
                    defaultMessage="※ 파일은 1개만 업로드 가능합니다."
                  />
                </li>
                <li>
                  <FormattedMessage
                    id="file.upload.guideline2"
                    defaultMessage="※ 2개 이상의 파일을 업로드하고 싶다면, 압축 파일(.zip) 형태로1개의 파일만 업로드해 주세요."
                  />
                </li>
              </ul>
              <div className="formWrapInputs">
                <ul>
                  <li>
                    <span className="labelSpan">
                      <FormattedMessage
                        id="file.attachment"
                        defaultMessage="파일 첨부"
                      />
                    </span>
                    <div className="fileBox">
                      <label htmlFor="fileInput1" className="basicButton">
                        <FormattedMessage
                          id="attach"
                          defaultMessage="첨부하기"
                        />{" "}
                        &gt;
                      </label>
                      <span className="fileName">
                        {fileName}

                        {(recmmdState.cnddtNm === "" ||
                          recmmdState.cnddtCelpL === "") && (
                          <strong className="red">
                            {" "}
                            <FormattedMessage
                              id="candidate.name.phone.first"
                              defaultMessage="후보자의 이름과 휴대폰 번호를 먼저 입력해주세요."
                            />
                          </strong>
                        )}
                        {/* {recmmdState.cnddtThmnlNm} */}
                      </span>

                      <input
                        type="file"
                        id="fileInput1"
                        className="fileInput"
                        disabled={
                          recmmdState.cnddtNm === "" ||
                          recmmdState.cnddtCelpL === ""
                        }
                        onChange={(e) => {
                          let tmpList = recmmdState.uploadFiles;
                          tmpList[1] = e.target.files[0];

                          let tmpJsonStrList = recmmdState.snglImgFileJsonStr;
                          tmpJsonStrList[1] = "";

                          // e.target.files[0].name
                          setFileName(e.target.files[0].name);

                          setRecmmdState({
                            ...recmmdState,
                            uploadFiles: tmpList,
                            snglImgFileJsonStr: tmpJsonStrList,
                            cnddtThmnl: "",
                            cnddtThmnlNm:
                              detailState.title +
                              "_" +
                              recmmdState.cnddtNm +
                              "&" +
                              recmmdState.cnddtCelpL +
                              "_" +
                              intl.formatMessage({
                                id: "resume",
                                defaultMessage: "resume",
                              }),
                          });
                        }}
                      />
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            <div className="formWrap">
              <h3 className="formTitle">
                <span>
                  <FormattedMessage
                    id="recommendation.writing"
                    defaultMessage="추천서 작성"
                  />
                  <em>
                    (500
                    <FormattedMessage id="less.than" defaultMessage="자 내" />)
                  </em>
                </span>
              </h3>
              <div className="formWrapInputs">
                <div>
                  <textarea
                    name
                    id
                    cols={30}
                    rows={10}
                    value={recmmdState.cnddtIntro}
                    onChange={(e) => {
                      setRecmmdState({
                        ...recmmdState,
                        cnddtIntro: e.target.value,
                      });
                    }}
                  />
                </div>
              </div>
            </div>
            <div className="flexAlignCenter marginTopBottom">
              <input
                type="button"
                value={`${intl.formatMessage({
                  id: "candidate.recommend",
                  defaultMessage: "후보자 추천하기",
                })} >`}
                className="bigButton blueColored"
                onClick={() => {
                  goCRUD();
                }}
              />
            </div>
          </form>
        </div>
      </section>
    </main>
  );
};
