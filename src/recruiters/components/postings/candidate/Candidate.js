import { RecruitResult } from "commons/components/select/RecruitResult";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { useContext, useEffect, useState } from "react";
import { Context } from "contexts";
import { toast, ToastContainer } from "react-toastify";
import axios from "axios";
import fileDownload from "js-file-download";
import Modal from "modal-all/ModalAll";

import { axiosCrudOnSubmit } from "commons/modules/commonUtil";
import * as commonUtil from "commons/modules/commonUtil";
import { snglFileUpload } from "commons/modules/multipartUtil";
import * as defVal from "commons/modules/defVal";
import { AddressInput } from "commons/components/AddressInput";
import { ImagePreview } from "commons/components/inputs/image-preview/ImagePreview";
import { TagTemplate } from "commons/components/tags/TagTemplate";
import BusinessTypeTemplate from "commons/components/inputs/business-type/BusinessTypeTemplate";
import WorkPlaceTemplate from "commons/components/inputs/work-place/WorkPlaceTemplate";
import WorkPositionTemplate from "commons/components/inputs/work-position/WorkPositionTemplate";
import { UPD_MBRUID } from "contexts/actionTypes";
import { FormattedMessage, useIntl } from "react-intl";

export const Candidate = () => {
  const intl = useIntl();

  let PROC_CHK = "";

  const {
    state: { server, mbr },
    dispatch,
  } = useContext(Context);

  const location = useLocation();
  const navigate = useNavigate();
  const moment = require("moment");
  let previewWindow = "";
  let previewImg = document.createElement("img");

  const [files, setFiles] = useState("");
  const [axiosFilesRes, setAxiosFilesRes] = useState("");

  const [mbrFilesRes, setMbrFilesRes] = useState([]);
  const [bizFilesRes, setBizFilesRes] = useState([]);
  const [extraFilesRes, setExtraFilesRes] = useState([]);

  const bizKndCd = defVal.BizKndCd();
  const salesSizeCd = defVal.SalesSizeCd();
  const empSizeCd = defVal.EmpSizeCd();
  const hireSizeCd = defVal.HireSizeCd();
  const celpFCd = defVal.CelpFCd();
  const candidateStateCd = defVal.CandidateStateCd();

  const [mbrCnt, setMbrCnt] = useState({});
  const [bizCate, setBizCate] = useState([]);
  const [empAddrCate, setEmpAddrCate] = useState([]);
  const [workPosCate, setWorkPosCate] = useState([]);

  const [recmmdState, setRecmmdState] = useState(
    defVal.setAxiosCnddtRcmmdState(null)
  );
  const [detailState, setDetailState] = useState(defVal.setAxiosJobState(null));
  const [qaListState, setQaListState] = useState([]);
  const [hireCnddtRcmmdList, setHireCnddtRcmmdList] = useState([]);

  const [modalVisible1, setModalVisible1] = useState(false);
  const [titleLength, setTitleLength] = useState("0");
  const [untilDone, setUntilDone] = useState(false);
  const [tagCnt, setTagCnt] = useState(0);

  const [bizCateCdCombo, setBizCateCdCombo] = useState([
    defVal.SelCateCdCombo(),
  ]);
  const [empAddrCateCdCombo, setEmpAddrCateCdCombo] = useState([
    defVal.SelCateCdCombo(),
  ]);
  const [workPosCateCdCombo, setWorkPosCateCdCombo] = useState([
    defVal.SelCateCdCombo(),
  ]);

  let atchFiles;

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid"),
    ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi"),
  };

  let locationState = {
    hireUid: location.state.hireUid,
    mbrUid: location.state.mbrUid,
    backUrl: location.state.backUrl ? location.state.backUrl : "",
  };

  useEffect(async () => {
    console.log("Page First Load EFFECT");
    window.scrollTo(0, 0);
    initSetHandle();
  }, []);

  async function initSetHandle() {
    //detailState.ssnMbrUid = ssn.ssnMbrUid;

    let goUrl = server.path + "/hire/hiredetail";
    let data = {
      ssnMbrUid: ssn.ssnMbrUid,
      ssnMbrDivi: ssn.ssnMbrDivi,
      hireUid: locationState.hireUid,
      sRecMbrUid: ssn.ssnMbrUid,
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    setDetailState(axiosRes.data);

    var tmpMbrCnt = await getMbrCnt();
    var tmpHireRcmmdList = await getHireCnddtRcmmdList();
  }

  const getMbrCnt = async () => {
    let goUrl = server.path + "/mbr/mbrcnt";
    let data = {
      hireUid: locationState.hireUid,
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    let tmpDate = axiosRes.data.mbrcnt;
    setMbrCnt(tmpDate);
    return tmpDate;
  };

  const getHireCnddtRcmmdList = async () => {
    let goUrl = server.path + "/hire/cnddtrecmmndlist";
    var sConts = "";
    try {
      sConts = document.getElementById("sConts").value;
    } catch (e) {}

    let data = {
      hireUid: locationState.hireUid,
      sConts: sConts,
      sRecmmndMbrUid: ssn.ssnMbrUid,
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    let tmpArr = [];
    for (var k = 0; k < axiosRes.data.resArr.length; k++) {
      var tmpData = defVal.setAxiosCnddtRcmmdState(axiosRes.data.resArr[k]);
      tmpArr.push(tmpData);
    }

    setHireCnddtRcmmdList(tmpArr);
    return tmpArr;
  };

  const reRender = () => {
    setDetailState({
      ...detailState,
      initSetCmplt: true,
    });
  };

  ////////////////////////////////////////////////////////////////////////////////

  const handleDownload = (idx, filePath, fileNm) => {
    if (filePath == "") {
      toast.error(
        intl.formatMessage({
          id: "error.no.attached.file",
          defaultMessage: "첨부된 파일이 없습니다.",
        })
      );
      return;
    }

    var orignlFileNm = fileNm;

    try {
      let fileListJson = JSON.parse(fileNm);
      orignlFileNm = fileListJson[0].orignlFileNm;
    } catch (e) {}

    if (filePath != "") {
      let goUrl = server.host + filePath;
      axios
        .get(goUrl, {
          responseType: "blob",
        })
        .then((res) => {
          fileDownload(res.data, orignlFileNm);
        });
    } else {
      fileDownload(detailState.uploadFiles[idx], orignlFileNm);
    }
  };

  const handlePreview = (idx, filePath) => {
    previewWindow = window.open(
      "",
      "",
      "width=600,height=400,left=200,top=200"
    );

    if (filePath != "") {
      let goUrl = server.host + filePath;
      previewImg.src = goUrl;
    } else {
      let file = detailState.uploadFiles[idx];
      let reader = new FileReader();

      reader.onload = (function (file) {
        return function (e) {
          previewImg.src = e.target.result;
        };
      })(file);

      reader.readAsDataURL(file);
    }

    previewWindow.document.body.appendChild(previewImg);
  };

  function onKeyDownForm(keyEvent) {
    if ((keyEvent.charCode || keyEvent.keyCode) === 13) {
      keyEvent.preventDefault();
    }
  }

  const onKeyUp = (e) => {
    if (e.key == "Enter") {
      getHireCnddtRcmmdList();
    }
    return false;
  };

  const chkeckAll = (e) => {
    var chkYn = "N";
    if (document.getElementById("selectAll").checked == true) {
      chkYn = "Y";
    }

    for (var k = 0; k < hireCnddtRcmmdList.length; k++) {
      hireCnddtRcmmdList[k].chkYn = chkYn;
    }

    reRender();
  };

  const downloadAll = (e) => {
    var chkCnt = 0;
    for (var k = 0; k < hireCnddtRcmmdList.length; k++) {
      if (hireCnddtRcmmdList[k].chkYn == "Y") {
        var filePath = hireCnddtRcmmdList[k].cnddtThmnl;
        var fileNm = hireCnddtRcmmdList[k].cnddtAtchFiles;
        if (filePath != "") handleDownload(k, filePath, fileNm);
        chkCnt++;
      }
    }

    if (chkCnt == 0)
      toast.error(
        intl.formatMessage({
          id: "error.select.candidate.download",
          defaultMessage: "다운로드할 후보자를 선택해 주세요.",
        })
      );
  };

  //////////////////////////////////////////////////////////////////////////////////////////////////

  const jsonCompResult = (resJson) => {
    console.log("PROC_CHK : " + PROC_CHK);

    if (resJson.resCd !== "0000") {
      toast.error(
        intl.formatMessage({
          id: "error.during.save",
          defaultMessage: "저장 중 오류가 발생했습니다.",
        })
      );
      return;
    } else {
      if (PROC_CHK === "SETSTATEUPD") {
        PROC_CHK = "";
        toast.info(
          intl.formatMessage({
            id: "info.recruit.step.changed",
            defaultMessage: "채용단계가 변경되었습니다.",
          })
        );
        initSetHandle();
      }
    }
  };

  const goStateCRUD = async (uid, seq) => {
    PROC_CHK = "SETSTATEUPD";
    var data = {
      mbrUid: locationState.mbrUid,
      rcmndUid: uid,
      rcmndStat: hireCnddtRcmmdList[seq].rcmndStat,
    };

    let goUrl = server.path + "/hire/cnddtrecmmndstatupdate";
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    jsonCompResult(axiosRes.data);
  };

  return (
    <main className="recruitersMain">
      <section className="recruitLists">
        <h3 className="sectionTitle">
          {detailState.hireStat === "end"
            ? `${intl.formatMessage({
                id: "status.done",
                defaultMessage: "종료된",
              })}`
            : detailState.hireStat === "ing" &&
              detailState.recEndYn !== "Y" &&
              detailState.myCmmntCnt === 0
            ? `${intl.formatMessage({
                id: "status.partners",
                defaultMessage: "거래 고객사의",
              })}`
            : detailState.hireStat === "ing" &&
              detailState.recEndYn !== "Y" &&
              detailState.myCmmntCnt > 0 &&
              detailState.myCmmntChooseCnt === 0
            ? `${intl.formatMessage({
                id: "status.suggested",
                defaultMessage: "제안한",
              })}`
            : detailState.hireStat === "ing"
            ? `${intl.formatMessage({
                id: "status.ing",
                defaultMessage: "진행중",
              })}`
            : `${intl.formatMessage({
                id: "status.ing",
                defaultMessage: "진행중",
              })}`}
          &nbsp;
          <FormattedMessage id="posting" defaultMessage="채용공고" />{" "}
          <em>
            <FormattedMessage
              id="candidate.manage"
              defaultMessage="후보자 관리"
            />
          </em>
        </h3>
        <div className="ingList">
          <ul>
            <li>
              <div className="listLeft">
                <h3>
                  <Link
                    to={
                      locationState.backUrl != ""
                        ? locationState.backUrl
                        : "/recruiters/ing-detail"
                    }
                    state={{
                      hireUid: detailState.hireUid,
                      mbrUid: detailState.mbrUid,
                    }}
                  >
                    [{detailState.pubYn == "Y" ? detailState.bizNm : "*****"}]{" "}
                    {detailState.title}
                  </Link>
                </h3>
                <p>
                  <FormattedMessage
                    id="registration.date"
                    defaultMessage="등록일"
                  />{" "}
                  : &nbsp;{moment(detailState.rgstDttm).format("YYYY-MM-DD")}
                  &nbsp; /{" "}
                  <FormattedMessage
                    id="recruitment.deadline"
                    defaultMessage="인재추천 마감일"
                  />{" "}
                  :{" "}
                  {detailState.rcmndUntilDon === "Y"
                    ? `${intl.formatMessage({
                        id: "until.hired",
                        defaultMessage: "채용시까지",
                      })}`
                    : commonUtil.substr(detailState.rcmndEndDttm, 0, 19)}
                  &nbsp; /{" "}
                  <strong>
                    <FormattedMessage
                      id="recruitment.people.number"
                      defaultMessage="모집인원"
                    />{" "}
                    : {detailState.hireSize}
                    <FormattedMessage id="people.count" defaultMessage="명" />
                  </strong>
                </p>
                <ul>
                  <li>
                    <FormattedMessage
                      id="suggestion.service.fee"
                      defaultMessage="서비스 수수료율"
                    />{" "}
                    : &nbsp;
                    {detailState.srvcFeeFixYn === "Y"
                      ? `${intl.formatMessage({
                          id: "suggestion.fixed.fee.system",
                          defaultMessage: "정액제",
                        })}`
                      : detailState.srvcFeePctMin +
                        "% ~ " +
                        detailState.srvcFeePctMax +
                        "%"}
                  </li>
                  <li>
                    <FormattedMessage
                      id="suggestion.service.guarantee"
                      defaultMessage="서비스 보증조건"
                    />{" "}
                    : &nbsp;
                    {detailState.srvcWrtyPeriod}
                    <FormattedMessage id="months" defaultMessage="개월" />{" "}
                    <FormattedMessage
                      id="suggestion.incase.retire"
                      defaultMessage="이내 퇴직시"
                    />
                    {detailState.srvcWrtyMthd === "prorated"
                      ? `${intl.formatMessage({
                          id: "suggestion.caculated.refund",
                          defaultMessage: "일할계산 환불",
                        })}`
                      : detailState.srvcWrtyMthd === "all"
                      ? `${intl.formatMessage({
                          id: "suggestion.all.refund",
                          defaultMessage: "100% 전액 환불",
                        })}`
                      : detailState.srvcWrtyMthd === "etc"
                      ? `${intl.formatMessage({
                          id: "etc",
                          defaultMessage: "기타",
                        })}`
                      : ""}
                  </li>
                </ul>
              </div>
              <div className="listRight">
                {detailState.hireStat === "end" ? (
                  ""
                ) : detailState.rcmndEndYn === "Y" ? (
                  <button type="button" className="recommendDone">
                    <FormattedMessage
                      id="candidate.recommend.finished"
                      defaultMessage="후보자 추천마감"
                    />
                  </button>
                ) : detailState.hireStat === "ing" &&
                  detailState.myCmmntChooseCnt > 0 &&
                  detailState.rcmndEndYn !== "Y" ? (
                  <Link
                    to="/recruiters/candidate-recommend"
                    className="recommendCandidate"
                    state={{
                      hireUid: detailState.hireUid,
                      mbrUid: detailState.mbrUid,
                    }}
                  >
                    <FormattedMessage
                      id="candidate.recommend"
                      defaultMessage="후보자 추천하기"
                    />{" "}
                    &gt;
                  </Link>
                ) : detailState.recEndYn === "Y" ? (
                  <div className="recommendDone">
                    <FormattedMessage
                      id="recruiter.finished"
                      defaultMessage="리크루터 마감"
                    />
                  </div>
                ) : /*거래고객사 채용공고*/
                detailState.hireStat === "ing" &&
                  detailState.recEndYn !== "Y" &&
                  detailState.myCmmntCnt === 0 ? (
                  ""
                ) : /*제안진행중 채용공고*/
                detailState.hireStat === "ing" &&
                  detailState.recEndYn !== "Y" &&
                  detailState.myCmmntCnt > 0 &&
                  detailState.myCmmntChooseCnt === 0 ? (
                  <div className="status">
                    <FormattedMessage
                      id="suggestion.ing"
                      defaultMessage="제안 진행중"
                    />
                  </div>
                ) : (
                  ""
                )}

                <div className="threeSections">
                  <div>
                    <span>
                      <FormattedMessage
                        id="recruiter.suggested"
                        defaultMessage="제안한 리크루터"
                      />
                    </span>
                    <em>
                      {detailState.cmmntCnt}
                      <FormattedMessage id="people.count" defaultMessage="명" />
                    </em>
                  </div>
                  <div>
                    <span>
                      <FormattedMessage
                        id="recruiter.selected"
                        defaultMessage="선택한 리크루터"
                      />
                    </span>
                    <em>
                      <strong>{detailState.cmmntChooseCnt}</strong>/
                      {detailState.recSize}
                      <FormattedMessage id="people.count" defaultMessage="명" />
                    </em>
                  </div>
                  <div>
                    <span>
                      <FormattedMessage
                        id="candidate.recommended"
                        defaultMessage="추천된 후보자"
                      />
                    </span>
                    <Link
                      to="/recruiters/candidate"
                      state={{
                        hireUid: detailState.hireUid,
                        mbrUid: detailState.mbrUid,
                      }}
                    >
                      {detailState.recmmndCnt}
                      <FormattedMessage id="people.count" defaultMessage="명" />
                      ({detailState.chkingRecmmndCnt}
                      <FormattedMessage id="people.count" defaultMessage="명" />
                      /{detailState.nopassRecmmndCnt}
                      <FormattedMessage id="people.count" defaultMessage="명" />
                      )
                    </Link>
                  </div>
                </div>
                <div className="qnaStatus">
                  <Link
                    to="/recruiters/qna"
                    state={{
                      hireUid: detailState.hireUid,
                      mbrUid: detailState.mbrUid,
                    }}
                  >
                    Q&amp;A{" "}
                    <span>
                      {detailState.qaCnt}
                      <FormattedMessage id="count" defaultMessage="개" />
                    </span>
                  </Link>
                </div>
              </div>
            </li>
          </ul>
        </div>
        <div className="candidateList">
          <div className="flexSpaceBetween">
            <div className="flexAlignCenter">
              <input
                type="checkbox"
                id="selectAll"
                name="selectAll"
                className="marginInline"
                value="N"
                onClick={(e) => {
                  chkeckAll(e);
                }}
              />
              <label htmlFor="selectAll" name="checkAll" id="checkAll">
                <FormattedMessage id="select.all" defaultMessage="전체선택" />
              </label>
            </div>
            <div>
              <button
                type="button"
                className="blueButton"
                onClick={(e) => {
                  downloadAll(e);
                }}
              >
                <FormattedMessage
                  id="selected.resume.download"
                  defaultMessage="선택한 이력서 일괄 다운로드"
                />{" "}
                &gt;
              </button>
            </div>
          </div>

          <form
            className="flex employersForm marginTopBottom"
            onKeyDown={onKeyDownForm}
          >
            <input
              type="search"
              name="sConts"
              id="sConts"
              placeholder={intl.formatMessage({
                id: "placeholder.candidate.name",
                defaultMessage: "후보자 이름을 입력하세요.",
              })}
              onKeyUp={(e) => {
                onKeyUp(e);
              }}
            />
            <input
              type="button"
              value={intl.formatMessage({
                id: "search",
                defaultMessage: "검색",
              })}
              className="basicButton"
              onClick={(e) => {
                getHireCnddtRcmmdList();
              }}
            />
          </form>

          <ul className="recruiters">
            {hireCnddtRcmmdList.map((list, idx) => (
              <li>
                <div className="candidateInfo">
                  <div>
                    <span className="candidateName">
                      <input
                        type="checkbox"
                        checked={list.chkYn === "Y" ? true : false}
                      />
                      {list.cnddtNm}
                    </span>
                    <span>
                      <FormattedMessage id="contact" defaultMessage="연락처" />
                      &nbsp; | &nbsp;
                      {list.cnddtCelpNumPubYn == "Y"
                        ? list.cnddtCelpNum
                        : "비공개"}
                    </span>
                    {list.cnddtCelpNumPubReq &&
                    list.cnddtCelpNumPubYn !== "Y" ? (
                      <button type="button" className="basicButton">
                        <FormattedMessage
                          id="candidate.contact.open.suggest"
                          defaultMessage="후보자 연락처 공개요청 받음"
                        />
                      </button>
                    ) : (
                      ""
                    )}
                  </div>
                  <p>
                    <time>
                      <FormattedMessage
                        id="recommend.date"
                        defaultMessage="추천일시"
                      />{" "}
                      : {moment(list.rgstDttm).format("YYYY-MM-DD")}
                    </time>
                  </p>
                  <p style={{ whiteSpace: "pre-wrap" }}>{list.cnddtIntro}</p>
                </div>
                <div className="notification">
                  <button
                    type="button"
                    className="basicButton"
                    onClick={() => {
                      handleDownload(idx, list.cnddtThmnl, list.cnddtAtchFiles);
                    }}
                  >
                    <FormattedMessage
                      id="resume.download"
                      defaultMessage="이력서 다운로드"
                    />{" "}
                    &gt;
                  </button>
                  <form className="recruitersForm">
                    <select className="recruitResult" value={list.rcmndStat}>
                      <option value="01">
                        ::{" "}
                        {intl.formatMessage({
                          id: "recruit.step",
                          defaultMessage: "채용단계",
                        })}{" "}
                        ::
                      </option>
                      {candidateStateCd.map((list, idx) =>
                        idx > 0 ? (
                          <option value={list.cateCd} className={list.clssName}>
                            {list.cdName}
                          </option>
                        ) : (
                          ""
                        )
                      )}
                    </select>
                  </form>
                  <div className="recruiterMemo">
                    <Link
                      to="/recruiters/candidate-recommend"
                      state={{
                        hireUid: list.hireUid,
                        mbrUid: list.mbrUid,
                        rcmndUid: list.rcmndUid,
                      }}
                    >
                      <FormattedMessage id="edit" defaultMessage="수정하기" />
                    </Link>
                  </div>
                </div>
              </li>
            ))}
          </ul>
        </div>
      </section>
    </main>
  );
};
