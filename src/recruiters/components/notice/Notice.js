import { axiosCrudOnSubmit } from "commons/modules/commonUtil";
import { Context } from "contexts";
import { useContext, useEffect, useState } from "react";
import { FormattedMessage, useIntl } from "react-intl";
import { NoticeItem } from "./NoticeItem";

export const Notice = () => {

  let tmpIdx = 1;

  const moment = require('moment');
  const intl = useIntl();
  
  const [notis, setNotis] = useState([]);
  const [buttonList, setButtonList] = useState({
    allViewBtn : "active",
  });

  const {
    state : {
        mbr,
        server
    },
    dispatch,
  } = useContext(Context);

  const getNotiList = async (data) => {
    let goUrl = server.path + "/noti/list"
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
   
    setNotis(axiosRes.data.notiArr);
  }

  const clickMenu = ( menu ) => {
    setButtonList({
      [menu] : "active"
    });
  }

  useEffect( async () => {
    window.scrollTo(0,0);

    let data = {
      ssnMbrUid : mbr.mbrUid
      ,sPubKnd : "rec"
    }

    getNotiList(data);
  },[])

  useEffect(()=>{
    console.log(buttonList);
  }, [buttonList])

  return (
    <main className='recruitersMain'>
      <section>
        <h4 className="sectionTitle">
          <FormattedMessage
            id="mypage"
            defaultMessage="마이페이지"
          />
        </h4>
        <div className="noticeWrap">
          <ul className="noticeTabButton">
          <li 
            className={(buttonList.allViewBtn === "active")? "active" : undefined}
            onClick={()=>{clickMenu("allViewBtn");}}
          >
            <FormattedMessage
              id="mypage"
              defaultMessage="마이페이지"
            />
          </li>
          <li className={(buttonList.serviceBtn === "active")? "active" : undefined}
            onClick={()=>{
              clickMenu("serviceBtn");
            }}>
              <FormattedMessage
                id="notice.using.service"
                defaultMessage="서비스 이용"
              />
          </li>
          <li className={(buttonList.etcBtn === "active")? "active" : undefined}
            onClick={()=>{
              clickMenu("etcBtn");
            }}>
              <FormattedMessage
                id="etc"
                defaultMessage="기타"
              />
          </li>
          </ul>
          <div className="noticeTabContent">
            <ul>
              {notis.map((noti, idx) => {
                  if(buttonList.allViewBtn === "active")
                  {
                    return <NoticeItem  
                      idx={idx + 1}
                      key={noti.brdUid} 
                      id={noti.brdUid} 
                      date={ moment(noti.rgstDttm).format("YYYY년 MM월 DD일 HH시 mm분") }
                      title={noti.title} 
                      content={noti.cntnt}
                    />
                  } else if(buttonList.serviceBtn === "active")
                  {
                    if(noti.cateCd === "02")
                    {
                      return <NoticeItem  
                        idx={tmpIdx++} 
                        key={noti.brdUid} 
                        id={noti.brdUid}
                        date={ moment(noti.rgstDttm).format("YYYY년 MM월 DD일 HH시 mm분") }
                        title={noti.title} 
                        content={noti.cntnt}
                      />
                    } 
                  } else if(buttonList.etcBtn === "active")
                  {
                    if(noti.cateCd === "01" || noti.cateCd === "03" || noti.cateCd === "04")
                    {
                      return <NoticeItem  
                        idx={tmpIdx++} 
                        key={noti.brdUid} 
                        id={noti.brdUid}
                        date={ moment(noti.rgstDttm).format("YYYY년 MM월 DD일 HH시 mm분") }
                        title={noti.title} 
                        content={noti.cntnt}
                      />
                    }
                  }
                }
              )}
            </ul>
          </div>
        </div>
      </section>
    </main>
  );
}