import { Route, Routes } from "react-router-dom";

import "./components/scss/section.scss";
import "./components/scss/form.scss";
import "./components/scss/recruiter-list.scss";
import "./components/scss/search.scss";

// 공통 - 헤더, 메뉴, 푸터
import { Header } from "./components/common/Header";
import { Aside } from "./components/common/Aside";
import { Footer } from "./components/common/Footer";

// 메인 페이지
import { Main } from "./components/Main";

// 서브페이지
import { Suggest } from "./components/postings/suggest/Suggest";
import { Done } from "./components/postings/Done/Done";
import { Ing } from "./components/postings/Ing/Ing";
import { Notice } from "./components/notice/Notice";
import { Payment } from "./components/payment/Payment";
import { Mypage } from "./components/mypage/Mypage";
import { JobPosting } from "./components/job-posting/JobPosting";

// 상세 페이지
import { SuggestDetail } from "./components/postings/suggest/SuggestDetail";
import { IngDetail } from "./components/postings/Ing/IngDetail";
import { DoneDetail } from "./components/postings/Done/DoneDetail";
import { Candidate } from "./components/postings/candidate/Candidate";
import { CandidateRecommend } from "./components/postings/candidate/CandidateRecommend";
import { Qna } from "./components/postings/qna/Qna";
import { PartnerDetail } from "./components/postings/partner/PartnerDetail";
import { MypageEdit } from "./components/mypage/MypageEdit";
import { Search } from "./components/search/Search";
import { Partner } from "./components/postings/partner/Partner";

import { Link, useLocation, useNavigate } from "react-router-dom";
import { useContext, useEffect, useState } from "react";
import { Context } from "contexts";
import * as defVal from "commons/modules/defVal";
import * as commonUtil from "commons/modules/commonUtil";
import CandidateContact from "./components/common/CandidateContact";

function RecruitersIndex({ logout }) {
  let PROC_CHK = "";

  const {
    state: { server, mbr },
    dispatch,
  } = useContext(Context);

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid"),
    ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi"),
  };

  console.log("ssn == " + ssn.ssnMbrUid);

  let ssnMbrData = defVal.setAxiosSsnState(null);

  useEffect(async () => {
    console.log("Page First Load EFFECT");
    await initSetHandle();
  }, []);

  async function initSetHandle() {
    let goUrl = server.path + "/mbr/mbrdetail";
    let data = {
      ssnMbrUid: ssn.ssnMbrUid,
      ssnMbrDivi: ssn.ssnMbrDivi,
      mbrUid: ssn.ssnMbrUid,
    };

    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");
    ssnMbrData = defVal.setAxiosSsnState(axiosRes.data);

    console.log("ssn 1== " + ssnMbrData.mbrUid);
    console.log("ssn 1== " + ssnMbrData.mbrId);
    console.log("ssn 1== " + ssnMbrData.mbrNm);
    console.log("ssn 1== " + ssnMbrData.bizNm);
  }

  return (
    <>
      <Header logout={logout} />
      <div className="mainWrap">
        <Aside />

        <Routes>
          <Route path="/" element={<Main />} />

          <Route path="/search" element={<Search />} />

          {/* 서브 페이지 */}
          <Route path="/job-posting" element={<JobPosting />} />

          <Route path="/partner" element={<Partner />} />
          <Route path="/suggest" element={<Suggest />} />
          <Route path="/done" element={<Done />} />
          <Route path="/ing" element={<Ing />} />

          <Route path="/payment" element={<Payment />} />
          <Route path="/notice" element={<Notice />} />
          <Route path="/mypage" element={<Mypage />} />

          {/* 상세 페이지 */}
          <Route path="/partner-detail" element={<PartnerDetail />} />
          <Route path="/suggest-detail" element={<SuggestDetail />} />
          <Route path="/ing-detail" element={<IngDetail />} />
          <Route path="/done-detail" element={<DoneDetail />} />

          {/* 후보자 관련 페이지 */}
          <Route path="/candidate" element={<Candidate />} />
          <Route path="/candidate-recommend" element={<CandidateRecommend />} />
          <Route path="/candidate-contact" element={<CandidateContact />} />

          <Route path="/qna" element={<Qna />} />

          <Route path="/mypage-edit" element={<MypageEdit />} />
        </Routes>
      </div>
      <Footer />
    </>
  );
}

export default RecruitersIndex;
