import { FormattedMessage } from 'react-intl'
import payImg from '../images/pay_img.jpg'

export const Payment = () => {
  return (
    <main className="employersMain">
      <section>
        <h3 className="sectionTitle">
          <FormattedMessage
            id="payment.manage"
            defaultMessage="비용 관리"
          />
        </h3>
        <div className="paymentWrap">
          <div>
            <h5>
              <FormattedMessage
                id="payment.safe.system"
                defaultMessage="안전한 대금 결제 시스템"
              />
            </h5>
            <p>
              <FormattedMessage
                id="description.payment"
                defaultMessage="리크루팅 수수료는 위크루트 대금 결제 시스템을 통해 이루어집니다.후보자 최종 채용 확정 후 해당 수수료를 위크루트에 지불하시면 안전하게 보관 후 담당 리크루터에게 전달해드립니다."
              />
            </p>
            <h6>
              &lt;<FormattedMessage
                id="payment.related.question"
                defaultMessage="비용 관련 문의"
              />&gt;
            </h6>
            <ul>
              <li>031) 548-2310, 2311</li>
              <li>hunters@wecruitcorp.com</li>
            </ul>
          </div>
          <figure>
            <img src={payImg} alt="컴퓨터와 핸드폰 사진" />
          </figure>
        </div>
      </section>
    </main>
  )
}