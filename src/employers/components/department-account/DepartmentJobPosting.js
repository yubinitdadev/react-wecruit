import { FormattedMessage } from 'react-intl'
import payImg from '../images/pay_img.jpg'

export const DepartmentJobPosting = () => {
  return(
    <main className="employersMain">
      <section>
        <h3 className="sectionTitle">
          <FormattedMessage
            id="job.posting.manage"
            defaultMessage="채용공고 등록 관리"
          />
        </h3>
        <div className="paymentWrap">
          <div>
            <h5>
              <FormattedMessage
                id="job.posting.manage.paragraph1"
                defaultMessage="채용공고 등록은 인사담당자에게 문의 바랍니다."
              />
            </h5>
            <p>
              <FormattedMessage
                id="job.posting.manage.paragraph2"
                defaultMessage="채용공고 등록은 인사담당자만 할 수 있습니다."
              />
            </p>
            <h6>
              &lt;
              <FormattedMessage
                id="employer"
                defaultMessage="인사 담당자"
              />
              &gt; 정성미 님</h6>
            <ul>
              <li>
                <FormattedMessage
                  id="office"
                  defaultMessage="사무실"
                />:
              </li>
              <li>
                <FormattedMessage
                  id="phone"
                  defaultMessage="휴대폰"
                />: 01029355301</li>
              <li>smjung@wecruitcorp.com</li>
            </ul>
          </div>
          <figure>
            <img src={payImg} alt="컴퓨터와 핸드폰 사진" />
          </figure>
        </div>
      </section>
    </main>
  )
}