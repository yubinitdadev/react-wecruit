import profileImg from "../images/profile-sample.jpg";
import Modal from "modal-all/ModalAll";
import { AddSearchFirm } from "modal-all/modals/AddSearchFirm";
import { ClosingRecruit } from "modal-all/modals/ClosingRecruit";
import { StarRating } from "commons/components/star-rating/StarRating";
import membership from "commons/images/sign_i.png";

import { Link, useLocation, useNavigate } from "react-router-dom";
import { useContext, useEffect, useState } from "react";
import { Context } from "contexts";
import { toast, ToastContainer } from "react-toastify";
import axios from "axios";
import fileDownload from "js-file-download";

import { axiosCrudOnSubmit } from "commons/modules/commonUtil";
import * as commonUtil from "commons/modules/commonUtil";
import { snglFileUpload } from "commons/modules/multipartUtil";
import * as defVal from "commons/modules/defVal";
import { AddressInput } from "commons/components/AddressInput";
import { ImagePreview } from "commons/components/inputs/image-preview/ImagePreview";
import { TagTemplate } from "commons/components/tags/TagTemplate";
import BusinessTypeTemplate from "commons/components/inputs/business-type/BusinessTypeTemplate";
import WorkPlaceTemplate from "commons/components/inputs/work-place/WorkPlaceTemplate";
import WorkPositionTemplate from "commons/components/inputs/work-position/WorkPositionTemplate";
import { UPD_MBRUID } from "contexts/actionTypes";
import { FormattedMessage, useIntl } from "react-intl";

export const Recruiter = () => {
  const intl = useIntl();

  let PROC_CHK = "";

  const {
    state: { server, mbr },
    dispatch,
  } = useContext(Context);

  const location = useLocation();
  const navigate = useNavigate();
  const moment = require("moment");
  let previewWindow = "";
  let previewImg = document.createElement("img");
  const [showMore, setShowMore] = useState(false);

  const [files, setFiles] = useState("");
  const [axiosFilesRes, setAxiosFilesRes] = useState("");

  const [mbrFilesRes, setMbrFilesRes] = useState([]);
  const [bizFilesRes, setBizFilesRes] = useState([]);
  const [extraFilesRes, setExtraFilesRes] = useState([]);

  const bizKndCd = defVal.BizKndCd();
  const salesSizeCd = defVal.SalesSizeCd();
  const empSizeCd = defVal.EmpSizeCd();
  const hireSizeCd = defVal.HireSizeCd();
  const celpFCd = defVal.CelpFCd();

  const [mbrCnt, setMbrCnt] = useState({});
  const [bizCate, setBizCate] = useState([]);
  const [empAddrCate, setEmpAddrCate] = useState([]);
  const [workPosCate, setWorkPosCate] = useState([]);

  const [recmmdState, setRecmmdState] = useState(
    defVal.setAxiosCnddtRcmmdState(null)
  );

  const [detailState, setDetailState] = useState(defVal.setAxiosJobState(null));
  const [fltrState, setFltrState] = useState({});
  const [cmmntListState, setCmmntListState] = useState([]);
  const [qaCntnt, setQaCntnt] = useState(defVal.setAxiosQaCntntState());

  const [rcmmndEndModal, setRcmmndEndModal] = useState(false);
  const [hireDoneModal, setHireDoneModal] = useState(false);
  const [titleLength, setTitleLength] = useState("0");
  const [untilDone, setUntilDone] = useState(false);
  const [tagCnt, setTagCnt] = useState(0);

  const [bizCateCdCombo, setBizCateCdCombo] = useState([
    defVal.SelCateCdCombo(),
  ]);
  const [empAddrCateCdCombo, setEmpAddrCateCdCombo] = useState([
    defVal.SelCateCdCombo(),
  ]);
  const [workPosCateCdCombo, setWorkPosCateCdCombo] = useState([
    defVal.SelCateCdCombo(),
  ]);

  const [uid, setUid] = useState("");
  const [seq, setSeq] = useState("");
  const [cnddtNm, setCnddtNm] = useState("");

  let atchFiles;

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid"),
    ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi"),
  };

  let locationState = {
    hireUid: location.state.hireUid,
    mbrUid: location.state.mbrUid,
    backUrl: location.state.backUrl ? location.state.backUrl : "",
  };

  useEffect(async () => {
    console.log("Page First Load EFFECT");
    initSetHandle();
  }, []);

  const reRender = () => {
    setDetailState({
      ...detailState,
      initSetCmplt: true,
    });
  };

  async function initSetHandle() {
    //detailState.ssnMbrUid = ssn.ssnMbrUid;

    let goUrl = server.path + "/hire/hiredetail";
    let data = {
      serverPath: server.path,
      ssnMbrUid: ssn.ssnMbrUid,
      ssnMbrDivi: ssn.ssnMbrDivi,
      hireUid: locationState.hireUid,
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    setDetailState(axiosRes.data);

    var tmpMbrCnt = await defVal.getMbrCnt(data);
    setMbrCnt(tmpMbrCnt);

    var tmpCmmntList = await getCmmntList();
  }

  const getCmmntList = async () => {
    let goUrl = server.path + "/hire/cmmntList";

    let data = {
      hireUid: locationState.hireUid,
      sConts: document.getElementById("sConts")
        ? document.getElementById("sConts").value
        : "",
      sFltrCate: Object.keys(fltrState)[0],
      sFltrCont: Object.values(fltrState)[0],
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    var tmpDate = axiosRes.data.resArr;

    setCmmntListState(tmpDate);
    return tmpDate;
  };

  async function changeFltr(key, fltr) {
    let tmpFltr = fltr === "ASC" ? "DESC" : fltr === "DESC" ? "ASC" : "ASC";

    setFltrState({ [key]: tmpFltr });

    var tmpCmmntList = await getCmmntList();
  }

  const [recEndModal, setRecEndModal] = useState(false);
  const [qnaModal, setQnaModal] = useState(false);
  const [addSrchFirmModal, setAddSrchFirmModal] = useState(false);
  const [cmmntModal, setCmmntModal] = useState(false);

  const openRecEndModal = () => {
    setRecEndModal(true);
  };

  const closeRecEndModal = () => {
    setRecEndModal(false);
  };

  const openQnaModal = (uid, seq, nm) => {
    console.log(uid);
    setUid(uid);
    setCnddtNm(nm);
    setQnaModal(true);
  };

  const closeQnaModal = () => {
    setQnaModal(false);
  };

  const openAddSrchFirmModal = (uid) => {
    setUid(uid);
    setAddSrchFirmModal(true);
  };

  const closeAddSrchFirmModal = () => {
    setAddSrchFirmModal(false);
  };

  const openCmmntModal = (uid, nm) => {
    setUid(uid);
    setCnddtNm(nm);
    setCmmntModal(true);
  };

  const closeCmmntModal = () => {
    setCmmntModal(false);
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////

  function onKeyDown(keyEvent) {
    if ((keyEvent.charCode || keyEvent.keyCode) === 13) {
      keyEvent.preventDefault();
    }
  }

  const onKeyUpSrch = (e) => {
    if (e.key == "Enter") {
      getCmmntList();
    }
    return false;
  };

  const [textLength, setTextLength] = useState(0);
  const [qsCntnt, setQsCntnt] = useState("");
  const onChangeText = (e) => {
    setTextLength(e.target.value.length);
  };

  ////////////////////////////////////////////////////////////////////////////////////////////////

  const jsonCompResult = (resJson) => {
    console.log("PROC_CHK : " + PROC_CHK);

    if (resJson.resCd !== "0000") {
      toast.error(
        intl.formatMessage({
          id: "error.during.save",
          defaultMessage: "저장 중 오류가 발생했습니다.",
        })
      );
      return;
    } else {
      if (PROC_CHK === "INSQA") {
        PROC_CHK = "";
        initSetHandle();
      } else if (PROC_CHK === "UPDRECEND") {
        PROC_CHK = "";
        detailState.recEndYn = "end";
        initSetHandle();
      } else if (PROC_CHK === "UPDCMMNT") {
        PROC_CHK = "";
        initSetHandle();
      } else if (PROC_CHK === "ADDSRCHFIRM") {
        PROC_CHK = "";
        initSetHandle();
      } else if (PROC_CHK === "FILEUPLOAD") {
        let jsonStr = JSON.stringify(resJson.resFileList);
        let fileList = JSON.parse(jsonStr);
        let filePos = 0;

        for (let k = 0; k < fileList.length; k++) {
          filePos = fileList[k].filePos;
          recmmdState.cnddtAtchFilesStr[filePos] = JSON.stringify(fileList[k]);
        }

        //cntntsCRUD();
      }
    }
  };

  const goRecCRUD = async () => {
    PROC_CHK = "UPDRECEND";

    var data = {
      hireUid: locationState.hireUid,
      mbrUid: ssn.ssnMbrUid,
    };

    let goUrl = server.path + "/hire/hircmmtend";
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    jsonCompResult(axiosRes.data);

    closeRecEndModal();
  };

  const goCmmntStatCRUD = async () => {
    PROC_CHK = "UPDCMMNT";

    var data = {
      commentUid: uid,
      mbrUid: ssn.ssnMbrUid,
    };

    let goUrl = server.path + "/hire/cmmntchoose";
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    jsonCompResult(axiosRes.data);

    closeCmmntModal();
  };

  const goQaCRUD = async () => {
    var data = defVal.setAxiosQaCntntState(null);
    data.cntnt = document.getElementById("qsCntnt").value;
    data.mbrUid = ssn.ssnMbrUid;
    data.hireUid = locationState.hireUid;
    data.lvl = 1;
    data.refUid = "";
    data.rcvKnd = uid;

    console.log("data.mbrUid == " + data.mbrUid);
    console.log("data.rcvKnd == " + data.rcvKnd);
    //alert(data.cntnt);

    if (
      commonUtil.CheckIsEmptyFromVal(
        data.cntnt,
        intl.formatMessage({
          id: "error.need.input.content",
          defaultMessage: "내용을 입력해주세요.",
        })
      ) === false
    )
      return;

    document.getElementById("qsCntnt").value = "";

    PROC_CHK = "INSQA";

    let goUrl = server.path + "/qa/qainsert";
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    jsonCompResult(axiosRes.data);

    closeQnaModal();
  };

  const goAddSrchFirmCRUD = async () => {
    PROC_CHK = "ADDSRCHFIRM";

    var data = {
      mbrUid: locationState.mbrUid,
      sMbrUid: uid,
    };

    let goUrl = server.path + "/srchfirm/searchfirmadd";
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    jsonCompResult(axiosRes.data);

    closeAddSrchFirmModal();
  };

  return (
    <main className="employersMain">
      <section className="recruitLists">
        <div className="ingList">
          <h3 className="sectionTitle">
            <span>
              {detailState.hireStat === "ing"
                ? `${intl.formatMessage({
                    id: "status.ing",
                    defaultMessage: "진행중",
                  })}`
                : `${intl.formatMessage({
                    id: "status.done",
                    defaultMessage: "종료된",
                  })}`}
              &nbsp;
              <FormattedMessage id="recruitment" defaultMessage="채용공고" />
            </span>
            <em>
              <FormattedMessage
                id="recruiter.manage"
                defaultMessage="리쿠르터 관리"
              />
            </em>
          </h3>
          <div className="noRecruitList" style={{ display: "none" }}>
            <FormattedMessage
              id="description.no.ing.posting"
              defaultMessage="진행중인 채용공고가 없습니다."
            />
          </div>
          <ul>
            <li className="ingListColumn">
              <div className="listTop">
                <div className="listLeft">
                  <h3>
                    <Link
                      to={
                        locationState.backUrl != ""
                          ? locationState.backUrl
                          : "/employers/ing-detail"
                      }
                      state={{
                        hireUid: detailState.hireUid,
                        mbrUid: detailState.mbrUid,
                      }}
                    >
                      {detailState.title}
                    </Link>
                  </h3>
                  <p>
                    <FormattedMessage
                      id="registration.date"
                      defaultMessage="등록일"
                    />{" "}
                    : &nbsp;{moment(detailState.rgstDttm).format("YYYY-MM-DD")}
                    &nbsp; /{" "}
                    <FormattedMessage
                      id="recruitment.deadline"
                      defaultMessage="인재추천 마감일"
                    />{" "}
                    :{" "}
                    {detailState.rcmndUntilDon === "Y"
                      ? `${intl.formatMessage({
                          id: "until.hired",
                          defaultMessage: "채용시까지",
                        })}`
                      : commonUtil.substr(detailState.rcmndEndDttm, 0, 19)}
                    &nbsp; /{" "}
                    <strong>
                      <FormattedMessage
                        id="recruitment.people.number"
                        defaultMessage="모집인원"
                      />{" "}
                      : {detailState.hireSize}
                      <FormattedMessage id="people.count" defaultMessage="명" />
                    </strong>
                  </p>

                  <div>
                    {detailState.recEndYn === "Y" ? (
                      <button type="button" className="recruitingClosed">
                        <FormattedMessage
                          id="recruiter.recruit.finisehd"
                          defaultMessage="리쿠르터 모집마감"
                        />
                      </button>
                    ) : (
                      <button
                        type="button"
                        className="modalButtonStyling"
                        onClick={openRecEndModal}
                      >
                        <FormattedMessage
                          id="recruiter.recruit.finisehd"
                          defaultMessage="리쿠르터 모집마감"
                        />{" "}
                        &gt;
                      </button>
                    )}
                    {
                      <Modal
                        visible={recEndModal}
                        closable={true}
                        maskClosable={true}
                        onSubmit={goRecCRUD}
                        onClose={closeRecEndModal}
                        inputName={intl.formatMessage({
                          id: "yes",
                          defaultMessage: "예",
                        })}
                        closeName={intl.formatMessage({
                          id: "no",
                          defaultMessage: "아니오",
                        })}
                      >
                        <ClosingRecruit />
                      </Modal>
                    }
                  </div>
                </div>
                <div className="flexColumn listRight">
                  <div className="threeSections">
                    <div>
                      <span>
                        <FormattedMessage
                          id="recruiter.suggested"
                          defaultMessage="제안한 리크루터"
                        />
                      </span>
                      <Link
                        to="/employers/recruiter-manage"
                        state={{
                          hireUid: detailState.hireUid,
                          mbrUid: detailState.mbrUid,
                          backUrl: locationState.backUrl,
                        }}
                      >
                        {detailState.cmmntCnt}
                        <FormattedMessage
                          id="people.count"
                          defaultMessage="명"
                        />
                      </Link>
                    </div>
                    <div>
                      <span>
                        <FormattedMessage
                          id="recruiter.selected"
                          defaultMessage="선택한 리크루터"
                        />
                      </span>
                      <Link
                        to="/employers/recruiter-manage"
                        state={{
                          hireUid: detailState.hireUid,
                          mbrUid: detailState.mbrUid,
                          backUrl: locationState.backUrl,
                        }}
                      >
                        <strong>{detailState.cmmntChooseCnt}</strong>/
                        {detailState.recSize}
                        <FormattedMessage
                          id="people.count"
                          defaultMessage="명"
                        />
                      </Link>
                    </div>
                    <div>
                      <span>
                        <FormattedMessage
                          id="candidate.recommended"
                          defaultMessage="추천된 후보자"
                        />
                      </span>
                      <Link
                        to="/employers/candidate-manage"
                        state={{
                          hireUid: detailState.hireUid,
                          mbrUid: detailState.mbrUid,
                          backUrl: locationState.backUrl,
                        }}
                      >
                        {detailState.recmmndCnt}
                        <FormattedMessage
                          id="people.count"
                          defaultMessage="명"
                        />
                        ({detailState.chkingRecmmndCnt}
                        <FormattedMessage
                          id="people.count"
                          defaultMessage="명"
                        />
                        /{detailState.nopassRecmmndCnt}
                        <FormattedMessage
                          id="people.count"
                          defaultMessage="명"
                        />
                        )
                      </Link>
                    </div>
                  </div>
                  <div className="qnaStatus">
                    <Link
                      to="/employers/qna-manage"
                      state={{
                        hireUid: detailState.hireUid,
                        mbrUid: detailState.mbrUid,
                        backUrl: locationState.backUrl,
                      }}
                    >
                      Q&amp;A &nbsp;
                      <span>
                        {detailState.qaCnt}
                        <FormattedMessage id="count" defaultMessage="개" />
                      </span>
                    </Link>
                  </div>
                  <p>
                    <FormattedMessage
                      id="description.open.to.recruiter"
                      defaultMessage="* 채용진행 현황은 참여중인 리크루터에게도 공개됩니다."
                    />
                  </p>
                </div>
              </div>
            </li>
          </ul>
        </div>
        <div className="recruiterList">
          <h5>
            <strong>
              {cmmntListState.length}
              <FormattedMessage id="people.count" defaultMessage="명" />
            </strong>
            <FormattedMessage
              id="select.more.recruiter"
              defaultMessage="의 리크루터를 더 선택할 수 있습니다."
            />
          </h5>
          <div className="flexSpaceBetween">
            <form action className="flex employersForm" onKeyDown={onKeyDown}>
              <input
                type="search"
                name="sConts"
                id="sConts"
                placeholder={intl.formatMessage({
                  id: "placeholder.recruiter.name",
                  defaultMessage: "리크루터 이름을 입력하세요.",
                })}
                onKeyUp={(e) => {
                  onKeyUpSrch(e);
                }}
              />
              <input
                type="button"
                value={intl.formatMessage({
                  id: "search",
                  defaultMessage: "검색",
                })}
                className="basicButton"
                onClick={(e) => {
                  getCmmntList();
                }}
              />
            </form>

            <ul className="searchFilters">
              <li
                className={
                  fltrState.rgstDttm === "DESC"
                    ? "descending"
                    : fltrState.rgstDttm === "ASC"
                    ? "ascending"
                    : undefined
                }
                onClick={() => {
                  changeFltr("rgstDttm", fltrState.rgstDttm);
                }}
              >
                <span>
                  <FormattedMessage
                    id="suggest.date"
                    defaultMessage="제안일시"
                  />
                </span>
                <div className="filterArrow">
                  <span />
                  <span />
                </div>
              </li>
              <li
                className={
                  fltrState.replyPoint === "DESC"
                    ? "descending"
                    : fltrState.replyPoint === "ASC"
                    ? "ascending"
                    : undefined
                }
                onClick={() => {
                  changeFltr("replyPoint", fltrState.replyPoint);
                }}
              >
                <span>
                  <FormattedMessage
                    id="cumulative.rate"
                    defaultMessage="누적평점"
                  />
                </span>
                <div className="filterArrow">
                  <span />
                  <span />
                </div>
              </li>
              <li
                className={
                  fltrState.srvcFee === "DESC"
                    ? "descending"
                    : fltrState.srvcFee === "ASC"
                    ? "ascending"
                    : undefined
                }
                onClick={() => {
                  changeFltr("srvcFee", fltrState.srvcFee);
                }}
              >
                <span>
                  <FormattedMessage
                    id="commission.rate"
                    defaultMessage="수수료율"
                  />
                </span>
                <div className="filterArrow">
                  <span />
                  <span />
                </div>
              </li>
            </ul>
          </div>

          <ul className="recruiters">
            {cmmntListState.map((list, idx) => (
              <li>
                <figure>
                  <img src={list.thmnl} alt="프로필" />
                </figure>
                <div className="recruiterInfo">
                  <div>
                    <Link
                      to="/employers/recruiter-info"
                      state={{
                        hireUid: list.hireUid,
                        mbrUid: list.mbrUid,
                        backUrl: "/admin/recruiter-manage",
                      }}
                    >
                      <span className="recruiterName">
                        {list.mbrNm}
                        <em>({list.rcmndCnt})</em>
                      </span>
                    </Link>

                    <div className="stars" style={{ display: "none" }}>
                      {list.replyPoint >= 1 ? (
                        <span className="material-icons-outlined active">
                          grade
                        </span>
                      ) : (
                        <span className="material-icons-outlined ">grade</span>
                      )}
                      {list.replyPoint >= 2 ? (
                        <span className="material-icons-outlined active">
                          grade
                        </span>
                      ) : (
                        <span className="material-icons-outlined ">grade</span>
                      )}
                      {list.replyPoint >= 3 ? (
                        <span className="material-icons-outlined active">
                          grade
                        </span>
                      ) : (
                        <span className="material-icons-outlined ">grade</span>
                      )}
                      {list.replyPoint >= 4 ? (
                        <span className="material-icons-outlined active">
                          grade
                        </span>
                      ) : (
                        <span className="material-icons-outlined ">grade</span>
                      )}
                      {list.replyPoint >= 5 ? (
                        <span className="material-icons-outlined active">
                          grade
                        </span>
                      ) : (
                        <span className="material-icons-outlined ">grade</span>
                      )}
                      <span>&nbsp;&nbsp;{list.replyPoint} / 5.0</span>
                    </div>

                    <button
                      type="button"
                      className="buttonStyling"
                      onClick={(e) => {
                        openQnaModal(list.mbrUid, idx, list.mbrNm);
                      }}
                    >
                      [
                      <FormattedMessage
                        id="send.memo"
                        defaultMessage="쪽지보내기"
                      />
                      ]
                    </button>
                    <button
                      type="button"
                      className="modalOpenButton modalButtonStyling"
                      onClick={(e) => {
                        openAddSrchFirmModal(list.mbrUid);
                      }}
                    >
                      <FormattedMessage
                        id="search.firm.register"
                        defaultMessage="기존 거래 써치펌으로 등록하기"
                      />
                    </button>
                    {list.hmpg != "" ? (
                      <img
                        src={membership}
                        className="flag"
                        alt="i가 그려진 깃발"
                        style={{ cursor: "pointer" }}
                        onClick={() => window.open(list.hmpg, "_blank")}
                      />
                    ) : (
                      ""
                    )}
                  </div>
                  <p>
                    &lt;
                    {list.mbrPos !== "" ? (
                      list.mbrPos
                    ) : (
                      <FormattedMessage
                        id="freelancer"
                        defaultMessage="프리랜서"
                      />
                    )}
                    &gt;
                    <time>{commonUtil.substr(list.rgstDttm, 0, 10)}</time>
                  </p>

                  <p style={{ display: showMore ? "block" : "-webkit-box" }}>
                    {list.cntnt}
                  </p>

                  {showMore ? null : (
                    <span
                      onClick={() => setShowMore(!showMore)}
                      className="showMore"
                    >
                      <FormattedMessage
                        id="load.more"
                        defaultMessage="더보기"
                      />{" "}
                      &gt;
                    </span>
                  )}
                </div>

                <div className="selectRecruiter">
                  {ssn.ssnMbrDivi === "MBRDIVI-01" ? (
                    <>
                      {list.commentStat === "02" ? (
                        <button type="button" className="selectedRecruiter">
                          <FormattedMessage
                            id="recruiter.selected"
                            defaultMessage="선택한 리크루터"
                          />
                        </button>
                      ) : (
                        <button
                          type="button"
                          className="modalButtonStyling"
                          onClick={(e) => {
                            openCmmntModal(list.commentUid, list.mbrNm);
                          }}
                        >
                          <FormattedMessage
                            id="recruiter.select"
                            defaultMessage="리쿠르터 선택하기"
                          />{" "}
                          &gt;
                        </button>
                      )}
                    </>
                  ) : (
                    ""
                  )}

                  {/* 현업부서 아이디로 접속시 활성화되는 버튼 */}
                  {ssn.ssnMbrDivi === "MBRDIVI-03" ? (
                    <button type="button" className="departmentBlock">
                      <FormattedMessage
                        id="recruiter.select"
                        defaultMessage="리쿠르터 선택하기"
                      />{" "}
                      &gt;
                    </button>
                  ) : (
                    ""
                  )}

                  <ul>
                    <li>
                      <span>
                        <FormattedMessage
                          id="commission"
                          defaultMessage="수수료"
                        />
                      </span>
                      <strong>{list.srvcFee}%</strong>
                    </li>
                    <li>
                      <span>
                        <FormattedMessage
                          id="guarantee.span"
                          defaultMessage="보증기간"
                        />
                      </span>
                      <strong>
                        {list.srvcWrtyPeriod}
                        <FormattedMessage id="months" defaultMessage="개월" />
                      </strong>
                    </li>
                    <li>
                      <span>
                        <FormattedMessage
                          id="guarantee.method"
                          defaultMessage="보증방식"
                        />
                      </span>
                      <strong>
                        {list.srvcWrtyMthd === "prorated"
                          ? `${intl.formatMessage({
                              id: "suggestion.caculated.refund",
                              defaultMessage: "일할계산 환불",
                            })}`
                          : list.srvcWrtyMthd === "all"
                          ? `${intl.formatMessage({
                              id: "suggestion.all.refund",
                              defaultMessage: "100% 전액 환불",
                            })}`
                          : list.srvcWrtyMthd === "etc"
                          ? `${intl.formatMessage({
                              id: "etc",
                              defaultMessage: "기타",
                            })}`
                          : ""}
                      </strong>
                    </li>
                  </ul>
                </div>
              </li>
            ))}
          </ul>
        </div>
      </section>

      {
        <Modal
          visible={cmmntModal}
          closable={true}
          maskClosable={true}
          onSubmit={goCmmntStatCRUD}
          onClose={closeCmmntModal}
          inputName={intl.formatMessage({
            id: "yes",
            defaultMessage: "예",
          })}
          closeName={intl.formatMessage({
            id: "no",
            defaultMessage: "아니오",
          })}
        >
          <>
            <h4>
              <FormattedMessage
                id="recruiter.choose"
                defaultMessage="리크루터 선택하기"
              />
            </h4>
            <div className="alertBox">
              {cnddtNm}{" "}
              <FormattedMessage
                id="ask.contract.recruiter"
                values={{
                  breakLine: <br />,
                }}
                defaultMessage="리크루터와 <br/> 계약체결을 하시겠습니까?"
              />
            </div>
            <p className="textAlignCenter">
              <FormattedMessage
                id="description.recommend.available"
                values={{
                  breakLine: <br />,
                }}
                defaultMessage="계약이 체결되면 <br/> 바로 인재를 추천받으실 수 있습니다."
              />
            </p>
          </>
        </Modal>
      }

      {
        <Modal
          visible={qnaModal}
          closable={true}
          maskClosable={true}
          onSubmit={goQaCRUD}
          onClose={closeQnaModal}
          inputName={intl.formatMessage({
            id: "send.memo",
            defaultMessage: "쪽지보내기",
          })}
          closeName={intl.formatMessage({
            id: "no",
            defaultMessage: "아니오",
          })}
        >
          <>
            <h4>
              <FormattedMessage
                id="recruiter.send.memo"
                defaultMessage="리크루터에게 쪽지보내기"
              />
            </h4>
            <div className="alertBox">{cnddtNm}</div>
            <p className="marginTopBottom">
              <FormattedMessage
                id="description.only.reveal.to.recruiter"
                values={{
                  breakLine: <br />,
                }}
                defaultMessage="문의 내용은 Q&amp;A 게시판에 작성되며 {breakLine} 작성내용은 해당 리크루터만 확인 가능합니다."
              />
            </p>
            <form className="employersForm">
              <textarea
                name="qsCntnt"
                id="qsCntnt"
                cols={100}
                rows={10}
                maxLength="100"
                onChange={onChangeText}
              />
              <span className="flexAlignRight">{textLength}/100</span>
            </form>
          </>
        </Modal>
      }

      {
        <Modal
          visible={addSrchFirmModal}
          closable={true}
          maskClosable={true}
          onSubmit={goAddSrchFirmCRUD}
          onClose={closeAddSrchFirmModal}
          inputName={intl.formatMessage({
            id: "search.firm.register",
            defaultMessage: "기존 거래 써치펌으로 등록하기",
          })}
          closeName={intl.formatMessage({
            id: "close",
            defaultMessage: "닫기",
          })}
        >
          <AddSearchFirm />
        </Modal>
      }
    </main>
  );
};
