import { StarRating } from "commons/components/star-rating/StarRating";
import Modal from "modal-all/ModalAll";
import profileSampleImg from "../images/profile-sample.jpg";

import { Link, useLocation, useNavigate } from "react-router-dom";
import { useContext, useEffect, useState } from "react";
import { Context } from "contexts";
import { toast, ToastContainer } from "react-toastify";
import axios from "axios";
import fileDownload from "js-file-download";

import { snglFileUpload } from "commons/modules/multipartUtil";
import * as commonUtil from "commons/modules/commonUtil";
import * as defVal from "commons/modules/defVal";
import { AddressInput } from "commons/components/AddressInput";
import { TagTemplate } from "commons/components/tags/TagTemplate";
import EditorComponent from "admin/components/editor/EditorComponent";
import { ImagePreview } from "commons/components/inputs/image-preview/ImagePreview";
import { FormattedMessage, useIntl } from "react-intl";

export const RecruiterInfo = () => {
  const intl = useIntl();

  let PROC_CHK = "";

  const {
    state: { server, mbr },
    dispatch,
  } = useContext(Context);

  const location = useLocation();
  const navigate = useNavigate();
  const moment = require("moment");
  let previewWindow = "";
  let previewImg = document.createElement("img");

  const [detailState, setDetailState] = useState(defVal.setAxiosMbrState(null));
  const [hireDetailState, setHireDetailState] = useState({});
  const [cmmntDetailState, setCmmntDetailState] = useState({});
  const [replyListState, setReplyListState] = useState([]);

  const [uid, setUid] = useState("");

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid"),
    ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi"),
  };

  let locationState = {
    hireUid: location.state.hireUid,
    mbrUid: location.state.mbrUid,
    mbrDivi: "MBRDIVI-02",
  };

  useEffect(async () => {
    console.log("Page First Load EFFECT");
    initSetHandle();
  }, []);

  async function initSetHandle() {
    detailState.filePosNm[1] = "thmnlNm";
    detailState.filePosNm[2] = "extraThmnlNm";
    detailState.ssnMbrUid = ssn.ssnMbrUid;

    let goUrl = server.path + "/mbr/mbrdetail";
    let data = {
      ssnMbrUid: ssn.ssnMbrUid,
      ssnMbrDivi: ssn.ssnMbrDivi,
      mbrUid: locationState.mbrUid,
      mbrDivi: locationState.mbrDivi,
    };

    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");

    let tagstr = axiosRes.data.tags;
    var tags = defVal.SplitToArray(tagstr, "<;:>");
    axiosRes.data.tags = tags;

    setDetailState(axiosRes.data);

    var tmpCmmnt = await getCmmnt();
    var tmpHire = await getHire();
    var tmpReplyList = await getReplyList();
  }

  const getCmmnt = async () => {
    let goUrl = server.path + "/hire/cmmntdetail";
    let data = {
      ssnMbrUid: ssn.ssnMbrUid,
      ssnMbrDivi: ssn.ssnMbrDivi,
      hireUid: locationState.hireUid,
      mbrUid: locationState.mbrUid,
    };

    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");
    setCmmntDetailState(axiosRes.data.resInfo);
    return axiosRes.data.resInfo;
  };

  const getHire = async () => {
    let goUrl = server.path + "/hire/hiredetail";
    let data = {
      ssnMbrUid: ssn.ssnMbrUid,
      ssnMbrDivi: ssn.ssnMbrDivi,
      hireUid: locationState.hireUid,
    };

    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");
    setHireDetailState(axiosRes.data);
    return axiosRes.data;
  };

  const getReplyList = async () => {
    let goUrl = server.path + "/hire/replyList";
    let data = {
      mbrUid: locationState.mbrUid,
    };

    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");
    var tmpDate = axiosRes.data.resArr;

    setReplyListState(tmpDate);
    return tmpDate;
  };

  const OnErrorMbrImg = (id) => {
    document.getElementById(id).src = server.mbrNoImg;
  };

  ////////////////////////////////////////////////////////////////////////////////////////////////

  const [cmmntModal, setCmmntModal] = useState(false);

  const openCmmntModal = (uid) => {
    setUid(uid);
    setCmmntModal(true);
  };

  const closeCmmntModal = () => {
    setCmmntModal(false);
  };

  ////////////////////////////////////////////////////////////////////////////////////////////////

  const jsonCompResult = (resJson) => {
    console.log("PROC_CHK : " + PROC_CHK);

    if (resJson.resCd !== "0000") {
      toast.error(
        intl.formatMessage({
          id: "error.during.save",
          defaultMessage: "저장 중 오류가 발생했습니다.",
        })
      );
      return;
    } else {
      if (PROC_CHK === "UPDCMMNT") {
        PROC_CHK = "";
        initSetHandle();
      }
    }
  };

  const goCmmntStatCRUD = async () => {
    PROC_CHK = "UPDCMMNT";

    var data = {
      commentUid: uid,
    };

    let goUrl = server.path + "/hire/cmmntchoose";
    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");

    jsonCompResult(axiosRes.data);

    closeCmmntModal();
  };

  return (
    <main>
      <section className="adminCustom">
        <h2 className="sectionTitle">
          <span>
            {hireDetailState.hireStat === "ing"
              ? `${intl.formatMessage({
                  id: "status.ing",
                  defaultMessage: "진행중",
                })}`
              : `${intl.formatMessage({
                  id: "status.done",
                  defaultMessage: "종료된",
                })}`}
            &nbsp;
            <FormattedMessage id="recruitment" defaultMessage="채용공고" />
          </span>
          <em>
            <FormattedMessage
              id="recruiter.show.detail"
              defaultMessage="리크루터 자세히보기"
            />
          </em>
        </h2>
        <div className="adminRecruiterInfo">
          <div className="userInfo">
            <div className="userInfoLeft">
              <figure>
                <img
                  name="recThmnl"
                  id="recThmnl"
                  src={detailState.thmnl}
                  onError={(e) => OnErrorMbrImg("recThmnl")}
                  alt="프로필"
                />
              </figure>
              <div>
                <h3>{detailState.mbrNm}</h3>

                <div className="stars" style={{ display: "none" }}>
                  {detailState.replyPoint >= 1 ? (
                    <span className="material-icons-outlined active">
                      grade
                    </span>
                  ) : (
                    <span className="material-icons-outlined ">grade</span>
                  )}
                  {detailState.replyPoint >= 2 ? (
                    <span className="material-icons-outlined active">
                      grade
                    </span>
                  ) : (
                    <span className="material-icons-outlined ">grade</span>
                  )}
                  {detailState.replyPoint >= 3 ? (
                    <span className="material-icons-outlined active">
                      grade
                    </span>
                  ) : (
                    <span className="material-icons-outlined ">grade</span>
                  )}
                  {detailState.replyPoint >= 4 ? (
                    <span className="material-icons-outlined active">
                      grade
                    </span>
                  ) : (
                    <span className="material-icons-outlined ">grade</span>
                  )}
                  {detailState.replyPoint >= 5 ? (
                    <span className="material-icons-outlined active">
                      grade
                    </span>
                  ) : (
                    <span className="material-icons-outlined ">grade</span>
                  )}
                  <span>&nbsp;&nbsp;{detailState.replyPoint} / 5.0</span>
                </div>

                <p>
                  <span>{detailState.mbrPos}</span>
                  <span>{detailState.celpNum}</span>
                  <span>{detailState.email}</span>
                </p>
              </div>
            </div>
            <div className="selectRecruiter">
              {cmmntDetailState.commentStat === "02" ? (
                <button type="button" className="selectedRecruiter">
                  <FormattedMessage
                    id="recruiter.selected"
                    defaultMessage="선택한 리크루터"
                  />
                </button>
              ) : (
                <button
                  type="button"
                  className="modalButtonStyling"
                  onClick={(e) => {
                    openCmmntModal(cmmntDetailState.commentUid);
                  }}
                >
                  <FormattedMessage
                    id="recruiter.select"
                    defaultMessage="리크루터 선택하기"
                  />{" "}
                  &gt;
                </button>
              )}

              <ul>
                <li>
                  <span>
                    <FormattedMessage id="commission" defaultMessage="수수료" />
                  </span>{" "}
                  <strong>{cmmntDetailState.srvcFee}%</strong>
                </li>
                <li>
                  <span>
                    <FormattedMessage
                      id="guarantee.span"
                      defaultMessage="보증기간"
                    />
                  </span>
                  <strong>
                    {hireDetailState.srvcWrtyPeriod}
                    <FormattedMessage id="months" defaultMessage="개월" />
                  </strong>
                </li>
                <li>
                  <span>
                    <FormattedMessage
                      id="guarantee.method"
                      defaultMessage="보증방식"
                    />
                  </span>
                  <strong>
                    {hireDetailState.srvcWrtyMthd === "prorated"
                      ? `${intl.formatMessage({
                          id: "suggestion.caculated.refund",
                          defaultMessage: "일할계산 환불",
                        })}`
                      : hireDetailState.srvcWrtyMthd === "all"
                      ? `${intl.formatMessage({
                          id: "suggestion.all.refund",
                          defaultMessage: "100% 전액 환불",
                        })}`
                      : hireDetailState.srvcWrtyMthd === "etc"
                      ? `${intl.formatMessage({
                          id: "etc",
                          defaultMessage: "기타",
                        })}`
                      : ""}
                  </strong>
                </li>
              </ul>
            </div>
          </div>
          <div className="userActivity">
            <ul>
              <li>
                <FormattedMessage
                  id="project.ing"
                  defaultMessage="진행중 프로젝트"
                />
              </li>
              <li>{detailState.hireIngCnt}</li>
            </ul>
            <ul>
              <li>
                <FormattedMessage
                  id="recommend.success.rate"
                  defaultMessage="인재추천 성공율"
                />
              </li>
              <li>
                {detailState.recmmndScssRate}% &nbsp;
                <em>
                  ( {detailState.recmmndChooseCnt}
                  <FormattedMessage id="case" defaultMessage="건" /> /{" "}
                  {detailState.recmmndCnt}
                  <FormattedMessage id="case" defaultMessage="건" /> )
                </em>
              </li>
            </ul>
            <ul>
              <li>
                <FormattedMessage
                  id="document.success.rate"
                  defaultMessage="서류 성공율"
                />
              </li>
              <li>
                {detailState.recmmndPaperRate}% &nbsp;
                <em>
                  ( {detailState.recmmndPaperCnt}건 / {detailState.recmmndCnt}건
                  )
                </em>
              </li>
            </ul>
            <ul>
              <li>
                <FormattedMessage
                  id="interview.success.rate"
                  defaultMessage="면접 성공율"
                />
              </li>
              <li>
                {detailState.recmmndFaceRate}% &nbsp;
                <em>
                  ( {detailState.recmmndFaceCnt}건 / {detailState.recmmndCnt}건
                  )
                </em>
              </li>
            </ul>
            <ul>
              <li>
                <FormattedMessage
                  id="project.done"
                  defaultMessage="종료된 프로젝트"
                />
              </li>
              <li>{detailState.hireEndCnt}</li>
            </ul>
          </div>
          <div className="average userActivity">
            <ul>
              <li>
                <FormattedMessage
                  id="average.recommend.number.position"
                  defaultMessage="포지션별 평균 추천인원"
                />
              </li>
              <li>
                0%{" "}
                <em>
                  (0
                  <FormattedMessage id="case" defaultMessage="건" />
                  /0
                  <FormattedMessage id="case" defaultMessage="건" />)
                </em>
              </li>
            </ul>
            <ul>
              <li>
                <FormattedMessage
                  id="average.first.time.recommend"
                  defaultMessage="첫 후보자 추천까지 평균소요시간"
                />
              </li>
              <li>
                {detailState.recmmndFrstTmAvgCnt > 0
                  ? detailState.recmmndFrstTmAvgCnt +
                    "H (" +
                    detailState.recmmndFrstDayAvgCnt +
                    "D)"
                  : `${intl.formatMessage({
                      id: "no.data",
                      defaultMessage: "데이터 없음",
                    })}`}
              </li>
            </ul>
            <ul>
              <li>
                <FormattedMessage
                  id="average.time.recommend"
                  defaultMessage="후보자 추천시 평균소요시간"
                />
              </li>
              <li>
                {detailState.recmmndTmAvgCnt > 0
                  ? detailState.recmmndTmAvgCnt +
                    "H (" +
                    detailState.recmmndDayAvgCnt +
                    "D)"
                  : `${intl.formatMessage({
                      id: "no.data",
                      defaultMessage: "데이터 없음",
                    })}`}
              </li>
            </ul>
          </div>
          <div className="selfIntro">
            <h4>
              <FormattedMessage
                id="recruiter.self.intro"
                defaultMessage="리크루터 자기소개"
              />
            </h4>
            <div className="tags">
              <div className="tagList">
                {detailState.tags.map((text, idx) =>
                  text !== "" ? <span>{text}</span> : ""
                )}
              </div>
            </div>
            <div
              className="introductionText"
              dangerouslySetInnerHTML={{ __html: detailState.intro }}
            ></div>
          </div>
        </div>

        <div className="employerComment" style={{ display: "none" }}>
          <h3>
            <FormattedMessage
              id="employer.comment"
              defaultMessage="인사담당자 코멘트"
            />{" "}
            <em>
              (
              <FormattedMessage
                id="description.no.show.to.recruiter"
                defaultMessage="※ 인사담당자 코멘트 내용은 리크루터는 볼 수 없습니다."
              />
              )
            </em>
          </h3>
          <ul>
            {replyListState.map((list, idx) => (
              <li>
                <span>{list.mbrNm}</span>

                <div className="stars">
                  {list.replyPoint >= 1 ? (
                    <span className="material-icons-outlined active">
                      grade
                    </span>
                  ) : (
                    <span className="material-icons-outlined ">grade</span>
                  )}
                  {list.replyPoint >= 2 ? (
                    <span className="material-icons-outlined active">
                      grade
                    </span>
                  ) : (
                    <span className="material-icons-outlined ">grade</span>
                  )}
                  {list.replyPoint >= 3 ? (
                    <span className="material-icons-outlined active">
                      grade
                    </span>
                  ) : (
                    <span className="material-icons-outlined ">grade</span>
                  )}
                  {list.replyPoint >= 4 ? (
                    <span className="material-icons-outlined active">
                      grade
                    </span>
                  ) : (
                    <span className="material-icons-outlined ">grade</span>
                  )}
                  {list.replyPoint >= 5 ? (
                    <span className="material-icons-outlined active">
                      grade
                    </span>
                  ) : (
                    <span className="material-icons-outlined ">grade</span>
                  )}
                </div>

                <p>{list.cntnt}</p>
              </li>
            ))}
          </ul>
        </div>

        <div className="flexAlignCenter" style={{ marginTop: "20px" }}>
          <Link
            to="/employers/recruiter-manage"
            state={{
              hireUid: hireDetailState.hireUid,
              mbrUid: detailState.mbrUid,
            }}
            className="basicButton"
          >
            <FormattedMessage id="show.list" defaultMessage="리스트 보기" />{" "}
            &gt;
          </Link>
        </div>
      </section>

      {
        <Modal
          visible={cmmntModal}
          closable={true}
          maskClosable={true}
          onSubmit={goCmmntStatCRUD}
          onClose={closeCmmntModal}
          inputName={intl.formatMessage({
            id: "yes",
            defaultMessage: "예",
          })}
          closeName={intl.formatMessage({
            id: "no",
            defaultMessage: "아니오",
          })}
        >
          <>
            <h4>
              <FormattedMessage
                id="recruiter.select"
                defaultMessage="리크루터 선택하기"
              />
            </h4>
            <div className="alertBox">
              {detailState.mbrNm}
              <FormattedMessage
                id="ask.contract.recruiter"
                values={{
                  breakLine: <br />,
                }}
                defaultMessage="리크루터와 <br/> 계약체결을 하시겠습니까?"
              />
            </div>
            <p className="textAlignCenter">
              <FormattedMessage
                id="description.recommend.available"
                values={{
                  breakLine: <br />,
                }}
                defaultMessage="계약이 체결되면 <br/> 바로 인재를 추천받으실 수 있습니다."
              />
            </p>
          </>
        </Modal>
      }
    </main>
  );
};
