
import {Link, useLocation, useNavigate} from 'react-router-dom';
import { useCallback, useContext, useEffect, useState } from 'react';
import { Context } from 'contexts';
import { toast, ToastContainer } from "react-toastify";
import axios from 'axios';
import fileDownload from 'js-file-download';
import Modal from "modal-all/ModalAll";
import { LeaveWecruit } from "modal-all/modals/LeaveWecruit";
import { ChangePassword } from "modal-all/modals/ChangePassword";

import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { axiosCrudOnSubmit } from 'commons/modules/commonUtil';
import * as commonUtil from 'commons/modules/commonUtil';
import * as defVal  from 'commons/modules/defVal';
import { snglFileUpload } from 'commons/modules/multipartUtil';
import { AddressInput } from 'commons/components/AddressInput';
import { TagTemplate } from 'commons/components/tags/TagTemplate';
import EditorComponent from 'admin/components/editor/EditorComponent';
import { ImagePreview } from 'commons/components/inputs/image-preview/ImagePreview';
import { UPD_MBRUID } from "contexts/actionTypes";
import { FormattedMessage, useIntl } from 'react-intl';



export const Mypage =() => {
  const intl = useIntl();

  let PROC_CHK = "";

  const {
    state : {
        server,
        mbr
    },
    dispatch,
  } = useContext(Context);

  
  const location = useLocation();
  const navigate = useNavigate();
  const moment = require('moment');
  let previewWindow = "";
  let previewImg = document.createElement("img");  

  const [files, setFiles] = useState("");
  const [axiosFilesRes, setAxiosFilesRes] = useState("");
  
  const [mbrFilesRes, setMbrFilesRes] = useState([]);
  const [bizFilesRes, setBizFilesRes] = useState([]);
  const [extraFilesRes, setExtraFilesRes] = useState([]);

  const salesSizeCd = defVal.SalesSizeCd();
  const empSizeCd = defVal.EmpSizeCd();
  const bankCd = defVal.BankCd();
  
  const [bizCateCd, setBizCateCd] = useState([]);
  const [detailState, setDetailState] = useState(defVal.setAxiosMbrState(null));
  const [tagCnt, setTagCnt] = useState(0);

  const [wthdrModal, setWthdrModal] = useState(false);

  const openWthdrModal = () => {
    setWthdrModal(true)
  }
  const closeWthdrModal = () => {
    setWthdrModal(false)
  }

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid")
    ,ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi")
  };

  let locationState = {
    mbrUid : ssn.ssnMbrUid,
    mbrDivi : "MBRDIVI-01"
  }

  /*
  useEffect( () => {
    window.scrollTo(0,0);
  },[])
  */


  useEffect(async () => {
    console.log("Page First Load EFFECT");
    initSetHandle();          
  },[])

  const reRender = () => {
    setDetailState({
      ...detailState,
      initSetCmplt: true
    })      
  };

  const initSetHandle = async () => {
    detailState.filePosNm[1] = "bizCertThmnl";
    detailState.filePosNm[2] = "bizLogoThmnl";
    detailState.ssnMbrUid = ssn.ssnMbrUid;
    
    let goUrl = server.path + "/mbr/mbrdetail"
    let data = {
      ssnMbrUid : ssn.ssnMbrUid,
      ssnMbrDivi : ssn.ssnMbrDivi,
      mbrUid : locationState.mbrUid,
      mbrDivi : locationState.mbrDivi
    }
    
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");    
    var tmpDetail = defVal.setAxiosMbrState(axiosRes.data);

    setDetailState(tmpDetail);
    
    await getBizCateCdList();
    await getMbrFileList();
    await getBizFileList();   
    await getExtraFileList();   
  }

  const getBizCateCdList = async () => {
    let goUrl = server.path + "/hireBizCateCd";
    let data = {};    
    let axiosCateRes = await axiosCrudOnSubmit(data, goUrl, "POST");  
    setBizCateCd(axiosCateRes.data.hireCateArr);
  }

  const getMbrFileList = async () => {
    let goUrl = server.path + "/mbr/mbrfiles";
    let data = {mbrUid: locationState.mbrUid};
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    setMbrFilesRes( axiosRes.data );
  }

  useEffect(() => {
    if(mbrFilesRes && mbrFilesRes.length > 0){
      for(let i = 0; i < mbrFilesRes.length; i++)
      {
          console.log("files getMbrFileList "+i+" === " + mbrFilesRes[i].orignlFileNm); 
          detailState.thmnlNm = mbrFilesRes[i].orignlFileNm;
          detailState.mbrFileJsonStr[i+1] = JSON.stringify(mbrFilesRes[i])+"";          
      }
    }
  }, [mbrFilesRes])

  const getBizFileList = async () => {
    let goUrl = server.path + "/mbr/bizfiles";
    let data = {mbrUid: locationState.mbrUid};
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    setBizFilesRes( axiosRes.data );
  }

  useEffect(() => {
    if(bizFilesRes && bizFilesRes.length > 0){
      for(let i = 0; i < bizFilesRes.length; i++)
        {
          if( i == 0 ){
            detailState.bizCertThmnlNm = bizFilesRes[i].orignlFileNm;            
          }
          else 
          {
            detailState.bizLogoThmnlNm = bizFilesRes[i].orignlFileNm;
          }
          detailState.bizFileJsonStr[i+1] = JSON.stringify(bizFilesRes[i])+"";
        }
    }
  }, [bizFilesRes])

  const getExtraFileList = async () => {
    let goUrl = server.path + "/mbr/extrafiles/";
    let data = {mbrUid: locationState.mbrUid};
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    setExtraFilesRes( axiosRes.data );
  }

  //////////////////////////////////////////////////////////////////////////////////////////

  const handleDownload = (idx, filePath, fileNm) => {
    
    if(filePath == "" )
    {
      toast.error(intl.formatMessage({
        id: "error.no.attached.file",
        defaultMessage: "첨부된 파일이 없습니다."
      }));
      return;
    }

    var orignlFileNm = fileNm;
    
    try {
      let fileListJson = JSON.parse(fileNm);
      orignlFileNm = fileListJson[0].orignlFileNm;
    } catch (e){
    }    
    
    if(filePath != "" )
    {
       let goUrl = server.host + filePath; 
       axios.get(goUrl, {
        responseType: 'blob',
      })
      .then((res) => {
        fileDownload(res.data, orignlFileNm);
      })
    } else {
      fileDownload(detailState.uploadFiles[idx], orignlFileNm);
    }
   }
  
   const handlePreview = (idx, filePath) => {
  
     previewWindow = window.open("", "", "width=600,height=400,left=200,top=200");
    
     if(filePath != "")
     {
       let goUrl = server.host + filePath; 
       previewImg.src = goUrl;
     } 
     else
     {
       let file = detailState.uploadFiles[idx];
       let reader = new FileReader();
     
       reader.onload = ( function(file) {
         return function(e) {
           previewImg.src = e.target.result;
         };
       })(file);
       
       reader.readAsDataURL(file);
     } 
       
     previewWindow.document.body.appendChild(previewImg);
   }

  /////////////////////////////////////////////////////////////////////////////////////////

  const jsonCompResult = ( resJson ) => {
    if( resJson.resCd !== "0000" )
    {
      toast.error(intl.formatMessage({
        id: "error.during.connecting.sever",
        defaultMessage: "서버와 통신 중 오류가 발생했습니다."
      }));
      return;
    }
    else
    {
      if( PROC_CHK === "FILEUPLOAD" )
      {
        PROC_CHK = "";
        let jsonStr = JSON.stringify(resJson.resFileList);
        let fileList = JSON.parse(jsonStr);

        let tmpMbrFilePosNm = detailState.mbrFilePosNm;
        let tmpMbrList = detailState.mbrFileJsonStr;
        let tmpBizFilePosNm = detailState.bizFilePosNm;
        let tmpBizList = detailState.bizFileJsonStr;
        
        let pos = 0;
        for(var i=0; i<fileList.length; i++ )
        {
          for(let n=0; n<tmpBizFilePosNm.length; n++)
          {
            if( tmpBizFilePosNm[n] == fileList[i].filePosNm)
            {
              tmpBizList[n] = JSON.stringify( fileList[i] );
            }
          }
        }

        detailState.mbrFileJsonStr = tmpMbrList;
        detailState.extraFileJsonStr = tmpBizList;

        PROC_CHK = "UPD";
        cntntsCRUD();

        /*
        let jsonStr = JSON.stringify(resJson.resFileList);
        let fileList = JSON.parse(jsonStr);
        let fileListPos = 0;
        let textAreaCnt = detailState.snglImgFileJsonStr.length;
        let tmpList = [...detailState.snglImgFileJsonStr];

        for(let i = 0; i < textAreaCnt; i++)
        {
          if( detailState.snglImgFileJsonStr[i] === "" )
          {
            tmpList[i] = JSON.stringify( fileList[fileListPos] );

            if( tmpList[i] === "undefined" ) 
            {
              tmpList[i] = "";
            }
            fileListPos++;
          }
        }

        setDetailState({
          ...detailState,
          fileUploadCmplt: true,
          snglImgFileJsonStr: tmpList
        });
        */
      } 
      else if (PROC_CHK === "UPD")
      {
        PROC_CHK = "";
        window.scrollTo(0,0);
        toast.info(intl.formatMessage({
          id: "info.my.info.changed",
          defaultMessage: "내 정보 수정이 완료되었습니다."
        }));
        
        /*
        let data = {
          ssnMbrUid : (mbr.mbrUid !== "") ?  mbr.mbrUid : window.localStorage.getItem("ssnMbrUid"),
          mbrUid : (mbr.mbrUid !== "") ?  mbr.mbrUid : window.localStorage.getItem("ssnMbrUid"),
          mbrDivi : (mbr.mbrDivi !== "") ? mbr.mbrDivi : window.localStorage.getItem("mbrDivi"),
        }
        
        initSetHandle(data);
        */
      }
      else if (PROC_CHK === "SETWTHDR")
      {
        window.scrollTo(0,0);
        closeWthdrModal();
        navigate("/");
        //return;
      }
    }
  }

  async function goCRUD() {
    let fileStorePath = "Globals.fileStoreBizPath"; //로컬저장경로
    let fileLinkPath = "Globals.fileLinkBizPath"; //link경로
    let filePrefix = "biz"; //파일코드

    PROC_CHK = "FILEUPLOAD";

    let uploadRes = await snglFileUpload( server.path, fileStorePath, fileLinkPath, filePrefix, detailState.uploadFiles, detailState.filePosNm);
    //jsonCompResult(uploadRes.data);
    if(uploadRes === 0) cntntsCRUD();
    else jsonCompResult(uploadRes.data);
  }

  async function cntntsCRUD() {
    PROC_CHK = 'UPD';

    let goUrl = server.path + "/mbr/mbrupdate";
    let axiosRes = await axiosCrudOnSubmit(detailState, goUrl, "POST");

    jsonCompResult(axiosRes.data);
  }

  const setWthdrYn = useCallback(async () => {
    PROC_CHK = 'SETWTHDR';

    let goUrl = server.path + "/mbr/wthdrYn";
    
    let data = {
      wthdrYn: "Y",
      mbrUid : ssn.ssnMbrUid
    }

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    jsonCompResult(axiosRes.data);

    closeWthdrModal();
  }, [])

  return(
    <main className="employersMain">
        <ToastContainer />
        <section>
          <h4 className="sectionTitle">
            <FormattedMessage
              id="mypage.edit.my.info"
              defaultMessage="내 정보 수정"
            />
          </h4>
          <div className="mypageWrap">
            <form>
              <div className="formSection">
                <div>
                  <label htmlFor="corpNum">
                    <FormattedMessage
                      id="mypage.corporate.number"
                      defaultMessage="사업자등록번호"
                    />*</label>
                  <input type="text" value={detailState.bizNum} id="corpNum" readOnly />
                </div>
                <div>
                  <label htmlFor="corpName">
                    <FormattedMessage
                      id="mypage.corporate.name"
                      defaultMessage="기업체명"
                    />*</label>
                  <input type="text" 
                    placeholder={intl.formatMessage({
                      id: "placeholder.corporate.name",
                      defaultMessage: "기업체명을 입력하세요"
                    })} 
                    id="corpName"
                    value={detailState.bizNm}
                    onChange={(e)=>{
                      setDetailState({
                        ...detailState,
                        bizNm: e.target.value
                      })
                    }}/>
                </div>
                <div className="tooltipBox">
                  <p>
                    <span className="material-icons">error</span> 
                      <FormattedMessage
                        id="warning"
                        defaultMessage="주의사항"
                      />
                  </p>
                  <div className="tooltip">
                    <p>
                      <FormattedMessage
                        id="description.corporate.name1"
                        defaultMessage="법인의 경우 약자를 활용하여 법인의 종류를 구분하여 입력바랍니다."
                      />  
                    </p>
                    <p>
                      <FormattedMessage
                        id="description.corporate.name2"
                        defaultMessage="(예) ㈜위크루트/위크루트㈜/(유)유미특허/위드회계법인(유) 등"
                      />
                    </p>
                  </div>
                </div>

                <div>
                  <label>
                    <FormattedMessage
                      id="mypage.corporate.file.attach"
                      defaultMessage="사업자등록증 첨부"
                    />*
                  </label>
                  <div className="fileUpload"> 
                    <span><span className="material-icons" style={{cursor:'pointer'}}
                      onClick={() => {handleDownload(1, detailState.bizCertThmnl, detailState.bizCertThmnlNm)}}
                      >file_download</span></span>
                    <input type="file" id="fileInput1" className="fileInput"
                      accept=".png, .jpg, .jpeg, .pdf" 
                      onChange={(e) => {
                        let tmpList = [...detailState.uploadFiles];
                        tmpList[1] = e.target.files[0];

                        let tmpJsonStrList = [...detailState.snglImgFileJsonStr];
                        tmpJsonStrList[1] = "";
                        
                        setDetailState({
                          ...detailState,
                          uploadFiles: tmpList,
                          snglImgFileJsonStr: tmpJsonStrList,
                          bizCertThmnl: "",
                          bizCertThmnlNm: e.target.files[0].name
                        })
                      }}/>
                    <span className="fileName">{detailState.bizCertThmnlNm}</span>
                    <label htmlFor="fileInput1">
                      <FormattedMessage
                        id="find"
                        defaultMessage="찾아보기"
                      />
                    </label>
                  </div>
                </div>
                <div className="tooltipBox">
                  <p>
                    <span className="material-icons">error</span> <FormattedMessage
                        id="warning"
                        defaultMessage="주의사항"
                      />
                  </p>
                  <div className="tooltip">
                    <p>
                      <FormattedMessage
                        id="description.corporate.file1"
                        defaultMessage="파일명에 특수문자가 있는 경우 등록할 수 없습니다. (국문, 영문, 숫자만 가능)"
                      />
                    </p>
                    <p>
                      <FormattedMessage
                        id="description.corporate.file2"
                        defaultMessage="구글 크롬(Chrome) 브라우저를 활용하여 업로딩을 진행바랍니다."
                      />
                    </p>
                  </div>
                </div>

                <div>
                  <label>
                    <FormattedMessage
                      id="mypage.corporate.logo.attach"
                      defaultMessage="기업로고 첨부"
                    />*
                  </label>
                  <div className="fileUpload">
                    <span><span className="material-icons" style={{cursor:'pointer'}}
                      onClick={(e) => {handlePreview(2, detailState.bizLogoThmnl)}}
                      >file_download</span></span>
                    <input type="file" id="fileInput2" className="fileInput"
                      accept=".png, .jpg, .jpeg" 
                      onChange={(e) => {
                        let tmpList = [...detailState.uploadFiles];
                        tmpList[2] = e.target.files[0];

                        let tmpJsonStrList = [...detailState.snglImgFileJsonStr];
                        tmpJsonStrList[2] = "";
                        
                        setDetailState({
                          ...detailState,
                          uploadFiles: tmpList,
                          snglImgFileJsonStr: tmpJsonStrList,
                          bizLogoThmnl: "",
                          bizLogoThmnlNm: e.target.files[0].name
                        })
                      }}/>
                    <span className="fileName">{detailState.bizLogoThmnlNm}</span>
                    <label htmlFor="fileInput2">
                      <FormattedMessage
                        id="find"
                        defaultMessage="찾아보기"
                      />
                    </label>
                  </div>
                </div>
                <div className="tooltipBox">
                  <p>
                    <span className="material-icons">error</span> <FormattedMessage
                        id="warning"
                        defaultMessage="주의사항"
                      />
                  </p>
                  <div className="tooltip">
                    <p>
                      <FormattedMessage
                        id="description.corporate.logo1"
                        values = {{
                          breakLine: <br/>
                        }}
                        defaultMessage="로고 첨부시 보고서 커버 페이지에 삽입됩니다. {breakLine} (첨부가 없어도 로고가 생략된 보고서 다운로드가 가능합니다.)"
                      />
                    </p>
                    <p>
                      <FormattedMessage
                        id="description.corporate.logo2"
                        values = {{
                          breakLine: <br/>
                        }}
                        defaultMessage="png/jpg 이미지 파일만 등록이 가능합니다. {breakLine} 파일은 2MB, 가로 200px(최대) 사이즈를 권장합니다."
                      /></p>
                  </div>
                </div>
              </div>
              <div className="formSection">
                <div>
                  <label htmlFor="email">
                    <FormattedMessage
                      id="email.address"
                      defaultMessage="이메일 주소"
                    />(ID) *</label>
                  <div>
                    <input type="text"
                      id="email" readOnly 
                      value={detailState.mbrId}/>
                  </div>
                </div>
                <div>
                  <label htmlFor="userName">
                    <FormattedMessage
                      id="employer.name"
                      defaultMessage="인사담당자명"
                    />*</label>
                  <input type="text" id="userName" 
                    placeholder={intl.formatMessage({
                      id: "placeholder.name",
                      defaultMessage: "이름을 입력하세요."
                    })}
                    value={detailState.mbrNm}
                    onChange={(e)=>{
                      setDetailState({
                        ...detailState,
                        mbrNm: e.target.value
                      })
                    }}/>
                </div>
                <div>
                  <label htmlFor="userPhone">
                    <FormattedMessage
                      id="phone.number"
                      defaultMessage="휴대폰 번호"
                    />*</label>
                  <div>
                    <select name id>
                      <option>+82</option>
                    </select>
                    <input type="text" id="userPhone" 
                      placeholder={intl.formatMessage({
                        id: "placeholder.phone.number",
                        defaultMessage: "&quot;-&quot;없이 숫자만 입력하세요."
                      })}
                      value={detailState.celpNum}
                      onChange={(e)=>{
                        setDetailState({
                          ...detailState,
                          celpNum: e.target.value
                        })
                      }}/>
                  </div>
                </div>
                <div>
                  <label htmlFor="officePhone">
                    <FormattedMessage
                      id="office.number"
                      defaultMessage="사무실 전화번호"
                    />
                  </label>
                  <div>
                    <select name id>
                      <option>+82</option>
                    </select>
                    <input type="text" id="officePhone" 
                      placeholder={intl.formatMessage({
                        id: "placeholder.phone.number",
                        defaultMessage: "&quot;-&quot;없이 숫자만 입력하세요."
                      })}
                      value={detailState.bizPhNum}
                      onChange={(e)=>{
                        setDetailState({
                          ...detailState,
                          bizPhNum: e.target.value
                        })
                      }}/>
                  </div>
                </div>

                <div className="flexAlignRight">
                  <button className="modalOpenButton basicColored" type="button" onClick={openWthdrModal}>회원탈퇴 &gt;</button>
                  {
                    <Modal visible={wthdrModal} closable={true} maskClosable={true} onSubmit={setWthdrYn} onClose={closeWthdrModal} 
                    inputName={intl.formatMessage({
                      id: "yes",
                      defaultMessage: "예"
                    })} 
                    closeName={intl.formatMessage({
                      id: "no",
                      defaultMessage: "아니오"
                    })} >
                      <LeaveWecruit state={detailState} />
                    </Modal>
                  } 
                </div>
              </div>
              
              <input 
                type="button" 
                value={intl.formatMessage({
                        id: "saving",
                        defaultMessage: "저장"
                      })}
                onClick={goCRUD}
              />
            </form>
          </div>
        </section>
      </main>
  );
}