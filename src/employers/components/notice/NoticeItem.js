import { axiosCrudOnSubmit } from "commons/modules/commonUtil";
import { Context } from "contexts";
import { useContext, useRef, useState } from "react"

export const NoticeItem =(props) => {
  const [setActive, setActiveState] = useState("");
  const [contentHeight, setContentHeight] = useState("0px");

  const content = useRef(null);

  const {
    state : {
        server
    },
    dispatch,
  } = useContext(Context);

  const updNotiCnt = async () => {
    let data = {
      brdUid : props.id
    }
    
    let goUrl = server.path + "/noti/" + props.id + "/cnt";
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    console.log("INIT RES : ");
    console.log(axiosRes);
  }

  function toggleHandler () {
    setActiveState(setActive === "" ? "active" : "");
    setContentHeight(
      setActive === "active" ? "0px" : `${content.current.scrollHeight}px`
    );

    if( setActive === "" )
    {
      updNotiCnt();
    }
  }

  return(
    <li onClick={toggleHandler} className={setActive}>
      <div>
        <div>
          <span>{props.idx}</span>
          <span>{props.title}</span>
        </div>
        <div>
          <span>{props.date}</span>
          <span className="material-icons">
            expand_more
          </span>
        </div>
      </div>
      <div ref={content} style={{maxHeight: `${contentHeight}`}} dangerouslySetInnerHTML={{__html:props.content}}>
      </div>
    </li>
  )
}