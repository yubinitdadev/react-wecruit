import { axiosCrudOnSubmit } from "commons/modules/commonUtil";
import { Context } from "contexts";
import { useContext, useEffect, useState } from "react";
import { FormattedMessage, useIntl } from "react-intl";
import { NoticeItem } from "./NoticeItem";

export const Notice =() => {
  const intl = useIntl();
  let tmpIdx = 1;

  const moment = require('moment');
  
  const [notis, setNotis] = useState([]);
  const [buttonList, setButtonList] = useState({
    allViewBtn : "active",
  });

  const {
    state : {
        mbr,
        server
    },
    dispatch,
  } = useContext(Context);

  const getNotiList = async (data) => {
    let goUrl = server.path + "/noti/list"
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    console.log("INIT RES : ");
    console.log(axiosRes);

    setNotis(axiosRes.data.notiArr);
  }

  const clickMenu = ( menu ) => {
    setButtonList({
      [menu] : "active"
    });
  }

  useEffect( async () => {
    window.scrollTo(0,0);

    let data = {
      ssnMbrUid : mbr.mbrUid
      ,sPubKnd : "emp"
    }

    getNotiList(data);
  },[])

  return(
    <main className="employersMain">
        <section>
          <h4 className="sectionTitle">
            <FormattedMessage
              id="notification"
              defaultMessage="공지사항"
            />
          </h4>
          <div className="noticeWrap">
            <ul className="noticeTabButton">
              <li className={(buttonList.allViewBtn === "active")? "active" : undefined}
                onClick={()=>{
                  clickMenu("allViewBtn");
                }}
                >
                  <FormattedMessage
                    id="show.all"
                    defaultMessage="전체보기"
                  />
                </li>
              <li className={(buttonList.createProjectBtn === "active")? "active" : undefined}
                onClick={()=>{
                  clickMenu("createProjectBtn");
                }}>
                  <FormattedMessage
                    id="notification.project.register"
                    defaultMessage="프로젝트 등록"
                  />
                </li>
              <li className={(buttonList.etcBtn === "active")? "active" : undefined}
                onClick={()=>{
                  clickMenu("etcBtn");
                }}>
                  <FormattedMessage
                    id="etc"
                    defaultMessage="기타"
                  />
              </li>
            </ul>
            <div className="noticeTabContent">
              <ul>
                {notis.map((noti, idx) => {
                  if(buttonList.allViewBtn === "active")
                  {
                    return <NoticeItem  
                      idx={idx + 1}
                      key={noti.brdUid} 
                      id={noti.brdUid}   
                      date={ moment(noti.rgstDttm).format("YYYY년 MM월 DD일 HH시 mm분") }
                      title={noti.title} 
                      content={noti.cntnt}
                    />
                  } else if(buttonList.createProjectBtn === "active")
                  {
                    if(noti.cateCd === "01")
                    {
                      return <NoticeItem  
                        idx={tmpIdx++}
                        key={noti.brdUid} 
                        id={noti.brdUid}  
                        date={ moment(noti.rgstDttm).format("YYYY년 MM월 DD일 HH시 mm분") }
                        title={noti.title} 
                        content={noti.cntnt}
                      />
                    } 
                  } else if(buttonList.etcBtn === "active")
                  {
                    if(noti.cateCd === "02" || noti.cateCd === "03" || noti.cateCd === "04")
                    {
                      return <NoticeItem  
                        idx={tmpIdx++}
                        key={noti.brdUid} 
                        id={noti.brdUid}   
                        date={ moment(noti.rgstDttm).format("YYYY년 MM월 DD일 HH시 mm분") }
                        title={noti.title} 
                        content={noti.cntnt}
                      />
                    }
                  }
                }
                )}
              </ul>
            </div>
          </div>
        </section>
      </main>
  );
}