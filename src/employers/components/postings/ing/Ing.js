import { DownloadBtns } from "commons/components/download-btn/DownloadBtns";
import { Pagination } from "commons/components/pagination/Pagination";
import { axiosCrudOnSubmit } from "commons/modules/commonUtil";
import * as commonUtil from "commons/modules/commonUtil";
import * as defVal from "commons/modules/defVal";
import { Context } from "contexts";
import { UPD_MBRUID } from "contexts/actionTypes";
import { useContext, useEffect, useRef, useState } from "react";
import { Link } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import { CopyToClipboard } from "react-copy-to-clipboard";
import { FormattedMessage, useIntl } from "react-intl";

export const Ing = () => {
  const intl = useIntl();

  const {
    state: { server, mbr },
    dispatch,
  } = useContext(Context);

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid"),
    ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi"),
    ssnRefUid: window.localStorage.getItem("ssnRefUid"),
  };

  const moment = require("moment");
  const componentRef = useRef();

  const [listState, setListState] = useState([]);
  const [dprtMappListState, setDprtMappListState] = useState([]);
  const [myDprtMbrlist, setMyDprtMbrList] = useState([]);
  const [sConts, setSConts] = useState("");
  const [fltrState, setFltrState] = useState({});

  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(10);

  const indexOfLast = currentPage * postsPerPage;
  const indexOfFirst = indexOfLast - postsPerPage;

  function currentPosts(tmp) {
    let currentPosts = 0;
    currentPosts = tmp.slice(indexOfFirst, indexOfLast);
    return currentPosts;
  }

  async function goSrch() {
    let goUrl = server.path + "/hire/hirelist";
    let data = {
      serverPath: server.path,
      ssnMbrUid: ssn.ssnMbrUid,
      mbrUid: ssn.ssnMbrUid,
      refUid: ssn.ssnRefUid,
      mbrDivi: ssn.ssnMbrDivi,
      hireStat: "ing",
      sConts: sConts,
      sFltr: fltrState,
      sFltrCate: Object.keys(fltrState)[0],
      sFltrCont: Object.values(fltrState)[0],
    };

    initSetHandle(data);
  }

  const changeFltr = (fltr) => {
    let tmpFltr = fltr === "ASC" ? "DESC" : fltr === "DESC" ? "ASC" : "ASC";

    return tmpFltr;
  };

  const initSetHandle = async (data) => {
    let goUrl = server.path + "/hire/hirelist";
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    console.log("ssn.ssnMbrUid == " + ssn.ssnMbrUid);

    let axiosResData = axiosRes.data.hireArr;
    let tmpAxiosResData = [];

    axiosResData.forEach((resData, idx) => {
      resData["rcmndEndDttm"] = moment(resData.rcmndEndDttm).format(
        "YYYY-MM-DD HH:mm"
      );

      tmpAxiosResData.push(resData);
    });

    var tmpMyDprtMbrList = await defVal.getMyDprtMbrList(data);
    setMyDprtMbrList(tmpMyDprtMbrList);

    console.log("tmpMyDprtMbrList == " + tmpMyDprtMbrList.length);

    setListState(tmpAxiosResData);

    //selEmpHireMappList
  };

  useEffect(() => {
    goSrch();
  }, [fltrState]);

  useEffect(async () => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <main className="employersMain">
      <section className="recruitLists">
        <div className="ingList">
          <h3 className="sectionTitle">
            <span>
              <FormattedMessage
                id="posting.ing"
                defaultMessage="진행중 채용공고"
              />
            </span>
          </h3>

          <ul>
            {listState.map((list, idx) => (
              <li className="ingListColumn">
                <div className="listTop">
                  <div className="listLeft">
                    <h3>
                      <Link
                        to="/employers/ing-detail"
                        state={{ hireUid: list.hireUid, mbrUid: list.mbrUid }}
                      >
                        {list.title}
                      </Link>
                    </h3>
                    <p>
                      <FormattedMessage
                        id="registration.date"
                        defaultMessage="등록일"
                      />{" "}
                      :&nbsp;{moment(list.rgstDttm).format("YYYY-MM-DD")}
                      &nbsp; /{" "}
                      <FormattedMessage
                        id="recruitment.deadline"
                        defaultMessage="인재추천 마감일"
                      />
                      :&nbsp;
                      {list.rcmndUntilDon === "Y"
                        ? `${intl.formatMessage({
                            id: "until.hired",
                            defaultMessage: "채용시까지",
                          })}`
                        : list.rcmndEndDttm}
                      &nbsp; /{" "}
                      <FormattedMessage
                        id="recruitment.people.number"
                        defaultMessage="모집인원"
                      />
                      : {list.hireSize}
                      <FormattedMessage id="people.count" defaultMessage="명" />
                    </p>
                    <p>
                      <FormattedMessage
                        id="suggestion.service.fee"
                        defaultMessage="서비스 수수료율"
                      />{" "}
                      :&nbsp;
                      {list.srvcFeeFixYn === "Y"
                        ? `${intl.formatMessage({
                            id: "suggestion.fixed.fee.system",
                            defaultMessage: "정액제",
                          })}`
                        : list.srvcFeePctMin +
                          "% ~ " +
                          list.srvcFeePctMax +
                          "%"}
                      &nbsp;/{" "}
                      <FormattedMessage
                        id="suggestion.service.guarantee"
                        defaultMessage="서비스 보증조건"
                      />{" "}
                      :&nbsp;
                      {list.srvcWrtyPeriod}
                      <FormattedMessage
                        id="months"
                        defaultMessage="개월"
                      />{" "}
                      <FormattedMessage
                        id="suggestion.incase.retire"
                        defaultMessage="이내 퇴직시"
                      />
                      {list.srvcWrtyMthd === "prorated"
                        ? `${intl.formatMessage({
                            id: "suggestion.caculated.refund",
                            defaultMessage: "일할계산 환불",
                          })}`
                        : list.srvcWrtyMthd === "all"
                        ? `${intl.formatMessage({
                            id: "suggestion.all.refund",
                            defaultMessage: "100% 전액 환불",
                          })}`
                        : list.srvcWrtyMthd === "etc"
                        ? `${intl.formatMessage({
                            id: "etc",
                            defaultMessage: "기타",
                          })}`
                        : ""}
                    </p>
                    <CopyToClipboard
                      text={73}
                      onCopy={() =>
                        alert(
                          `${intl.formatMessage({
                            id: "url.copy.done",
                            defaultMessage: "url 복사가 완료되었습니다.",
                          })}`
                        )
                      }
                    >
                      <p style={{ display: "none" }}>
                        URL : 73{" "}
                        <button type="button">
                          URL{" "}
                          <FormattedMessage id="copy" defaultMessage="복사" />
                        </button>
                      </p>
                    </CopyToClipboard>
                  </div>
                  <div className="flexColumn listRight">
                    <div className="threeSections">
                      <div>
                        <span>
                          <FormattedMessage
                            id="recruiter.suggested"
                            defaultMessage="제안한 리크루터"
                          />
                        </span>
                        <Link
                          to="/employers/recruiter-manage"
                          state={{ hireUid: list.hireUid, mbrUid: list.mbrUid }}
                        >
                          {list.cmmntCnt}
                          <FormattedMessage
                            id="people.count"
                            defaultMessage="명"
                          />
                        </Link>
                      </div>
                      <div>
                        <span>
                          <FormattedMessage
                            id="recruiter.selected"
                            defaultMessage="선택한 리크루터"
                          />
                        </span>
                        <Link
                          to="/employers/recruiter-manage"
                          state={{ hireUid: list.hireUid, mbrUid: list.mbrUid }}
                        >
                          <strong>{list.cmmntChooseCnt}</strong>/{list.recSize}
                          <FormattedMessage
                            id="people.count"
                            defaultMessage="명"
                          />
                        </Link>
                      </div>
                      <div>
                        <span>
                          <FormattedMessage
                            id="candidate.recommended"
                            defaultMessage="추천된 후보자"
                          />
                        </span>
                        <Link
                          to="/employers/candidate-manage"
                          state={{ hireUid: list.hireUid, mbrUid: list.mbrUid }}
                        >
                          {list.recmmndCnt}
                          <FormattedMessage
                            id="people.count"
                            defaultMessage="명"
                          />
                          ({list.chkingRecmmndCnt}
                          <FormattedMessage
                            id="people.count"
                            defaultMessage="명"
                          />
                          /{list.nopassRecmmndCnt}
                          <FormattedMessage
                            id="people.count"
                            defaultMessage="명"
                          />
                          )
                        </Link>
                      </div>
                    </div>
                    <div className="qnaStatus">
                      <Link
                        to="/employers/qna-manage"
                        state={{ hireUid: list.hireUid, mbrUid: list.mbrUid }}
                      >
                        Q&amp;A{" "}
                        <span>
                          {list.qaCnt}
                          <FormattedMessage id="count" defaultMessage="개" />
                        </span>
                      </Link>
                    </div>
                    <p>
                      <FormattedMessage
                        id="description.open.to.recruiter"
                        defaultMessage="* 채용진행 현황은 참여중인 리크루터에게도 공개됩니다."
                      />
                    </p>
                  </div>
                </div>
                <div className="idManage">
                  <span>
                    <FormattedMessage
                      id="department.id.matched"
                      defaultMessage="매칭된 현업부서 아이디"
                    />
                    : &nbsp;
                    {myDprtMbrlist.map((dprtlist, dprtIdx) =>
                      commonUtil.indexOf(list.mappMbrUid, dprtlist.mbrUid) >
                      -1 ? (
                        <span>{dprtlist.mbrId}</span>
                      ) : (
                        ""
                      )
                    )}
                  </span>
                  <Link
                    to="/employers/department"
                    state={{ hireUid: list.hireUid, mbrUid: list.mbrUid }}
                  >
                    <FormattedMessage
                      id="department.header"
                      defaultMessage="현업부서 아이디 관리"
                    />
                  </Link>
                </div>
              </li>
            ))}
          </ul>
          {listState.length === 0 ? (
            <div className="noRecruitList">
              <FormattedMessage
                id="description.no.ing.posting"
                defaultMessage="진행중인 채용공고가 없습니다."
              />
            </div>
          ) : (
            ""
          )}
        </div>
        <ul className="listNotice">
          <li>
            <FormattedMessage
              id="description.ing1"
              defaultMessage="ㆍ 검수가 완료되어 현재 진행중인 채용공고 목록입니다."
            />
          </li>
          <li>
            <FormattedMessage
              id="description.ing2"
              defaultMessage="ㆍ 채용공고에 맞는 리크루터를 선택하시고 추천하는 인재를 만나보세요."
            />
          </li>
        </ul>
      </section>
    </main>
  );
};
