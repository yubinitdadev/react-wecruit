import { DownloadBtns } from "commons/components/download-btn/DownloadBtns";
import { Pagination } from "commons/components/pagination/Pagination";
import { axiosCrudOnSubmit } from "commons/modules/commonUtil";
import { Context } from "contexts";
import { UPD_MBRUID } from "contexts/actionTypes";
import { useContext, useEffect, useRef, useState } from "react";
import { Link } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import { FormattedMessage, useIntl } from "react-intl";

export const Confirm = () => {
  const intl = useIntl();

  const {
    state: { server, mbr },
    dispatch,
  } = useContext(Context);

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid"),
    ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi"),
    ssnRefUid: window.localStorage.getItem("ssnRefUid"),
  };

  const moment = require("moment");
  const componentRef = useRef();

  const [listState, setListState] = useState([]);
  const [sConts, setSConts] = useState("");
  const [fltrState, setFltrState] = useState({});

  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(10);

  const indexOfLast = currentPage * postsPerPage;
  const indexOfFirst = indexOfLast - postsPerPage;

  function currentPosts(tmp) {
    let currentPosts = 0;
    currentPosts = tmp.slice(indexOfFirst, indexOfLast);
    return currentPosts;
  }

  async function goSrch() {
    let goUrl = server.path + "/hire/list";
    let data = {
      ssnMbrUid: ssn.ssnMbrUid,
      mbrUid: ssn.ssnMbrUid,
      refUid: ssn.ssnRefUid,
      mbrDivi: ssn.ssnMbrDivi,
      hireStat: "confirm",
      sConts: sConts,
      sFltr: fltrState,
      sFltrCate: Object.keys(fltrState)[0],
      sFltrCont: Object.values(fltrState)[0],
    };

    initSetHandle(data);
  }

  const changeFltr = (fltr) => {
    let tmpFltr = fltr === "ASC" ? "DESC" : fltr === "DESC" ? "ASC" : "ASC";

    return tmpFltr;
  };

  const initSetHandle = async (data) => {
    let goUrl = server.path + "/hire/hirelist";
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    console.log("ssn.ssnMbrUid == " + ssn.ssnMbrUid);

    let axiosResData = axiosRes.data.hireArr;
    let tmpAxiosResData = [];

    axiosResData.forEach((resData, idx) => {
      resData["rcmndEndDttm"] = moment(resData.rcmndEndDttm).format(
        "YYYY-MM-DD HH:mm"
      );

      tmpAxiosResData.push(resData);
    });

    setListState(tmpAxiosResData);
  };

  useEffect(() => {
    goSrch();
  }, [fltrState]);

  useEffect(async () => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <main className="employersMain">
      <section className="recruitLists">
        <div className="confirmingList">
          <h3 className="sectionTitle">
            <span>
              <FormattedMessage
                id="posting.confirm"
                defaultMessage="검수중 채용공고"
              />
            </span>
          </h3>

          <ul>
            {listState.map((list, idx) => (
              <li>
                <div className="listLeft">
                  <h3>
                    <Link
                      to="/employers/confirm-detail"
                      state={{ hireUid: list.hireUid, mbrUid: list.mbrUid }}
                    >
                      {list.title}
                    </Link>
                  </h3>
                  <p>
                    <FormattedMessage
                      id="registration.date"
                      defaultMessage="등록일"
                    />{" "}
                    :&nbsp;{moment(list.rgstDttm).format("YYYY-MM-DD")}
                    &nbsp; /{" "}
                    <FormattedMessage
                      id="recruitment.deadline"
                      defaultMessage="인재추천 마감일"
                    />
                    :&nbsp;
                    {list.rcmndUntilDon === "Y"
                      ? `${intl.formatMessage({
                          id: "until.hired",
                          defaultMessage: "채용시까지",
                        })}`
                      : list.rcmndEndDttm}
                    &nbsp; /{" "}
                    <FormattedMessage
                      id="recruitment.people.number"
                      defaultMessage="모집인원"
                    />
                    : {list.hireSize}
                    <FormattedMessage id="people.count" defaultMessage="명" />
                  </p>
                  <p>
                    <FormattedMessage
                      id="suggestion.service.fee"
                      defaultMessage="서비스 수수료율"
                    />{" "}
                    :&nbsp;
                    {list.srvcFeeFixYn === "Y"
                      ? `${intl.formatMessage({
                          id: "suggestion.fixed.fee.system",
                          defaultMessage: "정액제",
                        })}`
                      : list.srvcFeePctMin + "% ~ " + list.srvcFeePctMax + "%"}
                    &nbsp; /{" "}
                    <FormattedMessage
                      id="suggestion.service.guarantee"
                      defaultMessage="서비스 보증조건"
                    />{" "}
                    :&nbsp;
                    {list.srvcWrtyPeriod}
                    <FormattedMessage id="months" defaultMessage="개월" />{" "}
                    <FormattedMessage
                      id="suggestion.incase.retire"
                      defaultMessage="이내 퇴직시"
                    />
                    {list.srvcWrtyMthd === "prorated"
                      ? `${intl.formatMessage({
                          id: "suggestion.caculated.refund",
                          defaultMessage: "일할계산 환불",
                        })}`
                      : list.srvcWrtyMthd === "all"
                      ? `${intl.formatMessage({
                          id: "suggestion.all.refund",
                          defaultMessage: "100% 전액 환불",
                        })}`
                      : list.srvcWrtyMthd === "etc"
                      ? `${intl.formatMessage({
                          id: "etc",
                          defaultMessage: "기타",
                        })}`
                      : ""}
                  </p>
                </div>
                {list.hireStat === "confirm" ? (
                  <span className="confirmingStatus">
                    <FormattedMessage
                      id="status.confirming"
                      defaultMessage="검수중"
                    />
                  </span>
                ) : list.hireStat === "declined" ? (
                  <span className="confirmingStatus rejected">
                    <FormattedMessage id="refuse" defaultMessage="거절" />
                  </span>
                ) : (
                  ""
                )}
              </li>
            ))}
          </ul>

          {listState.length === 0 ? (
            <div className="noRecruitList">
              <FormattedMessage
                id="description.no.confirming.posting"
                defaultMessage="검수중인 채용공고가 없습니다."
              />
            </div>
          ) : (
            ""
          )}
        </div>
        <ul className="listNotice">
          <li>
            <FormattedMessage
              id="description.confirm1"
              defaultMessage="ㆍ 위크루트 내부에서 검수 중인 채용공고 목록입니다. 검수에는 최대 24시간이 소요됩니다."
            />
          </li>
          <li>
            <FormattedMessage
              id="description.confirm2"
              defaultMessage="ㆍ 검수 과정에서 위크루트 매니저가 유선 또는 이메일 연락을 할 수 있습니다."
            />
          </li>
          <li>
            <FormattedMessage
              id="description.confirm3"
              defaultMessage="ㆍ 검수가 완료되면 리크루터 모집이 시작됩니다."
            />
          </li>
        </ul>
      </section>
    </main>
  );
};
