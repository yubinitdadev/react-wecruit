import { useCallback, useContext, useEffect, useRef, useState } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { Context } from "contexts";
import Modal from "modal-all/ModalAll";
import { axiosCrudOnSubmit } from "commons/modules/commonUtil";
import * as commonUtil from "commons/modules/commonUtil";
import * as defVal from "commons/modules/defVal";
import { toast, ToastContainer } from "react-toastify";
import { UPD_MBRUID } from "contexts/actionTypes";
import { FormattedMessage, useIntl } from "react-intl";

export const SearchFirm = () => {
  const intl = useIntl();

  let PROC_CHK = "";

  const {
    state: { server, mbr },
    dispatch,
  } = useContext(Context);

  const location = useLocation();
  const navigate = useNavigate();
  const moment = require("moment");
  let previewWindow = "";
  let previewImg = document.createElement("img");

  const [files, setFiles] = useState("");
  const [listState, setListState] = useState([]);
  const [modalVisible1, setDetailModal] = useState(false);
  const [fltrState, setFltrState] = useState({});
  const [chckboxList, setChckboxList] = useState([]);
  const [detailState, setDetailState] = useState(
    defVal.setAxiosSerachFirmState(null)
  );

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid"),
    ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi"),
  };

  let locationState = {
    mbrUid: ssn.ssnMbrUid,
  };

  useEffect(async () => {
    console.log("Page First Load EFFECT");
    window.scrollTo(0, 0);
    initSetHandle();
  }, []);

  const reRender = () => {
    setListState({
      ...setListState,
      initSetCmplt: true,
    });
  };

  const initSetHandle = async () => {
    let goUrl = server.path + "/srchfirm/searchfirmlist";
    let data = {
      ssnMbrUid: ssn.ssnMbrUid,
      mbrUid: ssn.ssnMbrUid,
      mbrDivi: "MBRDIVI-01",
      sFltr: fltrState,
      sFltrCate: Object.keys(fltrState)[0],
      sFltrCont: Object.values(fltrState)[0],
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    let tmpArr = [];
    for (var k = 0; k < axiosRes.data.resArr.length; k++) {
      var tmpData = defVal.setAxiosSerachFirmState(axiosRes.data.resArr[k]);
      tmpArr.push(tmpData);
    }

    setListState(tmpArr);
  };

  async function changeFltr(key, fltr) {
    let tmpFltr = fltr === "ASC" ? "DESC" : fltr === "DESC" ? "ASC" : "ASC";

    setFltrState({ [key]: tmpFltr });

    initSetHandle();
  }

  const onChangeChecked = (e, seq) => {
    //var tmpList = listState;

    if (listState[seq].chkYn == "Y") listState[seq].chkYn = "N";
    else listState[seq].chkYn = "Y";

    //setListState(tmpList);
    //reRender();
  };

  /////////////////////////////////////////////////////////////////////////////////////////

  const openDetailModal = () => {
    setDetailModal(true);
  };

  const closeDetailModal = () => {
    setDetailModal(false);
  };

  function onKeyDownForm(keyEvent) {
    if ((keyEvent.charCode || keyEvent.keyCode) === 13) {
      keyEvent.preventDefault();
    }
  }

  /////////////////////////////////////////////////////////////////////////////////////////

  const jsonCompResult = (resJson) => {
    if (resJson.resCd !== "0000") {
      toast.error(
        intl.formatMessage({
          id: "error.during.connecting.sever",
          defaultMessage: "서버와 통신 중 오류가 발생했습니다.",
        })
      );
      return;
    } else {
      if (PROC_CHK == "INS") {
        PROC_CHK = "";

        if (resJson.srchfirmUid === "DUPL") {
          toast.error(
            intl.formatMessage({
              id: "address.exist",
              defaultMessage: "이미 등록된 주소입니다.",
            })
          );
          return;
        } else {
          toast.info(
            intl.formatMessage({
              id: "info.address.added",
              defaultMessage: "주소가 등록되었습니다.",
            })
          );
          initSetHandle();
          setDetailState(defVal.setAxiosSerachFirmState(null));
          closeDetailModal();
          return;
        }
      } else if (PROC_CHK === "DEL") {
        toast.info(
          intl.formatMessage({
            id: "info.selected.address.deleted",
            defaultMessage: "선택하신 주소가 삭제되었습니다.",
          })
        );
        initSetHandle();
      }
    }
  };

  const goCRUD = async () => {
    if (
      commonUtil.CheckIsEmptyFromVal(
        detailState.recNm,
        intl.formatMessage({
          id: "error.need.input.name",
          defaultMessage: "이름을 입력해주세요.",
        })
      ) === false
    )
      return;
    if (
      commonUtil.CheckIsEmptyFromVal(
        detailState.email,
        intl.formatMessage({
          id: "error.need.input.email",
          defaultMessage: "이메일을 입력해주세요.",
        })
      ) === false
    )
      return;

    if (listState.length >= 30) {
      toast.error(
        intl.formatMessage({
          id: "error.searchfirm.over.added",
          defaultMessage: "기존 거래 써치펌은 30개까지만 등록 가능합니다.",
        })
      );
      closeDetailModal();
      return;
    }

    cntntsCRUD();
  };

  async function cntntsCRUD() {
    PROC_CHK = "INS";
    let goUrl = server.path + "/srchfirm/searchfirminsert";
    detailState.mbrUid = ssn.ssnMbrUid;
    let axiosRes = await axiosCrudOnSubmit(detailState, goUrl, "POST");

    jsonCompResult(axiosRes.data);
  }

  const goDel = async () => {
    PROC_CHK = "DEL";
    let goUrl = server.path + "/srchfirm/searchfirmdelete";

    var tmpList = [];
    for (var k = 0; k < listState.length; k++) {
      if (listState[k].chkYn == "Y") {
        tmpList.push(listState[k].srchfirmUid);
      }
    }

    if (tmpList.length == 0) {
      toast.error(
        intl.formatMessage({
          id: "error.select.recruiter.to.delete",
          defaultMessage: "삭제하실 리크루터를 선택해 주세요.",
        })
      );
      return;
    }

    let data = {
      mbrUid: mbr.mbrUid,
      srchfirmUid: tmpList,
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    jsonCompResult(axiosRes.data);
  };

  return (
    <main className="employersMain">
      <ToastContainer />
      <section>
        <div className="searchFirmWrap">
          <h3 className="sectionTitle">
            <span>
              <FormattedMessage
                id="search.firm"
                defaultMessage="기존 거래 써치펌 주소록"
              />
            </span>
          </h3>
          <div className="infoParagraph">
            <p>
              <FormattedMessage
                id="description.searchfirm1"
                defaultMessage="※ 채용공고에서 기존 거래 써치펌으로 리크루터를 등록하시면, 별도의 중개 수수료 없이 해당 리크루터(헤드헌터)와 거래하실 수 있습니다."
              />
            </p>
            <p>
              <FormattedMessage
                id="description.searchfirm2"
                defaultMessage="(단, 계약내용에 따라 거래의 안전을 위한 별도의 보증보험료는 상호 협의하에 리크루터에게 발생할 수도 있습니다.)"
              />
            </p>
          </div>
          <table>
            <thead>
              <tr>
                <th>
                  <span>
                    <FormattedMessage id="selection" defaultMessage="선택" />
                  </span>
                </th>
                <th>
                  <span>
                    <FormattedMessage id="number" defaultMessage="번호" />
                  </span>
                </th>
                <th
                  className={
                    fltrState.recNm === "DESC"
                      ? "descending"
                      : fltrState.recNm === "ASC"
                      ? "ascending"
                      : undefined
                  }
                  onClick={() => {
                    changeFltr("recNm", fltrState.recNm);
                  }}
                >
                  <span>
                    <FormattedMessage
                      id="recruiter.name"
                      defaultMessage="리쿠르터 이름"
                    />
                  </span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th
                  className={
                    fltrState.cmpnyNm === "DESC"
                      ? "descending"
                      : fltrState.cmpnyNm === "ASC"
                      ? "ascending"
                      : undefined
                  }
                  onClick={() => {
                    changeFltr("cmpnyNm", fltrState.cmpnyNm);
                  }}
                >
                  <span>
                    <FormattedMessage
                      id="searchfirm.belong.to"
                      defaultMessage="소속 써치펌"
                    />
                  </span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th
                  className={
                    fltrState.email === "DESC"
                      ? "descending"
                      : fltrState.email === "ASC"
                      ? "ascending"
                      : undefined
                  }
                  onClick={() => {
                    changeFltr("email", fltrState.email);
                  }}
                >
                  <span>
                    <FormattedMessage id="email" defaultMessage="이메일" />
                  </span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
                <th
                  className={
                    fltrState.celpNum === "DESC"
                      ? "descending"
                      : fltrState.celpNum === "ASC"
                      ? "ascending"
                      : undefined
                  }
                  onClick={() => {
                    changeFltr("celpNum", fltrState.celpNum);
                  }}
                >
                  <span>
                    <FormattedMessage id="phone" defaultMessage="휴대폰" />
                  </span>
                  <div className="filterArrow">
                    <span />
                    <span />
                  </div>
                </th>
              </tr>
            </thead>
            <tbody>
              <tr className="noData" style={{ display: "none" }}>
                <td colSpan={6}>
                  <FormattedMessage
                    id="description.no.searchfirm"
                    defaultMessage="등록된 기존 거래 써치펌이 없습니다."
                  />
                </td>
              </tr>
              {listState.map((list, idx) => (
                <tr key={list.srchfirmUid}>
                  <td>
                    <input
                      type="checkbox"
                      onClick={(e) => {
                        onChangeChecked(e, idx);
                      }}
                    />
                  </td>
                  <td>{idx + 1}</td>
                  <td>{list.recNm}</td>
                  <td>{list.cmpnyNm}</td>
                  <td>{list.email}</td>
                  <td>{list.celpNum}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        <div className="flexAlignRight">
          <div>
            <button
              type="button"
              className="bigButton blueColored"
              onClick={openDetailModal}
            >
              추가하기
            </button>
            {
              <Modal
                visible={modalVisible1}
                closable={true}
                maskClosable={true}
                onClose={closeDetailModal}
                onSubmit={goCRUD}
                inputName="등록하기 >"
              >
                <h4>
                  <FormattedMessage
                    id="search.firm.address.register"
                    defaultMessage="기존 거래 써치펌 주소록 등록"
                  />
                </h4>
                <p className="textAlignCenter">
                  <FormattedMessage
                    id="description.search.firm.register"
                    values={{
                      breakLine: <br />,
                    }}
                    defaultMessage="주소록에는 최대 30명의 리크루터를 등록 할 수 있습니다.{breakLine}
                          등록된 리크루터는 채용공고 등록하기 화면에서{breakLine}
                          기존 거래 써치펌(리크루터)로 등록할 수 있습니다"
                  />
                </p>
                <form className="employersForm" onKeyDown={onKeyDownForm}>
                  <div className="inputWidthFull directionColumn">
                    <label htmlFor="recruiterName">
                      <FormattedMessage
                        id="recruiter.name"
                        defaultMessage="리쿠르터 이름"
                      />{" "}
                      <span>*</span>
                    </label>
                    <input
                      type="text"
                      id="recruiterName"
                      className="fullWidthInput"
                      placeholder={intl.formatMessage({
                        id: "placeholder.name",
                        defaultMessage: "이름을 입력하세요.",
                      })}
                      value={detailState.recNm}
                      onChange={(e) => {
                        setDetailState({
                          ...detailState,
                          recNm: e.target.value,
                        });
                      }}
                    />
                  </div>
                  <div className="inputWidthFull directionColumn">
                    <label htmlFor="searchFirm">
                      <FormattedMessage
                        id="searchfirm.belong.to"
                        defaultMessage="소속 써치펌"
                      />
                    </label>
                    <input
                      type="text"
                      id="searchFirm"
                      placeholder={intl.formatMessage({
                        id: "placeholder.search.firm.belong.to",
                        defaultMessage: "소속 써치펌을 입력해주세요",
                      })}
                      value={detailState.cmpnyNm}
                      onChange={(e) => {
                        setDetailState({
                          ...detailState,
                          cmpnyNm: e.target.value,
                        });
                      }}
                    />
                  </div>
                  <div className="inputWidthFull directionColumn">
                    <label htmlFor="email">
                      <FormattedMessage id="email" defaultMessage="이메일" />{" "}
                      <span>*</span>
                    </label>
                    <input
                      type="text"
                      id="email"
                      placeholder={intl.formatMessage({
                        id: "placeholder.email",
                        defaultMessage: "이메일을 입력해주세요.",
                      })}
                      value={detailState.email}
                      onChange={(e) => {
                        setDetailState({
                          ...detailState,
                          email: e.target.value,
                        });
                      }}
                    />
                  </div>
                  <div className="inputWidthFull directionColumn">
                    <label htmlFor="phone">
                      <FormattedMessage id="phone" defaultMessage="휴대폰" />
                    </label>
                    <input
                      type="text"
                      placeholder={intl.formatMessage({
                        id: "placeholder.phone.number",
                        defaultMessage: '"- "없이 숫자만 입력하세요.',
                      })}
                      value={detailState.celpNum}
                      onChange={(e) => {
                        setDetailState({
                          ...detailState,
                          celpNum: e.target.value,
                        });
                      }}
                    />
                  </div>
                </form>
              </Modal>
            }
          </div>
          <button
            type="button"
            className="bigButton greyColored"
            onClick={goDel}
          >
            <FormattedMessage id="delete" defaultMessage="삭제하기" />
          </button>
        </div>
      </section>
    </main>
  );
};
