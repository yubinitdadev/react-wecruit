
import * as commonUtil from 'commons/modules/commonUtil';
import * as defVal from 'commons/modules/defVal';
import { Context } from 'contexts';
import { UPD_MBRUID } from 'contexts/actionTypes';
import { useContext, useEffect, useRef, useState } from 'react';
import {Link, useLocation, useNavigate} from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { FormattedMessage, useIntl } from 'react-intl';

export const Search =() => {
  const intl = useIntl();

  const {
    state : {
      server,
      mbr
    },
    dispatch,
  } = useContext(Context);

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid")
    ,ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi")
  };
  

  const location = useLocation();
  const navigate = useNavigate();
  const moment = require('moment');
  let previewWindow = "";
  let previewImg = document.createElement("img");  
  const componentRef = useRef();

  
  const [mbrCnt, setMbrCnt] = useState({});
  const [listState, setListState] = useState([]);
  const [listCntState, setListCntState] = useState({});
  const [sContsKnd, setSContsKnd] = useState("");
  const [sConts, setSConts] = useState("");
  const [fltrState, setFltrState] = useState({});

  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(10);

  
  let locationState = {
    sContsKnd : (location.state.sContsKnd)?(location.state.sContsKnd):('')
    ,sConts : (location.state.sConts)?(location.state.sConts):('')
  }
  
  let prtnrChkCnt = 0;
  let ingChkCnt = 0;
  let cmmntChkCnt = 0;
  let endChkCnt = 0;

  useEffect(async () => {
    console.log("Page First Load EFFECT");
    
    if( locationState.sContsKnd == "" )locationState.sContsKnd="candidate"
    document.getElementById("sContsKnd").value = locationState.sContsKnd;
    
    document.getElementById("sConts").value = locationState.sConts;

    //setSContsKnd(locationState.sContsKnd);
    //setSConts(locationState.sConts);
    await initSetHandle();          
  },[])

  const initSetHandle = async () => {

    console.log("ssn.ssnMbrUid == " + ssn.ssnMbrUid)

    let goUrl = server.path + "/hire/hireempsrchlist"
    let data = {
      ssnMbrUid : ssn.ssnMbrUid,
      mbrUid : ssn.ssnMbrUid,
      refUid : ssn.ssnRefUid,
      mbrDivi : ssn.ssnMbrDivi,
      sContsKnd: (document.getElementById("sContsKnd").value)?(document.getElementById("sContsKnd").value):(''),
      sConts: (document.getElementById("sConts").value)?(document.getElementById("sConts").value):(''),
      hireStat : ""
    }

    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");
    let axiosResData = axiosRes.data.hireArr;
    let tmpAxiosResData = [];

    let ingCnt = 0;
    let cnfrmCnt = 0;
    let endCnt = 0;
    axiosResData.forEach((resData, idx)=>{
      resData["rcmndEndDttm"] = moment(resData.rcmndEndDttm).format("YYYY-MM-DD HH:mm");
      if( resData["rcmndDttm"] != "" )resData["rcmndDttm"] = moment(resData.rcmndDttm).format("YYYY-MM-DD HH:mm");      

      tmpAxiosResData.push(resData);
      if( resData.hireStat === "ing")ingCnt++;
      else if( resData.hireStat === "confirm")cnfrmCnt++;
      else if( resData.hireStat === "end")endCnt++;

    })

    setListState(tmpAxiosResData);

    var tmpCnt = {
      ingCnt : ingCnt,
      cnfrmCnt : cnfrmCnt,
      endCnt : endCnt,
    }
    setListCntState(tmpCnt);
  }

  const goSearch =() => {
    
    initSetHandle();
   }

  const onKeyUp =(e) => {
    if(e.key == "Enter")
    {
      goSearch();
    }
    return false;
   }

   function onKeyDown(keyEvent) {
    if ((keyEvent.charCode || keyEvent.keyCode) === 13) {
      keyEvent.preventDefault();
    }
  }
 
  return(
    <main className="dividedSections employersMain noBorder">
    <section className="recruitNoticeStats">
      <div>
        <p>
          <FormattedMessage
            id="posting.confirm"
            defaultMessage="검수중 채용공고"
          />
        </p>
        <Link to='/employers/confirm'>{listCntState.cnfrmCnt}</Link>
      </div>
      <div>
        <p>
          <FormattedMessage
            id="posting.ing"
            defaultMessage="진행중 채용공고"
          />
        </p>
        <Link to='/employers/ing'>{listCntState.ingCnt}</Link>
      </div>
      <div>
        <p>
          <FormattedMessage
            id="posting.done"
            defaultMessage="종료된 채용공고"
          />
        </p>
        <Link to='/employers/done'>{listCntState.endCnt}</Link>
      </div>
    </section>
    <section>
      <form className="searchForm employersForm">
        <select name="sContsKnd" id="sContsKnd">
          <option value>
            {intl.formatMessage({
              id: "candidate",
              defaultMessage: "후보자"
            })}
          </option>
          <option value>
            {intl.formatMessage({
              id: "recruiter",
              defaultMessage: "리크루터"
            })}
          </option>
        </select>
        <input 
          type="search" 
          placeholder={intl.formatMessage({
            id: "placeholder.name",
            defaultMessage: "이름을 입력하세요."
          })}

          onChange={(e) => {
            setSConts(e.target.value);
          }}
          
          onKeyUp={(e) => {
            onKeyUp(e);
          }}
        />

        <input 
          type="button" 
          value={intl.formatMessage({
            id: "search",
            defaultMessage: "검색"
          })}  
          className="basicButton" 

          onClick={(e) => {
            goSearch();
          }}
        />
      </form>
    </section>
    <section className="searchResult candidateResult">
      <p>
        <span><FormattedMessage
            id="candidate"
            defaultMessage="후보자"
          /></span> : 
          <strong>
            <FormattedMessage
              id="searched.word"
              defaultMessage="입력된 검색어"
            />
          </strong> <FormattedMessage
            id="searched.result"
            defaultMessage="검색결과"
          /> : <FormattedMessage
          id="total"
          defaultMessage="총"
        /> <strong>0</strong> <FormattedMessage
        id="case"
        defaultMessage="건"
      />
      </p>
      <table>
        <thead>
           
          <tr>
            <th>
              <FormattedMessage
                id="candidate"
                defaultMessage="후보자"
              />
            </th>
            <th>
              <FormattedMessage
                id="recruitment.title"
                defaultMessage="채용공고 제목"
              />
            </th>
            <th>
              <FormattedMessage
                id="recruiter"
                defaultMessage="리크루터"
              />
            </th>
            <th>
              <FormattedMessage
                id="recommend.date"
                defaultMessage="추천일시"
              />
            </th>
            <th>
              <FormattedMessage
                id="recommend.progress"
                defaultMessage="진행상태"
              />
            </th>
          </tr>
        </thead>
        <tbody className="noSearchResult" style={{display: 'none'}}>
          <tr>
            <td colSpan={5} align="center">
              <FormattedMessage
                id="description.no.search.result"
                defaultMessage="검색결과가 존재하지 않습니다."
              />
            </td>
          </tr>
        </tbody>
        <tbody>
          {
              listState.map((list, idx)=>
                (
                  (list.hireStat != 'tmp')?
                    (
                    <tr>
                      <td>{list.recMbrNm}</td>
                      <td className="tableTitle">
                        <Link to='/employers/candidate-manage'>{list.title}</Link>
                      </td>
                      <td>{list.cnddtNm}</td>
                      <td>{list.rcmndDttm}</td>
                      <td>
                        {
                          (list.hireStat == 'confirm')?('검수중'):
                          (list.hireStat == 'ing')?('진행중'):
                          (list.hireStat == 'end')?('종료'):
                          (list.hireStat == 'declined')?('거절'):                   
                          ('')
                        }
                      </td>
                    </tr>
                    ):('')
                )
              )
          }
        </tbody>
      </table>
    </section>
    <section className="searchResult recruiterResult" style={{display: 'none'}}>
      <p>
        <span>
          <FormattedMessage
            id="recruiter"
            defaultMessage="리크루터"
          />
        </span> : <strong>
          <FormattedMessage
            id="searched.word"
            defaultMessage="입력된 검색어"
          />
          </strong> <FormattedMessage
            id="searched.result"
            defaultMessage="검색결과"
          /> : <FormattedMessage
          id="total"
          defaultMessage="총"
          /> <strong>0</strong> <FormattedMessage
          id="case"
          defaultMessage="건"
        />
      </p>
      <table>
        <thead>
          <tr>
            <th>
              <FormattedMessage
                id="recruiter"
                defaultMessage="리크루터"
              />
            </th>
            <th>
              <FormattedMessage
                id="recruitment.title"
                defaultMessage="채용공고 제목"
              />
            </th>
            <th>
              <FormattedMessage
                id="candidate"
                defaultMessage="후보자"
              />
            </th>
            <th>
              <FormattedMessage
                id="recommend.date"
                defaultMessage="추천일시"
              />
            </th>
            <th>
              <FormattedMessage
                id="recommend.progress"
                defaultMessage="진행상태"
              />
            </th>
          </tr>
        </thead>
        <tbody className="noSearchResult" style={{display: 'none'}}>
          <tr>
            <td colSpan={5} align="center">
              <FormattedMessage
                id="description.no.search.result"
                defaultMessage="검색결과가 존재하지 않습니다."
              />
            </td>
          </tr>
        </tbody>
        <tbody>
          <tr>
            <td>리크루터 이름</td>
            <td className="tableTitle">
              <Link to='/employers/candidate-manage'>[대기업/금융/보험] 미국 변호사 경력채용 (과장~부장급)</Link>
            </td>
            <td>후보자 이름</td>
            <td>2021-05-21T13:45 23z</td>
            <td>모르는 상황(30)</td>
          </tr>
        </tbody>
      </table>
    </section>
  </main>
  );
}