import { useState } from "react";

const LocaleSelect = () => {
  const [locale, setLocale] = useState(localStorage.getItem("locale") ?? "ko");

  const handleLocale = (e) => {
    setLocale(e.target.value)
    localStorage.setItem("locale", e.target.value);
    window.location.reload();
  }

  return (
    <form className="localeSelect">
      <select
        id="locale"
        value={locale}
        onChange={(e) => handleLocale(e)}
      >
        <option value="en-US">En</option>
        <option value="ko">Ko</option>
      </select>
    </form>
  )
}

export default LocaleSelect