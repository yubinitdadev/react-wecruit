import {Link, useLocation, useNavigate} from 'react-router-dom';
import { useContext, useEffect, useState } from 'react';
import { Context } from 'contexts';
import * as defVal  from 'commons/modules/defVal';
import * as commonUtil from 'commons/modules/commonUtil';
import contactimg from '../images/contact.jpg';
import checker_banner from '../images/checker_banner.png';
import { FormattedMessage, useIntl } from "react-intl";
import '../scss/aside.scss';

export const Aside = () => {
  const intl = useIntl();
  let PROC_CHK = ""

  const {
    state : {
        server,
        mbr
    },
    dispatch,
  } = useContext(Context);

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid")
    ,ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi")
  };

  
  const location = useLocation();
  const navigate = useNavigate();

  const [state, setState] = useState({initSet:false});
  const [ssnState, setSsnState] = useState(defVal.setAxiosSsnState(null));
  let ssnMbrData = defVal.setAxiosSsnState(null);  

  useEffect(async () => {

    if( ssn.ssnMbrUid === "" || 
        ( ssn.ssnMbrDivi != "MBRDIVI-01" && ssn.ssnMbrDivi != "MBRDIVI-03") )
    {
      alert(intl.formatMessage({
        id: "wrong.access",
        defaultMessage: "정상적인 접속이 아닙니다."
      }));
      navigate("/");
    }

    console.log("Page First Load EFFECT");
    initSetHandle();          
  },[])
  //biz_logo_thmnl
  async function initSetHandle() {

    let goUrl = server.path + "/mbr/mbrdetail"
    let data = {
      ssnMbrUid : ssn.ssnMbrUid,
      ssnMbrDivi : ssn.ssnMbrDivi,
      mbrUid : ssn.ssnMbrUid,
    }

    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");

    if( axiosRes.data.bizLogoThmnl === "" )axiosRes.data.bizLogoThmnl = server.host+"/upload/mbr/noman01.png";
    else axiosRes.data.bizLogoThmnl = server.host+axiosRes.data.bizLogoThmnl;

    console.log(axiosRes.data.bizLogoThmnl)

    setSsnState(defVal.setAxiosSsnState(axiosRes.data));
    
  }

  return (
    <aside>
      <section className="leftLogo asidePadding">
        <img src={ssnState.bizLogoThmnl} alt="로고" />

        {/* 현업부서 아이디로 로그인시 화면 */}
        {/* <button>로고 등록하기</button> */}
      </section>

      {(ssnState.mbrDivi === 'MBRDIVI-01')?(
        <section className="recruitFormRegister">
          <Link to='/employers/job-posting'>
            <p>
              <FormattedMessage
                id="aside.paragraph1"
                defaultMessage="무료로 채용공고를 등록하세요"
              />
            </p>
              <FormattedMessage
                id="job.posting.submit"
                defaultMessage="채용공고 등록하기"
              />
              <span className="material-icons">
                keyboard_double_arrow_right
              </span>
          </Link>
        </section>
      ):('')
      }
      
      <a 
        href="https://checker.wecruitpro.com/projects"
        target="_blank"
        rel="noreferrer"
      >
        <img src={checker_banner} alt="자동화 레퍼런스 체크 솔루션 체커 서비스 바로가기" />
      </a>

      <nav>
        <Link to='/employers/notice'>
          <FormattedMessage
            id="nav.notice"
            defaultMessage="ㆍ공지사항"
          />
        </Link>

        <div className="subLinks">
          <Link to='/employers/confirm' className="lightblue">
            <FormattedMessage
              id="nav.confirm.posting"
              defaultMessage="ㆍ검수중 채용공고"
            />
          </Link>
          <Link to='/employers/ing' className="lightblue">
            <FormattedMessage
              id="nav.ing.posting"
              defaultMessage="ㆍ진행중 채용공고"
            />
          </Link>
          <Link to='/employers/done' className="lightblue">
            <FormattedMessage
              id="nav.done.posting"
              defaultMessage="ㆍ종료된 채용공고"
            />
          </Link>
          {(ssnState.mbrDivi === 'MBRDIVI-01')?(<Link to='/employers/payment'>
            <FormattedMessage
              id="nav.payment"
              defaultMessage="ㆍ비용 관리"
            />
          </Link>):('')}
          {(ssnState.mbrDivi === 'MBRDIVI-01')?(<Link to='/employers/mypage'>
            <FormattedMessage
              id="nav.mypage"
              defaultMessage="ㆍ마이페이지"
            />
          </Link>):('')}
          {(ssnState.mbrDivi === 'MBRDIVI-01')?(<Link to='/employers/search-firm'>
            <FormattedMessage
              id="nav.search.firm"
              defaultMessage="ㆍ기존 거래 써치펌 주소록"
            />
          </Link>):('')}
          {(ssnState.mbrDivi === 'MBRDIVI-01')?(<Link to='/employers/department'>
            <FormattedMessage
              id="nav.department"
              defaultMessage="ㆍ현업부서 아이디 관리"
            />
          </Link>):('')}
        </div>
      </nav>

      <article>
        <img src={contactimg} alt="위크루트 이용문의 전화번호 031-548-2310, 2311 이메일 hunters@wecruitcorp.com 영업시간 평일 10:00 ~ 18:00" />
      </article>
    </aside>
  );
}

