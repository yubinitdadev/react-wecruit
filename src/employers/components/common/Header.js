import { useNavigate } from "react-router-dom";
import wecruitLogo from "../images/logo.png";
import lightBulb from "../images/header-bulb-light-small.png";
import { Link } from "react-router-dom";

import "commons/scss/header-all.scss";
import { useContext } from "react";
import { Context } from "contexts";
import { toast, ToastContainer } from "react-toastify";
import { axiosCrudOnSubmit } from "commons/modules/commonUtil";
import LocaleSelect from "./LocaleSelect";
import { FormattedMessage, useIntl } from "react-intl";

export const Header = ({ logout }) => {
  const intl = useIntl();
  const navigate = useNavigate();

  const {
    state : {
        mbr,
        server
    },
    dispatch,
  } = useContext(Context);

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid")
    ,ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi")
    ,ssnMbrNm: window.localStorage.getItem("ssnMbrNm")
    ,ssnBizNm: window.localStorage.getItem("ssnBizNm")
  };

  const jsonCompResult = (resJson) => {
    if( resJson.resCd !== "0000" )
    {
      toast.error(intl.formatMessage({
        id: "error.fail.logout",
        defaultMessage: "로그아웃에 실패하였습니다."
      }));
      return;
    }
    else
    {
      navigate("/");
    }
  }

  async function handleLogout() {
    let goUrl = server.path + "/mbr/logout";
    let data = {
      ssnMbrUid : (window.localStorage.getItem("ssnMbrUid")) ? (window.localStorage.getItem("ssnMbrUid")):(''),
      mbrUid : (window.localStorage.getItem("ssnMbrUid")) ? (window.localStorage.getItem("ssnMbrUid")):(''),
      ssnMbrNm : (window.localStorage.getItem("ssnMbrNm")) ? (window.localStorage.getItem("ssnMbrNm")):(''), 
      ssnBizNm : (window.localStorage.getItem("ssnBizNm")) ? (window.localStorage.getItem("ssnBizNm")):('') 
    };

    window.localStorage.clear();

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    jsonCompResult(axiosRes.data);
  }

  return (
    <header className="employersHeader">
      <div className="headerWrap">
        <div>
          <Link to="/employers">
            <img src={wecruitLogo} alt="위크루트 로고" />
          </Link>
          <span>
            <img src={lightBulb} alt="전구 이미지" />
            활발한 피드백은 성공적인 인재 채용의 첫 걸음입니다.
          </span>
        </div>

        <div className="localeWrap">
          <LocaleSelect/>
  
          <div className="headerLogout">
            <span>
              {(ssn.ssnBizNm !== "" ) ? (ssn.ssnBizNm):(ssn.ssnMbrNm)}
            </span>
            <em
              onClick={() => {handleLogout();}}
              style={{ cursor: "pointer" }}
            >
              <FormattedMessage
                id="logout"
                defaultMessage="로그아웃"
              />
            </em>
          </div>
        </div>
      </div>
    </header>
  );
};
