import { useContext, useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import Modal from "modal-all/ModalAll";

// 모달창
import { AccessInfo } from "modal-all/modals/AccessInfo";

import { Context } from "contexts";
import * as commonUtil from "commons/modules/commonUtil";
import * as defVal from "commons/modules/defVal";
import { toast, ToastContainer } from "react-toastify";
import { FormattedMessage, useIntl } from "react-intl";

export const Department = () => {
  const intl = useIntl();

  let PROC_CHK = "";

  const {
    state: { mbr, server },
    dispatch,
  } = useContext(Context);

  const location = useLocation();
  const navigate = useNavigate();
  const moment = require("moment");
  let previewWindow = "";
  let previewImg = document.createElement("img");

  const [files, setFiles] = useState("");
  const [detailState, setDetailState] = useState({});
  const [listState, setListState] = useState([]);
  const [hireListState, setHireListState] = useState([]);

  const [mbrMappListState, setMbrMappListState] = useState([]);
  const [hireMappListState, setHireMappListState] = useState([]);

  const [fltrState, setFltrState] = useState({});

  const [mappMbrUid, setMappMbrUid] = useState("");

  const emailDomCd = defVal.EmailDomCd();
  const celpFCd = defVal.CelpFCd();

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid"),
    ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi"),
  };

  let locationState = {
    mbrUid: ssn.ssnMbrUid,
    mbrDivi: "MBRDIVI-03",
  };

  useEffect(async () => {
    console.log("Page First Load EFFECT");
    window.scrollTo(0, 0);
    initSetHandle();
  }, []);

  const reRender = () => {
    setDetailState({
      ...detailState,
      initSetCmplt: true,
    });
  };

  async function initSetHandle() {
    let goUrl = server.path + "/mbr/mydeprtmbrlist";
    let data = {
      mbrUid: locationState.mbrUid,
      mbrDivi: locationState.mbrDivi,
    };
    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");
    var tmpList = [];

    for (var k = 0; k < axiosRes.data.resArr.length; k++) {
      var tmpData = defVal.setAxiosMbrState(axiosRes.data.resArr[k]);
      if (tmpData.email.indexOf("@") > -1) {
        tmpData.emailId = tmpData.email.split("@")[0];
        tmpData.emailDom = tmpData.email.split("@")[1];
      }
      tmpList.push(tmpData);
    }

    setListState(tmpList);

    var tmpHireList = await getHireList();
    setHireListState(tmpHireList);

    var tmpHireMappList = await getHireMappList();
    setHireMappListState(tmpHireMappList);
  }

  const getHireList = async () => {
    let goUrl = server.path + "/hire/hirelist";
    let data = {
      serverPath: server.path,
      ssnMbrUid: ssn.ssnMbrUid,
      mbrUid: ssn.ssnMbrUid,
      mbrDivi: "MBRDIVI-01",
      hireStat: "ing",
      sConts: "",
    };
    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");
    let resArr = axiosRes.data.hireArr;

    var tmpList = [];
    for (var k = 0; k < resArr.length; k++) {
      var tmpData = defVal.setAxiosJobState(resArr[k]);
      tmpList.push(tmpData);
    }

    return tmpList;
  };

  const getHireMappList = async () => {
    let goUrl = server.path + "/hire/hiremapplist";
    let data = {
      serverPath: server.path,
      ssnMbrUid: ssn.ssnMbrUid,
      sMbrUid: ssn.ssnMbrUid,
      hireStat: "ing",
      sConts: "",
    };
    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");
    let resArr = axiosRes.data.resArr;

    var tmpList = [];
    for (var k = 0; k < resArr.length; k++) {
      var tmpData = defVal.setAxiosJobState(resArr[k]);
      tmpList.push(tmpData);
    }

    setMbrMappListState(axiosRes.data.resMbrArr);

    return tmpList;
  };

  ////////////////////////////////////////////////////////////////////
  const [authModal, setAuthModal] = useState(false);
  const [dprtMatchModal, setDprtMatchModal] = useState(false);
  const [detailModal, setDetailModal] = useState(false);
  const [modalVisible4, setModalVisible4] = useState(false);
  const [deleteModal, setDeleteModal] = useState(false);

  const openAuthModal = () => {
    setAuthModal(true);
  };

  const closeAuthModal = () => {
    setAuthModal(false);
  };

  const openDprtMatchModal = (uid) => {
    setMappMbrUid(uid);

    setDprtMatchModal(true);
  };

  const closeDprtMatchModal = () => {
    setDprtMatchModal(false);
  };

  const openDetailModal = (seq) => {
    //alert(seq);
    if (seq === -1) {
      setDetailState({
        ...detailState,
        mbrUid: "",
        mbrId: "",
        pswd: "",
        pswdConfirm: "",
        intro: "",
        email: "",
        emailId: "",
        emailDom: "",
        celpNum: "",
        mbrDivi: "MBRDIVI-03",
        refUid: locationState.mbrUid,
      });
    } else {
      setDetailState(listState[seq]);
    }

    setDetailModal(true);
  };

  const closeDetailModal = () => {
    setDetailModal(false);
  };

  const openModal4 = () => {
    setModalVisible4(true);
  };

  const openDeleteModal = () => {
    setDeleteModal(true);
  };

  const closeDeleteModal = () => {
    setDeleteModal(false);
  };

  const closeModal4 = () => {
    setModalVisible4(false);
  };

  /////////////////////////////////////////////////////////////////////////////////

  const chkeckAll = (e) => {
    var chkYn = "N";
    if (document.getElementById("selectAll").checked === true) {
      chkYn = "Y";
    }

    var tmpList = [];
    for (var k = 0; k < listState.length; k++) {
      listState[k].chkYn = chkYn;
      tmpList.push(listState[k]);
    }

    setListState(tmpList);
  };

  const chkeck = (e, seq) => {
    if (listState[seq].chkYn === "Y") listState[seq].chkYn = "N";
    else listState[seq].chkYn = "Y";

    var tmpList = [];
    for (var k = 0; k < listState.length; k++) {
      tmpList.push(listState[k]);
    }

    setListState(tmpList);
  };

  const onChngDetailVal = (e, key) => {
    if (key === "emailDomSample") {
      key = "emailDom";
    }

    setDetailState({
      ...detailState,
      [key]: e.target.value,
    });
  };

  const onChangeMatchDprtChk = (e, seq) => {
    var chkYn = "Y";
    if (hireListState[seq].chkYn === "Y") chkYn = "N";

    for (var k = 0; k < hireListState.length; k++) {
      hireListState[k].chkYn = "N";
    }

    hireListState[seq].chkYn = chkYn;
  };

  /////////////////////////////////////////////////////////////////////////////////

  const jsonCompResult = (resJson) => {
    if (resJson.resCd !== "0000") {
      toast.error(
        intl.formatMessage({
          id: "error.error.in.server",
          defaultMessage: "서버에 오류가 발생했습니다.",
        })
      );
      return;
    } else {
      if (PROC_CHK === "INS") {
        PROC_CHK = "";
        if (resJson.mbrUid === "DUPL") {
          toast.error(
            intl.formatMessage({
              id: "error.id.exist",
              defaultMessage: "이미 사용중인 아이디입니다.",
            })
          );
          return;
        }
        toast.info(
          intl.formatMessage({
            id: "info.department.id.created",
            defaultMessage: "현업부서 아이디가 생성되었습니다.",
          })
        );
        initSetHandle();
      } else if (PROC_CHK === "UPD") {
        PROC_CHK = "";
        toast.info(
          intl.formatMessage({
            id: "info.department.id.edited",
            defaultMessage: "현업부서 아이디 수정이 완료되었습니다.",
          })
        );
        initSetHandle();
      } else if (PROC_CHK === "DEL") {
        PROC_CHK = "";
        toast.info(
          intl.formatMessage({
            id: "info.department.id.deleted",
            defaultMessage: "선택한 현업부서 아이디가 삭제되었습니다.",
          })
        );
        //initSetHandle();
      } else if (PROC_CHK === "INSHIREMAPP") {
        PROC_CHK = "";
        toast.info(
          intl.formatMessage({
            id: "info.selected.id.matched",
            defaultMessage: "선택한 현업부서 아이디가 매칭되었습니다.",
          })
        );
        initSetHandle();
      } else if (PROC_CHK === "DELHIREMAPP") {
        PROC_CHK = "";
        toast.info(
          intl.formatMessage({
            id: "info.matching.info.deleted",
            defaultMessage: "매칭정보가 삭제되었습니다.",
          })
        );
        initSetHandle();
      } else if (PROC_CHK === "DUPL") {
        PROC_CHK = "";
        if (resJson.mbrUid === "DUPL")
          toast.error("이미 사용중인 아이디입니다.");
        else
          toast.info(
            intl.formatMessage({
              id: "info.usable.id",
              defaultMessage: "사용 가능한 아이디입니다.",
            })
          );
      }
    }
  };

  const goCRUD = () => {
    detailState.email = detailState.emailId + "@" + detailState.emailDom;

    if (
      commonUtil.CheckIsEmptyFromVal(
        detailState.mbrId,
        intl.formatMessage({
          id: "error.need.input.id",
          defaultMessage: "아이디를 입력해주세요.",
        })
      ) === false
    )
      return;
    if (
      commonUtil.CheckIsEmptyFromVal(
        detailState.pswd,
        intl.formatMessage({
          id: "error.need.input.password",
          defaultMessage: "비밀번호를 입력해주세요.",
        })
      ) === false
    )
      return;
    if (detailState.pswd !== document.getElementById("pswdConfirm").value) {
      toast.error(
        intl.formatMessage({
          id: "error.check.password",
          defaultMessage: "비밀번호를 확인해주세요.",
        })
      );
      return;
    }
    if (
      commonUtil.CheckIsEmptyFromVal(
        detailState.intro,
        intl.formatMessage({
          id: "error.need.input.department.description",
          defaultMessage: "현업부서 설명 메모를 입력해주세요.",
        })
      ) === false
    )
      return;
    if (
      commonUtil.CheckIsEmptyFromVal(
        detailState.emailId,
        intl.formatMessage({
          id: "error.need.input.email",
          defaultMessage: "이메일을 입력해주세요.",
        })
      ) === false
    )
      return;
    if (
      commonUtil.CheckIsEmptyFromVal(
        detailState.emailDom,
        intl.formatMessage({
          id: "error.need.input.email",
          defaultMessage: "이메일을 입력해주세요.",
        })
      ) === false
    )
      return;
    if (detailState.pswd != document.getElementById("pswdConfirm").value) {
      toast.error(
        intl.formatMessage({
          id: "error.check.password",
          defaultMessage: "비밀번호를 확인해주세요.",
        })
      );
      return;
    }

    if (detailState.mbrUid === "") goIns();
    else goUpd();

    closeDetailModal();
  };

  async function goIns() {
    PROC_CHK = "INS";

    detailState.refUid = ssn.ssnMbrUid;

    let goUrl = server.path + "/mbr/mbrinsert";
    let axiosRes = await commonUtil.axiosCrudOnSubmit(
      detailState,
      goUrl,
      "POST"
    );

    jsonCompResult(axiosRes.data);
  }

  async function goUpd() {
    PROC_CHK = "UPD";

    let goUrl = server.path + "/mbr/mbrupdate";
    //alert(detailState.pswd)
    //return
    let axiosRes = await commonUtil.axiosCrudOnSubmit(
      detailState,
      goUrl,
      "POST"
    );

    jsonCompResult(axiosRes.data);
  }

  const goDel = async () => {
    PROC_CHK = "DEL";

    var tmpList = [];
    for (var k = 0; k < listState.length; k++) {
      if (listState[k].chkYn === "Y") {
        tmpList.push(listState[k].mbrUid);
      }
    }

    closeDeleteModal();

    if (tmpList.length === 0) {
      toast.error(
        intl.formatMessage({
          id: "error.select.id.to.delete",
          defaultMessage: "삭제할 현업부서 아이디를 선택해 주세요.",
        })
      );
      return;
    }

    let goUrl = server.path + "/mbr/dprtmbrdelete";
    let data = {
      mbrDivi: "MBRDIVI-03",
      dprtMbrUid: tmpList,
      mbrUid: ssn.ssnMbrUid,
    };

    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");

    jsonCompResult(axiosRes.data);
  };

  const goHireMappCRUD = async () => {
    PROC_CHK = "INSHIREMAPP";

    var hireUid = "";
    for (var k = 0; k < hireListState.length; k++) {
      if (hireListState[k].chkYn === "Y") hireUid = hireListState[k].hireUid;
    }

    let goUrl = server.path + "/hire/hiremappinsert";
    let data = {
      hireUid: hireUid,
      mbrUid: mappMbrUid,
    };

    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");

    jsonCompResult(axiosRes.data);

    closeDprtMatchModal();
  };

  const goHireMappDel = async (mbrUid, hireUid) => {
    PROC_CHK = "DELHIREMAPP";

    let goUrl = server.path + "/hire/hiremappdelete";
    let data = {
      hireUid: hireUid,
      mbrUid: mbrUid,
    };

    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");

    jsonCompResult(axiosRes.data);
  };

  const goDupl = async () => {
    PROC_CHK = "DUPL";

    let goUrl = server.path + "/mbr/mbrdupl";
    let data = {
      mbrId: detailState.mbrId,
    };

    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");

    jsonCompResult(axiosRes.data);
  };

  return (
    <main className="employersMain">
      <ToastContainer />
      <section>
        <div className="idManagement">
          <h3 className="sectionTitle">
            <span>
              <FormattedMessage
                id="department.header"
                defaultMessage="현업부서 아이디 관리"
              />
            </span>
          </h3>
          <div className="idTableWrap">
            <div>
              <div>
                <input
                  type="checkbox"
                  id="selectAll"
                  name="selectAll"
                  className="marginInline"
                  value="N"
                  onClick={(e) => {
                    chkeckAll(e);
                  }}
                />
                <label htmlFor="selectAll">
                  <FormattedMessage id="select.all" defaultMessage="전체선택" />
                </label>
                <div className="flexAlignRight">
                  <button
                    type="button"
                    className="modalOpenButton"
                    onClick={openAuthModal}
                  >
                    <FormattedMessage
                      id="department.access.info"
                      defaultMessage="권한정보"
                    />
                  </button>
                </div>
              </div>
              <table className="idTable">
                <tbody>
                  {listState.map((list, idx) => (
                    <tr key={list.mbrUid}>
                      <td>
                        <input
                          type="checkbox"
                          checked={list.chkYn === "Y" ? true : false}
                          onClick={(e) => {
                            chkeck(e, idx);
                          }}
                        />
                      </td>
                      <td>{list.mbrId}</td>
                      <td>{list.intro}</td>
                      <td>
                        <div>
                          <button
                            type="button"
                            className="modalOpenButton"
                            onClick={(e) => {
                              openDprtMatchModal(list.mbrUid);
                            }}
                          >
                            <FormattedMessage
                              id="department.matching.toggle"
                              defaultMessage="현업부서 매칭/취소"
                            />
                          </button>
                        </div>
                      </td>
                      <td>
                        <div>
                          <button
                            type="button"
                            className="modalOpenButton"
                            onClick={() => {
                              openDetailModal(idx);
                            }}
                          >
                            <FormattedMessage
                              id="department.edit.info"
                              defaultMessage="정보수정"
                            />
                          </button>
                        </div>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div className="flexAlignRight idButtons">
          <div>
            <button
              type="button"
              className="bigButton blueColored"
              onClick={() => {
                openDetailModal(-1);
              }}
            >
              <FormattedMessage
                id="department.generate.id"
                defaultMessage="아이디 생성 (+)"
              />
            </button>
          </div>
          <button
            type="button"
            className="bigButton redColored"
            onClick={openDeleteModal}
          >
            <FormattedMessage
              id="department.remove.id"
              defaultMessage="아이디 삭제 (-)"
            />
          </button>
        </div>
        <div className="idManagement">
          <h3 className="sectionTitle">
            <span>
              <FormattedMessage
                id="department.matching.status"
                defaultMessage="채용공고 매칭 현황"
              />
            </span>
          </h3>

          {mbrMappListState.map((list, idx) => (
            <div className="matchingLists">
              <div>
                <b>아이디테스트</b>{" "}
                <span>
                  <FormattedMessage id="id" defaultMessage="아이디" />:{" "}
                  {list.mbrId}
                </span>
              </div>
              <ul>
                {hireMappListState.map((mapplist, hireidx) =>
                  list.mbrId === mapplist.mbrId ? (
                    <li>
                      {mapplist.title}
                      <span
                        className="material-icons deleteButton"
                        onClick={() => {
                          goHireMappDel(mapplist.mbrUid, mapplist.hireUid);
                        }}
                      >
                        close
                      </span>
                    </li>
                  ) : (
                    ""
                  )
                )}
              </ul>
            </div>
          ))}
        </div>
      </section>

      {
        <Modal
          visible={authModal}
          closable={true}
          maskClosable={true}
          onClose={closeAuthModal}
          nobutton={true}
        >
          <AccessInfo />
        </Modal>
      }

      {
        <Modal
          visible={dprtMatchModal}
          closable={true}
          maskClosable={true}
          onSubmit={() => {
            goHireMappCRUD();
          }}
          onClose={closeDprtMatchModal}
          inputName={
            <FormattedMessage
              id="department.matching"
              defaultMessage="매칭하기"
            />
          }
          closeName={
            <FormattedMessage
              id="department.matching.cancel"
              defaultMessage="매칭취소"
            />
          }
        >
          <>
            <h4>
              <FormattedMessage
                id="department.matching.toggle"
                defaultMessage="현업부서 매칭/취소"
              />
            </h4>
            <div className="matchingList">
              {hireListState.map((list, idx) => {
                return (
                  <div>
                    <input
                      type="radio"
                      name="departmentItem"
                      onClick={(e) => {
                        onChangeMatchDprtChk(e, idx);
                      }}
                    />
                    <label>{list.title}</label>
                  </div>
                );
              })}
            </div>
          </>
        </Modal>
      }

      {
        <Modal
          visible={detailModal}
          closable={true}
          maskClosable={true}
          onSubmit={() => {
            goCRUD();
          }}
          onClose={closeDetailModal}
          inputName={detailState.mbrUid === "" ? "등록하기 >" : "수정하기 >"}
        >
          <h4>
            <FormattedMessage
              id="department.id"
              defaultMessage="현업부서 아이디"
            />
            {detailState.mbrUid === ""
              ? `${intl.formatMessage({
                  id: "create",
                  defaultMessage: "생성",
                })}`
              : `${intl.formatMessage({
                  id: "update",
                  defaultMessage: "수정",
                })}`}
          </h4>
          <p>
            <span className="red">*</span>
            <FormattedMessage
              id="required"
              defaultMessage="는 필수 항목입니다."
            />
          </p>

          <form className="employersForm">
            <div>
              <label htmlFor="departmentId">
                <span>*</span>
                <FormattedMessage id="id" defaultMessage="아이디" />
              </label>
              <div className="flex">
                <input
                  type="text"
                  id="mbrId"
                  name="mbrId"
                  placeholder="아이디를 입력해주세요."
                  value={detailState.mbrId}
                  onChange={(e) => {
                    onChngDetailVal(e, "mbrId");
                  }}
                />
                <button
                  type="button"
                  className="modalButtonStyling"
                  onClick={goDupl}
                >
                  <FormattedMessage
                    id="check.duplicate"
                    defaultMessage="중복 확인"
                  />
                </button>
              </div>
            </div>
            <div className="inputWidthFull">
              <label htmlFor="departmentPw">
                <span>*</span>
                <FormattedMessage
                  id="pw"
                  defaultMessage={`
                  ${intl.formatMessage({
                    id: "pw",
                    defaultMessage: "비밀번호",
                  })}
                `}
                />
              </label>
              <input
                type="password"
                id="pswd"
                name="pswd"
                placeholder={`
                ${intl.formatMessage({
                  id: "placeholder.pw",
                  defaultMessage: "비밀번호를 입력해주세요.",
                })}
              `}
                value={detailState.pswd}
                onChange={(e) => {
                  onChngDetailVal(e, "pswd");
                }}
              />
            </div>
            <div className="inputWidthFull">
              <label htmlFor="departmentRePw">
                <span>*</span>
                <FormattedMessage id="re.pw" defaultMessage="비밀번호 재확인" />
              </label>
              <input
                type="password"
                id="pswdConfirm"
                name="pswdConfirm"
                placeholder={`
                ${intl.formatMessage({
                  id: "placeholder.re.pw",
                  defaultMessage: "비밀번호를 한번 더 입력해주세요.",
                })}
              `}
                value={detailState.pswdConfirm}
                onChange={(e) => {
                  onChngDetailVal(e, "pswdConfirm");
                }}
              />
            </div>
            <div>
              <label htmlFor="memo">
                <span>*</span>
                <FormattedMessage
                  id="department.description.memo"
                  defaultMessage="현업부서 설명 메모"
                />
              </label>
              <textarea
                id="intro"
                name="intro"
                cols={30}
                rows={10}
                placeholder={`
                ${intl.formatMessage({
                  id: "department.description.memo.placeholder",
                  defaultMessage: "예) 연구개발팀 홍길동 과장",
                })}
              `}
                value={detailState.intro}
                onChange={(e) => {
                  onChngDetailVal(e, "intro");
                }}
              />
            </div>
            <div>
              <label htmlFor="email">
                <span>*</span>
                <FormattedMessage id="email" defaultMessage="이메일 주소" />
              </label>
              <input
                type="text"
                id="emailId"
                name="emailId"
                value={detailState.emailId}
                onChange={(e) => {
                  onChngDetailVal(e, "emailId");
                }}
              />
              @
              <input
                type="text"
                id="emailDom"
                name="emailDom"
                value={detailState.emailDom}
                onChange={(e) => {
                  onChngDetailVal(e, "emailDom");
                }}
              />
              <select
                name
                id="emailAddress"
                value={detailState.emailDomSample}
                onChange={(e) => {
                  onChngDetailVal(e, "emailDomSample");
                }}
              >
                <option value="">
                  {intl.formatMessage({
                    id: "directly.input",
                    defaultMessage: "직접입력",
                  })}
                </option>
                {emailDomCd.map((type, idx1) => (
                  <option value={type.cateCd}>{type.cdName}</option>
                ))}
              </select>
            </div>
            <div>
              <label htmlFor="phone">
                <FormattedMessage
                  id="phone.number"
                  defaultMessage="휴대폰 번호"
                />
              </label>
              <div className="flex">
                <input
                  type="text"
                  id="celpNum"
                  name="celpNum"
                  value={detailState.celpNum}
                  onChange={(e) => {
                    onChngDetailVal(e, "celpNum");
                  }}
                />
              </div>
            </div>
          </form>
        </Modal>
      }

      {
        <Modal
          visible={deleteModal}
          closable={true}
          maskClosable={true}
          onSubmit={goDel}
          onClose={closeDeleteModal}
          inputName={`
          ${intl.formatMessage({
            id: "delete.warning",
            defaultMessage: "삭제하시겠습니까?",
          })}
        `}
          closeName={`
          ${intl.formatMessage({
            id: "no",
            defaultMessage: "아니오",
          })}
        `}
        >
          <h4>
            <FormattedMessage
              id="department.id.delete"
              defaultMessage="아이디 삭제 확인"
            />
          </h4>
          <div className="alertBox">
            <FormattedMessage id="id" defaultMessage="아이디" />: &nbsp;
            {listState.map((list, idx) =>
              list.chkYn === "Y" && idx === 0
                ? list.mbrId
                : list.chkYn === "Y" && idx > 0
                ? ", " + list.mbrId
                : ""
            )}
          </div>
        </Modal>
      }
    </main>
  );
};
