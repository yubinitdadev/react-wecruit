import { Link, useLocation, useNavigate } from "react-router-dom";
import { useContext, useEffect, useState } from "react";
import { Context } from "contexts";
import { toast, ToastContainer } from "react-toastify";
import axios from "axios";
import fileDownload from "js-file-download";
import Modal from "modal-all/ModalAll";
import DoubleModal from "modal-all/DoubleModal";
import { AccessInfo } from "modal-all/modals/AccessInfo";

import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import { RecommendProgram } from "modal-all/modals/RecommendProgram";

import { axiosCrudOnSubmit } from "commons/modules/commonUtil";
import * as commonUtil from "commons/modules/commonUtil";
import { snglFileUpload } from "commons/modules/multipartUtil";
import * as defVal from "commons/modules/defVal";
import { AddressInput } from "commons/components/AddressInput";
import { ImagePreview } from "commons/components/inputs/image-preview/ImagePreview";
import { TagTemplate } from "commons/components/tags/TagTemplate";
import BusinessTypeTemplate from "commons/components/inputs/business-type/BusinessTypeTemplate";
import WorkPlaceTemplate from "commons/components/inputs/work-place/WorkPlaceTemplate";
import WorkPositionTemplate from "commons/components/inputs/work-position/WorkPositionTemplate";
import { UPD_MBRUID } from "contexts/actionTypes";

import EditorComponent from "commons/components/editor/EditorComponent";
import { defineMessages, FormattedMessage, useIntl } from "react-intl";
import { messages } from "lang/DefineMessages";

export const JobPosting = () => {
  const intl = useIntl();

  let PROC_CHK = "";

  const {
    state: { server, mbr },
    dispatch,
  } = useContext(Context);

  const location = useLocation();
  const navigate = useNavigate();
  const moment = require("moment");
  let previewWindow = "";
  let previewImg = document.createElement("img");

  const bizKndCd = defVal.BizKndCd();
  const salesSizeCd = defVal.SalesSizeCd();
  const empSizeCd = defVal.EmpSizeCd();
  const hireSizeCd = defVal.HireSizeCd();
  const bizGuidDivi = defVal.BizGuidDivi();
  const bizGuidKnd = defVal.BizGuidKnd();

  const [listState, setListState] = useState([]);
  const [bizGuidListState, setBizGuidListState] = useState([]);
  const [guid01Cnt, setGuid01Cnt] = useState(0);
  const [guid02Cnt, setGuid02Cnt] = useState(0);
  const [detailState, setDetailState] = useState(defVal.setAxiosJobState(null));

  const [bizCateCdList, setBizCateCdList] = useState([]);
  const [bizCateCdCnt, setBizCateCdCnt] = useState([]);
  const [empAddrCateCdList, setEmpAddrCateCdList] = useState([]);
  const [empAddrCateCdCnt, setEmpAddrCateCdCnt] = useState([]);
  const [workPosCateCdList, setWorkPosCateCdList] = useState([]);
  const [workPosCateCdCnt, setWorkPosCateCdCnt] = useState([]);
  const [searchFirmList, setSearchFirmList] = useState([]);
  const [searchFirm, setSearchFirm] = useState(
    defVal.setAxiosSerachFirmState(null)
  );
  const [myDprtMbrlist, setMyDprtMbrList] = useState([]);

  const [bizCate, setBizCate] = useState([]);
  const [empAddrCate, setEmpAddrCate] = useState([]);
  const [workPosCate, setWorkPosCate] = useState([]);

  const [bizCateCdCombo, setBizCateCdCombo] = useState([
    defVal.SelCateCdCombo(),
  ]);
  const [empAddrCateCdCombo, setEmpAddrCateCdCombo] = useState([
    defVal.SelCateCdCombo(),
  ]);
  const [workPosCateCdCombo, setWorkPosCateCdCombo] = useState([
    defVal.SelCateCdCombo(),
  ]);

  const [fileName, setFileName] = useState(null);
  const [titleLength, setTitleLength] = useState("0");
  const [untilDone, setUntilDone] = useState(false);
  const [bestRecModal, setBestRecModal] = useState(false);
  const [serchFirmRegModal, setSerchFirmRegModal] = useState(false);
  const [serchFirmListModal, setSerchFirmListModal] = useState(false);
  const [serchFirmModal, setSerchFirmModal] = useState(false);

  const [dprtMatchModal, setDprtMatchModal] = useState(false);
  const [authModal, setAuthModal] = useState(false);

  const [srchFrimFltrState, setSrchFrimFltrState] = useState({});

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid"),
    ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi"),
    ssnBizNm: window.localStorage.getItem("ssnBizNm"),
  };

  let locationState = {
    mbrUid: ssn.ssnMbrUid,
    mbrDivi: "MBRDIVI-01",
  };

  let atchFiles;

  useEffect(async () => {
    console.log("Page First Load EFFECT");
    initSetHandle();
  }, []);

  const reRender = () => {
    setDetailState({
      ...detailState,
      initSetCmplt: true,
    });
  };

  async function initSetHandle() {
    detailState.ssnMbrUid = ssn.ssnMbrUid;

    let goUrl = server.path + "/hire/hiredetail";
    let data = {
      serverPath: server.path,
      ssnMbrUid: ssn.ssnMbrUid,
      ssnMbrDivi: ssn.ssnMbrDivi,
      mbrUid: locationState.mbrUid,
      mbrDivi: locationState.mbrDivi,
      hireUid: "",
      sFltrCate: "",
      sFltrCont: "",
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    let tagstr = axiosRes.data.tags;
    console.log("axiosRes.data tagstr == " + tagstr);
    if (tagstr) {
      var tags = defVal.SplitToArray(tagstr, "<;:>");
      axiosRes.data.tags = tags;
    }


    axiosRes.data.thmnlNm = "";
    axiosRes.data.thmnlNm2 = "";
    axiosRes.data.resumeFormThmnlNm = "";

    let atchFilesStr = axiosRes.data.atchFiles;
    if (atchFilesStr) {
      try {
        atchFiles = JSON.parse(atchFilesStr);
        for (var k = 0; k < atchFiles.length; k++) {
          if (atchFiles[k].filePos == 1)
            axiosRes.data.thmnlNm = atchFiles[k].orignlFileNm;
          else if (atchFiles[k].filePos == 2)
            axiosRes.data.thmnlNm2 = atchFiles[k].orignlFileNm;
          else if (atchFiles[k].filePos == 3)
            axiosRes.data.resumeFormThmnlNm = atchFiles[k].orignlFileNm;
        }
      } catch (e) {}
    }
    if (axiosRes.data.hireUid) setDetailState(axiosRes.data);

    var tmpBizCateCdList = await defVal.getBizCateCdList(data);
    setBizCateCdList(tmpBizCateCdList);

    var tmpEmpAddrCateCdList = await defVal.getEmpAddrCateCdList(data);
    setEmpAddrCateCdList(tmpEmpAddrCateCdList);

    var tmpWorkPosCateCdList = await defVal.getWorkPosCateCdList(data);
    setWorkPosCateCdList(tmpWorkPosCateCdList);

    data.sFltrCate = "bizNm";
    data.sFltrCont = "ASC";
    var tmpSearchFirmList = await defVal.getSearchFirmList(data);
    setSearchFirmList(tmpSearchFirmList);

    var tmpMyDprtMbrList = await defVal.getMyDprtMbrList(data);
    setMyDprtMbrList(tmpMyDprtMbrList);

    var tmpHireList = await getHireList();
    setListState(tmpHireList);

    
    //일반모집요강
    var tmpBizGuidList = await getBizGuidList(locationState.mbrUid);
  }

  const getHireList = async () => {
    let goUrl = server.path + "/hire/hirelist";

    let data = {
      ssnMbrUid: ssn.ssnMbrUid,
      mbrUid: ssn.ssnMbrUid,
      refUid: ssn.ssnRefUid,
      mbrDivi: ssn.ssnMbrDivi,
      hireStat: "",
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");

    let axiosResData = axiosRes.data.hireArr;
    //let tmpAxiosResData = [];

    return axiosResData;
  };

  const openBestRecModal = () => {
    setBestRecModal(true);
  };
  const closeBestRecModal = () => {
    setBestRecModal(false);
  };

  const openSerchFirmRegModal = () => {
    setSerchFirmRegModal(true);
  };
  const closeSerchFirmRegModal = () => {
    setSerchFirmRegModal(false);
  };

  const openSerchFirmListModal = () => {
    setSerchFirmListModal(true);
  };
  const closeSerchFirmListModal = () => {
    setSerchFirmListModal(false);
  };

  const openSerchFirmModal = () => {
    searchFirm.recNm = "";
    searchFirm.cmpnyNm = "";
    searchFirm.email = "";
    searchFirm.celpNum = "";

    setSerchFirmModal(true);
  };
  const closeSerchFirmModal = () => {
    setSerchFirmModal(false);
  };

  const openDprtMatchModal = () => {
    setDprtMatchModal(true);
  };

  const closeDprtMatchModal = () => {
    setDprtMatchModal(false);
  };

  const openAuthModal = () => {
    setAuthModal(true);
  };

  const closeAuthModal = () => {
    setAuthModal(false);
  };

  let fileChangedHandler = (e) => {
    var files = e.target.files;
    console.log(files);
    var filesArray = [].slice.call(files);
    filesArray.forEach((e) => {
      setFileName(e.name);
    });
  };

  ///////////////////////////////////////////////////////////////////////////
  const onChangeVal = (e, key) => {
    if (key == "hireKnd") {
      var hireKnd = "";
      if (document.getElementById("permanent").checked == true)
        hireKnd = "fullTime";
      if (document.getElementById("contract").checked == true) {
        if (hireKnd == "") hireKnd = "cntrct";
        else hireKnd = hireKnd + "," + "cntrct";
      }

      setDetailState({
        ...detailState,
        [key]: hireKnd,
      });
    } else {
      //

      setDetailState({
        ...detailState,
        [key]: e.target.value,
      });
    }
  };

  async function changeFltrSrchFrim(key, fltr) {
    let tmpFltr = fltr === "ASC" ? "DESC" : fltr === "DESC" ? "ASC" : "ASC";

    setSrchFrimFltrState({ [key]: tmpFltr });

    let data = {
      serverPath: server.path,
      ssnMbrUid: ssn.ssnMbrUid,
      ssnMbrDivi: ssn.ssnMbrDivi,
      mbrUid: locationState.mbrUid,
      mbrDivi: locationState.mbrDivi,
      sFltrCate: key,
      sFltrCont: tmpFltr,
    };

    var tmpSearchFirmList = await defVal.getSearchFirmList(data);
    setSearchFirmList(tmpSearchFirmList);
  }

  ////////////////////////////////////////////////////////////////////////////

  const addBizCate = () => {
    if (bizCateCdCombo.length == 5) {
      alert(
        intl.formatMessage({
          id: "job.type.max5",
          defaultMessage: "업종구분은 5개까지 입력 가능합니다!",
        })
      );
      return false;
    }

    var data = defVal.SelCateCdCombo();
    bizCateCdCombo.push(data);

    if (!detailState.bizCateCd1 || detailState.bizCateCd1.length == 0) {
      detailState.bizCateCd1.push("NONE");
      detailState.bizCateCd2.push("NONE");
      detailState.bizCateCd3.push("NONE");
    }

    detailState.bizCateCd1.push("NONE");
    detailState.bizCateCd2.push("NONE");
    detailState.bizCateCd3.push("NONE");

    reRender();
  };

  const rmvBizCate = (rowidx) => {
    var tmpList = commonUtil.rmvArrIdx(bizCateCdCombo, rowidx);
    setBizCateCdCombo(tmpList);

    var tmpList1 = commonUtil.rmvArrIdx(detailState.bizCateCd1, rowidx);
    var tmpList2 = commonUtil.rmvArrIdx(detailState.bizCateCd2, rowidx);
    var tmpList3 = commonUtil.rmvArrIdx(detailState.bizCateCd3, rowidx);

    setDetailState({
      ...detailState,
      bizCateCd1: tmpList1,
      bizCateCd2: tmpList2,
      bizCateCd3: tmpList3,
    });
  };

  const chngBizCate = (rowidx, val, lvl) => {
    var chkLvl = lvl + 1;

    var tmpList1 = detailState.bizCateCd1;
    var tmpList2 = detailState.bizCateCd2;
    var tmpList3 = detailState.bizCateCd3;

    var tmpComboList1 = bizCateCdCombo[rowidx].cateCdList1;
    var tmpComboList2 = bizCateCdCombo[rowidx].cateCdList2;
    var tmpComboList3 = bizCateCdCombo[rowidx].cateCdList3;

    if (lvl <= 1) {
      tmpList2[rowidx] = "";
      tmpComboList2 = "";
    }
    if (lvl <= 2) {
      tmpList3[rowidx] = "";
      tmpComboList3 = "";
    }

    if (lvl == 1) tmpList1[rowidx] = val;
    else if (lvl == 2) tmpList2[rowidx] = val;
    else if (lvl == 3) tmpList3[rowidx] = val;

    const cateArray = [];
    for (var k = 0; k < bizCateCdList.length; k++) {
      if (bizCateCdList[k].lvl == chkLvl && bizCateCdList[k].upCd == val) {
        cateArray.push({
          lvl: bizCateCdList[k].lvl,
          cateCd: bizCateCdList[k].cateCd,
          cdName: bizCateCdList[k].cdName,
        });
      }
    }

    if (chkLvl == 2) {
      tmpComboList2 = cateArray;
      bizCateCdCombo[rowidx].cateCdList2 = tmpComboList2;
    } else if (chkLvl == 3) {
      tmpComboList3 = cateArray;
      bizCateCdCombo[rowidx].cateCdList3 = tmpComboList3;
    }

    setDetailState({
      ...detailState,
      bizCateCd1: tmpList1,
      bizCateCd2: tmpList2,
      bizCateCd3: tmpList3,
    });
  };

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  const addEmpAddrCate = () => {
    if (empAddrCateCdCombo.length == 5) {
      alert(
        intl.formatMessage({
          id: "workplace.max5",
          defaultMessage: "근무예정지는 5개까지 입력 가능합니다!",
        })
      );
      return false;
    }

    var data = defVal.SelCateCdCombo();
    empAddrCateCdCombo.push(data);

    if (detailState.empAddrCateCd1.length == 0) {
      detailState.empAddrCateCd1.push("NONE");
      detailState.empAddrCateCd2.push("NONE");
    }

    detailState.empAddrCateCd1.push("NONE");
    detailState.empAddrCateCd2.push("NONE");

    reRender();
  };

  const rmvEmpAddrCate = (rowidx) => {
    var tmpList = commonUtil.rmvArrIdx(empAddrCateCdCombo, rowidx);
    setEmpAddrCateCdCombo(tmpList);

    var tmpList1 = commonUtil.rmvArrIdx(detailState.empAddrCateCd1, rowidx);
    var tmpList2 = commonUtil.rmvArrIdx(detailState.empAddrCateCd2, rowidx);

    setDetailState({
      ...detailState,
      empAddrCateCd1: tmpList1,
      empAddrCateCd2: tmpList2,
    });
  };

  const chngEmpAddrCate = (rowidx, val, lvl) => {
    var chkLvl = lvl + 1;

    var tmpList1 = detailState.empAddrCateCd1;
    var tmpList2 = detailState.empAddrCateCd2;

    var tmpComboList1 = empAddrCateCdCombo[rowidx].cateCdList1;
    var tmpComboList2 = empAddrCateCdCombo[rowidx].cateCdList2;

    if (lvl <= 1) {
      tmpList2[rowidx] = "";
      tmpComboList2 = "";
    }

    if (lvl == 1) tmpList1[rowidx] = val;
    else if (lvl == 2) tmpList2[rowidx] = val;

    const cateArray = [];
    for (var k = 0; k < empAddrCateCdList.length; k++) {
      if (
        empAddrCateCdList[k].lvl == chkLvl &&
        empAddrCateCdList[k].upCd == val
      ) {
        cateArray.push({
          lvl: empAddrCateCdList[k].lvl,
          cateCd: empAddrCateCdList[k].cateCd,
          cdName: empAddrCateCdList[k].cdName,
        });
      }
    }

    if (chkLvl == 2) {
      tmpComboList2 = cateArray;
      empAddrCateCdCombo[rowidx].cateCdList2 = tmpComboList2;
    }

    setDetailState({
      ...detailState,
      empAddrCateCd1: tmpList1,
      empAddrCateCd2: tmpList2,
    });
  };

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  const addWorkPosCate = () => {
    if (workPosCateCdCombo.length == 5) {
      alert(
        intl.formatMessage({
          id: "workplace.max5",
          defaultMessage: "직종별구분은 5개까지 입력 가능합니다!",
        })
      );
      return false;
    }

    var data = defVal.SelCateCdCombo();
    workPosCateCdCombo.push(data);

    if (detailState.workPosCateCd1.length == 0) {
      detailState.workPosCateCd1.push("NONE");
      detailState.workPosCateCd2.push("NONE");
      detailState.workPosCateCd3.push("NONE");
    }

    detailState.workPosCateCd1.push("NONE");
    detailState.workPosCateCd2.push("NONE");
    detailState.workPosCateCd3.push("NONE");

    reRender();
  };

  const rmvWorkPosCate = (rowidx) => {
    var tmpList = commonUtil.rmvArrIdx(workPosCateCdCombo, rowidx);
    setWorkPosCateCdCombo(tmpList);

    var tmpList1 = commonUtil.rmvArrIdx(detailState.workPosCateCd1, rowidx);
    var tmpList2 = commonUtil.rmvArrIdx(detailState.workPosCateCd2, rowidx);
    var tmpList3 = commonUtil.rmvArrIdx(detailState.workPosCateCd3, rowidx);

    setDetailState({
      ...detailState,
      workPosCateCd1: tmpList1,
      workPosCateCd2: tmpList2,
      workPosCateCd3: tmpList3,
    });
  };

  const chngWorkPosCate = (rowidx, val, lvl) => {
    var chkLvl = lvl + 1;

    var tmpList1 = detailState.workPosCateCd1;
    var tmpList2 = detailState.workPosCateCd2;
    var tmpList3 = detailState.workPosCateCd3;

    var tmpComboList1 = workPosCateCdCombo[rowidx].cateCdList1;
    var tmpComboList2 = workPosCateCdCombo[rowidx].cateCdList2;
    var tmpComboList3 = workPosCateCdCombo[rowidx].cateCdList3;

    if (lvl <= 1) {
      tmpList2[rowidx] = "";
      tmpComboList2 = "";
    }
    if (lvl <= 2) {
      tmpList3[rowidx] = "";
      tmpComboList3 = "";
    }

    if (lvl == 1) tmpList1[rowidx] = val;
    else if (lvl == 2) tmpList2[rowidx] = val;
    else if (lvl == 3) tmpList3[rowidx] = val;

    const cateArray = [];
    for (var k = 0; k < workPosCateCdList.length; k++) {
      if (
        workPosCateCdList[k].lvl == chkLvl &&
        workPosCateCdList[k].upCd == val
      ) {
        cateArray.push({
          lvl: workPosCateCdList[k].lvl,
          cateCd: workPosCateCdList[k].cateCd,
          cdName: workPosCateCdList[k].cdName,
        });
      }
    }

    if (chkLvl == 2) {
      tmpComboList2 = cateArray;
      workPosCateCdCombo[rowidx].cateCdList2 = tmpComboList2;
    } else if (chkLvl == 3) {
      tmpComboList3 = cateArray;
      workPosCateCdCombo[rowidx].cateCdList3 = tmpComboList3;
    }

    setDetailState({
      ...detailState,
      workPosCateCd1: tmpList1,
      workPosCateCd2: tmpList2,
      workPosCateCd3: tmpList3,
    });
  };
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  const addTag = (e) => {
    if (e.key == "Enter") {
      if (detailState.tags.length == 29) {
        alert(
          intl.formatMessage({
            id: "input.max30",
            defaultMessage: "30개까지 입력 가능합니다!",
          })
        );
        detailState.tagCntnt = "";
        return false;
      }

      var duplYn = "N";

      for (var k = 0; k < detailState.tags.length; k++) {
        if (detailState.tags[k] == e.target.value) {
          duplYn = "Y";
          break;
        }
      }

      if (duplYn == "N") detailState.tags.push(e.target.value);
      detailState.tagCntnt = "";
      reRender();
    }

    return false;
  };

  const rmvTag = (idx) => {
    var tmpList = commonUtil.rmvArrIdx(detailState.tags, idx);
    setDetailState({
      ...detailState,
      tags: tmpList,
    });
  };

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  const addSrchFrim = (e) => {
    var tmpList = [];
    var tmpUidList = [];
    for (var k = 0; k < searchFirmList.length; k++) {
      if (searchFirmList[k].chkYn == "Y") {
        tmpList.push(searchFirmList[k].email);
        tmpUidList.push(searchFirmList[k].srchfirmUid);
      }
    }

    detailState.srchFirms = tmpList;

    setDetailState({
      ...detailState,
      srchFirms: tmpList,
      srchFirmUids: tmpUidList,
    });

    closeSerchFirmListModal();
  };

  const rmvAllSrchFrim = (e) => {
    var tmpList = [];
    var tmpUidList = [];
    for (var k = 0; k < searchFirmList.length; k++) {
      searchFirmList[k].chkYn = "N";
    }

    detailState.srchFirms = tmpList;

    setDetailState({
      ...detailState,
      srchFirms: tmpList,
      srchFirmUids: tmpUidList,
    });

    closeSerchFirmListModal();
  };

  const rmvSrchFrim = (idx) => {
    var tmpList = commonUtil.rmvArrIdx(detailState.srchFirms, idx);
    searchFirmList[idx].chkYn = "N";
    setDetailState({
      ...detailState,
      srchFirms: tmpList,
    });
  };

  const addMatchDprt = (e) => {
    var tmpList = [];
    for (var k = 0; k < myDprtMbrlist.length; k++) {
      if (myDprtMbrlist[k].chkYn == "Y") {
        tmpList.push(myDprtMbrlist[k].mbrUid);
      }
    }

    setDetailState({
      ...detailState,
      matchDprt: tmpList,
    });

    closeDprtMatchModal();
  };

  const rmvMatchDprt = (e) => {
    var tmpList = [];
    for (var k = 0; k < myDprtMbrlist.length; k++) {
      myDprtMbrlist[k].chkYn = "N";
    }

    setDetailState({
      ...detailState,
      matchDprt: tmpList,
    });
    closeDprtMatchModal();
  };

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  const years = defVal.YearCnt();
  const months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

  let hours = [];
  let srvcFeePctMinList = [""];
  let srvcFeePctMaxList = [""];

  for (let i = 0; i <= 23; i++) {
    if (i >= 10) hours.push(i);
    else hours.push("0" + i);
  }

  for (let i = 0; i <= 32; i++) {
    srvcFeePctMinList.push(3 + i);
    srvcFeePctMaxList.push(3 + i);
  }

  const date = new Date();

  const [formChecked, setFormChekced] = useState(false);
  const radioChangeHandler = (event) => {
    const target = event.target;
    const value = target.value;

    if (value === "cmpnyForm") {
      setFormChekced(true);
    } else {
      setFormChekced(false);
    }

    let tmpList = detailState.uploadFiles;
    tmpList[3] = "disable";

    let tmpJsonStrList = detailState.snglImgFileJsonStr;
    tmpJsonStrList[3] = "";

    setDetailState({
      ...detailState,
      resumeForm: value,
      uploadFiles: tmpList,
      snglImgFileJsonStr: tmpJsonStrList,
      resumeFormThmnl: "",
      resumeFormThmnlNm: "파일 이름",
    });
  };

  const isUntilDoneClicked = () => {
    setUntilDone(!untilDone);

    if (!untilDone) {
      setDetailState({
        ...detailState,
        rcmndEndYear: "9999",
        rcmndUntilDon: "Y",
      });
    } else {
      setDetailState({
        ...detailState,
        rcmndEndYear: date.getFullYear().toString(),
        rcmndUntilDon: "N",
      });
    }
  };

  const isSrvcFeeFixYnChecked = (e) => {
    setDetailState({
      ...detailState,
      srvcFeePctMin: "",
      srvcFeePctMax: "",
      srvcFeeFixYn: e.target.checked ? "Y" : "N",
    });
  };

  const daysInMonth = (month, year) => {
    let tmpList = [];

    for (let i = 1; i <= new Date(year, month, 0).getDate(); i++) {
      tmpList.push(i);
    }

    return tmpList;
  };

  const [days, setDays] = useState(daysInMonth(1, date.getFullYear()));

  const checkAllSrchFrim = (e) => {
    var chkYn = "N";
    if (document.getElementById("selectAllSrchFrim").checked == true) {
      chkYn = "Y";
    }

    for (var k = 0; k < searchFirmList.length; k++) {
      searchFirmList[k].chkYn = chkYn;
    }

    reRender();
  };

  const onChangeSrchFirmChk = (e, idx) => {
    var chkYn = "N";
    if (searchFirmList[idx].chkYn == "N") chkYn = "Y";
    searchFirmList[idx].chkYn = chkYn;

    reRender();
  };

  const onChangeMatchDprtChk = (e, idx) => {
    var chkYn = "N";
    if (myDprtMbrlist[idx].chkYn == "N") chkYn = "Y";
    myDprtMbrlist[idx].chkYn = chkYn;

    reRender();
  };

  const onChangePrevHire = async (e) => {
    let goUrl = server.path + "/hire/hirecnfrmdetail";
    let data = {
      ssnMbrUid: ssn.ssnMbrUid,
      ssnMbrDivi: ssn.ssnMbrDivi,
      hireUid: e.target.value,
    };

    //상세정보 가저오기
    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    var tmpDetail = defVal.setAxiosJobState(axiosRes.data.resInfo);
    tmpDetail.ssnMbrUid = ssn.ssnMbrUid;

    console.log("tmpDetail.rcmndUntilDon == " + tmpDetail.rcmndUntilDon);

    //추천마감일
    if (tmpDetail.rcmndUntilDon == null || tmpDetail.rcmndUntilDon != "Y") {
      tmpDetail.rcmndUntilDonFlag = false;
      tmpDetail.rcmndEndYear = parseInt(tmpDetail.rcmndEndDttm.slice(0, 4));
      tmpDetail.rcmndEndMonth = parseInt(tmpDetail.rcmndEndDttm.slice(5, 7));
      tmpDetail.rcmndEndDay = parseInt(tmpDetail.rcmndEndDttm.slice(8, 10));
      tmpDetail.rcmndEndHour = tmpDetail.rcmndEndDttm.slice(11, 13);

      let tmpDays = [];
      for (
        let k = 1;
        k <=
        new Date(tmpDetail.rcmndEndYear, tmpDetail.rcmndEndMonth, 0).getDate();
        k++
      )
        tmpDays.push(k);

      tmpDetail.days = tmpDays;

      console.log("tmpDetail.rcmndEndYear == " + tmpDetail.rcmndEndYear);
      console.log("tmpDetail.rcmndEndMonth == " + tmpDetail.rcmndEndMonth);
      console.log("tmpDetail.rcmndEndDay == " + tmpDetail.rcmndEndDay);
      console.log("tmpDetail.rcmndEndHour == " + tmpDetail.rcmndEndHour);
    }

    //키워드 처리
    let tagstr = axiosRes.data.resInfo.tags;
    console.log("tagstr == " + tagstr);
    var tags = defVal.SplitToArray(tagstr, "<;:>");
    axiosRes.data.resInfo.tags = tags;
    tmpDetail.tags = tags;

    //첨부파일 처리
    let atchFilesStr = axiosRes.data.resInfo.atchFiles;
    if (atchFilesStr != "") {
      atchFiles = JSON.parse(atchFilesStr);
      for (var k = 0; k < atchFiles.length; k++) {
        if (atchFiles[k].filePos == 1)
          tmpDetail.thmnlNm = atchFiles[k].orignlFileNm;
        else if (atchFiles[k].filePos == 2)
          tmpDetail.thmnlNm2 = atchFiles[k].orignlFileNm;
        else if (atchFiles[k].filePos == 3)
          tmpDetail.resumeFormThmnlNm = atchFiles[k].orignlFileNm;

        tmpDetail.snglImgFileJsonStr[k + 1] = JSON.stringify(atchFiles[k]);
      }
    }

    setDetailState(tmpDetail);

    //일반모집요강
    var tmpBizGuidList = await getBizGuidList(locationState.mbrUid);

    setBizCate(axiosRes.data.bizCateArr);
    setEmpAddrCate(axiosRes.data.empAddrCateArr);
    setWorkPosCate(axiosRes.data.workPosCateArr);

    //setBizCateCd(axiosRes.data.resBizCateCdArr);
    //setEmpAddrCateCd(axiosRes.data.empAddrCateCdArr);
    //setWorkPosCateCd(axiosRes.data.workPosCateCdArr);

    //setDetailState(tmpDetail);
    //initSetHandle();
    //alert(e.target.value);

    //setDetailState(tmpDetail);
  };

  const getBizGuidList = async (mbrUid) => {
    let goUrl = server.path + "/corporate/bizguidlist";
    let data = {
      mbrUid: mbrUid,
    };

    let axiosRes = await axiosCrudOnSubmit(data, goUrl, "POST");
    let tmpArr = axiosRes.data.resArr;    

    for(var k=0; k<tmpArr.length; k++)
    {
      if( tmpArr[k].thmnl != "")
      {
        let fileList = JSON.parse(tmpArr[k].atchFiles);
        tmpArr[k].orignlFileNm = fileList[0].orignlFileNm;
      }

      if( tmpArr[k].brdDivi != "02" )setGuid01Cnt(1);
      else setGuid02Cnt(1);
    }

    setBizGuidListState(tmpArr);
    return tmpArr;
  };

  useEffect(() => {
    if (bizCate && bizCate.length > 0) {
      var tmpBizCateCdList = bizCateCdList;

      //업종구분 가저오기
      var tmpBizCateList = bizCate;

      //업종구분 combo추가하기
      for (var k = 1; k < tmpBizCateList.length; k++) addNoRenerBizCate();
      for (var k = 0; k < tmpBizCateList.length; k++) {
        detailState.bizCateCd1[k] = tmpBizCateList[k].cateCd1;
        detailState.bizCateCd2[k] = tmpBizCateList[k].cateCd2;
        detailState.bizCateCd3[k] = tmpBizCateList[k].cateCd3;

        var childCateList2 = getBizCateChildFilter(
          tmpBizCateCdList,
          tmpBizCateList[k].cateCd1,
          1
        );
        bizCateCdCombo[k].cateCdList2 = childCateList2;

        var childCateList3 = getBizCateChildFilter(
          tmpBizCateCdList,
          tmpBizCateList[k].cateCd2,
          2
        );
        bizCateCdCombo[k].cateCdList3 = childCateList3;
      }

      reRender();
    }
  }, [bizCate]);

  const addNoRenerBizCate = () => {
    var data = defVal.SelCateCdCombo();
    bizCateCdCombo.push(data);

    if (!detailState.bizCateCd1 || detailState.bizCateCd1.length == 0) {
      detailState.bizCateCd1.push("NONE");
      detailState.bizCateCd2.push("NONE");
      detailState.bizCateCd3.push("NONE");
    }

    detailState.bizCateCd1.push("NONE");
    detailState.bizCateCd2.push("NONE");
    detailState.bizCateCd3.push("NONE");
  };

  const getBizCateChildFilter = (cateList, val, lvl) => {
    var chkLvl = lvl + 1;

    const cateArray = [];
    for (var k = 0; k < cateList.length; k++) {
      if (cateList[k].lvl == chkLvl && cateList[k].upCd == val) {
        cateArray.push({
          lvl: cateList[k].lvl,
          cateCd: cateList[k].cateCd,
          cdName: cateList[k].cdName,
        });
      }
    }

    return cateArray;
  };

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  useEffect(() => {
    if (empAddrCate && empAddrCate.length > 0) {
      //근무지역 가저오기
      var tmpEmpAddrCateCdList = empAddrCateCdList;
      var tmpEmpAddrCateList = empAddrCate;

      //근무지역 combo추가하기
      for (var k = 1; k < tmpEmpAddrCateList.length; k++)
        addNoRenerEmpAddrCate();
      for (var k = 0; k < tmpEmpAddrCateList.length; k++) {
        detailState.empAddrCateCd1[k] = tmpEmpAddrCateList[k].cateCd1;
        detailState.empAddrCateCd2[k] = tmpEmpAddrCateList[k].cateCd2;

        var childCateList2 = getEmpAddrCateChildFilter(
          tmpEmpAddrCateCdList,
          tmpEmpAddrCateList[k].cateCd1,
          1
        );
        empAddrCateCdCombo[k].cateCdList2 = childCateList2;
      }

      reRender();
    }
  }, [empAddrCate]);

  const addNoRenerEmpAddrCate = () => {
    var data = defVal.SelCateCdCombo();
    empAddrCateCdCombo.push(data);

    if (detailState.empAddrCateCd1.length == 0) {
      detailState.empAddrCateCd1.push("NONE");
      detailState.empAddrCateCd2.push("NONE");
    }

    detailState.empAddrCateCd1.push("NONE");
    detailState.empAddrCateCd2.push("NONE");
  };

  const getEmpAddrCateChildFilter = (cateList, val, lvl) => {
    var chkLvl = lvl + 1;

    const cateArray = [];
    for (var k = 0; k < cateList.length; k++) {
      if (cateList[k].lvl == chkLvl && cateList[k].upCd == val) {
        cateArray.push({
          lvl: cateList[k].lvl,
          cateCd: cateList[k].cateCd,
          cdName: cateList[k].cdName,
        });
      }
    }

    return cateArray;
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  useEffect(() => {
    if (workPosCate && workPosCate.length > 0) {
      console.log(
        "6 workPosCate useEffect =========================================="
      );

      var tmpWorkPosCateCdList = workPosCateCdList;
      var tmpWorkPosCateList = workPosCate;
      for (var k = 1; k < tmpWorkPosCateList.length; k++)
        addNoRenerWorkPosCate();
      for (var k = 0; k < tmpWorkPosCateList.length; k++) {
        detailState.workPosCateCd1[k] = tmpWorkPosCateList[k].cateCd1;
        detailState.workPosCateCd2[k] = tmpWorkPosCateList[k].cateCd2;

        var childCateList2 = getWorkPosCateChildFilter(
          tmpWorkPosCateCdList,
          tmpWorkPosCateList[k].cateCd1,
          1
        );
        workPosCateCdCombo[k].cateCdList2 = childCateList2;
      }

      reRender();
    }
  }, [workPosCate]);

  const addNoRenerWorkPosCate = () => {
    var data = defVal.SelCateCdCombo();
    workPosCateCdCombo.push(data);

    if (detailState.workPosCateCd1.length == 0) {
      detailState.workPosCateCd1.push("NONE");
      detailState.workPosCateCd2.push("NONE");
      detailState.workPosCateCd3.push("NONE");
    }

    detailState.workPosCateCd1.push("NONE");
    detailState.workPosCateCd2.push("NONE");
    detailState.workPosCateCd3.push("NONE");
  };

  const getWorkPosCateChildFilter = (cateList, val, lvl) => {
    var chkLvl = lvl + 1;

    const cateArray = [];
    for (var k = 0; k < cateList.length; k++) {
      if (cateList[k].lvl == chkLvl && cateList[k].upCd == val) {
        cateArray.push({
          lvl: cateList[k].lvl,
          cateCd: cateList[k].cateCd,
          cdName: cateList[k].cdName,
        });
      }
    }

    return cateArray;
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  async function goFileUpload() {
    let fileStorePath = "Globals.fileStoreHirePath"; //로컬저장경로
    let fileLinkPath = "Globals.fileStoreHirePath"; //link경로
    let filePrefix = "hire"; //파일코드

    PROC_CHK = "FILEUPLOAD";

    let uploadRes = await snglFileUpload(
      server.path,
      fileStorePath,
      fileLinkPath,
      filePrefix,
      detailState.uploadFiles,
      detailState.filePosNm
    );

    //console.log("FILE UPLOAD RESULT : ");
    //console.log(uploadRes);

    if (uploadRes === 0) cntntsCRUD();
    else jsonCompResult(uploadRes.data);
  }

  useEffect(() => {
    if (detailState.crudStateSetCmplt) {
      goFileUpload();

      //cntntsCRUD();
    }
  }, [detailState.crudStateSetCmplt]);

  /////////////////////////////////////////////////////////////////////////////////////////////

  function onKeyDownForm(keyEvent) {
    if ((keyEvent.charCode || keyEvent.keyCode) === 13) {
      keyEvent.preventDefault();
    }
  }

  ///////////////////////////////////////////////////////////////////////////////////////////
  const jsonCompResult = (resJson) => {
    if (resJson.resCd !== "0000") {
      toast.error(
        intl.formatMessage({
          id: "error.during.save",
          defaultMessage: "저장 중 오류가 발생했습니다.",
        })
      );
      return;
    } else {
      if (PROC_CHK == "INS") {
        PROC_CHK = "";
        alert(
          intl.formatMessage({
            id: "info.recruit.posting.added",
            defaultMessage: "채용 공고가 등록되었습니다.",
          })
        );

        //let tmpPath = (detailState.hireStat === "tmp") ? "temporary" : "confirm";
        //navigate("/admin/" + tmpPath, { mbrUid : location.state.mbrUid });

        navigate("/employers/confirm");

        //initSetHandle();
      } else if (PROC_CHK == "FILEUPLOAD") {
        let jsonStr = JSON.stringify(resJson.resFileList);
        let fileList = JSON.parse(jsonStr);
        let filePos = 0;
        let textAreaCnt = detailState.snglImgFileJsonStr.length;
        let tmpList = detailState.snglImgFileJsonStr;

        for (let i = 0; i < fileList.length; i++) {
          filePos = fileList[i].filePos;
          detailState.snglImgFileJsonStr[filePos] = JSON.stringify(fileList[i]);
        }

        cntntsCRUD();
      } else if (PROC_CHK == "INSSRCHFIRM") {
        PROC_CHK = "";

        if (resJson.srchfirmUid == "DUPL") {
          alert(
            intl.formatMessage({
              id: "address.exist",
              defaultMessage: "이미 등록된 주소입니다.",
            })
          );
          return;
        } else {
          alert(
            intl.formatMessage({
              id: "info.address.added",
              defaultMessage: "주소가 등록되었습니다.",
            })
          );
        }

        closeSerchFirmModal();
        changeFltrSrchFrim("bizNm", "ASC");
      }
    }
  };

  const goCRUD = async (hireStat) => {
    if (detailState.rcmndEndYear == "") detailState.rcmndEndYear = "2022";
    if (detailState.rcmndEndMonth == "") detailState.rcmndEndMonth = "1";
    if (detailState.rcmndEndDay == "") detailState.rcmndEndMonth = "1";

    let tmpRcmndEndMonth =
      detailState.rcmndEndMonth < 10
        ? "0" + detailState.rcmndEndMonth
        : detailState.rcmndEndMonth;
    let tmpRcmndEndDay =
      parseInt(detailState.rcmndEndDay) < 10
        ? "0" + detailState.rcmndEndDay
        : detailState.rcmndEndDay;
    let tmpRcmndEndDt =
      detailState.rcmndEndYear + "-" + tmpRcmndEndMonth + "-" + tmpRcmndEndDay;
    let tmpRcmndEndDttm =
      detailState.rcmndEndYear +
      tmpRcmndEndMonth +
      tmpRcmndEndDay +
      detailState.rcmndEndHour +
      "0000";

    if (
      commonUtil.CheckIsEmptyFromValAlert(
        detailState.bizKnd,
        intl.formatMessage({
          id: "error.need.input.business.type",
          defaultMessage: "기업형태를 입력해주세요.",
        })
      ) === false
    )
      return;
    if (
      commonUtil.CheckIsEmptyFromValAlert(
        detailState.saleSize,
        intl.formatMessage({
          id: "error.need.input.sale.size",
          defaultMessage: "매출규모를 입력해주세요.",
        })
      ) === false
    )
      return;
    if (
      commonUtil.CheckIsEmptyFromValAlert(
        detailState.empSize,
        intl.formatMessage({
          id: "error.need.input.workers",
          defaultMessage: "종업원수를 입력해주세요.",
        })
      ) === false
    )
      return;
    if (
      commonUtil.CheckIsEmptyFromValAlert(
        detailState.bizCateCd1,
        intl.formatMessage({
          id: "error.need.input.job.type",
          defaultMessage: "업종구분을 입력해주세요.",
        })
      ) === false
    )
      return;
    if (
      commonUtil.CheckIsEmptyFromValAlert(
        detailState.bizCateCd2,
        intl.formatMessage({
          id: "error.need.input.job.type",
          defaultMessage: "업종구분을 입력해주세요.",
        })
      ) === false
    )
      return;
    if (
      commonUtil.CheckIsEmptyFromValAlert(
        detailState.bizCateCd3,
        intl.formatMessage({
          id: "error.need.input.job.type",
          defaultMessage: "업종구분을 입력해주세요.",
        })
      ) === false
    )
      return;
    if (
      commonUtil.CheckIsEmptyFromValAlert(
        detailState.empAddrCateCd1,
        intl.formatMessage({
          id: "error.need.input.workplace",
          defaultMessage: "근무예정지를 입력해주세요.",
        })
      ) === false
    )
      return;
    if (
      commonUtil.CheckIsEmptyFromValAlert(
        detailState.empAddrCateCd2,
        intl.formatMessage({
          id: "error.need.input.workplace",
          defaultMessage: "근무예정지를 입력해주세요.",
        })
      ) === false
    )
      return;
    if (
      commonUtil.CheckIsEmptyFromValAlert(
        detailState.title,
        intl.formatMessage({
          id: "error.need.input.posting.title",
          defaultMessage: "채용공고 제목을 입력해주세요.",
        })
      ) === false
    )
      return;
    if (
      commonUtil.CheckIsEmptyFromValAlert(
        detailState.workPosCateCd1,
        intl.formatMessage({
          id: "error.need.input.work.category",
          defaultMessage: "직종별구분을 입력해주세요.",
        })
      ) === false
    )
      return;
    if (
      commonUtil.CheckIsEmptyFromValAlert(
        detailState.workPosCateCd2,
        intl.formatMessage({
          id: "error.need.input.work.category",
          defaultMessage: "직종별구분을 입력해주세요.",
        })
      ) === false
    )
      return;

    if (commonUtil.getToday() >= tmpRcmndEndDt) {
      alert(
        intl.formatMessage(messages.deadline, {
          today: commonUtil.getToday(),
        })
      );
      return;
    }

    if (
      commonUtil.CheckIsEmptyFromValAlert(
        detailState.tags,
        intl.formatMessage({
          id: "error.need.input.searching.keyword",
          defaultMessage: "인재써칭 핵심 키워드를 입력해주세요.",
        })
      ) === false
    )
      return;

    detailState.mbrUid = locationState.mbrUid;
    detailState.rcmndEndDttm = tmpRcmndEndDttm;
    detailState.srvcFeePctMin =
      detailState.srvcFeeFixYn === "Y" ? "" : detailState.srvcFeePctMin;
    detailState.srvcFeePctMax =
      detailState.srvcFeeFixYn === "Y" ? "" : detailState.srvcFeePctMax;
    detailState.hireStat = hireStat;

    let tmpList = detailState.tags;

    let fileStorePath = "Globals.fileStoreHirePath"; //로컬저장경로
    let fileLinkPath = "Globals.fileLinkHirePath"; //link경로
    let filePrefix = "hire"; //파일코드

    PROC_CHK = "FILEUPLOAD";

    let uploadRes = await snglFileUpload(
      server.path,
      fileStorePath,
      fileLinkPath,
      filePrefix,
      detailState.uploadFiles,
      detailState.filePosNm
    );

    if (uploadRes === 0) cntntsCRUD();
    else jsonCompResult(uploadRes.data);
  };

  const cntntsCRUD = async () => {
    PROC_CHK = "INS";

    let goUrl = server.path + "/hire/hireinsert";
    let axiosRes = await axiosCrudOnSubmit(detailState, goUrl, "POST");

    jsonCompResult(axiosRes.data);
  };

  const goSrchFirmCRUD = async () => {
    if (searchFirm.recNm == "") {
      alert(
        intl.formatMessage({
          id: "error.need.input.name",
          defaultMessage: "이름을 입력해주세요.",
        })
      );
      return;
    }

    if (searchFirm.email == "") {
      alert(
        intl.formatMessage({
          id: "error.need.input.email",
          defaultMessage: "이메일을 입력해주세요.",
        })
      );
      return;
    }

    if (searchFirmList.length >= 30) {
      alert(
        intl.formatMessage({
          id: "error.searchfirm.over.added",
          defaultMessage: "기존 거래 써치펌은 30개까지만 등록 가능합니다.",
        })
      );
      closeSerchFirmModal();
      return;
    }

    PROC_CHK = "INSSRCHFIRM";
    let goUrl = server.path + "/srchfirm/searchfirminsert";
    searchFirm.mbrUid = ssn.ssnMbrUid;
    let axiosRes = await axiosCrudOnSubmit(searchFirm, goUrl, "POST");

    jsonCompResult(axiosRes.data);
  };


  /////////////////////////////////////////////////////////////////////////////////////////////////////////////
 const handleDownload = (filePath, orignlFileNm) => {
  
    if (filePath != "") {
      let goUrl = server.host + filePath;
      axios
        .get(goUrl, {
          responseType: "blob",
        })
        .then((res) => {
          fileDownload(res.data, orignlFileNm);
        });
    } else {
      
    }

  }

  return (
    <main className="employersMain">
      <form className="recruitForm employersForm">
        <div className="formTitle">
          <h1>
            <FormattedMessage
              id="posting.register"
              defaultMessage="채용공고 등록하기"
            />
          </h1>
          <div>
            <span>{ssn.ssnBizNm}</span>
            <div>
              <label htmlFor="prevForm">
                <FormattedMessage
                  id="job.posting.previous"
                  defaultMessage="이전 채용공고 불러오기"
                />
                &nbsp;
              </label>
              <select
                name="prevForm"
                id="prevForm"
                onChange={(e) => {
                  onChangePrevHire(e);
                }}
              >
                <option value="">
                  {intl.formatMessage({
                    id: "job.posting.previous.default",
                    defaultMessage: "불러올 채용공고를 선택하세요.",
                  })}
                </option>
                {listState.map((type, idx) => (
                  <option value={type.hireUid}>{type.title}</option>
                ))}
              </select>
            </div>
          </div>
        </div>
        <div className="formWrap">
          <h2>
            <FormattedMessage id="company.info" defaultMessage="기업정보" />
          </h2>
          <div className="formWrapInputs">
            <ul className="twoCol">
              <li>
                <span className="labelSpan">
                  <FormattedMessage
                    id="company.name.open"
                    defaultMessage="기업명 공개"
                  />
                </span>
                <div>
                  <input
                    type="radio"
                    id="agree"
                    name="disposal"
                    value="Y"
                    checked={detailState.pubYn === "Y" ? true : false}
                    onChange={(e) => {
                      onChangeVal(e, "pubYn");
                    }}
                  />
                  <label htmlFor="agree">
                    <FormattedMessage id="yes" defaultMessage="예" />
                  </label>
                  <input
                    type="radio"
                    id="disagree"
                    name="disposal"
                    value="N"
                    checked={detailState.pubYn === "N" ? true : false}
                    onChange={(e) => {
                      onChangeVal(e, "pubYn");
                    }}
                  />
                  <label htmlFor="disagree">
                    <FormattedMessage id="no" defaultMessage="아니오" />
                  </label>
                </div>
              </li>
              <li>
                <label htmlFor="businessForm">
                  <FormattedMessage
                    id="company.type"
                    defaultMessage="기업형태"
                  />
                </label>
                <select
                  name="businessForm"
                  id="businessForm"
                  value={detailState.bizKnd}
                  onChange={(e) => {
                    onChangeVal(e, "bizKnd");
                  }}
                >
                  <option value="">
                    --
                    {intl.formatMessage({
                      id: "select",
                      defaultMessage: "선택하세요",
                    })}
                    --
                  </option>
                  {bizKndCd.map((type, idx) => (
                    <option value={type.cateCd}>{type.cdName}</option>
                  ))}
                </select>
              </li>
            </ul>
            <ul className="twoCol">
              <li>
                <label htmlFor="sales">
                  <FormattedMessage
                    id="company.sales"
                    defaultMessage="매출규모"
                  />
                </label>
                <select
                  name="sales"
                  id="sales"
                  value={detailState.saleSize}
                  onChange={(e) => {
                    onChangeVal(e, "saleSize");
                  }}
                >
                  <option value="">
                    --
                    {intl.formatMessage({
                      id: "select",
                      defaultMessage: "선택하세요",
                    })}
                    --
                  </option>
                  {salesSizeCd.map((type, idx) => (
                    <option value={type.cateCd}>{type.cdName}</option>
                  ))}
                </select>
              </li>
              <li>
                <label htmlFor="employee">
                  <FormattedMessage
                    id="company.workers"
                    defaultMessage="종업원 수"
                  />
                </label>
                <select
                  name="employee"
                  id="employee"
                  value={detailState.empSize}
                  onChange={(e) => {
                    onChangeVal(e, "empSize");
                  }}
                >
                  <option value="">
                    --
                    {intl.formatMessage({
                      id: "select",
                      defaultMessage: "선택하세요",
                    })}
                    --
                  </option>
                  {empSizeCd.map((type, idx) => (
                    <option value={type.cateCd}>{type.cdName}</option>
                  ))}
                </select>
              </li>
            </ul>
            <ul>
              <li>
                <div className="adminFlexColumn">
                  {bizCateCdCombo.map((type, rowidx) => (
                    <div
                      style={{
                        display: bizCateCdCombo[rowidx].display,
                        marginBottom: "3px",
                      }}
                    >
                      <span htmlFor="businessType1-1" className="labelSpan">
                        {rowidx === 0
                          ? intl.formatMessage({
                              id: "company.business.type",
                              defaultMessage: "업종구분",
                            })
                          : ""}
                      </span>
                      <select
                        name="businessType1-1"
                        id="businessType1-1"
                        value={detailState.bizCateCd1[rowidx]}
                        onChange={(e) => {
                          chngBizCate(rowidx, e.target.value, 1);
                        }}
                      >
                        <option value="">
                          --
                          {intl.formatMessage({
                            id: "select",
                            defaultMessage: "선택하세요",
                          })}
                          --
                        </option>
                        {bizCateCdList.map((type1, idx1) =>
                          type1.lvl == 1 ? (
                            <option value={type1.cateCd}>{type1.cdName}</option>
                          ) : (
                            ""
                          )
                        )}
                      </select>
                      <select
                        name="businessType1-1"
                        id="businessType1-1"
                        value={detailState.bizCateCd2[rowidx]}
                        onChange={(e) => {
                          chngBizCate(rowidx, e.target.value, 2);
                        }}
                      >
                        <option value="">
                          --
                          {intl.formatMessage({
                            id: "select",
                            defaultMessage: "선택하세요",
                          })}
                          --
                        </option>
                        {bizCateCdCombo[rowidx].cateCdList2.length > 0
                          ? bizCateCdCombo[rowidx].cateCdList2.map(
                              (type2, idx2) => (
                                <option value={type2.cateCd}>
                                  {type2.cdName}
                                </option>
                              )
                            )
                          : ""}
                      </select>
                      <select
                        name="businessType1-1"
                        id="businessType1-1"
                        value={detailState.bizCateCd3[rowidx]}
                        onChange={(e) => {
                          chngBizCate(rowidx, e.target.value, 3);
                        }}
                      >
                        <option value="">
                          --
                          {intl.formatMessage({
                            id: "select",
                            defaultMessage: "선택하세요",
                          })}
                          --
                        </option>
                        {bizCateCdCombo[rowidx].cateCdList3.length > 0
                          ? bizCateCdCombo[rowidx].cateCdList3.map(
                              (type3, idx3) => (
                                <option value={type3.cateCd}>
                                  {type3.cdName}
                                </option>
                              )
                            )
                          : ""}
                      </select>
                      {rowidx === 0 ? (
                        <button
                          type="button"
                          className="formButton"
                          onClick={() => {
                            addBizCate();
                          }}
                        >
                          <FormattedMessage
                            id="add"
                            defaultMessage="추가하기"
                          />{" "}
                          &gt;
                        </button>
                      ) : (
                        <button
                          type="button"
                          className="formButton"
                          onClick={() => {
                            rmvBizCate(rowidx);
                          }}
                        >
                          <FormattedMessage
                            id="delete"
                            defaultMessage="삭제하기"
                          />{" "}
                          &gt;
                        </button>
                      )}
                    </div>
                  ))}
                </div>
              </li>
            </ul>
            <ul>
              <li>
                <div className="flexColmn">
                  {empAddrCateCdCombo.map((type, rowidx) => (
                    <div
                      style={{
                        display: empAddrCateCdCombo[rowidx].display,
                        marginBottom: "3px",
                      }}
                    >
                      <span htmlFor="businessType1-1" className="labelSpan">
                        {rowidx === 0
                          ? intl.formatMessage({
                              id: "company.workplace",
                              defaultMessage: "근무 예정지",
                            })
                          : ""}
                      </span>
                      <select
                        name="businessType1-1"
                        id="businessType1-1"
                        value={detailState.empAddrCateCd1[rowidx]}
                        onChange={(e) => {
                          chngEmpAddrCate(rowidx, e.target.value, 1);
                        }}
                      >
                        <option value="">
                          --
                          {intl.formatMessage({
                            id: "select",
                            defaultMessage: "선택하세요",
                          })}
                          --
                        </option>
                        {empAddrCateCdList.map((type1, idx1) =>
                          type1.lvl == 1 ? (
                            <option value={type1.cateCd}>{type1.cdName}</option>
                          ) : (
                            ""
                          )
                        )}
                      </select>
                      <select
                        name="businessType1-1"
                        id="businessType1-1"
                        value={detailState.empAddrCateCd2[rowidx]}
                        onChange={(e) => {
                          chngEmpAddrCate(rowidx, e.target.value, 2);
                        }}
                      >
                        <option value="">
                          --
                          {intl.formatMessage({
                            id: "select",
                            defaultMessage: "선택하세요",
                          })}
                          --
                        </option>
                        {empAddrCateCdCombo[rowidx].cateCdList2.length > 0
                          ? empAddrCateCdCombo[rowidx].cateCdList2.map(
                              (type2, idx2) => (
                                <option value={type2.cateCd}>
                                  {type2.cdName}
                                </option>
                              )
                            )
                          : ""}
                      </select>
                      {rowidx === 0 ? (
                        <button
                          type="button"
                          className="formButton"
                          onClick={() => {
                            addEmpAddrCate();
                          }}
                        >
                          <FormattedMessage
                            id="add"
                            defaultMessage="추가하기"
                          />{" "}
                          &gt;
                        </button>
                      ) : (
                        <button
                          type="button"
                          className="formButton"
                          onClick={() => {
                            rmvEmpAddrCate(rowidx);
                          }}
                        >
                          <FormattedMessage
                            id="delete"
                            defaultMessage="삭제하기"
                          />{" "}
                          &gt;
                        </button>
                      )}
                    </div>
                  ))}
                </div>
              </li>
            </ul>
          </div>
        </div>
        <div className="formWrap">
          <h2>모집내용</h2>
          <div className="formWrapInputs">
            <ul>
              <li>
                <label htmlFor="postingName">
                  <FormattedMessage
                    id="recruitment.title"
                    defaultMessage="채용공고 제목"
                  />
                </label>
                <input
                  type="text"
                  id="postingTitle"
                  className="fullWidth"
                  maxLength="50"
                  value={detailState.title}
                  onChange={(e) => {
                    onChangeVal(e, "title");
                    setTitleLength(e.target.value.length);
                  }}
                />
                <em>{titleLength} / 50</em>
              </li>
            </ul>
            <ul>
              <li>
                <div className="flexColmn">
                  {workPosCateCdCombo.map((type, rowidx) => (
                    <div
                      style={{
                        display: workPosCateCdCombo[rowidx].display,
                        marginBottom: "3px",
                      }}
                    >
                      <span htmlFor="businessType1-1" className="labelSpan">
                        {rowidx === 0 ? (
                          <FormattedMessage
                            id="recruitment.position.classify"
                            defaultMessage="직종별 구분"
                          />
                        ) : (
                          ""
                        )}
                      </span>
                      <select
                        name="businessType1-1"
                        id="businessType1-1"
                        value={detailState.workPosCateCd1[rowidx]}
                        onChange={(e) => {
                          chngWorkPosCate(rowidx, e.target.value, 1);
                        }}
                      >
                        <option value="">
                          --
                          {intl.formatMessage({
                            id: "select",
                            defaultMessage: "선택하세요",
                          })}
                          --
                        </option>
                        {workPosCateCdList.map((type1, idx1) =>
                          type1.lvl == 1 ? (
                            <option value={type1.cateCd}>{type1.cdName}</option>
                          ) : (
                            ""
                          )
                        )}
                      </select>
                      <select
                        name="businessType1-1"
                        id="businessType1-1"
                        value={detailState.workPosCateCd2[rowidx]}
                        onChange={(e) => {
                          chngWorkPosCate(rowidx, e.target.value, 2);
                        }}
                      >
                        <option value="">
                          --
                          {intl.formatMessage({
                            id: "select",
                            defaultMessage: "선택하세요",
                          })}
                          --
                        </option>
                        {workPosCateCdCombo[rowidx].cateCdList2.length > 0
                          ? workPosCateCdCombo[rowidx].cateCdList2.map(
                              (type2, idx2) => (
                                <option value={type2.cateCd}>
                                  {type2.cdName}
                                </option>
                              )
                            )
                          : ""}
                      </select>
                      {rowidx === 0 ? (
                        <button
                          type="button"
                          className="formButton"
                          onClick={() => {
                            addWorkPosCate();
                          }}
                        >
                          <FormattedMessage
                            id="add"
                            defaultMessage="추가하기"
                          />{" "}
                          &gt;
                        </button>
                      ) : (
                        <button
                          type="button"
                          className="formButton"
                          onClick={() => {
                            rmvWorkPosCate(rowidx);
                          }}
                        >
                          <FormattedMessage
                            id="delete"
                            defaultMessage="삭제하기"
                          />{" "}
                          &gt;
                        </button>
                      )}
                    </div>
                  ))}
                </div>
              </li>
            </ul>
            <ul className="twoCol">
              <li>
                <label htmlFor="numberofPeople">
                  <FormattedMessage
                    id="recruitment.people.number"
                    defaultMessage="모집인원"
                  />
                </label>
                <select
                  name="numberofPeople"
                  id="numberofPeople"
                  value={detailState.hireSize}
                  onChange={(e) => {
                    onChangeVal(e, "hireSize");
                  }}
                >
                  {hireSizeCd.map((type, idx) => (
                    <option value={type.cateCd}>{type.cdName}</option>
                  ))}
                </select>
              </li>
              <li>
                <span className="labelSpan">
                  <FormattedMessage
                    id="recruitment.employment.form"
                    defaultMessage="고용형태"
                  />
                </span>
                <div>
                  <input
                    type="checkbox"
                    id="permanent"
                    name="permanent"
                    value="fullTime"
                    checked={
                      commonUtil.indexOf(detailState.hireKnd, "fullTime") > -1
                        ? true
                        : false
                    }
                    onChange={(e) => {
                      onChangeVal(e, "hireKnd");
                    }}
                  />
                  <label htmlFor="permanent">
                    <FormattedMessage
                      id="recruitment.employment.permanent"
                      defaultMessage="정규직"
                    />
                  </label>
                  {/* 
                      <input type="checkbox" id="contract" />
                    */}
                  <input
                    type="checkbox"
                    id="contract"
                    name="contract"
                    value="cntrct"
                    checked={
                      commonUtil.indexOf(detailState.hireKnd, "cntrct") > -1
                        ? true
                        : false
                    }
                    onChange={(e) => {
                      onChangeVal(e, "hireKnd");
                    }}
                  />
                  <label htmlFor="contract">
                    <FormattedMessage
                      id="recruitment.employment.contract"
                      defaultMessage="계약직"
                    />
                  </label>
                </div>
              </li>
            </ul>
            <ul>
              <li>
                <span className="labelSpan">
                  <FormattedMessage
                    id="recruitment.deadline"
                    defaultMessage="인재추천 마감일"
                  />
                </span>
                <div>
                  <select
                    name="deadlineYear"
                    value={detailState.rcmndEndYear}
                    disabled={untilDone ? true : false}
                    onChange={(e) => {
                      onChangeVal(e, "rcmndEndYear");
                      daysInMonth(detailState.rcmndEndMonth, e.target.value);
                    }}
                  >
                    {years.map((year, idx) => (
                      <option key={year} value={year}>
                        {year}
                        {intl.formatMessage({
                          id: "year",
                          defaultMessage: "년",
                        })}
                      </option>
                    ))}
                  </select>
                  <select
                    name="deadlineMonth"
                    className="shortWidth"
                    value={detailState.rcmndEndMonth}
                    disabled={untilDone ? true : false}
                    onChange={(e) => {
                      onChangeVal(e, "rcmndEndMonth");
                      setDays(
                        daysInMonth(e.target.value, detailState.rcmndEndYear)
                      );
                    }}
                  >
                    {months.map((month, idx) => (
                      <option key={month} value={month}>
                        {month}
                        {intl.formatMessage({
                          id: "month",
                          defaultMessage: "월",
                        })}
                      </option>
                    ))}
                  </select>
                  <select
                    name="deadlineDay"
                    className="shortWidth"
                    value={detailState.rcmndEndDay}
                    disabled={untilDone ? true : false}
                    onChange={(e) => {
                      onChangeVal(e, "rcmndEndDay");
                    }}
                  >
                    {days.map((day, idx) => (
                      <option key={day} value={day}>
                        {day}
                        {intl.formatMessage({
                          id: "day",
                          defaultMessage: "일",
                        })}
                      </option>
                    ))}
                  </select>
                  <select
                    name="deadlineTime"
                    className="shortWidth"
                    value={detailState.rcmndEndHour}
                    disabled={untilDone ? true : false}
                    onChange={(e) => {
                      onChangeVal(e, "rcmndEndHour");
                    }}
                  >
                    {hours.map((hour, idx) => (
                      <option key={hour} value={hour}>
                        {hour}
                        {intl.formatMessage({
                          id: "hour",
                          defaultMessage: "시",
                        })}
                      </option>
                    ))}
                  </select>
                  <input
                    type="checkbox"
                    id="untilDone"
                    style={{ marginLeft: "20px" }}
                    checked={detailState.rcmndUntilDon == "Y" ? true : false}
                    onClick={isUntilDoneClicked}
                  />
                  <label htmlFor="untilDone">
                    <FormattedMessage
                      id="recruitment.or.until.hired"
                      defaultMessage="또는 채용시까지"
                    />
                  </label>
                </div>
              </li>
            </ul>
            <ul className="twoCol">
              <li>
                <span className="labelSpan">
                  <FormattedMessage
                    id="recruitment.recommend.auto.close"
                    defaultMessage="인재추천 자동마감"
                  />
                </span>
                <div className="radioInput">
                  <input
                    type="radio"
                    id="close"
                    name="autoClose"
                    value="Y"
                    checked={detailState.rcmndAutoEndYn === "Y" ? true : false}
                    onChange={(e) => {
                      onChangeVal(e, "rcmndAutoEndYn");
                    }}
                  />
                  <label htmlFor="close">
                    <FormattedMessage id="yes" defaultMessage="예" />
                  </label>
                  <input
                    type="radio"
                    id="open"
                    name="autoClose"
                    value="N"
                    checked={detailState.rcmndAutoEndYn === "N" ? true : false}
                    onChange={(e) => {
                      onChangeVal(e, "rcmndAutoEndYn");
                    }}
                  />
                  <label htmlFor="open">
                    <FormattedMessage id="no" defaultMessage="아니오" />
                  </label>
                </div>
              </li>
              <li>
                <span className="labelSpan">
                  <FormattedMessage
                    id="recruitment.progress.mailing"
                    defaultMessage="진행과정 메일링"
                  />
                </span>
                <div className="radioInput">
                  <input
                    type="radio"
                    id="mailAgree"
                    name="progressMailing"
                    value="Y"
                    checked={detailState.rcmndPrgMailYn === "Y" ? true : false}
                    onChange={(e) => {
                      onChangeVal(e, "rcmndPrgMailYn");
                    }}
                  />
                  <label htmlFor="mailAgree">
                    <FormattedMessage id="mail.agree" defaultMessage="수신" />
                  </label>
                  <input
                    type="radio"
                    id="mailDisagree"
                    name="progressMailing"
                    value="N"
                    checked={detailState.rcmndPrgMailYn === "N" ? true : false}
                    onChange={(e) => {
                      onChangeVal(e, "rcmndPrgMailYn");
                    }}
                  />
                  <label htmlFor="mailDisagree">
                    <FormattedMessage
                      id="mail.disagree"
                      defaultMessage="미수신"
                    />
                  </label>
                </div>
              </li>
            </ul>
          </div>
        </div>
        <div className="formWrap">
          <h2>
            <FormattedMessage
              id="normal.application.guideline"
              defaultMessage="일반 모집요강"
            />
          </h2>

          <article style={{marginTop:'-20px', display:(guid01Cnt == 0)? 'none':'' }}>
              <table>
                <thead>
                  <tr>
                    <th>
                      <FormattedMessage
                        id="application.guideline"
                        defaultMessage="모집요강"
                      />
                    </th>
                    <th>
                      <FormattedMessage id="prologue" defaultMessage="머릿말" />
                    </th>
                    <th>
                      <FormattedMessage id="content" defaultMessage="내용" />
                    </th>
                    <th>
                      <FormattedMessage
                        id="file.attached"
                        defaultMessage="첨부파일"
                      />
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {bizGuidListState.map((type, idx) =>
                    type.brdDivi !== "02" ? (
                      <tr>
                        <td>
                          {bizGuidDivi.map((type1, idx1) =>
                            type1.cateCd === type.brdDivi ? type1.cdName : ""
                          )}
                        </td>
                        <td>
                          {bizGuidKnd.map((type2, idx2) =>
                            type2.cateCd === type.brdKnd ? type2.cdName : ""
                          )}
                        </td>
                        <td>{type.cntnt}</td>
                        <td>
                          <span>{type.orignlFileNm}</span>
                            {
                              type.thmnl != '' ? 
                              (
                                <button type="button" className="basicButton" style={{marginLeft:'10px', display:(type.thmnl == '')? 'none' : ''}}
                                onClick={() => handleDownload(type.thmnl, type.orignlFileNm)}>
                                  <FormattedMessage
                                    id="file.download"
                                    defaultMessage="파일다운"
                                  />
                                </button>  
                              ) : ("")
                            }
                        </td>
                      </tr>
                    ) : (
                      ""
                    )
                  )}
                </tbody>
              </table>
            </article>

          <div className="fileBoxWrap">
            <div className="fileBox">
              <div>
                <span className="fileName">{detailState.thmnlNm}</span>
                <label htmlFor="fileInput1">
                  <FormattedMessage
                    id="file.attachment"
                    defaultMessage="파일 첨부"
                  />{" "}
                  &gt;
                </label>
                <input
                  type="file"
                  id="fileInput1"
                  className="fileInput"
                  onChange={(e) => {
                    let tmpList = detailState.uploadFiles;
                    tmpList[1] = e.target.files[0];

                    let tmpJsonStrList = detailState.snglImgFileJsonStr;
                    tmpJsonStrList[1] = "";

                    setDetailState({
                      ...detailState,
                      uploadFiles: tmpList,
                      snglImgFileJsonStr: tmpJsonStrList,
                      thmnl: "",
                      thmnlNm: e.target.files[0].name,
                    });
                  }}
                />
              </div>
            </div>
          </div>

          <div className="formWrapInputs">
            <div className="editorWrap">
              <CKEditor
                editor={ClassicEditor}
                data={detailState.intro}
                onChange={(event, editor) => {
                  detailState.intro = editor.getData();
                }}
              />
            </div>
          </div>
        </div>
        <div className="formWrap">
          <h2>
            <FormattedMessage
              id="detail.application.guideline"
              defaultMessage="상세 모집요강"
            />{" "}
            <em>
              (
              {intl.formatMessage({
                id: "detail.application.warning",
                defaultMessage:
                  "상세 모집요강은 선택된 리크루터에게만 공개됩니다.",
              })}
              )
            </em>
          </h2>

          <article style={{marginTop:'-20px', display:(guid02Cnt == 0)? 'none':'' }}>
              <table>
                <thead>
                  <tr>
                    <th>
                      <FormattedMessage
                        id="application.guideline"
                        defaultMessage="모집요강"
                      />
                    </th>
                    <th>
                      <FormattedMessage id="prologue" defaultMessage="머릿말" />
                    </th>
                    <th>
                      <FormattedMessage id="content" defaultMessage="내용" />
                    </th>
                    <th>
                      <FormattedMessage
                        id="file.attached"
                        defaultMessage="첨부파일"
                      />
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {bizGuidListState.map((type, idx) =>
                    type.brdDivi === "02" ? (
                      <tr>
                        <td>
                          {bizGuidDivi.map((type1, idx1) =>
                            type1.cateCd === type.brdDivi ? type1.cdName : ""
                          )}
                        </td>
                        <td>
                          {bizGuidKnd.map((type2, idx2) =>
                            type2.cateCd === type.brdKnd ? type2.cdName : ""
                          )}
                        </td>
                        <td>{type.cntnt}</td>
                        <td>
                          <span>{type.orignlFileNm}</span>
                            {
                              type.thmnl != '' ? 
                              (
                                <button type="button" className="basicButton" style={{marginLeft:'10px', display:(type.thmnl == '')? 'none' : ''}}
                                onClick={() => handleDownload(type.thmnl, type.orignlFileNm)}>
                                  <FormattedMessage
                                    id="file.download"
                                    defaultMessage="파일다운"
                                  />
                                </button>  
                              ) : ("")
                            }
                        </td>
                      </tr>
                    ) : (
                      ""
                    )
                  )}
                </tbody>
              </table>
            </article>    

          <div className="fileUploadGuide">
            <p>
              <FormattedMessage
                id="file.upload.guideline1"
                defaultMessage="※ 파일은 1개만 업로드 가능합니다."
              />
            </p>
            <p>
              <FormattedMessage
                id="file.upload.guideline2"
                defaultMessage="※ 2개 이상의 파일을 업로드하고 싶다면, 압축 파일(.zip) 형태로 1개의 파일만 업로드해주세요. (최대 10MB까지 첨부 가능)"
              />
            </p>
          </div>
          <div className="fileBoxWrap">
            <div className="fileBox flexAlignRight">
              <span className="fileName">{detailState.thmnlNm2}</span>
              <label htmlFor="fileInput2">
                <FormattedMessage
                  id="file.attachment"
                  defaultMessage="파일 첨부"
                />{" "}
                &gt;
              </label>

              <input
                type="file"
                id="fileInput2"
                className="fileInput"
                onChange={(e) => {
                  let tmpList = detailState.uploadFiles;
                  tmpList[2] = e.target.files[0];

                  let tmpJsonStrList = detailState.snglImgFileJsonStr;
                  tmpJsonStrList[2] = "";

                  setDetailState({
                    ...detailState,
                    uploadFiles: tmpList,
                    snglImgFileJsonStr: tmpJsonStrList,
                    thmnl2: "",
                    thmnlNm2: e.target.files[0].name,
                  });
                }}
              />
            </div>
          </div>
          <div className="formWrapInputs">
            <div className="infoBox">
              <FormattedMessage
                id="detail.application.description"
                defaultMessage="위크루트 매니저가 검수과정을 통해 별도로 입력합니다."
              />
            </div>
          </div>
        </div>
        <div className="formWrap">
          <h2>
            <FormattedMessage
              id="searching.keyword"
              defaultMessage="인재써칭 핵심 키워드"
            />
          </h2>
          <div className="formWrapInputs">
            <ul>
              <div className="tags tagsInput">
                <div className="tagsIn">
                  {detailState.tags.map((text, idx) => (
                    <span>
                      {text}
                      <button className="delete" type="button">
                        <span
                          className="material-icons"
                          onClick={() => {
                            rmvTag(idx);
                          }}
                        >
                          close
                        </span>
                      </button>
                    </span>
                  ))}

                  <input
                    type="text"
                    value={detailState.tagCntnt}
                    style={{ border: "none" }}
                    onChange={(e) => {
                      setDetailState({
                        ...detailState,
                        tagCntnt: e.target.value,
                      });
                    }}
                    onKeyUp={(e) => {
                      addTag(e);
                    }}
                  />
                </div>
              </div>
            </ul>
          </div>
        </div>
        <div className="formWrap">
          <h2>
            <FormattedMessage
              id="suggestion.setting"
              defaultMessage="제안 설정"
            />
          </h2>
          <div className="formWrapInputs">
            <ul>
              <li>
                <span className="labelSpan">
                  <FormattedMessage
                    id="suggestion.expected.salary"
                    defaultMessage="예상 연봉범위"
                  />
                </span>
                <div>
                  <span>
                    <FormattedMessage id="minimum" defaultMessage="최소" />
                    &nbsp;&nbsp;
                  </span>
                  <input
                    type="text"
                    value={detailState.salaryMin}
                    onChange={(e) => {
                      onChangeVal(e, "salaryMin");
                    }}
                  />{" "}
                  <FormattedMessage id="currency" defaultMessage="만원" />
                  &nbsp;&nbsp;~&nbsp;&nbsp;
                  <span>
                    <FormattedMessage id="maximum" defaultMessage="최대" />
                    &nbsp;&nbsp;
                  </span>
                  <input
                    type="text"
                    value={detailState.salaryMax}
                    onChange={(e) => {
                      onChangeVal(e, "salaryMax");
                    }}
                    onBlur={(e) => {
                      if (
                        parseInt(e.target.value) <
                        parseInt(detailState.salaryMin)
                      ) {
                        toast.error(
                          intl.formatMessage({
                            id: "toast.minmax.error",
                            defaultMessage:
                              "최대 금액은 최소 금액보다 커야 합니다.",
                          })
                        );
                        e.target.value = parseInt(detailState.salaryMin) + 100;
                      }
                    }}
                  />{" "}
                  <FormattedMessage id="currency" defaultMessage="만원" />
                </div>
              </li>
            </ul>
            <ul>
              <li>
                <span className="labelSpan">
                  <FormattedMessage
                    id="suggestion.service.fee"
                    defaultMessage="서비스 수수료율"
                  />
                </span>
                <div>
                  <span>
                    <FormattedMessage id="minimum" defaultMessage="최소" />
                    &nbsp;&nbsp;
                  </span>
                  <select
                    name="minCommision"
                    className="shortWidth"
                    disabled={detailState.srvcFeeFixYn === "Y" ? true : false}
                    value={detailState.srvcFeePctMin}
                    onChange={(e) => {
                      let targetVal = e.target.value;

                      if (
                        parseInt(e.target.value) >
                        parseInt(detailState.srvcFeePctMax)
                      ) {
                        toast.error(
                          intl.formatMessage({
                            id: "toast.minmax.fee.error",
                            defaultMessage:
                              "최소 수수료율이 최대 수수료율보다 큽니다.",
                          })
                        );
                        targetVal = detailState.srvcFeePctMax;
                      }

                      setDetailState({
                        ...detailState,
                        srvcFeePctMin: targetVal,
                      });
                    }}
                  >
                    {srvcFeePctMinList.map((srvcFeePctMin, idx) => {
                      if (srvcFeePctMin !== "") {
                        return (
                          <option key={srvcFeePctMin} value={srvcFeePctMin}>
                            {srvcFeePctMin}%
                          </option>
                        );
                      }
                    })}
                  </select>
                  <span>
                    &nbsp;&nbsp;~&nbsp;&nbsp;
                    <FormattedMessage id="maximum" defaultMessage="최대" />
                    &nbsp;&nbsp;
                  </span>
                  <select
                    name="maxCommision"
                    className="shortWidth"
                    disabled={detailState.srvcFeeFixYn === "Y" ? true : false}
                    value={detailState.srvcFeePctMax}
                    onChange={(e) => {
                      let targetVal = e.target.value;

                      if (
                        parseInt(e.target.value) <
                        parseInt(detailState.srvcFeePctMin)
                      ) {
                        toast.error(
                          intl.formatMessage({
                            id: "toast.maxmin.fee.error",
                            defaultMessage:
                              "최대 수수료율이 최소 수수료율보다 작습니다.",
                          })
                        );
                      }

                      setDetailState({
                        ...detailState,
                        srvcFeePctMax: targetVal,
                      });
                    }}
                  >
                    {srvcFeePctMinList.map((srvcFeePctMin, idx) => {
                      if (srvcFeePctMin !== "") {
                        return (
                          <option key={srvcFeePctMin} value={srvcFeePctMin}>
                            {srvcFeePctMin}%
                          </option>
                        );
                      }
                    })}
                  </select>
                  &nbsp;&nbsp;&nbsp;&nbsp;
                  <input
                    type="checkbox"
                    id="certainAmount"
                    onClick={(e) => {
                      isSrvcFeeFixYnChecked(e);
                    }}
                  />
                  <label htmlFor="certainAmount">
                    <FormattedMessage
                      id="suggestion.service.fee.warning"
                      defaultMessage="또는 정액 (부가세 별도입니다.)"
                    />
                  </label>
                </div>
              </li>
            </ul>
            <ul>
              <li>
                <label htmlFor="guaranteeCondition">
                  <FormattedMessage
                    id="suggestion.service.guarantee"
                    defaultMessage="서비스 보증조건"
                  />
                </label>
                <div>
                  <select
                    name="guaranteeCondition"
                    id="guaranteeCondition"
                    className="shortWidth"
                    value={detailState.srvcWrtyPeriod}
                    onChange={(e) => {
                      onChangeVal(e, "srvcWrtyPeriod");
                    }}
                  >
                    {months.map((month, idx) => (
                      <option key={month} value={month}>
                        {month}
                        {intl.formatMessage({
                          id: "months",
                          defaultMessage: "개월",
                        })}
                      </option>
                    ))}
                  </select>
                  <span>
                    &nbsp;&nbsp;
                    <FormattedMessage
                      id="suggestion.incase.retire"
                      defaultMessage="이내 퇴직시"
                    />
                    &nbsp;&nbsp;
                  </span>
                  <select
                    name="guaranteeCondition"
                    id="guaranteeCondition"
                    className="shortWidth"
                    value={detailState.srvcWrtyMthd}
                    onChange={(e) => {
                      onChangeVal(e, "srvcWrtyMthd");
                    }}
                  >
                    <option value="prorated">
                      {intl.formatMessage({
                        id: "suggestion.caculated.refund",
                        defaultMessage: "일할계산 환불",
                      })}
                    </option>
                    <option value="all">
                      {intl.formatMessage({
                        id: "suggestion.all.refund",
                        defaultMessage: "100% 전액 환불",
                      })}
                    </option>
                    <option value="etc">
                      {intl.formatMessage({
                        id: "etc",
                        defaultMessage: "기타",
                      })}
                    </option>
                  </select>
                </div>
              </li>
            </ul>
            <ul>
              <li>
                <span className="labelSpan">
                  <FormattedMessage
                    id="resume.format"
                    defaultMessage="이력서 양식"
                  />
                </span>
                <div>
                  <div className="radioInput resumeClickEvent">
                    <input
                      type="radio"
                      id="recruiterForm"
                      name="resumeForm"
                      value="recForm"
                      checked={!formChecked}
                      onChange={(e) => {
                        radioChangeHandler(e);
                      }}
                    />
                    <label htmlFor="recruiterForm">
                      <FormattedMessage
                        id="resume.format.free"
                        defaultMessage="리크루터 자유 양식 이력서"
                      />
                    </label>
                    <input
                      type="radio"
                      id="companyForm"
                      name="resumeForm"
                      value="cmpnyForm"
                      className="resumeUpload"
                      checked={formChecked}
                      onChange={(e) => {
                        radioChangeHandler(e);
                      }}
                    />
                    <label htmlFor="companyForm">
                      <FormattedMessage
                        id="resume.format.compnay"
                        defaultMessage="회사 소정 양식 이력서"
                      />
                    </label>
                  </div>

                  {formChecked === true ? (
                    <div className="fileBox inlineFileBox">
                      <div>
                        <label htmlFor="fileInput3">
                          <FormattedMessage
                            id="resume.file.attachment"
                            defaultMessage="이력서 양식 첨부"
                          />{" "}
                          &gt;
                        </label>
                        <span className="fileName">
                          {detailState.resumeFormThmnlNm}
                        </span>
                        <input
                          type="file"
                          id="fileInput3"
                          className="fileInput"
                          onChange={(e) => {
                            let tmpList = detailState.uploadFiles;
                            tmpList[3] = e.target.files[0];

                            let tmpJsonStrList = detailState.snglImgFileJsonStr;
                            tmpJsonStrList[3] = "";

                            setDetailState({
                              ...detailState,
                              uploadFiles: tmpList,
                              snglImgFileJsonStr: tmpJsonStrList,
                              resumeFormThmnl: "",
                              resumeFormThmnlNm: e.target.files[0].name,
                            });
                          }}
                        />
                      </div>
                      <div
                        className="fileUploadGuide"
                        style={{ marginTop: "10px" }}
                      >
                        <p>
                          <FormattedMessage
                            id="file.upload.guideline1"
                            defaultMessage="※ 파일은 1개만 업로드 가능합니다."
                          />
                        </p>
                        <p>
                          <FormattedMessage
                            id="file.upload.guideline2"
                            defaultMessage="※ 2개 이상의 파일을 업로드하고 싶다면, 압축 파일(.zip) 형태로 1개의 파일만 업로드해주세요. (최대 10MB까지 첨부 가능)"
                          />
                        </p>
                      </div>
                    </div>
                  ) : null}
                </div>
              </li>
            </ul>
          </div>
        </div>
        <div className="formWrap">
          <div className="formWrapInputs">
            <ul>
              <li>
                <span className="labelSpan">
                  <FormattedMessage
                    id="recommend.recruiter"
                    defaultMessage="우수 리크루터 추천"
                  />
                </span>
                <div>
                  <p>
                    <FormattedMessage
                      id="recommendation.wecruit.suggestion"
                      defaultMessage="위크루트가 추천하는 우수 리크루터 제안 받기"
                    />
                  </p>
                  <div className="radioInput">
                    <input
                      type="radio"
                      id="recommendAgree"
                      name="recommendQuestion"
                      checked={detailState.rcmndRecYn === "Y" ? true : false}
                      onChange={() => {
                        setDetailState({
                          ...detailState,
                          rcmndRecYn: "Y",
                        });
                      }}
                    />
                    <label htmlFor="recommendAgree">
                      <FormattedMessage
                        id="no.disagree"
                        defaultMessage="예, 동의합니다."
                      />
                    </label>
                    <input
                      type="radio"
                      id="recommendDisagree"
                      name="recommendQuestion"
                      checked={detailState.rcmndRecYn === "N" ? true : false}
                      onChange={() => {
                        setDetailState({
                          ...detailState,
                          rcmndRecYn: "N",
                        });
                      }}
                    />
                    <label htmlFor="recommendDisagree">
                      <FormattedMessage id="no" defaultMessage="아니오" />
                    </label>
                  </div>
                  <button
                    type="button"
                    className="modalOpenButton"
                    onClick={openBestRecModal}
                  >
                    <FormattedMessage
                      id="recommend.recruiter.program"
                      defaultMessage="우수 리크루터 추천 프로그램 자세히 보기"
                    />
                  </button>
                  {
                    <Modal
                      visible={bestRecModal}
                      closable={true}
                      maskClosable={true}
                      onClose={closeBestRecModal}
                      nobutton={true}
                    >
                      <RecommendProgram />
                    </Modal>
                  }
                </div>
              </li>
            </ul>
            <ul>
              <li>
                <span className="labelSpan">
                  <FormattedMessage
                    id="recruit.method"
                    defaultMessage="리크루터 모집방법"
                  />
                </span>
                <div>
                  <div className="radioList">
                    <div
                      className="radio1 fullWidth"
                      style={{ padding: "5px 10px" }}
                    >
                      <input
                        type="radio"
                        id="recruitMethod1"
                        name="recruitMethod"
                        checked={detailState.recMthd === "rec" ? true : false}
                        onChange={() => {
                          setDetailState({
                            ...detailState,
                            recMthd: "rec",
                          });
                        }}
                      />
                      <label htmlFor="recruitMethod1">
                        <FormattedMessage
                          id="recruit.method1"
                          defaultMessage="위크루트에 등록된 리크루터만 참여 가능"
                        />
                      </label>
                    </div>
                    <div
                      className="radio2 fullWidth"
                      style={{ padding: "5px 10px" }}
                    >
                      <input
                        type="radio"
                        id="recruitMethod2"
                        name="recruitMethod"
                        checked={
                          detailState.recMthd === "recAndSrchfirm"
                            ? true
                            : false
                        }
                        onChange={() => {
                          setDetailState({
                            ...detailState,
                            recMthd: "recAndSrchfirm",
                          });
                        }}
                      />
                      <label htmlFor="recruitMethod2">
                        <FormattedMessage
                          id="recruit.method2"
                          defaultMessage="위크루트에 등록된 리크루터 + 기존 거래 써치펌 참여 가능"
                        />
                      </label>
                    </div>
                    <div
                      className="radio3 fullWidth"
                      style={{ padding: "5px 10px" }}
                    >
                      <input
                        type="radio"
                        id="recruitMethod3"
                        name="recruitMethod"
                        checked={
                          detailState.recMthd === "srchfirm" ? true : false
                        }
                        onChange={() => {
                          setDetailState({
                            ...detailState,
                            recMthd: "srchfirm",
                          });
                        }}
                      />
                      <label htmlFor="recruitMethod3">
                        <FormattedMessage
                          id="recruit.method3"
                          defaultMessage="기존 거래 써치펌만 참여 가능"
                        />
                      </label>
                    </div>
                  </div>
                </div>
              </li>
            </ul>
            <ul>
              <li>
                <span className="labelSpan">
                  <FormattedMessage
                    id="attend.mail"
                    defaultMessage="참여요청 메일발송"
                  />
                </span>
                <div>
                  <button
                    type="button"
                    className="formButton"
                    onClick={openSerchFirmRegModal}
                  >
                    <FormattedMessage
                      id="attend.mail.existing"
                      defaultMessage="기존 써치펌에 인재추천 참여 요청하기"
                    />{" "}
                    &gt;
                  </button>
                  <span>
                    &nbsp;&nbsp;
                    <FormattedMessage
                      id="attend.mail.free"
                      defaultMessage="* 기존 써치펌은 무료로 이용가능합니다."
                    />
                  </span>
                </div>
              </li>
            </ul>
            <ul>
              <li>
                <span className="labelSpan">
                  <FormattedMessage
                    id="department.match"
                    defaultMessage="현업부서 매칭하기"
                  />
                </span>
                <div className="flex">
                  <div>
                    <button
                      type="button"
                      className="formButton"
                      onClick={openDprtMatchModal}
                    >
                      <FormattedMessage
                        id="department.match.id"
                        defaultMessage="현업부서 아이디와 현업부서 매칭하기"
                      />{" "}
                      &gt;
                    </button>
                    <span className="marginInline">
                      <FormattedMessage
                        id="department.description"
                        defaultMessage="* 현업부서도 해당 채용공고를 관리할 수 있습니다."
                      />
                    </span>
                  </div>
                  <div className="displayBlock marginInline">
                    <button
                      type="button"
                      className="modalButtonStyling"
                      onClick={openAuthModal}
                    >
                      <FormattedMessage
                        id="department.access.info"
                        defaultMessage="권한정보"
                      />
                    </button>
                    {
                      <Modal
                        visible={authModal}
                        closable={true}
                        maskClosable={true}
                        onClose={closeAuthModal}
                        nobutton={true}
                      >
                        <AccessInfo />
                      </Modal>
                    }
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
        <div className="formWrap">
          <h2>
            <FormattedMessage
              id="employer.contact.info.open"
              defaultMessage="인사담당자 연락처 공개 설정"
            />
            <em>
              <FormattedMessage
                id="employer.contact.info.open.alert1"
                defaultMessage="(연락처 공개 설정은 선택된 리크루터에게만 공개됩니다.)"
              />
            </em>
          </h2>
          <div className="formWrapInputs">
            <ul>
              <li>
                <div className="checkboxList">
                  <input
                    type="checkbox"
                    id="employerEmailDisposal"
                    checked={detailState.emailPubYn === "Y" ? true : false}
                    onChange={(e) => {
                      setDetailState({
                        ...detailState,
                        emailPubYn: e.target.checked ? "Y" : "N",
                      });
                    }}
                  />
                  <label htmlFor="employerEmailDisposal">
                    <FormattedMessage id="email" defaultMessage="이메일" />
                  </label>
                  <input
                    type="checkbox"
                    id="employerSnsDisposal"
                    checked={detailState.snsPubYn === "Y" ? true : false}
                    onChange={(e) => {
                      setDetailState({
                        ...detailState,
                        snsPubYn: e.target.checked ? "Y" : "N",
                      });
                    }}
                  />
                  <label htmlFor="employerSnsDisposal">
                    <FormattedMessage
                      id="sns"
                      defaultMessage="SNS (라인, 카카오톡 등)"
                    />
                  </label>
                  <input
                    type="checkbox"
                    id="employerOfficeDisposal"
                    checked={detailState.phNumPubYn === "Y" ? true : false}
                    onChange={(e) => {
                      setDetailState({
                        ...detailState,
                        phNumPubYn: e.target.checked ? "Y" : "N",
                      });
                    }}
                  />
                  <label htmlFor="employerOfficeDisposal">
                    <FormattedMessage
                      id="office.number"
                      defaultMessage="사무실 전화"
                    />
                  </label>
                  <input
                    type="checkbox"
                    id="employerPhoneDisposal"
                    checked={detailState.celpNumPubYn === "Y" ? true : false}
                    onChange={(e) => {
                      setDetailState({
                        ...detailState,
                        celpNumPubYn: e.target.checked ? "Y" : "N",
                      });
                    }}
                  />
                  <label htmlFor="employerPhoneDisposal">
                    <FormattedMessage
                      id="phone.number"
                      defaultMessage="휴대폰 번호"
                    />
                  </label>
                  <input
                    type="checkbox"
                    id="employerAddressDisposal"
                    checked={detailState.addrPubYn === "Y" ? true : false}
                    onChange={(e) => {
                      setDetailState({
                        ...detailState,
                        addrPubYn: e.target.checked ? "Y" : "N",
                      });
                    }}
                  />
                  <label htmlFor="employerAddressDisposal">
                    <FormattedMessage
                      id="address.open"
                      defaultMessage="주소공개 및 방문"
                    />
                  </label>
                  <p>
                    <FormattedMessage
                      id="employer.contact.info.open.alert2"
                      defaultMessage="* 위크루트는 Q&amp;A 게시판에 댓글을 남기시면 자동으로 담당자 업무용 이메일로 해당 내용이 발송됩니다."
                    />
                  </p>
                </div>
              </li>
            </ul>
          </div>
        </div>
        <div className="flexAlignRight">
          <button
            type="button"
            className="formButton"
            onClick={() => {
              goCRUD("tmp");
            }}
          >
            <FormattedMessage id="temp.save" defaultMessage="임시저장" /> &gt;
          </button>
        </div>
        <div className="flexAlignCenter">
          <input
            type="button"
            value={`${intl.formatMessage({
              id: "job.posting.submit",
              defaultMessage: "채용공고 등록하기",
            })} >`}
            className="bigButton blueColored"
            onClick={() => {
              goCRUD("confirm");
            }}
          />
        </div>
      </form>

      {
        <Modal
          visible={serchFirmRegModal}
          closable={true}
          maskClosable={true}
          onClose={closeSerchFirmRegModal}
          nobutton={true}
        >
          <>
            <h4>
              <FormattedMessage
                id="attend.mail.existing"
                defaultMessage="기존 써치펌에 인재추천 참여 요청하기"
              />
            </h4>
            <div className="infoBox">
              <FormattedMessage
                id="attend.mail.description"
                defaultMessage="본 메일은 채용공고 검수가 완료되면 검수된 내용으로 발송됩니다."
              />
            </div>
            <p className="mailContent">
              <FormattedMessage id="hello" defaultMessage="안녕하세요?" />{" "}
              <FormattedMessage
                id="dear.recruiter"
                defaultMessage="리크루터님,"
              />{" "}
              <br />
              &#60;{detailState.title == "" ? "-" : detailState.title}&#62;{" "}
              <FormattedMessage
                id="description.recommend.request1"
                defaultMessage="에 인재추천 참여 바랍니다."
              />{" "}
              <br />
              <FormattedMessage
                id="description.recommend.request2"
                defaultMessage="위크루트 시스템에 로그인하시면 참여신청 및 인재추천이 가능합니다."
              />
            </p>
            <big>
              <FormattedMessage
                id="email.address.input"
                defaultMessage="이메일 주소 입력"
              />{" "}
              ({" "}
              <FormattedMessage
                id="description.recommend.request3"
                defaultMessage="숨은 참조로 발송됩니다."
              />{" "}
              {detailState.srchFirms ? detailState.srchFirms.length : "0"} / 30
              <FormattedMessage id="count" defaultMessage="개" />)
            </big>
            <form>
              <div className="tagList">
                {detailState.srchFirms
                  ? detailState.srchFirms.map((text, idx) => (
                      <span
                        style={{
                          display: "inline-block",
                          background: "#0F7EC3",
                          color: "#ffffff",
                          borderRadius: "20px",
                          padding: "5px 12px 2px 12px",
                          marginRight: "3px",
                          marginBottom: "3px",
                        }}
                      >
                        {text}
                        <button
                          style={{ float: "right" }}
                          className="delete"
                          type="button"
                        >
                          <span
                            className="material-icons"
                            style={{ color: "#ffffff" }}
                            onClick={() => {
                              rmvSrchFrim(idx);
                            }}
                          >
                            close
                          </span>
                        </button>
                      </span>
                    ))
                  : ""}
              </div>
            </form>

            <ul>
              <li>
                {intl.formatMessage(messages.existingSearchfirm, {
                  strong: (str) => <strong>{str}</strong>,
                })}
              </li>
              <li>
                <FormattedMessage
                  id="description.searchfirm2"
                  defaultMessage="(단, 계약내용에 따라 거래의 안전을 위한 별도의 보증보험료는 상호 협의하에 리크루터에게 발생할 수도 있습니다.)"
                />
              </li>
              <li>
                리크루터분들은{" "}
                <strong>
                  수신한 이메일로 회원 가입을 한 경우에 한하여 무료
                </strong>
                로 이용하실 수 있습니다.
              </li>
            </ul>
            <div className="buttons">
              <button
                type="button"
                className="greyColoredBtn"
                onClick={openSerchFirmListModal}
              >
                <FormattedMessage
                  id="get.address"
                  defaultMessage="주소록 불러오기"
                />
              </button>
              <button
                type="button"
                className="greyColoredBtn"
                onClick={openSerchFirmModal}
                style={{ marginRight: "10px" }}
              >
                주소록 추가하기
              </button>
              <button type="button" onClick={closeSerchFirmRegModal}>
                <FormattedMessage
                  id="recruitment.join.request"
                  defaultMessage="채용공고 참여 요청하기"
                />
              </button>
            </div>
          </>
        </Modal>
      }

      {
        <DoubleModal
          visible={serchFirmListModal}
          closable={true}
          maskClosable={true}
          onClose={closeSerchFirmListModal}
          inputName={intl.formatMessage({
            id: "add",
            defaultMessage: "추가하기",
          })}
          closeName={intl.formatMessage({
            id: "delete",
            defaultMessage: "삭제하기",
          })}
          buttonAction={addSrchFrim}
          closeAction={rmvAllSrchFrim}
        >
          <div>
            <h4>
              <FormattedMessage
                id="exsting.searchfirm.address"
                defaultMessage="기존 거래 써치펌 주소록"
              />
            </h4>
            <p>
              <FormattedMessage
                id="description.searchfirm1"
                defaultMessage="※ 채용공고에서 기존 거래 써치펌으로 리크루터를 등록하시면, 별도의 중개 수수료 없이 해당 리크루터(헤드헌터)와 거래하실 수 있습니다."
              />
              <br />
              <FormattedMessage
                id="description.searchfirm2"
                defaultMessage="(단, 계약내용에 따라 거래의 안전을 위한 별도의 보증보험료는 상호 협의하에 리크루터에게 발생할 수도 있습니다.)"
              />
            </p>
            <table>
              <thead>
                <tr>
                  <th>
                    <span>
                      <input
                        type="checkbox"
                        id="selectAllSrchFrim"
                        name="selectAllSrchFrim"
                        onClick={(e) => {
                          checkAllSrchFrim(e);
                        }}
                      />
                    </span>
                  </th>
                  <th>
                    <span>
                      <FormattedMessage id="number" defaultMessage="번호" />
                    </span>
                  </th>
                  <th
                    className={
                      srchFrimFltrState.recNm === "DESC"
                        ? "descending"
                        : srchFrimFltrState.recNm === "ASC"
                        ? "ascending"
                        : undefined
                    }
                    onClick={() => {
                      changeFltrSrchFrim("recNm", srchFrimFltrState.recNm);
                    }}
                  >
                    <span>
                      <FormattedMessage
                        id="recruiter.name"
                        defaultMessage="리크루터 이름"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </th>
                  <th
                    className={
                      srchFrimFltrState.cmpnyNm === "DESC"
                        ? "descending"
                        : srchFrimFltrState.cmpnyNm === "ASC"
                        ? "ascending"
                        : undefined
                    }
                    onClick={() => {
                      changeFltrSrchFrim("cmpnyNm", srchFrimFltrState.cmpnyNm);
                    }}
                  >
                    <span>
                      <FormattedMessage
                        id="searchfirm.assigned"
                        defaultMessage="소속 써치펌"
                      />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </th>
                  <th
                    className={
                      srchFrimFltrState.email === "DESC"
                        ? "descending"
                        : srchFrimFltrState.email === "ASC"
                        ? "ascending"
                        : undefined
                    }
                    onClick={() => {
                      changeFltrSrchFrim("email", srchFrimFltrState.email);
                    }}
                  >
                    <span>
                      <FormattedMessage id="email" defaultMessage="이메일" />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </th>
                  <th
                    className={
                      srchFrimFltrState.celpNum === "DESC"
                        ? "descending"
                        : srchFrimFltrState.celpNum === "ASC"
                        ? "ascending"
                        : undefined
                    }
                    onClick={() => {
                      changeFltrSrchFrim("celpNum", srchFrimFltrState.celpNum);
                    }}
                  >
                    <span>
                      <FormattedMessage id="phone" defaultMessage="휴대폰" />
                    </span>
                    <div className="filterArrow">
                      <span />
                      <span />
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody>
                {searchFirmList.map((list, idx) => {
                  return (
                    <tr key={list.srchfirmUid}>
                      <td>
                        <input
                          type="checkbox"
                          checked={list.chkYn === "Y" ? true : false}
                          onChange={(e) => {
                            onChangeSrchFirmChk(e, idx);
                          }}
                        />
                      </td>
                      <td>{idx + 1}</td>
                      <td>{list.recNm}</td>
                      <td>{list.cmpnyNm}</td>
                      <td>{list.email}</td>
                      <td>{list.celpNum}</td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </DoubleModal>
      }

      {
        <DoubleModal
          visible={serchFirmModal}
          closable={true}
          maskClosable={true}
          onClose={closeSerchFirmModal}
          onSubmit={goSrchFirmCRUD}
          inputName="등록하기 >"
          buttonAction={goSrchFirmCRUD}
          closeAction={closeSerchFirmModal}
        >
          <h4>
            <FormattedMessage
              id="search.firm.address.register"
              defaultMessage="기존 거래 써치펌 주소록 등록"
            />
          </h4>
          <p className="textAlignCenter">
            <FormattedMessage
              id="description.search.firm.register"
              values={{
                breakLine: <br />,
              }}
              defaultMessage="주소록에는 최대 30명의 리크루터를 등록 할 수 있습니다.{breakLine}
                          등록된 리크루터는 채용공고 등록하기 화면에서{breakLine}
                          기존 거래 써치펌(리크루터)로 등록할 수 있습니다"
            />
          </p>
          <form className="employersForm" onKeyDown={onKeyDownForm}>
            <div className="inputWidthFull directionColumn">
              <label htmlFor="recruiterName">
                <FormattedMessage
                  id="recruiter.name"
                  defaultMessage="리쿠르터 이름"
                />{" "}
                <span>*</span>
              </label>
              <input
                type="text"
                id="recruiterName"
                className="fullWidthInput"
                placeholder={intl.formatMessage({
                  id: "placeholder.name",
                  defaultMessage: "이름을 입력하세요.",
                })}
                value={searchFirm.recNm}
                onChange={(e) => {
                  setSearchFirm({
                    ...searchFirm,
                    recNm: e.target.value,
                  });
                }}
              />
            </div>
            <div className="inputWidthFull directionColumn">
              <label htmlFor="searchFirm">
                <FormattedMessage
                  id="searchfirm.belong.to"
                  defaultMessage="소속 써치펌"
                />
              </label>
              <input
                type="text"
                id="searchFirm"
                placeholder={intl.formatMessage({
                  id: "placeholder.search.firm.belong.to",
                  defaultMessage: "소속 써치펌을 입력해주세요",
                })}
                value={searchFirm.cmpnyNm}
                onChange={(e) => {
                  setSearchFirm({
                    ...searchFirm,
                    cmpnyNm: e.target.value,
                  });
                }}
              />
            </div>
            <div className="inputWidthFull directionColumn">
              <label htmlFor="email">
                <FormattedMessage id="email" defaultMessage="이메일" />{" "}
                <span>*</span>
              </label>
              <input
                type="text"
                id="email"
                placeholder={intl.formatMessage({
                  id: "placeholder.email",
                  defaultMessage: "이메일을 입력해주세요.",
                })}
                value={searchFirm.email}
                onChange={(e) => {
                  setSearchFirm({
                    ...searchFirm,
                    email: e.target.value,
                  });
                }}
              />
            </div>
            <div className="inputWidthFull directionColumn">
              <label htmlFor="phone">
                <FormattedMessage id="phone" defaultMessage="휴대폰" />
              </label>
              <input
                type="text"
                placeholder={intl.formatMessage({
                  id: "placeholder.phone.number",
                  defaultMessage: '"- "없이 숫자만 입력하세요.',
                })}
                value={searchFirm.celpNum}
                onChange={(e) => {
                  setSearchFirm({
                    ...searchFirm,
                    celpNum: e.target.value,
                  });
                }}
              />
            </div>
          </form>
        </DoubleModal>
      }

      {
        <Modal
          visible={dprtMatchModal}
          closable={true}
          maskClosable={true}
          onSubmit={addMatchDprt}
          onClose={rmvMatchDprt}
          inputName={
            <FormattedMessage
              id="department.matching"
              defaultMessage="매칭하기"
            />
          }
          closeName={
            <FormattedMessage
              id="department.matching.cancel"
              defaultMessage="매칭취소"
            />
          }
        >
          <>
            <h4>
              <FormattedMessage
                id="department.matching.toggle"
                defaultMessage="현업부서 매칭/취소"
              />
            </h4>
            <div className="matchingList">
              {myDprtMbrlist.map((list, idx) => {
                return (
                  <div>
                    <input
                      type="radio"
                      name="departmentItem"
                      checked={list.chkYn === "Y" ? true : false}
                      onChange={(e) => {
                        onChangeMatchDprtChk(e, idx);
                      }}
                    />
                    <label>{list.mbrId}</label>
                  </div>
                );
              })}
            </div>
          </>
        </Modal>
      }
    </main>
  );
};
