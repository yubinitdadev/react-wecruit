import * as commonUtil from "commons/modules/commonUtil";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { Context } from "contexts";
import { UPD_MBRUID } from "contexts/actionTypes";
import { useContext, useEffect, useRef, useState } from "react";
import { ToastContainer } from "react-toastify";
import { CopyToClipboard } from "react-copy-to-clipboard";
import { FormattedMessage, useIntl } from "react-intl";

export const Main = () => {
  const intl = useIntl();

  const {
    state: { server, mbr },
    dispatch,
  } = useContext(Context);

  let ssn = {
    ssnMbrUid: window.localStorage.getItem("ssnMbrUid"),
    ssnMbrDivi: window.localStorage.getItem("ssnMbrDivi"),
    ssnRefUid: window.localStorage.getItem("ssnRefUid"),
  };

  const moment = require("moment");
  const navigate = useNavigate();
  const componentRef = useRef();

  const [listState, setListState] = useState([]);
  const [listCntState, setListCntState] = useState({});

  const [sContsKnd, setSContsKnd] = useState("");
  const [sConts, setSConts] = useState("");
  const [fltrState, setFltrState] = useState({});

  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(10);

  let cnfrmChkCnt = 0;
  let ingChkCnt = 0;
  let endChkCnt = 0;

  useEffect(async () => {
    window.scrollTo(0, 0);
    initSetHandle();
  }, []);

  const initSetHandle = async () => {
    let goUrl = server.path + "/hire/hirelist";
    let data = {
      ssnMbrUid: ssn.ssnMbrUid,
      mbrUid: ssn.ssnMbrUid,
      refUid: ssn.ssnRefUid,
      mbrDivi: ssn.ssnMbrDivi,
      hireStat: "",
    };

    let axiosRes = await commonUtil.axiosCrudOnSubmit(data, goUrl, "POST");
    let axiosResData = axiosRes.data.hireArr;
    let tmpAxiosResData = [];

    let ingCnt = 0;
    let cnfrmCnt = 0;
    let endCnt = 0;
    axiosResData.forEach((resData, idx) => {
      resData["rcmndEndDttm"] = moment(resData.rcmndEndDttm).format(
        "YYYY-MM-DD HH:mm"
      );

      tmpAxiosResData.push(resData);
      if (resData.hireStat === "ing") ingCnt++;
      else if (resData.hireStat === "confirm") cnfrmCnt++;
      else if (resData.hireStat === "end") endCnt++;
    });

    setListState(tmpAxiosResData);
    var tmpCnt = {
      ingCnt: ingCnt,
      cnfrmCnt: cnfrmCnt,
      endCnt: endCnt,
    };
    setListCntState(tmpCnt);
  };

  ////////////////////////////////////////////////////////////////////////////////////////
  function onKeyDown(keyEvent) {
    if ((keyEvent.charCode || keyEvent.keyCode) === 13) {
      keyEvent.preventDefault();
    }
  }

  const onKeyUp = (e) => {
    if (e.key == "Enter") {
      document.getElementById("srchLink").click();
    }
    return false;
  };

  function onKeyDown(keyEvent) {
    if ((keyEvent.charCode || keyEvent.keyCode) === 13) {
      keyEvent.preventDefault();
    }
  }

  return (
    <main className="dividedSections employersMain noBorder">
      <section className="recruitNoticeStats">
        <div>
          <p>
            <FormattedMessage
              id="posting.confirm"
              defaultMessage="검수중 채용공고"
            />
          </p>
          <Link to="/employers/confirm">{listCntState.cnfrmCnt}</Link>
        </div>
        <div>
          <p>
            <FormattedMessage
              id="posting.ing"
              defaultMessage="진행중 채용공고"
            />
          </p>
          <Link to="/employers/ing">{listCntState.ingCnt}</Link>
        </div>
        <div>
          <p>
            <FormattedMessage
              id="posting.done"
              defaultMessage="종료된 채용공고"
            />
          </p>
          <Link to="/employers/done">{listCntState.endCnt}</Link>
        </div>
      </section>
      <section>
        <form className="searchForm employersForm" onKeyDown={onKeyDown}>
          <select
            name="sContsKnd"
            id="sContsKnd"
            onChange={(e) => {
              setSContsKnd(e.target.value);
            }}
          >
            <option value="cnddtNm">
              {intl.formatMessage({
                id: "candidate",
                defaultMessage: "후보자",
              })}
            </option>
            <option value="recNm">
              {intl.formatMessage({
                id: "recruiter",
                defaultMessage: "리크루터",
              })}
            </option>
          </select>
          <input
            type="search"
            name="sConts"
            id="sConts"
            onChange={(e) => {
              setSConts(e.target.value);
            }}
            onKeyUp={(e) => {
              onKeyUp(e);
            }}
            placeholder={intl.formatMessage({
              id: "placeholder.name",
              defaultMessage: "이름을 입력하세요.",
            })}
          />

          <Link
            id="srchLink"
            name="srchLink"
            to="/employers/search"
            state={{ sContsKnd: sContsKnd, sConts: sConts }}
          >
            <input
              type="button"
              value={intl.formatMessage({
                id: "search",
                defaultMessage: "검색",
              })}
              className="basicButton"
            />
          </Link>
        </form>
      </section>
      <section className="recruitLists">
        <div className="confirmingList">
          <h2 className="sectionTitle">
            <span>
              <FormattedMessage
                id="posting.confirm"
                defaultMessage="검수중 채용공고"
              />
            </span>
            <Link to="/employers/confirm">
              <FormattedMessage id="show.all" defaultMessage="전체보기" /> &gt;
            </Link>
          </h2>
          <div
            className="noRecruitList"
            style={{ display: listCntState.cnfrmCnt == 0 ? "" : "none" }}
          >
            <FormattedMessage
              id="description.no.confirming.posting"
              defaultMessage="검수중인 채용공고가 없습니다."
            />
          </div>
          <ul>
            {listState.map((list, idx) =>
              list.hireStat === "confirm" ? (
                (cnfrmChkCnt = cnfrmChkCnt + 1) < 4 ? (
                  <li>
                    <div className="listLeft">
                      <h3>
                        <Link
                          to="/employers/confirm-detail"
                          state={{ hireUid: list.hireUid, mbrUid: list.mbrUid }}
                        >
                          {list.title}
                        </Link>
                      </h3>
                      <p>
                        <FormattedMessage
                          id="registration.date"
                          defaultMessage="등록일"
                        />{" "}
                        :&nbsp;{moment(list.rgstDttm).format("YYYY-MM-DD")}
                        &nbsp; /{" "}
                        <FormattedMessage
                          id="recruitment.deadline"
                          defaultMessage="인재추천 마감일"
                        />
                        :&nbsp;
                        {list.rcmndUntilDon === "Y"
                          ? `${intl.formatMessage({
                              id: "until.hired",
                              defaultMessage: "채용시까지",
                            })}`
                          : list.rcmndEndDttm}
                        &nbsp; /{" "}
                        <FormattedMessage
                          id="recruitment.people.number"
                          defaultMessage="모집인원"
                        />
                        : {list.hireSize}
                        <FormattedMessage
                          id="people.count"
                          defaultMessage="명"
                        />
                      </p>
                      <p>
                        <FormattedMessage
                          id="suggestion.service.fee"
                          defaultMessage="서비스 수수료율"
                        />{" "}
                        :&nbsp;
                        {list.srvcFeeFixYn === "Y"
                          ? `${intl.formatMessage({
                              id: "suggestion.fixed.fee.system",
                              defaultMessage: "정액제",
                            })}`
                          : list.srvcFeePctMin +
                            "% ~ " +
                            list.srvcFeePctMax +
                            "%"}
                        &nbsp;/{" "}
                        <FormattedMessage
                          id="suggestion.service.guarantee"
                          defaultMessage="서비스 보증조건"
                        />{" "}
                        :&nbsp;
                        {list.srvcWrtyPeriod}
                        <FormattedMessage
                          id="months"
                          defaultMessage="개월"
                        />{" "}
                        <FormattedMessage
                          id="suggestion.incase.retire"
                          defaultMessage="이내 퇴직시"
                        />
                        {list.srvcWrtyMthd === "prorated"
                          ? `${intl.formatMessage({
                              id: "suggestion.caculated.refund",
                              defaultMessage: "일할계산 환불",
                            })}`
                          : list.srvcWrtyMthd === "all"
                          ? `${intl.formatMessage({
                              id: "suggestion.all.refund",
                              defaultMessage: "100% 전액 환불",
                            })}`
                          : list.srvcWrtyMthd === "etc"
                          ? `${intl.formatMessage({
                              id: "etc",
                              defaultMessage: "기타",
                            })}`
                          : ""}
                      </p>
                    </div>
                    {list.hireStat === "confirm" ? (
                      <span className="confirmingStatus">
                        <FormattedMessage
                          id="status.confirming"
                          defaultMessage="검수중"
                        />
                      </span>
                    ) : list.hireStat === "declined" ? (
                      <span className="confirmingStatus rejected">
                        <FormattedMessage id="refuse" defaultMessage="거절" />
                      </span>
                    ) : (
                      ""
                    )}
                  </li>
                ) : (
                  ""
                )
              ) : (
                ""
              )
            )}
          </ul>
        </div>
        <div className="ingList">
          <h2 className="sectionTitle">
            <span>
              <FormattedMessage
                id="posting.ing"
                defaultMessage="진행중 채용공고"
              />
            </span>
            <Link to="/employers/ing">
              <FormattedMessage id="show.all" defaultMessage="전체보기" /> &gt;
            </Link>
          </h2>
          <div
            className="noRecruitList"
            style={{ display: listCntState.ingCnt == 0 ? "" : "none" }}
          >
            <FormattedMessage
              id="description.no.ing.posting"
              defaultMessage="진행중인 채용공고가 없습니다."
            />
          </div>
          <ul>
            {listState.map((list, idx) =>
              list.hireStat === "ing" ? (
                (ingChkCnt = ingChkCnt + 1) < 4 ? (
                  <li>
                    <div className="listLeft">
                      <h3>
                        <Link
                          to="/employers/ing-detail"
                          state={{ hireUid: list.hireUid, mbrUid: list.mbrUid }}
                        >
                          {list.title}
                        </Link>
                      </h3>
                      <p>
                        <FormattedMessage
                          id="registration.date"
                          defaultMessage="등록일"
                        />{" "}
                        :&nbsp;{moment(list.rgstDttm).format("YYYY-MM-DD")}
                        &nbsp; /{" "}
                        <FormattedMessage
                          id="recruitment.deadline"
                          defaultMessage="인재추천 마감일"
                        />
                        :&nbsp;
                        {list.rcmndUntilDon === "Y"
                          ? `${intl.formatMessage({
                              id: "until.hired",
                              defaultMessage: "채용시까지",
                            })}`
                          : list.rcmndEndDttm}
                        &nbsp; /{" "}
                        <FormattedMessage
                          id="recruitment.people.number"
                          defaultMessage="모집인원"
                        />
                        : {list.hireSize}
                        <FormattedMessage
                          id="people.count"
                          defaultMessage="명"
                        />
                      </p>
                      <p>
                        <FormattedMessage
                          id="suggestion.service.fee"
                          defaultMessage="서비스 수수료율"
                        />{" "}
                        :&nbsp;
                        {list.srvcFeeFixYn === "Y"
                          ? `${intl.formatMessage({
                              id: "suggestion.fixed.fee.system",
                              defaultMessage: "정액제",
                            })}`
                          : list.srvcFeePctMin +
                            "% ~ " +
                            list.srvcFeePctMax +
                            "%"}
                        &nbsp; /{" "}
                        <FormattedMessage
                          id="suggestion.service.guarantee"
                          defaultMessage="서비스 보증조건"
                        />{" "}
                        :&nbsp;
                        {list.srvcWrtyPeriod}
                        <FormattedMessage
                          id="months"
                          defaultMessage="개월"
                        />{" "}
                        <FormattedMessage
                          id="suggestion.incase.retire"
                          defaultMessage="이내 퇴직시"
                        />
                        {list.srvcWrtyMthd === "prorated"
                          ? `${intl.formatMessage({
                              id: "suggestion.caculated.refund",
                              defaultMessage: "일할계산 환불",
                            })}`
                          : list.srvcWrtyMthd === "all"
                          ? `${intl.formatMessage({
                              id: "suggestion.all.refund",
                              defaultMessage: "100% 전액 환불",
                            })}`
                          : list.srvcWrtyMthd === "etc"
                          ? `${intl.formatMessage({
                              id: "etc",
                              defaultMessage: "기타",
                            })}`
                          : ""}
                      </p>
                      <CopyToClipboard
                        style={{ display: "none" }}
                        text={73}
                        onCopy={() =>
                          alert(`
                                ${intl.formatMessage({
                                  id: "url.copy.done",
                                  defaultMessage: "url 복사가 완료되었습니다.",
                                })}
                              `)
                        }
                      >
                        <p>
                          URL : 73{" "}
                          <button type="button">
                            URL{" "}
                            <FormattedMessage id="copy" defaultMessage="복사" />
                          </button>
                        </p>
                      </CopyToClipboard>
                    </div>
                    <div className="flexColumn listRight">
                      <div className="threeSections">
                        <div>
                          <span>
                            <FormattedMessage
                              id="recruiter.all"
                              defaultMessage="전체 리크루터"
                            />
                          </span>
                          <Link
                            to="/employers/recruiter-manage"
                            state={{
                              hireUid: list.hireUid,
                              mbrUid: list.mbrUid,
                            }}
                          >
                            {list.cmmntCnt}
                            <FormattedMessage
                              id="people.count"
                              defaultMessage="명"
                            />
                          </Link>
                        </div>
                        <div>
                          <span>
                            <FormattedMessage
                              id="recruiter.selected"
                              defaultMessage="선택한 리크루터"
                            />
                          </span>
                          <Link
                            to="/employers/recruiter-manage"
                            state={{
                              hireUid: list.hireUid,
                              mbrUid: list.mbrUid,
                            }}
                          >
                            <strong>{list.cmmntChooseCnt}</strong>/
                            {list.recSize}
                            <FormattedMessage
                              id="people.count"
                              defaultMessage="명"
                            />
                          </Link>
                        </div>
                        <div>
                          <span>
                            <FormattedMessage
                              id="recruiter.recommended"
                              defaultMessage="추천된 후보자"
                            />
                          </span>
                          <Link
                            to="/employers/candidate-manage"
                            state={{
                              hireUid: list.hireUid,
                              mbrUid: list.mbrUid,
                            }}
                          >
                            {list.recmmndCnt}
                            <FormattedMessage
                              id="people.count"
                              defaultMessage="명"
                            />
                            ({list.chkingRecmmndCnt}
                            <FormattedMessage
                              id="people.count"
                              defaultMessage="명"
                            />
                            /{list.nopassRecmmndCnt}
                            <FormattedMessage
                              id="people.count"
                              defaultMessage="명"
                            />
                            )
                          </Link>
                        </div>
                      </div>
                      <div className="qnaStatus">
                        <Link
                          to="/employers/qna-manage"
                          state={{ hireUid: list.hireUid, mbrUid: list.mbrUid }}
                        >
                          Q&amp;A{" "}
                          <span>
                            {list.qaCnt}
                            <FormattedMessage id="count" defaultMessage="개" />
                          </span>
                        </Link>
                      </div>
                      <p>
                        <FormattedMessage
                          id="description.open.to.recruiter"
                          defaultMessage="* 채용진행 현황은 참여중인 리크루터에게도 공개됩니다."
                        />
                      </p>
                    </div>
                  </li>
                ) : (
                  ""
                )
              ) : (
                ""
              )
            )}
          </ul>
        </div>
        <div className="doneList">
          <h2 className="sectionTitle">
            <span>
              <FormattedMessage
                id="posting.done"
                defaultMessage="종료된 채용공고"
              />
            </span>
            <Link to="/employers/done">
              <FormattedMessage id="show.all" defaultMessage="전체보기" /> &gt;
            </Link>
          </h2>
          <div
            className="noRecruitList"
            style={{ display: listCntState.endCnt == 0 ? "" : "none" }}
          >
            <FormattedMessage
              id="description.no.done.posting"
              defaultMessage="종료된 채용공고가 없습니다."
            />
          </div>
          <ul>
            {listState.map((list, idx) =>
              list.hireStat === "end" ? (
                (endChkCnt = endChkCnt + 1) < 4 ? (
                  <li>
                    <div className="listLeft">
                      <h3>
                        <Link
                          to="/employers/ing-detail"
                          state={{ hireUid: list.hireUid, mbrUid: list.mbrUid }}
                        >
                          {list.title}
                        </Link>
                      </h3>
                      <p>
                        <FormattedMessage
                          id="registration.date"
                          defaultMessage="등록일"
                        />{" "}
                        :&nbsp;{moment(list.rgstDttm).format("YYYY-MM-DD")}
                        &nbsp; /{" "}
                        <FormattedMessage
                          id="recruitment.deadline"
                          defaultMessage="인재추천 마감일"
                        />
                        :&nbsp;
                        {list.rcmndUntilDon === "Y"
                          ? `${intl.formatMessage({
                              id: "until.hired",
                              defaultMessage: "채용시까지",
                            })}`
                          : list.rcmndEndDttm}
                        &nbsp; /{" "}
                        <FormattedMessage
                          id="recruitment.people.number"
                          defaultMessage="모집인원"
                        />
                        : {list.hireSize}
                        <FormattedMessage
                          id="people.count"
                          defaultMessage="명"
                        />
                      </p>
                      <p>
                        <FormattedMessage
                          id="suggestion.service.fee"
                          defaultMessage="서비스 수수료율"
                        />{" "}
                        :&nbsp;
                        {list.srvcFeeFixYn === "Y"
                          ? `${intl.formatMessage({
                              id: "suggestion.fixed.fee.system",
                              defaultMessage: "정액제",
                            })}`
                          : list.srvcFeePctMin +
                            "% ~ " +
                            list.srvcFeePctMax +
                            "%"}
                        &nbsp; /{" "}
                        <FormattedMessage
                          id="suggestion.service.guarantee"
                          defaultMessage="서비스 보증조건"
                        />{" "}
                        :&nbsp;
                        {list.srvcWrtyPeriod}
                        <FormattedMessage
                          id="months"
                          defaultMessage="개월"
                        />{" "}
                        <FormattedMessage
                          id="suggestion.incase.retire"
                          defaultMessage="이내 퇴직시"
                        />
                        {list.srvcWrtyMthd === "prorated"
                          ? `${intl.formatMessage({
                              id: "suggestion.caculated.refund",
                              defaultMessage: "일할계산 환불",
                            })}`
                          : list.srvcWrtyMthd === "all"
                          ? `${intl.formatMessage({
                              id: "suggestion.all.refund",
                              defaultMessage: "100% 전액 환불",
                            })}`
                          : list.srvcWrtyMthd === "etc"
                          ? `${intl.formatMessage({
                              id: "etc",
                              defaultMessage: "기타",
                            })}`
                          : ""}
                      </p>
                    </div>
                    <div className="flexColumn listRight">
                      <div className="threeSections">
                        <div>
                          <span>
                            <FormattedMessage
                              id="recruiter.all"
                              defaultMessage="전체 리크루터"
                            />
                          </span>
                          <Link
                            to="/employers/recruiter-manage"
                            state={{
                              hireUid: list.hireUid,
                              mbrUid: list.mbrUid,
                              backUrl: "/employers/done-detail",
                            }}
                          >
                            {list.cmmntCnt}
                            <FormattedMessage
                              id="people.count"
                              defaultMessage="명"
                            />
                          </Link>
                        </div>
                        <div>
                          <span>
                            <FormattedMessage
                              id="recruiter.selected"
                              defaultMessage="선택한 리크루터"
                            />
                          </span>
                          <Link
                            to="/employers/recruiter-manage"
                            state={{
                              hireUid: list.hireUid,
                              mbrUid: list.mbrUid,
                              backUrl: "/employers/done-detail",
                            }}
                          >
                            <strong>{list.cmmntChooseCnt}</strong>/
                            {list.recSize}
                            <FormattedMessage
                              id="people.count"
                              defaultMessage="명"
                            />
                          </Link>
                        </div>
                        <div>
                          <span>
                            <FormattedMessage
                              id="candidate.recommended"
                              defaultMessage="추천된 후보자"
                            />
                          </span>
                          <Link
                            to="/employers/candidate-manage"
                            state={{
                              hireUid: list.hireUid,
                              mbrUid: list.mbrUid,
                              backUrl: "/employers/done-detail",
                            }}
                          >
                            {list.recmmndCnt}
                            <FormattedMessage
                              id="people.count"
                              defaultMessage="명"
                            />
                            ({list.chkingRecmmndCnt}
                            <FormattedMessage
                              id="people.count"
                              defaultMessage="명"
                            />
                            /{list.nopassRecmmndCnt}
                            <FormattedMessage
                              id="people.count"
                              defaultMessage="명"
                            />
                            )
                          </Link>
                        </div>
                      </div>
                      <div className="qnaStatus">
                        <Link
                          to="/employers/qna-manage"
                          state={{
                            hireUid: list.hireUid,
                            mbrUid: list.mbrUid,
                            backUrl: "/employers/done-detail",
                          }}
                        >
                          Q&amp;A{" "}
                          <span>
                            {list.qaCnt}
                            <FormattedMessage id="count" defaultMessage="개" />
                          </span>
                        </Link>
                      </div>
                      <p>
                        <FormattedMessage
                          id="description.open.to.recruiter"
                          defaultMessage="* 채용진행 현황은 참여중인 리크루터에게도 공개됩니다."
                        />
                      </p>
                    </div>
                  </li>
                ) : (
                  ""
                )
              ) : (
                ""
              )
            )}
          </ul>
        </div>
      </section>
    </main>
  );
};
