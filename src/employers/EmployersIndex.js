import { Route, Routes } from "react-router-dom";

import "./components/scss/form.scss";
import "./components/scss/employer-list.scss";
import "./components/scss/search.scss";
import "./components/scss/section.scss";

import { Header } from "./components/common/Header";
import { Aside } from "./components/common/Aside";
import { Footer } from "./components/common/Footer";

// 인덱스 페이지
import { Main } from "./components/Main";
import { Confirm } from "./components/postings/confirm/Confirm";
import { Ing } from "./components/postings/ing/Ing";
import { Done } from "./components/postings/done/Done";
import { Payment } from "./components/payment/Payment";
import { SearchFirm } from "./components/searchFirm/SearchFirm";
import { Department } from "./components/department/Department";
import { Notice } from "./components/notice/Notice";
import { JobPosting } from "./components/job-posting/JobPosting";
import { ConfirmDetail } from "./components/postings/confirm/ConfirmDetail";
import { IngDetail } from "./components/postings/ing/IngDetail";
import { DoneDetail } from "./components/postings/done/DoneDetail";
import { Recruiter } from "./components/manage/Recruiter";
import { Candidate } from "./components/manage/Candidate";
import { Qna } from "./components/manage/Qna";

import { Mypage } from "./components/mypage/Mypage";
import { Search } from "./components/search/Search";
import { DepartmentJobPosting } from "./components/department-account/DepartmentJobPosting";
import { RecruiterInfo } from "./components/manage/RecruiterInfo";

// 서브 페이지
function EmployersIndex({ logout }) {
  return (
    <>
      <Header logout={logout} />
      <div className="mainWrap">
        <Aside />

        <Routes>
          <Route path="/" element={<Main />} />

          {/* 서브페이지 */}
          <Route path="/search" element={<Search />} />
          <Route path="/job-posting" element={<JobPosting />} />

          <Route path="/notice" element={<Notice />} />

          <Route path="/confirm" element={<Confirm />} />
          <Route path="/ing" element={<Ing />} />
          <Route path="/done" element={<Done />} />
          <Route path="/payment" element={<Payment />} />
          <Route path="/mypage" element={<Mypage />} />
          <Route path="/search-firm" element={<SearchFirm />} />
          <Route path="/department" element={<Department />} />

          {/* 상세페이지 */}
          <Route path="/confirm-detail" element={<ConfirmDetail />} />
          <Route path="/ing-detail" element={<IngDetail />} />
          <Route path="/done-detail" element={<DoneDetail />} />

          <Route path="/recruiter-manage" element={<Recruiter />} />
          <Route path="/candidate-manage" element={<Candidate />} />
          <Route path="/qna-manage" element={<Qna />} />

          <Route path="/recruiter-info" element={<RecruiterInfo />} />

          {/* 현업부서 아이디로 로그인시 화면 */}
          <Route path="/department-job-posting" element={<DepartmentJobPosting />} />

        </Routes>
      </div>
      <Footer />
    </>
  );
}

export default EmployersIndex;
